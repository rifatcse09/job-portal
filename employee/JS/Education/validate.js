function validate_education(){
    var level = document.getElementById('education_level');
    var title = document.getElementById('exam_title');
    var major = document.getElementById('major');
    var institute = document.getElementById('institute');
    var result = document.getElementById('result');
    var year = document.getElementById('year_passing');
    if(level.value == "-1" || title.value == "" || major.value == "" || institute == "" || result.value == "" || year.value ==""){
        alert('please fill up all required fields.');
        return false;
    }
    return true;
}

function validate_training_sum(){
    var title = document.getElementById('training_title');
    var topics = document.getElementById('topics_covered');
    var institute = document.getElementById('train_institute');
    var country = document.getElementById('train_country');
    var location = document.getElementById('train_location');
    var year = document.getElementById('year_training');
    var duration = document.getElementById('train_duration');
    if(title.value == "" || topics.value == "" || institute.value == "" || 
        location == "" || year.value == "" || duration.value =="" || country.value == ""){
        alert('please fill up all required fields.');
        return false;
    }
    return true;
}

function validate_prof_qual(){
    var title = document.getElementById('prof_certification');
    var topics = document.getElementById('prof_institute');
    var institute = document.getElementById('prof_from');
    var country = document.getElementById('prof_to');
    
    if(title.value == "" || topics.value == "" || institute.value == "" || country.value == ""){
        alert('please fill up all required fields.');
        return false;
    }
    return true;
}

function validate_education_edit(){
    var level = document.getElementById('education_level_edit');
    var title = document.getElementById('exam_title_edit');
    var major = document.getElementById('major_edit');
    var institute = document.getElementById('institute_edit');
    var result = document.getElementById('result_edit');
    var year = document.getElementById('year_passing_edit');
    if(level.value == "-1" || title.value == "" || major.value == "" || institute == "" || result.value == "" || year.value ==""){
        alert('please fill up all required fields.');
        return false;
    }
    return true;
}

function edit_education(id){
    $.ajax({
        url: base_url + "/cont_education/edit_education_view?id=" + id,
        type: "GET",
        success: function(d){
            $("#edit_diag").html(d);
        }
    })
    $('#edit_diag').dialog('open');
}