
function personal_validate(form1,strDateType) {
    var NAME=document.getElementById('account_name').value //form1.txtName.value;//
    var blue_check = document.getElementById('blue_worker_true').checked;
    
    var GENDER=document.getElementById('account_gender').value//cboGender
    var MSTATUS=document.getElementById('account_marital_status').value
    var NATIONALITY=document.getElementById('account_nationality').value//form1.txtNationality.value;//
    
    var PRESENT_ADD=document.getElementById('account_present_address').value//form1.txtPresentAdd.value//
    
    var PRESENT_LOC=document.getElementById('account_location').options[document.getElementById('account_location').selectedIndex].value;
    var PHONE_O=document.getElementById('account_contact_office').value//form1.txtPhone_Off.value;//
    var PHONE_H=document.getElementById('account_contact_home').value//form1.txtPhone_H.value;//
    var MOBILE=document.getElementById('account_contact_mobile').value//form1.txtMobile.value;//
    var EMAIL_1 =trim_it(document.getElementById('account_email').value);//form1.txtEmail1.value;//
    var EMAIL_2 =trim_it(document.getElementById('account_alt_email').value); //form1.txtEmail2.value;//
    var OBJ=document.getElementById('career_objective').value//form1.txtObjective.value;//
    var EXPERIENCE=document.getElementById('career_year_experience').value; //txtExperience
    var PRESENT_SAL=document.getElementById('career_present_salary').value//form1.txtPresentSalary.value;//
    var EXPECTED_SAL=document.getElementById('career_exp_salary').value//form1.txtExpectedSalary.value;//
    var KEYWORD=document.getElementById('account_keyword').value//form1.txtKeyword.value;//
    
    var PRE_CAT=document.getElementById("selected_Cat").value;
    var JOB_LOCATION=document.getElementById("selected_Dist").value;
    //var JOB_LOCATION_OPT=document.form1.optJobArea.value;
    if( NAME == "")//if( NAME1 == "")
    {
        alert('Name field cannot be blank!');
        document.getElementById('account_name').focus();
        return false;
    }
    // if invalid birth date
    /*if(BIRTH_DATE=="")
{
alert('Please enter your Date of Birth.');
//form1.txtBirthDate.focus();
document.getElementById('txtBirthDate').focus();
return false;
}
else
{
var today;
var birth;
birth=new Date(BIRTH_DATE);
today=new Date();
if (today.getFullYear()-birth.getFullYear()<15 || today.getFullYear()-birth.getFullYear()>85)
{
alert('Age must between 15 to 85 years');
return false;
}
}*/
    var DOB = document.getElementById('account_dob');
    if(DOB == "" && blue_check == false){
        alert("please select your birth date");
        document.getElementById('account_dob').focus();
        return false;
    }
    //return true;
    if (GENDER=="-1")//Gender
    {
        alert('Please select gender.');
        document.getElementById('account_gender').focus();
        return false;
    }
    if (MSTATUS=="-1")//Marital Status
    {
        alert('Please select marital status.');
        document.getElementById('account_marital_status').focus();
        return false;
    }
    if(NATIONALITY=="" && blue_check == false) // if NATIONALITY==""
    {
        alert('Check your Nationality.');
        //form1.txtNationality.focus();
        document.getElementById('account_nationality').focus();
        return false;
    }
    /*if(RELIGION=="") // if RELIGION==""
{
alert('Check your Religion.');
//form1.txtReligion.focus();
document.getElementById('txtReligion').focus();
return false;
}*/
    if (PRESENT_ADD=="") // if PRESENT_ADD==""
    {
        alert('Present address field cannot be blank!.');
        document.getElementById('account_present_address').focus();
        //form1.txtPresentAdd.focus();
        return false;
    }
    if (PRESENT_LOC=="-1" && blue_check == false) // if PRESENT_LOC==""
    {
        alert('Please select present location.');
        //form1.cboPLocation.focus();
        document.getElementById('account_location').focus();
        return false;
    }
    if (PHONE_O=="" && PHONE_H=="" && MOBILE=="") // if PHONE==""
    {
        alert("Please give at least one contact number.");
        //form1.txtPhone_H.focus();
        document.getElementById('account_contact_home').focus();
        return false;
    }
    if(EMAIL_1 == "" && blue_check == false) // if Emaol==""
    {
        alert("Please give one valid e-mail address in Email 1 field.");
        //form1.txtEmail1.focus();
        document.getElementById('account_email').focus();
        return false;
    }
    //E-mail validation
    if(EMAIL_1 != "")
    {
        if (validate_email(EMAIL_1,"Not a valid e-mail address!")==false)
        {
            //form1.txtEmail1.focus();
            document.getElementById('account_email').focus();
            return false
            }
    }
    //alternate email
    if(EMAIL_2 != "")
    {
        if (validate_email(EMAIL_2,"Not a valid e-mail address!")==false)
        {
            //form1.
            document.getElementById('account_alt_email').focus();
            return false
            }
    }
    if(OBJ=="" && blue_check == false)
    {
        alert('Objective field cannot be blank!');
        // form1.txtObjective.focus();
        document.getElementById('career_objective').focus();
        return false;
    }
    if (EXPERIENCE !="" && blue_check == false)
    {
        if ( parseInt(EXPERIENCE) >70)
        {
            alert('Invalid year of Experience');
            document.getElementById('career_year_experience').focus();
            return false;
        }
    }
    /*if(EXPECTED_SAL=="")
{
alert('Expected salary field cannot be blank!');
form1.txtExpectedSalary.focus();
return false;
}*/
    if(PRE_CAT=="" && blue_check == false)
    {
        alert('You have to select at least one preferred category!');
        // form1.lstWorkArea.focus();
        document.getElementById('selected_Cat').focus();
        return false;
    }
    if (form1.optJobArea[1].checked)
    {
        if (JOB_LOCATION=="" && blue_check == false)
        {
            alert('Please select preferred Job Area!');
            //form1.
            document.getElementById('lstJobArea').focus();
            return false;
        }
    }
    if(KEYWORD=="" && blue_check == false)
    {
        alert('Keyword field cannot be blank!');
        //form1.
        document.getElementById('account_keyword').focus();
        return false;
    }
    // this section used only for step_01
    if(document.getElementById('account_username'))
    {
        var USER=document.getElementById('account_username').value;
        var PASS1=document.getElementById('account_password').value;
        var PASS2=document.getElementById('account_retype_password').value;
        //--------------------------------------------User Name Validation--------------------------------------------------------------------------------
        if (USER=="")
        {
            alert('Please enter user name');
            form1.account_username.focus();
            return false;
        }
        if (USER.length < 5 )
        {
            alert('User name must be at least 5 characters!');
            form1.account_username.focus();
            return false;
        }
        //dv.indexOf(';', 0) >= 0
        if (USER.indexOf('/', 0) >= 0 )
        {
            alert('User name does not allow /');
            form1.account_username.focus();
            return false;
        }
        // If does not work properly
        if (USER.indexOf("\\", 0) >= 0 )
        {
            alert("User name does not allow '\\'");
            form1.account_username.focus();
            return false;
        }
        if (USER.indexOf(';', 0) >= 0 )
        {
            alert('User name does not allow ;');
            form1.account_username.focus();
            return false;
        }
        if (USER.indexOf(':', 0) >= 0 )
        {
            alert('User name does not allow :');
            form1.account_username.focus();
            return false;
        }
        if (USER.indexOf('&', 0) >= 0 )
        {
            alert('User name does not allow & ');
            form1.account_username.focus();
            return false;
        }
        if (USER.indexOf('"', 0) >= 0 )
        {
            alert('User name does not allow "');
            form1.account_username.focus();
            return false;
        }
        if (USER.indexOf("'", 0) >= 0 )
        {
            alert("User name does not allow '");
            form1.account_username.focus();
            return false;
        }
        if (USER.indexOf(' ', 0) >= 0 )
        {
            alert('User Name should be only one word! No space allowed.');
            form1.account_username.focus();
            return false;
        }
        //-------------------------------------------Password Validation-------------------------------------------------------------------------------------------
        if(PASS1=="")
        {
            alert('Please enter your password');
            form1.account_password.focus();
            return false;
        }
        if (PASS1.indexOf(' ', 0) >= 0 )
        {
            alert('Password should be only one word!');
            form1.account_password.focus();
            return false;
        }
        if (PASS1.indexOf("'", 0) >= 0 )
        {
            alert("Password field does not allow ' ");
            form1.account_password.focus();
            return false;
        }
        if (PASS1.indexOf('"', 0) >= 0 )
        {
            alert('Password field does not allow "');
            form1.account_password.focus();
            return false;
        }
        if (PASS1.indexOf(';', 0) >= 0 )
        {
            alert('Password field does not allow ;');
            form1.account_password.focus();
            return false;
        }
        if (PASS1.indexOf('&', 0) >= 0 )
        {
            alert('Password field does not allow &');
            form1.account_password.focus();
            return false;
        }
        if (PASS1.length<8)
        {
            alert(' Password must be 8 to 12 characters long .');
            form1.account_password.focus();
            return false;
        }
        if(PASS2=="")
        {
            alert('Please re-enter your password.');
            form1.account_retype_password.focus();
            return false;
        }
        if(PASS1!=PASS2)
        {
            alert('Please be sure of your password \n Enter same password in the fields');
            form1.account_retype_password.focus();
            return false;
        }
    //-------------------------------End Password Validation----------------------------------------------------------------------------------
    }
} // end of validate func()
function trim_it(string_txt)
{
    elem = string_txt ; ///document.getElementById(filename) ;
    while(elem.charAt(0)==' ')// Ltrim
    {
        elem = elem.substring(1,elem.length);
    }
    while(elem.charAt(elem.length - 1)==' ') // rtrim
    {
        elem = elem.substring(0,elem.length - 1);
    }
    return elem ;
}//fnc trim_it


function edit_personal(id){
    $.ajax({
        url: base_url + "/cont_personal/edit_personal_view?id=" + id,
        type: "GET",
        success: function(d){
            $("#edit_diag").html(d);
        }
    })
    $('#edit_diag').dialog('open');
}

function validate_personal_edit(){
    var NAME=document.getElementById('personal_name_edit').value 
    var NATIONALITY=document.getElementById('personal_nationality').value
    var PRESENT_ADD=document.getElementById('personal_present_add').value
    var PHONE_O=document.getElementById('personal_contact_office').value
    var PHONE_H=document.getElementById('personal_contact_home').value
    var MOBILE=document.getElementById('personal_contact_mobile').value
    //var EMAIL_1 =trim_it(document.getElementById('account_email').value);
    if( NAME == "")//if( NAME1 == "")
    {
        alert('Name field cannot be blank!');
        document.getElementById('personal_name_edit').focus();
        return false;
    }
    
    if(NATIONALITY=="")
    {
        alert('you must have nationality.');
        //form1.txtNationality.focus();
        document.getElementById('personal_nationality').focus();
        return false;
    }
    
    if (PRESENT_ADD=="")
    {
        alert('Present address field cannot be blank!.');
        document.getElementById('personal_present_add').focus();
        //form1.txtPresentAdd.focus();
        return false;
    }
    
    if(PHONE_H == "" && PHONE_O == "" && MOBILE == ""){
        alert('You must provide at least one phone number!');
        document.getElementById('personal_contact_home').focus();
        return false;
    }
}