function edit_career(id){
    $.ajax({
        url: base_url + "/cont_personal/edit_career_view?id=" + id,
        type: "GET",
        success: function(d){
            $("#edit_diag_career").html(d);
        }
    })
    $('#edit_diag_career').dialog('open');
}

function edit_category(id){
    $.ajax({
        url: base_url + "/cont_personal/edit_category_view?id=" + id,
        type: "GET",
        success: function(d){
            $("#edit_diag_category").html(d);
        }
    })
    $('#edit_diag_category').dialog('open');
}

function edit_location(id){
    $.ajax({
        url: base_url + "/cont_personal/edit_location_view?id=" + id,
        type: "GET",
        success: function(d){
            $("#edit_diag_location").html(d);
        }
    })
    $('#edit_diag_location').dialog('open');
}

function edit_organization(id){
    $.ajax({
        url: base_url + "/cont_personal/edit_organization_view?id=" + id,
        type: "GET",
        success: function(d){
           $("#edit_diag_organization").html(d);
        }
    });
    $('#edit_diag_organization').dialog('open');
}
function edit_relevant(id){
    $.ajax({
        url: base_url + "/cont_personal/edit_relevant_view?id=" + id,
        type: "GET",
        success: function(d){
            $("#edit_diag_relevant").html(d);
        }
    })
    $('#edit_diag_relevant').dialog('open');
}

function validate_career_edit(){
    var c_summary = document.getElementById('career_summary');
    var keyword = document.getElementById('keywords');
    
    if(!c_summary.value || !keyword.value){
        alert("Career Summary and Keyword should not be blank.");
        return false;
    }
}