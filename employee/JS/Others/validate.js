function validate_language(){
    var level = document.getElementById('education_level');
    var title = document.getElementById('exam_title');
    var major = document.getElementById('major');
    var institute = document.getElementById('institute');
    var result = document.getElementById('result');
    var year = document.getElementById('year_passing');
    if(level.value == "-1" || title.value == "" || major.value == "" || institute == "" || result.value == "" || year.value ==""){
        alert('please fill up all required fields.');
        return false;
    }
    return true;
}

function validate_reference(){
    var name = document.getElementById('ref_name');
    var org = document.getElementById('ref_org');
    var designation = document.getElementById('ref_designation');
    
    if(name.value == "" || org.value == "" || designation.value == ""){
        alert('please fill up all required fields.');
        return false;
    }
    return true;
}

function validate_reference_edit(){
    var name = document.getElementById('ref_name_edit');
    var org = document.getElementById('ref_org_edit');
    var designation = document.getElementById('ref_designation_edit');
    
    if(name.value == "" || org.value == "" || designation.value == ""){
        alert('please fill up all required fields.');
        return false;
    }
    return true;
}

function edit_reference(id){
    $.ajax({
        url: base_url + "/other_quality/edit_reference_view?id=" + id,
        type: "GET",
        success: function(d){
            $("#edit_diag").html(d);
        }
    })
    $('#edit_diag').dialog('open');
}

function validate_employment(){
    var comp_name = document.getElementById('emp_company');
    var business = document.getElementById('emp_business');
    var location = document.getElementById('emp_business');
    var department = document.getElementById('emp_department');
    var position = document.getElementById('emp_position');
    
    if(comp_name.value == "" || business.value == "" || location.value == "" || department.value == "" || position == ""){
        alert('please fill up all required fields.');
        return false;
    }
    return true;
}

function disable_field(id_to_disable, id_on_click){
    var field = document.getElementById(id_to_disable);
    var chkBox = document.getElementById(id_on_click);
    if(chkBox.checked){
        field.value = "";
        field.disabled = true;
    }
    else{
        field.disabled = false
    }
}

function edit_experience(id){
    $.ajax({
        url: base_url + "/cont_employment/edit_employment_view?id=" + id,
        type: "GET",
        success: function(d){
            $("#edit_diag").html(d);
        }
    })
    $('#edit_diag').dialog('open');
}

function edit_language(){
    $.ajax({
        url: base_url + "/other_quality/edit_language_view",
        type: "GET",
        success: function(d){
            $("#edit_diag").html(d);
        }
    })
    $('#edit_diag').dialog('open');
}

function validate_employment_edit(){
    var comp_name = document.getElementById('emp_company_edit');
    var business = document.getElementById('emp_business_edit');
    var location = document.getElementById('emp_business_edit');
    var department = document.getElementById('emp_department_edit');
    var position = document.getElementById('emp_position_edit');
    
    if(comp_name.value == "" || business.value == "" || location.value == "" || department.value == "" || position == ""){
        alert('please fill up all required fields.');
        return false;
    }
    return true;
}