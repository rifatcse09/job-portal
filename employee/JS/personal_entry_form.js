var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
    var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
} 

function ClearListBox(lst)
{
    var len=document.getElementById(lst).length ;
    for (i=0; i<=len; i++)
    {
        document.getElementById(lst).remove(0)
    }
} 

function append(fromSel,theSel,hID,maxSize,msg)
{
    var selIndex;
    var IDCollection;
    FSel=document.getElementById(fromSel);
    TSel=document.getElementById(theSel);
    selIndex = FSel.selectedIndex;
    if (TSel.length >=parseInt(maxSize))
    {
        alert(msg);
        return false;
    }
    if (selIndex>=0)
    {
        newValue=FSel.options[selIndex].value;
        newText=FSel.options[selIndex].text;
        for(i = 0 ; i < TSel.length ; i++)// find duplicate
        {
            if (newValue==TSel.options[i].value)
            {
                return false;
            }
        }//end for
        var newOpt1 = new Option(newText, newValue);
        TSel.options[TSel.length] = newOpt1;
        TSel.selectedIndex =TSel.length-1;
        IDCollection=ListBox_IDCollection(theSel);
        document.getElementById(hID).value=IDCollection;
    }
} 

function isDate(dtStr, strDateType){
    var daysInMonth = DaysArray(12);
    var pos1=dtStr.indexOf(dtCh);
    var pos2=dtStr.indexOf(dtCh,pos1+1);
    var strMonth=dtStr.substring(0,pos1);
    var strDay=dtStr.substring(pos1+1,pos2);
    var strYear=dtStr.substring(pos2+1);
    strYr=strYear;
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }
    month=parseInt(strMonth);
    day=parseInt(strDay);
    year=parseInt(strYr);
    /*THIS MESSAGE WILL NEVER DISPLAY IF DATE IS TAKEN FROM DROPDOWN LIST:*/
    if (pos1==-1 || pos2==-1){
        //alert("The date format should be : mm/dd/yyyy")
        if (strDateType=="DOB")
        {
            alert("Please enter a valid Date of Birth.");
        }
        if (strDateType=="ProQualification")
        {
            alert("Please enter a valid Date in Professional Qualificaton.");
        }
        if (strDateType=="Experience")
        {
            alert("Please enter a valid Date in Experience.");
        }
        return false;
    }
    if (strMonth.length<1 || month<1 || month>12){
        //alert("Please enter a valid month")
        if (strDateType=="DOB")
        {
            alert("Please enter a valid Date of Birth.");
        }
        if (strDateType=="ProQualification")
        {
            alert("Please enter a valid Date in Professional Qualificaton.");
        }
        if (strDateType=="Experience")
        {
            alert("Please enter a valid Date in Experience.");
        }
        return false;
    }
    if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
        //alert("Please enter a valid day")
        if (strDateType=="DOB")
        {
            alert("Please enter a valid Date of Birth.");
        }
        if (strDateType=="ProQualification")
        {
            alert("Please enter a valid Date in Professional Qualificaton.");
        }
        if (strDateType=="Experience")
        {
            alert("Please enter a valid Date in Experience.");
        }
        return false;
    }
    /*THIS MESSAGE WILL NEVER DISPLAY IF 4 DIGIT YEAR IS TAKEN FROM DROPDOWN LIST:*/
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
        //alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
        if (strDateType=="DOB")
        {
            alert("Please enter a valid Date of Birth.");
        }
        if (strDateType=="ProQualification")
        {
            alert("Please enter a valid Date in Professional Qualificaton.");
        }
        if (strDateType=="Experience")
        {
            alert("Please enter a valid Date in Experience.");
        }
        return false;
    }
    if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
        if (strDateType=="DOB")
        {
            alert("Please enter a valid Date of Birth.");
        }
        if (strDateType=="ProQualification")
        {
            alert("Please enter a valid Date in Professional Qualificaton.");
        }
        if (strDateType=="Experience")
        {
            alert("Please enter a valid Date in Experience.");
        }
        return false;
    }
    return true;
} 

function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31;
        if (i==4 || i==6 || i==9 || i==11) {
            this[i] = 30
            }
        if (i==2) {
            this[i] = 29
            }
    }
    return this;
} 

function ListBox_IDCollection(theSel)
{
    var TempID;
    var TSel;
    TempID="";
    TSel=document.getElementById(theSel);
    if (TSel.length>0)
    {
        for(i=0; i<TSel.length; i++)
        {
            if(TempID != "") //add new id to temp id
            {
                TempID = TempID + "," + TSel.options[i].value;
            }
            else
            {
                TempID = TSel.options[i].value;
            }
        }//end for
        TempID="," + TempID + ",";
    }//outer if
    return TempID;
}//end function 


function remove(tSel,hID)
{
    theSel=document.getElementById(tSel);
    var selIndex = theSel.selectedIndex;
    if (selIndex != -1)
    {
        if(theSel.options[selIndex].selected)
        {
            theSel.options[selIndex] = null;//alert(selIndex)
        }
        if (theSel.length > 0)
        {
            theSel.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
        }
        document.getElementById(hID).value=ListBox_IDCollection(tSel);
    }//outer if
}

function EnableDisable(obj,objState)
{
    document.getElementById(obj).disabled = objState;
}

function SetValueNull(obj)
{
    document.getElementById(obj).value = "";
}

function validate_email(dv,alerttxt) // e-mail validation
{
    if((dv.indexOf(':', 0) >= 0) || (dv.indexOf(';', 0) >= 0) ||(dv.indexOf('<', 0) >= 0) ||(dv.indexOf('>', 0) >= 0) ||(dv.indexOf('\\', 0) >= 0) ||(dv.indexOf(',', 0) >= 0) ||(dv.indexOf(' ', 0) >= 0) ||(dv.indexOf('/', 0) >= 0) ||(dv.indexOf('@', 0) == -1) || (dv.indexOf('.', 0) == -1))
    {
        alert(alerttxt);
        return false;
    }
    apos=dv.indexOf("@")
    dotpos=dv.lastIndexOf(".")
    if (apos<1||dotpos-apos<2)
    {
        alert(alerttxt);
        //alert("Second If");
        return false;
    }
    apos2=dv.lastIndexOf("@")
    if (apos !=apos2)
    {
        alert(alerttxt);
        //alert("Third If");
        return false;
    }
    else 
    {
        return true
    }
} 