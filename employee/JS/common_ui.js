function dialog_open(id, title){
    $(id).dialog({
        autoOpen: false,
        modal: true,
        width: "750",
        height: "auto",
        title: title,
        resizable: false,
        position: ["center", "top"]
    });
}