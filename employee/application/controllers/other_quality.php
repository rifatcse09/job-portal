<?php
class Other_Quality extends CI_Controller{
    function __construct() {
        parent::__construct();
    }
    
    function create_language(){
        $this->load->model('OtherQuality');
        $this->OtherQuality->create_language($this->session->userdata['user_id']);
        redirect('/page/step_04_view');
    }
    
    function create_reference(){
        $this->load->model('OtherQuality');
        $this->OtherQuality->create_reference($this->session->userdata['user_id']);
        redirect('/page/step_04_view');
    }
    
    function edit_language_view(){
        $this->load->view('editresume/edit_language');
    }
    
    function edit_language(){
        $this->load->model('OtherQuality');
        $this->OtherQuality->edit_language($this->session->userdata['user_id']);
        redirect('/page/step_04_view');
    }
    
    function edit_reference_view(){
        $id = $_GET['id'];
        $this->load->model('OtherQuality');
        $data['reference'] = $this->OtherQuality->get_reference($id);
        $data['id'] = $id;
        $this->load->view('editresume/edit_reference', $data);
    }
    
    function edit_reference(){
        $this->load->model('OtherQuality');
        $this->OtherQuality->edit_reference($this->session->userdata['user_id']);
        redirect('/page/step_04_view');
    }
}
?>
