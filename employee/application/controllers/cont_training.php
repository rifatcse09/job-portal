<?php
class Cont_Training extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    function create_training_summary(){
        $this->load->model('Training_Summary');
        $this->Training_Summary->save_training_summary($this->session->userdata['user_id']);
        redirect('/page/step_02_view');
    }
}

?>
