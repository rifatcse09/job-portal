<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller {

    function Page() {
        parent::__construct();
    }

    function index() {
        if (isset($this->session->userdata['user_id'])) {
            $this->load->library('template');
            $this->load->model('User');

            $this->template->write('title', 'Welcome');
            $this->template->write_view('header', 'common/header');
            $this->template->write_view('sidebar', 'common/sidebar');

            $data['personal'] = $this->User->get_user_personal($this->session->userdata['user_id']);
            $data['career'] = $this->User->get_user_career($this->session->userdata['user_id']);
            $data['pref_categories'] = $this->User->get_user_preferred_category($this->session->userdata['user_id']);
            $data['pref_orgs'] = $this->User->get_user_preferred_organization($this->session->userdata['user_id']);
            $data['pref_locs'] = $this->User->get_user_preferred_location($this->session->userdata['user_id']);
            $data['pref_cons'] = $this->User->get_user_preferred_country($this->session->userdata['user_id']);
            $data['summary'] = $this->User->get_user_relevant($this->session->userdata['user_id']);

            $this->template->write_view('content', 'editresume/step_01_view', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('msg', 'sorry, your session has been end. Login again.');

            redirect('page/login_failed');
        }
    }

    function login_failed() {
        $data['msg'] = $this->session->flashdata('msg');
        $data['title'] = isset($_GET['title']) ? $_GET['title'] : "Login Error";

        if (isset($_GET['link'])) {
            $this->session->set_flashdata('link', $_GET['link']);
        } else if ($this->session->flashdata('link')) {
            $this->session->set_flashdata('link', $this->session->flashdata('link'));
        }
        $this->load->view('login_failed', $data);
    }

    public function log_me_in() {
        $this->load->model('useraccount');
        if ($this->useraccount->log_me_in($_POST['user_name'], $_POST['password'])) {
            if ($this->session->flashdata('link')) {
                redirect(site_url() . '/' . $this->session->flashdata('link'));
                
                
            } else {
                redirect(site_url() . '/page');
            }
        } else {
            if ($this->session->flashdata('link')) {
                $this->session->set_flashdata('link', $this->session->flashdata('link'));
            }
            $this->session->set_flashdata('msg', $this->session->flashdata('msg'));
        redirect(site_url() . '/page/login_failed');
            
        }
    }
    
     public function login_from_mainpage() {
//        $this->load->model('useraccount');
//        if ($this->useraccount->log_me_in($_POST['user_name'], $_POST['password'])) {
//            if ($this->session->flashdata('link')) {
//                redirect(site_url() . '/' . $this->session->flashdata('link'));
//                
//                
//            } else {
//                redirect(site_url() . '/page');
//            }
//        } else {
//            if ($this->session->flashdata('link')) {
//                $this->session->set_flashdata('link', $this->session->flashdata('link'));
//            }
//            $this->session->set_flashdata('msg', $this->session->flashdata('msg'));
        redirect(site_url() . '/page/login_failed');
            
        //}
    }
    
    
    
    

    function view_resume() {
        $this->load->library('template');
        //User personal information
        $this->load->Model('User');
        $user_id = $this->session->userdata['user_id'];
        $data['personal'] = $this->User->get_user_personal($this->session->userdata['user_id']);
        $data['emp_history'] = $this->User->get_user_emp_history($user_id);
        $data['acc_qualification'] = $this->User->get_acc_qualification($user_id);
        $data['career'] = $this->User->get_user_career($user_id);
        $data['pre_location'] = $this->User->get_user_preferred_location($user_id);
        $data['pre_organization'] = $this->User->get_user_preferred_organization($user_id);
        $data['language'] = $this->User->get_language($user_id);
        $data['reference'] = $this->User->get_reference($user_id);
        $data['categories'] = $this->User->get_user_preferred_category($user_id);
        $data['summary'] = $this->User->get_user_relevant($user_id);
        $this->template->write('title', 'View Resume');
        $this->template->write_view('header', 'common/header');
        $this->template->write_view('sidebar', 'common/sidebar');
        $this->template->write_view('content', 'view_resume', $data);
        $this->template->render();
    }

	    public function appliedjob() {

          $id = $this->session->userdata['user_id'];
          $this->load->model('Job');


        $data['query'] = $this->Job->applied_job($id);



        //$this->load->view($template, $data);
        // $this->load->view('applied_job_view', $data);
        $this->load->library('template');
        $this->template->write('title', 'Welcome');
        $this->template->write_view('header', 'common/header');
        $this->template->write_view('sidebar', 'common/sidebar');
       
        $this->template->write_view('content', 'applied_job_view', $data);
        $this->template->render();
    }
    function step_02_view() {
        $this->load->model('Education');
        $this->load->model('Training_Summary');
        $this->load->model('Professional_Qualification');
        $this->load->library('template');
        $this->template->write('title', 'View Resume');
        $this->template->write_view('header', 'common/header');
        $this->template->write_view('sidebar', 'common/sidebar');

        $data['education_level']['levels'] = $this->Education->get_all_level();
        $data['educational_qualifications'] = $this->Education->get_all_education($this->session->userdata['user_id']);
        $data['training_summaries'] = $this->Training_Summary->get_training_summary($this->session->userdata['user_id']);
        $data['professional_qualifications'] = $this->Professional_Qualification->get_professional_qualification($this->session->userdata['user_id']);

        $this->template->write_view('content', 'editresume/step_02_view', $data, true);
        $this->template->render();
    }

    function step_03_view() {
        $this->load->model('Employment');
        $this->load->library('template');
        $this->template->write('title', 'View Resume');

        $data['employments'] = $this->Employment->get_all_employments($this->session->userdata['user_id']);
        $this->template->write_view('header', 'common/header');
        $this->template->write_view('sidebar', 'common/sidebar');
        $this->template->write_view('content', 'editresume/step_03_view', $data);
        $this->template->render();
    }

    function step_04_view() {
        $this->load->model('OtherQuality');
        $this->load->library('template');
        $this->template->write('title', 'View Resume');
        $this->template->write_view('header', 'common/header');
        $this->template->write_view('sidebar', 'common/sidebar');

        $data['languages'] = $this->OtherQuality->get_all_language($this->session->userdata['user_id']);
        $data['references'] = $this->OtherQuality->get_all_reference($this->session->userdata['user_id']);
        $this->template->write_view('content', 'editresume/step_04_view', $data);
        $this->template->render();
    }

    function step_05_view() {
        $this->load->library('template');
        $this->load->model('User');
        $user = $this->User->get_user_personal($this->session->userdata['user_id']);
        $data['image'] = $user['image'];
        $this->template->write('title', 'View Resume');
        $this->template->write_view('header', 'common/header');
        $this->template->write_view('sidebar', 'common/sidebar');
        $this->template->write_view('content', 'editresume/step_05_view', $data);
        $this->template->render();
    }

    public function upload_photograph() {
        $config['upload_path'] = './photographs/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '4000';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $config['overwrite'] = True;
        $ext = pathinfo($_FILES['photo_image']['name'], PATHINFO_EXTENSION);
        $file = $this->session->userdata['user_id'] . '.' . $ext;
        $config['file_name'] = $file;

        $this->load->helper('form');
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('photo_image')) {
            $data['error'] = array('error' => $this->upload->display_errors());
            $data['title'] = 'Upload';
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        $this->load->library('template');
        $this->load->model('User');
        $this->User->update_photograph($this->session->userdata['user_id'], $file);
        redirect('page/step_05_view');
    }
 function cv($id)
 {
//         $this->load->library('template');
//
//        //User personal information
//        $this->load->Model('User');
//        $user_id = $this->session->userdata['user_id'];
//        $data['personal'] = $this->User->get_user_personal($this->session->userdata['user_id']);
//        $data['emp_history'] = $this->User->get_user_emp_history($user_id);
//        $data['acc_qualification'] = $this->User->get_acc_qualification($user_id);
//        $data['career'] = $this->User->get_user_career($user_id);
//        $data['pre_location'] = $this->User->get_user_preferred_location($user_id);
//        $data['pre_organization'] = $this->User->get_user_preferred_organization($user_id);
//        $data['language'] = $this->User->get_language($user_id);
//        $data['reference'] = $this->User->get_reference($user_id);
//        $data['categories'] = $this->User->get_user_preferred_category($user_id);

       $data['id']=$id;
        $this->load->view('cv',$data);
        //$this->template->render();
 }
}

?>
