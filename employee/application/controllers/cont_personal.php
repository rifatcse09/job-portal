<?php

/**
 * Description of cont_personal
 *
 * @author Anick
 */
class Cont_personal extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function edit_personal_view() {
        $id = $_GET['id'];
        $this->load->model('User');
        $data['personal'] = $this->User->get_user_personal($id);
        $data['id'] = $id;
        $this->load->view('editresume/edit_personal', $data);
    }

    public function edit_personal() {
        $this->load->model('User');
        $this->User->edit_personal($_POST['personal_id']);
        redirect('/page/');
    }

    function edit_career_view() {
        $id = $_GET['id'];
        $this->load->model('User');
        $data['career'] = $this->User->get_user_career($id);
        $data['id'] = $id;
        $this->load->view('editresume/edit_career', $data);
    }

    public function edit_career() {
        $this->load->model('Career');
        $this->Career->edit_career($_POST['personal_id']);
        redirect('/page/');
    }

    function edit_category_view() {
        $id = $_GET['id'];
        $this->load->model('Category');
        $user_category = $this->Category->get_emp_category($id);
        $cat_ids = '';
        foreach ($user_category as $key => $cat) {
            if ($key == 0)
                $cat_ids .= $cat['category_id'];//category_id concatenate with $cat_ids
            else {
                $cat_ids .= ',' . $cat['category_id'];
            }
        }
        $data['categories'] = $this->Category->select_all_category();
        $data['user_categories'] = $user_category;
        $data['cat_ids'] = $cat_ids;
        $data['id'] = $id;
        $this->load->view('editresume/edit_job_category', $data);
    }
    

    public function edit_category() {
        $this->load->model('User');
        $this->User->update_employee_category($_POST['personal_id'], $_POST['selected_Cat']);
        redirect('/page/');
    }
    function edit_organization_view() {
        $id = $_GET['id'];
        $this->load->model(array('BusinessType','User'));//data fetches from two table
        $org_ids = '';
        $user_organization = $data['organization'] = $this->User->get_user_preferred_organization($id);
       
        foreach ($user_organization as $key => $org) {
            if ($key == 0)
                $org_ids .= $org['business_id'];
            else {
                $org_ids .= ',' . $org['business_id'];
            }
        }
        $data['business_areas'] = $this->BusinessType->select_all_business_type();
        $data['id'] = $id;
        $data['org_ids'] = $org_ids;
        $this->load->view('editresume/edit_job_organization', $data);
    }
//	
//     function edit_organization_view() {
//        $id = $_GET['id'];
//        $this->load->model('businesstype');
//       // $this->load->model('user');
//        $user_org = $this->businesstype->get_emp_businesstype($id);
//        $org_ids = '';
//        foreach ($user_org as $key => $org) {
//            if ($key == 0)
//                $org_ids .= $org['business_id'];
//            else {
//                $org_ids .= ',' . $org['business_id'];
//            }
//        }
//        $data['organizations'] = $this->businesstype->select_all_business_type();
//        $data['user_organizations'] = $user_org;
//        $data['org_ids'] = $org_ids;
//        $data['id'] = $id;
//        $this->load->view('editresume/edit_job_organization', $data);
//    }
//    
////
////    
     public function edit_organization()
    {
         $this->load->model('User');
         $this->User->update_employee_organization($_POST['personal_id'],$_POST['selected_org']);
         redirect('/page/');
         //$this->load->view('e');
    }

    public function edit_location_view() {
        $id = $_GET['id'];
        $this->load->model('Location');
        $user_location = $this->Location->get_emp_location($id);
        $user_country = $this->Location->get_emp_country($id);
        $loc_ids = '';
        $con_ids = '';
        foreach ($user_country as $key => $con) {
            if ($key == 0)
                $con_ids .= $con['country_id'];
            else {
                $con_ids .= ',' . $con['country_id'];
            }
        }
        foreach ($user_location as $key => $loc) {
            if ($key == 0)
                $loc_ids .= $loc['location_id'];
            else {
                $loc_ids .= ',' . $loc['location_id'];
            }
        }
        $data['district']['districts'] = $this->Location->select_all_cities();
        $data['district']['district_size'] = 5;
        $data['district']['additional_att'] = 'disabled';
        $data['district']['district_name'] = 'lstJobArea';
        $data['district']['district_id'] = 'lstJobArea';

        $data['country']['countries'] = $this->Location->select_all_countries();
        $data['country']['country_size'] = 5;
        $data['country']['country_name'] = 'lstJobCountry';
        $data['country']['country_id'] = 'lstJobCountry';

        $data['user_locations'] = $user_location;
        $data['loc_ids'] = $loc_ids;
        $data['user_countries'] = $user_country;
        $data['con_ids'] = $con_ids;
        $data['id'] = $id;

        $this->load->view('editresume/edit_job_location', $data);
    }

    public function edit_location() {
        $this->load->model('User');
        if (isset($_POST['selected_Dist'])) {
            $this->User->update_employee_location($_POST['personal_id'], $_POST['selected_Dist']);
        }
        if (isset($_POST['selected_JobCountry'])) {
            $this->User->update_employee_country($_POST['personal_id'], $_POST['selected_JobCountry']);
        }
        redirect('/page/');
    }
   

    function edit_relevant_view() {
        $id = $_GET['id'];
        $this->load->model('User');
        $data['relevant'] = $this->User->get_user_relevant($id);
        $data['id'] = $id;
        $this->load->view('editresume/edit_relevant', $data);
    }

    public function edit_relevant() {
        $this->load->model('User');
        $this->User->update_employee_relevant($_POST['personal_id']);
        redirect('/page/');
    }

}

?>
