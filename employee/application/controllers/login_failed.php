<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<head>
    <title><?= $title ?></title>

    <script type="text/javascript">
        function validate(){
            var user = document.getElementById('user_name');
            var pass= document.getElementById('password');
            
            if(user.value == "" || pass.value == ""){
                alert('You have to fill up both fields.');
                return false;
            }
        }
</script>
    <style>
          input[type=text]:focus, input[type=password]:focus 
                                               {
                                               box-shadow: 0 0 5px rgba(81, 203, 238, 1);
/*                                               padding: 3px 0px 3px 3px;*/
/*                                               margin: 5px 1px 3px 0px;*/
                                               border: 1px solid rgba(81, 203, 238, 1);
                                               }  
        input[type="submit"] 
                                               {
                                                       background-color: #F5F5F5;
                                                       border: 1px solid rgba(0, 0, 0, 0.1);
                                                       border-radius: 2px;
                                                       color: #222222;
                                                       font: bold 12px arial;
                                                       height: 30px;
                                                       margin: 1px 0 0 15px;
                                                       padding-bottom: 0;
                                                       text-align: center;
                                                       text-shadow: 0 1px rgba(0, 0, 0, 0.1);
                                                       vertical-align: top;
                                                       width: 80px;
                                              }
                                              #login {
    background-color: #4D90F0;
    border: 1px solid #3079ED;
    color: #FFFFFF;
    font-weight: bold;
    margin: 8px 0 0 18px;
}
                               #login:hover {
    background-color: #3571eb;
    border-color:#2f5bb7;
 
    font-weight: bold;
    margin: 8px 0 0 18px;
}   
    </style>
</head>
<body style="background-color:#CCE0FF">
    <div style='width: 500px;height: 500px; text-align: center; border-radius: 2px;margin: 0 auto; padding: 10px;background-color: #FFFFFF;
    background-image: url("http://localhost:81/purbodesh/employee/images/top_bg.gif");
    background-repeat: repeat-x;border: 1px solid #F0EEEE;
   '>
       
        <div><img style="float:left" src="http://localhost:81/purbodesh/employee/images/logo.png" width="120"></div>
        <div> 
        <form style='margin-top:100px' action="<?= site_url()?>/page/log_me_in" method="Post" onsubmit="return validate()" name="login_form">
            <? if($this->session->flashdata('link')) $this->session->set_flashdata('link', $this->session->flashdata('link')); ?>
            <table style='width: 100%;'>
                <thead>
                    <tr>
                        <th colspan="2" style="background: #000;color: #fff">User Login</th>
                    </tr>
                </thead>
               
                <tbody>
                       <tr style="height:20px"><td></td>
                       <td></td><tr>
                    <tr>
                        <td style="text-align:right">User name:</td>
                        <td style="text-align:left"><input type="text" name="user_name" id="user_name"/></td>
                    </tr>
                    <tr>
                        <td style="text-align:right">Password:</td>
                        <td style="text-align:left"><input type="password" name="password" id="password"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="1" style="text-align:left"><input id="login" type="submit" name="login_submit" id="login_submit" value="Login"/></td>
                    </tr>
          
                </tbody>
                 
            </table>
        </form>
         <?= $msg ?><br/>
        <a  style="text-decoration:none " href="<?= site_url()?>/createaccount">New user? Create a new account</a><br/>
        </div>
    </div>
</body>
</html>