<?php
/**
 * Description of cont_emplyment
 *
 * @author ANICK
 */
class Cont_Employment extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function create_employment(){
        $this->load->model('Employment');
        //echo $this->session->userdata['user_id'];
        $this->Employment->create_employment($this->session->userdata['user_id']);
        redirect('/page/step_03_view');
    }
    
    public function edit_employment_view(){
        $id = $_GET['id'];
        $this->load->model('Employment');
        $data['employment'] = $this->Employment->get_experience($id);
        $data['id'] = $id;
        $this->load->view('editresume/edit_employment', $data);
    }
    
    public function edit_employment(){
        $this->load->model('Employment');
        $this->Employment->edit_employment($this->session->userdata['user_id']);
        redirect('/page/step_03_view');
    }
}

?>
