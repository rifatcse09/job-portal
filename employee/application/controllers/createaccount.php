<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CreateAccount extends CI_Controller {

    function index() {
        if (isset($_GET['link'])) {
            $this->session->set_flashdata('link', $_GET['link']);
        } else if ($this->session->flashdata('link')) {
            $this->session->set_flashdata('link', $this->session->flashdata('link'));
        }
        $this->load->library('template');
        $this->load->model('Category');
        $this->load->model('BusinessType');
        $this->load->model('Location');

        $data['business']['business_areas'] = $this->BusinessType->select_all_business_type();
        $data['business']['business_type_size'] = 5;
        $data['business']['business_name'] = 'lstOrganization';
        $data['business']['business_id'] = 'lstOrganization';

        $data['district']['districts'] = $this->Location->select_all_cities();
        $data['district']['district_size'] = 5;
        $data['district']['additional_att'] = 'disabled';
        $data['district']['district_name'] = 'lstJobArea';
        $data['district']['district_id'] = 'lstJobArea';

        $data['country']['countries'] = $this->Location->select_all_countries();
        $data['country']['country_size'] = 5;
        $data['country']['country_name'] = 'lstJobCountry';
        $data['country']['country_id'] = 'lstJobCountry';

        $data['categories'] = $this->Category->select_all_category();

        $this->template->write('title', 'Create a new Account');
        $this->template->add_region('style');
        $this->template->write('style', '#content {width:1000px;}');
        $this->template->write_view('header', 'common/header');
        $this->template->write_view('content', 'createaccount/step_01', $data, True);
        $this->template->render();
    }

    //create account of a user through this controller
    function Register() {
        $this->load->model('User');
        $this->load->model('UserAccount');
        $this->load->model('Career');
        $this->load->library('template');
        $this->template->write_view('header', 'common/header');
        $msg = '';
        $title = '';
        //if user is not exist
        if ($this->UserAccount->check_user($_POST['account_username']) == true) {
            $res = $this->User->CreateAccount();
            $flag = false;

            //if user personal information saved, then create account info
            if ($this->User->id > 1) {
                $this->UserAccount->CreateUserAccount($this->User->id);
                $this->Career->CreateCareer($this->User->id);

                $categories = (isset($_POST['selected_Cat']) && $_POST['selected_Cat'] != '') ? $_POST['selected_Cat'] : '';
                $this->User->CreateEmployeeCategory($this->User->id, $categories);

                $jobs = (isset($_POST['selected_Job']) && $_POST['selected_Job'] != '') ? $_POST['selected_Job'] : '';
                $this->User->create_employee_preferred_job($this->User->id, $jobs);
                $this->User->create_employee_relevant($this->User->id);

                $districts = (isset($_POST['selected_Dist']) && $_POST['selected_Dist'] != '') ? $_POST['selected_Dist'] : '';
                $this->User->create_employee_location($this->User->id, $districts);

                $job_countries = (isset($_POST['selected_JobCountry']) && $_POST['selected_JobCountry'] != '') ? $_POST['selected_JobCountry'] : '';
                $this->User->create_employee_country($this->User->id, $job_countries);
                $flag = true;
            } else {
                $flag = FALSE;
                
            }

            if ($flag == FALSE) {
                $msg = 'Your Account hasn\'t created. Something is wrong. Please try again later';
                $title = 'Error';

                if ($this->session->flashdata('link')) {
                    $this->session->set_flashdata('link', $this->session->flashdata('link'));
                }
            } else {
                $msg = 'Your Account has been created. Thank you.';
                $title = 'Successful';
            }
        } else {
            $msg = 'User name already taken. Please try with another one.';
            $title = 'Error';

            if ($this->session->flashdata('link')) {
                $this->session->set_flashdata('link', $this->session->flashdata('link'));
            }
        }
        
        $this->UserAccount->log_me_in($_POST['account_username'], $_POST['account_password']);
        $this->session->set_flashdata('msg', $msg);
        if ($this->session->flashdata('link')) {
            redirect(site_url() . '/' . $this->session->flashdata('link'));
        } else {
            redirect(site_url() . '/page');
        }
    }

    function category_list() {
        $this->load->model('Category');
        $data['categories'] = $this->Category->select_all_category();

        $this->load->view('common/category_list', $data);
    }

    function change_password() {
        if (isset($this->session->userdata['user_id'])) {
            $data['user_name'] = $this->session->userdata['user_name'];

            $this->load->library('template');
            $this->template->write('title', 'Change Password');
            $this->template->write_view('header', 'common/header');
            $this->template->write_view('sidebar', 'common/sidebar');
            $this->template->write_view('content', 'createaccount/change_password', $data, True);

            $this->template->render();
        } else {
            $this->session->flashdata('msg', 'You are either not logged in or your sesison has been destroyed. please login again for changing password.');
            $this->session->set_flashdata('link', 'createaccount/change_password');
            redirect('page/login_failed');
        }
    }

    function change_password_post() {
        if (isset($this->session->userdata['user_id'])) {
            $user_id = $this->session->userdata['user_id'];
            $user_name = $_POST['account_username'];
            $password = $_POST['account_password'];
            $this->load->model('UserAccount');

            if (!$this->UserAccount->change_password($user_id, $user_name, $password)) {
                $this->session->keep_flashdata('msg', 'Passord couldn\'t change.Please try again.');
            } else {
                $this->load->model('User');
                $this->load->library('email');
                $user_info = $this->UserAccount->get_user_account_info($user_id);
                //send message informing the password change
                $send_to = $user_info[0]['email'];
                $send_from = 'info@ckrbd.com';
                $subject = "Password Change";

                $msg = "You have recently Changed your password. Your new login credentials are as follows: <br/>";
                $msg .= "User Name: " . $user_name . "<br/>";
                $msg .= "Password: " . $password . "<br/>";

                $config = Array(
                    'protocol' => 'smtp',
            'smtp_host' => 'ckrbd.com',
            'smtp_port' => '25',
            'smtp_user' => 'info@ckrbd.com',
            'smtp_pass' => 'pentagems14',
            'mailtype' => 'html',
			 'smtp_timeout' =>30
                );
                $this->email->set_mailtype("html");
               // $this->email->initialize($config);
                $this->email->from($send_from, 'azadijobs');
                $this->email->to($send_to);
                // $this->email->bcc('them@their-example.com');

                $this->email->subject($subject);
                $this->email->message($msg);

                if ($this->email->send()) {
                    $this->session->keep_flashdata('msg', 'password changed.You should receive an email.');
                } else {
                    $this->session->keep_flashdata('msg', 'password changed.');
                }
            }
            redirect('page');
        } else {
            $this->session->set_flashdata('msg', 'You are either not logged in or your sesison has been destroyed. please login again for changing password.');
            $this->session->set_flashdata('link', 'createaccount/change_password');
            redirect('page/login_failed');
        }
    }

    function logout() {
        $this->load->model('UserAccount');
        $this->UserAccount->logout();
        redirect('http://' . $_SERVER['HTTP_HOST'] . '/azadijobs');
    }

}

?>