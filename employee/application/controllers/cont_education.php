<?php
class Cont_Education extends CI_Controller{
    function create_education(){
        $this->load->model('Education');
        $this->Education->save_education($this->session->userdata['user_id']);
        redirect('/page/step_02_view');
    }
    
    function create_professional_qualification(){
        $this->load->model('Professional_Qualification', 'pq');
        $this->pq->save_professional_qualification($this->session->userdata['user_id']);
        redirect('/page/step_02_view');
    }
    
    function edit_education_view(){
        $id = $_GET['id'];
        $this->load->model('Education');
        $data['education'] = $this->Education->get_education($id);
        $data['education_level']['levels'] = $this->Education->get_all_level();
        
        $data['id'] = $id;
        $this->load->view('editresume/edit_education', $data);
    }
    
    public function edit_education(){
        $this->load->model('Education');
        $this->Education->edit_education($this->session->userdata['user_id']);
        redirect('/page/step_02_view');
    }
}
?>
