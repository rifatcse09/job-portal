<?php

class JobCategory extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
    }

    function showAllJobs() {
        
        $data['title'] = empty($_GET['cat_name']) ? 'Azadijobs' : $_GET['cat_name'];
      
        $this->load->model('Category');
        $per_page = 10;
        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        $ses_data = array(
            'cat_id' => $_GET['cat_id'],
            'cat_name' => $_GET['cat_name']
        );
        $this->session->set_userdata($ses_data);

        $data['cat_id'] = $this->session->userdata('cat_id');
        $data['cat_name'] = $this->session->userdata('cat_name');
        $data['jobs'] = $this->Category->jobs_by_category($_GET['cat_id'], $per_page, $num_of_links);

        $config['per_page'] = $per_page;
        $config['total_rows'] = $this->Category->get_total_jobs($_GET['cat_id']);
        $config['base_url'] = site_url() . '/jobcategory/showAllJobs?cat_id=' . $data['cat_id'] . '&cat_name=' . $data['cat_name'];

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $this->load->view('jobsbycatagories', $data);
         
    }

    function showCompanyJobs() {
        $data['title'] = 'Company Jobs';
        $this->load->model('Category');
        $per_page = 20;
        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        $company_id = (isset($_GET['com_id'])) ? $_GET['com_id'] : 0;

        $data['jobs'] = $this->Category->company_job($company_id, $per_page, $num_of_links);

        $data['cat_name'] = 'Company Jobs';

        $config['per_page'] = $per_page;
        $config['total_rows'] = $this->Category->num_rows;
        $config['base_url'] = site_url() . '/jobcategory/showCompanyJobs?q=auto';

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

        $this->load->view('jobsbycatagories', $data);
    }
    /////
    function search_all() {
        
        $data['title'] = empty($_GET['cat_name']) ? 'Azadijobs' : $_GET['cat_name'];
      
        $this->load->model('Category');
        $per_page = 10;
        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        /*$ses_data = array(
            'cat_id' => $_GET['cat_id'],
            'cat_name' => $_GET['cat_name']
        );*/
        //$this->session->set_userdata($ses_data);

       /* $data['cat_id'] = $this->session->userdata('cat_id');
        $data['cat_name'] = $this->session->userdata('cat_name');
        $data['jobs'] = $this->Category->jobs_by_category($_GET['cat_id'], $per_page, $num_of_links);

        $config['per_page'] = $per_page;
        $config['total_rows'] = $this->Category->get_total_jobs($_GET['cat_id']);*/
		$data['jobs'] = $this->Category->jobs_by_category(0, $per_page, $num_of_links);
        $config['base_url'] = site_url() . '/jobcategory/showAllJobs';

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $this->load->view('jobsbycatagories',$data);
         
    }
    
    
    
    ////////
    function showCtgJobs() {
        $data['title'] = 'Jobs In Chittagong';
        $this->load->model('Category');
        $per_page = 20;
        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        $data['jobs'] = $this->Category->job_in_ctg($per_page, $num_of_links);

        $data['cat_name'] = 'Jobs In Chittagong';

        $config['per_page'] = $per_page;
        $config['total_rows'] = $this->Category->num_rows;
        $config['base_url'] = site_url() . '/jobcategory/showCtgJobs?q=auto';

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

        $this->load->view('jobsbycatagories', $data);
    }
    
    ///test for dhaka
    function showDhakaJobs() {
        $data['title'] = 'Jobs In Dhaka';
        $this->load->model('Category');
        $per_page = 20;
        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        $data['jobs'] = $this->Category->job_in_dhaka($per_page, $num_of_links);

        $data['cat_name'] = 'Jobs In Dhaka';

        $config['per_page'] = $per_page;
        $config['total_rows'] = $this->Category->num_rows;
        $config['base_url'] = site_url() . '/jobcategory/showDhakaJobs?q=auto';

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

        $this->load->view('jobsbycatagories', $data);
    }
    ///test for dhaka
    
    ///test for part time

function showPartTimeJobs() {
        $data['title'] = 'Part Time Job' ;
        $this->load->model('Category');
        $per_page = 20;
        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        $data['jobs'] = $this->Category->part_time_job($per_page, $num_of_links);

        $data['cat_name'] = 'Part Time Job' ;

        $config['per_page'] = $per_page;
        $config['total_rows'] = $this->Category->num_rows;
        $config['base_url'] = site_url() . '/jobcategory/showPartTimeJobs?q=auto';

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

        $this->load->view('jobsbycatagories', $data);
    }    

///test for part time
    

    function showkeywordJobs() {
        $data['title'] = 'Search Result';
        $this->load->model('Category');
        $keyword = '';
        if (isset($_POST['keyword'])) {
            $keyword = $_POST['keyword'];
            $this->session->set_flashdata('keyword', $_POST['keyword']);
        } else {
            $keyword = $this->session->flashdata('keyword');
            $this->session->set_flashdata('keyword', $this->session->flashdata('keyword'));
        }

        $per_page = 20;

        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        if ($keyword != '')
            $data['jobs'] = $this->Category->keyword_search($keyword, $per_page, $num_of_links);

        $data['cat_name'] = 'Search Result';

        $config['per_page'] = $per_page;
        $config['total_rows'] = $this->Category->num_rows;
        $config['base_url'] = site_url() . '/jobcategory/showkeywordJobs?q=auto';

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $this->load->view('jobsbycatagories', $data);
    }

//    function showbluejobs() {
//        $data['title'] = 'Jobs In Chittagong';
//        $this->load->model('Category');
//        $per_page = 20;
//        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;
//
//        $data['jobs'] = $this->Category->job_in_ctg($per_page, $num_of_links);
//
//        $data['cat_name'] = 'Jobs In Chittagong';
//
//        $config['per_page'] = $per_page;
//        $config['total_rows'] = $this->Category->num_rows;
//        $config['base_url'] = site_url() . '/jobcategory/showCtgJobs?q=auto';
//
//        $this->pagination->initialize($config);
//        $data["links"] = $this->pagination->create_links();
//
//        $this->load->view('jobsbycatagories', $data);
//    }

    function showbluejobs() {
        $data['title'] = 'Blue Jobs';
        $this->load->model('Category');

        $per_page = 20;

        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        $data['cat_name'] = 'Search Result';
        $data['jobs'] = $this->Category->job_in_blue($per_page, $num_of_links);

        $config['per_page'] = $per_page;
        $config['total_rows'] = $this->Category->num_rows;
        $config['base_url'] = site_url() . '/jobcategory/showbluejobs?q=auto';

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $this->load->view('jobsbycatagories', $data);
    }

    function showCategorySearch() {
        $data['title'] = 'Search Result';
        $this->load->model('Category');
        $Cat = '';
        $qJobLevel = '';
        $qJobNature = '';
        $qJobPosted = '';
        $qJobDeadline='';
		$qJoblocation ='';

        if (isset($_POST['Cat'])) {
            $Cat = $_POST['Cat'];
            $this->session->set_flashdata('Cat', $_POST['Cat']);
        } else {
            $Cat = $this->session->flashdata('Cat');
            $this->session->set_flashdata('Cat', $this->session->flashdata('Cat'));
        }

        if (isset($_POST['qJobNature'])) {
            $qJobNature = $_POST['qJobNature'];
            $this->session->set_flashdata('qJobNature', $_POST['qJobNature']);
        } else {
            $qJobNature = $this->session->flashdata('qJobNature'); //read flash data variable
            $this->session->set_flashdata('qJobNature', $this->session->flashdata('qJobNature'));//add flash data
        }

        if (isset($_POST['qJobLevel'])) {
            $qJobLevel = $_POST['qJobLevel'];
            $this->session->set_flashdata('qJobLevel', $_POST['qJobLevel']);
        } else {
            $qJobLevel = $this->session->flashdata('qJobLevel');
            $this->session->set_flashdata('qJobLevel', $this->session->flashdata('qJobLevel'));
        }
		
		if (isset($_POST['qJoblocation'])) {
            $qJoblocation = $_POST['qJoblocation'];
            $this->session->set_flashdata('qJoblocation', $_POST['qJoblocation']);
        } else {
            $qJoblocation = $this->session->flashdata('qJoblocation');
            $this->session->set_flashdata('qJoblocation', $this->session->flashdata('qJoblocation'));
        }
		
		
		
        if (isset($_POST['qPosted'])) {
            $qJobPosted = $_POST['qPosted'];
            $this->session->set_flashdata('qPosted', $_POST['qPosted']);
        } else {
            if ($this->session->flashdata('qPosted'))//read flash data variable
                $qJobPosted = $this->session->flashdata('qPosted');
            else
                $qJobPosted = 0;
            $this->session->set_flashdata('qPosted', $this->session->flashdata('qPosted'));
        }
        
        if (isset ($_POST['qDeadline'])){
             $qJobDeadline = $_POST['qDeadline'];
            $this->session->set_flashdata('qDeadline', $_POST['qDeadline']);
        } else {
            if ($this->session->flashdata('qDeadline'))
                $qJobDeadline = $this->session->flashdata('qDeadline');
            else
                $qJobDeadline = 0;
            $this->session->set_flashdata('qDeadline', $this->session->flashdata('qDeadline'));
            
            
        }
        $per_page = 5;

        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        $data['jobs'] = $this->Category->category_search($Cat, $qJobLevel,$qJoblocation,$qJobNature, $qJobPosted,$qJobDeadline, $per_page, $num_of_links);
        $data['cat_name'] = 'Search Result';
        $config['per_page'] = $per_page;
        $config['total_rows'] = $this->Category->num_rows;
        $config['base_url'] = base_url() . '/jobcategory/showCategorySearch?q=auto';
	//echo $config['base_url'];
       // $config['base_url'] = site_url() . '/jobcategory/showCategorySearch?q=auto';
	
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
		$data['stored'] = $this->session->all_userdata();
        $this->load->view('jobsbycatagories', $data);
    }

    function showDeadlineJobs() {
        $data['title'] = 'Jobs Tomorrow';
        $this->load->model('Category');
        $data['jobs'] = $this->Category->job_deadline();

        $data['cat_name'] = 'Jobs Tomorrow';
        $this->load->view('jobsbycatagories', $data);
    }

    function JobDetails() {
        
        $this->load->model('Job');
        $this->load->model('Emp_acc_info');
        $this->load->model('Category');
    
        $data['main_job'] = $this->Job->job_details($_GET['ID']);
        $data['business_types'] = $this->Job->job_business_type($_GET['ID']);
        $data['business_areas'] = $this->Job->job_business_area($_GET['ID']);
        $data['cat_name'] = $this->session->userdata('cat_name');
        
        $com_id = $data['main_job']['company_id'];
        $data['com_info'] = $this->Emp_acc_info->get_acc_info($com_id);
        $template=$data['main_job']['template'];
        $this->load->view($template, $data);
        
       
          //else {$this->load->view('jobdetails', $data);}
        
    }
    
    function hire_job() {
        $this->load->model('Job');
       
        $data['hire_job'] = $this->Job->hire_job_details($_GET['ID']);
        
            
        $this->load->view('hire_job_details', $data);
    }
       function resume_details() {
        
       
        $data['id'] = $_GET['ID'];
        
            
        $this->load->view('resume_view', $data);
    }

    function newspapershowjobs() {
        $data['title'] = 'Welcome To Azadijobs Newspaper Jobs';
        $this->load->view('newspaperjobshow', $data);
    }

    function newspapejobsview() {
        $data['title'] = 'Welcome To Azadijobs Newspaper Jobs';
        $this->load->view('newspaper_jobview', $data);
    }

    function examresultview() {
        $data['title'] = 'Welcome To Azadijobs Newspaper Jobs';
        $this->load->view('examresultshow', $data);
    }

    function examresult_show() {
        $data['title'] = 'Welcome To  Newspaper Jobs';
        $this->load->view('examresultshowview', $data);
    }

    function scholarshipall() {
        $data['title'] = 'Welcome To Azadijobs Newspaper Jobs';
        $this->load->view('scholarship', $data);
    }

    function showscholarship() {
        $data['title'] = 'Welcome To Azadijobs Newspaper Jobs';
        $this->load->view('scholarshipview', $data);
    }

    function admissionall() {
        $data['title'] = 'Welcome To Azadijobs Newspaper Jobs';
        $this->load->view('admission', $data);
    }

    function admissionview() {
        $data['title'] = 'Welcome To Azadijobs Newspaper Jobs';
        $this->load->view('admissionshow', $data);
    }

    function foreignall() {
        $data['title'] = 'Welcome To Azadijobs Newspaper Jobs';
        $this->load->view('foreignall', $data);
    }

    function foreignview() {
        $data['title'] = 'Welcome To Azadijobs Newspaper Jobs';
        $this->load->view('foreignview', $data);
    }

    function request() {
        $data['title'] = 'Request For Registration';
        $this->load->view('requestregistration', $data);
    }

    function requestinsert() {

        $this->load->model('Emp_acc_info');
        $this->Emp_acc_info->insert_entrys();
        redirect(site_url() . '/mainpage');
        // echo "Saved";
    }

    function upload_photograph() {
        $config['upload_path'] = './hotjobimages/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '4000';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $config['overwrite'] = True;
        $ext = pathinfo($_FILES['imgSrc']['name'], PATHINFO_EXTENSION);
        $file = $this->session->userdata['user_id'] . '.' . $ext;
        $config['file_name'] = $file;

        $this->load->helper('form');
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('imgSrc')) {
            $data['error'] = array('error' => $this->upload->display_errors());
            $data['title'] = 'Upload';
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        $this->load->library('template');
        $this->load->model('User');
        $this->User->update_photograph($this->session->userdata['user_id'], $file);
        redirect('page/step_05_view');
    }

    function companysearch() {
        $data['title'] = 'Search Result';
        $this->load->model('Category');
        $qjobnature = isset($_GET['qjobnature']) ? $_GET['qjobnature'] : 'A';

        $data['jobs'] = $this->Category->company_search($qjobnature);
        $data['cat_name'] = 'Search Company List';
        $this->load->view('company', $data);
    }

    function company_jobs() {
        $data['title'] = 'Search Result';
        $this->load->model('Category');
        $c_id = $_GET['id'];

        $per_page = 20;

        $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

        $data['jobs'] = $this->Category->company_job($c_id, $per_page, $num_of_links);

        $data['cat_name'] = 'Company Job';
        $data['company_name'] = isset($data['jobs'][0]['company_name']) ? $data['jobs'][0]['company_name'] : '';
        $this->load->view('companyjob', $data);
    }

    function newlypostedjob() {
        $data['title'] = 'Newly Posted Jobs';
        $this->load->model('Category');
        $data['jobs'] = $this->Category->new_job();

        $data['cat_name'] = 'Newly Posted Jobs';
        $this->load->view('jobsbycatagories', $data);
    }

    function email_job_to_friend() {
        if (isset($_GET['job_id']) && $_GET['job_id'] != '') {
            $data['job_id'] = $_GET['job_id'];
        } else {
            $data['no_job'] = 'job not found. please try again.';
        }
        $this->load->view('email_job_to_friend', $data);
    }

    function email_job_post() {
        if ($_POST['friend_name'] == '' || $_POST['friend_email'] == '' || $_POST['your_name'] == '' || $_POST['your_email'] == '') {
            $data['msg'] = 'You have to fill up all data';
            $data['job_id'] = $_POST['job_id'];
            $this->load->view('email_job_to_friend', $data);
        } else {
            $this->load->library('email');

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'azadijobs.com',
                'smtp_port' => '25',
                'smtp_user' => 'info@azadijobs.com',
                'smtp_pass' => '4=mW*SVSK5TT',
                'mailtype' => 'html'
            );

            $this->email->initialize($config);

            $this->email->from($_POST['your_email'], $_POST['your_name']);
            $this->email->to($_POST['friend_email'], $_POST['friend_name']);

            $this->email->subject('Azadijobsjobs.com Email Service');

            $this->load->model('Job');
            $this->load->model('Emp_acc_info');
            $data['main_job'] = $this->Job->job_details($_POST['job_id']);
            $data['business_types'] = $this->Job->job_business_type($_POST['job_id']);
            $data['business_areas'] = $this->Job->job_business_area($_POST['job_id']);
            $data['cat_name'] = $this->session->userdata('cat_name');
            $com_id = $data['main_job']['company_id'];
            $data['com_info'] = $this->Emp_acc_info->get_acc_info($com_id);

            $view = $this->load->view('jobdetails_email', $data, TRUE);
            $this->email->message($view);
            if ($this->email->send()) {
                $this->session->set_flashdata("email_msg", "Job have been Sent to your friend's email");
            }
            else
                $this->session->set_flashdata("email_msg", "Job was not Sent to your friend's email");
            redirect(site_url() . "/mainpage/");
        }
    }
    
   function hot_job_more()
    {
       
       //echo $com_id;
        $this->load->view('more_hot_jobs');
    }

}

?>
