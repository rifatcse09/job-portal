<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<title>Job Category</title>
<head>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>mainpage_css/job.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>mainpage_css/style.css" />
    <style type="text/css">
        #sel_opt
        {
            padding: 5px;
        }

       .styled-button-12 {
	background: #5B74A8;
	background: -moz-linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	background: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#5B74A8), color-stop(100%,#5B74A8));
	background: -webkit-linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	background: -o-linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	background: -ms-linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	background: linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	filter:DXImageTransform.Microsoft.gradient( startColorstr='#5B74A8',endColorstr='#5B74A8',GradientType=0);
	padding:2px 6px;
	color:#fff;
	font-family:'Helvetica',sans-serif;
	font-size:14px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:1px solid #1A356E
        
        }

    </style>
</head>
<body>
<div id="content_container">
    <div id="job_category_header">
        <?//= $this->load->view("jobsbycatagories_header.php"); ?>
    </div>
    <div id="content_all">
            <div id="job_category_navigation">

                    <?//= $this->load->view("jobsbycategory_menu.php"); ?>
                
                </div>
        <div id="content_content">
          
            </div>
  <div style="float:left;width: 80%">      
            <div id="job_category_jobSearch_train">
                <div id="job_category_jobSearch">
                    <form action="<?= site_url() ?>/jobcategory/showCategorySearch" method="post">
                        <table class="form" width="95%" style='margin-top: 10px;'cellspacing="0" cellpadding="3"  align="center">
                            <tbody>
                                <tr >
                                    <td align="right" style="font-family: Calibri;color:#777676;font-size: 18px">
                                        Category:
                                        <br>
                                    </td>
                                    <td>
                                     <?php $cat='';if (isset($_POST['Cat']))$cat=$_POST['Cat'] ?>
                                        <select id="select" style="width:100%;;background: none;border:1px solid #e6e6e6;height: 25px;" name="Cat">
                                            <option id="sel_opt" <?php if ($cat=='') echo 'selected="selected"'?>value="">All Categories</option>
                                            <option id="sel_opt" <?php if ($cat=='1') echo 'selected="selected"'?>value="1"> Accounting/Finance</option>
                                            <option id="sel_opt" <?php if ($cat=='3') echo 'selected="selected"'?>value="3"> Banking/Insurance/Leasing</option>
                                            <option id="sel_opt" <?php if ($cat=='4') echo 'selected="selected"'?>value="4"> Commercial/Supply Chain</option>
                                            <option id="sel_opt" <?php if ($cat=='8') echo 'selected="selected"'?>value="8"> Education/Training</option>
                                            <option id="sel_opt" <?php if ($cat=='9') echo 'selected="selected"'?>value="9"> Engineer/Architect</option>
                                            <option id="sel_opt" <?php if ($cat=='10') echo 'selected="selected"'?>value="10"> Garments/Textile</option>
                                            <option id="sel_opt" <?php if ($cat=='11') echo 'selected="selected"'?>value="11"> General Management/Admin</option>
                                            <option id="sel_opt" <?php if ($cat=='13') echo 'selected="selected"'?>value="13"> IT/Telecommunication</option>
                                            <option id="sel_opt" <?php if ($cat=='14') echo 'selected="selected"'?>value="14"> Marketing/Sales</option>
                                            <option id="sel_opt" <?php if ($cat=='2') echo 'selected="selected"'?>value="2"> Advertisement/Event Mgt.</option>
                                            <option id="sel_opt" <?php if ($cat=='15') echo 'selected="selected"'?>value="15"> Medical/Pharmaceutical</option>
                                            <option id="sel_opt" <?php if ($cat=='16') echo 'selected="selected"'?>value="16"> NGO/Development</option>
                                            <option id="sel_opt" <?php if ($cat=='17') echo 'selected="selected"'?>value="17"> Research/Consultancy</option>
                                            <option id="sel_opt" <?php if ($cat=='18') echo 'selected="selected"'?>value="18"> Secretary/Receptionist</option>
                                            <option id="sel_opt" <?php if ($cat=='6') echo 'selected="selected"'?>value="6"> Data Entry/Operator/BPO</option>
                                            <option id="sel_opt" <?php if ($cat=='5') echo 'selected="selected"'?>value="5"> Customer Support/Call Centre</option>
                                            <option id="sel_opt" <?php if ($cat=='12') echo 'selected="selected"'?>value="12"> HR/Org. Development</option>
                                            <option id="sel_opt" <?php if ($cat=='7') echo 'selected="selected"'?>value="7"> Design/Creative</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="font-family: Calibri;color:#777676;font-size: 18px">Job Label:

                                    </td>
                                    <td>
                                        <?php $qJobLevel='';if(isset ($_POST['qJobLevel']))$qJobLevel=$_POST['qJobLevel']?>
                                        <select id="select3" style="width:100%;;background: none;border:1px solid #e6e6e6;height: 25px;" name="qJobLevel">
                                            <option <?php if($qJobLevel=='') echo 'selected="selected"'; ?>value="">Any</option>
                                            <option <?php if($qJobLevel=='0') echo 'selected="selected"'; ?>value="0">Entry level</option>
                                            <option <?php if($qJobLevel=='1') echo 'selected="selected"'; ?>value="1">Mid level</option>
                                            <option <?php if($qJobLevel=='2') echo 'selected="selected"'; ?>value="2">Top level</option>
                                        </select>
                                    </td>
                                </tr>
								
								<!--Location-->
								
								<tr>
                                    <td align="right" style="font-family: Calibri;color:#777676;font-size: 18px">Job Location:

                                    </td>
                                    <td>
                                        <?php $qJoblocation='';if(isset ($_POST['qJoblocation']))$qJobLevel=$_POST['qJoblocation']?>
                                        <select id="select3" style="width:100%;;background: none;border:1px solid #e6e6e6;height: 25px;" name="qJoblocation">
                                            <option <?php if($qJoblocation=='') echo 'selected="selected"'; ?>value="">Any</option>
                                            <option <?php if($qJoblocation=='1') echo 'selected="selected"'; ?>value="1">B. Baria</option>
                                            <option <?php if($qJoblocation=='2') echo 'selected="selected"'; ?>value="2">Bagerhat</option>
                                            <option <?php if($qJoblocation=='3') echo 'selected="selected"'; ?>value="3">Bandarban</option>
                                            <option <?php if($qJoblocation=='4') echo 'selected="selected"'; ?>value="4">Barisal</option>
                                            <option <?php if($qJoblocation=='5') echo 'selected="selected"'; ?>value="5">Bhola</option>
                                            <option <?php if($qJoblocation=='6') echo 'selected="selected"'; ?>value="6">Bogra</option>
                                            <option <?php if($qJoblocation=='7') echo 'selected="selected"'; ?>value="7">Borguna</option>
                                            <option <?php if($qJoblocation=='8') echo 'selected="selected"'; ?>value="8">Chandpur</option>
                                            <option <?php if($qJoblocation=='9') echo 'selected="selected"'; ?>value="9">Chapainawabganj</option>
                                            <option <?php if($qJoblocation=='10') echo 'selected="selected"'; ?>value="10">Chittagong</option>
                                            <option <?php if($qJoblocation=='11') echo 'selected="selected"'; ?>value="11">Chuadanga</option>
                                            <option <?php if($qJoblocation=='12') echo 'selected="selected"'; ?>value="12">Comilla</option>
                                            <option <?php if($qJoblocation=='13') echo 'selected="selected"'; ?>value="13">Cox's Bazar</option>
                                            <option <?php if($qJoblocation=='14') echo 'selected="selected"'; ?>value="14">Dhaka</option>
                                            <option <?php if($qJoblocation=='15') echo 'selected="selected"'; ?>value="15">Dinajpur</option>
                                            <option <?php if($qJoblocation=='16') echo 'selected="selected"'; ?>value="16">Faridpur</option>
                                            <option <?php if($qJoblocation=='17') echo 'selected="selected"'; ?>value="17">Feni</option>
                                            <option <?php if($qJoblocation=='18') echo 'selected="selected"'; ?>value="18">Gaibandha</option>
                                            <option <?php if($qJoblocation=='19') echo 'selected="selected"'; ?>value="19">Gazipur</option>
                                            <option <?php if($qJoblocation=='20') echo 'selected="selected"'; ?>value="20">Gopalgonj</option>
                                            <option <?php if($qJoblocation=='21') echo 'selected="selected"'; ?>value="21">Hobigonj</option>
                                            <option <?php if($qJoblocation=='22') echo 'selected="selected"'; ?>value="22">Jamalpur</option>
                                            <option <?php if($qJoblocation=='23') echo 'selected="selected"'; ?>value="23">Jessore</option>
                                            <option <?php if($qJoblocation=='24') echo 'selected="selected"'; ?>value="24">Jhalokathi</option>
                                            <option <?php if($qJoblocation=='25') echo 'selected="selected"'; ?>value="25">Jhenaidah</option>
                                            <option <?php if($qJoblocation=='26') echo 'selected="selected"'; ?>value="26">Joypurhat</option>
                                            <option <?php if($qJoblocation=='27') echo 'selected="selected"'; ?>value="27">Khagrachari</option>
                                            <option <?php if($qJoblocation=='28') echo 'selected="selected"'; ?>value="28">Khulna</option>
                                            <option <?php if($qJoblocation=='29') echo 'selected="selected"'; ?>value="29">Kishorgonj</option>
                                            <option <?php if($qJoblocation=='30') echo 'selected="selected"'; ?>value="30">Kurigram</option>
                                            <option <?php if($qJoblocation=='31') echo 'selected="selected"'; ?>value="31">Kushtia</option>
                                            <option <?php if($qJoblocation=='32') echo 'selected="selected"'; ?>value="32">Lalmonirhat</option>
                                            <option <?php if($qJoblocation=='33') echo 'selected="selected"'; ?>value="33">Laxmipur</option>
                                            <option <?php if($qJoblocation=='34') echo 'selected="selected"'; ?>value="34">Madaripur</option>
                                            <option <?php if($qJoblocation=='35') echo 'selected="selected"'; ?>value="35">Magura</option>
                                            <option <?php if($qJoblocation=='36') echo 'selected="selected"'; ?>value="36">Manikgonj</option>
                                            <option <?php if($qJoblocation=='37') echo 'selected="selected"'; ?>value="37">Meherpur</option>
                                            <option <?php if($qJoblocation=='38') echo 'selected="selected"'; ?>value="38">MoulaviBazar</option>
                                            <option <?php if($qJoblocation=='39') echo 'selected="selected"'; ?>value="39">Munshigonj</option>
                                            <option <?php if($qJoblocation=='40') echo 'selected="selected"'; ?>value="40">Mymensingh</option>
                                            <option <?php if($qJoblocation=='41') echo 'selected="selected"'; ?>value="41">Naogaon</option>
                                            <option <?php if($qJoblocation=='42') echo 'selected="selected"'; ?>value="42">Narail</option>
                                            <option <?php if($qJoblocation=='43') echo 'selected="selected"'; ?>value="43">Narayangonj</option>
                                            <option <?php if($qJoblocation=='44') echo 'selected="selected"'; ?>value="44">Narshingdi</option>
                                            <option <?php if($qJoblocation=='45') echo 'selected="selected"'; ?>value="45">Natore</option>
                                            <option <?php if($qJoblocation=='46') echo 'selected="selected"'; ?>value="46">Netrokona</option>
                                            <option <?php if($qJoblocation=='47') echo 'selected="selected"'; ?>value="47">Nilphamari</option>
                                            <option <?php if($qJoblocation=='48') echo 'selected="selected"'; ?>value="48">Noakhali</option>
                                            <option <?php if($qJoblocation=='49') echo 'selected="selected"'; ?>value="49">Pabna</option>
                                            <option <?php if($qJoblocation=='50') echo 'selected="selected"'; ?>value="50">Panchagarh</option>
                                            <option <?php if($qJoblocation=='51') echo 'selected="selected"'; ?>value="51">Patuakhali</option>
                                            <option <?php if($qJoblocation=='52') echo 'selected="selected"'; ?>value="52">Pirojpur</option>
                                            <option <?php if($qJoblocation=='53') echo 'selected="selected"'; ?>value="53">Rajbari</option>
                                            <option <?php if($qJoblocation=='54') echo 'selected="selected"'; ?>value="54">Rajshahi</option>
                                            <option <?php if($qJoblocation=='55') echo 'selected="selected"'; ?>value="55">Rangamati</option>
                                            <option <?php if($qJoblocation=='56') echo 'selected="selected"'; ?>value="56">Rangpur</option>
                                            <option <?php if($qJoblocation=='57') echo 'selected="selected"'; ?>value="57">Satkhira</option>
                                            <option <?php if($qJoblocation=='58') echo 'selected="selected"'; ?>value="58">Shariatpur</option>
                                            <option <?php if($qJoblocation=='59') echo 'selected="selected"'; ?>value="59">Sherpur</option>
                                            <option <?php if($qJoblocation=='60') echo 'selected="selected"'; ?>value="60">Sirajgonj</option>
                                            <option <?php if($qJoblocation=='61') echo 'selected="selected"'; ?>value="61">Sunamgonj</option>
                                            <option <?php if($qJoblocation=='62') echo 'selected="selected"'; ?>value="62">Sylhet</option>
                                            <option <?php if($qJoblocation=='63') echo 'selected="selected"'; ?>value="63">Tangail</option>
                                            <option <?php if($qJoblocation=='64') echo 'selected="selected"'; ?>value="64">Thakurgaon</option>
                                           
                                        </select>
                                    </td>
                                </tr>
								
								
								
								<!--Location-->
								
                                <tr>
                                    <td align="right" style="font-family: Calibri;color:#777676;font-size: 18px">Job Nature :</td>
                                    <td>
                                        <?php $qJobNature='';if(isset ($_POST['$qJobNature']))$qJobNature=$_POST['qJobNature'];?>
                                        <select id="select3" style="width:100%;;background: none;border:1px solid #e6e6e6;height: 25px;" name="qJobNature">
                                            <option <?php if($qJobNature=='') echo 'selected="selected"';?>value="">Any</option>
                                            <option <?php if($qJobNature=='Full time') echo 'selected="selected"';?>value="Full time">Full-Time</option>
                                            <option <?php if($qJobNature=='Part time') echo 'selected="selected"';?>value="Part time">Part-Time</option>
                                            <option value="Contract">Contractual</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="" align="right" style="font-family: Calibri;color:#777676;font-size: 18px">
                                        Posted within :
                                        <br>
                                    </td>
                                    <td>
                                        <span>
					<?php $qPosted='';if(isset($_POST['qPosted']))$qPosted=$_POST['qPosted'];?>
                                            <select id="select4" style="width:100%;;background: none;border:1px solid #e6e6e6;height: 25px;" name="qPosted">
                                                <option <?php if($qPosted=='')echo 'selected="selected"';?> value="">Any</option>
                                                <option <?php if($qPosted=='0')echo 'selected="selected"';?>value="0">Today</option>
                                                <option <?php if($qPosted=='1')echo 'selected="selected"';?> value="1">Yesterday</option>
                                                <option <?php if($qPosted=='2')echo 'selected="selected"';?> value="2">Last 2 days</option>
                                                <option <?php if($qPosted=='3')echo 'selected="selected"';?> value="3">Last 3 days</option>
                                                <option <?php if($qPosted=='4')echo 'selected="selected"';?> value="4">Last 4 days</option>
                                                <option <?php if($qPosted=='5')echo 'selected="selected"';?> value="5">Last 5 days</option>
                                                <option <?php if($qPosted=='6')echo 'selected="selected"';?> value="6">Last 6 days</option>
                                                <option <?php if($qPosted=='7')echo 'selected="selected"';?> value="7">Last 7 days</option>
                                            </select>
                                        </span>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="right" style="font-family: Calibri;color:#777676;font-size: 18px">Deadline :</td>
                                    <td>
				       <?php $qDeadline='';if(isset($_POST['qDeadline']))$qDeadline=$_POST['qDeadline'];?>
                                        <select id="select4" style="width:100%;;background: none;border:1px solid #e6e6e6;height: 25px;" name="qDeadline">
                                            <option <?php if($qDeadline=='')echo 'selected="selected"';?> selected="" value="">Any </option>
                                            <option <?php if($qDeadline=='0')echo 'selected="selected"';?> value="0">Today</option>
                                            <option <?php if($qDeadline=='1')echo 'selected="selected"';?> value="1">Tomorrow</option>
                                            <option <?php if($qDeadline=='2')echo 'selected="selected"';?> value="2">After 1 day</option>
                                            <option <?php if($qDeadline=='3')echo 'selected="selected"';?> value="3">After 2 days</option>
                                            <option <?php if($qDeadline=='4')echo 'selected="selected"';?> value="4">After 3 days</option>
                                            <option <?php if($qDeadline=='5')echo 'selected="selected"';?> value="5">After 4 days</option>
                                            <option <?php if($qDeadline=='6')echo 'selected="selected"';?> value="6">After 5 days</option>
                                            <option <?php if($qDeadline=='7')echo 'selected="selected"';?> value="7">After 6 days</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="float:right;width: 100%;text-align: right">
                                        <input type="submit" class="styled-button-12" value="Search" style="height:40px;" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>  

              
                <div id="job_category_train" >
<!--                    <div id="job_category_train_content">-->
                   <marquee scrolldelay="200" onmouseout="this.setAttribute('scrollamount', 6, 0);" onmouseover="this.setAttribute('scrollamount', 0, 0);" direction="up" behavior="scroll" style="height: 140px;margin-top: 20px;margin-bottom: 50px" scrollamount="6">
                      <table style="height: 20px">
                    <?
                   
                    //include("database.php");
                    $sql = "select * from training order by training_id desc";
                    $data = mysql_query($sql);
                    while ($r = mysql_fetch_array($data)) {
                        ?>
                        <tr>
                             <td width="250px" height="10px" style="padding-left: 50px">
                                 <p style="width:170px; color: #a7090a; font-size: 8pt; font-family:Tahoma; margin-top: 10px;font-weight: bold"><? echo $r['title']; ?></p>
                                 <p style="width:200px; color: black; font-size:8pt; font-family: Tahoma; margin-top:10px;font-weight: bold">Date:<? echo $r['training_date']; ?></p>
                                 <p style="width:220px; color: #000000; font-size:5pt; font-family: Tahoma; margin-top: 10px;padding-bottom: 5px;font-weight: bold"><? echo substr($r['resource_person'],0,50) ?><a style="text-decoration:none; margin-top: 3px; margin-left: 10px; font-weight:bold; color:#1a4f91" href="<?= site_url() ?>/training/training_details?training_id=<? echo $r['training_id']; ?>" target="_blank">Click Details</a></p>
                             </td>
                        </tr>
                        <?
                    }
                    ?>
                        </table>
                     </marquee>
                        <div></div>
<!--                    </div>-->
                </div>
            </div>

            <div id="job_type_container">
                <div id="job_type_content">
                    <table width="100%">
<!--                        <tr> 
                            <td width="200" bgcolor="#000" height="29" align="center" class="BDJLebels" id="tdcornertl">
                                <strong>Company</strong></td>
                            <td width="156" bgcolor="#000" align="center" class="BDJLebels" id="tdcornertm1">
                                <strong>Job Title</strong></td>
                            <td width="219" bgcolor="#000" align="center" class="BDJLebels" id="tdcornertm2">
                                <strong>Education</strong></td>
                            <td width="102" bgcolor="#000" nowrap="" align="center" class="BDJLebels" id="tdcornertr">
                                <strong>Last Date</strong></td>
                        </tr>-->

                        <?
                        if ($jobs) {
                            foreach ($jobs as $job) {
                                ?>
                        
                        <div id="job_div" style="padding-bottom: 10px" >
                           <div style='margin-left: 5px;margin-top: 10px;font-weight: bold;color: #68aa47'><?= $job['company_name'] ?></div>
                            <div style='margin-left: 5px;margin-top: 10px;font-weight: bold;outline: medium none;color:#68aa47'><a target="_blank" style="text-decoration: none" href="<?= site_url() ?>/jobcategory/JobDetails?ID=<?= $job['id'] ?>"><?= $job['job_title'] ?></a></div>

                            <div style='margin-left: 5px;margin-top: 10px;font-family: Tahoma'><span style='font-weight: bold'>Education :</span><span style='font-size: 14px'> <?
                                        $edu_array = explode("\n", $job['edu_qualification']);
                                        foreach ($edu_array as $edu) {
                                            echo $edu . '<br />';
                                        }
                                        ?></span></div>
							<div style='margin-left: 5px;margin-top: 10px;outline: medium none;color:#000;margin-bottom: 5px'><span style="font-weight: bold">Job Location:</span>
							<?
							if($job['job_location']=='-1')echo 'Anywhere in Bangladesh';
							else if($job['job_location']=='1')echo 'B. Baria';
							else if($job['job_location']=='2')echo 'Bagerhat';
							else if($job['job_location']=='3')echo 'Bandarban';
							else if($job['job_location']=='4')echo 'Barisal';
							else if($job['job_location']=='5')echo 'Bhola';
							else if($job['job_location']=='6')echo 'Bogra';
							else if($job['job_location']=='7')echo 'Borguna';
							else if($job['job_location']=='8')echo 'Chandpur';
							else if($job['job_location']=='9')echo 'Chapainawabganj';
							else if($job['job_location']=='10')echo 'Chittagong';
							else if($job['job_location']=='11')echo 'Chuadanga';
							else if($job['job_location']=='12')echo 'Comilla';
							else if($job['job_location']=='13')echo 'Coxs Bazar';
							else if($job['job_location']=='14')echo 'Dhaka';
							else if($job['job_location']=='15')echo 'Dinajpur';
							else if($job['job_location']=='16')echo 'Faridpur';
							else if($job['job_location']=='17')echo 'Feni';
							else if($job['job_location']=='18')echo 'Gaibandha';
							else if($job['job_location']=='19')echo 'Gazipur';
							else if($job['job_location']=='20')echo 'Gopalgonj';
							else if($job['job_location']=='21')echo 'Hobigonj';
							else if($job['job_location']=='22')echo 'Jamalpur';
							else if($job['job_location']=='23')echo 'Jessore';
							else if($job['job_location']=='24')echo 'Jhalokathi';
							else if($job['job_location']=='25')echo 'Jhenaidah';
							else if($job['job_location']=='26')echo 'Joypurhat';
							else if($job['job_location']=='27')echo 'Khagrachari';
							else if($job['job_location']=='28')echo 'Khulna';
							else if($job['job_location']=='29')echo 'Kishorgonj';
							else if($job['job_location']=='30')echo 'Kurigram';
							else if($job['job_location']=='31')echo 'Kushtia';
							else if($job['job_location']=='32')echo 'Lalmonirhat';
							else if($job['job_location']=='33')echo 'Laxmipur';
							else if($job['job_location']=='34')echo 'Madaripur';
							else if($job['job_location']=='35')echo 'Magura';
							else if($job['job_location']=='36')echo 'Manikgonj';
							else if($job['job_location']=='37')echo 'Meherpur';
							else if($job['job_location']=='38')echo 'MoulaviBazar';
							else if($job['job_location']=='39')echo 'Munshigonj';
							else if($job['job_location']=='40')echo 'Mymensingh';
							else if($job['job_location']=='41')echo 'Naogaon';
							else if($job['job_location']=='42')echo 'Narail';
							else if($job['job_location']=='43')echo 'Narayangonj';
							else if($job['job_location']=='44')echo 'Narshingdi';
							else if($job['job_location']=='45')echo 'Natore';
							else if($job['job_location']=='46')echo 'Netrokona';
							else if($job['job_location']=='47')echo 'Nilphamari';
							else if($job['job_location']=='48')echo 'Noakhali';
							else if($job['job_location']=='49')echo 'Pabna';
							else if($job['job_location']=='50')echo 'Panchagarh';
							else if($job['job_location']=='51')echo 'Patuakhali';
							else if($job['job_location']=='52')echo 'Pirojpur';
							else if($job['job_location']=='53')echo 'Rajbari';
							else if($job['job_location']=='54')echo 'Rajshahi';
							else if($job['job_location']=='55')echo 'Rangamati';
							else if($job['job_location']=='56')echo 'Rangpur';
							else if($job['job_location']=='57')echo 'Satkhira';
							else if($job['job_location']=='58')echo 'Shariatpur';
							else if($job['job_location']=='59')echo 'Sherpur';
							else if($job['job_location']=='60')echo 'Sirajgonj';
							else if($job['job_location']=='61')echo 'Sunamgonj';
							else if($job['job_location']=='62')echo 'Sylhet';
							else if($job['job_location']=='63')echo 'Tangail';
							else if($job['job_location']=='64')echo 'Thakurgaon';
							?>
							</div>
							
							
                            <div style='margin-left: 5px;margin-top: 10px;outline: medium none;color:#000;margin-bottom: 5px'><span style="font-weight: bold">Dead Line : </span><?= date("d F,Y", strtotime($job['deadline'])) ?></div>
                        <div style='margin-left:-20px;margin-top:-70px;margin-right:20px;font-weight:none;outline: medium none;float:right;width:100px;'>
						<a target="_blank" style="width:100px;border:1px solid #68aa47;border-radius:5px;background:#68aa47;color:#fff; text-decoration: none" href="<?= site_url() ?>/jobcategory/JobDetails?ID=<?= $job['id'] ?>">
						<b>Apply</b>
						</a></div>
                        </div>
                        
                         
<!--                                <tr>
                                    <td bgcolor="#f2f2f2" height="29" class="body"><?= $job['company_name'] ?></td>
                                    <td bgcolor="#f2f2f2"><a target="_blank" style="text-decoration: none" href="<?= site_url() ?>/jobcategory/JobDetails?ID=<?= $job['id'] ?>"><?= $job['job_title'] ?></a></td>
                                    <td bgcolor="#f2f2f2" class="body">
                                        <?
                                        $edu_array = explode("\n", $job['edu_qualification']);
                                        foreach ($edu_array as $edu) {
                                            echo $edu . '<br />';
                                        }
                                        ?>
                                    </td>
                                    <td bgcolor="#f2f2f2" align="center" class="body"><?= date("d F,Y", strtotime($job['deadline'])) ?> </td>
                                </tr>-->
                                <?
                            }
                        } else {
                            echo '<tr>
                                    <td>No Data Available</td>
                                </tr>';
                        }
                        ?>
                    </table>
                </div>
                <div>
                    <?= isset($links) ? $links : '' ?>
                </div>
            </div>
<!--            <div id="page_index">

            </div>-->

</div>
 <div id="content_add">
            <div>
            <?
            //include("database.php");
            $sql = "select * from  advertisement where advertisement_type='jobcategory2' ORDER BY add_id DESC LIMIT 0,3";
            $data = mysql_query($sql);
            while ($r = mysql_fetch_array($data)) {
                ?>
                <tr>
                    <td valign="top"><a href="<? echo $r['website']; ?>" target="_blank"><img src="<?= base_url() ?>adminpanel/advertise_images/<? echo $r['name']; ?>" width="130" height="120" style="margin:2px 0px 0px 0px;"/></a></td>
                </tr>
                <?
            }
            ?>
                </div>
        </div>
        </div>
       
    
    </div>
    
     
    
</div>
<div id="footer_bottom_navigation">
        <?// $this->load->view("footer_bottom_navigation.php"); ?>
</div>

 <div id="wrapper_bottom">
             <div style="float:right;margin-right: 20px"><p title="Design & Developed by PentaGems" style="color:#fff">Design & Developed by <a title="PentaGems"  style="text-decoration: none;color:#E6E600;font-weight: bold"target="_blank" href="http://pentagemsbd.com">PentaGems</a></p></div>
                <? //$this->load->view("footer_bottom_navigation.php"); ?>
 </div>
</body>
</html>