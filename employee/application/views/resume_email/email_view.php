<?= $this->load->view("editresume/content_top_navigation"); ?>
<script type="text/javascript">
    function OpenResumeWindow(){
         window.open("cont_email/resume_only", "mywin", "menubar=0, toolbar=1, scrollbars=1, status=1, width=800, resizable=1, top=50, left=100");
    }
    
    function verifyemail(){
        var com_email = document.getElementById('comopany_email_address');
        var subject = document.getElementById('email_subject');
        
        if(com_email.value == "" || subject.value == ""){
            alert('You must fill up company email and subject.');
            return false;
        }
    }
</script>

<div id="main-content">
    <? $this->load->helper('form')?>
    <?= form_open('cont_email/post_email', 'method="Post" onsubmit="return verifyemail();"')?>
    <h3 style="margin-left: 250px;">Email Resume to a company.</h3>
    <?
        $email_name = '';
        if(isset($personal)){
            $email = $personal['email'];
            $name = $personal['name'];
//            $email_name = $name . '<' . $email . '>';
            $email_name=$email;
        }
    ?>
    <input type="hidden" name="email_id" value="<?= $email?>"/>
    <input type="hidden" name="name" value="<?= $name?>"/>
    <table class="form-table" style="width: 147%">
        <tr>
            <td class="right">My Email: </td>
            <td class="left"><input type="text" readonly="true" name="email_address" id="email_address" value="<?= $email_name?>"/></td>
        </tr>
        <tr>
            <td class="right">Company E-mail: </td>
            <td class="left">
                <? if(!isset ($company)) {?>
                    <input type="text" name="company_email_address" id="comopany_email_address" />
                <? } 
                else { 
                    $email = $company[0]['contact_email'];    
                    if($company[0]['email_id'] != 'n/a')
                        $email = $company[0]['email_id'];
                    ?>
                    <input type="text" name="company_email_address" id="comopany_email_address" readonly value="<?= $email?>" />
                    <input type="hidden" name="job_id" value="<?= $_GET['job_id']?>"/>
                <? } ?>    
                <span>*</span>
            </td>
        </tr>
        <tr>
            <td class="right">Subject: </td>
            <td class="left">
                <input type="text" name="email_subject" id="email_subject" />
                <span>*</span>
            </td>
        </tr>
        
        
        <tr>
            <td class="right">Expected Salary: </td>
            <td class="left">
                <input type="text" name="salary" id="salary" />
                <span>*</span>
            </td>
        </tr>
        
        
        <tr>
            <td class="right"></td>
            <td class="left"><a href="#" onclick="OpenResumeWindow();">Preview Resume</a></td>
        </tr>
        <tr>
            <td class="right">Message</td>
            <td class="left">
                <textarea name="email_message" id="email_message" style="max-height: 150px; min-height: 150px; max-width: 250px; min-width: 250px"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" name="email_submit" value="Submit Resume"/>
            </td>
        </tr>
    </table>
    <?= form_close();?>
</div>