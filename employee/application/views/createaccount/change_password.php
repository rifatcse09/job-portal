<script type="text/javascript">
    function verify_input(){
        var form1 = document.getElementById('form1');
        if(document.getElementById('account_username'))
        {
            var USER=document.getElementById('account_username').value;
            var PASS1=document.getElementById('account_password').value;
            var PASS2=document.getElementById('account_retype_password').value;
            //--------------------------------------------User Name Validation--------------------------------------------------------------------------------
            if (USER=="")
            {
                alert('Please enter user name');
                form1.account_username.focus();
                return false;
            }
            if (USER.length < 5 )
            {
                alert('User name must be at least 5 characters!');
                form1.account_username.focus();
                return false;
            }
            //dv.indexOf(';', 0) >= 0
            if (USER.indexOf('/', 0) >= 0 )
            {
                alert('User name does not allow /');
                form1.account_username.focus();
                return false;
            }
            // If does not work properly
            if (USER.indexOf("\\", 0) >= 0 )
            {
                alert("User name does not allow '\\'");
                form1.account_username.focus();
                return false;
            }
            if (USER.indexOf(';', 0) >= 0 )
            {
                alert('User name does not allow ;');
                form1.account_username.focus();
                return false;
            }
            if (USER.indexOf(':', 0) >= 0 )
            {
                alert('User name does not allow :');
                form1.account_username.focus();
                return false;
            }
            if (USER.indexOf('&', 0) >= 0 )
            {
                alert('User name does not allow & ');
                form1.account_username.focus();
                return false;
            }
            if (USER.indexOf('"', 0) >= 0 )
            {
                alert('User name does not allow "');
                form1.account_username.focus();
                return false;
            }
            if (USER.indexOf("'", 0) >= 0 )
            {
                alert("User name does not allow '");
                form1.account_username.focus();
                return false;
            }
            if (USER.indexOf(' ', 0) >= 0 )
            {
                alert('User Name should be only one word! No space allowed.');
                form1.account_username.focus();
                return false;
            }
            //-------------------------------------------Password Validation-------------------------------------------------------------------------------------------
            if(PASS1=="")
            {
                alert('Please enter your password');
                form1.account_password.focus();
                return false;
            }
            if (PASS1.indexOf(' ', 0) >= 0 )
            {
                alert('Password should be only one word!');
                form1.account_password.focus();
                return false;
            }
            if (PASS1.indexOf("'", 0) >= 0 )
            {
                alert("Password field does not allow ' ");
                form1.account_password.focus();
                return false;
            }
            if (PASS1.indexOf('"', 0) >= 0 )
            {
                alert('Password field does not allow "');
                form1.account_password.focus();
                return false;
            }
            if (PASS1.indexOf(';', 0) >= 0 )
            {
                alert('Password field does not allow ;');
                form1.account_password.focus();
                return false;
            }
            if (PASS1.indexOf('&', 0) >= 0 )
            {
                alert('Password field does not allow &');
                form1.account_password.focus();
                return false;
            }
            if (PASS1.length<8)
            {
                alert(' Password must be 8 to 12 characters long .');
                form1.account_password.focus();
                return false;
            }
            if(PASS2=="")
            {
                alert('Please re-enter your password.');
                form1.account_retype_password.focus();
                return false;
            }
            if(PASS1!=PASS2)
            {
                alert('Please be sure of your password \n Enter same password in the fields');
                form1.account_retype_password.focus();
                return false;
            }
            //-------------------------------End Password Validation----------------------------------------------------------------------------------
        }
    }
</script>

<div id="main-content">
    <? $this->load->helper('form') ?>
    <?= form_open('createaccount/change_password_post', 'method="Post" name="form1" id="form1" onsubmit="return verify_input();"') ?>
    <h3 style="margin-left: 250px;">Change Password</h3>

    <table class="form-table" style="width: 147%">
        <tr>
            <td class="right">User Name: </td>
            <td class="left"><input type="text" name="account_username" id="account_username" value="<?= $user_name?>" />
                <span>*</span>
            </td>
        </tr>
        <tr>
            <td class="right">Password: </td>
            <td class="left">
                <input type="password" name="account_password" id="account_password" />
                <span>*</span>
            </td>
        </tr>
        <tr>
            <td class="right">Confirm Password: </td>
            <td class="left">
                <input type="password" name="account_retype_password" id="account_retype_password" />
                <span>*</span>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="change_submit" value="Change"/></td>
        </tr>
    </table>
    <?= form_close(); ?>
</div>
