<? 
    $size = isset($district_size) ? $district_size : 1; 
    $name = isset($district_name) ? $district_name : 'district_name';
    $id = isset($district_id) ? $district_id : 'district_id';
?>
<select id="<?=$id?>" style="width:280px;margin-top: 15px;margin-bottom: 15px;border:1px solid #e6e6e6" size="<?=$size;?>" name="<?=$name?>" <?= isset($additional_att)? $additional_att : '' ?>>
<?php
    if($districts){
        
        foreach ($districts as $district){
            echo '<option style="height:25px;font-size:16px;padding-left:10px" value="' . $district['District_Id'] . '">' . $district['District_Name'] . '</option>';
        }
    }
?>
</select>