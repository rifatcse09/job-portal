<style>
    #head{
        line-height: .5px;
    }
</style>
<h3 style="margin-left: 250px;">View Resume</h3>
 <div><table style="border-collapse: none;border: none">
         
         <tr>
             <td style="border:none"><span style="">Download Resume</span></td>
             <td style="border:none"><a style="color:black"  href="<?=site_url()?>/page/cv/<?=$personal['id']?>"><img src="<?=base_url()?>images/Word.gif"></a></td>
         </tr>
     </table>
</div>
<div id="main-content" style="float:left;">  
    
    <div style="float: left;width: 147%">
        <div style="float:left;width: 75%">
            <table class="no-border" style="border:none;float: left;width: 100%">
                <tr>
                    <td style="border: none;text-align: left;font-size: 22px;font-weight: bold" ><?= $personal['name'] ?></td>
                  
                </tr>
                 <tr>
                    <td style="border: none;text-align: left;font-weight: bold"> 
                       Address : <?= $personal['present_add'] ?></td>
                   
                </tr>
                 <tr>
                  <td style="border: none;text-align: left;font-weight: bold">
                          Mobile : <?= $personal['mobile'] ?></td>
                    
                </tr>
                <tr>
                    <td style="border: none;text-align: left;font-weight: bold">Email : <?= $personal['email'] ?></td>
                </tr>
                
            </table>
           
            

             
        </div>
        <div style="float:left;"> 
            <table  style="border: none;float: left">
                <tr>
                    <td style="border: none"><img src="<?= base_url() . 'photographs/' . $personal['image'] ?>" alt="no photo" width="150" height="150" /></td>
                </tr>
            </table>
        </div>
    </div>
<table class="no-border" style="width: 147%">
            <thead>
                <tr>
                    <th style="margin:2px; text-align: center;background-color: #003D5C;padding: 3px">Career Objective</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="text-align: justify"><?= $career['objective'] ?></td>
                </tr>
           
            </tbody>
</table>
       <?
    if ($summary['career_summary']!=='') {
        ?>
    <table class="no-border" style="width: 147%">
            <thead>
                <tr>
                    <th style="margin:2px; text-align: center;background-color: #003D5C;padding: 3px">Career Summary</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="text-align: justify"><?= $summary['career_summary'] ?></td>
                </tr>
           
            </tbody>
        </table>
    <?
    }
   if($summary['special_qualification']!=='')
    {
        ?>
      <table class="no-border" style="width: 147%">
            <thead>
                <tr>
                    <th style="margin:2px; text-align: center;background-color: #003D5C;padding: 3px">Special Qualification</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="text-align: justify"><?= $summary['special_qualification'] ?></td>
                </tr>
           
            </tbody>
        </table>
    
    <?
    }
    
 
        if (count($emp_history) > 0) {
        ?>
        <table class="no-border" style="width: 147%">
            <thead>
                <tr>
                    <th style="margin:2px; text-align: center;background-color: #003D5C;padding: 3px">Employment History</th>
            </tr>
            </thead>
            <tbody>
                <?
                foreach ($emp_history as $key => $history) {
                    ?>

                    <tr>
                        <td align="left">
                            <span style="font-weight: bold; text-decoration: underline">
                                <?= ($key + 1) . '.' . $history['position_held']; ?>
                                <?= '(' . date('F d, Y', strtotime($history['from'])) . ' - '; ?>
                                <?= ($history['continuing'] == 'y') ? 'continuing)' : date('F d, Y', strtotime($history['to'])) . ')'; ?>
                            </span><br/><br/>
                            <span style="font-weight: bold;">
                                <?= $history['company_name'] ?>
                            </span><br/>
                            <span>
                                Company Location: <?= $history['company_location'] ?><br/>
                                Department: <?= $history['department'] ?>
                            </span><br/>
                            <span>
                                <? if($history['responsibilities']!=='')
                                {
                                    ?>
                                <i><b style="text-decoration: underline">Duties/Responsibilities</b></i><br/>
                                <?
//                                        $duties = explode("\n", $history['responsibilities']);
//                                        foreach ($duties as $duty) {
//                                            echo $duty . '<br/>';
//                                        }
                                echo $history['responsibilities'];
                                }
                                ?>
                                
                            </span>
                        </td>
                    </tr>
                <? } ?>

            </tbody>
        </table>
    <? } ?>
    <br />

    <?
    if (count($acc_qualification) > 0) {
        ?>
        <table class="no-border" style="width: 147%">
            <thead>
                <tr>
                    <th style="margin:2px; text-align: center;background-color: #003D5C;padding: 3px">Academic Qualification</th>
            </tr>
            </thead>
            <tbody>

                <tr>
                    <td>
                        <table class="form-table">
                               <thead>
                                <tr>
                                    <th style="text-align: center;background: #ffffff">Exam Title</th>
                                    <th style="text-align: center;background: #ffffff">Concentration/Major</th>
                                    <th style="padding-left: 80px;padding-right: 80px;text-align: center;background: #ffffff">Institute</th>
                                    <th style="text-align: center;background: #ffffff;padding-left: 10px;padding-right: 10px;">Result</th>
                                    <th style="padding-left: 10px;padding-right: 10px;text-align: center;background: #ffffff">Passing Year</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?
                                foreach ($acc_qualification as $acc) {
                                    ?>
                                    <tr>
                                        <td><?= $acc['degree_title'] ?></td>
                                        <td><?= $acc['major'] ?></td>
                                        <td><?= $acc['institute'] ?></td>
                                        <td><?= $acc['result'] ?></td>
                                        <td><?= $acc['passing_year'] ?></td>
                                    </tr>
                                    <?
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                </tr>       

            </tbody>
        </table>
        <?
    }
    ?>
    <br />
    <?
    if (count($career) > 0) {
        ?>
        <table class="no-border" style="width: 147%">
            <thead>
                <tr>
                    <th colspan="2" style="margin:2px; text-align: center;background-color: #003D5C;padding: 3px">Career and Application</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="left">Looking For:</td>
                    <td class="left"><?= $career['looking_for'] ?> Level Job</td>
                </tr>
                <tr>
                    <td class="left">Available For:</td>
                    <td class="left"><?= $career['available_for'] ?></td>
                </tr>
                <tr>
                    <td class="left">Present Salary:</td>
                    <td class="left"><?= $career['present_salary'] ?></td>
                </tr>
                <tr>
                    <td class="left">Expected Salary:</td>
                    <td class="left"><?= $career['expected_salary'] ?></td>
                </tr>
                <? if (isset($categories)) {
                    if (count($categories) > 0) { ?>
                        <tr>
                            <td class="left">Preferred Job Category:</td>
                            <td class="left">
                                <?
                                foreach ($categories as $key => $cat) {
                                    if ($key == 0) {
                                        echo $cat['category_name'];
                                    } else {
                                        echo ', ' . $cat['category_name'];
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                    <? }
                } ?>
                <? if (count($pre_organization) > 0) { ?>
                    <tr>
                        <td class="left">Preferred Organization:</td>
                        <td class="left">
                            <?
                            foreach ($pre_organization as $key => $org) {
                                if ($key == 0) {
                                    echo $org['business_name'];
                                } else {
                                    echo ', ' . $org['business_name'];
                                }
                            }
                            ?>
                        </td>
                    </tr>
                <? } ?>
                <tr>
                    <td class="left">Preferred Area:</td>
                    <td class="left">
                        <? if (count($pre_location) > 0) { ?>

                            <?
                            foreach ($pre_location as $key => $loc) {
                                if ($key == 0) {
                                    echo $loc['District_Name'];
                                } else {
                                    echo ', ' . $loc['District_Name'];
                                }
                            }
                            ?>

                            <?
                        } else {
                            echo 'Anywhere in Bangladesh.';
                        }
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    <? } ?>
    <br />

    <?
    if (count($language) > 0) {
        ?>

        <table class="no-border" style="width: 147%">
            <thead>
                <tr>
                    <th colspan="4" style="margin:2px; text-align: center;background-color: #003D5C;padding: 3px">Language Proficiency</th>
                </tr>
                <tr>
                    <th style="width: 50px;background: #ffffff;border: none;text-align: center">Language</th>
                    <th style="background: #ffffff;border: none;text-align: center">Reading</th>
                    <th style="background: #ffffff;border: none;text-align: center">Writing</th>
                    <th style="background: #ffffff;border: none;text-align: center">Speaking</th>
                </tr>
            </thead>
            <tbody>
                <?
                foreach ($language as $lan) {
                    ?>
                    <tr>
                        <td><?= $lan['language_name'] ?></td>
                        <td><?= $lan['reading'] ?></td>
                        <td><?= $lan['writing'] ?></td>
                        <td><?= $lan['speaking'] ?></td>
                    </tr>
                <? } ?>
            </tbody>
        </table>
    <? } 
    
    
    
    
     if (isset($personal)) {
        ?>
        <table class="no-border" style="width: 147%">
            <thead>
                <tr>
                    <th colspan="3" style="margin:2px; text-align: center;background-color: #003D5C;padding: 3px">Personal Detail</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="left">Name:</td>
                    <td class="left"><?= $personal['name'] ?></td>

                </tr>
                <tr>
                    <td class="left">Father's Name:</td>
                    <td class="left"><?= $personal['father_name'] ?></td>
                </tr>
                <tr>
                    <td class="left">Mother's Name:</td>
                    <td class="left"><?= $personal['mother_name'] ?></td>
                </tr>
                <tr>
                    <td class="left">Date of Birth:</td>
                    <td class="left"><?= date('d F, Y', strtotime($personal['date_birth'])) ?></td>
                </tr>
                <tr>
                    <td class="left">Gender:</td>
                    <td class="left"><? echo $personal['gender'] == 'm' ? 'Male' : 'Female'; ?></td>
                </tr>
                <tr>
                    <td class="left">Marital status:</td>
                    <td class="left" colspan="2"><? echo $personal['marital_status'] == 'm' ? 'Married' : 'Single'; ?></td>
                </tr>
                <tr>
                    <td class="left">Nationality:</td>
                    <td class="left" colspan="2"><?= $personal['nationality'] ?></td>
                </tr>
                <tr>
                    <td class="left">Religion:</td>
                    <td class="left" colspan="2"><?= $personal['religion'] ?></td>
                </tr>
                <tr>
                    <td class="left">Present Address:</td>
                    <td class="left" colspan="2"><?= $personal['present_add'] ?></td>
                </tr>
                <tr>
                    <td class="left">Permanent Address:</td>
                    <td class="left" colspan="2"><?= $personal['permanent_add'] ?></td>
                </tr>
                <tr>
                    <td class="left">Current Location:</td>
                    <td class="left" colspan="2"><?= $personal['District_Name'] ?></td>
                </tr>
                <tr>
                    <td class="left">Contact(home):</td>
                    <td class="left" colspan="2"><?= $personal['home_phone'] ?></td>
                </tr>
                <tr>
                    <td class="left">Contact(Mobile):</td>
                    <td class="left" colspan="2"><?= $personal['mobile'] ?></td>
                </tr>
                <tr>
                    <td class="left">Contact(Office):</td>
                    <td class="left" colspan="2"><?= $personal['office_phone'] ?></td>
                </tr>
                <tr>
                    <td class="left">Email:</td>
                    <td class="left" colspan="2"><?= $personal['email'] ?></td>
                </tr>
                <tr>
                    <td class="left">Alternate Email:</td>
                    <td class="left" colspan="2"><?= $personal['alternate_email'] ?></td>
                </tr>
            </tbody>
        </table>
    <? } 
     if (count($reference) > 0) { 
        $flag = isset ($refernce[1]);
        ?>
        <table class="no-border" style="width: 147%">
            <thead>
                <tr>
                    <th colspan="3" style="margin:2px; text-align: center;background-color: #003D5C;padding: 3px">Reference(s)</th>
                </tr>

                <tr>
                    <th></th>
                    <th><strong>Reference: 1</strong></th>
                    <th><strong><? if (isset($reference[1])) { ?>Reference : 2<? } ?></strong></th>
                </tr>
            </thead>
            <tbody>		
                <tr> 
                    <td class="left">Name :</td>
                    <td class="left"><?= $reference[0]['ref_name'] ?></td>
                    <td class="left"><?= ($flag) ? $reference[1]['ref_name'] : '' ?></td>
                </tr>
                <tr> 
                    <td class="left">organization :</td>
                    <td class="left"><?= $reference[0]['organization'] ?></td>
                    <td class="left"><?= ($flag) ? $reference[1]['organization'] : '' ?></td>
                </tr>
                <tr> 
                    <td class="left">Designation :</td>
                    <td class="left"><?= $reference[0]['designation'] ?></td>
                    <td class="left"><?= ($flag) ? $reference[1]['designation'] : '' ?></td>
                </tr>
                <tr> 
                    <td class="left">Address :</td>
                    <td class="left"><?= $reference[0]['address'] ?></td>
                    <td class="left"><?= ($flag) ? $reference[1]['address'] : '' ?></td>
                </tr>
                <tr> 
                    <td class="left">Phone(Off) :</td>
                    <td class="left"><?= $reference[0]['phone_office'] ?></td>
                    <td class="left"><?= ($flag) ? $reference[1]['phone_office'] : '' ?></td>
                </tr>
                <tr> 
                    <td class="left">Phone(Res) :</td>
                    <td class="left"><?= $reference[0]['phone_resident'] ?></td>
                    <td class="left"><?= ($flag) ? $reference[1]['phone_resident'] : '' ?></td>
                </tr>
                <tr> 
                    <td class="left">mobile :</td>
                    <td class="left"><?= $reference[0]['mobile'] ?></td>
                    <td class="left"><?= ($flag) ? $reference[1]['mobile'] : '' ?></td>
                </tr>
                <tr> 
                    <td class="left">Email :</td>
                    <td class="left"><?= $reference[0]['email'] ?></td>
                    <td class="left"><?= ($flag) ? $reference[1]['email'] : '' ?></td>
                </tr>
                <tr> 
                    <td class="left">Relation :</td>
                    <td class="left"><?= $reference[0]['relation'] ?></td>
                    <td class="left"><?= ($flag) ? $reference[1]['relation'] : '' ?></td>
                </tr>
            </tbody>
        </table>
    <? } 
   ?>
</div>


<!--<div><a style="color:black" href="http://localhost:81/purbodesh/adminpanel/report_page/cv.php">Convert PDF</a></div>-->
