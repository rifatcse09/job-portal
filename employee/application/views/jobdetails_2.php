<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<head>
    <title> Job Details</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/job_details_template2.css" />
    
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</head>
<body>
    <div id="content_container">

        <div id="content">
            <div id="content_content">

                <div id="header">
                    <?= $this->load->view("backpage_header"); ?>
                   <div style="float:left;margin:10px;width: 30%;"><img style="width: 100%;height:200px" src="<?=base_url().'employer/companylogos/'.$com_info['company_logo']?>"/></div>
                   <div style="float:left"> <p style="margin: 15px;font-family: Tahoma;font-size: 20pt"><?= $main_job['company_name'] ?></p>   
                       <p>
                           <?=$com_info['business_description'] ?>
                       </p>  
                   </div>
                </div>
                <div id="main_box">
<!--                    <div id="header2">
                        <div id="header2_left">
                             <p><strong>Category :</strong> <?= $main_job['category_name'] ?></p><br>
                           
                        </div>
                    </div>-->
                    <div id="header3">
                        <p><?= $main_job['job_title'] ?></p>
                    </div>
                    <div id="job_details_container" style="margin-top: 10px">
                        <table id="job_details_content">
                            <tr>
                                <td width="52%" valign="top" height="21">
                                    <span class="BDJtabNormal">No. of Vacancies :</span>
                                </td>
                                <td width="48%" valign="top" style="font-weight: bold">
                                    <?= $main_job['no_vacancy'] ?>
                                </td>
                            </tr>
                        </table>
                        <br/>
                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal"> Job Description / Responsibility : </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?
                                    $des_array = explode("\n", $main_job['job_responsibility']);
                                    foreach ($des_array as $des) {
                                        ?>
                                        <ul> 
                                            <li><?= $des ?></li>
                                        </ul>
                                        <?
                                    }
                                    ?>                           

                                </td>
                            </tr>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Job Nature :</span>
                                </td>
                                <td><?= $main_job['job_type'] ?></td>
                            </tr>


                        </table>
                        <br/>
                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Educational Requirements :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?
                                    $edu_array = explode("\n", $main_job['edu_qualification']);
                                    foreach ($edu_array as $edu) {
                                        ?>
                                        <ul>
                                            <li><?= $edu ?></li>
                                        </ul>
                                        <?
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Experience Requirements :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul><?
                                    if ($main_job['experience_min'] >= 0 && $main_job['experience_max'] >= 0) {
                                        echo '<li>' . $main_job['experience_min'] . ' to ' . $main_job['experience_max'] . ' year(s).</li>';
                                    } else if ($main_job['experience_min'] >= 0 && $main_job['experience_max'] < 0) {
                                        echo '<li>Minimum ' . $main_job['experience_min'] . ' year(s).</li>';
                                    } else if ($main_job['experience_min'] < 0 && $main_job['experience_max'] >= 0) {
                                        echo '<li>Maximum ' . $main_job['experience_min'] . ' year(s).</li>';
                                    } else if ($main_job['experience_min'] < 0 && $main_job['experience_max'] < 0) {
                                        echo '<li>N/A</li>';
                                    }
                                    if ($business_types) {
                                        if (count($business_types)) {
                                            echo '<li>The applicants should have experience in the following area(s):<br />';
                                            foreach ($business_types as $key => $business_type) {
                                                echo $business_type['business_name'];
                                                if($key < (count($business_types) - 1))
                                                    echo ', ';
                                            }
                                            echo '</li>';
                                        }
                                    }
                                    if ($business_areas) {
                                        if (count($business_areas)) {
                                            echo '<li>The applicants should have experience in the following business area(s):<br />';
                                            foreach ($business_areas as $key => $business_area) {
                                                echo $business_area['area_name'];
                                                if($key < (count($business_areas) - 1))
                                                    echo ', ';
                                            }
                                            echo '</li>';
                                        }
                                    }
                                    ?>

                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Additional Job Requirements :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul>
                                        <?
                                        $add_req_array = explode("\n", $main_job['add_job_requirement']);
                                        foreach ($add_req_array as $edu) {
                                            ?>
                                            <li><?= $edu ?></li>
                                            <?
                                        }
                                        ?>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Salary Range :</span>
                                </td>
                            </tr>
                            <?
                            if ($main_job['optBenefits'] != 2) {
                                ?>
                                <tr>
                                    <td>
                                        <ul>
                                            <?
                                            if ($main_job['optBenefits'] == 1) {
                                                echo '<li>Negotiable</li>';
                                            } else if ($main_job['optBenefits'] == 3) {
                                                if ($main_job['salary_min'] > 0 && $main_job['salary_max'] > 0) {
                                                    echo '<li>' . $main_job['salary_min'] . ' to ' . $main_job['salary_max'] . ' BDT.</li>';
                                                } else if ($main_job['salary_min'] > 0 && $main_job['salary_max'] <= 0) {
                                                    echo '<li>' . $main_job['salary_min'] . ' BDT.</li>';
                                                } else if ($main_job['salary_min'] <= 0 && $main_job['salary_max'] > 0) {
                                                    echo '<li>' . $main_job['salary_max'] . ' BDT.</li>';
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </td>
                                </tr>
                                <?
                            }
                            ?>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Other Benefits :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul>
                                        <?
                                        $opt_benefits = explode("\n", $main_job['other_benefits']);
                                        foreach ($opt_benefits as $opt) {
                                            ?>
                                            <li><?= $opt ?></li>
                                            <?
                                        }
                                        ?>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Job Location :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul>
									<?
							if($main_job['job_location']=='-1')echo '<li>Anywhere in Bangladesh.</li>';
							else if($main_job['job_location']=='1')echo 'B. Baria';
							else if($main_job['job_location']=='2')echo 'Bagerhat';
							else if($main_job['job_location']=='3')echo 'Bandarban';
							else if($main_job['job_location']=='4')echo 'Barisal';
							else if($main_job['job_location']=='5')echo 'Bhola';
							else if($main_job['job_location']=='6')echo 'Bogra';
							else if($main_job['job_location']=='7')echo 'Borguna';
							else if($main_job['job_location']=='8')echo 'Chandpur';
							else if($main_job['job_location']=='9')echo 'Chapainawabganj';
							else if($main_job['job_location']=='10')echo 'Chittagong';
							else if($main_job['job_location']=='11')echo 'Chuadanga';
							else if($main_job['job_location']=='12')echo 'Comilla';
							else if($main_job['job_location']=='13')echo 'Coxs Bazar';
							else if($main_job['job_location']=='14')echo 'Dhaka';
							else if($main_job['job_location']=='15')echo 'Dinajpur';
							else if($main_job['job_location']=='16')echo 'Faridpur';
							else if($main_job['job_location']=='17')echo 'Feni';
							else if($main_job['job_location']=='18')echo 'Gaibandha';
							else if($main_job['job_location']=='19')echo 'Gazipur';
							else if($main_job['job_location']=='20')echo 'Gopalgonj';
							else if($main_job['job_location']=='21')echo 'Hobigonj';
							else if($main_job['job_location']=='22')echo 'Jamalpur';
							else if($main_job['job_location']=='23')echo 'Jessore';
							else if($main_job['job_location']=='24')echo 'Jhalokathi';
							else if($main_job['job_location']=='25')echo 'Jhenaidah';
							else if($main_job['job_location']=='26')echo 'Joypurhat';
							else if($main_job['job_location']=='27')echo 'Khagrachari';
							else if($main_job['job_location']=='28')echo 'Khulna';
							else if($main_job['job_location']=='29')echo 'Kishorgonj';
							else if($main_job['job_location']=='30')echo 'Kurigram';
							else if($main_job['job_location']=='31')echo 'Kushtia';
							else if($main_job['job_location']=='32')echo 'Lalmonirhat';
							else if($main_job['job_location']=='33')echo 'Laxmipur';
							else if($main_job['job_location']=='34')echo 'Madaripur';
							else if($main_job['job_location']=='35')echo 'Magura';
							else if($main_job['job_location']=='36')echo 'Manikgonj';
							else if($main_job['job_location']=='37')echo 'Meherpur';
							else if($main_job['job_location']=='38')echo 'MoulaviBazar';
							else if($main_job['job_location']=='39')echo 'Munshigonj';
							else if($main_job['job_location']=='40')echo 'Mymensingh';
							else if($main_job['job_location']=='41')echo 'Naogaon';
							else if($main_job['job_location']=='42')echo 'Narail';
							else if($main_job['job_location']=='43')echo 'Narayangonj';
							else if($main_job['job_location']=='44')echo 'Narshingdi';
							else if($main_job['job_location']=='45')echo 'Natore';
							else if($main_job['job_location']=='46')echo 'Netrokona';
							else if($main_job['job_location']=='47')echo 'Nilphamari';
							else if($main_job['job_location']=='48')echo 'Noakhali';
							else if($main_job['job_location']=='49')echo 'Pabna';
							else if($main_job['job_location']=='50')echo 'Panchagarh';
							else if($main_job['job_location']=='51')echo 'Patuakhali';
							else if($main_job['job_location']=='52')echo 'Pirojpur';
							else if($main_job['job_location']=='53')echo 'Rajbari';
							else if($main_job['job_location']=='54')echo 'Rajshahi';
							else if($main_job['job_location']=='55')echo 'Rangamati';
							else if($main_job['job_location']=='56')echo 'Rangpur';
							else if($main_job['job_location']=='57')echo 'Satkhira';
							else if($main_job['job_location']=='58')echo 'Shariatpur';
							else if($main_job['job_location']=='59')echo 'Sherpur';
							else if($main_job['job_location']=='60')echo 'Sirajgonj';
							else if($main_job['job_location']=='61')echo 'Sunamgonj';
							else if($main_job['job_location']=='62')echo 'Sylhet';
							else if($main_job['job_location']=='63')echo 'Tangail';
							else if($main_job['job_location']=='64')echo 'Thakurgaon';
							
							
							
							
                                   ?>     
                                    </ul>
                                </td>
                            </tr>
                        </table>


                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Job Source :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul>
                                        <li>Azadijobs Online Job Posting.</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <div id="job_details_bottom">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                <form method="post" action="#" name="form1"></form>
                                <tbody>
                                    <tr class="body">
                                        <td align="center" style="padding-bottom:10px;">

                                            <a style="width: 220px;height: 50px;background-color: green;color:#fff;padding:10px;font-weight: bold" href="<?= site_url() ?>/cont_email?job_id=<?= $_GET['ID'] ?>&title=login">Apply Online</a>
                                            <br>
                                            <br>
                                            <? if ($main_job['email_attach'] == 'y') { ?>
                                                or
                                                <br>Send your CV to
                                                <strong> <?= $main_job['email_id'] ?></strong>
                                                <br>
                                            <? } ?>
                                            <strong>Special Instruction :</strong>
                                            <?= $main_job['apply_instruction'] ?>
                                            <br>

                                            <strong>
                                                <tr class="body">
                                                    <td align="center">
                                                        <strong class="BDJNoticeVerdana11Red">Application Deadline: </strong>
                                                        <span style="color:#FF0000"> <?= date("d F,Y", strtotime($main_job['deadline'])) ?></span>
                                                    </td>
                                                </tr>
                                                <tr class="body">
                                                    <td align="center" style="padding-top:10px;">
                                                        <span><?= $main_job['job_title'] ?></span>
                                                        <?
                                                        if ($main_job['hide_address'] == 'n') {
                                                            ?>    
                                                            <br>
                                                            <span>Company Address:</span>    
                                                            <span class="BDJGrayArial11px"><?= $com_info['company_address'] ?></span>
                                                            <br>
                                                            <? if (isset($com_info['website_address'])) { ?>
                                                                <span>Web : </span>
                                                                <a href="http://<?= $com_info['website_address'] ?>" target="_blank"><?= $com_info['website_address'] ?></a>
                                                                <br>
                                                                <span class="BDJGrayArial11px">
                                                                    <span style="color: #003399" <strong><?= $com_info['business_description'] ?></strong>
                                                                    </span>
                                                                    <?
                                                                }
                                                            }
                                                            ?>
                                                    </td>
                                                </tr>
                                                <tr><td align="center"><div class="fb-share-button" data-href="https://www.facebook.com/bdAccountingSoftware" data-layout="button_count"></div></td></tr>
                            </table>  
                               
                        </div>
                        <div id="footer_last_navigation">
                            <?
                            if (isset($main_job['company_id'])) {
                                $data['company_id'] = $main_job['company_id'];
                            } else {
                                $data['company_id'] = "#";
                            }
                            ?>

                            <?= $this->load->view("jobsbycategory_bottom_link", $data); ?>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>

       

    </div>
    
    <div id="footer_bottom_navigation">
            <?= $this->load->view("footer_bottom_navigation"); ?>
    </div>
    
    <div id="wrapper_bottom">
             <div style="float:right;margin-right: 20px"><p title="Design & Developed by PentaGems" style="color:#fff">Design & Developed by <a title="PentaGems"  style="text-decoration: none;color:#E6E600;font-weight: bold"target="_blank" href="http://pentagemsbd.com">PentaGems</a></p></div>
                <? //$this->load->view("footer_bottom_navigation.php"); ?>
    </div>
<!--    <div id="footer_last"></div>-->
</body>

</html>
