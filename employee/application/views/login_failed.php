<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<head>
    <title>Login</title>
    <link rel="stylesheet" href="css/style.css">
    <script type="text/javascript">
        function validate(){
            var user = document.getElementById('user_name');
            var pass= document.getElementById('password');
            
            if(user.value == "" || pass.value == ""){
                alert('You have to fill up both fields.');
                return false;
            }
        }
</script>
    <style>
          input[type=text]:focus, input[type=password]:focus 
                                               {
                                               box-shadow: 0 0 5px rgba(81, 203, 238, 1);
/*                                               padding: 3px 0px 3px 3px;*/
/*                                               margin: 5px 1px 3px 0px;*/
                                               border: 1px solid rgba(81, 203, 238, 1);
                                               }  
        input[type="submit"] 
                                               {
                                                       background-color: #F5F5F5;
                                                       border: 1px solid rgba(0, 0, 0, 0.1);
                                                       border-radius: 2px;
                                                       color: #222222;
                                                       font: bold 12px arial;
                                                       height: 30px;
                                                       margin: 1px 0 0 15px;
                                                       padding-bottom: 0;
                                                       text-align: center;
                                                       text-shadow: 0 1px rgba(0, 0, 0, 0.1);
                                                       vertical-align: top;
                                                       width: 80px;
                                              }
                                              #login {
    background-color: #4D90F0;
    border: 1px solid #3079ED;
    color: #FFFFFF;
    font-weight: bold;
    margin: 8px 0 0 18px;
}
                               #login:hover {
    background-color: #3571eb;
    border-color:#2f5bb7;
 
    font-weight: bold;
    margin: 8px 0 0 18px;
}   
    </style>
    <link rel="stylesheet" href="<?= base_url(); ?>/stylesheets/font-awesome.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
    <link href="<?= base_url(); ?>/stylesheets/bootstrap.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>/stylesheets/custom.css" rel="stylesheet" />
</head>
<body style="background-color:#fff">
   
    <div id="container" style="margin-left: 100px;margin-right: 20px;">
    <div id="con_left" style="float:left;width:49%;margin-left: 20px;margin-bottom: 20px;margin-right: 20px; margin-top: 50px;border: 2px solid #1a4f91;">
                            <div id="training" class="table-responsive" style='margin-top:5px;float:left'>
                                <div id="training_header" style='width:100%;border-top-right-radius:.10em;border-top-left-radius:.5em'>
                                    
                                    <div id="training_header_text" style="text-align:left;" >

                                        <p style="text-align: center;font-size: 20px; font-family: Tahoma;color:#000"><i class="fa fa-pencil fa-2x"></i> <b>Training</b></p>
                                    </div>
                                </div>
                                <div id="training_content" style="width: 99.5%" >
                                    <div id="training_content_left" class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="" style="border-collapse: collapse;width: 100%">
                                            <?
                                            
                                            $sql = "select * from training order by training_id desc";
                                            $data = mysql_query($sql);
                                            $num_rows = mysql_num_rows($data);
                                            while ($r = mysql_fetch_array($data)) {

                                                $resultSet[] = $r;
                                            }

                                            $count_train = 1;
                                            for ($i = 0; $i < 6;) {
                                                $count_train++;

                                                if (($count_train + 1) % 2 == 0) {
                                                    echo '<tr style="background:#F1F2F3;border-bottom:1px solid #e5e6e6">';
                                                } else {
                                                    echo '<tr style="background:#E7ECF2;border-bottom:1px solid #e6e6e6">';
                                                }

                                                for ($j = 0; $j < 2; $j++) {
                                                    ?>


                                                    <td style="border-right:1px solid #e6e6e6;width: 50%"><p style="width:98%; color: #4091f0; font-size: 8pt; font-family:Tahoma; margin-top: 10px;font-weight: bold;padding-left: 5px;padding-bottom: 5px;"><? echo $resultSet[$i][1]; ?></p>
                                                        <p style="width:100%; color:#ff0000; font-size:9pt;font-family:century gothic; margin-top: -6px;font-weight: bold;padding-left: 5px">Date: <? echo date('d-m-y', strtotime($resultSet[$i][2])); ?></p>

                                                        <div style="width:98%;margin-left: 0%;padding-left: 5px;font-family:Tahoma;font-size: 12px"><? echo substr($resultSet[$i][3], 0, 50) ?></div>
                                                        <a style="text-decoration:none; margin-top: 3px;padding-left: 5px;font-weight:bold; color:#1a4f91;font-size: 8pt;" href="../../../index.php/training/training_details?training_id=<? echo $resultSet[$i][0]; ?>" target="_blank">Click for Details</a>
                                                    </td>


                                                    <?
                                                    $i++;
                                                }
                                                echo '</tr>';
                                            }
                                            ?>
                                        </table>
                                    </div>

                                </div>

                                <div id="training_more" style="background: none">
                                    <div style="margin-left:240px;margin-bottom: 5px;" id="training_more_redBar">

                                        <a  class="btn btn-success"  href="../../../index.php/training/training_all"><i class="fa fa-angle-double-right"></i> Next Training</a>
                                    </div>
                                </div>
                            </div>
                        </div>
    
    <div style='border: 2px solid #1a4f91;float: left; width: 500px; text-align: center; border-radius: 2px;margin-top:51px; padding: 10px;background-color: #FFFFFF;
    
   '>
       
        <div >
            <i class="fa fa-user-secret fa-5x" style="color:#00141F;" ></i>
            
        </div>
        
        <div style="background:#fff;margin-top: 50px; "> 
        <form style='margin-top:50px' action="<?= site_url()?>/page/log_me_in" method="Post" onsubmit="return validate()" name="login_form">
            <? if($this->session->flashdata('link')) $this->session->set_flashdata('link', $this->session->flashdata('link')); ?>
            <table style='width: 100%;' class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th colspan="2" style="background: #000;color: #fff">User Login</th>
                    </tr>
                </thead>
               
                <tbody>
                       <tr style="height:20px"><td></td>
                       <td></td><tr>
                    <tr>
                        <td style="text-align:right">User name:</td>
                        <td style="text-align:left"><input type="text" name="user_name" id="user_name"/></td>
                    </tr>
                    <tr>
                        <td style="text-align:right">Password:</td>
                        <td style="text-align:left"><input type="password" name="password" id="password"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="1" style="text-align:left"><input id="login" type="submit" name="login_submit" id="login_submit" value="Login"/></td>
                    </tr>
          
                </tbody>
                 
            </table>
            
      
        </form>
         <?= $msg ?><br/>
        <a  style="text-decoration:none " href="<?= site_url()?>/createaccount">New user? Create a new account</a><br/>
        </div>
       
            
            
            
        </div>
    </div>
</body>
</html>