<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
    
<meta charset="utf-8">
    <head>
        
    
        <title><?= $title ?></title>
        <?
        if (!isset($this->session->userdata['user_id'])) {

            echo '<link rel="stylesheet" type="text/css" href="' . base_url() . 'stylesheets/stylesheet_1.css" />';
        } else {
            echo '<link rel="stylesheet" type="text/css" href="' . base_url() . 'stylesheets/stylesheet.css" />';
        }
        ?>

        <style type="text/css">
               <?= $style ?>
        </style>
    </head>
    <body>


        <div id="container">
            <div id="header">
                <?= $header ?>
            </div>

            <div id="container_content">

                <div id="main">

<!--                    <div id="main_navigation">
                        //$this->load->view("common/content_header");  
                    </div>-->
                    <?= $sidebar ?>
                    
                    <div id="main_logged">
                        <div id="main_logged_header">
                            <? if (isset($this->session->userdata['user_name'])) {
                                ?>
                                <p>Logged in as: <?= $this->session->userdata['user_name'] ?><span style="margin-left: 50px; color:snow;"><a href="../../">Logout</a></span></p>
                            <? } ?>
                        </div>
                    </div>

                    <div id="main_box">
                        <div id="content">
                            
                            <?= $content ?>
                        </div>

                        <div id="footer">
                            <?= $footer ?>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </body>
</html>