<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
    <head>
        <title><?= isset($title) ? $title : 'PurobodeshJobs' ?></title>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>stylesheets/stylesheet.css" />
        <style type="text/css">
            body{
                background: #FFF;
            }
        </style>
    </head>
    <body style="font-size: 0.8em">
        <div style="width: 100%; border: 1px solid #FFF;">
            <? if (isset($career)) { ?>
                <table style="width: 100%; border: 0px;">
                    <thead>
                        <tr>
                            <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career Objective</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr><td style="text-align: left;"><?= $career['objective'] ?></td></tr>
                    </tbody>
                </table>
            <? } ?>
            <table style="width: 100%; border: 0px;">
                <thead>
                    <tr>
                        <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Personal Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align: left;">Name:</td>
                        <td style="text-align: left;"><?= $personal['name'] ?></td>
                        <td width="126" height="135" rowspan="5"><img src="<?= base_url() . 'photographs/' . $personal['image'] ?>" alt="no photo" width="124" height="135" /></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Father's Name:</td>
                        <td style="text-align: left;"><?= $personal['father_name'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Mother's Name:</td>
                        <td style="text-align: left;"><?= $personal['mother_name'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Date of Birth:</td>
                        <td style="text-align: left;"><?= date('d F, Y', strtotime($personal['date_birth'])) ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Gender:</td>
                        <td style="text-align: left;"><? echo $personal['gender'] == 'm' ? 'Male' : 'Female'; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Marital status:</td>
                        <td style="text-align: left;" colspan="2"><? echo $personal['marital_status'] == 'm' ? 'Married' : 'Single'; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Nationality:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['nationality'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Religion:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['religion'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Present Address:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['present_add'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Permanent Address:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['permanent_add'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Current Location:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['District_Name'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Contact(home):</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['home_phone'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Contact(Mobile):</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['mobile'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Contact(Office):</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['office_phone'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Email:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['email'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Alternate Email:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['alternate_email'] ?></td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <?
            if (count($emp_history) > 0) {
                ?>
                <table style="width: 100%; border: 0px;">
<!--                    <thead>
                        <tr>
                            <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career Objective</th>
                        </tr>
                    </thead>-->
                    <tbody>
                        <?
                        foreach ($emp_history as $key => $history) {
                            ?>

                            <tr>
                                <td align="left">
                                    <span style="font-weight: bold; text-decoration: underline">
                                        <?= ($key + 1) . '.' . $history['position_held']; ?>
                                        <?= '(' . date('F d, Y', strtotime($history['from'])) . ' - '; ?>
                                        <?= ($history['continuing'] == 'y') ? 'continuing)' : date('F d, Y', strtotime($history['to'])) . ')'; ?>
                                    </span><br/><br/>
                                    <span style="font-weight: bold;">
                                        <?= $history['company_name'] ?>
                                    </span><br/>
                                    <span>
                                        Company Location: <?= $history['company_location'] ?><br/>
                                        Department: <?= $history['department'] ?>
                                    </span><br/>
                                    <span>
                                        <i><b style="text-decoration: underline">Duties/Responsibilities</b></i><br/>
                                        <?
//                                        $duties = explode("\n", $history['responsibilities']);
//                                        foreach ($duties as $duty) {
//                                            echo $duty . '<br/>';
//                                        }
                                        echo $history['responsibilities'];
                                        ?>
                                    </span>
                                </td>
                            </tr>

                            <?
                        }
                        ?>
                    </tbody>
                </table>
                <?
            }
            ?>
            <br />
            <?
            if (count($acc_qualification) > 0) {
                ?>
                <table style="width: 100%; border: 0px;">
<!--                    <thead>
                        <tr>
                            <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career Objective</th>
                        </tr>
                    </thead>-->
                    <tbody>

                        <tr>
                            <td>
                                <table style="margin: 0 auto; border: 0px;">
                                    <thead>
                                        <tr>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Exam Title</th>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Concentration/Major</th>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Institute</th>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Result</th>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Passing Year</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?
                                        foreach ($acc_qualification as $acc) {
                                            ?>
                                            <tr>
                                                <td><?= $acc['degree_title'] ?></td>
                                                <td><?= $acc['major'] ?></td>
                                                <td><?= $acc['institute'] ?></td>
                                                <td><?= $acc['result'] ?></td>
                                                <td><?= $acc['passing_year'] ?></td>
                                            </tr>
                                            <?
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>       

                    </tbody>
                </table>
                <?
            }
            ?>
            <br />
            <?
            if (count($career) > 0) {
                ?>

                <table style="width: 100%; border: 0px;">
<!--                    <thead>
                        <tr>
                            <th colspan="2" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career Objective</th>
                        </tr>
                    </thead>-->
                    <tbody>
                        <tr>
                            <td style="text-align: left;">Looking For:</td>
                            <td style="text-align: left;"><?= $career['looking_for'] ?> Level Job</td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">Available For:</td>
                            <td style="text-align: left;"><?= $career['available_for'] ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">Present Salary:</td>
                            <td style="text-align: left;"><?= $career['present_salary'] ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">Expected Salary:</td>
                            <td style="text-align: left;"><?= $career['expected_salary'] ?></td>
                        </tr>
                        <? if (isset($categories)) {
                            if (count($categories) > 0) { ?>
                                <tr>
                                    <td style="text-align: left;">Preferred Job Category:</td>
                                    <td style="text-align: left;">
                                        <?
                                        foreach ($categories as $key => $cat) {
                                            if ($key == 0) {
                                                echo $cat['category_name'];
                                            } else {
                                                echo ', ' . $cat['category_name'];
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <? }
                        } ?>
                        <? if (count($pre_organization) > 0) { ?>
                            <tr>
                                <td style="text-align: left;">Preferred Organization:</td>
                                <td style="text-align: left;">
                                    <?
                                    foreach ($pre_organization as $key => $org) {
                                        if ($key == 0) {
                                            echo $org['business_name'];
                                        } else {
                                            echo ', ' . $org['business_name'];
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                        <? } ?>
                        <tr>
                            <td style="text-align: left;">Preferred Area:</td>
                            <td style="text-align: left;">
                                <? if (count($pre_location) > 0) { ?>

                                    <?
                                    foreach ($pre_location as $key => $loc) {
                                        if ($key == 0) {
                                            echo $loc['District_Name'];
                                        } else {
                                            echo ', ' . $loc['District_Name'];
                                        }
                                    }
                                    ?>

                                    <?
                                } else {
                                    echo 'Anywhere in Bangladesh.';
                                }
                                ?>
                            </td>
                        </tr>
    <!--                            <tr>
                        <td class="right">Looking For:</td>
                        <td>Name</td>
                    </tr>
                    <tr>
                        <td class="right">Available For:</td>
                        <td>Name</td>
                    </tr>-->

                    </tbody>
                </table>
            <? } ?>
            <br />

<!--            <table class="no-border" style="width: 100%">
    <thead>
        <tr>
            <th colspan="2">Specialization</th>
        </tr>
        <tr>
            <th>Fields of Specialization</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>c#,MVC,JQuery,Mysql,Sql Server, ASP.Net</td>
            <td>name</td>
        </tr>
    </tbody>
</table>-->
            <?
            if (count($language) > 0) {
                ?>

                <table style="width: 100%; border: 0px;">
                    <thead>
<!--                        <tr>
                            <th colspan="4" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career Objective</th>
                        </tr>-->
                        <tr>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;">Language</th>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;">Reading</th>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;">Writing</th>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;">Speaking</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($language as $lan) {
                            ?>
                            <tr>
                                <td><?= $lan['language_name'] ?></td>
                                <td><?= $lan['reading'] ?></td>
                                <td><?= $lan['writing'] ?></td>
                                <td><?= $lan['speaking'] ?></td>
                            </tr>
                        <? } ?>
                    </tbody>
                </table>
            <? } ?>
            <? if (count($reference) > 0) { ?>
                <table style="width: 100%; border: 0px;">
                    <thead>
<!--                        <tr>
                            <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career Objective</th>
                        </tr>-->

                        <tr>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;"></th>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;"><strong>Reference: 1</strong></th>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;"><strong><? if (isset($reference[1])) { ?>Reference : 2<? } ?></strong></th>
                        </tr>
                    </thead>
                    <tbody>		
                        <tr> 
                            <td style="text-align: left;">Name :</td>
                            <td style="text-align: left;"><?= $reference[0]['ref_name'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['ref_name']) ? $reference[1]['ref_name'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">organization :</td>
                            <td style="text-align: left;"><?= $reference[0]['organization'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['organization']) ? $reference[1]['organization'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Designation :</td>
                            <td style="text-align: left;"><?= $reference[0]['designation'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['designation']) ? $reference[1]['designation'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Address :</td>
                            <td style="text-align: left;"><?= $reference[0]['address'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['address']) ? $reference[1]['address'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Phone(Off) :</td>
                            <td style="text-align: left;"><?= $reference[0]['phone_office'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['phone_office']) ? $reference[1]['phone_office'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Phone(Res) :</td>
                            <td style="text-align: left;"><?= $reference[0]['phone_resident'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['phone_resident']) ? $reference[1]['phone_resident'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">mobile :</td>
                            <td style="text-align: left;"><?= $reference[0]['mobile'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['mobile']) ? $reference[1]['mobile'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Email :</td>
                            <td style="text-align: left;"><?= $reference[0]['email'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['email']) ? $reference[1]['email'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Relation :</td>
                            <td style="text-align: left;"><?= $reference[0]['relation'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['relation']) ? $reference[1]['relation'] : '' ?></td>
                        </tr>
                    </tbody>
                </table>
            <? } ?>
        </div>

    </body>
</html>
