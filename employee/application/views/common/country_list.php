<? 
    $size = isset($country_size) ? $country_size : 1; 
    $name = isset($country_name) ? $country_name : 'country_name';
    $id = isset($country_id) ? $country_id : 'country_id';
?>
<select id="<?=$id?>" style="width:280px;margin-top: 15px;margin-bottom: 15px;border:1px solid #e6e6e6" size="<?=$size;?>" name="<?=$name?>" ?>>
<?php
    if($countries){
        
        foreach ($countries as $country){
            echo '<option  style="height:25px;font-size:16px;padding-left:10px" value="' . $country['Country_Id'] . '">' . $country['Country_Name'] . '</option>';
        }
    }
?>
</select>