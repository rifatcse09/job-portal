<div id="add_employment">
    <h3>Entry form - Employment History</h3>
    <form action="<?= site_url()?>/cont_employment/create_employment" method="post" name="employment_form" id="employment_form" onsubmit="return validate_employment()">
    <table class="form-table" style="width: 90%">
        <thead>
            <tr>
                <th colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Company Name:</td>
                <td>
                    <input type="text" name="emp_company" id="emp_company"/>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Company Business:</td>
                <td>
                    <input type="text" name="emp_business" id="emp_business" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Company Location:</td>
                <td>
                    <input type="text" name="emp_location" id="emp_location" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Department:</td>
                <td>
                    <input type="text" name="emp_department" id="emp_department" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Position Held:</td>
                <td>
                    <input type="text" name="emp_position" id="emp_position" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Area of Experience:</td>
                <td>
                    <input type="text" name="emp_area_exp" id="emp_area_exp"/>
                </td>
            </tr>
            <tr>
                <td class="right">Responsibilities:</td>
                <td>
                    <textarea name="emp_responsibilities" id="emp_responsibilities"></textarea>
                </td>
            </tr>
            <tr>
                <td class="right">From:</td>
                <td>
                    <input type="text" name="emp_from" id="emp_from" />
                </td>
            </tr>
            <tr>
                <td class="right">To:</td>
                <td>
                    <input type="text" name="emp_to" id="emp_to" /><br/>
                    <input type="checkbox" name="emp_continue" id="emp_continue" onclick="disable_field('emp_to', this.id)"/>continuing
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="emp_submit" id="emp_submit" value="Submit" />
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</div>
