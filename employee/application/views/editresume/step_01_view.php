<link rel="stylesheet" type="text/css" href="<?= base_url() ?>stylesheets/jquery-ui-1.8.18.custom.css" />
<script type="text/javascript" src="<?= base_url() ?>JS/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/jquery-ui-1.8.11.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/common_ui.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/personal_validation.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/career/career_validation.js"></script>
<script type="text/javascript">
    var base_url = '<?= site_url() ?>';
    $(document).ready(function(){
        dialog_open("#edit_diag", "Edit Personal");
        dialog_open("#edit_diag_career", "Edit Career");
        dialog_open("#edit_diag_category", "Edit Category");
        dialog_open("#edit_diag_location", "Edit Location");
        dialog_open("#edit_diag_organization", "Edit Organization");
        dialog_open("#edit_diag_relevant", "Edit Special");
    })
    
</script>
<?= $this->load->view("editresume/content_top_navigation"); ?>
<div id="main-content">
    <h3 style="margin-left: 250px;">Personal Details</h3>
    <table class="form-table" style="width: 147%">
        <thead>
            <tr>
                <th colspan="2">Personal Detail
                    <span style="float: right; margin-right: 5px;">
                        <a href="javascript:edit_personal(<?= $personal['id'] ?>)">edit</a></span>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Name:</td>
                <td><?= isset($personal['name']) ? $personal['name'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Father's Name:</td>
                <td><?= isset($personal['father_name']) ? $personal['father_name'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Mother's Name:</td>
                <td><?= isset($personal['mother_name']) ? $personal['mother_name'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Date of Birth:</td>
                <td><?= isset($personal['date_birth']) ? date('d F, Y', strtotime($personal['date_birth'])) : '' ?></td>
            </tr>
            <tr>
                <td class="right">Gender:</td>
                <td>
                    <?
                    if ($personal['gender'] == 'm')
                        echo 'Male';
                    else
                        echo 'Female';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="right">Marital status:</td>
                <td>
                    <?
                    if ($personal['marital_status'] == 's')
                        echo 'Single';
                    else
                        echo 'Married';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="right">Nationality:</td>
                <td><?= isset($personal['nationality']) ? $personal['nationality'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Religion:</td>
                <td><?= isset($personal['religion']) ? $personal['religion'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Present Address:</td>
                <td><?= isset($personal['present_add']) ? $personal['present_add'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Permanent Address:</td>
                <td><?= isset($personal['permanent_add']) ? $personal['permanent_add'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Current Location:</td>
                <td><?= isset($personal['District_Name']) ? $personal['District_Name'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Contact(home):</td>
                <td><?= isset($personal['home_phone']) ? $personal['home_phone'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Contact(Mobile):</td>
                <td><?= isset($personal['mobile']) ? $personal['mobile'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Contact(Office):</td>
                <td><?= isset($personal['office_phone']) ? $personal['office_phone'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Email:</td>
                <td><?= isset($personal['email']) ? $personal['email'] : '' ?></td>
            </tr>
            <tr>
                <td class="right">Alternate Email:</td>
                <td><?= isset($personal['alternate_email']) ? $personal['alternate_email'] : '' ?></td>
            </tr>
        </tbody>
    </table>
    <br/>
    <table class="form-table" style="width: 147%">
        <thead>
            <tr>
                <th colspan="2">Career and Application
                    <span style="float: right; margin-right: 5px;">
                        <a href="javascript:edit_career(<?= $career['user_id'] ?>)">edit</a></span>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Career Objective:</td>
                <td><?= $career['objective'] ?></td>
            </tr>
            <tr>
                <td class="right">Year of Experience:</td>
                <td><?= $career['year_experience'] ?></td>
            </tr>
            <tr>
                <td class="right">Present Salary:</td>
                <td><?= $career['present_salary'] ?></td>
            </tr>
            <tr>
                <td class="right">Expected Salary:</td>
                <td><?= $career['expected_salary'] ?></td>
            </tr>
            <tr>
                <td class="right">Looking For:</td>
                <td><?= $career['looking_for'] ?> level job</td>
            </tr>
            <tr>
                <td class="right">Available For:</td>
                <td><?= $career['available_for'] ?></td>
            </tr>

        </tbody>
    </table>
    <br />
    <table class="form-table" style="width: 147%">
        <thead>
            <tr>
                <th colspan="2">Preferred Job Category
                    <span style="float: right; margin-right: 5px;">
                        <a href="javascript:edit_category(<?= $personal['id'] ?>)">edit</a></span>
                </th>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Preferred Job Category:</td>
                <td>
                    <?
                    foreach ($pref_categories as $key => $cat) {
                        if ($key == 0) {
                            echo $cat['category_name'];
                        } else {
                            echo ', ' . $cat['category_name'];
                        }
                    }
                    ?>
                </td>
            </tr>       

        </tbody>
    </table>
    <br />
    <table class="form-table" style="width: 147%">
        <thead>
            <tr>
                <th colspan="2">Preferred Job Location
                    <span style="float: right; margin-right: 5px;">
                        <a href="javascript:edit_location(<?= $personal['id'] ?>)">edit</a></span>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Inside Bangladesh:</td>
                <td>
                    <?
                    foreach ($pref_locs as $key => $loc) {
                        if ($key == 0) {
                            echo $loc['District_Name'];
                        } else {
                            echo ', ' . $loc['District_Name'];
                        }
                    }
                    ?>
                </td>
            </tr>       
            <tr>
                <td class="right">Outside Bangladesh:</td>
                <td>
                    <?
                    foreach ($pref_cons as $key => $con) {
                        if ($key == 0) {
                            echo $con['Country_Name'];
                        } else {
                            echo ', ' . $con['Country_Name'];
                        }
                    }
                    ?>
                </td>
            </tr>    
        </tbody>
    </table>
    <br />
    <table class="form-table" style="width: 147%">
        <thead>
            <tr>
                <th colspan="2">Preferred Organization Type
                    <span style="float: right; margin-right: 5px;">
                        <a href="javascript:edit_organization(<?= $personal['id'] ?>)">edit</a></span>
                </th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Organization Type:</td>
                <td>
                    <?
                    foreach ($pref_orgs as $key => $org) {
                        if ($key == 0) {
                            echo $org['business_name'];
                        } else {
                            echo ', ' . $org['business_name'];
                        }
                    }
                    ?>
                </td>
            </tr>       

        </tbody>
    </table>
    <br />
    <table class="form-table" style="width: 147%">
        <thead>
            <tr>
                <th colspan="2">Other Relevant Information
                    <span style="float: right; margin-right: 5px;">
                        <a href="javascript:edit_relevant(<?= $personal['id'] ?>)">edit</a></span>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Career Summary:</td>
                <td><?= isset($summary['career_summary']) ? $summary['career_summary'] : '' ?></td>
            </tr> 
            <tr>
                <td class="right">Special Qualification:</td>
                <td><?= isset($summary['special_qualification']) ? $summary['special_qualification'] : '' ?></td>
            </tr>  
            <tr>
                <td class="right">Keyword:</td>
                <td><?= isset($summary['keywords']) ? $summary['keywords'] : '' ?></td>
            </tr>         
        </tbody>
    </table>
</div>
<div id="edit_diag"></div>
<div id="edit_diag_career"></div>
<div id="edit_diag_category"></div>
<div id="edit_diag_location"></div>
<div id="edit_diag_organization"></div>
<div id="edit_diag_relevant"></div>