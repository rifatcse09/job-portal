<div id="edit_language">
    <h3>Entry form - Language</h3>
    <form action="<?= site_url()?>/other_quality/edit_language" method="post" name="language_form" id="education_form">
        <table>
            <thead>
            <th>Language</th>
            <th>Reading</th>
            <th>Writing</th>
            <th>Speaking</th>
            </thead>
            <tbody>
                <?
                for ($i = 0; $i < 3; $i++) {
                    ?>
                    <tr>
                        <td>
                            <input type="text" name="language_edit[]" id="language<?= $i ?>"/>
                        </td>
                        <td>
                            <select name="reading_edit[]" id="reading<?= $i ?>">
                                <option value="High">High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                            </select>
                        </td>
                        <td>
                            <select name="writing_edit[]" id="writing<?= $i ?>">
                                <option value="High">High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                            </select>
                        </td>
                        <td>
                            <select name="speaking_edit[]" id="speaking<?= $i ?>">
                                <option value="High">High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                            </select>
                        </td>
                    </tr>
                    <?
                }
                ?>
                    <tr>
                        <td colspan="2">
                            <input type="submit" name="lang_submit" id="lang_submit" value="Submit" />
                        </td>
                    </tr>
            </tbody>
        </table>
    </form>
</div>