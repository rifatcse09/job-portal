<link rel="stylesheet" type="text/css" href="<?= base_url() ?>stylesheets/jquery-ui-1.8.18.custom.css" />
<script type="text/javascript" src="<?= base_url() ?>JS/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/jquery-ui-1.8.11.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/common_ui.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/Others/validate.js"></script>
<script type="text/javascript">
    var base_url = '<?= site_url() ?>';
    $(document).ready(function(){
        dialog_open("#add_employment", "Experience entry");
        dialog_open("#edit_diag", "Edit Experience");
        $("#add_employment_btn").click(function(){
            $("#add_employment").dialog('open');
        });
        
        $("#emp_from, #emp_to").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '-60',
            dateFormat: 'yy-mm-dd'
        });
    });
</script>
<style>
        
        

.myButton {
	-moz-box-shadow: 0px 0px 0px 2px #9fb4f2;
	-webkit-box-shadow: 0px 0px 0px 2px #9fb4f2;
	box-shadow: 0px 0px 0px 2px #9fb4f2;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #7892c2), color-stop(1, #476e9e));
	background:-moz-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:-webkit-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:-o-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:-ms-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:linear-gradient(to bottom, #7892c2 5%, #476e9e 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#7892c2', endColorstr='#476e9e',GradientType=0);
	background-color:#7892c2;
	-moz-border-radius:10px;
	-webkit-border-radius:10px;
	border-radius:10px;
	border:1px solid #4e6096;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:arial;
	font-size:19px;
	padding:12px 37px;
	text-decoration:none;
	text-shadow:0px 1px 0px #283966;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #476e9e), color-stop(1, #7892c2));
	background:-moz-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:-webkit-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:-o-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:-ms-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:linear-gradient(to bottom, #476e9e 5%, #7892c2 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#476e9e', endColorstr='#7892c2',GradientType=0);
	background-color:#476e9e;
}
.myButton:active {
	position:relative;
	top:1px;
}

        </style>
<?= $this->load->view("editresume/content_top_navigation"); ?>
<div id="main-content" style="width: 147%; height: auto">
    <div style="border: 3px solid #4e6096;margin-top: 100px; ">
<div style="border-bottom: 1px solid #4e6096;margin-bottom: 15px">
        <h3 style="padding-bottom: 10px">Employment History</h3>
    </div>
            <?
    if (isset($employments)) {
        if (count($employments) > 0) {
            foreach ($employments as $k => $emp) {
                ?>
                <table class="form-table" style="width: 147%">
                    <thead style="background-color:#08233F">
                        <tr>
                            <th colspan="2"><u>Experience-<?= $k + 1 ?></u>
                    <span style="float: right; margin-right: 5px;">
                        <a href="javascript:edit_experience(<?= $emp['id'] ?>)">edit</a></span>
                    </th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Company Name :</td>
                            <td><?= $emp['company_name'] ?></td>
                        </tr>
                        <tr>
                            <td>Company Business :</td>
                            <td><?= $emp['company_business'] ?></td>
                        </tr>
                        <tr>
                            <td>Company Location :</td>
                            <td><?= $emp['company_location'] ?></td>
                        </tr>
                        <tr>
                            <td>Department :</td>
                            <td><?= $emp['department'] ?></td>
                        </tr>
                        <tr>
                            <td>Position Held :</td>
                            <td><?= $emp['position_held'] ?></td>
                        </tr>
                        <tr>
                            <td>Area of Experience :</td>
                            <td><?= $emp['area_of_experience'] ?></td>
                        </tr>
                        <tr>
                            <td>Responsibilities :</td>
                            <td><?= $emp['responsibilities'] ?></td>
                        </tr>
                        <tr>
                            <td>From :</td>
                            <td><?= Date('d F, Y', strtotime($emp['from'])) ?></td>
                        </tr>
                        <tr>
                            <td>To :</td>
                            <td>
                                <?
                                if (($emp['continuing']) == 'y')
                                    echo 'present';
                                else
                                    echo Date('d F, Y', strtotime($emp['to']));
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?
            }
        } else {
            echo '<span style="padding: 20px; margin-top:10px; margin-bottom;10px">No data found. Please click on button bellow to add new employment.</span>';
        }
    }
    ?>
    <div style="margin-bottom: 15px;margin-top: 15px"><button id="add_employment_btn" value="add education" class="myButton">add Experience</button>
        <br/><br/>
    </div>
</div>
</div>
<? $this->load->view('editresume/add_employment'); ?>
<div id="edit_diag">
</div>