<script type="text/javascript" src="<?= base_url() ?>JS/career/career_validation.js"></script>
<form action="<?= site_url() ?>/cont_personal/edit_relevant" method="post" name="career_form" id="career_form" onsubmit="return validate_career_edit()">
    <input type='hidden' name="personal_id" value="<?= $id ?>"/>
    <?
    if (isset($relevant)) {
        ?>
    <table>    
    <tbody>
            <tr>
                <td class="right">Career Summary:</td>
                <td>
                    <input type="text" name="career_summary" id="career_summary" value="<?= $relevant['career_summary'] ?>"/>
                </td>
            </tr>
            <tr>
                <td class="right">Special Qualification:</td>
                <td>
                    <input type="text" name="qualification" id="qualification" value="<?= $relevant['special_qualification'] ?>"/>
                </td>
            </tr>
            <tr>
                <td class="right">Keyword:</td>
                <td>
                    <input type="text" name="keywords" id="keywords" value="<?= $relevant['keywords'] ?>"/>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <input type="submit" name="submit_relevant" value="Submit Changes"/>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
    <?
} else {
    echo 'No data found. Please try again.';
}
?>