<script type="text/javascript" src="<?= base_url() ?>JS/career/career_validation.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/personal_entry_form.js"></script>
<form method="Post" action="<?= site_url() ?>/cont_personal/edit_category">
    <input type="hidden" name="personal_id" value="<?= $id?>"/>
    <table class="form-table" style="width: 80%">
        <thead>
            <tr>
                <th colspan="3">Edit-Preferred Job Category</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="left">

                    <? $this->load->view('common/category_list', $categories) ?>

                    <input id="selected_Cat" type="hidden" value="<?= $cat_ids ?>" name="selected_Cat" />
                </td>
                <td>
                    <input id="cmdAdd_WorkArea" type="button" name="cmdAdd_WorkArea" value="Add >" onclick="append('preferred_category','lstSelectedWorkArea','selected_Cat','2','You cannot add more than 2 working sector!');" />
                    <br />
                    <input id="cmdRemove_WorkArea" type="button" value="< Remove" onclick="remove('lstSelectedWorkArea','selected_Cat');" name="cmdRemove_WorkArea" /> 
                </td>
                <td>
                    <select id="lstSelectedWorkArea" style="width:180px;" size="5" name="lstSelectedWorkArea">
                        <?
                        foreach ($user_categories as $cat) {
                            echo '<option value="' . $cat['category_id'] . '">' . $cat['category_name'] . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>   
            <tr>
                <td colspan="4">
                    <input type="submit" name="name_category" value="Submit Changes"/>
                </td>
            </tr>
        </tbody>
    </table>
</form>