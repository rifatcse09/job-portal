<h3>Entry form - Add Reference</h3>
<script type="text/javascript" src="<?= base_url()?>JS/others/validate.js">
    
</script>
<form action="<?= site_url() ?>/other_quality/edit_reference" method="post" name="reference_form" id="reference_form" onsubmit="return validate_reference_edit()">
    <input type='hidden' name="reference_id" value="<?= $id ?>"/>
    <?
    if (isset($reference)) {
        ?>
        <table class="form-table" style="width: 90%">
            <thead>
                <tr>
                    <th colspan="2"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="right">Name:</td>
                    <td>
                        <input type="text" name="ref_name_edit" id="ref_name_edit" value="<?= $reference['ref_name'] ?>"/>
                        <span>*</span>
                    </td>
                </tr>
                <tr>
                    <td class="right">Organization:</td>
                    <td>
                        <input type="text" name="ref_org_edit" id="ref_org_edit" value="<?= $reference['organization'] ?>" />
                        <span>*</span>
                    </td>
                </tr>
                <tr>
                    <td class="right">Designation</td>
                    <td>
                        <input type="text" name="ref_designation_edit" id="ref_designation_edit" value="<?= $reference['designation'] ?>" />
                        <span>*</span>
                    </td>
                </tr>
                <tr>
                    <td class="right">Address</td>
                    <td>
                        <textarea name="ref_address_edit" id="ref_address"><?= $reference['address'] ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="right">Phone(off)</td>
                    <td>
                        <input type="text" name="ref_off_phone_edit" id="ref_off_phone" value="<?= $reference['phone_office'] ?>" />
                    </td>
                </tr>
                <tr>
                    <td class="right">Phone(Res):</td>
                    <td>
                        <input type="text" name="ref_res_phone_edit" id="ref_res_phone" value="<?= $reference['phone_resident'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="right">Mobile:</td>
                    <td>
                        <input type="text" name="ref_mobile_edit" id="ref_mobile" value="<?= $reference['mobile'] ?>" />
                    </td>
                </tr>
                <tr>
                    <td class="right">Email:</td>
                    <td>
                        <input type="text" name="ref_email_edit" id="ref_email" value="<?= $reference['email'] ?>" />
                    </td>
                </tr>
                <tr>
                    <td class="right">Relation:</td>
                    <td>
                        <? $value = $reference['relation']; ?>
                        <select name="relation_edit" id="relation">
                            <option value="relative" <? if ($value == 'relative')
                        echo 'checked="checked"'; ?>>Relative</option>
                            <option value="family friend" <? if ($value == 'family friend')
                                    echo 'checked="checked"'; ?>>Family Friend</option>
                            <option value="academic" <? if ($value == 'academic')
                                    echo 'checked="checked"'; ?>>Academic</option>
                            <option value="professional" <? if ($value == 'professional')
                                    echo 'checked="checked"'; ?>>Professional</option>
                            <option value="others" <? if ($value == 'others')
                                    echo 'checked="checked"'; ?>>Others</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" name="ref_submit" id="ref_submit" value="Submit" />
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    <?
} else {
    echo 'No data found. Please try again.';
}
?>
