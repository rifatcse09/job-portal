    <link rel="stylesheet" type="text/css" href="<?=base_url()?>stylesheets/jquery-ui-1.8.18.custom.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>stylesheets/button.css" />
    <script type="text/javascript" src="<?=  base_url()?>JS/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?=  base_url()?>JS/jquery-ui-1.8.11.min.js"></script>
    <script type="text/javascript" src="<?=  base_url()?>JS/common_ui.js"></script>
    <script type="text/javascript" src="<?=  base_url()?>JS/Education/validate.js"></script>
    <style>
        
        

.myButton {
	-moz-box-shadow: 0px 0px 0px 2px #9fb4f2;
	-webkit-box-shadow: 0px 0px 0px 2px #9fb4f2;
	box-shadow: 0px 0px 0px 2px #9fb4f2;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #7892c2), color-stop(1, #476e9e));
	background:-moz-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:-webkit-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:-o-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:-ms-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:linear-gradient(to bottom, #7892c2 5%, #476e9e 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#7892c2', endColorstr='#476e9e',GradientType=0);
	background-color:#7892c2;
	-moz-border-radius:10px;
	-webkit-border-radius:10px;
	border-radius:10px;
	border:1px solid #4e6096;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:arial;
	font-size:19px;
	padding:12px 37px;
	text-decoration:none;
	text-shadow:0px 1px 0px #283966;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #476e9e), color-stop(1, #7892c2));
	background:-moz-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:-webkit-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:-o-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:-ms-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:linear-gradient(to bottom, #476e9e 5%, #7892c2 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#476e9e', endColorstr='#7892c2',GradientType=0);
	background-color:#476e9e;
}
.myButton:active {
	position:relative;
	top:1px;
}

        </style>
<script type="text/javascript">
    var base_url = '<?= site_url() ?>';
    $(document).ready(function(){
        dialog_open("#add_education", "Education entry");
        dialog_open("#add_training", "Training Summary entry");
        dialog_open("#add_professional_qualification", "Professional Qualification Entry");
        dialog_open("#edit_diag", "Edit Experience");
        $("#add_education_btn").click(function(){
            $("#add_education").dialog('open');
        });
        $("#add_training_btn").click(function(){
            $("#add_training").dialog('open');
        });
        $("#add_prof_btn").click(function(){
            $("#add_professional_qualification").dialog('open');
        });
        $("#prof_from, #prof_to").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '-60',
            dateFormat: 'yy-mm-dd'
        });
    });
</script>

<?= $this->load->view("editresume/content_top_navigation"); ?>
<div id="main-content" style="height: auto;  width:147%">
   <div style="margin-top:20px">
    <h3 >Academic Qualification</h3>
    <?
        if(count($educational_qualifications)){
            foreach($educational_qualifications as $key => $edu){
    ?>
    
    <table class="form-table" style="padding: 5px">
        <thead style="background-color:#08233F">
            <tr>
                <th colspan="2">
                    <u >Academic Qualification <?= $key + 1?></u>
                    <span style="float: right; margin-right: 5px;">
                        <a href="javascript:edit_education(<?= $edu['id'] ?>)">edit</a></span>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Level of Education</td>
                <td><?= $edu['level_name']?></td>
            </tr>
            <tr>
                <td>Exam/Degree Title</td>
                <td><?= $edu['degree_title']?></td>
            </tr>
            <tr>
                <td>Concentration/Major/Group</td>
                <td><?= $edu['major']?></td>
            </tr>
            <tr>
                <td>Institute</td>
                <td><?= $edu['institute']?></td>
            </tr>
            <tr>
                <td>Result</td>
                <td><?= $edu['result']?></td>
            </tr>
            <tr>
                <td>Year of Passing</td>
                <td><?= $edu['passing_year']?></td>
            </tr>
            <tr>
                <td>Duration</td>
                <td><?= $edu['duration']?></td>
            </tr>
            <tr>
                <td>Achievement</td>
                <td><?= $edu['achievement']?></td>
            </tr>
        </tbody>
    </table>
    <?
            }
    }
    else{
        echo '<div><span>no data exist. please add education qualification by clicking button bellow.</span></div>';
    }
    ?>
    <div style="margin-bottom: 15px;  margin-top: 15px"><button id="add_education_btn" value="add education" class="myButton">add education</button>
        <br/><br/>
    </div>
    </div>
    <!-- training summary -->
    <div style="border: 3px solid #4e6096; ">
    <div style="border-bottom: 1px solid #4e6096 ">
    <h3 style=" padding-bottom: 10px" >Training Summary</h3>
    </div>
        <?
        if(count($training_summaries)){
            foreach($training_summaries as $key => $train){
    ?>
    
    <table class="form-table" >
        <thead>
            <tr>
                    <th colspan="2"><u>Training <?= $key + 1?></u></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Training Title</td>
                <td><?= $train['training_title']?></td>
            </tr>
            <tr>
                <td>Topics Covered</td>
                <td><?= $train['topics_covered']?></td>
            </tr>
            <tr>
                <td>Institute</td>
                <td><?= $train['institute']?></td>
            </tr>
            <tr>
                <td>Country</td>
                <td><?= $train['country']?></td>
            </tr>
            <tr>
                <td>Location</td>
                <td><?= $train['location']?></td>
            </tr>
            <tr>
                <td>Training year</td>
                <td><?= $train['year']?></td>
            </tr>
            <tr>
                <td>Duration</td>
                <td><?= $train['duration']?></td>
            </tr>
        </tbody>
    </table>
    <?
            }
        }
        else{
            echo '<div style="padding: 20px;" ><span>no data exist. please add training summary by clicking button bellow.</span></div>';
        }
    ?>
    <div style="margin-bottom: 15px;margin-top: 15px;"><button id="add_training_btn" value="add training" class="myButton">add training</button>
        <br/><br/>
    </div>
    </div>
    <div style="border: 3px solid #4e6096;  margin-top: 5px">
        
    <h3 style="padding: 15px; border-bottom: 1px solid #4e6096 ">Professional Qualification</h3>
    <?
        if(count($professional_qualifications)){
            foreach($professional_qualifications as $key => $pq){
    ?>
    
    <table class="form-table" >
        <thead>
            <tr>
                <th colspan="2"><u>Qualification - <?= $key + 1?></u></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Certification</td>
                <td><?= $pq['certification']?></td>
            </tr>
            <tr>
                <td>Institute</td>
                <td><?= $pq['institute']?></td>
            </tr>
            <tr>
                <td>Location</td>
                <td><?= $pq['location']?></td>
            </tr>
            <tr>
                <td>From: <?= date('d F, Y', strtotime($pq['from']))?></td>
                <td>To: <?= date('d F, Y', strtotime($pq['to']))?></td>
            </tr>
        </tbody>
    </table>
    <?
            }
        }
        else{
            echo '<div style="padding:20px"><span>no data exist. please add professional qualification by clicking button bellow.</span></div>';
        }
    ?>
    <div style="margin-bottom: 15px;"><button id="add_prof_btn" value="add training" class="myButton">add qualification</button>
        <br/><br/>
    </div>
    </div>
</div>
    
<? $this->load->view('editresume/add_education', $education_level) ?>
<? $this->load->view('editresume/add_training_summary') ?>
<? $this->load->view('editresume/add_professional_qualification') ?>
<div id="edit_diag">
    
</div>