<div id="add_training">
    <h3>Entry form - Training Summary</h3>
    <form action="<?= site_url()?>/cont_training/create_training_summary" method="post" name="training_sum_form" id="training_sum_form" onsubmit="return validate_training_sum()">
    <table class="form-table" style="width: 90%">
        <thead>
            <tr>
                <th colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Training Title</td>
                <td>
                    <input type="text" name="training_title" id="training_title"/>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Topics Covered</td>
                <td>
                    <textarea name="topics_covered" id="topics_covered" cols="40" rows="2" style="min-width: 300px; max-width: 300px; max-height: 40px;">
                    
                    </textarea>
                </td>
            </tr>
            <tr>
                <td class="right">Institute</td>
                <td>
                    <input type="text" name="train_institute" id="train_institute" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Country</td>
                <td>
                    <input type="text" name="train_country" id="train_country" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Location</td>
                <td>
                    <input type="text" name="train_location" id="train_location" />
                </td>
            </tr>
            <tr>
                <td class="right">Year</td>
                <td>
                    <select id="year_training" name="year_training">
                        <option value="-1">select</option>
                    <?
                        $year = date('Y');
                        for($i = $year + 4;$i >= 1950;$i--){
                            echo '<option value="' . $i . '">' . $i . '</option>';
                        }
                    ?>
                    </select>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Duration</td>
                <td>
                    <input type="text" name="train_duration" id="train_duration" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="training_submit" id="training_submit" value="Submit" />
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</div>
