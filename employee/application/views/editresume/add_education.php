<div id="add_education">
    <h3>Entry form - Academic Qualification</h3>
    <form action="<?= site_url()?>/cont_education/create_education" method="post" name="education_form" id="education_form" onsubmit="return validate_education()">
    <table class="form-table" style="width: 90%">
        <thead>
            <tr>
                <th colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Level of Education</td>
                <td>
                    <?
                        $data['levels'] = $levels;
                        $this->load->view('education/education_level_list', $education_level);
                    ?>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Exam/Degree Title</td>
                <td>
                    <input type="text" name="exam_title" id="exam_title" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Concentration/Major/Group</td>
                <td>
                    <input type="text" name="major" id="major" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Institute</td>
                <td>
                    <input type="text" name="institute" id="institute" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Result</td>
                <td>
                    <input type="text" name="result" id="result" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Year of Passing</td>
                <td>
                    <select id="year_passing" name="year_passing">
                        <option value="-1">select</option>
                    <?
                        $year = date('Y');
                        for($i = $year + 4;$i >= 1950;$i--){
                            echo '<option value="' . $i . '">' . $i . '</option>';
                        }
                    ?>
                    </select>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Duration</td>
                <td>
                    <input type="text" name="duration" id="duration" />
                </td>
            </tr>
            <tr>
                <td class="right">Achievement</td>
                <td>
                    <input type="text" name="achievement" id="achievement" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="submit" id="submit" value="Submit" />
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</div>
