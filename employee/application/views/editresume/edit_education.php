<script type="text/javascript" src="<?= base_url()?>JS/Education/validate.js"></script>

<?
    if(isset($education)){
?>
<form action="<?= site_url()?>/cont_education/edit_education" method="post" name="education_form" id="education_form" onsubmit="return validate_education_edit()">
    <input type="hidden" name="id" value="<?= $id?>"/>
    <table class="form-table" style="width: 90%">
        <thead>
            <tr>
                <th colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Level of Education</td>
                <td>
                    <?
                        $education_level['level_id'] = $education['level_id'];
                        $this->load->view('education/education_level_list', $education_level);
                    ?>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Exam/Degree Title</td>
                <td>
                    <input type="text" name="exam_title" id="exam_title_edit" value="<?= $education['degree_title']?>" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Concentration/Major/Group</td>
                <td>
                    <input type="text" name="major" id="major_edit" value="<?= $education['major']?>" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Institute</td>
                <td>
                    <input type="text" name="institute" id="institute_edit" value="<?= $education['institute']?>" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Result</td>
                <td>
                    <input type="text" name="result" id="result_edit" value="<?= $education['result']?>" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Year of Passing</td>
                <td>
                    <select id="year_passing" name="year_passing">
                        <option value="-1">select</option>
                    <?
                        $year = date('Y');
                        for($i = $year + 4;$i >= 1950;$i--){
                            $select = "";
                            if($i == $education['passing_year'])
                                $select = "selected='selected'";
                            echo '<option value="' . $i . '" '. $select . '>' . $i . '</option>';
                        }
                    ?>
                    </select>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Duration</td>
                <td>
                    <input type="text" name="duration" id="duration_edit" value="<?= $education['duration']?>" />
                </td>
            </tr>
            <tr>
                <td class="right">Achievement</td>
                <td>
                    <input type="text" name="achievement" id="achievement_edit" value="<?= $education['achievement']?>" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="submit" id="submit" value="Submit" />
                </td>
            </tr>
        </tbody>
    </table>
    </form>
<?
    }else{
        echo 'No data found. Please try again.';
    }
?>