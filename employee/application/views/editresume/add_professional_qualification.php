<div id="add_professional_qualification">
    <h3>Entry form - Professional Qualification</h3>
    <form action="<?= site_url()?>/cont_education/create_professional_qualification" method="post" name="prof_qual_form" id="prof_qual_form" onsubmit="return validate_prof_qual()">
    <table class="form-table" style="width: 90%">
        <thead>
            <tr>
                <th colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Certification</td>
                <td>
                    <input type="text" name="prof_certification" id="prof_certification"/>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Institute</td>
                <td>
                    <input type="text" name="prof_institute" id="prof_institute"/>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Location</td>
                <td>
                    <input type="text" name="prof_location" id="prof_location" />
                </td>
            </tr>
            <tr>
                <td class="right">From
                    <input type="text" name="prof_from" id="prof_from" />
                    <span>*</span>
                </td>
            
                <td class="right">To
                    <input type="text" name="prof_to" id="prof_to" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="prof_submit_btn" id="prof_submit_btn" value="Submit" />
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</div>
