<div id="add_reference">
    <h3>Entry form - Add Reference</h3>
    <form action="<?= site_url()?>/other_quality/create_reference" method="post" name="reference_form" id="reference_form" onsubmit="return validate_reference()">
    <table class="form-table" style="width: 90%">
        <thead>
            <tr>
                <th colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Name:</td>
                <td>
                    <input type="text" name="ref_name" id="ref_name"/>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Organization:</td>
                <td>
                    <input type="text" name="ref_org" id="ref_org" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Designation</td>
                <td>
                    <input type="text" name="ref_designation" id="ref_designation" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Address</td>
                <td>
                    <input type="text" name="ref_address" id="ref_address" />
                </td>
            </tr>
            <tr>
                <td class="right">Phone(off)</td>
                <td>
                    <input type="text" name="ref_off_phone" id="ref_off_phone" />
                </td>
            </tr>
            <tr>
                <td class="right">Phone(Res):</td>
                <td>
                    <input type="text" name="ref_res_phone" id="ref_res_phone"/>
                </td>
            </tr>
            <tr>
                <td class="right">Mobile:</td>
                <td>
                    <input type="text" name="ref_mobile" id="ref_mobile" />
                </td>
            </tr>
            <tr>
                <td class="right">Email:</td>
                <td>
                    <input type="text" name="ref_email" id="ref_email" />
                </td>
            </tr>
            <tr>
                <td class="right">Relation:</td>
                <td>
                    <select name="relation" id="relation">
                        <option value="relative">Relative</option>
                        <option value="family friend">Family Friend</option>
                        <option value="academic">Academic</option>
                        <option value="professional">Professional</option>
                        <option value="others">Others</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="ref_submit" id="ref_submit" value="Submit" />
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</div>
