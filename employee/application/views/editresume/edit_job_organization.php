<script type="text/javascript" src="<?= base_url() ?>JS/career/career_validation.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/personal_entry_form.js"></script>
<form method="Post" action="<?= site_url() ?>/cont_personal/edit_organization">
    <input type="hidden" name="personal_id" value="<?= $id?>"/>
    <table class="form-table" style="width: 80%">
        <thead>
            <tr>
                <th colspan="3">Edit-Organization-Type</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="left">
                    <select id="organization_category" style="width:180px;" size="5" name="organization_category">
                        <?php
                        if (isset($business_areas)) {
                            foreach ($business_areas as $v) {
                                echo '<option value="' . $v['business_id'] . '">' . $v['business_name'] . '</option>';
                            }
                        }
					?>
					</select>
                   <input id="selected_org" type="hidden" value="<?= $org_ids ?>" name="selected_org" />
                </td>
                <td>
                    <input id="cmdAdd_org" type="button" name="cmdAdd_org" value="Add >" onclick="append('organization_category','lstSelectedOrg','selected_org','2','You cannot add more than 2 organization!');" />
                    <br />
                    <input id="cmdRemove_Org" type="button" value="< Remove" onclick="remove('lstSelectedOrg','selected_org');" name="cmdRemove_Org" /> 
                </td>
                <td>
                    <select id="lstSelectedOrg" style="width:180px;" size="5" name="lstSelectedOrg">
                        <?
                        foreach ($organization as $org) {
                            echo '<option value="' . $org['business_id'] . '">' . $org['business_name'] . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>   
            <tr>
                <td colspan="4">
                    <input type="submit" name="name_organization" value="Submit Changes"/>
                </td>
            </tr>
        </tbody>
    </table>
</form>