
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>stylesheets/jquery-ui-1.8.18.custom.css" />
<script type="text/javascript" src="<?= base_url() ?>JS/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/jquery-ui-1.8.11.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/common_ui.js"></script>
<script type="text/javascript" src="<?= base_url() ?>JS/Others/validate.js"></script>
<script type="text/javascript">
    var base_url = '<?= site_url()?>';
    $(document).ready(function(){
        dialog_open("#add_language", "Language entry");
        dialog_open("#edit_diag", "Language edit");
        dialog_open("#add_reference", "Reference entry");
        $("#add_language_btn").click(function(){
            $("#add_language").dialog('open');
        });
        $("#edit_language_btn").click(function(){
            edit_language();
        });
        $("#add_reference_btn").click(function(){
            $("#add_reference").dialog('open');
        });
    });
</script>
   <style>
        
        

.myButton {
	-moz-box-shadow: 0px 0px 0px 2px #9fb4f2;
	-webkit-box-shadow: 0px 0px 0px 2px #9fb4f2;
	box-shadow: 0px 0px 0px 2px #9fb4f2;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #7892c2), color-stop(1, #476e9e));
	background:-moz-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:-webkit-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:-o-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:-ms-linear-gradient(top, #7892c2 5%, #476e9e 100%);
	background:linear-gradient(to bottom, #7892c2 5%, #476e9e 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#7892c2', endColorstr='#476e9e',GradientType=0);
	background-color:#7892c2;
	-moz-border-radius:10px;
	-webkit-border-radius:10px;
	border-radius:10px;
	border:1px solid #4e6096;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:arial;
	font-size:19px;
	padding:12px 37px;
	text-decoration:none;
	text-shadow:0px 1px 0px #283966;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #476e9e), color-stop(1, #7892c2));
	background:-moz-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:-webkit-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:-o-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:-ms-linear-gradient(top, #476e9e 5%, #7892c2 100%);
	background:linear-gradient(to bottom, #476e9e 5%, #7892c2 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#476e9e', endColorstr='#7892c2',GradientType=0);
	background-color:#476e9e;
}
.myButton:active {
	position:relative;
	top:1px;
}

        </style>

<?= $this->load->view("editresume/content_top_navigation"); ?>
<div id="main-content" style="width: 147%; height: auto">
   <!-- <h3 style="margin-left: 250px;">Specialization</h3>

    <table class="form-table" width="147%">
        <thead>
            <tr>
                <th>Fields of Specialization</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <h3 style="margin-left: 250px;">Extra Curricular Activities</h3>
    <div></div>--->
    <h3 style="margin: 10px">Language Proficiency</h3>
    <?
    if ($languages) {
        if (count($languages) > 0) {
            ?>
            <table class="form-table" style="">
                <thead>
                    <tr>
                        <th>Language</th>
                        <th>Reading</th>
                        <th>Writing</th>
                        <th>Speaking</th>
                    </tr>
                </thead>
                <tbody>
                    <? foreach ($languages as $lang) { ?>
                        <tr>
                            <td><?= $lang['language_name'] ?></td>
                            <td><?= $lang['reading'] ?></td>
                            <td><?= $lang['writing'] ?></td>
                            <td><?= $lang['speaking'] ?></td>
                        </tr>
                    <? } ?>
                </tbody>
            </table>
            <?
        } else {
            echo 'no language specified. Please click on button bellow for adding language skill.';
        }
    }
    ?>
    <div style="margin-bottom: 15px; margin-top: 20px;padding: 10px"><button id="add_language_btn" value="add language" class="myButton">add language</button>
        <button id="edit_language_btn" value="edit language" class="myButton">edit language</button>
        <br/><br/>
    </div>
    <div style="border: 3px solid #4e6096;">
        <div style="border-bottom: 1px solid #4e6096;margin-bottom: 15px " >
    <h3 style="padding-bottom:5px">References</h3>
    </div>
        <?
    if (isset($references)) {
        if (count($references) > 0) {
            ?>
            <table class="form-table" style="width: 147%">
                <tr>
                    <th>Reference - 1
                        <span style="float: right; margin-right: 5px;">
                        <a href="javascript:edit_reference(<?= $references[0]['id'] ?>)">edit</a></span>
                    </th>
                    <th>Reference - 2
                        <?if (isset($references[1]))?>
                        <span style="float: right; margin-right: 5px;">
                        <? if(isset($references[1])){?>
                            <a href="javascript:edit_reference(<?= $references[1]['id'] ?>)">edit</a></span>
                        <? } else{ ?>
                            edit
                        <? } ?>
                    </th>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr> 
                                <td style="width: 50%">Name :</td>
                                <td><?= $references[0]['ref_name'] ?></td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">organization :</td>
                                <td style="width: 50%"><?= $references[0]['organization'] ?></td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Designation :</td>
                                <td style="width: 50%"><?= $references[0]['designation'] ?></td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Address :</td>
                                <td style="width: 50%"><?= $references[0]['address'] ?></td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Phone(Off) :</td>
                                <td style="width: 50%"><?= $references[0]['phone_office'] ?></td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Phone(Res) :</td>
                                <td style="width: 50%"><?= $references[0]['phone_resident'] ?></td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">mobile :</td>
                                <td style="width: 50%"><?= $references[0]['mobile'] ?></td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Email :</td>
                                <td style="width: 50%"><?= $references[0]['email'] ?></td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Relation :</td>
                                <td style="width: 50%"><?= $references[0]['relation'] ?></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr> 
                                <td style="width: 50%">Name :</td>
                                <td>
                                    <?
                                    if (isset($references[1]['ref_name']))
                                        echo $references[1]['ref_name'];
                                    ?>
                                </td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">organization :</td>
                                <td>
                                    <?
                                    if (isset($references[1]['organization']))
                                        echo $references[1]['organization'];
                                    ?>
                                </td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Designation :</td>
                                <td>
                                    <?
                                    if (isset($references[1]['designation']))
                                        echo $references[1]['designation'];
                                    ?>
                                </td>
                            </tr>
                            <tr> 
                                <td style="width: 50%" >Address :</td>
                                <td>
                                    <?
                                    if (isset($references[1]['address']))
                                        echo $references[1]['address'];
                                    ?>
                                </td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Phone(Off) :</td>
                                <td>
                                    <?
                                    if (isset($references[1]['phone_office']))
                                        echo $references[1]['phone_office'];
                                    ?>
                                </td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Phone(Res) :</td>
                                <td>
                                    <?
                                    if (isset($references[1]['phone_resident']))
                                        echo $references[1]['phone_resident'];
                                    ?>
                                </td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">mobile :</td>
                                <td>
                                    <?
                                    if (isset($references[1]['mobile']))
                                        echo $references[1]['mobile'];
                                    ?>
                                </td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Email :</td>
                                <td>
                                    <?
                                    if (isset($references[1]['email']))
                                        echo $references[1]['email'];
                                    ?>
                                </td>
                            </tr>
                            <tr> 
                                <td style="width: 50%">Relation :</td>
                                <td>
                                    <?
                                    if (isset($references[1]['relation']))
                                        echo $references[1]['relation'];
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <?
        } else {
            echo '<span style="margin-top:10px;margin-bottom:10px">no reference specified. Please click on button bellow for adding a reference.</span>';
        }
    }
    ?>
    
    <div style="margin-bottom: 15px; margin-top: 10px">
        <? if (count($references) < 2) { ?>
            <button id="add_reference_btn" value="add reference" class="myButton">add reference</button><? } ?>
        <br/><br/>
    </div>
    </div>
</div>

<? $this->load->view('editresume/add_language') ?>
<? $this->load->view('editresume/add_reference') ?>
<div id="edit_diag"></div>