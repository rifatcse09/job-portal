<script type="text/javascript" src="<?= base_url() ?>JS/personal_validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#personal_dob").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '-60:-15',
            dateFormat: 'yy-mm-dd'
        });
    })
</script>
<form action="<?= site_url() ?>/cont_personal/edit_personal" method="post" name="personal_form" id="personal_form" onsubmit="return validate_personal_edit()">
    <input type='hidden' name="personal_id" value="<?= $id?>"/>
    <?
    if (isset($personal)) {
        ?>
        <table class="form-table" style="width: 100%">
            <thead>
                <tr>
                    <th colspan="2">Personal Detail Edit
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="right">Name:</td>
                    <td>
                        <input type="text" name="personal_name" id="personal_name_edit" value="<?= $personal['name'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="right">Father's Name:</td>
                    <td>
                        <input type="text" name="personal_father" id="personal_father" value="<?= $personal['father_name'] ?>"/>        
                    </td>
                </tr>
                <tr>
                    <td class="right">Mother's Name:</td>
                    <td>
                        <input type="text" name="personal_mother" id="personal_mother" value="<?= $personal['mother_name'] ?>"/>

                    </td>
                </tr>
                <tr>
                    <td class="right">Date of Birth:</td>
                    <td>
                        <input type="text" readonly name="personal_dob" id="personal_dob" value="<?= date('Y-m-d', strtotime($personal['date_birth'])) ?>"/>        
                    </td>
                </tr>
                <tr>
                    <td class="right">Gender:</td>
                    <td>
                        <?
                        if ($personal['gender'] == 'm')
                            $male = 'selected="selected"';
                        else
                            $female = 'selected="selected"';
                        ?>
                        <select name="personal_gender">
                            <option value="m" <? if (isset($male))
                        echo $male ?>>male</option>
                            <option value="f" <? if (isset($female))
                        echo $female ?>>female</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="right">Marital status:</td>
                    <td>
                        <?
                        if ($personal['marital_status'] == 'm')
                            $married = 'selected="selected"';
                        else
                            $single = 'selected="selected"';
                        ?>
                        <select name="personal_marital_status">
                            <option value="m" <? if (isset($married))
                            echo $married ?>>Married</option>
                            <option value="s" <? if (isset($single))
                            echo $single ?>>Single</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="right">Nationality:</td>
                    <td>
                        <input type="text" name="personal_nationality" id="personal_nationality" value="<?= $personal['nationality'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="right">Religion:</td>
                    <td>
                        <input type="text" name="personal_religion" id="personal_religion" value="<?= $personal['religion'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="right">Present Address:</td>
                    <td>
                        <input type="text" name="personal_present_add" id="personal_present_add" value="<?= $personal['present_add'] ?>" style="width: 380px"/>
                    </td>
                </tr>
                <tr>
                    <td class="right">Permanent Address:</td>
                    <td>
                        <input type="text" name="personal_permanent_add" id="personal_permanent_add" value="<?= $personal['permanent_add'] ?>" style="width: 380px"/>
                    </td>
                </tr>
    <!--                <tr>
                    <td class="right">Current Location:</td>
                    <td><?= $personal['District_Name'] ?></td>
                </tr>-->
                <tr>
                    <td class="right">Contact(home):</td>
                    <td>
                        <input type="text" name="personal_contact_home" id="personal_contact_home" value="<?= $personal['home_phone'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="right">Contact(Mobile):</td>
                    <td>
                        <input type="text" name="personal_contact_mobile" id="personal_contact_mobile" value="<?= $personal['mobile'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="right">Contact(Office):</td>
                    <td>
                        <input type="text" name="personal_contact_office" id="personal_contact_office" value="<?= $personal['office_phone'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="right">Email:</td>
                    <td>
                        <input type="text" name="personal_email" id="personal_email" value="<?= $personal['email'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td class="right">Alternate Email:</td>
                    <td>
                        <input type="text" name="personal_alt_email" id="personal_alt_email" value="<?= $personal['alternate_email'] ?>"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" name="edit_submit" value="submit changes"/>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    <?
}else {
    echo 'No data found. Please try again.';
}
?>