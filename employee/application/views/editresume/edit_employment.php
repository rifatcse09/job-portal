<script type="text/javascript" src="<?= base_url()?>JS/Others/validate.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#emp_from, #emp_to_edit").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '-60',
            dateFormat: 'yy-mm-dd'
        });
    })
</script>

<?
    if(isset($employment)){
?>
<form action="<?= site_url() ?>/cont_employment/edit_employment" method="post" name="employment_form" id="employment_form" onsubmit="return validate_employment_edit()">
    <input type="hidden" name="id" value="<?= $employment['id']?>"/>
    <table class="form-table" style="width: 90%">
        <thead>
            <tr>
                <th colspan="2">Employment Edit</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="right">Company Name:</td>
                <td>
                    <input type="text" name="emp_company" id="emp_company_edit" value="<?= $employment['company_name']?>"/>
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Company Business:</td>
                <td>
                    <input type="text" name="emp_business" id="emp_business_edit" value="<?= $employment['company_business']?>" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Company Location:</td>
                <td>
                    <input type="text" name="emp_location" id="emp_location_edit" value="<?= $employment['company_location']?>" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Department:</td>
                <td>
                    <input type="text" name="emp_department" id="emp_department_edit" value="<?= $employment['department']?>" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Position Held:</td>
                <td>
                    <input type="text" name="emp_position" id="emp_position_edit" value="<?= $employment['position_held']?>" />
                    <span>*</span>
                </td>
            </tr>
            <tr>
                <td class="right">Area of Experience:</td>
                <td>
                    <input type="text" name="emp_area_exp" id="emp_area_exp_edit" value="<?= $employment['area_of_experience']?>"/>
                </td>
            </tr>
            <tr>
                <td class="right">Responsibilities:</td>
                <td>
                    <textarea name="emp_responsibilities" id="emp_responsibilities_edit"><?= $employment['responsibilities']?></textarea>
                </td>
            </tr>
            <tr>
                <td class="right">From:</td>
                <td>
                    <input type="text" name="emp_from" id="emp_from_edit" readonly value="<?= $employment['from']?>" />
                </td>
            </tr>
            <tr>
                <td class="right">To:</td>
                <td>
                    <input type="text" name="emp_to" id="emp_to_edit_edit" readonly value="<? if($employment['continuing'] == 'n') echo $employment['to'];?>" /><br/>
                    <input type="checkbox" name="emp_continue" id="emp_continue_edit" onclick="disable_field('emp_to_edit', this.id) <? if($employment['continuing'] == 'y') echo 'checked';?>"/>continuing
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="emp_submit_edit" id="emp_submit_edit" value="Submit" />
                </td>
            </tr>
        </tbody>
    </table>
</form>
<?
    }else{
        echo 'No data found. Please try again.';
    }
?>