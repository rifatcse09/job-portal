<?= $this->load->view("editresume/content_top_navigation"); ?>
<div id="main-content" style="width: 147%">
    <? $this->load->helper('form')?>
    <h3 style="">Photograph</h3>
    <div style="width: 182px; height: 182px; float: left; margin: 10px;">
        <?
        $img_src = '';    
        if(isset($image)){
                $img_src = $image;
            }
            else{
                $img_src = 'no_image.jpg';
            }
        ?>
        <img src="<?= base_url() . 'photographs/' . $img_src?>" alt="No Photo" width="182px" height="182px" style="border: 5px solid #999; width: 182px; height: 182px;" />
    </div>
    <div style="margin-right: 100px" >
        <?php
        if (isset($error)) {
            foreach ($error as $err) {
                echo $err;
            }
        }
        echo '<h3 style="margin: 15px">Upload your image</h3>';
        echo form_open_multipart('page/upload_photograph');
        ?>
        <input type="file" name="photo_image" value="choose a file"/>
        
            <?
        echo form_submit('mysubmit', 'Submit Image');
        echo form_close();
        ?>        
    </div>
</div>