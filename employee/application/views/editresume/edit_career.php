<script type="text/javascript" src="<?= base_url() ?>JS/career/personal_validation.js"></script>
<form action="<?= site_url() ?>/cont_personal/edit_career" method="post" name="career_form" id="career_form" onsubmit="return validate_career_edit()">
    <input type='hidden' name="personal_id" value="<?= $id ?>"/>
    <?
    if (isset($career)) {
        ?>
    <table>    
    <tbody>
            <tr>
                <td class="right">Career Objective:</td>
                <td>
                    <input type="text" name="career_obj" id="career_obj" value="<?= $career['objective'] ?>"/>
                </td>
            </tr>
            <tr>
                <td class="right">Year of Experience:</td>
                <td>
                    <input type="text" name="career_year_exp" id="career_year_exp" value="<?= $career['year_experience'] ?>"/>
                    <br/>
                    <small>(only numeric value allowed. eg: 1, 2, .5)</small>
                </td>
            </tr>
            <tr>
                <td class="right">Present Salary:</td>
                <td>
                    <input type="text" name="career_pre_salary" id="career_pre_salary" value="<?= $career['present_salary'] ?>"/>
                </td>
            </tr>
            <tr>
                <td class="right">Expected Salary:</td>
                <td>
                    <input type="text" name="career_exp_salary" id="career_exp_salary" value="<?= $career['expected_salary'] ?>"/>
                </td>
            </tr>
            <tr>
                <td class="right">Looking For:</td>
                <td><?
                        $check = 'checked="checked"';
                        $value = $career['looking_for'];
                    ?>
                    <input type="radio" <? if($value == 'Entry') echo $check;?> name="career_level" value="Entry" />Entry Level Job 
                    <input type="radio" <? if($value == 'Mid') echo $check;?> name="career_level" value="Mid" />Mid/Managerial Level Job
                    <input type="radio" <? if($value == 'Top') echo $check;?> name="career_level" value="Top" />Top Level Job
                </td>
            </tr>
            <tr>
                <td class="right">Available For:</td>
                <td>
                    <?
                        $check = 'checked="checked"';
                        $value = $career['available_for'];
                    ?>
                    <input type="radio" <? if($value == 'Full Time') echo $check;?> value="Full Time" name="career_available" />Full Time   
                    <input type="radio" <? if($value == 'Part Time') echo $check;?> value="Part Time" name="career_available" />Part-Time
                    <input type="radio" <? if($value == 'Contract') echo $check;?> value="Contract" name="career_available" />Contract 
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="submit_career" value="Submit Changes"/>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
    <?
} else {
    echo 'No data found. Please try again.';
}
?>