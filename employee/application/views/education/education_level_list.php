<?
$size = isset($level_size) ? $level_size : 1;
$name = isset($level_name) ? $level_name : 'education_level';
$id = isset($level_id) ? $level_id : 'education_level';
?>
<select id="<?= $id ?>" size="<?= $size; ?>" name="<?= $name ?>">
    <?
    $select = "";
    if (isset($level_id))
        $select = 'selected="selected"';
    ?>
    <option <? if ($select != "")
        echo 'selected="selected"' ?> value="-1">select</option>

    <?php
    if ($levels) {
        if (!isset($level_id)) {
            foreach ($levels as $level) {
                echo '<option value="' . $level['level_id'] . '">' . $level['level_name'] . '</option>';
            }
        } else {
            foreach ($levels as $level) {
                $s = "";
                if ($level['level_id'] == $level_id)
                    $s = 'selected="selected"';
                echo '<option value="' . $level['level_id'] . '" ' . $s . '>' . $level['level_name'] . '</option>';
            }
        }
    }
    ?>

</select>