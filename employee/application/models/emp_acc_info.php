<?php
class Emp_acc_info extends CI_Model {
	
    
    function insert_entry()
    {
        $data = array(
		   'username' => $_POST['username'] ,
		   'password' => $_POST['password'],
		   'company_name' => $_POST['company_name'],
		   'alt_company_name' => $_POST['alt_company_name'],
		   'contact_person' => $_POST['contact_person'],
		   'contact_designation' => $_POST['contact_designation'],
		   'business_id' => $_POST['business_id'],
		   'business_description' => $_POST['business_description'],
		   'company_address' => $_POST['company_address'],
		   'country_name' => $_POST['country_name'],
		   'city' => $_POST['city'],
		   'area' => $_POST['area'],
		   'billing_address' => $_POST['billing_address'],
		   'contact_phone' => $_POST['contact_phone'],
		   'contact_email' => $_POST['contact_email'],
		   'website_address' => $_POST['website_address']
		);
        
        $this->db->insert('emp_acc_info', $data);
    }
    
    function insert_entrys()
    {
        $data = array(
		   'name' => $_POST['name'] ,
		   'address' => $_POST['address'],
		   'email_id' => $_POST['email_id'],
		   'phone' => $_POST['phone'],
		   'gender' => $_POST['gender'],
		   'organization' => $_POST['organization'],
		   'designation' => $_POST['designation'],
		   'specialized' => $_POST['specialized']
		);
        
        $this->db->insert('requestregistration', $data);
    }
    
    function get_acc_info($acc_id){
        $q = $this->db->get_where('emp_acc_info', array('id' => $acc_id));
        return $q->row_array();
    }
    
 }
 ?>