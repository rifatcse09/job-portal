<?php

/**
 * Description of Training_Summary
 *
 * @author Anick
 */
class Training_Summary extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function save_training_summary($user_id){
        $data = array(
                'employee_id' => $user_id,
                'training_title' => $_POST['training_title'],
                'topics_covered' => $_POST['topics_covered'],
                'institute' => $_POST['train_institute'],
                'country' => $_POST['train_country'],
                'location' => $_POST['train_location'],
                'year' => $_POST['year_training'],
                'duration' => $_POST['train_duration']
            );
        $this->db->insert('job_training', $data);
    }
    
    function get_training_summary($user_id){
        $q = $this->db->get_where('job_training', array('employee_id' => $user_id));
        return $q->result_array();
    }
}

?>
