<?php

class Job extends CI_Model {

    function job_details($id) {
        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
        $this->db->join('job_category', 'job_category.category_id = emp_new_job.category_id');

        $this->db->where('id', $id);
    
        $query = $this->db->get();
        return $query->row_array();
    }

	//test

	function get_company_job_info($job_id) {
        $this->db->select('company_id, email_id, contact_email, emp_new_job.company_name');
        $this->db->from('emp_new_job');
        $this->db->join('emp_acc_info', 'emp_new_job.company_id = emp_acc_info.id');
        $this->db->where('emp_new_job.id', $job_id);

        $q = $this->db->get();

        return $q->result_array();
    }
    
     function applied_job($id) {
        $query = $this->db->query("SELECT distinct emp_new_job.company_name,emp_new_job.job_title,job_application.is_viewed FROM job_application join emp_new_job WHERE job_application.job_id=emp_new_job.id and job_application.emp_id= ".$id);
        return $query->result();
    }

    function create_job_application($job_id, $emp_id) {
        $q = $this->db->get_where('job_application', array('job_id' => $job_id, 'emp_id' => $emp_id));
        if ($q->num_rows() < 1) {
            $data['job_id'] = $job_id;
            $data['emp_id'] = $emp_id;
            $data['is_viewed'] = 0;
            $this->db->insert('job_application', $data);
        }
    }

	//test
	
	
	
	
    function hire_job_details($id) {
        $query = $this->db->query("Select * from hire_job where id= " . $id);
        return $query->row_array();
    }

    function job_business_type($job_id) {
        $query = $this->db->query("Select emp_business_type.* From emp_business_type JOIN job_to_business_type ON emp_business_type.business_id = job_to_business_type.business_type_id WHERE job_to_business_type.emp_job_id = " . $job_id);
        return $query->result_array();
    }

    function job_business_area($job_id) {
        $query = $this->db->query("Select emp_area.*
                                From emp_area
                                JOIN job_to_experience_area
                                ON emp_area.area_id = job_to_experience_area.experience_area_id
                                WHERE job_to_experience_area.emp_job_id = " . $job_id);
        return $query->result_array();
    }

    function hot_job() {
        
//        $query = $this->db->query("Select emp_new_job.*, company_logo From emp_new_job, emp_acc_info WHERE deadline >= '" . date('Y-m-d') . "' and is_hot_job= 'y' AND emp_new_job.is_active = 1 AND
//                                 emp_new_job.company_id = emp_acc_info.id");
        
        $query = $this->db->query("Select distinct emp_new_job.company_id,emp_new_job.company_name,emp_acc_info.company_logo From emp_new_job, emp_acc_info WHERE deadline >= '" . date('Y-m-d') . "' and is_hot_job= 'y' AND emp_new_job.is_active = 1 AND emp_new_job.company_id = emp_acc_info.id ");
      
        return $query->result_array();
    }

    function hire_job() {
        $query = $this->db->query("select * from hire_job where is_active=1 order by id desc");
        return $query->result_array();
    }

}

?>
