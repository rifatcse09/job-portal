<?php
/**
 * Description of employment
 *
 * @author ANICK
 */
class Employment extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    
    function create_employment($user_id){
        $data['employee_id'] = $user_id;
        $data['company_name'] = $_POST['emp_company'];
        $data['company_business'] = $_POST['emp_business'];
        $data['company_location'] = $_POST['emp_location'];
        $data['department'] = $_POST['emp_department'];
        $data['position_held'] = $_POST['emp_position'];
        $data['responsibilities'] = $_POST['emp_responsibilities'];
        $data['area_of_experience'] = $_POST['emp_area_exp'];
        $data['from'] = date('Y-m-d', strtotime($_POST['emp_from']));
        if(!isset($_POST['emp_continue'])){
            $data['to'] = date('Y-m-d', strtotime($_POST['emp_to']));
            $data['continuing'] = 'n';
        }
        else{
            $data['continuing'] = 'y';
        }
        
        $this->db->insert('job_employment_history', $data);
    }
    
    public function get_all_employments($user_id){
        $q = $this->db->get_where('job_employment_history', array('employee_id' => $user_id));
        return $q->result_array();
    }
    
    public function get_experience($id){
        $q = $this->db->get_where('job_employment_history', array('id' => $id));
        return $q->row_array() ;
    }
    
    function edit_employment($user_id){
        $data['company_name'] = $_POST['emp_company'];
        $data['company_business'] = $_POST['emp_business'];
        $data['company_location'] = $_POST['emp_location'];
        $data['department'] = $_POST['emp_department'];
        $data['position_held'] = $_POST['emp_position'];
        $data['responsibilities'] = $_POST['emp_responsibilities'];
        $data['area_of_experience'] = $_POST['emp_area_exp'];
        $data['from'] = date('Y-m-d', strtotime($_POST['emp_from']));
        if(!isset($_POST['emp_continue'])){
            $data['to'] = date('Y-m-d', strtotime($_POST['emp_to']));
            $data['continuing'] = 'n';
        }
        else{
            $data['continuing'] = 'y';
        }
        
        $this->db->where(array('id' => $_POST['id'], 'employee_id' => $user_id));
        $this->db->update('job_employment_history', $data);
    }
}

?>
