<?php
class Education extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    function get_all_level(){
        $q = $this->db->get_where('job_education_level', array('is_active' => 1));
        return $q->result_array();
    }
    
    function get_all_education($user_id){
        $this->db->select('*');
        $this->db->from('job_education_level');
        $this->db->join('job_academic', 'job_academic.level_id = job_education_level.level_id');
        $this->db->where('employee_id', $user_id);
        
        $q = $this->db->get();
        return $q->result_array();
    }
    
    public function get_education($id){
        $q = $this->db->get_where('job_academic', array('id' => $id));
        return $q->row_array() ;
    }
    
    function save_education($user_id){
        $data = array(
                'employee_id' => $user_id,
                'level_id' => $_POST['education_level'],
                'degree_title' => $_POST['exam_title'],
                'major' => $_POST['major'],
                'institute' => $_POST['institute'],
                'result' => $_POST['result'],
                'passing_year' => $_POST['year_passing'],
                'duration' => $_POST['duration'],
                'achievement' => $_POST['achievement']
            );
        $this->db->insert('job_academic', $data);
    }
    
    function edit_education($user_id){
        $data = array(
                'employee_id' => $user_id,
                'level_id' => $_POST['education_level'],
                'degree_title' => $_POST['exam_title'],
                'major' => $_POST['major'],
                'institute' => $_POST['institute'],
                'result' => $_POST['result'],
                'passing_year' => $_POST['year_passing'],
                'duration' => $_POST['duration'],
                'achievement' => $_POST['achievement']
            );
        $this->db->where(array('id' => $_POST['id'], 'employee_id' => $user_id));
        $this->db->update('job_academic', $data);
    }
}

?>
