<?php

/**
 * Description of User
 *
 * @author Anick
 */
class User extends CI_Model {

    public $id = 0;
    public $name = '';
    public $father_name = '';
    public $mother_name = '';
    public $date_birth = '';
    public $gender = '';
    public $marital_status = '';
    public $nationality = '';
    public $religion = '';
    public $present_add = '';
    public $permanent_add = '';
    public $current_loc = '';
    public $home_phone = '';
    public $mobile = '';
    public $office_phone = '';
    public $email = '';
    public $alternate_email = '';
    public $is_blue_worker = 'n';

    function __construct() {
        parent::__construct();
    }
 
    
    function CreateAccount() {

        $this->name = $_POST['account_name'];
        $this->father_name = (isset($_POST['account_father_name']) && $_POST['account_father_name'] != "") ? $_POST['account_father_name'] : '';
        $this->mother_name = (isset($_POST['account_mother_name']) && $_POST['account_mother_name'] != "") ? $_POST['account_mother_name'] : '';
        $this->date_birth = (isset($_POST['account_dob']) && $_POST['account_dob'] != "") ? $_POST['account_dob'] : NULL;
        $this->gender = (isset($_POST['account_gender']) && $_POST['account_gender'] != "") ? $_POST['account_gender'] : '';
        $this->marital_status = (isset($_POST['account_marital_status']) && $_POST['account_marital_status'] != "") ? $_POST['account_marital_status'] : '';
        $this->nationality = (isset($_POST['account_nationality']) && $_POST['account_nationality'] != "") ? $_POST['account_nationality'] : '';
        $this->religion = (isset($_POST['account_religion']) && $_POST['account_religion'] != "") ? $_POST['account_religion'] : '';
        $this->present_add = (isset($_POST['account_present_address']) && $_POST['account_present_address'] != "") ? $_POST['account_present_address'] : '';
        $this->permanent_add = (isset($_POST['account_permanent_address']) && $_POST['account_permanent_address'] != "") ? $_POST['account_permanent_address'] : '';
        $this->current_loc = (isset($_POST['account_location']) && $_POST['account_location'] != "") ? $_POST['account_location'] : '';
        $this->home_phone = (isset($_POST['account_contact_home']) && $_POST['account_contact_home'] != "") ? $_POST['account_contact_home'] : '';
        $this->mobile = (isset($_POST['account_contact_mobile']) && $_POST['account_contact_mobile'] != "") ? $_POST['account_contact_mobile'] : '';
        $this->office_phone = (isset($_POST['account_contact_office']) && $_POST['account_contact_office'] != "") ? $_POST['account_contact_office'] : '';
        $this->email = (isset($_POST['account_email']) && $_POST['account_email'] != "") ? $_POST['account_email'] : '';
        $this->alternate_email = (isset($_POST['account_alt_email']) && $_POST['account_alt_email'] != "") ? $_POST['account_alt_email'] : '';
        $this->is_blue_worker = (isset($_POST['is_blue_worker']) && $_POST['is_blue_worker'] != "") ? $_POST['is_blue_worker'] : 'n';

        $this->db->insert('job_resume_personal', $this);
        $this->id = $this->db->insert_id(); //get the last insrt id ()
//        $send_by='info@ckrbd.com';
//        $sub='Azadijobs Job Seeker Account Confirmation';
//        $c='';
//        $bc='';
//        $msg='Welcome';
//       //echo $emalia;
//        $this->send_email($emalia,$send_by,$sub,$c,$bc,$msg);
    }
 function send_email($to, $from, $content, $subject, $cc = '', $bcc = '') {
        $this->load->library('email');

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ckrbd.com',
            'smtp_port' => '25',
            'smtp_user' => 'info@ckrbd.com',
            'smtp_pass' => 'pentagems14',
            'mailtype' => 'html',
            'validate'=>'FALSE'
        );

        $this->email->initialize($config);

        $this->email->from($from);
        $this->email->to($to);

        $this->email->subject($subject);
        $this->email->message($content);
        if ($this->email->send()) {
            return true;
        } else {
            //echo $this->email->display_errors();
            return FALSE;
        }
    }


    function CreateEmployeeCategory($emp_id, $cat_ids) {
        if ($cat_ids != '') {
            $d = explode(",", $cat_ids);
            $data = array();
            foreach ($d as $ds) {
                if (!empty($ds)) {
                    $row['employee_id'] = $emp_id;
                    $row['category_id'] = $ds;
                    array_push($data, $row);
                }
            }
            $this->db->insert_batch('job_emp_category', $data);
        }
    }

    function create_employee_preferred_job($emp_id, $job_ids) {
        if ($job_ids != '') {
            $d = explode(",", $job_ids);
            $data = array();
            foreach ($d as $ds) {
                if (!empty($ds)) {
                    $row['employee_id'] = $emp_id;
                    $row['org_id'] = $ds;
                    array_push($data, $row);
                }
            }
            $this->db->insert_batch('job_emp_preferred_org', $data);
        }
    }

    function create_employee_relevant($emp_id) {
        $data = array(
            'emp_id' => $emp_id,
            'special_qualification' => (isset($_POST['special_qualification']) && $_POST['special_qualification'] != "") ? $_POST['special_qualification'] : '',
            'career_summary' => (isset($_POST['career_summary']) && $_POST['career_summary'] != "") ? $_POST['career_summary'] : '',
            'keywords' => (isset($_POST['account_keyword']) && $_POST['account_keyword'] != "") ? $_POST['account_keyword'] : ''
        );
        if ($data['special_qualification'] != '' || $data['career_summary'] != '' || $data['keywords'] != '')
            $this->db->insert('job_resume_relevant', $data);
    }

    function create_employee_location($emp_id, $locations) {
        if ($locations != '') {
            $d = explode(",", $locations);
            $data = array();
            foreach ($d as $ds) {
                if (!empty($ds)) {
                    $row['employee_id'] = $emp_id;
                    $row['location_id'] = $ds;
                    array_push($data, $row);
                }
            }
            $this->db->insert_batch('job_emp_location', $data);
        }
    }

    function create_employee_country($emp_id, $countries) {
        if ($countries != '') {
            $d = explode(",", $countries);
            $data = array();
            foreach ($d as $ds) {
                if (!empty($ds)) {
                    $row['employee_id'] = $emp_id;
                    $row['country_id'] = $ds;
                    array_push($data, $row);
                }
            }
            $this->db->insert_batch('job_emp_country', $data);
        }
    }

    function get_user_personal($user_id) {
        $this->db->select('*');
        $this->db->from('job_resume_personal');
        $this->db->join('district', 'current_loc = District_Id', 'left');
        $this->db->where('id', $user_id);
        $q = $this->db->get();

        return $q->row_array();
    }

    function get_user_career($user_id) {
        $q = $this->db->get_where('job_resume_career', array('user_id' => $user_id));
        return $q->row_array();
    }

    function get_user_preferred_category($user_id) {
        $this->db->select('*');
        $this->db->from('job_emp_category');
        $this->db->join('job_category', 'job_emp_category.category_id = job_category.category_id');
        $this->db->where('job_emp_category.employee_id', $user_id);

        $q = $this->db->get();
        return $q->result_array();
    }

    function get_user_preferred_organization($user_id) {
        $this->db->select('*');
        $this->db->from('job_emp_preferred_org');
        $this->db->join('emp_business_type', 'job_emp_preferred_org.org_id = emp_business_type.business_id');
        $this->db->where('job_emp_preferred_org.employee_id', $user_id);

        $q = $this->db->get();
        return $q->result_array();
    }

    function get_user_preferred_location($user_id) {
        $this->db->select('*');
        $this->db->from('job_emp_location');
        $this->db->join('district', 'job_emp_location.location_id = district.district_id');
        $this->db->where('job_emp_location.employee_id', $user_id);

        $q = $this->db->get();
        return $q->result_array();
    }

    function get_user_preferred_country($user_id) {
        $this->db->select('*');
        $this->db->from('job_emp_country');
        $this->db->join('country', 'job_emp_country.country_id = country.Country_Id');
        $this->db->where('job_emp_country.employee_id', $user_id);

        $q = $this->db->get();
        return $q->result_array();
    }

    function get_user_relevant($user_id) {
        $q = $this->db->get_where('job_resume_relevant', array('emp_id' => $user_id));
        return $q->row_array();
    }

    function get_user_emp_history($user_id) {
        $q = $this->db->get_where('job_employment_history', array('employee_id' => $user_id));
        return $q->result_array();
    }

    function get_acc_qualification($user_id) {
        $this->db->order_by("level_id", "desc");
        $q = $this->db->get_where('job_academic', array('employee_id' => $user_id));
        return $q->result_array();
    }

    function get_language($user_id) {
        $q = $this->db->get_where('job_other_language', array('employee_id' => $user_id));
        return $q->result_array();
    }

    function get_reference($user_id) {
        $q = $this->db->get_where('job_other_reference', array('employee_id' => $user_id));
        return $q->result_array();
    }

    function edit_personal($user_id) {
        $data['name'] = $_POST['personal_name'];
        $data['father_name'] = $_POST['personal_father'];
        $data['mother_name'] = $_POST['personal_mother'];
        $data['date_birth'] = $_POST['personal_dob'];
        $data['gender'] = $_POST['personal_gender'];
        $data['marital_status'] = $_POST['personal_marital_status'];
        $data['nationality'] = $_POST['personal_nationality'];
        $data['religion'] = $_POST['personal_religion'];
        $data['present_add'] = $_POST['personal_present_add'];
        $data['permanent_add'] = $_POST['personal_permanent_add'];
        //$data['current_loc'] = $_POST['personal_location'];
        $data['home_phone'] = $_POST['personal_contact_home'];
        $data['mobile'] = $_POST['personal_contact_mobile'];
        $data['office_phone'] = $_POST['personal_contact_office'];
        $data['email'] = $_POST['personal_email'];
        $data['alternate_email'] = $_POST['personal_alt_email'];

        $this->db->where(array('id' => $_POST['personal_id']));
        $this->db->update('job_resume_personal', $data);
    }

    function update_employee_category($emp_id, $cat_ids) {
        $this->db->where('employee_id', $emp_id);
        $this->db->delete('job_emp_category');
        if (isset($cat_ids) && $cat_ids != '') {
            $d = explode(",", $cat_ids);
            $data = array();
            foreach ($d as $ds) {
                if (!empty($ds)) {
                    $row['employee_id'] = $emp_id;
                    $row['category_id'] = $ds;
                    array_push($data, $row);
                }
            }
            $this->db->insert_batch('job_emp_category', $data);
        }
    }
    function update_employee_organization($emp_id,$org_id)
    {
        
        $this->db->where('employee_id',$emp_id);
        $this->db->delete('job_emp_preferred_org');
        if (isset ($org_id) && $org_id != '') {
            $d= explode(",", $org_id);
            $data = array();
            foreach ($d as $ds){
                if(!empty($ds))
                {
                    $row['employee_id']=$emp_id;
                    $row['org_id']=$ds;
                    array_push($data, $row);
                    
                }
                
            }
            $this->db->insert_batch('job_emp_preferred_org',$data);
        }
        
    }

    function update_employee_location($emp_id, $locations) {
        $this->db->where('employee_id', $emp_id);
        $this->db->delete('job_emp_location');
        if (isset($locations) && $locations != '') {
            $d = explode(",", $locations);
            $data = array();
            foreach ($d as $ds) {
                if (!empty($ds)) {
                    $row['employee_id'] = $emp_id;
                    $row['location_id'] = $ds;
                    array_push($data, $row);
                }
            }
            $this->db->insert_batch('job_emp_location', $data);
        }
    }

    function update_employee_country($emp_id, $countries) {
        $this->db->where('employee_id', $emp_id);
        $this->db->delete('job_emp_country');
        if (isset($countries) && $countries != '') {
            $d = explode(",", $countries);
            $data = array();
            foreach ($d as $ds) {
                if (!empty($ds)) {
                    $row['employee_id'] = $emp_id;
                    $row['country_id'] = $ds;
                    array_push($data, $row);
                }
            }
            $this->db->insert_batch('job_emp_country', $data);
        }
    }

    function update_employee_relevant($emp_id) {
        $data = array(
            'special_qualification' => $_POST['qualification'],
            'career_summary' => $_POST['career_summary'],
            'keywords' => $_POST['keywords']
        );
        $this->db->update('job_resume_relevant', $data, array('emp_id' => $emp_id));
    }

    function update_photograph($user_id, $img) {
        $data['image'] = $img;
        $this->db->where('id', $user_id);
        $this->db->update('job_resume_personal', $data);
    }

    function update_job_application($emp_id, $job_id) {
        $data['is_viewed'] = 1;
        $this->db->where(array('job_id' => $job_id, 'emp_id' => $emp_id));
        $this->db->update('job_application', $data);
    }

}

?>
