<?php
class Category extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    function select_all_category(){
        $query = $this->db->get_where('job_category',array('is_active' => 1));
        return $query->result_array();
    }
    //test
        
    function category_search($cat_id, $level, $location, $nature, $posted_within,$deadline_within, $limit, $start) {
        $offset = 0;
        if ($start > 0)
            $offset = $start . ', ';
		
	//echo date('Y-m-d');
        //echo "that was today's date";
        $date = strtotime('-' . $posted_within . ' day', strtotime(date('Y-m-d')));
        //echo $date;
        $post_date = date('Y-m-d', $date);
     //echo "Then";
        //echo $post_date;
        $date1 = strtotime('+' . $deadline_within . ' day', strtotime(date('Y-m-d')));
	
        $deadline = date('Y-m-d', $date1);
	//echo "again";
        //echo $deadline;
        if($deadline_within=='')$date_query="emp_new_job.deadline >= '" .date('Y-m-d')."'";
		else
		$date_query="emp_new_job.deadline between '".date('Y-m-d'). "' and " . "'$deadline'";
		
		if($posted_within!='')$date_query.="and posting_date >= '$post_date'";
		
        $query = 'SELECT * FROM emp_new_job join job_category on emp_new_job.category_id= job_category.category_id join 
            emp_requirement_s2 join emp_voucher on emp_new_job.id=emp_requirement_s2.emp_job_id and emp_new_job.id = emp_voucher.job_id where '.$date_query. ' and emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 and emp_voucher.is_publish = 1 and
            (is_blue_worker_job != \'y\' OR is_blue_worker_job is null)';
        
		
        if (!empty($cat_id)) {
            $query .= ' and job_category.category_id =  ' . $cat_id;
        }

        if (!empty($level)) {
            $query .= ' and emp_requirement_s2.job_level like \'%' . $level . '%\' ';
        }
		if (!empty($location)) {
            $query .= ' and emp_requirement_s2.job_location = '.$location;
        }
		
        if (!empty($nature)) {
            $query .= ' and emp_requirement_s2.job_type like \'%' . $nature . '%\' ';
        }

        $n = $query;

//        $q = $this->db->query('SELECT * FROM emp_new_job join job_category on emp_new_job.category_id= job_category.category_id join 
//            emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id where job_category.category_id =  ' . $cat_id . '  and emp_new_job.deadline >= \'' . date('Y-m-d') . '\' and 
//                emp_requirement_s2.job_level like \'%' . $level . '%\' and  emp_requirement_s2.job_type like \'%' . $nature . '%\' and  
//                    emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 and (is_blue_worker_job != \'y\' OR is_blue_worker_job is null) limit ' . $offset . $limit);
//
//        $n = $this->db->query('SELECT * FROM emp_new_job join job_category on emp_new_job.category_id= job_category.category_id join 
//            emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id where job_category.category_id =  ' . $cat_id . '  and emp_new_job.deadline >= \'' . date('Y-m-d') . '\' and
//                emp_requirement_s2.job_level like \'%' . $level . '%\' and  emp_requirement_s2.job_type like \'%' . $nature . '%\' and  
//                    emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 and (is_blue_worker_job != \'y\' OR is_blue_worker_job is null)');
       //echo $query;
        //echo $query;
        $q = $this->db->query($query);
        $this->num_rows = $q->num_rows();
        $query .= ' limit ' . $offset . $limit;

       // $q = $this->db->query($query);
        //echo $this->db->last_query();
        return $q->result_array();
    }
    
    function select_category_for_plain_list() {
    
//        $q = $this->db->query('Select job_category.*, IFNULL(total.count,0) count FROM job_category left 
//            join (select category_id,Count(id) count 
//            from emp_new_job, emp_requirement_s2
//            where id = emp_job_id and deadline >= \'' . date('Y-m-d') . '\' 
//            and emp_new_job.is_active = 1 and (is_blue_worker_job != \'y\' OR is_blue_worker_job is null)
//            group by category_id) total on total.category_id = job_category.category_id');
              $q = $this->db->query('Select job_category.*, IFNULL(total.count,0) count FROM job_category left 
            join (select category_id,Count(emp_new_job.id) count 
            from emp_new_job, emp_requirement_s2,emp_voucher
            where emp_new_job.id = emp_requirement_s2.emp_job_id and emp_new_job.id=emp_voucher.job_id and
			deadline >= \'' . date('Y-m-d') . '\' 
            and emp_new_job.is_active = 1 and emp_voucher.is_publish=1
			and (is_blue_worker_job != \'y\' OR is_blue_worker_job is null)
            group by category_id) total on total.category_id = job_category.category_id
');
        return $q->result_array();
    }
    
    
    
    
    
    //test
    function get_emp_category($user_id){
        $this->db->select('job_emp_category.*, category_name');
        $this->db->from('job_emp_category');
        $this->db->join('job_category', 'job_emp_category.category_id = job_category.category_id');
        $this->db->where(array('employee_id' => $user_id));
        
        $q = $this->db->get();
        return $q->result_array();
    }
    
     function jobs_by_category($cat_id, $limit, $start) {
        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
		$this->db->join('emp_voucher', 'emp_new_job.id = emp_voucher.job_id');
        $where = "(is_blue_worker_job != 'y' OR is_blue_worker_job is null)";
        $this->db->where(array('category_id' => $cat_id, 'deadline >=' => date('Y-m-d'), 'emp_new_job.is_active' => 1, 'emp_voucher.is_publish' => 1));
        $this->db->where($where);
        $this->db->order_by('posting_date desc');
        $this->db->limit($limit, $start);
        $q = $this->db->get();

        $n = $this->db->get_where('emp_new_job', array('category_id' => $cat_id, 'deadline >=' => date('Y-m-d')));
        $this->num_rows = $n->num_rows();

        return $q->result_array();
    }
    
    
}
?>
