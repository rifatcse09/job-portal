<?php

class Career extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    function CreateCareer($user_id){
        $data = array(
            'user_id' => $user_id,
            'objective' => $_POST['career_objective'],
            'year_experience' => $_POST['career_year_experience'],
            'present_salary' => $_POST['career_present_salary'],
            'expected_salary' => $_POST['career_exp_salary'],
            'looking_for' => $_POST['career_level'],
            'available_for' => $_POST['career_available']
        );
        
        $this->db->insert('job_resume_career', $data);
    }
    
    function edit_career($user_id){
        $data = array(
            'objective' => $_POST['career_obj'],
            'year_experience' => $_POST['career_year_exp'],
            'present_salary' => $_POST['career_pre_salary'],
            'expected_salary' => $_POST['career_exp_salary'],
            'looking_for' => $_POST['career_level'],
            'available_for' => $_POST['career_available']
        );
        
        $this->db->where('user_id', $user_id);
        $this->db->update('job_resume_career', $data);
    }
}

?>
