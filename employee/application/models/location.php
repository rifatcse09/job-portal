<?php

class Location extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function select_all_cities() {
        $query = $this->db->get('district');
        return $query->result_array();
    }

    function select_all_countries() {
        $query = $this->db->get('country');
        return $query->result_array();
    }

    function get_emp_location($user_id) {
        $this->db->select('job_emp_location.*, District_Name');
        $this->db->from('job_emp_location');
        $this->db->join('district', 'job_emp_location.location_id = district.District_Id');
        $this->db->where(array('employee_id' => $user_id));

        $q = $this->db->get();
        return $q->result_array();
    }
    
    function get_emp_country($user_id){
        $this->db->select('job_emp_country.*, Country_Name');
        $this->db->from('job_emp_country');
        $this->db->join('Country', 'job_emp_country.country_id = Country.Country_Id');
        $this->db->where(array('employee_id' => $user_id));

        $q = $this->db->get();
        return $q->result_array();
    }

}

?>
