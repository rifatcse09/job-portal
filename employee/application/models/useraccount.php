<?php

class UserAccount extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function check_user($user_name) {
        $query = $this->db->get_where('job_employee_login', array('user_name' => $user_name));
        if ($query->num_rows() > 0)
            return FALSE;
        return true;
    }

    function CreateUserAccount($user_id) {
        $data = array(
            'user_id' => $user_id,
            'user_name' => $_POST['account_username'],
            'password' => $_POST['account_password'],
            'email' => $_POST['account_email']
        );

        $this->db->insert('job_employee_login', $data);
    }

    public function log_me_in($user, $pass) {
        $q = $this->db->get_where('job_employee_login', array('user_name' => $user, 'password' => $pass));
        if ($q->num_rows() > 0) {
            $res = $q->row_array(1);
            $query = $this->db->get_where('job_resume_personal', array('id' => $res['user_id']));
            $personal_info = $query->row_array(1);
            $ses_array = array(
                'user_id' => $res['user_id'],
                'user_name' => $res['user_name'],
                'name' => $personal_info['name']
            );
            $this->session->set_userdata($ses_array);
            return true;
        }
        else
            return false;
    }

    public function change_password($user_id, $user_name, $password) {
        $data = array('user_name' => $user_name, 'password' => $password);
        $this->db->update('job_employee_login', $data, array('user_id' => $user_id));

        if ($this->db->affected_rows() > 0) {
            $this->log_me_in($user_name, $password);
            return true;
        } else {
            return false;
        }
    }
    
    function logout(){
        $array_items = array('user_name' => '', 'user_id' => '', 'name' => '');
        $this->session->unset_userdata($array_items);
        $this->session->sess_destroy();
    }
    
    function get_user_account_info($user_id){
        $q = $this->db->get_where('job_employee_login', array('user_id' => $user_id));
        return $q->result_array();
    }
}

?>
