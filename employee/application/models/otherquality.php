<?php

/**
 * Description of OtherQuality
 *
 * @author Anick
 */
class OtherQuality extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function create_language($user_id) {
        $data = array();
        foreach ($_POST['language'] as $key => $lang) {
            if (!empty($lang)) {
                $row['employee_id'] = $user_id;
                $row['language_name'] = $lang;
                $row['reading'] = $_POST['reading'][$key];
                $row['writing'] = $_POST['writing'][$key];
                $row['speaking'] = $_POST['speaking'][$key];

                array_push($data, $row);
            }
        }
        $this->db->insert_batch('job_other_language', $data);
    }

    function get_all_language($user_id) {
        $q = $this->db->get_where('job_other_language', array('employee_id' => $user_id));
        return $q->result_array();
    }

    function create_reference($user_id) {

        $row['employee_id'] = $user_id;
        $row['ref_name'] = $_POST['ref_name'];
        $row['organization'] = $_POST['ref_org'];
        $row['designation'] = $_POST['ref_designation'];
        $row['address'] = $_POST['ref_address'];
        $row['phone_office'] = $_POST['ref_off_phone'];
        $row['phone_resident'] = $_POST['ref_res_phone'];
        $row['mobile'] = $_POST['ref_mobile'];
        $row['email'] = $_POST['ref_email'];
        $row['relation'] = $_POST['relation'];

        $this->db->insert('job_other_reference', $row);
    }

    function get_all_reference($user_id) {
        $q = $this->db->get_where('job_other_reference', array('employee_id' => $user_id));
        return $q->result_array();
    }

    function get_reference($ref_id) {
        $q = $this->db->get_where('job_other_reference', array('id' => $ref_id));
        return $q->row_array();
    }

    function edit_language($user_id) {
        $data = array();
        foreach ($_POST['language_edit'] as $key => $lang) {
            if (!empty($lang)) {
                $row['employee_id'] = $user_id;
                $row['language_name'] = $lang;
                $row['reading'] = $_POST['reading_edit'][$key];
                $row['writing'] = $_POST['writing_edit'][$key];
                $row['speaking'] = $_POST['speaking_edit'][$key];

                array_push($data, $row);
            }
        }
        $this->db->where('employee_id', $user_id);
        $this->db->delete('job_other_language');
        $this->db->insert_batch('job_other_language', $data);
    }

    function edit_reference($user_id) {

        $row['employee_id'] = $user_id;
        $row['ref_name'] = $_POST['ref_name_edit'];
        $row['organization'] = $_POST['ref_org_edit'];
        $row['designation'] = $_POST['ref_designation_edit'];
        $row['address'] = $_POST['ref_address_edit'];
        $row['phone_office'] = $_POST['ref_off_phone_edit'];
        $row['phone_resident'] = $_POST['ref_res_phone_edit'];
        $row['mobile'] = $_POST['ref_mobile_edit'];
        $row['email'] = $_POST['ref_email_edit'];
        $row['relation'] = $_POST['relation_edit'];

        $this->db->where('id', $_POST['reference_id']);
        $this->db->update('job_other_reference', $row);
    }

}

?>
