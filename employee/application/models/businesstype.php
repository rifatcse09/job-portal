<?php
class BusinessType extends CI_Model{
        function __construct() {
            parent::__construct();
        }
        
        function select_all_business_type(){
            $query = $this->db->get_where('emp_business_type',array('is_active' => 1));
            return $query->result_array();
        }
         function get_emp_businesstype($user_id){
         $this->db->select('job_emp_preferred_org.*, business_Name');
        $this->db->from('job_emp_preferred_org');
        $this->db->join('emp_business_type', 'job_emp_preferred_org.org_id = emp_business_type.business_id');
        $this->db->where(array('job_emp_preferred_org.employee_id' => $user_id));

        $q = $this->db->get();
        return $q->result_array();
//        $this->db->select('*');
//        $this->db->from('job_emp_preferred_org');
//        $this->db->join('emp_business_type', 'job_emp_preferred_org.org_id = emp_business_type.business_id');
//        $this->db->where('job_emp_preferred_org.employee_id', $user_id);
//
//        
//        $q = $this->db->get();
//        return $q->result_array();
    }
}
?>
