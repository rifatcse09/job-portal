<?php
/**
 * Description of Professinal_Qualification
 *
 * @author ANICK
 */
class Professional_Qualification extends CI_Model {
    //put your code here
    function __construct() {
        parent::__construct();
    }
    
    function save_professional_qualification($user_id){
        $data = array(
                'employee_id' => $user_id,
                'certification' => $_POST['prof_certification'],
                'institute' => $_POST['prof_institute'],
                'location' => $_POST['prof_location'],
                'from' => $_POST['prof_from'],
                'to' => $_POST['prof_to']            );
        $this->db->insert('job_professional', $data);
    }
    
    function get_professional_qualification($user_id){
        $q = $this->db->get_where('job_professional', array('employee_id' => $user_id));
        return $q->result_array();
    }
}

?>
