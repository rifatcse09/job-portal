-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 26, 2015 at 11:26 PM
-- Server version: 5.5.42-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ckrbd_azadijobs`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`ckrbd`@`localhost` PROCEDURE `pro_winner`(IN `r_ans` VARCHAR(30), IN `id` INT(15))
    NO SQL
BEGIN


CREATE TEMPORARY TABLE IF NOT EXISTS temp ENGINE=MEMORY AS (
SELECT *
FROM quiz_ans
WHERE ans = r_ans
AND quiz_id = id
LIMIT 0 , 3);

select *  from temp join quiz_reg where temp.user_id=quiz_reg.id;

DROP TEMPORARY TABLE temp;

END$$

CREATE DEFINER=`ckrbd`@`localhost` PROCEDURE `ready_for_job`(IN `page` INT(10))
BEGIN


CREATE TEMPORARY TABLE IF NOT EXISTS table2 ENGINE=MEMORY AS (
SELECT employee_id,max( level_id ) as level_id
FROM job_employee_login
JOIN job_academic 
WHERE job_employee_login.user_id =job_academic.employee_id
AND job_employee_login.is_active =1
GROUP BY employee_id);

select *  from table2 join job_academic join job_resume_personal where table2.employee_id=job_academic.employee_id and table2.level_id=job_academic.level_id  and table2.employee_id=job_resume_personal.id limit page,10 ;

DROP TEMPORARY TABLE table2;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admission`
--

CREATE TABLE IF NOT EXISTS `admission` (
  `ad_id` int(35) NOT NULL AUTO_INCREMENT,
  `versity_name` varchar(100) NOT NULL,
  `admission_name` varchar(100) NOT NULL,
  `last_date` date NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `advertise`
--

CREATE TABLE IF NOT EXISTS `advertise` (
  `ad_id` int(50) NOT NULL AUTO_INCREMENT,
  `ad_type` varchar(100) NOT NULL,
  `page_location` varchar(50) NOT NULL,
  `size_pixel` varchar(50) NOT NULL,
  `price` varchar(50) NOT NULL,
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `advertisement`
--

CREATE TABLE IF NOT EXISTS `advertisement` (
  `add_id` int(11) NOT NULL AUTO_INCREMENT,
  `advertisement_name` varchar(100) NOT NULL,
  `advertisement_type` varchar(50) NOT NULL,
  `website` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `path` varchar(200) NOT NULL,
  PRIMARY KEY (`add_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `advertisement`
--

INSERT INTO `advertisement` (`add_id`, `advertisement_name`, `advertisement_type`, `website`, `name`, `size`, `type`, `path`) VALUES
(55, 'm', 'MainpageAd2', 'http://bdmessenger.com', 'SSSL_Home_Page.gif', '2887', 'image/gif', '../advertise_images/SSSL_Home_Page.gif'),
(54, 'a', 'MainpageAd1', 'http://bdmessenger.com', 'space.png', '10416', 'image/png', '../advertise_images/space.png'),
(59, 'a', 'jobcategory2', 'http://bdmessenger.com', 'ad3.png', '19232', 'image/png', '../advertise_images/ad3.png'),
(58, '', 'jobcategory1', 'http://bdmessenger.com', 'macc.png', '10431', 'image/png', '../advertise_images/macc.png'),
(53, 'e', 'MainpageAd1', 'http://bdmessenger.com', 'banglalink.gif', '4063', 'image/gif', '../advertise_images/banglalink.gif'),
(52, 'ad', 'MainpageAd1', 'http://bdmessenger.com', 'chevron_120x65.gif', '2976', 'image/gif', '../advertise_images/chevron_120x65.gif'),
(49, 'ad1', 'MainpageAd1', 'http://bdmessenger.com', 'Nestle_Logo.gif', '1976', 'image/gif', '../advertise_images/Nestle_Logo.gif'),
(50, 'ad2', 'MainpageAd1', 'http://bdmessenger.com', 'Citygroup.gif', '2195', 'image/gif', '../advertise_images/Citygroup.gif'),
(51, 'ad', 'MainpageAd1', 'http://bdmessenger.com', 'coats_2011.gif.jpg', '3129', 'image/jpeg', '../advertise_images/coats_2011.gif.jpg'),
(56, 'm', 'MainpageAd2', 'http://bdmessenger.com', 'bdjobs-bkash.gif', '2871', 'image/gif', '../advertise_images/bdjobs-bkash.gif'),
(57, 'aa', 'jobdetails', 'http://www.dainikazadi.org', 'f6bf125d8458b29b034cd324936c3da8.gif', '3217', 'image/gif', '../advertise_images/f6bf125d8458b29b034cd324936c3da8.gif'),
(60, '', 'jobcategory2', 'http://bdmessenger.com', 'ad2.png', '35559', 'image/png', '../advertise_images/ad2.png'),
(61, 'rr', 'jobcategory2', 'http://bdmessenger.com', 'macc.png', '10431', 'image/png', '../advertise_images/macc.png'),
(62, 'fsdf', 'MainpageTop', 'pentagemsbd.com', 'Employer-Big2742014133639.png', '180017', 'image/png', '../advertise_images/Employer-Big2742014133639.png'),
(66, 'PentaGems Accounting', 'Mainpagebottom', 'http://pentagemsbd.com/', 'addbangla3.png', '11178', 'image/png', '../advertise_images/addbangla3.png'),
(64, 'fdsf', 'MainpageTop', 'pentagemsbd.com', 'Employer-Big2742014133639.png', '180017', 'image/png', '../advertise_images/Employer-Big2742014133639.png'),
(68, 'bsrm', 'Mainpagebottom', 'f', 'add1.png', '36105', 'image/png', '../advertise_images/add1.png');

-- --------------------------------------------------------

--
-- Table structure for table `basic_info`
--

CREATE TABLE IF NOT EXISTS `basic_info` (
  `id` int(11) NOT NULL,
  `profile_created` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dob` date NOT NULL,
  `img_name` varchar(100) NOT NULL,
  `img_size` varchar(50) NOT NULL,
  `img_type` varchar(50) NOT NULL,
  `img_path` varchar(100) NOT NULL,
  `marital_status` varchar(100) NOT NULL,
  `height` varchar(30) NOT NULL,
  `body_type` varchar(50) NOT NULL,
  `body_weight` varchar(30) NOT NULL,
  `health_info` varchar(200) NOT NULL,
  `complexion` varchar(20) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `land_phone` varchar(100) NOT NULL,
  `special_case` varchar(200) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `is_front` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_info`
--

INSERT INTO `basic_info` (`id`, `profile_created`, `gender`, `dob`, `img_name`, `img_size`, `img_type`, `img_path`, `marital_status`, `height`, `body_type`, `body_weight`, `health_info`, `complexion`, `mobile`, `land_phone`, `special_case`, `blood_group`, `is_front`, `is_active`) VALUES
(1, 'Self', 'Male', '1985-02-21', '40CB3EA9-C992-BC49-0BE0-32D7150EC38B.jpg', '845941', 'image/jpeg', '../uploaded_images/40CB3EA9-C992-BC49-0BE0-32D7150EC38B.jpg', 'Never Married', '5ft 10in - 177cm', 'Average', '70', 'No Health Problems', 'Fair', '8801826523658', '636465', 'None', 'A+', 1, 1),
(4, 'Self', 'Male', '1990-01-01', '032020B7-0BD5-DB0B-4C45-7E330F8910C5.BMP', '304182', 'image/bmp', '032020B7-0BD5-DB0B-4C45-7E330F8910C5.BMP', 'Never Married', '6ft - 182cm', 'Slim', '60', 'No Health Problems', 'Very Fair', '01711371938', '711235', 'None', 'O+', 0, 1),
(5, 'Self', 'Male', '1989-02-15', '1E660849-C9AA-E64C-D8B4-B89F2C730E4C.jpg', '845941', 'image/jpeg', '../uploaded_images/1E660849-C9AA-E64C-D8B4-B89F2C730E4C.jpg', 'Never Married', '5ft 10in - 177cm', 'Average', '80', 'No Health Problems', 'Fair', '2341654562', '564656515', 'None', 'A+', 0, 1),
(7, 'Self', 'Male', '1987-04-08', 'D44D0072-632E-954C-843B-19675C0BE472.JPG', '405073', 'image/jpeg', '../uploaded_images/D44D0072-632E-954C-843B-19675C0BE472.JPG', 'Never Married', '5ft 6in - 167cm', 'Slim', '67', 'No Health Problems', 'Fair', '01730799799', '711234', 'None', 'B+', 1, 1),
(9, 'Friend', 'Male', '1983-05-08', 'E0E960EA-5579-3A9B-0CD8-59E4C26CCE4B.jpg', '71189', 'image/jpeg', 'E0E960EA-5579-3A9B-0CD8-59E4C26CCE4B.jpg', 'Never Married', '5ft 8in - 172cm', 'Average', '72', 'No Health Problems', 'Fair', '01554311134', '031-711234', 'None', 'A+', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `beauty_info`
--

CREATE TABLE IF NOT EXISTS `beauty_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `p_no` int(20) NOT NULL,
  `m_no` int(20) NOT NULL,
  `address` longtext NOT NULL,
  `description` longtext NOT NULL,
  `date` date NOT NULL,
  `img_name` varchar(200) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `path` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `beauty_info`
--

INSERT INTO `beauty_info` (`id`, `name`, `email`, `p_no`, `m_no`, `address`, `description`, `date`, `img_name`, `size`, `type`, `path`) VALUES
(1, 'fair touch beauty parlour', 'fairtouch@gmail.com', 12054005, 12458745, 'rohitpur bazar road...keranigonj\r\ndhaka.\r\ndhaka\r\n1310\r\nBangladesh', '   fair touch beauty parlour - we offerd Bridal makeup..party makeup...facial..hair spa..hair cut..hair rebonding...fair polis..hair tretment...threding...hair stret..meniqure/pediqure...etc.                         ', '2013-02-28', 'ADCA344E-74C0-EF34-F3F5-0870918874C3.', '45003', 'image/png', '../uploaded_images/ADCA344E-74C0-EF34-F3F5-0870918874C3.'),
(2, 'fair touch beauty parlour', 'fairtouch@gmail.com', 12054005, 12458745, 'rohitpur bazar road...keranigonj\r\ndhaka.\r\ndhaka\r\n1310\r\nBangladesh', '   fair touch beauty parlour, we offerd Bridal makeup, party makeup facial hair spa hair, cut, hair rebonding, fair polis, hair tretment, threding, hair stret, meniqure/pediqure etc.                         ', '2013-02-28', '1EB2D56A-0AFD-581B-8BAB-554E379EF2EA.', '45003', 'image/png', '../uploaded_images/1EB2D56A-0AFD-581B-8BAB-554E379EF2EA.');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_client`
--

CREATE TABLE IF NOT EXISTS `corporate_client` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `employer_id` int(100) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `discount` float NOT NULL,
  `job_no` int(100) NOT NULL,
  `cv_no` int(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `insert_date` datetime NOT NULL,
  `is_active` int(10) NOT NULL DEFAULT '1',
  `other1` varchar(100) NOT NULL,
  `other2` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `corporate_client`
--

INSERT INTO `corporate_client` (`id`, `employer_id`, `start_date`, `end_date`, `discount`, `job_no`, `cv_no`, `user`, `insert_date`, `is_active`, `other1`, `other2`) VALUES
(1, 1, '2015-04-09 00:00:00', '2016-01-01 00:00:00', 200, 20, 100, 'admin', '2015-04-09 12:27:03', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `Country_Id` int(10) NOT NULL,
  `Country_Name` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`Country_Id`, `Country_Name`) VALUES
(101, 'Afghanistan'),
(102, 'Albania'),
(103, 'Algeria'),
(104, 'American Samoa'),
(105, 'Andorra'),
(106, 'Angola'),
(107, 'Anguilla'),
(108, 'Antarctica'),
(109, 'Antigua'),
(110, 'Argentina'),
(111, 'Armenia'),
(112, 'Aruba'),
(113, 'Australia'),
(114, 'Austria'),
(115, 'Azerbaijan'),
(116, 'Bahamas'),
(117, 'Bahrain'),
(118, 'Bangladesh'),
(119, 'Barbados'),
(120, 'Belarus'),
(121, 'Belgium'),
(122, 'Belize'),
(123, 'Benin'),
(124, 'Bermuda'),
(125, 'Bhutan'),
(126, 'Bolivia'),
(127, 'Bosnia and Herzegovina'),
(128, 'Botswana'),
(129, 'Brazil'),
(130, 'British Virgin Islands'),
(131, 'Brunei'),
(132, 'Bulgaria'),
(133, 'Burkina Faso'),
(134, 'Burma'),
(135, 'Burundi'),
(136, 'Cambodia'),
(137, 'Cameroon'),
(138, 'Canada'),
(139, 'Cape Verde'),
(140, 'Central African Republic'),
(141, 'Chad'),
(142, 'Chile'),
(143, 'China'),
(144, 'Colombia'),
(145, 'Comoros'),
(147, 'Congo'),
(146, 'Congo(Zaire)'),
(148, 'Cook Islands'),
(149, 'Costa Rica'),
(150, 'Cote d''Ivoire (Ivory Coast)'),
(151, 'Croatia'),
(152, 'Cuba'),
(153, 'Cyprus'),
(154, 'Czech Republic'),
(155, 'Denmark'),
(156, 'Djibouti'),
(157, 'Dominica'),
(158, 'Dominican Republic'),
(323, 'East Timor'),
(159, 'Ecuador'),
(160, 'Egypt'),
(161, 'El Salvador'),
(162, 'Equatorial Guinea'),
(163, 'Eritrea'),
(164, 'Estonia'),
(165, 'Ethiopia'),
(166, 'Falkland Islands'),
(232, 'Federated States of Micronesia'),
(167, 'Fiji'),
(168, 'Finland'),
(169, 'France'),
(170, 'French Guiana'),
(171, 'French Polynesia'),
(172, 'Gabon'),
(174, 'Gaza Strip and West Bank'),
(175, 'Georgia'),
(176, 'Germany'),
(177, 'Ghana'),
(178, 'Gibraltar'),
(179, 'Greece'),
(180, 'Greenland'),
(181, 'Grenada'),
(182, 'Guadeloupe'),
(183, 'Guam'),
(184, 'Guatemala'),
(185, 'Guinea'),
(186, 'Guinea-Bissau'),
(187, 'Guyana'),
(188, 'Haiti'),
(190, 'Honduras'),
(191, 'Hong Kong'),
(192, 'Hungary'),
(193, 'Iceland'),
(194, 'India'),
(195, 'Indonesia'),
(196, 'Iran'),
(197, 'Iraq'),
(198, 'Ireland'),
(199, 'Israel'),
(200, 'Italy'),
(201, 'Jamaica'),
(202, 'Japan'),
(203, 'Jordan'),
(204, 'Kazakhstan'),
(205, 'Kenya'),
(206, 'Kiribati'),
(207, 'Kuwait'),
(208, 'Kyrgyzstan'),
(209, 'Laos'),
(210, 'Latvia'),
(211, 'Lebanon'),
(212, 'Lesotho'),
(213, 'Liberia'),
(214, 'Libya'),
(215, 'Liechtenstein'),
(216, 'Lithuania'),
(217, 'Luxembourg'),
(218, 'Macau'),
(219, 'Macedonia'),
(220, 'Madagascar'),
(221, 'Malawi'),
(222, 'Malaysia'),
(223, 'Maldives'),
(224, 'Mali'),
(225, 'Malta'),
(226, 'Marshall Islands'),
(227, 'Martinique'),
(228, 'Mauritania'),
(229, 'Mauritius'),
(230, 'Mayotte'),
(231, 'Mexico'),
(233, 'Moldova'),
(234, 'Monaco'),
(235, 'Mongolia'),
(236, 'Montserrat'),
(237, 'Morocco'),
(238, 'Mozambique'),
(239, 'Namibia'),
(240, 'Nauru'),
(241, 'Nepal'),
(242, 'Netherlands'),
(243, 'Netherlands Antilles'),
(244, 'New Caledonia'),
(245, 'New Zealand'),
(246, 'Nicaragua'),
(247, 'Niger'),
(248, 'Nigeria'),
(249, 'North Korea'),
(250, 'Northern Mariana Islands'),
(251, 'Norway'),
(252, 'Oman'),
(253, 'Pakistan'),
(254, 'Palau'),
(255, 'Panama'),
(256, 'Papua New Guinea'),
(257, 'Paraguay'),
(258, 'Peru'),
(259, 'Philippines'),
(260, 'Pitcairn Islands'),
(261, 'Poland'),
(262, 'Portugal'),
(263, 'Puerto Rico'),
(264, 'Qatar'),
(265, 'Reunio'),
(266, 'Romania'),
(267, 'Russia'),
(268, 'Rwanda'),
(269, 'Saint Kitts and Nevis'),
(270, 'Saint Lucia'),
(271, 'Saint Pierre and Miquelon'),
(272, 'Saint Vincent and the GrenadinSamoan'),
(273, 'San Mariano'),
(274, 'Sao Tome and Principe'),
(275, 'Saudi Arabia'),
(276, 'Senegal'),
(277, 'Serbia and Montenegro'),
(278, 'Sierra Leone'),
(279, 'Singapore'),
(280, 'Slovakia'),
(281, 'Slovenia'),
(282, 'Solomon Islands'),
(283, 'Somalia'),
(284, 'South Africa'),
(285, 'South Korea'),
(286, 'South Sudan'),
(287, 'Spain'),
(324, 'Sri Lanka'),
(288, 'Sudan'),
(289, 'Suriname'),
(290, 'Swaziland'),
(291, 'Swede'),
(292, 'Switzerland'),
(293, 'Syria'),
(294, 'Taiwan'),
(295, 'Tajikistan'),
(296, 'Tanzania'),
(297, 'Thailand'),
(298, 'The Gambia'),
(299, 'The Holy See'),
(173, 'Tonga'),
(189, 'Trinidad and Tobago'),
(300, 'Tunisia'),
(301, 'Turkey'),
(302, 'Turkmenistan'),
(303, 'Turks and Caicos Islands'),
(304, 'Tuvalu'),
(305, 'Uganda'),
(306, 'Ukraine'),
(307, 'United Arab Emirates'),
(308, 'United Kingdom'),
(309, 'United States'),
(310, 'United States Virgin Islands'),
(311, 'Uruguay'),
(312, 'Togo'),
(313, 'Uzbekistan'),
(314, 'Vanuatu'),
(315, 'Venezuela'),
(316, 'Vietnam'),
(317, 'West Bank and Gaza Strip'),
(318, 'Western Sahara'),
(319, 'Yeme'),
(320, 'Zambia'),
(321, 'Montenegro');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
  `District_Id` int(10) NOT NULL,
  `District_Name` varchar(50) DEFAULT NULL,
  `Division_Name` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`District_Id`, `District_Name`, `Division_Name`) VALUES
(1, 'B. Baria', 'Chittagong'),
(2, 'Bagerhat', 'Khulna'),
(3, 'Bandarban', 'Chittagong'),
(4, 'Barisal', 'Barisal'),
(5, 'Bhola', 'Barisal'),
(6, 'Bogra', 'Rajshahi'),
(7, 'Borguna', 'Barisal'),
(8, 'Chandpur', 'Chittagong'),
(9, 'Chapainawabga', 'Rajshahi'),
(10, 'Chittagong', 'Chittagong'),
(11, 'Chuadanga', 'Khulna'),
(12, 'Comilla', 'Chittagong'),
(13, 'Cox''s Bazar', 'Chittagong'),
(14, 'Dhaka', 'Dhaka'),
(15, 'Dinajpur', 'Rangpur'),
(16, 'Faridpur', 'Dhaka'),
(17, 'Feni', 'Chittagong'),
(18, 'Gaibandha', 'Rangpur'),
(19, 'Gazipur', 'Dhaka'),
(20, 'Gopalgonj', 'Dhaka'),
(21, 'Hobigonj', 'Sylhet'),
(22, 'Jamalpur', 'Dhaka'),
(23, 'Jessore', 'Khulna'),
(24, 'Jhalokathi', 'Barisal'),
(25, 'Jhenaidah', 'Khulna'),
(26, 'Joypurhat', 'Rajshahi'),
(27, 'Khagrachari', 'Chittagong'),
(28, 'Khulna', 'Khulna'),
(29, 'Kishorgonj', 'Dhaka'),
(30, 'Kurigram', 'Rangpur'),
(31, 'Kushtia', 'Khulna'),
(32, 'Lalmonirhat', 'Rangpur'),
(33, 'Laxmipur', 'Chittagong'),
(34, 'Madaripur', 'Dhaka'),
(35, 'Magura', 'Khulna'),
(36, 'Manikgonj', 'Dhaka'),
(37, 'Meherpur', 'Khulna'),
(38, 'MoulaviBazar', 'Sylhet'),
(39, 'Munshigonj', 'Dhaka'),
(40, 'Mymensingh', 'Dhaka'),
(41, 'Naogao', 'Rajshahi'),
(42, 'Narail', 'Khulna'),
(43, 'Narayangonj', 'Dhaka'),
(44, 'Narshingdi', 'Dhaka'),
(45, 'Natore', 'Rajshahi'),
(46, 'Netrokona', 'Dhaka'),
(47, 'Nilphamari', 'Rangpur'),
(48, 'Noakhali', 'Chittagong'),
(49, 'Pabna', 'Rajshahi'),
(50, 'Panchagarh', 'Rangpur'),
(51, 'Patuakhali', 'Barisal'),
(52, 'Pirojpur', 'Barisal'),
(53, 'Rajbari', 'Dhaka'),
(54, 'Rajshahi', 'Rajshahi'),
(55, 'Rangamati', 'Chittagong'),
(56, 'Rangpur', 'Rangpur'),
(57, 'Satkhira', 'Khulna'),
(58, 'Shariatpur', 'Dhaka'),
(59, 'Sherpur', 'Dhaka'),
(60, 'Sirajgonj', 'Rajshahi'),
(61, 'Sunamgonj', 'Sylhet'),
(62, 'Sylhet', 'Sylhet'),
(63, 'Tangail', 'Dhaka'),
(64, 'Thakurgao', 'Rangpur');

-- --------------------------------------------------------

--
-- Table structure for table `edu_carrer`
--

CREATE TABLE IF NOT EXISTS `edu_carrer` (
  `id` int(11) NOT NULL,
  `education_deg` varchar(150) NOT NULL,
  `education_area` varchar(150) NOT NULL,
  `college_attend` varchar(100) NOT NULL,
  `college_attend_other` varchar(100) NOT NULL,
  `work_with` varchar(100) NOT NULL,
  `work_as` varchar(100) NOT NULL,
  `annual_income` varchar(100) NOT NULL,
  `diet` varchar(100) NOT NULL,
  `drink` varchar(100) NOT NULL,
  `smoke` varchar(100) NOT NULL,
  `about_you` varchar(250) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emp_acc_info`
--

CREATE TABLE IF NOT EXISTS `emp_acc_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `password` varchar(10) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `is_corporate` int(10) NOT NULL DEFAULT '0',
  `alt_company_name` varchar(100) DEFAULT NULL,
  `contact_person` varchar(100) NOT NULL,
  `contact_designation` varchar(50) NOT NULL,
  `business_id` int(10) NOT NULL,
  `business_description` varchar(250) DEFAULT NULL,
  `company_address` varchar(250) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `area` varchar(50) DEFAULT NULL,
  `billing_address` varchar(250) NOT NULL,
  `contact_phone` varchar(100) NOT NULL,
  `contact_email` varchar(50) NOT NULL,
  `website_address` varchar(50) NOT NULL,
  `company_logo` varchar(20) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `emp_acc_info`
--

INSERT INTO `emp_acc_info` (`id`, `username`, `password`, `company_name`, `is_corporate`, `alt_company_name`, `contact_person`, `contact_designation`, `business_id`, `business_description`, `company_address`, `country_name`, `city`, `area`, `billing_address`, `contact_phone`, `contact_email`, `website_address`, `company_logo`, `is_active`) VALUES
(1, 'pentagems', '12345678', 'pg', 1, 'pg', 'sanjoy', 'se', 42, 'SOftware Developer', 'CTG', 'Bangladesh', 'CTG', 'CTG', 'CTG', '01954195589', 'sanjoy.ict@gmail.com', '', '1.png', 1),
(2, 'nahid', '12345678', 'Meverick Inc.', 0, '', 'Nahidul', 'CTO', 71, 'Game Developer', 'GEC,Chittagong', 'Bangladesh', 'Chittagong', 'GEC', 'Ifco Complex, GEC', '01511156534', 'nahid.cuet09@gmail.com', '', '2.png', 1),
(3, 'masud', '12345678', 'Masud Group', 0, '', 'ksfdkjf', 'khsdkfjhk', 1, 'dfs', 'fsdssdfs', 'Bangladesh', 'sfsdf', 'sdf', 'sasdfsdf', 'sadf', 'sanjoy.ict@gmail.com', 'safdsas', NULL, 1),
(4, 'Ditan', '1234567890', 'ctgsoftware', 0, '', 'nahid', 'Software Engg.', 71, 'Software', 'ctgsoftware', 'Bangladesh', 'chittagong', 'gsc,ctg', 'agrabad', '1234567', 'hawauljannat@gmail.com', '', NULL, 1),
(5, 'ditan', '22222222', 'ctgsoftware', 0, '', 'nahid', 'Software Engg.', 71, '   sofware', 'ctgsoftware', 'Bangladesh', 'chittagong', 'ctg', 'nahid', '0123456789', 'hawauljannat@gmail.com', '', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `emp_area`
--

CREATE TABLE IF NOT EXISTS `emp_area` (
  `area_id` int(10) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `emp_area`
--

INSERT INTO `emp_area` (`area_id`, `area_name`, `is_active`) VALUES
(1, 'Accounts', 1),
(2, 'Audit', 1),
(3, 'Cash Management', 1),
(4, 'Commercial/Export-Import', 1),
(5, 'Company Secretary/Share Division', 1),
(6, 'Finance', 1),
(7, 'Internal Audit', 1),
(8, 'Strategic Planning', 1),
(9, 'Tax (VAT/ Customs Duty/ Income Tax)', 1);

-- --------------------------------------------------------

--
-- Table structure for table `emp_business_type`
--

CREATE TABLE IF NOT EXISTS `emp_business_type` (
  `business_id` int(10) NOT NULL AUTO_INCREMENT,
  `business_name` varchar(250) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`business_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `emp_business_type`
--

INSERT INTO `emp_business_type` (`business_id`, `business_name`, `is_active`) VALUES
(1, 'Advertising Agency', 1),
(2, 'Agro based firms (incl. Agro Processing/Seed/GM)', 1),
(3, 'Airline/GSA', 1),
(4, 'Architecture/Interior Design Firm', 1),
(5, 'Audit Firms /Tax Consultant', 1),
(6, 'Automobile Companies', 1),
(7, 'Banks', 1),
(8, 'BPO', 1),
(9, 'Buying House (Garments)', 1),
(10, 'Call Center', 1),
(11, 'Cement Industry', 1),
(12, 'Chemical Industries', 1),
(13, 'Clearing & Forwarding (C&F) companies', 1),
(14, 'Computer Hardware/Network Companies', 1),
(15, 'Consulting Firms', 1),
(16, 'Cosmetics/Personal Care', 1),
(17, 'Design/Printing/Publishing', 1),
(18, 'Development Agencies', 1),
(19, 'Direct Selling/Marketing Service Company', 1),
(20, 'Distribution Companies/Wholesale', 1),
(21, 'Education (School & Colleges)', 1),
(22, 'Education (Universities)', 1),
(23, 'Electronic Equipment/Home Appliances', 1),
(24, 'Embassies/Foreign Consulate', 1),
(25, 'Engineering Firms', 1),
(26, 'Event Management', 1),
(27, 'Fisheries', 1),
(28, 'Food (Packaged)/Beverage', 1),
(29, 'Freight forwarding', 1),
(30, 'Furniture Manufacturer', 1),
(31, 'Garments (Woven/Apparel/Knitting)', 1),
(32, 'Garments Accessories', 1),
(33, 'Governement Organizations', 1),
(34, 'Group of Companies', 1),
(35, 'Hospitals/Clinic/Diagonastic Centre', 1),
(36, 'Hotels/Resorts', 1),
(37, 'Immigration & Education Consultancy Service', 1),
(38, 'Indenting Firm', 1),
(39, 'Insurance', 1),
(40, 'Investment/Merchant Banking', 1),
(41, 'ISP', 1),
(42, 'IT Enabled Service', 1),
(43, 'Jute Goods/ Jute Yarn', 1),
(44, 'Law Firm', 1),
(45, 'Leasing', 1),
(46, 'Logistic/Courier/Air Express Companies', 1),
(47, 'Manpower Recruitment', 1),
(48, 'Manufacturing (FMCG)', 1),
(49, 'Manufacturing (Light Engineering & Heavy Industry)', 1),
(50, 'Market Research Firms', 1),
(51, 'Media/Public Relation Companies', 1),
(52, 'Medical Equipment', 1),
(53, 'Mobile Accessories', 1),
(54, 'Multinational Companies', 1),
(55, 'Newspaper/Magazine', 1),
(56, 'NGOs', 1),
(57, 'Overseas Companies', 1),
(58, 'Packaging Industry', 1),
(59, 'Pharmaceutical/Medicine Companies', 1),
(60, 'Plastic/ Polymer Industry', 1),
(61, 'Poultry/Dairy/Veterinary', 1),
(62, 'Power Equipment/Generator/CNG', 1),
(63, 'Real Estate/ Developer', 1),
(64, 'Research Organization', 1),
(65, 'Resturants', 1),
(66, 'Retail/Shops', 1),
(67, 'Security Service Company', 1),
(68, 'Share Brokerage/ Securities House', 1),
(69, 'Shipping', 1),
(70, 'Shrimp/Hatchery', 1),
(71, 'Software Companies', 1),
(72, 'Sweater Industry', 1),
(73, 'Tannery/Footwear', 1),
(74, 'Tea Company', 1),
(75, 'Telecommunication', 1),
(76, 'Textile (Spinning, Weaving, Knitting, Dyeing/Finishing)', 1),
(77, 'Tobacco', 1),
(78, 'Trading or Export/Import', 1),
(79, 'Training Institutues', 1),
(80, 'Travel Agents/Tour Operators', 1);

-- --------------------------------------------------------

--
-- Table structure for table `emp_new_job`
--

CREATE TABLE IF NOT EXISTS `emp_new_job` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) DEFAULT NULL,
  `job_title` varchar(200) DEFAULT NULL,
  `no_vacancy` int(10) DEFAULT NULL,
  `online_cv` enum('y','n') DEFAULT NULL,
  `email_attach` enum('y','n') DEFAULT NULL,
  `email_id` varchar(100) DEFAULT NULL,
  `hard_copy` enum('y','n') DEFAULT NULL,
  `photogrph_cv` enum('y','n') DEFAULT NULL,
  `apply_instruction` varchar(250) DEFAULT NULL,
  `posting_date` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `billing_contact` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `job_display` enum('y','n') DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `hide_address` enum('y','n') DEFAULT NULL,
  `is_hot_job` enum('y','n') DEFAULT NULL,
  `is_blue_worker_job` enum('y','n') DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `template` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `emp_new_job`
--

INSERT INTO `emp_new_job` (`id`, `category_id`, `job_title`, `no_vacancy`, `online_cv`, `email_attach`, `email_id`, `hard_copy`, `photogrph_cv`, `apply_instruction`, `posting_date`, `deadline`, `billing_contact`, `designation`, `job_display`, `company_id`, `company_name`, `hide_address`, `is_hot_job`, `is_blue_worker_job`, `is_active`, `template`) VALUES
(2, 13, 'IT Engineer', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-08', '2015-09-14', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(3, 12, 'HRM', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'MSC/MBA', '2015-04-09', '2015-07-06', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(4, 12, 'HRD', 1, 'n', 'y', 'sanjoy.ict@gmail.com', 'n', 'n', 'NA', '2015-04-09', '2015-07-05', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(5, 16, 'Field Officer', 10, 'y', 'y', 'sanjoy.ict@gmail.com', 'n', 'n', 'NA', '2015-04-09', '2015-05-10', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(6, 16, 'Field Officer', 10, 'y', 'y', 'sanjoy.ict@gmail.com', 'n', 'n', 'NA', '2015-04-09', '2015-05-10', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(7, 16, 'Field Officer', 10, 'y', 'y', 'sanjoy.ict@gmail.com', 'n', 'n', 'NA', '2015-04-09', '2015-05-10', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(8, 18, 'PS', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-09', '2015-06-10', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(9, 1, 'Accountant', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-09', '2015-06-10', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'y', 'n', 1, 'jobdetails_1'),
(10, 18, 'Waiter', 8, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-09', '2015-05-11', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'y', 'n', 1, 'jobdetails_2'),
(11, 18, 'PS', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-09', '2015-07-01', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(12, 3, 'Accountant', 3, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-09', '2015-06-10', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(13, 13, 'Game Developer', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-09', '2015-05-10', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(14, 14, 'Chief Marketing Officer', 1, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'Send CV using only one method', '2015-04-09', '2015-06-10', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'n', 'n', 1, 'jobdetails'),
(15, 13, 'Trainee Software Engineer', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-09', '2015-04-16', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'n', 'n', 1, 'jobdetails'),
(16, 18, 'Personal Assistant', 1, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'n', 'Apply using only method', '2015-04-09', '2015-05-09', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'n', 'n', 1, 'jobdetails'),
(17, 18, 'Personal Assistant', 1, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'n', 'Apply using only method', '2015-04-09', '2015-05-09', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'n', 'n', 1, 'jobdetails'),
(18, 9, 'Chief Engineer', 1, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-09', '2015-05-12', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(19, 7, 'UI Designer', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-09', '2015-05-14', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(20, 7, 'Interior Designer', 1, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-13', '2015-04-14', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'n', 'n', 1, 'jobdetails'),
(21, 18, 'Personal Assistant', 1, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-18', '2015-05-01', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(22, 6, 'Data Entry Operator', 3, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-18', '2015-05-03', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(23, 6, 'Data Entry Operator', 3, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-18', '2015-05-03', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(24, 6, 'Data Entry Operator', 3, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-18', '2015-05-03', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(25, 1, 'Finance officer', 2, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-18', '2015-06-04', 'Nahidul', 'CTO', 'y', 2, 'PentaGems', 'n', 'y', 'n', 1, 'jobdetails_1'),
(26, 1, 'Finance officer', 2, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-18', '2015-06-04', 'Nahidul', 'CTO', 'y', 2, 'PentaGems', 'n', 'y', 'n', 1, 'jobdetails_1'),
(27, 15, 'medical officer', 5, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'n', 'na', '2015-04-18', '2015-06-05', 'Nahidul', 'CTO', 'y', 2, 'PentaGems', 'n', 'y', 'n', 1, 'jobdetails_1'),
(28, 15, 'medical officer', 5, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'n', 'na', '2015-04-18', '2015-06-05', 'Nahidul', 'CTO', 'y', 2, 'PentaGems', 'n', 'y', 'n', 1, 'jobdetails_1'),
(29, 7, 'Interior Designer', 1, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-18', '2015-05-20', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'n', 'n', 1, 'jobdetails'),
(30, 13, 'Game Developer', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-18', '2015-04-30', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(31, 11, 'admin ', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'n', 'na', '2015-04-18', '2015-07-12', 'Meverick Inc.', '', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(32, 6, 'Data Entry Operator', 3, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-18', '2015-04-30', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(33, 13, 'IT Officer', 2, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-21', '2015-08-14', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'y', 'n', 1, 'jobdetails_1'),
(34, 13, 'Software Developer', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'gjgj', '2015-04-21', '2015-04-30', 'Meverick Inc.', '', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(35, 11, 'General Manager', 2, 'n', 'y', 'nahid.cuet09@gmail.com', 'y', 'n', 'test', '2015-04-21', '2015-04-27', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'n', 'n', 1, 'jobdetails'),
(36, 13, 'IT Engineer', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-21', '2015-04-30', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(37, 13, 'IT Engineer', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-21', '2015-04-30', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(38, 12, 'HRM', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'MSC/MBA', '2015-04-22', '2015-04-30', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(39, 13, 'Trainee Software Engineer', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-22', '2015-05-01', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'n', 'n', 1, 'jobdetails'),
(40, 13, 'Game Developer', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-22', '2015-06-01', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(41, 13, 'IT Engineer', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-22', '2015-06-11', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(42, 7, 'Interior Designer', 1, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-22', '2015-05-01', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'n', 'n', 1, 'jobdetails'),
(43, 18, 'Waiter', 8, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-22', '2015-06-24', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'y', 'n', 1, 'jobdetails_2'),
(44, 12, 'HRM', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'MSC/MBA', '2015-04-22', '2015-04-30', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(45, 18, 'PS', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-22', '2015-04-30', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(46, 7, 'UI Designer', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'y', 'N/A', '2015-04-22', '2015-05-02', 'Nahidul', 'CTO', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(47, 6, 'data entry', 2, 'n', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'test', '2015-04-22', '2015-05-30', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'y', 'n', 1, 'jobdetails_1'),
(48, 11, 'admin ', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'n', 'na', '2015-04-22', '2015-05-01', 'Meverick Inc.', '', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(49, 11, 'admin ', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'n', 'na', '2015-04-22', '2015-05-01', 'Meverick Inc.', '', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(50, 11, 'admin ', 2, 'y', 'y', 'nahid.cuet09@gmail.com', 'y', 'n', 'na', '2015-04-22', '2015-05-01', 'Meverick Inc.', '', 'y', 2, 'Meverick Inc.', 'n', 'y', 'n', 1, 'jobdetails_1'),
(51, 12, 'HRM', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'MSC/MBA', '2015-04-22', '2015-06-12', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(52, 12, 'HRM', 5, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'MSC/MBA', '2015-04-22', '2015-06-12', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(53, 16, 'Field Officer', 10, 'y', 'y', 'sanjoy.ict@gmail.com', 'n', 'n', 'NA', '2015-04-22', '2015-04-30', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(54, 12, 'Human Resources', 1, 'n', 'y', 'hawauljannat@gmail.com', 'y', 'n', 'test', '2015-04-22', '2015-08-31', 'ctgsoftware', '', 'y', 5, 'ctgsoftware', 'n', 'y', 'n', 1, 'jobdetails_1'),
(55, 12, 'Human Resources', 1, 'n', 'y', 'hawauljannat@gmail.com', 'y', 'n', 'test', '2015-04-22', '2015-04-23', 'ctgsoftware', '', 'y', 5, 'ctgsoftware', 'n', 'y', 'n', 1, 'jobdetails_1'),
(56, 10, 'General Manager', 1, 'y', 'y', 'hawauljannat@gmail.com', 'y', 'n', 'Canditate must enclose his/her photograph with his/her resume\n\nIf you feel you are the right person for the above positions, please send your application along with C.V & 2 recent passport size photographs to HR & Admin Department.', '2015-04-23', '2015-06-16', 'nahid', 'Software Engg.', 'y', 5, 'ctgsoftware', 'n', 'y', 'n', 1, 'jobdetails_1'),
(57, 14, 'Marketing Executive', 2, 'n', 'y', 'hawauljannat@gmail.com', 'y', 'n', 'n/a', '2015-04-23', '2015-09-19', 'nahid', 'Software Engg.', 'y', 5, 'ctgsoftware', 'n', 'y', 'n', 0, 'jobdetails_1'),
(58, 14, 'Marketing Executive', 2, 'n', 'y', 'hawauljannat@gmail.com', 'y', 'n', 'n/a', '2015-04-23', '2015-04-29', 'nahid', 'Software Engg.', 'y', 5, 'ctgsoftware', 'n', 'y', 'n', 1, 'jobdetails_1'),
(59, 3, 'Bank/Insurance/Leasing', 1, 'n', 'y', 'hawauljannat@gmail.com', 'y', 'n', 'Canditate must enclose his/her photograph with his/her resume', '2015-04-23', '2013-04-13', 'nahid', 'Software Engg.', 'y', 5, 'ctgsoftware', 'n', 'y', 'n', 1, 'jobdetails_1'),
(60, 13, 'Senior Executive Officer', 10, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-23', '2015-06-04', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'n', 'n', 1, 'jobdetails'),
(61, 13, 'Java/J2EE Developer', 10, 'y', 'y', 'sanjoy.ict@gmail.com', 'y', 'n', 'NA', '2015-04-23', '2015-06-07', 'sanjoy', 'se', 'y', 1, 'pg', 'n', 'y', 'n', 1, 'jobdetails_1'),
(62, 2, 'Event Mgt.', 1, 'y', 'y', 'hawauljannat@gmail.com', 'y', 'n', 'N/A', '2015-04-23', '2015-05-18', 'nahid', 'Software Engg.', 'y', 5, 'ctgsoftware', 'n', 'y', 'n', 1, 'jobdetails_1');

-- --------------------------------------------------------

--
-- Table structure for table `emp_requirement_s2`
--

CREATE TABLE IF NOT EXISTS `emp_requirement_s2` (
  `emp_job_id` int(11) NOT NULL AUTO_INCREMENT,
  `age_from` int(11) DEFAULT NULL,
  `age_to` int(11) DEFAULT NULL,
  `gender` enum('m','f','a') DEFAULT NULL,
  `job_type` varchar(10) DEFAULT NULL,
  `job_level` varchar(20) DEFAULT NULL,
  `edu_qualification` varchar(250) DEFAULT NULL,
  `job_responsibility` varchar(250) DEFAULT NULL,
  `add_job_requirement` varchar(250) DEFAULT NULL,
  `experience_min` int(5) DEFAULT NULL,
  `experience_max` int(5) DEFAULT NULL,
  `optJobLocationType` int(50) NOT NULL,
  `job_location` varchar(50) DEFAULT NULL,
  `optBenefits` int(10) DEFAULT NULL,
  `salary_min` decimal(10,0) DEFAULT NULL,
  `salary_max` decimal(10,0) DEFAULT NULL,
  `other_benefits` varchar(250) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`emp_job_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `emp_requirement_s2`
--

INSERT INTO `emp_requirement_s2` (`emp_job_id`, `age_from`, `age_to`, `gender`, `job_type`, `job_level`, `edu_qualification`, `job_responsibility`, `add_job_requirement`, `experience_min`, `experience_max`, `optJobLocationType`, `job_location`, `optBenefits`, `salary_min`, `salary_max`, `other_benefits`, `is_active`) VALUES
(2, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(3, -1, -1, 'a', 'Full time', 'Entry', 'MSC/MBA', 'NA', 'NA', 3, 10, 0, '3', 1, '0', '0', 'n/a', 1),
(4, 24, 27, 'a', 'Full time', 'Entry', 'MBA', 'NA', 'Computer Proficiency', 5, 10, -1, '-1', 1, '0', '0', 'n/a', 1),
(5, -1, -1, 'a', 'Full time', 'Entry', 'MBA', 'NA', 'NA', 0, 6, 0, '63', 1, '0', '0', 'n/a', 1),
(6, -1, -1, 'a', 'Full time', 'Entry', 'MBA', 'NA', 'NA', 0, 6, 0, '63', 1, '0', '0', 'n/a', 1),
(7, -1, -1, 'a', 'Full time', 'Entry', 'MBA', 'NA', 'NA', 0, 6, 0, '63', 1, '0', '0', 'n/a', 1),
(8, -1, -1, 'a', 'Full time', 'Entry', 'SSC &HSC', 'NA', 'Honest', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(9, 32, 28, 'a', 'Full time', 'Entry', 'ACCA/CA', 'NA', 'Hardworking', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(10, 24, 36, 'a', 'Full time', 'Entry', 'HSC', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(11, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(12, 24, 31, 'a', 'Full time', 'Entry', 'MBA', 'NA', 'Honest', 0, 0, 0, '14', 1, '0', '0', 'n/a', 1),
(13, 25, 29, 'a', 'Full time', 'Entry', 'B.Sc in CSE', 'Developing Game', 'N/A', 0, 0, 0, '10', 1, '0', '0', 'n/a', 1),
(14, 25, 29, 'a', 'Full time', 'Entry', 'MBA in Marketing', 'Develop marketing plan', 'N/A', 0, 0, 0, '14', 1, '0', '0', 'n/a', 1),
(15, 25, 29, 'a', 'Full time', 'Entry', 'B.Sc in CSE', 'N/A', 'N/A', 0, 2, -1, '-1', 1, '0', '0', 'n/a', 1),
(16, 24, 26, 'f', 'Full time', 'Entry', 'Graduate', 'N/A', 'N/A', 0, 1, -1, '-1', 3, '0', '0', 'n/a', 1),
(17, 24, 26, 'f', 'Part time', 'Entry', 'Graduate', 'N/A', 'N/A', 0, 1, 0, '10', 1, '0', '0', 'n/a', 1),
(18, 30, 35, 'm', 'Full time', 'Mid', 'B.Sc in ME, EEE, Civil', 'N/A', 'N/A', 3, 7, 0, '14', 1, '0', '0', 'n/a', 1),
(19, 25, 30, 'a', 'Part time', 'Entry', 'Graduate in Fine Arts', 'N/A', 'N/A', 0, 1, -1, '-1', 1, '0', '0', 'n/a', 1),
(20, 26, 30, 'a', 'Contract', 'Entry', 'Graduate in Fine Arts', 'Designing the interior for a new office', 'N/A', 0, 1, 0, '10', 1, '0', '0', 'n/a', 1),
(21, 23, 28, 'f', 'Full time', 'Entry', 'Graduate', 'Taking notes, Keep calender', 'Fluent in english', 0, 0, 0, '14', 1, '0', '0', 'n/a', 1),
(22, 24, 27, 'a', 'Part time', 'Entry', 'Minimum H.S.C', 'N/A', 'N/A', 0, 0, 0, '19', 1, '0', '0', 'n/a', 1),
(23, 24, 27, 'a', 'Part time', 'Entry', 'Minimum H.S.C', 'N/A', 'N/A', -1, -1, 0, '19', 1, '0', '0', 'n/a', 1),
(24, 24, 27, 'a', 'Part time', 'Entry', 'Minimum H.S.C', 'N/A', 'N/A', -1, -1, 0, '19', 1, '0', '0', 'n/a', 1),
(25, 21, 25, 'a', 'Full time', 'Entry', 'NA', 'NA', 'na', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(26, 21, 25, 'a', 'Full time', 'Entry', 'NA', 'NA', 'na', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(27, 21, 30, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 13, 8, -1, '-1', 1, '0', '0', 'n/a', 1),
(28, 21, 30, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 13, 8, -1, '-1', 1, '0', '0', 'n/a', 1),
(29, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(30, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(31, 23, 32, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(32, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(33, 20, 30, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 2, 26, -1, '-1', 1, '0', '0', 'n/a', 1),
(34, 22, -1, 'a', 'Full time', 'Entry', 'CSE', 'test', 'test', 0, 1, -1, '-1', 3, '10000', '20000', 'n/a', 1),
(35, 22, -1, 'a', 'Full time', 'Entry', 'BBA', 'test', 'test', 0, 1, -1, '-1', 3, '20000', '30000', 'n/a', 1),
(36, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(37, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(38, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(39, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(40, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(41, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(42, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(43, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(44, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(45, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(46, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(47, 22, -1, 'a', 'Full time', 'Entry', 'hsc', 'data entry', 'test', 0, 0, -1, '-1', 3, '8000', '1000', 'n/a', 1),
(48, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(49, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(50, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(51, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(52, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(53, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(54, 25, 28, 'a', 'Full time', 'Mid', 'BBA,MBA', 'Human Resources', '0', 0, 1, -1, '-1', 3, '20000', '30000', 'n/a', 1),
(55, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(56, 26, 32, 'm', 'Full time', 'Mid', 'Graduate in pattern making/ garments technology/ Graduate in Fashion Designing from BUET.\nB.Sc./ M.Sc. in garments technology will be given preference.', 'Not applicable\n', 'Not applicable\n', 1, 3, 0, '49', 1, '0', '0', 'n/a', 1),
(57, -1, -1, 'm', 'Full time', 'Entry', 'N/A', 'Not Applicable', 'Not Applicable', -1, -1, -1, '-1', 3, '15000', '20000', 'n/a', 1),
(58, -1, -1, 'a', 'Full time', 'Entry', 'NA', 'NA', 'NA', 0, 0, -1, '-1', 1, '0', '0', 'n/a', 1),
(59, 27, 31, 'a', 'Full time', 'Top', 'BBA,MBA.', 'Advising the board on effective decision-making, legal and regulatory matters and risk management.\nDeveloping and overseeing the systems that ensure the company complies with all applicable codes, in addition to its legal and statutory requirements.\n', 'n/a', 4, 5, 0, '29', 1, '0', '0', 'n/a', 1),
(60, 20, 30, 'a', 'Full time', 'Entry', 'BSC', 'BSC', 'NA', 10, 15, 0, '10', 1, '0', '0', 'n/a', 1),
(61, 20, 30, 'a', 'Full time', 'Entry', 'MSC in CSE', 'Confident & Team Worker', 'NA', 9, 17, 0, '1', 1, '0', '0', 'n/a', 1),
(62, -1, -1, 'f', 'Full time', 'Entry', 'Graduate in any subject.', 'N/A', 'N/A', 0, 0, 0, '62', 1, '0', '0', 'N/A', 1);

-- --------------------------------------------------------

--
-- Table structure for table `emp_voucher`
--

CREATE TABLE IF NOT EXISTS `emp_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_no` varchar(100) NOT NULL,
  `company_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `vat` decimal(10,0) NOT NULL,
  `discount` int(100) NOT NULL,
  `create_date` datetime NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `is_paid` int(11) NOT NULL,
  `is_publish` int(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `emp_voucher`
--

INSERT INTO `emp_voucher` (`id`, `voucher_no`, `company_id`, `job_id`, `amount`, `vat`, `discount`, `create_date`, `from_date`, `to_date`, `is_paid`, `is_publish`, `status`) VALUES
(2, '{B86321D3-E191-6F6B-9392-DA3F82A7EFF7}', 1, 2, '1500', '225', 0, '2015-04-08 04:44:12', '2015-04-08 04:44:12', '2015-09-14 00:00:00', 0, 1, 1),
(3, '{C52ABE96-1D75-23BA-7BD1-9EE9EE184C67}', 1, 3, '1500', '225', 0, '2015-04-09 06:14:22', '2015-04-09 06:14:22', '2015-07-06 00:00:00', 0, 1, 1),
(4, '{92C6F39F-0DEE-68C5-093E-7E7D034567AE}', 1, 4, '1500', '225', 0, '2015-04-09 06:29:48', '2015-04-09 06:29:48', '2015-07-05 00:00:00', 0, 1, 1),
(5, '{50711C5B-930C-4432-3951-7836FD83D672}', 1, 5, '1500', '225', 0, '2015-04-09 06:48:19', '2015-04-09 06:48:19', '2015-05-10 00:00:00', 0, 1, 1),
(6, '{435EB47A-1B92-33AD-F440-9BFAB2008465}', 1, 6, '1500', '225', 0, '2015-04-09 06:56:28', '2015-04-09 06:56:28', '2015-05-10 00:00:00', 0, 1, 1),
(7, '{00CB84E6-43E0-2AA9-AB57-7A04382E6A6F}', 1, 7, '1500', '225', 0, '2015-04-09 07:01:10', '2015-04-09 07:01:10', '2015-05-10 00:00:00', 0, 1, 1),
(8, '{F2C4F382-5AC6-CB4C-C2C6-418BB082445F}', 1, 8, '1500', '225', 0, '2015-04-09 07:14:27', '2015-04-09 07:14:27', '2015-06-10 00:00:00', 0, 1, 1),
(9, '{C1ECBD94-7A58-1587-F82A-BCD550EDE6DD}', 1, 9, '2500', '375', 0, '2015-04-09 07:24:22', '2015-04-09 07:24:22', '2015-06-10 00:00:00', 0, 1, 1),
(10, '{4FDB36B1-FF15-0723-032C-D42C80FBABBD}', 1, 10, '2500', '375', 0, '2015-04-09 07:27:38', '2015-04-09 07:27:38', '2015-05-11 00:00:00', 0, 1, 1),
(11, '{7DA1341B-BF6A-DD46-EC8D-571B8A9088A5}', 1, 11, '1000', '150', 0, '2015-04-09 08:21:29', '2015-04-09 08:21:29', '2015-07-01 00:00:00', 0, 1, 1),
(12, '{A01058E0-869C-FD32-F2BB-72510208E7EF}', 1, 12, '1500', '225', 200, '2015-04-09 08:29:18', '2015-04-09 08:29:18', '2015-06-10 00:00:00', 0, 0, 1),
(13, '{A3A5DD8E-CDAA-7EE8-302F-A6B184AA03BF}', 2, 13, '2500', '375', 0, '2015-04-09 08:56:38', '2015-04-09 08:56:38', '2015-05-10 00:00:00', 0, 1, 1),
(14, '{69FF4B2E-999B-154E-FAEE-843A84C42CE3}', 2, 14, '1500', '225', 0, '2015-04-09 09:02:54', '2015-04-09 09:02:54', '2015-06-10 00:00:00', 0, 1, 1),
(15, '{480A8BCB-AC50-9EC1-AD69-33DD863C0C03}', 2, 15, '1500', '225', 0, '2015-04-09 10:51:08', '2015-04-09 10:51:08', '2015-04-16 00:00:00', 0, 1, 1),
(16, '{1260C633-A76B-1413-512F-CA4FB4F397CB}', 2, 16, '1500', '225', 0, '2015-04-09 01:14:30', '2015-04-09 01:14:30', '2015-05-09 00:00:00', 0, 1, 1),
(17, '{351E6DD3-045C-8F97-982C-067C4043047F}', 2, 17, '1500', '225', 0, '2015-04-09 01:14:47', '2015-04-09 01:14:47', '2015-05-09 00:00:00', 0, 0, 1),
(18, '{01B3660B-4540-1D24-56B7-0B8D763D5D84}', 2, 18, '2500', '375', 0, '2015-04-09 01:20:55', '2015-04-09 01:20:55', '2015-05-12 00:00:00', 0, 1, 1),
(19, '{381E1D99-A43B-039A-DAB5-35F7F9A00B60}', 2, 19, '2500', '375', 0, '2015-04-09 01:30:19', '2015-04-09 01:30:19', '2015-05-14 00:00:00', 0, 1, 1),
(20, '{068814C4-A172-3327-D22C-E6808DBFB785}', 2, 20, '1500', '225', 0, '2015-04-13 06:19:45', '2015-04-13 06:19:45', '2015-04-14 00:00:00', 0, 1, 1),
(21, '{8D7FBAD0-44CA-E6F6-3208-D5F8B5B17941}', 2, 21, '2500', '375', 0, '2015-04-18 06:23:22', '2015-04-18 06:23:22', '2015-05-01 00:00:00', 0, 0, 1),
(22, '{BEA93D3E-FF2B-3356-1F8C-3829EB47CFD4}', 2, 22, '2500', '375', 0, '2015-04-18 01:12:08', '2015-04-18 01:12:08', '2015-05-03 00:00:00', 0, 0, 1),
(23, '{7123A947-F1C4-B726-1CFD-681A0CAE9B5D}', 2, 23, '2500', '375', 0, '2015-04-18 01:26:53', '2015-04-18 01:26:53', '2015-05-03 00:00:00', 0, 0, 1),
(24, '{3F5115EE-6AD7-1BFD-F3B9-842D157BC269}', 2, 24, '2500', '375', 0, '2015-04-18 01:32:30', '2015-04-18 01:32:30', '2015-05-03 00:00:00', 0, 0, 1),
(25, '{C50BE2B1-84C8-80D1-9699-5F7A54A3825B}', 2, 25, '2500', '375', 0, '2015-04-18 01:39:01', '2015-04-18 01:39:01', '2015-06-04 00:00:00', 0, 0, 1),
(26, '{8EC4BB88-C125-BD36-DB3D-5AEE66A9B52F}', 2, 26, '2500', '375', 0, '2015-04-18 01:51:35', '2015-04-18 01:51:35', '2015-06-04 00:00:00', 0, 0, 1),
(27, '{22E0A3B7-6DAB-8608-80B9-95F2DDD9B282}', 2, 27, '2500', '375', 0, '2015-04-18 02:01:13', '2015-04-18 02:01:13', '2015-06-05 00:00:00', 0, 0, 1),
(28, '{415EFA0A-D1F3-3EC7-41D5-6D7F477A610C}', 2, 28, '2500', '375', 0, '2015-04-18 02:02:20', '2015-04-18 02:02:20', '2015-06-05 00:00:00', 0, 0, 1),
(29, '{5C9D35F1-D6D6-81CA-731A-50A08ECC76F1}', 2, 29, '1000', '150', 0, '2015-04-18 02:02:23', '2015-04-18 02:02:23', '2015-05-20 00:00:00', 0, 0, 1),
(30, '{6B7CC1AC-48D1-9E4F-A5B0-E43C8F38D4A8}', 2, 30, '1000', '150', 0, '2015-04-18 02:05:56', '2015-04-18 02:05:56', '2015-04-30 00:00:00', 0, 0, 1),
(31, '{50B85D7A-A1C1-808F-52C8-BA6C9B4288E8}', 2, 31, '2500', '375', 0, '2015-04-18 02:09:04', '2015-04-18 02:09:04', '2015-07-12 00:00:00', 0, 0, 1),
(32, '{563A79EF-3896-B59B-CFD3-C495C2D56593}', 2, 32, '1000', '150', 0, '2015-04-18 02:09:35', '2015-04-18 02:09:35', '2015-04-30 00:00:00', 0, 0, 1),
(33, '{D78C05C9-55AB-7C35-4941-C51A0652C4A9}', 1, 33, '2500', '375', 200, '2015-04-21 06:19:44', '2015-04-21 06:19:44', '2015-08-14 00:00:00', 0, 1, 1),
(34, '{FCC1C957-B966-2D0A-9878-F6CFF1F3B7D4}', 2, 34, '2500', '375', 0, '2015-04-21 06:26:52', '2015-04-21 06:26:52', '2015-04-30 00:00:00', 0, 1, 1),
(35, '{7D0F1DBD-B90C-F794-1C86-9C2F010F4DCE}', 2, 35, '1500', '225', 0, '2015-04-21 06:37:20', '2015-04-21 06:37:20', '2015-04-27 00:00:00', 0, 1, 1),
(36, '{22D7FF50-CBA8-0F30-F76A-B0178FCB9ECA}', 1, 36, '1000', '150', 200, '2015-04-21 11:24:30', '2015-04-21 11:24:30', '2015-04-30 00:00:00', 0, 0, 1),
(37, '{F79DC9F3-3BCB-423D-23FE-31036B1590C0}', 1, 37, '1000', '150', 200, '2015-04-21 11:36:56', '2015-04-21 11:36:56', '2015-04-30 00:00:00', 0, 0, 1),
(38, '{0F2E9139-2606-A3F1-42A8-3FD94F4343CE}', 1, 38, '1000', '150', 200, '2015-04-22 06:01:11', '2015-04-22 06:01:11', '2015-04-30 00:00:00', 0, 0, 1),
(39, '{BCCE2DED-07A3-2792-1A70-56EEF7B6166A}', 2, 39, '1000', '150', 0, '2015-04-22 06:02:43', '2015-04-22 06:02:43', '2015-05-01 00:00:00', 0, 1, 1),
(40, '{25BF0FF1-F088-40E1-CB65-C98EFB8FA760}', 2, 40, '1000', '150', 0, '2015-04-22 06:06:01', '2015-04-22 06:06:01', '2015-06-01 00:00:00', 0, 0, 1),
(41, '{1D5782B8-80F4-76EF-0183-35BFA052647A}', 1, 41, '1000', '150', 200, '2015-04-22 06:15:54', '2015-04-22 06:15:54', '2015-06-11 00:00:00', 0, 0, 1),
(42, '{B53E8E14-6737-A613-24A6-72BA73ACA73D}', 2, 42, '1000', '150', 0, '2015-04-22 06:16:29', '2015-04-22 06:16:29', '2015-05-01 00:00:00', 0, 0, 1),
(43, '{3E9F3FDD-FCEF-9B98-27E0-F4878037F508}', 1, 43, '1000', '150', 200, '2015-04-22 06:17:28', '2015-04-22 06:17:28', '2015-06-24 00:00:00', 0, 0, 1),
(44, '{6DE76DC3-26DB-DA81-45EF-1480AB5D43BD}', 1, 44, '1000', '150', 200, '2015-04-22 06:19:56', '2015-04-22 06:19:56', '2015-04-30 00:00:00', 0, 0, 1),
(45, '{1A1310C2-D33E-EBD8-80E8-ECA7EC625A4A}', 1, 45, '1000', '150', 200, '2015-04-22 06:21:17', '2015-04-22 06:21:17', '2015-04-30 00:00:00', 0, 0, 1),
(46, '{88367D54-9767-E577-772D-40D7992D47C3}', 2, 46, '1000', '150', 0, '2015-04-22 06:22:05', '2015-04-22 06:22:05', '2015-05-02 00:00:00', 0, 1, 1),
(47, '{68E46795-8D63-C838-267A-F327E7A69CED}', 1, 47, '2500', '375', 200, '2015-04-22 06:26:52', '2015-04-22 06:26:52', '2015-05-30 00:00:00', 0, 1, 1),
(48, '{5291BA81-C122-0FBC-6A57-392380F6F722}', 2, 48, '1000', '150', 0, '2015-04-22 06:33:31', '2015-04-22 06:33:31', '2015-05-01 00:00:00', 0, 0, 1),
(49, '{698C2115-DE63-627E-F556-E1436EEC99FF}', 2, 49, '1000', '150', 0, '2015-04-22 06:33:35', '2015-04-22 06:33:35', '2015-05-01 00:00:00', 0, 0, 1),
(50, '{03C17D88-F6F4-3861-AF7D-42D8CD17FECC}', 2, 50, '1000', '150', 0, '2015-04-22 06:33:50', '2015-04-22 06:33:50', '2015-05-01 00:00:00', 0, 0, 1),
(51, '{00182774-332D-ABF4-310A-F4BF36CBB940}', 1, 51, '1000', '150', 200, '2015-04-22 06:34:15', '2015-04-22 06:34:15', '2015-06-12 00:00:00', 0, 0, 1),
(52, '{4B725966-EA7F-0B8E-A586-F3AD715A7C26}', 1, 52, '1000', '150', 200, '2015-04-22 06:35:56', '2015-04-22 06:35:56', '2015-06-12 00:00:00', 0, 0, 1),
(53, '{C62336EF-03B3-BBAF-1357-84E0AC03BC0E}', 1, 53, '1000', '150', 200, '2015-04-22 06:41:27', '2015-04-22 06:41:27', '2015-04-30 00:00:00', 0, 0, 1),
(54, '{6365C7ED-0538-04EC-844F-E84DD1C333F9}', 5, 54, '2500', '375', 0, '2015-04-22 06:50:55', '2015-04-22 06:50:55', '2015-08-31 00:00:00', 0, 0, 1),
(55, '{38D1D747-C752-99FF-DA7F-08F29312E189}', 5, 55, '1000', '150', 0, '2015-04-22 06:55:43', '2015-04-22 06:55:43', '2015-04-23 00:00:00', 0, 0, 1),
(56, '{EB30438E-386D-DCD9-ED15-A09A38E8D6A0}', 5, 56, '2500', '375', 0, '2015-04-23 12:19:17', '2015-04-23 12:19:17', '2015-06-16 00:00:00', 0, 1, 1),
(57, '{79C44817-D709-155F-833F-733B56BEA43A}', 5, 57, '2500', '375', 0, '2015-04-23 12:30:07', '2015-04-23 12:30:07', '2015-05-19 00:00:00', 0, 1, 1),
(58, '{10E5CF99-FA6A-1674-E2B7-443302C0D5DF}', 5, 58, '1000', '150', 0, '2015-04-23 12:54:25', '2015-04-23 12:54:25', '2015-04-29 00:00:00', 0, 1, 1),
(59, '{82A36EBF-1FB4-947A-FC22-77D05876444E}', 5, 59, '2500', '375', 0, '2015-04-23 01:09:43', '2015-04-23 01:09:43', '2013-04-13 00:00:00', 0, 1, 1),
(60, '{669AF6FF-0620-7BBE-CCE7-CAFC0E38A962}', 1, 60, '1500', '225', 200, '2015-04-23 05:14:41', '2015-04-23 05:14:41', '2015-06-04 00:00:00', 0, 1, 1),
(61, '{59D16E90-9988-0965-63DE-422E6B006D9D}', 1, 61, '2500', '375', 200, '2015-04-23 05:21:31', '2015-04-23 05:21:31', '2015-06-07 00:00:00', 0, 1, 1),
(62, '{41C9FC68-A736-39F0-B3C4-519CA6899685}', 5, 62, '2500', '375', 0, '2015-04-23 05:57:07', '2015-04-23 05:57:07', '2015-05-18 00:00:00', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exam_result`
--

CREATE TABLE IF NOT EXISTS `exam_result` (
  `exam_id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_name` varchar(50) NOT NULL,
  `publish_date` date NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `size` varchar(50) NOT NULL,
  `path` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`exam_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `family_religion`
--

CREATE TABLE IF NOT EXISTS `family_religion` (
  `id` int(11) NOT NULL,
  `father_name` varchar(150) NOT NULL,
  `father_status` varchar(20) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `mother_status` varchar(20) NOT NULL,
  `brother_no` int(11) NOT NULL,
  `bro_married` int(11) NOT NULL,
  `sister_no` int(11) NOT NULL,
  `sis_married` int(11) NOT NULL,
  `native_place` varchar(200) NOT NULL,
  `afflence_level` varchar(200) NOT NULL,
  `family_values` varchar(20) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `mother_tonge` varchar(20) NOT NULL,
  `caste` varchar(150) NOT NULL,
  `religious_type` varchar(150) NOT NULL,
  `religious_birth` varchar(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` longtext NOT NULL,
  `date` date NOT NULL,
  `ip` varchar(50) NOT NULL,
  `mac` varchar(50) NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `name`, `email`, `message`, `date`, `ip`, `mac`, `is_active`) VALUES
(1, 'A', 'sanjoy.ict@gmail.com', 's', '2015-03-14', '::1', '\r\n   DHCP Enabled', 1),
(2, 'SANJOY', 'sanjoy.ict@gmail.com', 'Hi! How are you.........', '2015-03-14', '::1', '\r\n   DHCP Enabled', 1),
(3, 'MD Nazrul Islam Khan(BUET)', 'sukanta.mbsu@il.com', 'Thank You...', '2015-03-14', '::1', '\r\n   DHCP Enabled', 1),
(4, 'Jannat', 'hawauljannat@hotmail.com', 'test', '2015-04-23', '180.234.141.39', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `foreignjob`
--

CREATE TABLE IF NOT EXISTS `foreignjob` (
  `f_id` int(10) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(500) NOT NULL,
  `job_name` varchar(100) NOT NULL,
  `job_title` varchar(100) NOT NULL,
  `last_date` date NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  PRIMARY KEY (`f_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `groom_loc`
--

CREATE TABLE IF NOT EXISTS `groom_loc` (
  `id` int(11) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `residency_status` varchar(100) NOT NULL,
  `grew_up` varchar(50) NOT NULL,
  `ethinic_origin` varchar(100) NOT NULL,
  `zip` varchar(100) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groom_loc`
--

INSERT INTO `groom_loc` (`id`, `country`, `state`, `city`, `residency_status`, `grew_up`, `ethinic_origin`, `zip`, `is_active`) VALUES
(1, 'Bangladesh', 'Chittagong (Chittagong)', 'Chittagong', 'Citizen', '', 'Bangladesh', '4000', 1),
(4, '', '', '', '', '', '', '', 1),
(5, 'Bangladesh', 'Chittagong', 'Chittagong', 'Citizen', '', 'Bangladesh', '4000', 1),
(7, 'Bangladesh', '', '', '', '', '', '', 1),
(9, 'Bangladesh', 'Chittagong', 'Chittagong', 'Citizen', '', 'Bangladesh', '4000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hire_job`
--

CREATE TABLE IF NOT EXISTS `hire_job` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `expert` longtext NOT NULL,
  `expert_area` longtext NOT NULL,
  `rate` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `nid` varchar(50) NOT NULL,
  `location` longtext NOT NULL,
  `free_time` text NOT NULL,
  `img_name` varchar(50) NOT NULL,
  `img_size` varchar(50) NOT NULL,
  `img_type` varchar(20) NOT NULL,
  `img_path` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `hire_job`
--

INSERT INTO `hire_job` (`id`, `name`, `expert`, `expert_area`, `rate`, `email`, `mobile`, `nid`, `location`, `free_time`, `img_name`, `img_size`, `img_type`, `img_path`, `date`, `is_active`) VALUES
(1, 'Mr. Henry', 'Photographer', '0', '25$', 'rifat_callin@yahoo.com', '12345677', '123456', 'Chittagong', 'Sun(7pm to 11pm)\r\nThu(7pm to 11pm)', 'F3AE00DD-8F92-D7D2-EAFA-AA3F4AFC4300.jpg', '5696', 'image/jpeg', '../hier_job_images/F3AE00DD-8F92-D7D2-EAFA-AA3F4AF', '2014-04-01', 1),
(2, 'Mr.Wakar', 'Graphics Design,Photoshop,Illustrator,', '0', '50$', 'rifat_callin@yahoo.com', '1234567890', '123', 'Agrabad,Chittagong', 'Sun,Thu,Fri\r\n(7pm to 11pm)', 'E6F9B2E6-4B6C-5F8C-BD05-068AA2D9A62D.jpg', '9737', 'image/jpeg', '../hier_job_images/E6F9B2E6-4B6C-5F8C-BD05-068AA2D', '2014-04-01', 1),
(3, 'Abdur Rahman', 'Financial Consultant,\r\nManager DBBL,Dhaka', '0', '50$', 'rifat_callin@yahoo.com', '11111111111111111111', '1111111111111111111', 'Malibag,Dhaka', 'Fri-Sat\r\n(10.00 am to 8.00pm)', '1432AC1A-3FD7-0F0F-95A5-3401A6AA917E.jpg', '6927', 'image/jpeg', '../hier_job_images/1432AC1A-3FD7-0F0F-95A5-3401A6A', '2014-04-01', 1),
(4, 'Eakub Hider', 'Marketing Expert.\r\nExpert in real time market analysis,', '0', '60$', 'rifat_callin@yahoo.com', '11111111111111111', '11111111111111111', 'Agrabad,Chittagong', 'Sat-Sun-Thu\r\n(5.00pm to 8.00pm)', 'B0C79277-865E-6D68-B06B-B04B61019FAB.jpg', '7158', 'image/jpeg', '../hier_job_images/B0C79277-865E-6D68-B06B-B04B610', '2014-04-01', 1),
(5, 'Saifuddin Ahmed', 'Artical Writing,\r\nArtical Translator', '0', '20$', 'rifat_callin@yahoo.com', '11111111111111111', '11111111111111111', 'Halishohor,Ctg', 'Sun-Fri\r\n(5.00pm to 11.00pm)', 'EB5F71F7-80F7-6C7E-F6EC-6994E6D73623.jpg', '6496', 'image/jpeg', '../hier_job_images/EB5F71F7-80F7-6C7E-F6EC-6994E6D', '2014-04-01', 1),
(6, 'Blog Writer', 'Blog Writing in any tophics', '0', '10$', 'rifat_callin@yahoo.com', '222222222222', '222222222222222', 'Agrabad,Ctg', 'San-Fri\r\n(9.00 am to 12.00pm)', 'D1432B5A-12E4-5662-94D2-ABDDDC5142DB.jpg', '5258', 'image/jpeg', '../hier_job_images/D1432B5A-12E4-5662-94D2-ABDDDC5', '2014-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_academic`
--

CREATE TABLE IF NOT EXISTS `job_academic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) NOT NULL,
  `level_id` int(10) NOT NULL,
  `degree_title` varchar(100) NOT NULL,
  `major` varchar(100) NOT NULL,
  `institute` varchar(100) NOT NULL,
  `result` varchar(35) DEFAULT NULL,
  `passing_year` int(4) NOT NULL,
  `duration` varchar(11) DEFAULT NULL,
  `achievement` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `job_academic`
--

INSERT INTO `job_academic` (`id`, `employee_id`, `level_id`, `degree_title`, `major`, `institute`, `result`, `passing_year`, `duration`, `achievement`) VALUES
(1, 3, 1, 'sdfs', 'dfs', 'df', 'sdfs', 2008, 'sdf', 'sdf'),
(2, 11, 2, 'HSC', 'Science', 'CCC', '5.00', 2009, '2 years', 'excellent'),
(3, 3, 6, 'Beker Degree', 'Beker', 'Beker Bari Univeristy', 'Full Brighter', 2018, 'sdfs', ''),
(4, 3, 4, 'Kabil', 'kabil', 'kabil bari', 'ksdkjf', -1, 'jsakdjfk', '');

-- --------------------------------------------------------

--
-- Table structure for table `job_ac_major`
--

CREATE TABLE IF NOT EXISTS `job_ac_major` (
  `major_id` int(10) NOT NULL AUTO_INCREMENT,
  `major_name` varchar(200) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`major_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_ac_result`
--

CREATE TABLE IF NOT EXISTS `job_ac_result` (
  `result_id` int(10) NOT NULL AUTO_INCREMENT,
  `result_name` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`result_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_application`
--

CREATE TABLE IF NOT EXISTS `job_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `is_viewed` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `job_application`
--

INSERT INTO `job_application` (`id`, `job_id`, `emp_id`, `is_viewed`) VALUES
(1, 13, 11, 0),
(2, 34, 10, 0),
(3, 34, 5, 0),
(4, 33, 4, 0),
(5, 2, 3, 0),
(6, 33, 5, 0),
(7, 2, 5, 0),
(8, 5, 3, 0),
(9, 33, 3, 0),
(10, 4, 10, 0),
(11, 46, 5, 0),
(12, 19, 11, 0),
(13, 2, 11, 0),
(14, 61, NULL, 0),
(15, 61, 11, 0),
(16, 61, 2, 0),
(17, 62, 2, 0),
(18, 9, 11, 0),
(19, 54, 3, 0),
(20, 17, 3, 0),
(21, 4, 3, 0),
(22, 58, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_category`
--

CREATE TABLE IF NOT EXISTS `job_category` (
  `category_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `job_category`
--

INSERT INTO `job_category` (`category_id`, `category_name`, `is_active`) VALUES
(1, 'Accounting/Finance', 1),
(2, 'Advertisement/Event Mgt.', 1),
(3, 'Banking/Insurance/Leasing', 1),
(4, 'Commercial/Supply Chain', 1),
(5, 'Customer Support/Call Centre', 1),
(6, 'Data Entry/Operator/BPO', 1),
(7, 'Design/Creative', 1),
(8, 'Education/Training', 1),
(9, 'Engineer/Architect', 1),
(10, 'Garments/Textile', 1),
(11, 'General Management/Admin', 1),
(12, 'HR/Org. Development', 1),
(13, 'IT/Telecommunication', 1),
(14, 'Marketing/Sales', 1),
(15, 'Medical/Pharmaceutical', 1),
(16, 'NGO/Development', 1),
(17, 'Research/Consultancy', 1),
(18, 'Secretary/Receptionist', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_education_level`
--

CREATE TABLE IF NOT EXISTS `job_education_level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(25) NOT NULL,
  `is_active` enum('y','n') NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `job_education_level`
--

INSERT INTO `job_education_level` (`level_id`, `level_name`, `is_active`) VALUES
(1, 'Secondary', 'y'),
(2, 'Higher Secondary', 'y'),
(3, 'Diploma', 'y'),
(4, 'Bachelor/Honors', 'y'),
(5, 'Masters', 'y'),
(6, 'Doctoral', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `job_employee_login`
--

CREATE TABLE IF NOT EXISTS `job_employee_login` (
  `user_id` int(10) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `is_active` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_employee_login`
--

INSERT INTO `job_employee_login` (`user_id`, `user_name`, `password`, `email`, `is_active`) VALUES
(2, 'nusrat', '111111111111', 'hawauljannat@hotmail.com', 0),
(3, 'masud', '852852852', 'mrskas@yahoo.com', 0),
(4, 'sukanta', '12345678', 'sanjoy.ict@gmail.com', 0),
(5, 'shuva', '12345678', 'sanjoy.ict@gmail.com', 0),
(6, 'prakash', '12345678', 'sanjoy.ict@gmail.com', 0),
(7, 'jannat', '111111111111', 'a@gmail.com', 0),
(8, 'nahidul', '12345678', 'sanjoy.ict@gmail.com', 0),
(9, 'ashfi', '123456789123', 'hawauljannat@gmail.com', 0),
(10, 'test1', '123456789', 'a@gmail.com', 0),
(11, 'uzzal', '12345678', 'sanjoy.ict@gmail.com', 0),
(12, 'Ditan', '1234567890123', 'hawauljannat@hotmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_employment_history`
--

CREATE TABLE IF NOT EXISTS `job_employment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) NOT NULL,
  `company_name` varchar(200) NOT NULL,
  `company_business` varchar(100) NOT NULL,
  `company_location` varchar(200) DEFAULT NULL,
  `position_held` varchar(150) NOT NULL,
  `department` varchar(100) DEFAULT NULL,
  `area_of_experience` varchar(100) NOT NULL,
  `responsibilities` varchar(500) DEFAULT NULL,
  `from` date NOT NULL,
  `to` date DEFAULT NULL,
  `continuing` enum('y','n') DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_emp_category`
--

CREATE TABLE IF NOT EXISTS `job_emp_category` (
  `employee_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_emp_category`
--

INSERT INTO `job_emp_category` (`employee_id`, `category_id`) VALUES
(2, 7),
(3, 4),
(4, 1),
(5, 1),
(6, 3),
(7, 8),
(8, 1),
(9, 8),
(10, 8),
(11, 1),
(12, 8);

-- --------------------------------------------------------

--
-- Table structure for table `job_emp_country`
--

CREATE TABLE IF NOT EXISTS `job_emp_country` (
  `employee_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_emp_location`
--

CREATE TABLE IF NOT EXISTS `job_emp_location` (
  `employee_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_emp_preferred_org`
--

CREATE TABLE IF NOT EXISTS `job_emp_preferred_org` (
  `employee_id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_emp_preferred_org`
--

INSERT INTO `job_emp_preferred_org` (`employee_id`, `org_id`) VALUES
(3, 4),
(3, 3),
(4, 1),
(5, 1),
(6, 1),
(8, 1),
(11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_organization`
--

CREATE TABLE IF NOT EXISTS `job_organization` (
  `org_id` int(10) NOT NULL AUTO_INCREMENT,
  `oganization_name` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`org_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_other_extracurricular`
--

CREATE TABLE IF NOT EXISTS `job_other_extracurricular` (
  `id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `excategory_id` int(10) NOT NULL,
  `skill_description` varchar(250) DEFAULT NULL,
  `extracurricular_activities` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_other_language`
--

CREATE TABLE IF NOT EXISTS `job_other_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) NOT NULL,
  `language_name` varchar(50) DEFAULT NULL,
  `reading` varchar(10) DEFAULT NULL,
  `writing` varchar(10) DEFAULT NULL,
  `speaking` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `job_other_language`
--

INSERT INTO `job_other_language` (`id`, `employee_id`, `language_name`, `reading`, `writing`, `speaking`) VALUES
(1, 3, 'sdfsdf', 'High', 'High', 'High'),
(2, 3, 'Japani', 'High', 'High', 'High'),
(3, 3, 'Hindi', 'High', 'High', 'High'),
(4, 3, 'Arobi', 'High', 'High', 'High'),
(5, 3, 'korian', 'High', 'High', 'High'),
(6, 3, 'Vondami', 'High', 'High', 'High');

-- --------------------------------------------------------

--
-- Table structure for table `job_other_reference`
--

CREATE TABLE IF NOT EXISTS `job_other_reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) NOT NULL,
  `ref_name` varchar(150) DEFAULT NULL,
  `organization` varchar(150) DEFAULT NULL,
  `designation` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `phone_office` varchar(150) DEFAULT NULL,
  `phone_resident` varchar(150) DEFAULT NULL,
  `mobile` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `relation` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `job_other_reference`
--

INSERT INTO `job_other_reference` (`id`, `employee_id`, `ref_name`, `organization`, `designation`, `address`, `phone_office`, `phone_resident`, `mobile`, `email`, `relation`) VALUES
(1, 3, 'fsfd', 'fsdf', 'sdf', 'safasf', 'sa', '', 'sadfas', 'sadf', 'academic'),
(2, 3, 'sdfswerwqe', 'fsf', 'sdafae', 'dfs', 'sdfs', 'sadf', 'sdfsd', 'sadf', 'relative');

-- --------------------------------------------------------

--
-- Table structure for table `job_professional`
--

CREATE TABLE IF NOT EXISTS `job_professional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) NOT NULL,
  `certification` varchar(200) DEFAULT NULL,
  `institute` varchar(200) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `job_professional`
--

INSERT INTO `job_professional` (`id`, `employee_id`, `certification`, `institute`, `location`, `from`, `to`) VALUES
(1, 3, 'sdf', 'sdf', 'sdf', '2015-04-02', '2015-04-06'),
(2, 3, 'keta kon', 'keta kon', 'jsdkjk', '2015-04-15', '2015-04-23');

-- --------------------------------------------------------

--
-- Table structure for table `job_resume`
--

CREATE TABLE IF NOT EXISTS `job_resume` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `location_id` int(10) NOT NULL,
  `org_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_resume_career`
--

CREATE TABLE IF NOT EXISTS `job_resume_career` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `objective` varchar(500) DEFAULT NULL,
  `year_experience` float DEFAULT NULL,
  `present_salary` decimal(10,0) DEFAULT NULL,
  `expected_salary` decimal(10,0) DEFAULT NULL,
  `looking_for` varchar(10) DEFAULT NULL,
  `available_for` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `job_resume_career`
--

INSERT INTO `job_resume_career` (`id`, `user_id`, `objective`, `year_experience`, `present_salary`, `expected_salary`, `looking_for`, `available_for`) VALUES
(1, 2, 'hldh dli', 0, '0', '15000', 'Entry', 'Full Time'),
(2, 3, 'sdfs', 0, '0', '0', 'Entry', 'Full Time'),
(3, 4, 'NA', 1, '1', '2', 'Entry', 'Full Time'),
(4, 5, 'NA', 1, '1', '1', 'Entry', 'Full Time'),
(5, 6, 'NA', 1, '1', '1', 'Entry', 'Full Time'),
(6, 7, 'test', 0, '0', '15000', 'Entry', 'Full Time'),
(7, 8, 'NA', 0, '0', '0', 'Entry', 'Full Time'),
(8, 9, 'test', 0, '0', '15000', 'Entry', 'Full Time'),
(9, 10, 'test', 0, '0', '15000', 'Entry', 'Full Time'),
(10, 11, 'NA', 1, '1', '1', 'Entry', 'Full Time'),
(11, 12, 'qwe', 0, '0', '20000', 'Entry', 'Full Time');

-- --------------------------------------------------------

--
-- Table structure for table `job_resume_personal`
--

CREATE TABLE IF NOT EXISTS `job_resume_personal` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `gender` enum('m','f') DEFAULT NULL,
  `marital_status` enum('m','s') DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `religion` varchar(50) DEFAULT NULL,
  `present_add` varchar(250) DEFAULT NULL,
  `permanent_add` varchar(250) DEFAULT NULL,
  `current_loc` varchar(100) DEFAULT NULL,
  `home_phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `office_phone` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `alternate_email` varchar(100) DEFAULT NULL,
  `is_blue_worker` enum('y','n') DEFAULT 'n',
  `image` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `job_resume_personal`
--

INSERT INTO `job_resume_personal` (`id`, `name`, `father_name`, `mother_name`, `date_birth`, `gender`, `marital_status`, `nationality`, `religion`, `present_add`, `permanent_add`, `current_loc`, `home_phone`, `mobile`, `office_phone`, `email`, `alternate_email`, `is_blue_worker`, `image`) VALUES
(1, 'Jeny', 'Kamal', 'Afroza', '2015-04-15', 'f', 's', 'Bangladeshi', 'Islam', 'ctg', 'ctg', '10', '123', '1234', '4321', 'A@yahoo.com', '', 'n', NULL),
(2, 'nusrat', 'miron', 'afroza', '2015-04-20', 'f', 's', 'bangadeshi', 'islam', 'ctg', 'ctg', '10', '111111', '111', '1111', 'hawauljannat@gmail.com', '', 'n', NULL),
(3, 'kjsdjf', 'kajsdk', 'kjaskjfd', '2015-04-15', 'm', 's', 'sdhfk', 'k', 'jkjsdsdfs', 'ksdfs', '22', 'sdsfs', 'fsdfs', 'fsdfs', 'mrskas@yahoo.com', '', 'n', '3.jpg'),
(4, 'sukanta', 'Monoranjon Paul', 'Anita Paul', '2015-04-18', 'm', 's', 'Bangladeshi', 'Hindu', 'CTG', 'CTG', '9', '01954195589', '01954195589', '01954195589', 'sanjoy.ict@gmail.com', '', 'n', NULL),
(5, 'shuva', 'Monoranjon Paul', 'Anita Paul', NULL, 'm', 's', 'Bangladeshi', 'Hindu', 'CTG', 'CTG', '10', '01954195589', '01954195589', '01954195589', 'sanjoy.ict@gmail.com', '', 'n', NULL),
(6, 'joy', 'Monoranjon Paul', 'Anita Paul', '1990-04-24', 'm', 's', 'Bangladeshi', 'Hindu', 'CTG', 'CTG', '8', '01954195589', '00000', '01954195589', 'sanjoy.ict@gmail.com', '', 'n', NULL),
(7, 'Nusrat', 'Miron', 'Afroza', '2015-04-13', 'f', 's', 'bangadeshi', 'islam', 'ctg', '', '10', '123', '', '', 'a@gmail.com', '', 'n', NULL),
(8, 'prakash', 'M', 'A', '2015-04-23', 'm', 's', 'B', 'H', 'C', 'C', '10', '000000', '', '', 'sanjoy.ict@gmail.com', '', 'n', NULL),
(9, 'Kashfi', 'Main Uddin', 'Afroza', '2015-04-03', 'f', 's', 'bangadeshi', 'islam', 'ctg', 'ctg', '10', '1111', '', '', 'hawauljannat@gmail.com', '', 'n', NULL),
(10, 'Nusrat', 'Miron', 'Afroza', '2014-07-15', 'f', 's', 'bangadeshi', 'islam', 'ctg', '', '10', '111111111111', '', '', 'a@gmail.com', '', 'n', NULL),
(11, 'uzzal', 'U', 'U', '2015-04-02', 'm', 's', 'Bangladeshi', 'H', 'CTG', 'CTG', '7', '01954195589', '', '', 'sanjoy.ict@gmail.com', '', 'n', '11.jpg'),
(12, 'Ditan', 'Main Uddin ', 'Afroza Begum', '2013-04-02', 'f', 's', 'Bangladeshi', 'Islam', 'ctg', 'ctg', '', '123', '', '', 'hawauljannat@hotmail.com', '', 'n', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_resume_relevant`
--

CREATE TABLE IF NOT EXISTS `job_resume_relevant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(10) NOT NULL,
  `career_summary` varchar(300) DEFAULT NULL,
  `special_qualification` varchar(300) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `job_resume_relevant`
--

INSERT INTO `job_resume_relevant` (`id`, `emp_id`, `career_summary`, `special_qualification`, `keywords`) VALUES
(1, 2, 'khklshlk', '', 'nksh'),
(2, 3, 'sdf', 'sdfsd', 'dsa'),
(3, 4, 'NA', 'NA', 'NA'),
(4, 5, 'NA', 'NA', 'NA'),
(5, 6, 'CTG', 'CTG', 'CTG'),
(6, 7, 'test', '', 'test'),
(7, 8, 'NA', 'NA', 'NA'),
(8, 9, 'test', '', 'test'),
(9, 10, 'test', '', 'test'),
(10, 11, 'NA', 'NA', 'NA'),
(11, 12, 'qwe', 'qwe', 'qwe');

-- --------------------------------------------------------

--
-- Table structure for table `job_to_business_type`
--

CREATE TABLE IF NOT EXISTS `job_to_business_type` (
  `emp_job_id` int(11) NOT NULL,
  `job_req_id` int(11) NOT NULL,
  `business_type_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_to_business_type`
--

INSERT INTO `job_to_business_type` (`emp_job_id`, `job_req_id`, `business_type_id`) VALUES
(7, 0, 7),
(8, 0, 8),
(9, 0, 9),
(10, 0, 10),
(12, 0, 12),
(15, 15, 71),
(17, 17, 10),
(18, 18, 25),
(19, 19, 17),
(19, 19, 1),
(20, 20, 1),
(20, 20, 17),
(20, 20, 26),
(56, 56, 32),
(60, 60, 1),
(61, 61, 42),
(61, 61, 53);

-- --------------------------------------------------------

--
-- Table structure for table `job_to_experience_area`
--

CREATE TABLE IF NOT EXISTS `job_to_experience_area` (
  `emp_job_id` int(11) NOT NULL,
  `job_requirement_id` int(11) NOT NULL,
  `experience_area_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_to_experience_area`
--

INSERT INTO `job_to_experience_area` (`emp_job_id`, `job_requirement_id`, `experience_area_id`) VALUES
(7, 0, 7),
(8, 0, 8),
(9, 0, 9),
(10, 0, 10),
(12, 0, 12),
(16, 16, 5),
(17, 17, 5),
(18, 18, 8),
(56, 56, 8),
(60, 60, 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_training`
--

CREATE TABLE IF NOT EXISTS `job_training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) NOT NULL,
  `training_title` varchar(150) NOT NULL,
  `topics_covered` varchar(200) DEFAULT NULL,
  `institute` varchar(200) NOT NULL,
  `country` varchar(100) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `year` int(4) NOT NULL,
  `duration` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `job_training`
--

INSERT INTO `job_training` (`id`, `employee_id`, `training_title`, `topics_covered`, `institute`, `country`, `location`, `year`, `duration`) VALUES
(1, 182, 'CCNA', '    Cisco Certified Network Associate Course Complete                \n                    ', 'New Horizons CTG', 'Bangladesh', 'Chittagong', 2013, '3 Month'),
(2, 73, 'PentaGems Accounting Software', '                    \n                    ', 'Premier University Chittagong', 'Bangladesh', '', 2014, '2 days'),
(3, 73, 'Certificate in Computer Hardware Engineering', '                    \n                    ', 'National Youth Development Training Centre(NYDTC)', 'Bangladesh', 'Chittagong', 2007, '3 months'),
(4, 3, 'fdf', '                    \n                sdffs    ', 'dfsf', 'sdf', 'sdfs', 2015, 'sdfs'),
(5, 3, 'fsdfs', '         fsdf           \n                    ', 'sfsdf', 'sdf', 'sfsf', -1, 'sfs');

-- --------------------------------------------------------

--
-- Table structure for table `kg_list`
--

CREATE TABLE IF NOT EXISTS `kg_list` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `est_year` year(4) NOT NULL,
  `princ_name` varchar(250) NOT NULL,
  `teacher_name` varchar(250) NOT NULL,
  `school_time` varchar(100) NOT NULL,
  `session` varchar(100) NOT NULL,
  `start` varchar(100) NOT NULL,
  `end` varchar(100) NOT NULL,
  `medium` varchar(200) NOT NULL,
  `xtra_cur_activities` varchar(500) NOT NULL,
  `play_ground` varchar(50) NOT NULL,
  `sec_system` varchar(200) NOT NULL,
  `transport` varchar(200) NOT NULL,
  `loc_details` varchar(300) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `web` varchar(250) NOT NULL,
  `other_info` varchar(500) NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `kg_list`
--

INSERT INTO `kg_list` (`id`, `name`, `est_year`, `princ_name`, `teacher_name`, `school_time`, `session`, `start`, `end`, `medium`, `xtra_cur_activities`, `play_ground`, `sec_system`, `transport`, `loc_details`, `mobile`, `phone`, `email`, `web`, `other_info`, `is_active`) VALUES
(1, 'a', 0000, 'a', 'a', 'a', 'Morning', 'a', 'a', 'Bangla', '                          a                      j                                        ', 'Yes', 'Yes', 'Yes', '               a                                m                                        ', 'a', 'a', 'a', 'a', '                       a                         k                                        ', 0),
(2, 'dsfdf', 2012, 'SDF', 'SDF', '8:05 AM', 'Morning', '8:05 AM', '8:05 AM', 'Bangla', '                        \r\n                                    ', 'No', 'No', 'Yes', '                                                            ', ' 01730214532', '011-2153698', 'ERE@GMAIL.com', 'WWW.KHAN.COM', '                        SDFDSFSDF                    ', 0),
(3, 'khan', 2010, 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'm', 'n', 'o', 'p', 'q', 'k', 0),
(4, 'fghfgh', 2012, 'erter', 'SDF', '9:05 PM', 'Morning', 'SDF', '8:05 AM', 'Bangla', 'fhfgh\r\n                ', 'Yes', 'No', 'Yes', '                ', 'fghfghfh', 'fghfghgf', 'ERE@GMAIL.com', 'gfhfgh', '                fghfg', 0),
(5, 'dffsdfsd', 2015, 'sdfsdf', 'dsfsdf', '9:05 PM', 'Morning', '9:05 PM', '9:05 PM', 'Bangla', '\r\n                ', 'Yes', 'Yes', 'Yes', '                ', '', '', 'asdf@gmail.com', '', '                ', 0),
(6, 'fdsfds', 2012, 'sdfsdf', '3', '9:05 PM', 'Morning', '9:05 PM', '9:05 PM', 'English National Curriculum', '\r\n                ', 'Yes', 'Yes', 'No', '                ', '01739747186', '', 'asdf@gmail.com', '', '                ', 0),
(7, 'sdfsadf', 2012, 'sdfsdf', 'sdfsdfsd', '9:05 PM', 'Morning', '9:05 PM', '9:05 PM', 'Bangla', 'fdsf\r\n                ', 'Yes', 'Yes', 'Yes', '                ', '01739747186', '012-2546987', 'asdf@gmail.com', '', '                sdfsdfsd', 0),
(8, 'Play School', 2008, 'Mr.Karim', '15', '07:00 AM', 'Morning', '7:30 AM', '05:00 PM', 'English National Curriculum', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                                                            ', ' 01715429184', '', 'mehrul@colbd.com', '', '                                                            ', 1),
(9, 'Agrabad Public School', 2008, 'Arpita Nargis', '18', '09:00 AM', 'Day', '9.00 Am', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                            H#3, R#23/1, CDA R/A, Agrabad, Ctg                                ', ' 01758801605', '', 'apsc2008bd@gmail.com', '', '                                                            ', 1),
(10, 'Independent School & College', 2000, 'Md.Masuqur Rahaman (Masud)', '20', '09:00 AM', 'Day', '9.00 AM', '05:00 PM', 'English National Curriculum', '                                                \r\n                                                        ', 'Yes', 'Yes', 'Yes', '                                                               134, O.R. Nizam Road,Panchlaish R/A, Ctg                                         ', '  01716742664', '031654802', 'msdegs73@yahoo.com', '', '                                                                                                        ', 1),
(11, 'ST. LAWRENCE ACADEMY', 1998, 'Rozina Holder', '18', '07:00 AM', 'Morning', '7:30 AM', '11:30AM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                                        100 Bongshal Road, Feeringi Bazar, Ctg                    ', ' 01819800009', '031622202', 'moongroup4@gmail.com', '', '                                                            ', 1),
(12, 'Chittagong School & College', 2000, 'Shamima Sultana Shimu', '15', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                                                \r\n                                                        ', 'Yes', 'Yes', 'No', '                                                        Halishahar Road (Infront of Shaymali R/A,) Ctg                                                ', '  01933749798', '', 'moongroup4@gmail.com', '', '                                                                                                        ', 1),
(13, 'Greenland School & College', 2011, 'Md.Fazlur Rahaman', '20', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                                   H#27, R#2, B#L, Halishahar Housing Estate, Ctg                         ', ' 01712856740', '', 'mfrahman3011@yahoo.com', '', '                                                            ', 1),
(14, 'Al-Amin Residential School', 2006, 'A.S.M.Rafique Uddin', '15', '07:00 AM', 'Morning', '7:30 AM', '11:30AM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                               Al-Amin Sarak,(West of Bahaddarhat Jam-E-Mosque) Bahaddarhat, Ctg                             ', ' 01817215238', '', 'asmrafiq@yahoo.com', '', '                                                            ', 1),
(15, 'United School & College', 2000, 'Md.Sohrab Hossain', '25', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                           R#01, Bishaw Bank R/A, Colonel Hat,Ctg                                 ', ' 01727239911', '0312771614', 'uscctg@yahoo.com', '', '                                                            ', 1),
(16, 'ABC Method School & College', 1999, 'Md.Nazrul Islam', '10', '07:00 AM', 'Morning', '7:30 AM', '11:30AM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                                Nahar Mansion, 1219, Nasirabad Housing Socity,R#3, Panchlaish, Ctg                            ', ' 01832806855,01763818807', '', 'moongroup4@gmail.com', '', '                                                            ', 1),
(17, 'Hatey Khari School & College', 2000, 'Professor Md. Shah Alam', '25', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                              CDA, R/A, Agrabad, Ctg                              ', ' 01818111040,01914315455', '031714791', 'moongroup4@gmail.com', '', '                                                            ', 1),
(18, 'Standard School & College', 2008, 'Parveen Sultana', '15', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'English National Curriculum', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                                      H#18, R#1, Nasirabad Housing Socity, Nasirabad, Ctg                      ', ' 01815385490', '031655382', 'moongroup4@gmail.com', '', '                                                            ', 1),
(19, 'National Public College', 2004, 'Prof.Md.Qamaruzzaman', '20', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                                Bahaddarhat, Chandgoan, Ctg                            ', ' 01711116523', '031657617', 'npc_3010@yahoo.com', '', '                                                            ', 1),
(20, 'J.M.Sen School & College', 2004, 'Md.Zahurul Islam', '15', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                           Firingibazar, kotowali, Ctg                                 ', ' 01673117191', '031631462', 'jmsen_school@yahool.com', '', '                                                            ', 1),
(21, 'Oxyzen School & College', 2000, 'Md.Nazrul Islam', '18', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                               Wajedia Road,Oxyzen, Ctg                             ', ' 01814941837', '0312580211', 'moongroup4@gmail.com', '', '                                                            ', 1),
(22, 'Oasis Kindergarten', 2008, 'Mir Akter Hossain', '18', '07:00 AM', 'Morning', '7:30 AM', '11:30AM', 'English National Curriculum', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                              H#10,  R#01, B#G, Halishahar Housing Estate, Ctg                              ', ' 01819314521', '0312521365', 'mirakterhossain@gmail.com', '', '                                                            ', 1),
(23, 'Childerns Paradise School', 2005, 'Md.Muhib Ullah Miah', '18', '07:00 AM', 'Morning', '7:30 AM', '11:30AM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                              H#2/10, R#22, CDA R/A, Agrabad, Ctg                              ', ' 01731471542', '', 'moongroup4@gmail.com', '', '                                                            ', 1),
(24, 'Bay View School', 2000, 'John Benette Roy', '15', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'English National Curriculum', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                            R#9, H#104, O.R. Nizam Road, Ctg                                ', ' 01730069840', '031653483', 'bvs@jlmbd.net', '', '                                                            ', 1),
(25, 'Child Heaven School', 2006, 'Mrs. Shelina Islam', '15', '07:00 AM', 'Morning', '7:30 AM', '11:30AM', 'English National Curriculum', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                                  1042/A, Zakir Hossain Road,Nasirabad,Ctg                          ', ' 01716716614', '031653655', 'moongroup4@gmail.com', '', '                                                            ', 1),
(26, 'Magpie International School', 2011, 'Md.Aman Ullah', '15', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'English National Curriculum', '                        4G Education in Chittagong\r\n                                    ', 'Yes', 'Yes', 'Yes', '                                    128, Panchlaish R/A, (Near Passport Office), Ctg                        ', ' 01816035260', '', 'amans_english@yahoo.com', 'www.magpieschool.com', '                                                            ', 1),
(27, 'Navy School & College ', 1987, 'Captain M G Sobur BN (Retd)', '25', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                                Sailors Colony -1, Bandar, Ctg                            ', ' 01713124253', '031740391-9', 'moongroup4@gmail.com', '', '                                                            ', 1),
(28, 'S.Alam Scholar School', 2011, 'Md.Shah Alam', '15', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                           H#305, (3rd Floor), R#08, CDA R/A, Agrabad, Ctg                                 ', ' 01840421446', '', 'moongroup4@gmail.com', '', '                                                            ', 1),
(29, 'Eden Residetail School & College', 2008, 'H.M.A. Aoual', '17', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                             Hafiz Comissioner Road, Saraipara, Pahartali, Ctg                               ', ' 01827501409,01935365256', '0312773200', 'moongroup4@gmail.com', 'www.edenschoolbd.com', '                                                            ', 1),
(30, 'Illuminate Ideal School & College', 2008, 'Hosne Jahan(Koli)', '18', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                             1654/A, Mistripara Road, Dewanhat, Ctg                               ', ' ', '0312529905', 'moongroup4@gmail.com', '', '                                                            ', 1),
(31, 'Presidency International School', 2000, 'Md. Rezaul Hoque Chowdhury', '25', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'English National Curriculum', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                            51, Panchlaish R/A, Ctg                                ', ' 01199701642', '031655108,0312551571', 'rezabarta@gmail.com', 'www.presidencybd.edu.bd', '                                                            ', 1),
(32, 'Talant Campus School', 2007, 'Md.Shahedul Haq Rumon', '18', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'English National Curriculum', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                                  B# K, R# 4, H# 18, Halishahar, Ctg                          ', ' 01840010700', '031728255', 'thetalantcampus@gmail.com', '', '                                                            ', 1),
(33, 'Chittagong English School & College', 2008, 'Md.Shajjad Hossain Siddiqi', '20', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'English National Curriculum', '                        IT Based with national Curriculum\r\n                                    ', 'Yes', 'Yes', 'No', '                                      334?A, Muradpur, Panchlaish, Ctg                      ', ' 01839443922', '031655138,0312557077', 'sazzadhs@yahoo.com', '', '                                                            ', 1),
(34, 'WIN School & College', 2008, 'Rumana Akter Shuma', '10', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                           Hazi Asraf Ali Road, Saraipara, Pahartali, Ctg                                 ', ' ', '0312772803', 'moongroup4@gmail.com', '', '                                                            ', 1),
(35, 'Bayazid Pre-Cadet School', 2006, 'Md. Mahfuz Ahmed', '15', '07:00 AM', 'Morning', '7:30 AM', '11:30AM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                           181/2, Bayazid Bostami, Ctg                                 ', ' 01818104956', '', 'mahfujahmed26@yahoo.com', '', '                                                            ', 1),
(36, 'Chittagong Ambassador School', 2000, 'Mrs. Farzana Kamal', '20', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'Bangla', '\r\n                ', 'Yes', 'Yes', 'Yes', '            VIP Tower(2nd Floor), Kazir Dewri, Ctg    ', '01819353006', '0312865161', 'ctgambassador@yahoo.com', '', '                ', 1),
(37, 'Housing & Satelment Public School', 1990, 'Abu Sayed Mia', '25', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                               B# K, Halishahar Housing Estate, Halishahar, Ctg                             ', ' 01556534751', '0312521786', 'moongroup4@gmail.com', '', '                                                            ', 1),
(38, 'South Breeze Int. School', 2000, 'Benzer Hannan', '20', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'English National Curriculum', '\r\n                ', 'Yes', 'Yes', 'Yes', '        109 Panchlaish R/A, (Behind Panchlaish Police Station), Ctg        ', '01611450067', '031653169', 'southbreezeintlschool@yahoo.com', '', '                ', 1),
(39, 'Chittagong Lafboratory School & College', 2000, 'Md.Muzammel Hoque', '18', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                              116, Chatteshawari Road, Chawkbazar, Ctg                              ', ' 01819071338', '031625403', 'moongroup4@gmail.com', '', '                                                            ', 1),
(40, 'Mastermind International  School', 2000, 'Prof. Ghulam Abu Muhammad', '25', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'English National Curriculum', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                                12,24,36,37, Panchlaish R/A, Ctg                            ', ' 01729097332', '', 'ghulamabu@yahoo.com', 'www.mastermindctg.net', '                                                            ', 1),
(41, 'BEPZA Public School & College', 1999, 'Syed Kamrul Islam', '25', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                                     CEPZ Gate, Ctg                       ', ' 01824837228', '031740017', 'moongroup4@gmail.com', '', '                                                            ', 1),
(42, 'Chittagong  Ideal School', 1999, 'S.M Didarul Alam', '15', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                                Sea Beach Road, North Potenga, Ctg                            ', ' 01673262467', '0312500363', 'moongroup4@gmail.com', '', '                                                            ', 1),
(43, 'Pacific International  School & College', 2006, 'Md. Tafail Hossain', '15', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                                Oxyzen R/A, Oxyzen, Ctg                            ', ' 01820503101', '031685122', 'moongroup4@gmail.com', '', '                                                            ', 1),
(44, 'Orbit Residential School & College', 2008, 'Md. mujibur Rahman', '18', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'No', '                                        1322/1485, CDA Avenue, East Nasirabad, Ctg                    ', ' 01819390505', '0312553185', 'mujibbd61@yahoo.com', '', '                                                            ', 1),
(45, 'Asian Residentail School & College', 2008, 'Md. Ariful Haq', '15', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'Bangla', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                              Bohaddarhat New CDA R/A, Ctg                              ', ' 01819398444', '0312553983', 'arif.bcb@gmail.com', '', '                                                            ', 1),
(46, 'Agrabad National Academy', 2000, 'Prof. Md. Ruhul Amin', '18', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '\r\n                ', 'Yes', 'Yes', 'Yes', '        H# 699, R# 17, CDA R/A, Agrabad, Ctg        ', '01817207110', '031726713', 'moongroup4@gmail.com', '', '                ', 1),
(47, 'South Asian college', 2006, 'Md. Tanvirul Haque', '18', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'Bangla', '\r\n                ', 'Yes', 'Yes', 'Yes', '             101, Chottashari Road, Chawkbazar, Ctg   ', '0177572386', '0312868800', 'vp@sac-ctg.com', 'www.sac-ctg.com', '                ', 1),
(48, 'Cambridge School &  College', 2000, 'Mrs.Noorjahan Begum', '20', '09:00 AM', 'Day', '09:00 AM', '05:00 PM', 'English National Curriculum', '                        \r\n                                    ', 'Yes', 'Yes', 'Yes', '                              R# 14, CDA R/A, Agrabad, Ctg                              ', ' 01818742679', '031712143', 'moongroup4@gmail.com', '', '                                                            ', 1),
(49, 'Dewpoint School', 2005, 'Professor Md. Shah Alam', '25', '07:00 AM', 'Morning & Day', '7:30 AM', '05:00 PM', 'English National Curriculum', 'British Curriculum, Enriched Computer Lab,Own Campus\r\n                ', 'Yes', 'Yes', 'Yes', '          Plot#A/6, Road#1, Block#G, Halishahar Housing Estate, Ctg.      ', '01818111040', '0312524120', 'dewpointschool@gmail.com', 'N/A', '    Fully Air Condition Class Rooms            ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`) VALUES
('admin', '123');

-- --------------------------------------------------------

--
-- Table structure for table `member_info`
--

CREATE TABLE IF NOT EXISTS `member_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `frm_check` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `member_info`
--

INSERT INTO `member_info` (`id`, `user_name`, `password`, `frm_check`, `is_active`) VALUES
(1, 'hamaruf@yahoo.com', '1', 1, 1),
(2, 'ershadkhan.cse@gmail.com', '1', 0, 1),
(3, 'enamul@gmail.com', '123', 0, 1),
(4, 'mourshed4@gmail.com', '123456', 1, 1),
(5, 'a@yahoo.com', '1', 1, 1),
(6, 'hrdmoongrouo@gmail.com', '12345', 0, 1),
(7, 'hrdmoongroup@gmail.com', '123', 1, 1),
(8, 'shuvo.ctgbd@ymail.com', '123', 0, 1),
(9, 'shuvopsdctg@gmail.com', '12345', 1, 1),
(10, 'abcd@yahoo.com', '1', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `micro_job`
--

CREATE TABLE IF NOT EXISTS `micro_job` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(100) NOT NULL,
  `subject` text NOT NULL,
  `description` text NOT NULL,
  `submit_by` text NOT NULL,
  `location` text NOT NULL,
  `budget` varchar(25) NOT NULL,
  `start_date` date NOT NULL,
  `duration` varchar(35) NOT NULL,
  `special_notes` varchar(35) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `micro_job`
--

INSERT INTO `micro_job` (`id`, `job_title`, `subject`, `description`, `submit_by`, `location`, `budget`, `start_date`, `duration`, `special_notes`, `status`) VALUES
(4, '&#2476;&#2494;&#2476;&#2497;&#2480;&#2509;&#2458;&#2495;', '', '', '', '', '&#2542;&#2534;&#2534;&#25', '2014-11-23', '', '', 0),
(5, '&#2461;&#2494;&#2480;&#2497;&#2470;&#2494;&#2480;', '', '', '&#2480;&#2495;&#2475;&#2494;&#2468;', '\r\n&#2542;&#2534;&#2542;&#2540;/&#2460;\r\n', '&#2539;&#2534;&#2534;&#25', '2014-11-23', '&#2536;', '', 0),
(6, '&#2470;&#2494;&#2480;&#2507;&#2438;&#2472;', '', '', '&#2480;&#2495;&#2475;&#2494;&#2468;', '&#2438;&#2455;&#2509;&#2480;&#2494;&#2476;&#2494;&#2470;', '&#2535;&#2534;&#2534;&#25', '2014-11-25', '&#2536; &#2478;&#2494;&#2488;', '', 0),
(8, '&#2437;&#2476;&#2471;&#2494;&#2480;&#2453;', '', '', '', '', '', '2014-11-24', '', '', 0),
(11, '&#2455;&#2509;,&#2489; &#2474;&#2480;&#2495;&#2458;&#2494;&#2480;&#2453;', '', '&#2488;&#2476; &#2453;&#2494;&#2460;', '&#2480;&#2495;&#2475;&#2494;&#2468;', '&#2542;&#2534;&#2542;&#2534;', '&#2536;&#2534;&#2534;&#25', '2014-11-23', '&#2536;', '', 0),
(12, '&#2480;&#2494;&#2460;&#2478;&#2495;&#2488;&#2509;&#2468;&#2509;&#2480;&#2495;', '&#2539; &#2460;&#2472; &#2480;&#2494;&#2460;&#2478;&#2495;&#2488;&#2509;&#2468;&#2509;&#2480;&#2495; &#2474;&#2509;&#2480;&#2527;&#2507;&#2460;&#2472;&#2404;', '', '&#2480;&#2495;&#2475;&#2494;&#2468;', '&#2482;&#2494;&#2482;&#2454;&#2494;&#2472; &#2476;&#2494;&#2460;&#2494;&#2480;,&#2458;&#2463;&#2509;&#2463;&#2455;&#2509;&#2480;&#2494;&#2478;&#2404;', '&#2539;&#2534;&#2534; &#2', '2014-11-26', '&#2468;&#2495;&#2472; &#2470;&#2495', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `more_preference`
--

CREATE TABLE IF NOT EXISTS `more_preference` (
  `id` int(11) NOT NULL,
  `marital_status` varchar(50) NOT NULL,
  `body_type` varchar(100) NOT NULL,
  `complexion` varchar(100) NOT NULL,
  `education` varchar(150) NOT NULL,
  `working_with` varchar(100) NOT NULL,
  `profession_area` varchar(200) NOT NULL,
  `residency_status` varchar(100) NOT NULL,
  `smoke` varchar(20) NOT NULL,
  `drink` varchar(20) NOT NULL,
  `special_case` varchar(50) NOT NULL,
  `hiv` varchar(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `more_preference`
--

INSERT INTO `more_preference` (`id`, `marital_status`, `body_type`, `complexion`, `education`, `working_with`, `profession_area`, `residency_status`, `smoke`, `drink`, `special_case`, `hiv`, `is_active`) VALUES
(1, 'Never Married', 'Slim', 'Fair', 'Bachelors', 'Private Company', 'Accounting, Banking & Finance', 'Citizen', 'Does not matter', 'Does not matter', 'Does not matter', 'Include HIV profiles', 1),
(9, 'Never Married', 'Slim', 'Fair', 'Bachelors', 'Private Company', 'Education & Training', 'Citizen', 'Do not include profi', 'Never Drinks', 'Does not matter', 'Include HIV profiles', 1),
(12, '', 'Select', 'Select', 'Bachelors', 'Private Company', 'Accounting, Banking & Finance', 'Citizen', 'Does not matter', 'Does not matte', 'Does not matter', 'Include HIV profiles', 1),
(17, 'Never Married', 'Slim', 'Fair', 'Bachelors', 'Private Company', 'Accounting, Banking & Finance', 'Citizen', 'Does not matter', 'Does not matte', 'Does not matter', 'Include HIV profiles', 1);

-- --------------------------------------------------------

--
-- Table structure for table `more_teacher`
--

CREATE TABLE IF NOT EXISTS `more_teacher` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `id_more` int(100) NOT NULL,
  `institute_m` varchar(250) NOT NULL,
  `year_m` varchar(100) NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `more_teacher`
--

INSERT INTO `more_teacher` (`id`, `id_more`, `institute_m`, `year_m`, `is_active`) VALUES
(1, 12, 'hjkghjk', 'jhkhjkjhk', 1),
(2, 13, 'cu', '2000', 1),
(3, 13, '', '', 1),
(4, 13, '', '', 1),
(5, 13, '', '', 1),
(6, 13, '', '', 1),
(7, 13, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `more_univ`
--

CREATE TABLE IF NOT EXISTS `more_univ` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_teacher` int(5) NOT NULL,
  `faculty` varchar(200) NOT NULL,
  `din` varchar(250) NOT NULL,
  `is_active` int(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `more_univ`
--

INSERT INTO `more_univ` (`id`, `id_teacher`, `faculty`, `din`, `is_active`) VALUES
(1, 8, 'yu', 'ui', 1),
(2, 9, '', '', 1),
(3, 10, '', '', 1),
(4, 11, '', '', 1),
(5, 12, '', '', 1),
(6, 13, '', '', 1),
(7, 14, '', '', 1),
(8, 15, '', '', 1),
(9, 16, '', '', 1),
(10, 17, '', '', 1),
(11, 18, '', '', 1),
(12, 19, '', '', 1),
(13, 20, '', '', 1),
(14, 21, '', '', 1),
(15, 22, '', '', 1),
(16, 23, '', '', 1),
(17, 24, '', '', 1),
(18, 25, '', '', 1),
(19, 26, '', '', 1),
(20, 27, '', '', 1),
(21, 28, '', '', 1),
(22, 29, '', '', 1),
(23, 30, '', '', 1),
(24, 31, '', '', 1),
(25, 32, '', '', 1),
(26, 33, '', '', 1),
(27, 34, '', '', 1),
(28, 35, '', '', 1),
(29, 36, '', '', 1),
(30, 37, '', '', 1),
(31, 38, '', '', 1),
(32, 39, '', '', 1),
(33, 40, '', '', 1),
(34, 41, '', '', 1),
(35, 42, '', '', 1),
(36, 43, '', '', 1),
(37, 44, '', '', 1),
(38, 45, '', '', 1),
(39, 46, '', '', 1),
(40, 47, '', '', 1),
(41, 48, 'ty', 'rty', 1),
(42, 49, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(500) NOT NULL,
  `content` longtext NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `headline`, `content`, `is_active`) VALUES
(12, 'How to Start C & F Agent Business', 'The main objective of the training program is to provide the practical knowledge to highlight the C&F Business Agent of Bangladesh. The main growing sectors of Bangladesh are Garments, Pharmaceuticals, Leather, Ceramic, Plastics, Steel, Shipbuilding and many other industries.', 1),
(13, 'Accounting for Professionals', 'This introductory course will provide the students with basic understanding of the principles and concepts of accounting as well as their applicability and relevance in the national context and with the ability to apply these principles and concepts in the preparation of financial and related information to meet internal and external obligations.', 1),
(14, 'Management Skills for Administrative Professionals', 'Administrative professionals need to have versatile management skills. General administrative knowledge is the vital skill for the administrative professionals. To manage general administrative activities, professionals need to have some other attributes other than general administrative knowledge. This training is designed to discuss management skills for administrative professionals, which is required at work place for administrative activities.', 1),
(15, 'VAT Deduction at Source (VDS)', 'The main objective of the training program is to provide the practical knowledge to highlight the C&F Business Agent of Bangladesh. The main growing sectors of Bangladesh are Garments, Pharmaceuticals, Leather, Ceramic, Plastics, Steel, Shipbuilding and many other industries.', 1),
(16, 'Knit and Woven Production Planning', 'Garments is not a business of One Man Show rather it is a Team-Work where lot of people, agent, organizations, industries and manufacturers are involved, moreover all works has to be accomplished within a given time-frame to ship out the garments on time. Buyer will not accept the garments of winter in summer and vise versa as such if the garments cannot be shipped out on time then the order may be cancelled or may have to send by air or Buyer may ask for discount. To avoid all such adverse situation we need to do proper planning to execute any order.', 1),
(17, '  Implementation of ICAAP and Basel-III in Bangladesh', 'Upon completion of the course, the participant will be capable of understanding the insights of Internal Capital Adequacy Assessment Process (ICAAP) and the relationship in relation to the Risk Management. The participant will also understand the importance Basel-III reforms for improving the banking sector’s ability to absorb shocks arising from financial and economic stress, reducing the risk of spill over from the financial sector to the real economy. ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newspaper`
--

CREATE TABLE IF NOT EXISTS `newspaper` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `newspaper_name` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `newsjob_title` varchar(100) NOT NULL,
  `education` varchar(100) NOT NULL,
  `last_date` date NOT NULL,
  `name` varchar(200) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `path` varchar(200) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `newspaper`
--

INSERT INTO `newspaper` (`news_id`, `newspaper_name`, `company_name`, `newsjob_title`, `education`, `last_date`, `name`, `size`, `type`, `path`, `is_active`) VALUES
(29, 'Alokito Chattagram of The Prothom Alo', 'Daffodil Institute of IT', 'Faculty Member(Full/Part time)', 'Bachelor Degree from a recognized university. Must have knowledge of  System Analysis  & Design DBMS', '0000-00-00', 'DIIT_Alokito Chattagram_Page-2_25-05-2014.jpg', '117257', 'image/jpeg', '../newspaper_images/DIIT_Alokito Chattagram_Page-2_25-05-2014.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `partner_preference`
--

CREATE TABLE IF NOT EXISTS `partner_preference` (
  `id` int(11) NOT NULL,
  `age_from` int(11) NOT NULL,
  `age_to` int(11) NOT NULL,
  `height_from` varchar(50) NOT NULL,
  `height_to` varchar(50) NOT NULL,
  `religion` varchar(50) NOT NULL,
  `mother_tonge` varchar(50) NOT NULL,
  `caste` varchar(100) NOT NULL,
  `country_live` varchar(150) NOT NULL,
  `country_grew` varchar(100) NOT NULL,
  `diet` varchar(100) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner_preference`
--

INSERT INTO `partner_preference` (`id`, `age_from`, `age_to`, `height_from`, `height_to`, `religion`, `mother_tonge`, `caste`, `country_live`, `country_grew`, `diet`, `is_active`) VALUES
(1, 19, 24, '5ft 1in - 154cm', '5ft 6in - 167cm', 'Muslim', 'Bengali', 'Follow few practices', 'Bangladesh', 'Bangladesh', 'Doesn''t Matter', 1),
(9, 18, 22, '5ft 2in - 157cm', '5ft 6in - 167cm', 'Hindu', 'Bengali', 'Not very strict', 'Bangladesh', 'Bangladesh', 'Doesn''t Matter', 1),
(12, 20, 21, '4', '4', 'Muslim', 'Oriya', 'Follow few practices', 'Argentina', 'Kuwait', 'Non-vegetable', 1),
(17, 0, 25, '5ft - 152cm', '5ft 4in - 162cm', 'Muslim', 'Bengali', 'Strict follower of Islam', 'Bangladesh', 'Australia', 'Vegetable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment_receive`
--

CREATE TABLE IF NOT EXISTS `payment_receive` (
  `receive_id` varchar(50) NOT NULL,
  `work_id` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `receive_type` varchar(15) DEFAULT NULL,
  `receive_date` date DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `receive_amount` decimal(10,0) DEFAULT NULL,
  `other_first` varchar(10) DEFAULT NULL,
  `other_second` varchar(10) DEFAULT NULL,
  `other_third` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`receive_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_receive`
--

INSERT INTO `payment_receive` (`receive_id`, `work_id`, `description`, `receive_type`, `receive_date`, `cust_id`, `receive_amount`, `other_first`, `other_second`, `other_third`) VALUES
('avfertt54wt34634', '{293172B6-C5FA-4741-BAF2-50F72916C043}', 'fresdfsdf', 'job', '2013-01-06', 3, '1000', NULL, NULL, NULL),
('gdzfgarea3454362', '{17B3A8EC-B6BE-4466-B4B0-2678F49796F4}', 'dsvsdcsdc', 'job', '2013-01-06', 3, '800', NULL, NULL, NULL),
('greasdvr', '{D890B2FF-2852-4C72-9FC4-729576BD0F2A}', 'gdf', 'job', '2013-01-07', 3, '1500', NULL, NULL, NULL),
('gtredsfg', '{13800A3C-5D21-418D-B1B3-2318DF1E4196}', 'llllll', 'job', '2013-01-07', 3, '1500', NULL, NULL, NULL),
('trwtrgfbd', '{4645EDEB-287B-4722-AABD-BD37B3234FDA}', 'dsvsdcsdc', 'advertisement', '2013-01-06', 3, '800', NULL, NULL, NULL),
('{02014329-C7A9-4975-B70D-51B643EC48E6}', '36', 'gg', 'job', '2013-01-06', 0, '1000', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_ans`
--

CREATE TABLE IF NOT EXISTS `quiz_ans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `company` varchar(100) NOT NULL,
  `ans` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `quiz_ans`
--

INSERT INTO `quiz_ans` (`id`, `user_id`, `quiz_id`, `company`, `ans`) VALUES
(1, 1, 1, 'Meridian', 'KKR'),
(2, 5, 1, 'Meridian', 'KKR'),
(3, 6, 1, 'Meridian', 'KKR'),
(4, 6, 1, 'Meridian', 'RCBans_2'),
(5, 6, 2, 'Finlay Properties', 'Prahlad Modi'),
(6, 6, 3, 'PentaGems', 'Maherpur'),
(7, 1, 2, 'Finlay Properties', 'Krishno Modi'),
(8, 7, 2, 'Finlay Properties', 'Prahlad Modi'),
(9, 8, 2, 'Finlay Properties', 'Prahlad Modi'),
(10, 8, 3, 'PentaGems', 'Maherpur'),
(11, 0, 2, 'Finlay Properties', '');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_info`
--

CREATE TABLE IF NOT EXISTS `quiz_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `company` varchar(100) NOT NULL,
  `quiz_title` text NOT NULL,
  `ans_1` text NOT NULL,
  `ans_2` text NOT NULL,
  `ans_3` text NOT NULL,
  `ans_4` text NOT NULL,
  `right_ans` text NOT NULL,
  `date_entry` date NOT NULL,
  `date_result` date NOT NULL,
  `time` varchar(30) NOT NULL,
  `image` varchar(55) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `quiz_info`
--

INSERT INTO `quiz_info` (`id`, `company`, `quiz_title`, `ans_1`, `ans_2`, `ans_3`, `ans_4`, `right_ans`, `date_entry`, `date_result`, `time`, `image`, `is_active`) VALUES
(1, 'Meridian', 'Who is the winner of IPL 2012', 'SSK', 'RCB', 'KKR', 'RR', 'KKR', '0000-00-00', '0000-00-00', '6:10pm', '2B6BE3E3-B7B2-C3BE-DED6-85C2F5E7869A.jpg', 0),
(2, 'Finlay Properties', 'What is the father name of Modhi?', 'Damodardas Mulchand Modi', 'Pankaj Modi', 'Prahlad Modi', 'Krishno Modi', 'Damodardas Mulchand Modi', '0000-00-00', '0000-00-00', '6:10pm', '54F254A1-A635-623C-DA32-CCE9201E7C35.jpg', 1),
(3, 'PentaGems', 'Whis the small district of Bangladesh?', 'Chandpur', 'Commila', 'Maherpur', 'Chittagong', 'Maherpur', '0000-00-00', '0000-00-00', '12:00 pm', '09051B46-6BEC-8E67-9C8F-5852B532317E.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_reg`
--

CREATE TABLE IF NOT EXISTS `quiz_reg` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `reg_date` date NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `quiz_reg`
--

INSERT INTO `quiz_reg` (`id`, `name`, `email`, `mobile`, `password`, `ip`, `reg_date`, `is_active`) VALUES
(1, 'Rifat Mohammadi', 'rifat_callin@yahoo.com', '01670840934', '1', '::1', '0000-00-00', 1),
(2, 'Rahman', 'rohoman@gmail.com', '0123456789', '1', '::1', '2014-05-20', 1),
(3, 'pg', 'pg@test.com', '978848334', '123', '114.130.67.27', '2014-05-21', 1),
(4, 'Rohoma', 'rohoman@gmail.com', '123456789', '1', '180.149.1.248', '2014-05-22', 1),
(5, 'A.K.M Shohel', 'akm@gmail.com', '1234567890', '1', '180.149.1.248', '2014-05-22', 1),
(6, 'test', 'test@test.com', '888', 'test', '114.130.66.12', '2014-05-22', 1),
(7, 'Shaikat Barua', 'shaikat213@gmail.com', '01195172224', 'abc123', '180.234.148.90', '2014-05-25', 1),
(8, 'pg', 'pg@gmail.com', '43545', 'pg', '114.130.66.7', '2014-05-25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `profile_for` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `email`, `profile_for`, `name`, `gender`, `dob`, `age`, `religion`, `address`, `is_active`) VALUES
(1, 'hamaruf@yahoo.com', 'Self', 'Shwadhin', 'Male', '1985-02-21', 27, 'Muslim', 'Chittagong, Bangladesh', 1),
(2, 'ershadkhan.cse@gmail.com', 'Self', 'Pial', 'Male', '1988-04-08', 24, 'Muslim', 'Chittagong, Bangladesh', 1),
(3, 'enamul@gmail.com', 'Self', 'Enamul Hoque', 'Male', '2005-01-21', 8, 'Muslim', 'Brahmanbaria,Comilla', 1),
(4, 'mourshed4@gmail.com', 'Self', 'sHALA UDDIN LITON', 'Male', '1990-01-01', 23, 'Muslim', 'Vill-Kajirtaluck. Ps-Mirsharai.dist-chittagong', 1),
(5, 'a@yahoo.com', 'Self', 'a', 'Male', '1989-02-15', 24, 'Muslim', 'a, Chittagong', 1),
(6, 'hrdmoongrouo@gmail.com', 'Brother', 'Ala uddin Shohel', 'Male', '1989-04-03', 24, 'Muslim', 'Chotopul Chittagong', 1),
(7, 'hrdmoongroup@gmail.com', 'Self', 'Shiful Islam', 'Male', '1987-04-08', 26, 'Muslim', 'Chittagong', 1),
(8, 'shuvo.ctgbd@ymail.com', 'Friend', 'Munna Dey', 'Male', '1983-04-01', 30, 'Hindu', 'Patharghata, Chittagong.', 1),
(9, 'shuvopsdctg@gmail.com', 'Friend', 'Uttam dey', 'Male', '1983-05-08', 29, 'Hindu', 'Pathar Ghata, Chittagong.', 1),
(10, 'abcd@yahoo.com', 'Self', 'aaa', 'Male', '2013-07-15', -1, 'Muslim', 'aa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requestregistration`
--

CREATE TABLE IF NOT EXISTS `requestregistration` (
  `r_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `organization` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `specialized` varchar(200) NOT NULL,
  PRIMARY KEY (`r_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `requestregistration`
--

INSERT INTO `requestregistration` (`r_id`, `name`, `address`, `email_id`, `phone`, `gender`, `organization`, `designation`, `specialized`) VALUES
(17, 'samianaz musa', 'haji abdul kader road,east nasirabad', 'samianaz.m@hotmail.com', '01672014841', 'F', 'premier university', 'student', 'Finance, Accounts & Commercial Track'),
(16, 'golam kibria', 'billapara,agrabad', 'sobjanta.ron@gmail.com', '01672018267', 'M', 'premier university ', 'student', 'Finance, Accounts & Commercial Track'),
(15, 'Rony Barua', '34 jamal Khan, Chittagong.', 'ronybarua87@gmail.com', '01819889820', 'M', '', '', '0'),
(12, 'Ishtiaque Ahmed Chowdhury', 'Banglaw no.8/A.Port Residential Area.Sadarghat.Chittagong', 'ishti.11ctg@gmail.com', '01911269320', 'M', 'Premier University Chittgong', 'Student', 'Banking & Financial Industry Track'),
(13, 'Md azad rahaman', '  ', 'ar_sunny92@yahoo.com', '01814504991', 'M', '', '', 'Finance, Accounts & Commercial Track'),
(14, 'Rony Barua', '34 Jamal Khan Lane, jamal Khan, Chittagong, Bangladesh.', 'ronybarua87@gmail.com', '01819889820', 'M', '', '', '0'),
(18, 'rehnuma nazneen choity', 'purbo madarbari', 'nazneen.choity@gmaiil.com', '01672014841', 'F', 'premier university', 'student', 'Finance, Accounts & Commercial Track'),
(19, 'maharin wohab', 'cda , agrabad', 'maherinwahab@yahoo.com', '01672014841', 'F', 'premier university', 'student', 'Finance, Accounts & Commercial Track'),
(20, 'tanushri bhowmick', 'hajari goli,andorkilla', 'tanushri001@gmail.com', '01682147949', 'F', 'premier university', 'student', 'Finance, Accounts & Commercial Track'),
(21, 'nurul abser', 'badamtoli , agrabad', 'nurul.abser.140@facebook.com ', '01815341099', 'M', 'premier university', 'student', 'Finance, Accounts & Commercial Track'),
(22, 'golam kibria', 'cda, agrabad ', 'sobjanta.ron@gmail.com', '01672018267', 'M', 'premier university', 'student', 'Finance, Accounts & Commercial Track'),
(23, 'Rajib Sarbabidya', 'Jamalkhan,Chittagong', 'sarbarajib@gmail.com', '01813985877', 'M', 'Premier University', '8th Semester,BBA,Finance department,20th batch,', 'Finance, Accounts & Commercial Track'),
(24, 'Mozammel Hoque', 'CEPZ,Freeport,Bandar,Chittagong', 'mzml_coolhq@yahoo.com', '01674248065', 'M', '', '', 'Finance, Accounts & Commercial Track'),
(25, 'Md. Asif Azad Chy', 'Dohazari, Chandanish, Chittagong', 'Asifchy123@hotmail.com', '01673966679', 'M', '', '', 'Finance, Accounts & Commercial Track'),
(26, 'Muhammad Akbar Hossain', '30-31/C, Kaiballadham R/A, Firojshah, Akbarshah, Chittagong.', 'akbaridbr15@gmail.com', '01815814826', 'M', '', '', 'IT Track');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship`
--

CREATE TABLE IF NOT EXISTS `scholarship` (
  `s_id` int(10) NOT NULL AUTO_INCREMENT,
  `institute_name` varchar(100) NOT NULL,
  `scholarship_name` varchar(100) NOT NULL,
  `last_date` date NOT NULL,
  `name` varchar(150) NOT NULL,
  `type` varchar(50) NOT NULL,
  `size` varchar(50) NOT NULL,
  `path` varchar(150) NOT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `scholarship`
--

INSERT INTO `scholarship` (`s_id`, `institute_name`, `scholarship_name`, `last_date`, `name`, `type`, `size`, `path`) VALUES
(6, 'Srilanka Institute of Technology', 'Scholarship for Srilanka Institute of Technology', '2014-08-26', 'Scholarship for Srilanka Institute of Technology.pdf', 'application/pdf', '3093229', '../scholarship_images/Scholarship for Srilanka Institute of Technology.pdf'),
(7, 'The Fondation Rainbow Bridge ', 'The Fondation Rainbow Bridge offers scholarship for women from Bangladesh', '2014-08-18', '3.jpg', 'image/jpeg', '100785', '../scholarship_images/3.jpg'),
(8, ' Deutscher Akademischer Austausch Dienst (DAAD) ', 'DAAD offers scholarships in Public Policy and Good Governance for Bangladeshi students', '2014-08-20', '4.jpg', 'image/jpeg', '157674', '../scholarship_images/4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE IF NOT EXISTS `student_info` (
  `stu_id` int(30) NOT NULL,
  `student_id` varchar(50) NOT NULL,
  `student_name` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `bod` date NOT NULL,
  `present_address` varchar(100) NOT NULL,
  `per_address` varchar(100) NOT NULL,
  `blood` varchar(10) NOT NULL,
  `mobile_no` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  `current_education` varchar(100) NOT NULL,
  `institute` varchar(100) NOT NULL,
  `stu_subject` varchar(200) DEFAULT '',
  `student_result` varchar(50) NOT NULL,
  `yearpass` varchar(50) NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`stu_id`),
  KEY `stu_id` (`stu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`stu_id`, `student_id`, `student_name`, `gender`, `bod`, `present_address`, `per_address`, `blood`, `mobile_no`, `email`, `name`, `size`, `type`, `path`, `current_education`, `institute`, `stu_subject`, `student_result`, `yearpass`, `is_active`) VALUES
(2, '123', 'sdfsdf', 'Male', '2011-01-11', 'sdffd', 'sdff', 'B+', '01723653298', 'dsf@gmail.com', 'ECE53EE1-934B-9433-2A7C-B1A1E74EF3EB.jpg', '595284', 'image/jpeg', 'ECE53EE1-934B-9433-2A7C-B1A1E74EF3EB.jpg', 'sdf', 'sdfsdf', '', '4.3', '2011', 0),
(3, '1001', 'Joydeb Basak', 'Male', '1995-05-01', 'Halishahar, Chittagong.', 'Satkania, Chittagong.', 'A+', '01839500386', 'shuvo.ctgbd@ymail.com', '9C0208AA-C839-67C4-2F2B-BCE5A79FC371.jpg', '777835', 'image/jpeg', '9C0208AA-C839-67C4-2F2B-BCE5A79FC371.jpg', 'H. S. C', 'Govt. City College, CTG.', '', '5', '2012', 1),
(7, '1003', 'Md. Abdus Satter', 'Male', '1988-11-25', 'Halishahar, Ctg', 'Khagrachari', 'B+', '01730799807', 'satter_78@yahoo.com', 'FB7D20D8-BA0C-3F99-19E3-54FB335A122E.jpg', '2451', 'image/jpeg', 'FB7D20D8-BA0C-3F99-19E3-54FB335A122E.jpg', 'B.A (Hons)', 'Chittagong College', '', '4.88', '2008', 0),
(9, '1003', 'Sahida Akter', 'Female', '1992-06-02', 'Chandgow, Ctg', 'Homna, Comilla', 'A-', '01924428616', 'rupali_88@ymail.com', 'D24D1924-E65C-BA8A-E163-3C913F0DF20D.png', '59508', 'image/png', 'D24D1924-E65C-BA8A-E163-3C913F0DF20D.png', 'B.A.(Hons) 4th Year', 'Women Govt. College,Ctg', '', '4.88', '2008', 1),
(10, '123', 'ershad', 'Male', '0000-00-00', 'ctg', '', 'O+', '01924828156', 'ershadkhan.cse@gmail.com', '05BC3DF0-C2C4-B4EE-4535-40B41461E89A.jpg', '879394', 'image/jpeg', '05BC3DF0-C2C4-B4EE-4535-40B41461E89A.jpg', 'b.sc', 'cu', '', '3.55', '2010', 1);

-- --------------------------------------------------------

--
-- Table structure for table `success_stories`
--

CREATE TABLE IF NOT EXISTS `success_stories` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `your_name` varchar(100) NOT NULL,
  `your_email` varchar(100) NOT NULL,
  `p_name` varchar(100) NOT NULL,
  `p_email` varchar(100) NOT NULL,
  `wed_date` date NOT NULL,
  `not_fixed` int(11) NOT NULL DEFAULT '0',
  `story` longtext NOT NULL,
  `img_name` varchar(200) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `path` varchar(200) NOT NULL,
  `is_publish` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `success_stories`
--

INSERT INTO `success_stories` (`id`, `your_name`, `your_email`, `p_name`, `p_email`, `wed_date`, `not_fixed`, `story`, `img_name`, `size`, `type`, `path`, `is_publish`) VALUES
(1, 'Test', 'Test@yahoo.com', 'test', 'test@yahoo.com', '2013-02-28', 0, 'By the Grace of God, we are blessed to lead a happy life together and thanks to Moon Marriage Entertainment for helping us to find the right person and fulfill our dreams.From the bottom of our heart, we along with our families would like to wish Moon Marriage Entertainment all the very best.\r\n            ', 'B0D99AA7-0A4B-73FC-D537-DD994FBFCFCE.', '561276', 'image/jpeg', '../uploaded_images/B0D99AA7-0A4B-73FC-D537-DD994FBFCFCE.', 0),
(2, 'Test', 'Test@yahoo.com', 'test', 'test@yahoo.com', '2013-02-28', 0, 'By the Grace of God, we are blessed to lead a happy life together and thanks to Moon Marriage Entertainment for helping us to find the right person and fulfill our dreams.From the bottom of our heart, we along with our families would like to wish Moon Marriage Entertainment all the very best.\r\n            ', 'DF66A516-3386-C294-D3E5-E55245F72A0A.', '561276', 'image/jpeg', '../uploaded_images/DF66A516-3386-C294-D3E5-E55245F72A0A.', 0),
(3, 'Mr Akbor Hossain', 'moongroup4@gmail.com', 'Anjumanara begum', 'mourshed4@gmail.com', '2010-08-31', 0, '\r\n    We are very happy Continuous our life Together.         ', 'AFC10CBF-BD48-CC18-227A-5087C9C00DC0.', '304182', 'image/bmp', '../uploaded_images/AFC10CBF-BD48-CC18-227A-5087C9C00DC0.', 0),
(4, 'Shiful islam', 'hrdmoongroup@gmail.com', 'Sanjida Akter Mitu', 'mourshed4@gmail.com', '2010-05-12', 0, 'Moon Marriage entertainment is popular matching site. we submitted our own profile in moon M.entertainment Site.& choose our Profile Each other Easily.\r\n            ', 'A7B63E59-E18E-1FAC-4C5C-EAE42468BA44.', '405073', 'image/jpeg', '../uploaded_images/A7B63E59-E18E-1FAC-4C5C-EAE42468BA44.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_info`
--

CREATE TABLE IF NOT EXISTS `teacher_info` (
  `t_id` int(20) NOT NULL,
  `tea_id` varchar(50) NOT NULL,
  `tea_name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `bod` date NOT NULL,
  `present_address` varchar(200) NOT NULL,
  `per_address` varchar(200) NOT NULL,
  `blood` varchar(10) NOT NULL,
  `mobile_no` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `size` varchar(50) NOT NULL,
  `type` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  `carrer_obj` varchar(300) NOT NULL,
  `last_graduation` varchar(50) NOT NULL,
  `institute` varchar(100) NOT NULL,
  `concentration` varchar(500) NOT NULL,
  `ad_train` varchar(250) NOT NULL,
  `ad_ins` varchar(200) NOT NULL,
  `teac_ins` varchar(250) NOT NULL,
  `year` year(4) NOT NULL,
  `medium` varchar(200) NOT NULL,
  `designation` varchar(250) NOT NULL,
  `teaching_subject` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  `location` varchar(50) NOT NULL,
  `area` varchar(100) NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_info`
--

INSERT INTO `teacher_info` (`t_id`, `tea_id`, `tea_name`, `gender`, `bod`, `present_address`, `per_address`, `blood`, `mobile_no`, `email`, `name`, `size`, `type`, `path`, `carrer_obj`, `last_graduation`, `institute`, `concentration`, `ad_train`, `ad_ins`, `teac_ins`, `year`, `medium`, `designation`, `teaching_subject`, `level`, `location`, `area`, `is_active`) VALUES
(4, '1001', 'Debashis Das', 'Male', '1977-05-17', 'Halishahar H/E, Chittagong.', 'Satkania, Chittagong.', 'O+', '01554347087', 'debashis_cgs@yahoo.com', '8ACCB114-3A97-6372-1F0F-FCE8C3A42BBB.jpg', '620888', 'image/jpeg', '8ACCB114-3A97-6372-1F0F-FCE8C3A42BBB.jpg', 'Teaching.', 'M. Sc (Physics)', 'Chittagong University', 'Physics', '', '', '', 0000, 'English', 'Senior Teacher', 'Physics', 'A Level', 'Chittagong', 'Sugandha R/A, GEC, Khulshi.', 0),
(5, '1001', 'Debashis Das', 'Male', '1977-05-17', 'Halishahar H/E, Chittagong.', 'Satkania, Chittagong.', 'O+', '01554347087', 'debashis_cgs@yahoo.com', 'FFE5091C-F38C-CBDB-BFD1-10715A392078.jpg', '620888', 'image/jpeg', 'FFE5091C-F38C-CBDB-BFD1-10715A392078.jpg', 'Teaching.', 'M. Sc (Physics)', 'Chittagong University', 'Physics', '', '', '', 0000, 'English', 'Senior Teacher', 'Physics', 'A Level', 'Chittagong', 'Sugandha R/A, GEC, Khulshi.', 0),
(6, '1002', 'Md. abdus satter', 'Male', '1978-11-25', 'Halishahar, Ctg', 'Matiranga, Khagra chari', 'B+', '01730799807', 'satter_78@yahoo.com', '19D8D5CC-9544-3492-9994-6751B3C6BDB0.jpg', '2451', 'image/jpeg', '19D8D5CC-9544-3492-9994-6751B3C6BDB0.jpg', '', 'M. A', 'Victoria University College, Comilla', '', 'Teacher', 'Victoria University College, Comilla', '', 0000, 'Bengali', 'Joniur Teacher', 'Bangla', 'Pass & Hons', 'Chittagong', 'Khagrachari', 0),
(8, '1002', 'Md. abdus satter', 'Male', '1978-11-25', 'Halishahar, Ctg', 'Matiranga, Khagrachari', 'Select Blo', '01730799807', 'satter_78@yahoo.com', '46101068-B967-B199-DE01-67B36B59FCAC.jpg', '2451', 'image/jpeg', '46101068-B967-B199-DE01-67B36B59FCAC.jpg', '', 'M. A', 'Victoria University College, Comilla', '', 'Teacher', 'Victoria University College, Comilla', '', 0000, 'Bengali', 'Joniur Teacher', 'Bangla', 'Pass & Hons', 'Chittagong', 'Khagrachari', 0),
(11, '122', 'ghg', 'Male', '2013-07-03', 'ghf', 'gh', 'A+', '01736589421', 'ali@gmail.com', '86FD0CEB-E05D-BC6D-D8FF-33FF624C9153.jpg', '775702', 'image/jpeg', '86FD0CEB-E05D-BC6D-D8FF-33FF624C9153.jpg', 'jhkjh', 'jhkhjk', 'khjk', 'jhkhgjk', 'hjkhj', 'kjkghjk', '', 0000, 'Bengali', 'hjkhjk', 'HTML, CSS & javascript', 'Secondary', 'Chittagong', 'hjkhjk', 1),
(12, '122', 'ghg', 'Male', '2013-07-03', 'ghf', 'gh', 'A+', '01736589421', 'ali@gmail.com', 'D6EE60A6-04EF-4782-5749-5282DA4F489B.jpg', '775702', 'image/jpeg', 'D6EE60A6-04EF-4782-5749-5282DA4F489B.jpg', 'jhkjh', 'jhkhjk', 'khjk', 'jhkhgjk', 'hjkhj', 'kjkghjk', '', 0000, 'Bengali', 'hjkhjk', 'HTML, CSS & javascript', 'Secondary', 'Chittagong', 'hjkhjk', 0),
(13, '1111', 'dddddddddd', 'Male', '1998-08-05', 'ssssssssssss', 'sssssssssss', 'B+', '01915003234', 'ddddddd@gmail.com', '92ABC5A3-5FAE-286D-B73B-88637D965BFB.jpg', '879394', 'image/jpeg', '92ABC5A3-5FAE-286D-B73B-88637D965BFB.jpg', '', 'B. Sc', 'cu', '', 'cse', 'ttc', '', 0000, 'English', 'Lacturer', 'English', 'A Level', 'Chittagong', 'Agrabad', 1);

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE IF NOT EXISTS `training` (
  `training_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `training_date` date NOT NULL,
  `resource_person` longtext NOT NULL,
  `shift` varchar(50) NOT NULL,
  `Venue` varchar(100) NOT NULL,
  `lastdate_registration` date NOT NULL,
  `time` varchar(25) NOT NULL,
  `registration_fees` varchar(50) NOT NULL,
  `training_content` longtext NOT NULL,
  `name` varchar(200) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `path` varchar(200) NOT NULL,
  `training_date_to` date NOT NULL,
  `attend_person` longtext NOT NULL,
  `methodology` longtext NOT NULL,
  `t_testimonial` longtext NOT NULL,
  `days` varchar(25) NOT NULL,
  `hour` varchar(30) NOT NULL,
  PRIMARY KEY (`training_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`training_id`, `title`, `training_date`, `resource_person`, `shift`, `Venue`, `lastdate_registration`, `time`, `registration_fees`, `training_content`, `name`, `size`, `type`, `path`, `training_date_to`, `attend_person`, `methodology`, `t_testimonial`, `days`, `hour`) VALUES
(23, 'PentaGems Web developing training', '2014-08-30', '<p>Mr.Rifatul Islam<br>Sr. Software Engg.PentaGems</p>', '2', 'PentaGems,Singapore Market(5th floor),Hajipara,Agrabad', '2014-08-20', '3.00pm', '15000', '<p><span style="font-weight:bold">Training Overview</span><br>\n\nThis hands-on object oriented PHP5 and MySQL training course teaches attendees everything they need to successfully build data driven website using PHP and MySQL. You will also learn about MySQL, HTML5, CSS3 and JavaScript. We will teach you advanced CSS3 techniques. We will teach you Ajax and jQuery with Ajax Image upload, Object Oriented Ajax with Dojo Toolkit and Develop a Real-World Ajax, jQuery Application.<br><br>\n\n\n<span style="font-weight:bold">Training Objectives</span> \n\n• To teach attendees the Object Oriented PHP programming skills they need to successfully build interactive website.\n• To teach students enough MySQL database skills to build the databases that will power their websites.\n\n• HTML5, advanced CSS3 techniques and JavaScript.\n• Ajax Image upload & Develop a Real-World Ajax Application.\n• Object Oriented Ajax with Dojo Toolkit.\n• jQuery UI & Theming jQuery UI. \n• Individual / Group PHP MySQL Project with Dojo Toolkit & jQuery UI.\n</p>', 'logoFinal.png', '57764', 'image/jpeg', '../training_images/logoFinal.png', '2014-08-31', '<p>Who are interested to know about web designing, development.Outsourcing,Programming.</p>\r\n<p>&nbsp;</p>', '', 'Certificate of Course Completion will be awarded to participants at the end of course', 'Sat-Mon-Thu', '2'),
(19, 'Diploma in Computer Hardware & Networking', '2014-08-31', '<p>Experienced Engineer</p>', '', 'PentaGems,Singapore Market(5th floor),Hajipara,Agrabad', '2014-08-20', '3.00pm', '', '<p><span style="font-weight:bold">Hardware :</span><br>\nPS spec & Assembling; OS , Driver, Application, Utility Software Installation; Computer System, Peripherals and PS trouble Shooting & more »<br>\n<span style="font-weight:bold">Networking</span><br>Network Environment, Installation, Cabling, TCP/IP Suits, Subnet, FLSM, VLSM, Topology, LAN, Linux OS Environment, System Administration & more</p>', 'logoFinal.png', '11000', 'image/png', '../training_images/logoFinal.png', '2014-08-30', '<p>Who are interested to know about Computer Hardware &amp; Networking.</p>', '.', 'Certificate of Course Completion will be awarded to participants at the end of', '', ''),
(17, 'Diploma in Graphics Design', '2014-08-31', '<p>PentaGems Graphics Designer</p>', '1', 'PentaGems,Singapore Market(5th floor),Hajipara,Agrabad', '2014-08-20', '0', '5000', '<p>Who are interested to know Graphiscs Design</p>', 'logoFinal.png', '11000', 'image/png', '../training_images/logoFinal.png', '2014-08-25', '<p>Everyone</p>', '', '', '', '2 hours'),
(21, 'PG Oracle Training', '2014-08-31', '<p>Mr.Nur Karim Robin,Database Administrator</p>', '1', 'Haji Para Singapore Market,Ctg', '2014-08-31', '10.00 am', '25000', '<ul class="taleoBox1bullets1">\r\n<li>We look forward to providing a comprehensive curriculum that meets your training requirements.</li>\r\n<li>Instruction developed for Oracle Database 12<em>c</em> supports existing and new database administrators who would like to learn which features are available.</li>\r\n<li>Courses are prepared and delivered by industry experts to help you get up and running with deployment, performance and management of your Oracle Database 12<em>c</em> solutions.</li>\r\n</ul>', 'logoFinal.png', '11000', 'image/png', '../training_images/logoFinal.png', '2014-08-31', '<p>Minimum Graduate,Should have knowledge in IT.</p>', '', 'Certificate of Course Completion will be awarded to participants at the end of ', '', '2 hours'),
(22, 'Training on PG Accounting Software', '2014-08-31', '<p>Accounting Professionals,<br> CA CPA trainers, qualified and experienced mentors<br></p>', '1', 'PentaGems,Singapore Market(5th floor),Hajipara,Agrabad', '2014-08-31', '10.00 am', 'Free', '<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This course is an intensive introduction to the preparation and interpretation of financial information for investors (external users) and managers (internal users). The course adopts a decision-maker perspective on accounting and finance with the goal of helping students&nbsp;develop a framework for understanding financial, managerial, and other reports.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You will acquire basic concepts of corporate financial accounting and reporting</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You will be solving accounting problems from real cases using real clients.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You will be able for preparation of financial statements and disclosures.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You will use software that is widely used in accounting practices.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CA &amp; CPA trainers, qualified and experienced mentors will assist, support and encourage your progress throughout your time at PentaGems.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Various Versions of Accounting Software applicable for various types of companies will be shown in these training sessions.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Developing Presentation skills</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Preparing and researching prior to your interview</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Using the right language with different people (employers, clients etc.)</p>', 'logoFinal.png', '57764', 'image/jpeg', '../training_images/logoFinal.png', '2014-08-31', '<p>Students of BBA, MBA or any Discipline</p>', 'Real Software Presentation, Real Software Practice to each participentproviding hard copy and user manual ,Question and Answer.', 'Certificate of Course Completion will be awarded to participants at the end of workshop', 'Monday-Tuesday', '4'),
(1, 'PHP Programming with HTML,CSS,Javascript,MYSQL,', '2014-08-31', '<p>Mr.Rifatul Islam<br>Sr. Software Engg.PentaGems</p>', '1', 'PentaGems,Singapore Market(5th floor),Hajipara,Agrabad', '2014-08-31', '3.00 pm', '15000', '<p>Professional website design and development</p>', 'logoFinal.png', '11000', 'image/png', '../training_images/logoFinal.png', '2014-08-28', '<p>Who are interested.</p>', '.', '.', '', '2 hours'),
(20, 'Creating Interactive Websites using ASP.NET,Ajax & Jquery', '2014-08-31', '<p>Mr.Abdur Rahman <br>Sr. Software Engg.PentaGems</p>', '1', 'PentaGems,Singapore Market(5th floor),Hajipara,Agrabad', '2014-08-31', '3.00 pm', '25000', '<p>Course Highlights:<br>Introduction to Programming, List and collection, Data structure and algorithm, Conditions and loop, Object oriented programming,  Introduction to MS visual studio, Introduction to windows application, Introduction to MS SQL, Advance SQL Programming, A simple windows application,\nSQL DB backup SQl DB restore,Advance windows Application, \nIntroduction to asp.net,Introduction to html\nIntroduction to css, \nIntroduction java script and J-Query,\nIntroduction to MVC\nMore about MVC\n, Project with asp.net and MVC\nAdvance level web application  </p>', 'logoFinal.png', '11000', 'image/png', '../training_images/logoFinal.png', '2014-08-30', '<p>Minimum Graduate and should have IT knowledge</p>', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `username`, `password`, `type`, `is_active`) VALUES
(1, 'admin', 'admin', 'admin', 1),
(2, 'abc', '1', 'student', 0),
(3, 'student', '1', 'student', 1),
(4, 'Debashis', '123', 'teacher', 0),
(5, 'Debashis', '123', 'teacher', 0),
(6, 'Satter', '123', 'teacher', 0),
(7, 'satter', '123', 'student', 0),
(8, 'satter', '123', 'teacher', 0),
(9, 'sahida', '123', 'student', 1),
(10, 'kamal', '123', 'student', 1),
(11, 'fgf', '1', 'teacher', 1),
(12, 'fgf', '1', 'teacher', 0),
(13, 'teacher', '1', 'teacher', 1);

-- --------------------------------------------------------

--
-- Table structure for table `varsity_list`
--

CREATE TABLE IF NOT EXISTS `varsity_list` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `varsity_type` varchar(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `est_year` year(4) NOT NULL,
  `vc_name` varchar(250) NOT NULL,
  `faculty_name` varchar(200) NOT NULL,
  `din` varchar(200) NOT NULL,
  `no_campus` varchar(50) NOT NULL,
  `session` varchar(50) NOT NULL,
  `start` varchar(100) NOT NULL,
  `end` varchar(100) NOT NULL,
  `ad_time` varchar(100) NOT NULL,
  `sec_system` varchar(20) NOT NULL,
  `transport` varchar(20) NOT NULL,
  `hostel` varchar(200) NOT NULL,
  `classroom` varchar(200) NOT NULL,
  `loc_details` varchar(500) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `web` varchar(250) NOT NULL,
  `other_info` varchar(500) NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `varsity_list`
--

INSERT INTO `varsity_list` (`id`, `varsity_type`, `name`, `est_year`, `vc_name`, `faculty_name`, `din`, `no_campus`, `session`, `start`, `end`, `ad_time`, `sec_system`, `transport`, `hostel`, `classroom`, `loc_details`, `mobile`, `phone`, `email`, `web`, `other_info`, `is_active`) VALUES
(9, 'Private', 'Southern University Bangladesh', 2001, 'Professor Mohammad Ali', '', '', '4', 'Day', '09:00 AM', '8:30 PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                                                                                       House# 20, Road#02, Block#A, Chandgoan, R/A, Chittagong                                                                                             ', '01923652067', '0312851340', 'vc_sub@yahoo.com', 'www.southern-bd.info', '                                                                                                                                                                                    ', 1),
(10, 'Private', 'East Delta University(EDU)', 2006, 'Professor Muhammad Sekandar Khan', '', '', '', 'Day', '8:30 AM', '05:30 PM', '10:00 AM', 'Select', 'Select', 'Select', '', '                           Rumana Haq Tower, 1267/A Goshaildanga, Agrabad, Chittagong,\r\nBangladesh.                                              ', '01714102062', '31-2514441-3', 'ctgfoundation@yahoo.com', 'www.eastdelta.edu.bd', '                                                                        ', 1),
(11, 'Private', 'International Islamic University, Chittagong', 1995, 'Prof. Dr. A.K.M. Azharul Islam', '', '', '02', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                                                    IIUC, Permanent Campus\r\n\r\nKumirah, Chittagong.                                                        ', '01817- 20 89 69', '031 610307', 'vc@iiuc.ac.bd', 'http://www.iiuc.ac.bd', '                                                                                                            ', 1),
(12, 'Private', 'American International University-Bangladesh', 1994, 'Dr. Carmen Z. Lamagna ', '', '', '06', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                                            	House #83/B, Road # 4,\r\nKemal Ataturk Avenue, Banani, Dhaka - 1213,                             ', '', '029890415,028815386,8811749,', 'info@aiub.edu', 'www.aiub.edu', '                                                                        ', 1),
(13, 'Private', 'BRAC University', 2001, 'Professor Ainun Nishat', '', '', '03', 'Morning & Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                                         66 Mohakhali\r\nDhaka 1212\r\nBangladesh\r\n                               ', '', '028824051-4', 'info@bracu.ac.bd', 'http://www.bracuniversity.ac.bd', '                                                                        ', 1),
(14, 'Private', 'Eastern University, Bangladesh', 2003, 'Professor Dr. Nurul Islam', '', '', '2', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                             House 26, Road 5, Dhanmondi\r\nDhaka-1205                                           ', '01741300002 ', '029676031-5', 'dd_admission@easternuni.edu.bd', 'http://www.easternuni.edu.bd', '                                                                        ', 1),
(15, 'Private', 'IBAIS University', 2002, 'Prof. Dr. M. Delwar Hossain', '', '', '03', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', 'House # 21/A, Road # 16 (Old-27), Dhanmondi R/A, Dhaka-1209                                                        ', '', '', 'registrar@ibais.edu.bd', 'www.ibais.edu.bd/', '                                                                                                            ', 1),
(16, 'Private', 'Independent University, Bangladesh', 1993, 'Professor M Omar Rahman, MD, MPH, DSc (Harvard)', '', '', '', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                                              Plot 16, Block B\r\n\r\nAftabuddin Ahmed Road\r\n\r\nBashundhara R/A, Dhaka-1212, Bangladesh                          ', '01780185007', '028401645', 'info@iub.edu.bd', 'http://www.iub.edu.bd', '                                                                        ', 1),
(17, 'Private', 'Northern University, Bangladesh', 2002, 'Professor M. Shamsul Haque', '', '', '04', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'Yes', '', '                       93 Kazi Nazrul Islam Avenue, Dhaka-1215.             ', '01191 426305, 01191 426307 & 01191 426308', '029110293', 'admission@nub.ac.bd', 'http://www.nub.ac.bd', '                                    ', 1),
(18, 'Private', 'North South University', 1992, '  	Prof. Dr. Md. Abdus Sattar', '', '', '', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'Yes', 'Yes', '', '      Plot 15, Block B, Level 3\r\nBashundhara, Dhaka 1229.                              ', '', '029885611-20', 'registrar@northsouth.edu', 'www.northsouth.edu/', '                                    ', 1),
(19, 'Private', 'Presidency University', 2003, 'Dr. Muhammad Mahboob Ali', '', '', '02', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                                          11/A, Road 92, Gulshan, Dhaka 1212                              ', '', '029857617 / 8, (02)-8831182-4', 'info@presidency.edu.bd', 'www.presidency.edu.bd', '                                                                        ', 1),
(20, 'Private', 'Royal University of Dhaka', 2003, 'Professor Dr. M. Badiul Alam', '', '', '2', 'Day', '09:00 AM', '05:00 PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                           House No. 02, Road No. 10, Block. E, Banani, Dhaka-1213.                                             ', '01745477241 ', '02 9886150, 8861628,', 'moongroup4@gmail.com', 'http://www.royal.edu.bd/', '                                                                        ', 1),
(21, 'Private', 'Shanto-Mariam University of Creative Technology', 2003, 'Professor Dr. Shamsul Haq', '', '', '10', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                            House # 01, Road # 14, Sector # 13, Uttara, Dhaka-1230.                                            ', '', '02 8918932, + 88 02 8919366, + 88 02 8958048, + 88', 'moongroup4@gmail.com', 'http://www.smuct.edu.bd/', '                                                                        ', 1),
(22, 'Private', 'Stamford University Bangladesh', 2002, 'Prof. M. A. Hannan Feroz, Ph.D', '', '', '2', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '         744, Satmosjid Road, Dhanmondi\r\nDhaka-1209, Bangladesh                            ', '', '8153168-69, 8156122-23, 8155834 ', 'moongroup4@gmail.com', 'www.stamforduniversity.edu.bd', '                                    ', 1),
(23, 'Private', 'Uttara University ', 2003, ' 	Dr. M.Azizur Rahman', '', '', '02', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'Yes', 'Yes', 'Yes', '       House-4 & 5, Road- 15, Sector-6, Uttara, Dhaka- 1230                              ', '', '028919794, 8919116, 8932325, 8932541', 'uumain_edu@yahoo.com', ' 	www.uttarauniversity.edu.bd', '                                    ', 1),
(24, 'Private', 'Victoria University of Bangladesh ', 2002, 'Prof. Dr. Curtis R. Doyle', '', '', '1', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '             58/11/A Panthapath, Dhaka-1205                        ', '', '02 	8622634-5', 'info@vub.edu.bd', 'www.vub.edu.bd', '                                    ', 1),
(25, 'Private', 'University of Information Technology and Sciences (UITS) ', 2003, 'Professor Dr. Mohammed Abdul Aziz', '', '', '01', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'Yes', 'No', 'Yes', '         Jamalpur Twin Tower (Tower 2) (Opposition to American Embassy), Baridhara View, GA-37/1 Progati Sarani, Baridhara J-Block, Dhaka-1212                            ', '', '029565388,8850010,8832136', 'info@uits-bd.org', ' www.uits-bd.org', '                                    ', 1),
(26, 'Private', 'United International University (UIU) ', 2003, ' 	Prof. Dr. M Rezwan Khan', '', '', '', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '       House # 80, Road# 8/A Satmasjid Road Dhanmondi, Dhaka-1209                              ', '', '029125912-5', 'info@uiu.ac.bd', 'http://www.uiubd.com/', '                                    ', 1),
(27, 'Private', 'The University of Asia Pacific (UAP) ', 1996, 'Prof. Dr. Abdul Matin Patwari', '', '', '', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '   House-49/C, Road-4A, Dhanmondi R/A, Dhaka-1209                                  ', '', '028629368', 'admin@uap-bd.edu', 'www.uap-bd.edu', '                                    ', 1),
(28, 'Private', 'University of Liberal Arts Bangladesh (ULAB) ', 2004, ' 	Professor Rafiquel Islam', '', '', '1', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '                House 56, Road 4/A, Satmosjid Road, Dhanmondi, Dhaka-1209. Dhaka                     ', '', '029661255, 9661301,9675919,8626906', 'info@ulab.edu.bd', 'www.ulab.edu.bd', '                                    ', 1),
(29, 'Private', 'University of South Asia ', 2003, 'Prof. M.A. Matin', '', '', '', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '             \r\nHouse-76 & 78, Road-14, Block-B, Banani, Dhaka                        ', '', '', 'mamatin@unisa.ac.bd', 'www.unisa.ac.bd', '                                    ', 1),
(30, 'Private', 'World University of Bangladesh (WUB) ', 2003, 'Prof. Dr. Abdul Mannan Choudhury', '', '', '4', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'Yes', 'Yes', '      Plot # 3A, Road # 4, Dhanmondi, Dhaka – 1205.                              ', '', '029611410, 9611411, 9611412, 9611413, 9667435, 966', 'info@wub.edu', 'www.wub.edu.bd', '                                    ', 1),
(31, 'Private', 'City University ', 2002, 'Dr. N R M Borhan Uddin', '', '', '02', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'Yes', 'Yes', '         Bulu Ocean Tower 40, Kemal Ataturk Avenue, Banani, Dhaka-1213                            ', '', '02 	9893983,9861543', 'moongroup4@gmail.com', 'cityuniversity.edu.bd', '                                    ', 1),
(32, 'Private', 'Central Women’s University (CWU)', 1993, 'Dr. Perween Hasan', '', '', '2', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'Yes', 'Yes', '         \r\nAbhoy Das Lane, Wari, Dhaka-1203 (Previous Address). 6, Hatkhola (Shahid Nazrul Islam) Road, Dhaka-1213 (Present Address).                           ', '', '027171141, 9567499 ', 'info@cwu-bd.net', 'central-womens-university-cwu.html', '                                    ', 1),
(33, 'Private', 'Bangladesh University of Business & Technology (BUBT) ', 2002, 'Prof. Md. Abu Saleh', '', '', '02', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '                       Bangladesh University of Business & Technology (BUBT)\r\nDhaka Commerce College Road, Mirpur-2, Dhaka-1216\r\n             ', '01190658100', '02 	8057581-3', 'info@bubt.edu.bd', 'www.bubt.edu.bd', '                                    ', 1),
(34, 'Private', 'America Bangladesh University (ABU) ', 1997, '', '', '', '04', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '     Permanent Campus : Dawlotpur, Nawabgonj, Dhaka.                                ', '', '', 'moongroup4@gmail.com', 'America Bangladesh University ', '                                    ', 1),
(35, 'Private', 'Ahsanullah University of Science and Technology', 2002, '', '', '', '02', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'Yes', 'Yes', '    141 & 142, Love Road, Tejgaon Industrial Area, Dhaka-1208.                                ', '', '029120248,9130613, 9115461', 'vc@aust.edu', 'www.aust.edu ', '                                    ', 1),
(36, 'Private', 'University of Science and Technology Chittagong (USTC) ', 2001, '', '', '', '1', 'Morning & Day', '07:30 AM', '09:30 PM', '10:00 AM', 'yes', 'No', 'Yes', 'Yes', '                                    ', '', '031659070', 'ontact@ustc.edu.bd', 'www.ustc.edu.bd', '                                    ', 1),
(37, 'Private', 'Begum Gulchemonara Trust University (BGC)', 2002, '', '', '', '02', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'Yes', 'No', 'Yes', '   74/81, Nasirabad, Bayazid Bostami Road\r\nChittagong, Bangladesh                                   ', '', '031 656841', 'info@bgctub-edu.com', 'www.bgctub-edu.com ', '                                    ', 1),
(38, 'Private', 'ATISH DIPANKAR UNIVERSITY OF SCIENCE & TECHNOLOGY', 2003, '', '', '', '2', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '        Faisal Tower , 27,Gulshan North C/A,Dhaka-1212                             ', '', '029897700,9891904 ', 'moongroup4@gmail.com', 'atishdipankaruniversity.edu.bd', '                                    ', 1),
(39, 'Private', 'Darul Ihsan University', 1989, 'Professor Dr. Anwar Islam ', '', '', '9', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', '', '     House No. 21, Road No. 9/A,\r\nDhanmondi R/A, Dhaka-1209                                ', '', '02-9127841,02-8123983', 'info@diu.ac.bd', 'www.diu.ac.bd', '                                    ', 1),
(40, 'Private', 'Primeasia University ', 2003, '', '', '', '02', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '     Main Campus Address: HBR Tower, 9 Banani , C/A, Dhaka-1213                               ', '', '02-8853386, 8860905, 9895234, 8817534, 8825155', 'info@primeasia.edu.bd', 'www.primeasia.edu.bd', '                                    ', 1),
(41, 'Private', 'Sylhet International University', 2001, 'Professor Susanta Kumar Das Ph.D', '', '', '02', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                            Shamimabad, Bagbari, Sylhet. Bangladesh                                            ', '01754 313 182', '(0821) 720771, 717193', 'moongroup4@gmail.com', 'http://www.siu.edu.bd/', '                                                                        ', 1),
(42, 'Private', 'Metropolitan University', 2003, 'Professor Md. Abdul Aziz', '', '', '2', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '      Al-Hamra (7-th floor),\r\n\r\nZindabazar,\r\n\r\nSylhet-3100\r\n\r\nBangladesh                              ', '', '88-0821-713077-8', 'info@metrouni.edu.bd', 'http://www.metrouni.edu.bd/', '                                    ', 1),
(43, 'Private', 'International University of Business Agriculture and Technology ( IUBAT', 1991, 'Prof Dr M. Alimullah Miyan ', '', '', '', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', ' \r\n4 Embankment Drive Road, Sector-10 (Off Dhaka-Ashulia Road)\r\nUttara Model Town, Dhaka-1230.                                    ', '0171 4014933', '896 3523-27, 892 3469-70', 'info@iubat.edu', 'www.iubat.edu ', '                                    ', 1),
(44, 'Private', 'Manarat International University', 1992, 'Professor Dr. Choudhury Mahmood Hasan', '', '', '2', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', 'Plot#CEN-16, Road # 106\r\nGulshan-2, Dhaka1212\r\nBangladesh                                    ', '01819245895', '029884736, 029862251, 8817525, 9893226', 'info@manarat.ac.bd', 'http://www.manarat.ac.bd', '                                    ', 1),
(45, 'Private', 'University of Development Alternative (UODA) ', 2002, 'Prof. Dr. Emajuddin Ahamed', '', '', '', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'Yes', 'Yes', 'Yes', '80, Satmasjid Road, Dhanmondi R/A, Dhaka-1209                                     ', '', '029145740, 9145741, 9138227', 'registrar@uoda.org', 'www.uoda.edu.bd', '                                    ', 1),
(46, 'Private', 'ASA University Bangladesh ', 2006, ' 	 Prof. Md. Muinuddin Khan ', '', '', '1', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '23/3 Khilji Road, Shyamoli Dhaka-1207                                     ', '', '029116375,8119828 ', 'asabd@dhaka.net', 'www.asaub.edu.bd', '                                    ', 1),
(47, 'Private', 'Premier University, Chittagong ', 2002, 'Dr.Anupam Sen', '', '', '5', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'Select', 'Select', 'Select', 'Yes', '                                                                                1/A, O.R. Nizam Road, Panchlaish, Chittagong                                                                                                    ', '', '031656917,657654,2554317,2554318, 2550811,639914', 'puc@pubd.net', 'www.pubd.net', '                                                                                                                                                                                    ', 1),
(48, 'Public', 'tytr', 2012, 'ty', '', '', '1', 'Morning', '9:05 PM', '9:05 PM', '9:05 PM', 'No', 'Yes', 'No', 'df', '                         sdf           ', '0172365447', '012-6543219', 'ershadkhan.cse@gmail.com', 'dfsd', '  dsfdf                                  ', 1),
(49, 'Private', 'Premier University, Chittagong ', 2002, 'Dr.Anupam Sen', '', '', '5', 'Day', '09:00 AM', '08:00PM', '10:00 AM', 'yes', 'No', 'No', 'Yes', '     1/A, O.R. Nizam Road, Panchlaish, Chittagong                               ', '', '02656917,657654,2554317,2554318, 2550811,639914', 'puc@pubd.net', '  www.pubd.net', '                                    ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wedding_gift`
--

CREATE TABLE IF NOT EXISTS `wedding_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `g_name` varchar(30) NOT NULL,
  `description` longtext NOT NULL,
  `date` date NOT NULL,
  `img_name` varchar(200) NOT NULL,
  `size` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `path` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `wedding_gift`
--

INSERT INTO `wedding_gift` (`id`, `g_name`, `description`, `date`, `img_name`, `size`, `type`, `path`) VALUES
(1, 'dsfsd', '                         A gift of a pretty rose? these are hand soaps directly crafted into paper thin petals. We have a variety of colors for you to choose from pink, purpole, green, orang and blue! ', '2013-02-21', 'B95DEFD2-B496-419A-BBFE-B39B7C622FB1.tmp', '7540', 'image/png', '../uploaded_images/B95DEFD2-B496-419A-BBFE-B39B7C622FB1.tmp'),
(2, 'Flower', '`A gift of a pretty rose? these are hand soaps directly crafted into paper thin petals. We have a variety of colors for you to choose from pink, purpole, green, orang and blue! A gift of a pretty rose? these are hand soaps directly crafted into paper thin petals. We have a variety of colors for you to choose from pink, purpole, green, orang and blue!                                         ', '2013-02-16', '555C7430-6187-4FD1-A1D5-96059203259B.tmp', '26043', 'image/png', '../uploaded_images/555C7430-6187-4FD1-A1D5-96059203259B.tmp');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
