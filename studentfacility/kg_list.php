<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Moon Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>

        <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <link  rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui.css" />

        <script type="text/javascript">

            $(document).ready(function() {
                $("#biog").dialog({
                    autoOpen: false,
                    height: 620,
                    width: 800,
                    modal: true,
                    title: "KG Information Edit Panel",
                    position: ["center", "middle"],
                    autoresize: false
                });
            });

            function edit(id) {
                $.ajax({
                    url: "kgupdate/update_kg_form.php?id=" + id,
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    success: function(res) {
                        if (res.redirect) {
                            alert("sorry, something is wrong. May be you are not authenticated.Page will go to the home after clicking ok.")
                            window.location.href = res.redirect;
                            return;
                        }
                        $("#biog").html(res);

                    }
                });
                $("#biog").dialog("open");

            }

        </script>

        <style type="text/css">
            .hovertable
            {
                float: left;
                width: 100%;
                margin-top: 25px;
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 14px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;

            }
            table.hovertable td {
                border-width: 1px;
                font-size: 13px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
        </style>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div> 
            <div id="navigationbar">
                <? include ("admin/ad_menu/menu.php"); ?>
            </div>
            <div id="content"> 
                <?
                include ("database.php");
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 1;
                }

                $start_from = ($page - 1) * 10;
                $sql = "SELECT * FROM kg_list WHERE is_active=1  LIMIT $start_from, 10";
                $result = mysql_query($sql);
                ?>
                <table class = "hovertable">
                    <tr>
                        <th colspan="11">List of KG Schools</th>
                    </tr>
                    <tr>
                        <th>KG Name</th>
                        <th>Name of Principal</th>
                        <th>Established Year</th>
                        <th>Number of Teacher</th>
                        <th>Medium</th>
                        <th>Session</th>
                        <th>School Time</th>
                        <th>Starting Time</th>
                        <th>Ending Time</th>
                        <th>Action</th>
                        <th>Action</th>
                    </tr>
                    <?
                    While ($arr = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td><? echo $arr['name']; ?></td>
                            <td><?= $arr['princ_name']; ?></td>
                            <td><?= $arr['est_year']; ?></td>
                            <td><?= $arr['teacher_name']; ?></td>
                            <td><?= $arr['medium']; ?></td>
                            <td><?= $arr['session']; ?></td>
                            <td><?= $arr['school_time']; ?></td>
                            <td><?= $arr['start']; ?></td>
                            <td><?= $arr['end']; ?></td>
                            <td><a href="javascript:edit(<?= $arr['id'] ?>)">Edit</a></td>
                            <td><a href="kgupdate/kg_delete.php?delete_id=<?= $arr['id'] ?>">Delete</a></td>
                        </tr>
                        <?
                    }
                    ?>
                </table>

                <p align="center">
                    <?
                    $sq = "SELECT COUNT(id) FROM kg_list WHERE is_active=1";
                    $rs_result = mysql_query($sq);
                    $row = mysql_fetch_row($rs_result);
                    $total_records = $row[0];
                    $total_pages = ceil($total_records / 10);
                    //paging.......
                    for ($i = 1; $i <= $total_pages; $i++) {
                        if ($page == $i) {
                            echo $i . ' ';
                        } else {
                            echo "<a href='kg_list.php?page=" . $i . "'>" . $i . "</a> ";
                        }
                    }
                    ?>
                </p>

                <div id="biog">
                </div>
                <div id="gallery">
                    <div id="sub_gallery">
                        <? include("gallery.php"); ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>
