<?php
error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>Moon Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />

            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>

            <!-- Javascript goes in the document HEAD -->
            <script type="text/javascript">
                function altRows(id) {
                    if (document.getElementsByTagName) {

                        var table = document.getElementById(id);
                        var rows = table.getElementsByTagName("tr");

                        for (i = 0; i < rows.length; i++) {
                            if (i % 2 == 0) {
                                rows[i].className = "evenrowcolor";
                            } else {
                                rows[i].className = "oddrowcolor";
                            }
                        }
                    }
                }
                window.onload = function() {
                    altRows('alternatecolor');
                }
            </script>
            <!-- CSS goes in the document HEAD or added to your external stylesheet -->
            <style type="text/css">
                .altrowstable
                {
                    float: left;
                    margin-top: 20px;
                    margin-bottom: 10px;
                    margin-left: 35%;
                }
                table.altrowstable {
                    font-family: verdana,arial,sans-serif;
                    font-size:11px;
                    color:#333333;
                    border-width: 1px;
                    border-color: #a9c6c9;
                    border-collapse: collapse;
                }
                table.altrowstable th {
                    border-width: 1px;
                    padding: 8px;
                    border-style: solid;
                    border-color: #a9c6c9;
                }
                table.altrowstable td {
                    border-width: 1px;
                    font-size: 15px;
                    padding: 8px;
                    border-style: solid;
                    border-color: #a9c6c9;
                }
                .oddrowcolor{
                    background-color:#d4e3e5;
                }
                .evenrowcolor{
                    background-color:#c3dde0;
                }
            </style>

        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <?php
                $id = $_GET['tea_id'];
                include("database.php");
                $sql = "SELECT student_name,mobile_no FROM student_info JOIN user_info ON student_info.stu_id=user_info.user_id  WHERE username='$user_name'";
                $result = mysql_query($sql);
                $row = mysql_fetch_array($result);
                $student_name = $row['student_name'];
                $mobile_no = $row['mobile_no'];
                ?>


                <div id="content"> 
                    <?php
                    include('SMTPconfig.php');
                    include('SMTPClass.php');
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $to = 'ershadkhan.cse@gmail.com';
                        $from = 'infofinlay@yahoo.com';
                        $subject = $_POST['subject'];
                        $name = $_POST['nam'];
                        $mob = $_POST['mobile'];
                        $t_id = $_POST['t_id'];
                        $body = 'Name: ' . $name . "\n" . 'Mobile: ' . $mob . "\n" . 'Teacher Id: ' . $t_id . "\n\n" . $_POST['message'];
                        //$body= $_POST['message'];
                        $SMTPMail = new SMTPClient($SmtpServer, $SmtpPort, $SmtpUser, $SmtpPass, $from, $to, $subject, $body);
                        $SMTPChat = $SMTPMail->SendMail();
                       ?>
                    <script type="text/javascript">
                        alert("Your Mail has been sent");
                    </script>
                    <?
                    echo "<meta http-equiv='refresh' content='1 URL= all_teacher.php'>";
                    }
                   
                    ?>

                    <form action="" method ="POST" style="float: left; width: 100%; height: auto; padding-bottom: 20px;">
                        <table class="altrowstable" id="alternatecolor">
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td> <input type="text" name="nam" value="<? echo $student_name; ?>" readonly="readonly"  /></td>
                            </tr>
                            <tr>
                                <td>Mobile No.</td>
                                <td>:</td>
                                <td><input type="text" name="mobile" value="<? echo $mobile_no; ?>" readonly="readonly"  /></td>
                            </tr>
                            <tr>
                                <td>Subject</td>
                                <td>:</td>
                                <td><input type="text" name="subject" value="Teacher Want" readonly="readonly"  /></td>
                            </tr>
                            <tr>
                                <td>Teacher ID</td>
                                <td>:</td>
                                <td><input type="text" name="t_id" value="<? echo $id; ?>" readonly="readonly"  /></td>
                            </tr>
                            <tr>
                                <td>Message</td>
                                <td>:</td>
                                <td>
                                    <textarea name="message" rows="1" cols="16" VIRTUAL="wrap">
                                    </textarea>
                                </td>
                            </tr>
                        </table>
                        <p style="clear: left; margin-left: 47%;"><input type="submit" value=" Send "/></p>
                    </form>
                    <div id="gallery">
                        <div id="sub_gallery">
                            <? include("gallery.php"); ?>
                        </div>
                    </div>
                </div>

            </div>

            <div id="clear">
            </div>   
            <div id="footer">
                <div id="footer_div">
                    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>