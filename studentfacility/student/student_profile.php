<?php
//error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>Moon Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />

            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>

            <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
            <script type="text/javascript" src="js/jquery-ui.js"></script>
            <link  rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui.css" />

            <script type="text/javascript">

                $(document).ready(function() {
                    $("#pw_dialog").dialog({
                        autoOpen: false,
                        height: 170,
                        width: 450,
                        modal: true,
                        title: "Password Editing Panel",
                        position: ["center", "middle"],
                        autoresize: false
                    });
                });

                function pw_change(stu_id) {
                    $.ajax({
                        url: "update/student_pw_update/update_password.php?stu_id=" + stu_id,
                        type: "GET",
                        contentType: 'application/json; charset=utf-8',
                        success: function(res) {
                            if (res.redirect) {
                                alert("sorry, something is wrong. May be you are not authenticated.Page will go to the home after clicking ok.")
                                window.location.href = res.redirect;
                                return;
                            }
                            $("#pw_dialog").html(res);

                        }
                    });
                    $("#pw_dialog").dialog("open");

                }

            </script>

            <style type="text/css">
                .hovertable
                {
                    float: left;
                    margin-left: 18%;
                    width: 60%;
                    margin-top: 25px;
                    margin-bottom: 10px;
                }
                table.hovertable {
                    font-family: verdana,arial,sans-serif;
                    font-size:11px;
                    color:#333333;
                    border-width: 1px;
                    border-color: #999999;
                    border-collapse: collapse;
                }
                table.hovertable th {
                    background-color:#c3dde0;
                    border-width: 1px;
                    font-size: 14px;
                    padding: 8px;
                    border-style: solid;
                    border-color: #a9c6c9;
                }
                table.hovertable tr {
                    background-color:#d4e3e5;

                }
                table.hovertable td {
                    border-width: 1px;
                    font-size: 13px;
                    padding: 8px;
                    /*border-style: solid;*/
                    border-color: #a9c6c9;
                }
            </style>

        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="content"> 
                    <table align="center" class="hovertable" style="margin-top: 40px;">
                        <?
                        include("database.php");

                        $sql = "SELECT * FROM student_info JOIN user_info ON user_info.user_id = student_info.stu_id
                              WHERE user_info.username = '$user_name'";
                        $data = mysql_query($sql);
                        $row = mysql_fetch_array($data);
                        $show_id = $row['stu_id'];
//                      $show_id = $_GET['stu_id'];

                        $sql = "select * from student_info where stu_id='$show_id'";
                        $data = mysql_query($sql);
                        $row = mysql_fetch_array($data);
                        {
                            ?>
                            <tr>
                                <th colspan="6">Student's Profile</th>
                            </tr>
                            <tr>
                                <td colspan="6"><img src="../student/student_image/<? echo $row['name']; ?>" style=" float: left; width: 120px; height: 100px; border: 3px #ffffff solid;   " /></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td><? echo $row['student_name']; ?></td>
                                <td>Present Address</td>
                                <td>:</td>
                                <td><? echo $row['present_address']; ?>
                                </td>

                            </tr>

                            <tr>
                                <td colspan="6"><b><u>Academic Information</u></b></td>
                            </tr>
                            <tr>
                                <td>Current Education</td>
                                <td>:</td>
                                <td><? echo $row['current_education']; ?></td>
                                <td>Institute Name</td>
                                <td>:</td>
                                <td><? echo $row['institute']; ?></td>
                            </tr>
                            <tr>

                                <td>Result</td>
                                <td>:</td>
                                <td><? echo $row['student_result']; ?></td>
                                <td>Year Of Passing</td>
                                <td>:</td>
                                <td><? echo $row['yearpass']; ?></td>
                            </tr>
     
                            <tr>
                                <td colspan="6"><b><u>Personal Information</u></b></td>
                            </tr>
                            <tr>
                                <td colspan="6"><table>
                                        <tr>
                                            <td>Student ID</td>
                                            <td>:</td>
                                            <td><? echo $row['student_id']; ?></td>
                                            <td></td>
                                            <td>Gender</td>
                                            <td>:</td>
                                            <td><? echo $row['gender']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Blood Group</td>
                                            <td>:</td>
                                            <td><? echo $row['blood']; ?></td>
                                            <td></td>
                                            <td>Date Of Birth</td>
                                            <td>:</td>
                                            <td><? echo $row['bod']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile No</td>
                                            <td>:</td>
                                            <td><? echo $row['mobile_no']; ?></td>
                                            <td></td>
                                            <td>E-mail</td>
                                            <td>:</td>
                                            <td><? echo $row['email']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Permanent Address</td>
                                            <td>:</td>
                                            <td colspan="5"><? echo $row['per_address']; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="6"><a href="javascript:pw_change(stu_id=<? echo $row['stu_id']; ?>)"  style="float: right;background: #D9E021;text-decoration: none;font-size: 18px; color:#000;">Change Password</a></td>
                            </tr>

                            <?
                        }
                        ?>
                    </table>

                    <div id="pw_dialog">
                    </div>

                    <div id="gallery">
                        <div id="sub_gallery">
    <? include("gallery.php"); ?>
                        </div>
                    </div>
                </div>

            </div>

            <div id="clear">
            </div>   


            <div id="footer">
                <div id="footer_div">
    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>