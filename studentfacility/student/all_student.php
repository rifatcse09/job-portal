<?php
error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>Moon Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />
            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>
        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="content"> 
                    <form id="form"style="float: left; width: 100%; height: 100%;">
                        <table id="table">
                            <tr>
                                <th colspan="10">All Student Information</th>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <th>Present Address</th>
                                <th>Mobile</th>
                                <th>E-mail</th>
                                <th>Current Education</th>
                                <th>Institute</th>
                                <th>Result</th>
                                <th>Year Of Passing</th>
                                <th>Picture</th>
                                <th>Action</th>
                            </tr>
                            <?
                            include("database.php");
                            $sql = "select * from student_info";
                            $data = mysql_query($sql);
                            while ($row = mysql_fetch_array($data)) {
                                ?>
                                <tr>
                                    <td><? echo $row['student_name']; ?></td>
                                    <td><? echo $row['present_address']; ?></td>
                                    <td><? echo $row['mobile_no']; ?></td>
                                    <td><? echo $row['email']; ?></td>
                                    <td><? echo $row['current_education']; ?></td>
                                    <td><? echo $row['institute']; ?></td>
                                    <td><? echo $row['student_result']; ?></td>
                                    <td><? echo $row['yearpass']; ?></td>
                                    <td><img src="../student_image/<? echo $row['name']; ?>" width="80" height="50" /></td>
                                    <td><a href="#">View</a></td>
                                </tr>
                                <?
                            }
                            ?>
                        </table>
                    </form>
                    <div id="gallery">
                        <div id="sub_gallery">
                            <? include("gallery.php"); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="clear">
            </div>   


            <div id="footer">
                <div id="footer_div">
                    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>