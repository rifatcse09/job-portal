<?php
//error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>Moon Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />
            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>

            <style type="text/css">
                .hovertable
                {
                    float: left;
                    width: 80%;
                    margin-top: 30px;
                    margin-left: 10%;
                    margin-bottom: 10px;
                }
                table.hovertable {
                    font-family: verdana,arial,sans-serif;
                    font-size:11px;
                    color:#333333;
                    border-width: 1px;
                    border-color: #999999;
                    border-collapse: collapse;
                }
                table.hovertable th {
                    background-color:#c3dde0;
                    border-width: 1px;
                    font-size: 14px;
                    padding: 8px;
                    border-style: solid;
                    border-color: #a9c6c9;
                }
                table.hovertable tr {
                    background-color:#d4e3e5;

                }
                table.hovertable td {
                    border-width: 1px;
                    font-size: 15px;
                    padding: 8px;
                    border-style: solid;
                    border-color: #a9c6c9;
                }
            </style>

        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="content"> 
                    <form method="post" enctype="multipart/form-data" action="update/update_student_send_db.php">
                        <?php
                        include("database.php");

                        $sql = "SELECT * FROM student_info JOIN user_info ON user_info.user_id = student_info.stu_id
                              WHERE user_info.username = '$user_name'";

                        $data = mysql_query($sql);
                        $row = mysql_fetch_array($data);
                        $update_id = $row['stu_id']; {
                            ?>
                            <form method="post" enctype="multipart/form-data" action="update/update_student_send_db.php">
                                <table class="hovertable">
                                    <tr>
                                        <th colspan="7">Personal Information</th>
                                    </tr>
                                    <tr>
                                        <td>Teacher ID</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_student_id" value="<? echo $row['student_id']; ?>" /></td>
                                        <td>&nbsp;</td>
                                        <td>Teacher Name</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_student_name" value="<? echo $row['student_name']; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_gender" value="<? echo $row['gender']; ?>"></td>
                                        <td>&nbsp;</td>
                                        <td>Birth Of Date</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_bod" id="bod" value="<? echo $row['bod']; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td>Present Address</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_present_address" value="<? echo $row['present_address']; ?>" /></td>
                                        <td>&nbsp;</td>
                                        <td>Permanent Address</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_per_address" value="<? echo $row['per_address']; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td>Blood Group</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_blood" value="<? echo $row['blood']; ?>"></td>
                                        <td>&nbsp;</td>
                                        <td>Mobile No</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_mobile_no" value="<? echo $row['mobile_no']; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td>E-mail</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_email" value="<? echo $row['email']; ?>" /></td>
                                        <td>&nbsp;</td>
                                        <td>Picture</td>
                                        <td>:</td>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
                                    <td><input type="file" name="userfile" value="<? echo $row['name']; ?>" id="userfile" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">::Academic Information::</td>
                                    </tr>
                                    <tr>
                                        <td>Current Education</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_current_education" value="<? echo $row['current_education']; ?>" /></td>
                                        <td></td>
                                        <td>Institute Name</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_institute" value="<? echo $row['institute']; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td>Result</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_student_result" value="<? echo $row['student_result']; ?>" /></td>
                                        <td></td>
                                        <td>Year Of Passing</td>
                                        <td>:</td>
                                        <td><input type="text" name="update_yearpass" value="<? echo $row['yearpass']; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="hidden" name="update_id" value="<?php echo $update_id; ?>" /></td>
                                        <td colspan="6"><input type="submit" name="change" value="Update" /></td>
                                    </tr>
                                </table>

                                <?php
                            }
                            ?>
                        </form>
                        <div id="gallery">
                            <div id="sub_gallery">
                                <? include("gallery.php"); ?>
                            </div>
                        </div>
                </div>

            </div>

            <div id="clear">
            </div>   


            <div id="footer">
                <div id="footer_div">
                    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>