<?php
//error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>Moon Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />
            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>

            <style type="text/css">
                .hovertable
                {
                    float: left;
                    width: 100%;
                    margin-top: 30px;
                    margin-bottom: 10px;
                }
                table.hovertable {
                    font-family: verdana,arial,sans-serif;
                    font-size:11px;
                    color:#333333;
                    border-width: 1px;
                    border-color: #999999;
                    border-collapse: collapse;
                }
                table.hovertable th {
                    background-color:#c3dde0;
                    border-width: 1px;
                    font-size: 14px;
                    padding: 8px;
                    border-style: solid;
                    border-color: #a9c6c9;
                }
                table.hovertable tr {
                    background-color:#d4e3e5;

                }
                table.hovertable td {
                    border-width: 1px;
                    font-size: 15px;
                    padding: 8px;
                    border-style: solid;
                    border-color: #a9c6c9;
                }
            </style>

        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="content"> 
                    <form>
                        <?php
                        include("database.php");

                        $sql = "SELECT * FROM student_info JOIN user_info ON user_info.user_id = student_info.stu_id
                              WHERE user_info.username = '$user_name'";
                        $data = mysql_query($sql);
                        $row = mysql_fetch_array($data);
                        $stu_id = $row['stu_id'];
                        {
                            ?>
                            <table class="hovertable">
                                <tr onmouseover="this.style.backgroundColor = '#ffff66';" onmouseout="this.style.backgroundColor = '#d4e3e5';">
                                    <td><? echo $row['student_id']; ?></td>
                                    <td><? echo $row['student_name']; ?></td>
                                    <td><? echo $row['gender']; ?></td>
                                    <td><? echo $row['bod']; ?></td>
                                    <td><? echo $row['present_address']; ?></td>
                                    <td><? echo $row['per_address']; ?></td>
                                    <td><? echo $row['blood']; ?></td>
                                    <td><? echo $row['mobile_no']; ?></td>
                                    <td><? echo $row['email']; ?></td>
                                    <td><a style="text-decoration:none; font-size: 20px; color: #1c94c4;" href="student_update_profile.php?stu_id=<? echo $row['stu_id']; ?>">update</a></td>
                                </tr>
                            </table>
                            <?php
                        }
                        ?>
                    </form>

                    <div id="gallery">
                        <div id="sub_gallery">
    <? include("gallery.php"); ?>
                        </div>
                    </div>
                </div>

            </div>

            <div id="clear">
            </div>   


            <div id="footer">
                <div id="footer_div">
    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>