<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>PentaGems Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>

        <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <link  rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui.css" />

        <style type="text/css">
            .hovertable
            {
                float: left;
                width: 100%;
                margin-top: 25px;
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 14px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;

            }
            table.hovertable td {
                border-width: 1px;
                font-size: 13px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
        </style>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div> 
            <div id="navigationbar">
                <? include ("menu1.php"); ?>
            </div>
            <div id="content"> 
                <?
                include ("database.php");
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 1;
                }

                $start_from = ($page - 1) * 20;
                $sql = "SELECT * FROM varsity_list WHERE is_active=1  LIMIT $start_from, 20";
                $result = mysql_query($sql);
                ?>
                <table class = "hovertable">
                    <tr>
                        <th colspan="11">List of University</th>
                    </tr>
                    <tr>
                        <th>University type</th>
                        <th>Name</th>
                        <th>Established Year</th>
                        <th>Name of VC</th>
                        <th>Session</th>
                        <th>Website URL</th>
                        <th>Starting Time</th>
                        <th>Ending Time</th>
                        <th>View</th>
                    </tr>
                    <?
                    While ($arr = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td><? echo $arr['varsity_type']; ?></td>
                            <td><?= $arr['name']; ?></td>
                            <td><?= $arr['est_year']; ?></td>
                            <td><?= $arr['vc_name']; ?></td>
                            <td><?= $arr['session']; ?></td>
                            <td><?= $arr['web']; ?></td>
                            <td><?= $arr['start']; ?></td>
                            <td><?= $arr['end']; ?></td>
                            <td><a href="varsity_list_search.php?id=<?= $arr['id'] ?>">Details</a></td>
                        </tr>
                        <?
                    }
                    ?>
                </table>

                <p align="center">
                    <?
                    $sq = "SELECT COUNT(id) FROM varsity_list WHERE is_active=1";
                    $rs_result = mysql_query($sq);
                    $row = mysql_fetch_row($rs_result);
                    $total_records = $row[0];
                    $total_pages = ceil($total_records / 20);
                    //paging.......
                    for ($i = 1; $i <= $total_pages; $i++) {
                        if ($page == $i) {
                            echo $i . ' ';
                        } else {
                            echo "<a href='more_varsity_list.php?page=" . $i . "'>" . $i . "</a> ";
                        }
                    }
                    ?>
                </p>

                <div id="biog">
                </div>
<!--                <div id="gallery">
                    <div id="sub_gallery">
                        <? //include("gallery.php"); ?>
                    </div>
                </div>-->
            </div>
        </div>

        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>
