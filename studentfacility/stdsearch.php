<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>PentaGems Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link  rel="stylesheet" type="text/css" href="css/searchstyle.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>

        <style type="text/css">
            .hovertable
            {
                float: left;
                margin-top: 25px;
                width: 100%;
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 14px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;

            }
            table.hovertable td {
                border-width: 1px;
                font-size: 13px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
        </style>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div>
            <div id="navigationbar">
                <? include ("searchmenu.php"); ?>
            </div>
            <div id="content"> 
                <?
                include("database.php");
                if (isset($_GET['page'])) {
                    $page = $_GET['page'];
                } else {
                    $page = 1;
                }
                $start_from = ($page - 1) * 10;
                $location = $_GET['location'];
                $institute = $_GET['institute'];
                $sql = "SELECT * FROM student_info WHERE present_address LIKE '%$location%' AND institute= '$institute' AND student_info.is_active=1 LIMIT $start_from, 10";
                $data = mysql_query($sql);
                ?>
                <table class="hovertable">
                    <tr>
                        <th colspan="12">Student Information</th>
                    </tr>
                    <tr>
                        <th>Student ID</th>
                        <th>Name</th>
                        <th>Present Address</th>
                        <th>Current Education</th>
                        <th>Institute</th>
                        <th>Result</th>
                        <th>Year Of Passing</th>
                    </tr>
                    <?php
                    while ($row = mysql_fetch_array($data)) {
                        ?>
                        <tr onmouseover="this.style.backgroundColor = '#ffff66';" onmouseout="this.style.backgroundColor = '#d4e3e5';">
                            <td><? echo $row['student_id']; ?></td>
                            <td><? echo $row['student_name']; ?></td>
                            <td><? echo $row['present_address']; ?></td>
                            <td><? echo $row['institute']; ?></td>
                            <td><? echo $row['institute']; ?></td>
                            <td><? echo $row['student_result']; ?></td>
                            <td><? echo $row['yearpass']; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <p align="center">
                    <?
                    $sq = "SELECT COUNT(stu_id) FROM student_info WHERE present_address LIKE '%$location%' AND institute= '$institute' AND student_info.is_active=1";
                    $rs_result = mysql_query($sq);
                    $row = mysql_fetch_row($rs_result);
                    $total_records = $row[0];
                    $total_pages = ceil($total_records / 10);
                    //paging.......
                    for ($i = 1; $i <= $total_pages; $i++) {
                        if ($page == $i) {
                            echo $i . ' ';
                        } else {
                            echo "<a href='stdsearch.php?page=" . $i . "&location=" . $location . "&institute=" . $institute . "'>" . $i . "</a> ";
                        }
                    }
                    ?>
                </p>
<!--                <div id="gallery">
                    <div id="sub_gallery">
                        <? //include("gallery.php"); ?>
                    </div>
                </div>-->
            </div>
        </div>
        <div id="clear">
        </div>   
        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>