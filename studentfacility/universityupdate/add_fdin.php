<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Moon Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="../css/style.css" />
        <link href="../images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="../css/jquerycssmenu.css" />
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="../js/crawler.js">
        </script>

        <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <link  rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui.css" />

        <script type="text/javascript">
            function check() {
                var p = document.getElementById("pass").value;
                var rP = document.getElementById("newpass").value;
                if (p != rP) {
                    document.getElementById("error").innerHTML = "passwords don't match";
                    return false;
                }
                else {
                    return true;
                }
            }
        </script>

        <script type="text/javascript">

            $(document).ready(function() {
                $("#biog").dialog({
                    autoOpen: false,
                    height: 590,
                    width: 900,
                    modal: true,
                    title: "Entering KG Details Panel",
                    position: ["center", "top"],
                    autoresize: false
                });
            });

            function insert_kg() {
                $.ajax({
                    url: "kg.php",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    success: function(res) {
                        if (res.redirect) {
                            alert("sorry, something is wrong. May be you are not authenticated.Page will go to the home after clicking ok.")
                            window.location.href = res.redirect;
                            return;
                        }
                        $("#biog").html(res);

                    }
                });
                $("#biog").dialog("open");

            }


        </script>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div> 
            <div id="navigationbar">
                <? include ("menu.php"); ?>
            </div>
            <div id="content"> 
                <div id="left_content">
                    <form action="mor_upate.php" method="POST">
                        <table class="hovertable">
                            <?
                            $uid = $_GET['id'];
                            include("../database.php");
                            $sql = "select * from more_univ where id='$uid'";
                            $resul = mysql_query($sql);
                            $row = mysql_fetch_array($resul);
                            $f = $row['faculty'];
                            $d = $row['din'];
                            ?>
                            <tr>
                                <td>Name of Faculty</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="fa" value="<?= $f ?>">
                                    <!--<div id="fname"></div>-->
                                </td>
                                <td>DIN</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="di" value="<?= $d ?>">
                                    <!--<div id="din"></div>-->
                                </td>
                            <input type="hidden" value="0" id="no_din" name="no_din">
                            <input type="hidden" name="uid" value="<?= $uid ?>">
                            </tr>
                        </table>
                        <p style="text-align: center"> <input type="submit" name="send" value="Update"></p>

                    </form>

                </div>
                <div id="right_content">
                    <div id="tutrsrch">
                        <div id="tutrsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Tutor Search</p>
                        </div>
                        <form action="teachersearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="../images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Subject</td>
                                    <td><input  type="text" size="20px" name="teaching_subject"></td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="stdsrch">
                        <div id="stdsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Student Search</p>
                        </div>
                        <form action="stdsearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('../database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM student_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="../images/arrow.png" />
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="biog">
                    </div>
                    <div id="biog_uv">
                    </div>
                    <div id="TV">
                        <img src="../images/tv.png">
                    </div>
                </div>
                <div id="gallery">
                    <div id="sub_gallery">
                        <? include("gallery.php"); ?>
                    </div>
                </div>
            </div>
        </div>


        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("../footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>
