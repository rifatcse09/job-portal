<form action="universityupdate/update_univ_db.php" method="POST">
    <?
    include ("../database.php");
    $update_id = $_GET['id'];
    $sql = "select * from varsity_list where is_active=1 and id='$update_id'";
    $result = mysql_query($sql);
    $ar = mysql_fetch_array($result);
    $vt = $ar['varsity_type'];
    $se = $ar['session'];
    $ss = $ar['sec_system'];
    $tr = $ar['transport'];
    $hos = $ar['hostel'];
    $select = "selected='true'";
    ?>
    <table class="hovertable">
        <tr>
            <th colspan="8">University Details</th>
        </tr>
        <tr>
            <td><span>*</span>University Type</td>
            <td>:</td>
            <td>
                <select name="univ_type">
                    <option value="Select" selected="selected">Select</option>
                    <option value="Public" <? if ($vt == 'Public') echo $select; ?>>Public</option>
                    <option value="Private" <? if ($vt == 'Private') echo $select; ?>>Private</option>
                </select>
            </td>
            <td><span>*</span>Name</td>
            <td>:</td>
            <td colspan="3">
                <input type="text" name="name" value="<?= $ar['name'] ?>">
            </td>
        </tr>

        <tr>
            <td><span>*</span>Established Year</td>
            <td>:</td>
            <td>
                <input type="text" name="est_year" id="est" value="<?= $ar['est_year'] ?>">
            </td>
            <td>Name of VC</td>
            <td>:</td>
            <td colspan="2">
                <input type="text" name="vcn" value="<?= $ar['vc_name'] ?>">
            </td>
        </tr>
        <?
        include ("../database.php");
        $sql = "select * from more_univ where is_active=1 and id_teacher='$update_id'";
        $resul = mysql_query($sql);
        $n = 1;
        $i = 1;
        while ($row = mysql_fetch_array($resul)) {
            ?>
            <tr>
                <td><?= $n . ') ' ?>Name of Faculty</td>
                <td>:</td>
                <td>
                    <input type="text" name="fac" value="<?= $row['faculty'] ?>">
                </td>

                <td><?= $n . ') ' ?>DIN</td>
                <td>:</td>
                <td>
                    <input type="text" name="dn" value="<?= $row['din'] ?>">
                </td>
            <input type="hidden" value="<?= $row['id']; ?>" name="id">
            <td><a href="universityupdate/add_fdin.php?id=<?= $row['id'] ?>">Edit</a>/<a href="universityupdate/delete.php?id=<?= $row['id'] ?>">Delete</a></td>
            </tr>
            <?
            $n++;
            $i++;
            ?>
            <?
        }
        ?>

        <tr>
            <td><span>*</span>Session</td>
            <td>:</td>
            <td colspan="6">
                <select name="session">
                    <option value="Select" selected="selected">Select</option>
                    <option value="Morning" <? if ($se == 'Morning') echo $select; ?>>Morning</option>
                    <option value="Day" <? if ($se == 'Day') echo $select; ?>>Day</option>
                    <option value="Morning & Day"  <? if ($se == 'Morning & Day') echo $select; ?>>Morning & Day</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><span>*</span>Starting Time</td>
            <td>:</td>
            <td>
                <input type="text" name="srtt" id="time" value="<?= $ar['start'] ?>" onblur="show()">
            </td>
            <td><span>*</span>Ending Time</td>
            <td>:</td>
            <td colspan="2">
                <input type="text" name="endt" value="<?= $ar['end'] ?>">
            </td>
        </tr>

        <tr>
            <td>No. of Campus</td>
            <td>:</td>
            <td>
                <input type="text" name="no_campus" value="<?= $ar['no_campus'] ?>">
            </td>
            <td style="width:100%"><span>*</span>Admission Time</td>
            <td>:</td>
            <td colspan="2">
                <input type="text" name="adt"  value="<?= $ar['ad_time'] ?>"> 
            </td>
        </tr>

        <tr>
            <td><span>*</span>Security system</td>
            <td>:</td>
            <td>
                <select name="sect">
                    <option value="Select" selected="selected">Select</option>
                    <option value="Yes" <? if ($ss == 'Yes') echo $select ?>>Yes</option>
                    <option value="No" <? if ($ss == 'No') echo $select ?>>No</option>
                </select>
            </td>
            <td><span>*</span>Transportation</td>
            <td>:</td>
            <td colspan="3">
                <select name="transp">
                    <option value="Select" selected="selected">Select</option>
                    <option value="Yes" <? if ($ss == 'Yes') echo $select ?>>Yes</option>
                    <option value="No" <? if ($ss == 'No') echo $select ?>>No</option>
                </select>
            </td>
        </tr>

        <tr>
            <td><span>*</span>Hostel Facility</td>
            <td>:</td>
            <td>
                <select name="hostel">
                    <option value="Select" selected="selected">Select</option>
                    <option value="Yes" <? if ($ss == 'Yes') echo $select ?>>Yes</option>
                    <option value="No" <? if ($ss == 'No') echo $select ?>>No</option>
                </select>
            </td>
            <td>Classroom Facility</td>
            <td>:</td>
            <td colspan="2">
                <input type="text" name="clssroom" value="<?= $ar['classroom'] ?>">
            </td>
        </tr>

        <tr>
            <td>Location Details</td>
            <td>:</td>
            <td colspan="5">
                <textarea name="locd">
                    <?= $ar['loc_details'] ?>
                </textarea>

            </td>
        </tr>

        <tr>
            <td><span>*</span>Mobile No.</td>
            <td>:</td>
            <td>
                <input type="text" name="mobile" id="mobile" value="<?= $ar['mobile'] ?>">
            </td>
            <td><span>*</span>Phone No.</td>
            <td>:</td>
            <td  colspan="2">
                <input type="text" name="phone" id="phone" onclick="phon()" value="<?= $ar['phone'] ?>">
            </td>
        </tr>
        <tr>
            <td><span>*</span>Email ID</td>
            <td>:</td>
            <td>
                <input type="text" name="eid" id="email" value="<?= $ar['email'] ?>">
            </td>
            <td><span>*</span>Website URL</td>
            <td>:</td>
            <td  colspan="2">
                <input type="text" name="weburl" id="url" value="<?= $ar['web'] ?>">
            </td>
        </tr>

        <tr>
            <td style="width:58%">Other Information</td>
            <td>:</td>
            <td colspan="5">
                <textarea type="text" name="oinfo">
                    <?= $ar['other_info'] ?>
                </textarea>
            </td>
            <td>
                <input type="hidden" name="update_id" value="<?= $update_id ?>"
            </td>
        </tr>
    </table>
    <p style="text-align: center"> <input type="submit" name="send" value="Edit"></p>

</form>
