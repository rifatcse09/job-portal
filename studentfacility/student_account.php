<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>PentaGems Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js"></script>
        <!--username check-->
        <script type="text/javascript" src="js/jquery.js"></script>
        <!--start date-->
        <link  rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui.css" />
        <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript">
            //ON LOADING PAGE
            $(document).ready(function() {
                $("#dob").datepicker({dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, yearRange: "-100:+0"});
            });
        </script>
        <!--end date-->

        <script type="text/javascript">

            $(document).ready(function() {
                var min_chars = 3;
                var characters_error = 'Minimum amount of chars is 3';
                var checking_html = '<img src="images/loading.gif" /> Checking...';
                //when button is clicked
                $('#check_username_availability').click(function() {
                    //run the character number check
                    if ($('#username').val().length < min_chars) {
                        //if it's bellow the minimum show characters_error text
                        $('#username_availability_result').html(characters_error);
                    } else {
                        //else show the cheking_text and run the function to check
                        $('#username_availability_result').html(checking_html);
                        check_availability();
                    }
                });

            });

            //function to check username availability	
            function check_availability() {
                //get the username
                var username = $('#username').val();
                //use ajax to run the check
                $.post("user_check/check_user_st.php", {username: username},
                function(result) {
                    //if the result is 1
                    if (result == 1) {
                        //show that the username is available
                        $('#username_availability_result').html('<span class="is_available"><b>' + username + '</b> is Available</span>');
                    } else {
                        //show that the username is NOT available
                        $('#username_availability_result').html('<span class="is_not_available"><b>' + username + '</b> is not Available</span>');
                    }
                });

            }

            function id_check() {
                var testresults;
                var str = document.getElementById('student_id');
                var filter = /^[0-9]+$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Please input a valid ID!");
                    testresults = false;
                }
                return (testresults);
            }

            function stname_check() {
                var testresults;
                var str = document.getElementById('stname');
                var filter = /^[a-zA-Z. ]{3,30}$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid Name between 3-30 characters!");
                    testresults = false;
                }
                return (testresults);
            }

            function mobile_check() {
                var testresults;
                var str = document.getElementById('mno');
                var filter = /^[0-9]{5}[0-9]{6}$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid mobile no.!");
                    testresults = false;
                }
                return (testresults);
            }

            function checkemail() {
                var testresults;
                var str = document.getElementById('email');
                var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid email address!");
                    testresults = false;
                }
                return (testresults);
            }

            function image_check()
            {
                var fup = document.getElementById('image');
                var fileName = fup.value;
                var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
                if (ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "PNG" || ext == "png")
                {
                    return true;
                }
                else
                {
                    alert("Upload gif/jpg/png images only");
                    fup.focus();
                    return false;
                }
            }


            function gpa_check() {
                var testresults;
                var str = document.getElementById('gpa');
                var filter = /^(?:(?:[0-4](?:\.\d\d?)?)|5(?:\.0)?)$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid GPA!");
                    testresults = false;
                }
                return (testresults);
            }

            function year_check() {
                var testresults;
                var str = document.getElementById('yearpass');
                var filter = /^(19|20)\d{2}$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid Year!");
                    testresults = false;
                }
                return (testresults);
            }



            function valid() {
                if (document.regis.username.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter UserName.");
                    document.regis.username.focus();
                    return  false;
                }

                if (document.regis.password.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter Password.");
                    document.regis.password.focus();
                    return false;
                }

                if (document.regis.student_id.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter ID.");
                    document.regis.student_id.focus();
                    return false;
                }
                if (id_check() !== true) {
                    return false;
                }
                if (document.regis.student_name.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter Name.");
                    document.regis.student_name.focus();
                    return false;
                }
                if (stname_check() !== true) {
                    return false;
                }

                if (document.regis.gender.value == "Select Gender.....") {
                    alert("(Red * ) symbol field is mandatory. Enter Gender.");
                    document.regis.gender.focus();
                    return false;
                }

                if (document.regis.present_address.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter Permanent Address.");
                    document.regis.present_address.focus();
                    return false;
                }

                if (document.regis.mobile_no.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter Mobile no.");
                    document.regis.mobile_no.focus();
                    return false;
                }
                if (mobile_check() !== true) {
                    return false;
                }
                if (document.regis.email.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter Email.");
                    document.regis.email.focus();
                    return false;
                }
                if (checkemail() !== true) {
                    return false;
                }
                if (document.regis.userfile.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter an image");
                    document.regis.userfile.focus();
                    return false;
                }

                if (image_check() !== true) {
                    return false;
                }

                if (document.regis.current_education.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter Current education.");
                    document.regis.current_education.focus();
                    return false;
                }
                if (document.regis.institute.value == "") {
                    alert("All fields are Mandatory. Please enter your institute name.");
                    document.regis.institute.focus();
                    return false;
                }
                if (document.regis.student_result.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter GPA.");
                    document.regis.student_result.focus();
                    return false;
                }

                if (gpa_check() !== true) {
                    return false;
                }
                if (document.regis.yearpass.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter Passing year.");
                    document.regis.yearpass.focus();
                    return false;
                }
                if (year_check() !== true) {
                    return false;
                }

                return true;
            }

        </script>

        <style type="text/css">
            .hovertable
            {
                float: left;
                margin-top: 25px;
                /*width: 100%;*/
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                /*border-width: 1px;*/
                font-size: 14px;
                padding:2px;
                /*border-style: solid;*/
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;

            }
            table.hovertable td {
                /*border-width: 1px;*/
                font-size: 13px;
                padding: 8px;
                /*                border-style: solid;*/
                border-color: #a9c6c9;
            }
        </style>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header_student.php"); ?>   
            </div>  
            <div id="navigationbar">
                <? include ("searchmenu.php"); ?>
            </div>
            <div id="content"> 
                <div id="left_content_st">
                    <form action="insert/student_insert.php" name="regis" onsubmit="return valid()" method="POST" enctype="multipart/form-data" style=" float: left;width:70%; height: auto; margin-top: 40px;padding-left: 5px;">
                        <table class='hovertable'>
                            <tr>
                                <th colspan="9" style="font-size:20px ;font-weight: bold; background:  #D9E021;">Welcome to Student Registration Panel</th>
                            </tr>
                            <tr>
                                <th colspan="7">Login Information</th>
                            </tr>
                            <tr>
                                <td><span>*</span>User Name</td>
                                <td>:</td>
                                <td colspan="5">
                                    <input type="hidden" name="user_type" value="student" readonly="student" />
                                    <input type="text" name="username" id="username" /> <div id='username_availability_result'></div>
                                    <input type='button' id='check_username_availability' value='Ensure Availability'>
                                </td>
                            </tr>
                            <tr>
                                <td><span>*</span>Password</td>
                                <td>:</td>
                                <td colspan="5"><input type="password" name="password" /></td>
                            </tr>
                            <tr>
                                <th colspan="7">Personal Information</th>
                            </tr>
                            <tr>
                                <td><span>*</span>Student ID</td>
                                <td>:</td>
                                <td><input type="text" name="student_id" id="student_id" /></td>

                                <td>&nbsp;</td>

                                <td><span>*</span>Student Name</td>
                                <td>:</td>
                                <td><input type="text" name="student_name" id="stname" ></td>
                            </tr>
                            <tr>
                                <td><span>*</span>Gender</td>
                                <td>:</td>
                                <td>
                                    <select name="gender">
                                        <option>Select Gender.....</option>
                                        <option value="Male">Male</option> 
                                        <option value="Female">Female</option>
                                    </select>
                                </td>

                                <td>&nbsp;</td>

                                <td>Date Of Birth</td>
                                <td>:</td>
                                <td><input type="text" name="dob" id="dob" /></td>
                            </tr>
                            <tr>
                                <td><span>*</span>Present Address</td>
                                <td>:</td>
                                <td><textarea name="present_address"></textarea></td>

                                <td>&nbsp;</td>

                                <td>Permanent Address</td>
                                <td>:</td>
                                <td><textarea name="per_address"></textarea></td>
                            </tr>
                            <tr>
                                <td>Blood Group</td>
                                <td>:</td>
                                <td><select name="blood">
                                        <option>Select Blood Group....</option>
                                        <option>A+</option>
                                        <option>B+</option>
                                        <option>AB+</option>
                                        <option>O+</option>
                                        <option>A-</option>
                                        <option>B-</option>
                                        <option>AB-</option>
                                        <option>O-</option>
                                    </select>
                                </td>

                                <td>&nbsp;</td>

                                <td><span>*</span>Mobile No</td>
                                <td>:</td>
                                <td><input type="text" name="mobile_no" id="mno" /></td>
                            </tr>
                            <tr>
                                <td><span>*</span>E-mail</td>
                                <td>:</td>
                                <td><input type="text" name="email" id="email"/></td>

                                <td>&nbsp;</td>

                                <td><span>*</span>Picture :</td>
                                <td></td>
                            <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
                            <td><input type="file" name="userfile" id="image" style="margin-left: -70px;float: left;"></td>
                            </tr>

                            <tr>
                                <th colspan="7">Academic Information</th>
                            </tr>
                            <tr>
                                <td><span>*</span>Current Education</td>
                                <td>:</td>
                                <td><input type="text" name="current_education" /></td>

                                <td>&nbsp;</td>

                                <td><span>*</span>Institute Name</td>
                                <td>:</td>
                                <td><input type="text" name="institute" /></td>
                            </tr>
                            <tr>
                                <td><span>*</span>Current GPA</td>
                                <td>:</td>
                                <td><input type="text" name="student_result" id="gpa"/>(5)</td>

                                <td>&nbsp;</td>

                                <td><span>*</span>Year Of Passing</td>
                                <td>:</td>
                                <td><input type="text" name="yearpass" id="yearpass"/></td>
                            </tr>

                        </table>
                        <p style="margin-left: 70%; color:#F15A24;"><input type="submit" value="Submit" name="upload"></p>
                    </form>
                </div>
                <div id="right_content"  style="margin-top:26px">
                    <div id="tutrsrch">
                        <div id="tutrsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Tutor Search</p>
                        </div>
                        <form action="teachersearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                      <td>Subject</td>
                                    <td>
                                        <select name="teaching_subject">
                                            <option selected="selected">Select subject</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT teaching_subject FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ts = $row['teaching_subject'];
                                                echo '<option value="' . $ts . '">' . $ts . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="stdsrch">
                        <div id="stdsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Student Search</p>
                        </div>
                        <form action="stdsearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM student_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

<!--                    <div id="TV">
                        <img src="images/tv.png">
                    </div>-->
                </div>
<!--                <div id="gallery">
                    <div id="sub_gallery">
                        <? //include("gallery.php"); ?>
                    </div>
                </div>-->
            </div>
        </div>

        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>