<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>PentaGems Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link  rel="stylesheet" type="text/css" href="css/searchstyle.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>

        <style type="text/css">
            .hovertable
            {
                margin: 25px auto;
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 18px;
                padding: 2px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;
            }
            table.hovertable td {
                font-size: 14px;
                padding: 8px 6px 6px 18px;
                background: #ceceb7;
            }
        </style>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div>
            <div id="navigationbar">
                <? include ("searchmenu.php"); ?>
            </div>
            <div id="content"> 
                <table align="center" class="hovertable">
                    <tr>
                        <th colspan="7">University Information Details</th>
                    </tr>
                    <?
                    include ("database.php");
                    $s_id = $_GET['id'];
                    $sql = "SELECT * FROM varsity_list WHERE varsity_list.is_active=1 AND varsity_list.id='$s_id'";
                    $result = mysql_query($sql);
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td colspan="7">
                        </tr>
                        <tr>
                            <td>University Type</td>
                            <td>:</td>
                            <td> <? echo $row['varsity_type'] . '.  '; ?></td>
                            <td>University Name</td>
                            <td>:</td>
                            <td> <? echo $row['name'] . '.  '; ?></td>
                        </tr>

                        <tr>
                            <td>Established Year</td>
                            <td>:</td>
                            <td><? echo $row['est_year'] . '.  '; ?></td>
                            <td>Name of VC</td>
                            <td>:</td>
                            <td><? echo $row['vc_name'] . '.  '; ?></td>
                        </tr>
                        <?
                        $s = "select * from more_univ where is_active=1 and id_teacher='$s_id'";
                        $result = mysql_query($s);
                        $r = mysql_num_rows($result);
                        $i = 1;
                        while ($ro = mysql_fetch_array($result)) {
                            ?>

                            <tr>
                                <td><?= $i . '. ' ?>Name of Faculty</td>
                                <td>:</td>
                                <td>
                                    <?= $ro['faculty'] ?>
                                </td>
                                <td><?= $i . '. ' ?>DIN</td>
                                <td>:</td>
                                <td><?= $ro['din'] ?></td>

                            </tr>

                            <?
                            $i++;
                        }
                        ?>
                        <tr>
                            <td>Session</td>
                            <td>:</td>
                            <td><? echo $row['session'] . '.  '; ?></td>
                            <td>Starting at</td>
                            <td>:</td>
                            <td><? echo $row['start'] . '.  '; ?></td>
                        </tr>

                        <tr>
                            <td>Ending at</td>
                            <td>:</td>
                            <td><? echo $row['end'] . '.  '; ?></td>
                            <td>Campus Number</td>
                            <td>:</td>
                            <td><? echo $row['no_campus'] . '.  '; ?></td>
                        </tr>
                        <tr>
                            <td>Security System</td>
                            <td>:</td>
                            <td><? echo $row['sec_system'] . '.  '; ?></td>
                            <td>Transportation System</td>
                            <td>:</td>
                            <td><? echo $row['transport'] . '.  '; ?></td>
                        </tr>

                        <tr>
                            <td>Hostel Facility</td>
                            <td>:</td>
                            <td><? echo $row['hostel'] . '.  '; ?></td>
                            <td>Classroom Facility</td>
                            <td>:</td>
                            <td><? echo $row['classroom'] . '.  '; ?></td>
                        </tr>
                        <tr>
                            <td>Mobile & Phone No.</td>
                            <td>:</td>
                            <td><?=
                                $row['mobile'] . ' '.'<span>&</span>'.' ';
                                echo $row['phone']
                                ?></td>
                            <td>Email Address</td>
                            <td>:</td>
                            <td><? echo $row['classroom'] . '.  '; ?></td>
                        </tr>
                        <tr>
                            <td>Web URL</td>
                            <td>:</td>
                            <td colspan="4"> <?= $row['web'] ?> </td>
                        </tr>
                        <tr>
                            <td>Location details</td>
                            <td>:</td>
                            <td colspan="4"><? echo $row['loc_details'] . '.  '; ?></td>
                        </tr>
                        <tr>
                            <td>Other info</td>
                            <td>:</td>
                            <td colspan="4"><? echo $row['other_info'] . '.   '; ?></td>
                        </tr>
                        <?
                    }
                    ?>
                </table>

<!--                <div id="gallery">
                    <div id="sub_gallery">
                    <? //include("gallery.php"); ?>
                    </div>
                </div>-->
            </div>
        </div>
        <div id="clear">
        </div>   
        <div id="footer">
            <div id="footer_div">
              <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>
