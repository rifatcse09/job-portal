<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>PentaGems Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>
    </head>
    <body>

        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div> 
            <!-- BEGIN CBOX - www.cbox.ws - v001 -->
            <!--            <div id="cboxdiv" style="text-align: center; line-height: 0">
                            <div><iframe frameborder="0" width="200" height="120" src="http://www4.cbox.ws/box/?boxid=4129056&amp;boxtag=wjw27l&amp;sec=main" marginheight="2" marginwidth="2" scrolling="auto" allowtransparency="yes" name="cboxmain4-4129056" style="border:#ababab 1px solid;" id="cboxmain4-4129056"></iframe></div>
                            <div><iframe frameborder="0" width="200" height="75" src="http://www4.cbox.ws/box/?boxid=4129056&amp;boxtag=wjw27l&amp;sec=form" marginheight="2" marginwidth="2" scrolling="no" allowtransparency="yes" name="cboxform4-4129056" style="border:#ababab 1px solid;border-top:0px" id="cboxform4-4129056"></iframe></div>
                        </div>
                         END CBOX -->
            <div id="content"> 
                <!--<a href="tel:01924828156">Call me</a>-->
                <div id="left_content">

                    <div id="lftyellowdv">
                        <div id="stimage_text">
                            <div id="stimage">
                            </div>
                            <div id="stimage_hdr">
                                <p style="float:left; margin: 10px 0px 0px 0px;"> FOR STUDENTS</p>
                                <p style=" float: left;margin:0px; color: #D9E021;">Search Thousands of Tutors</p>
                                <hr style="float:left; width:100%; height:2px; background-color: #CCCCCC;"></hr>
                            </div>
                            <div id="stimage_content">
                                <p style="float: left;margin-top: 0px; font-size: 80%;">Find a tutor in your area Contact and arrange Lessons with<br>
                                    tutors <br>
                                    Provide and read tutor feedback
                                </p>
                            </div>
                            <div id="joinnow">
                                <a href="student_account.php" target="_blank"><p>Join Now</p></a>
                            </div>
                        </div>
                        <div id="tutrimage_text">
                            <div id="tutrimage">
                            </div>
                            <div id="tutrimage_hdr">
                                <p style="float:left; margin: 10px 0px 0px 0px;">FOR TUTORS</p>
                                <p style=" float: left;margin:0px;color: #D9E021;">Let students Find you</p>
                                <hr style="float:left; width:100%; height:2px; background-color: #CCCCCC;"></hr>
                            </div>
                            <div id="tutrimage_content">
                                <p style="float: left;margin-top: 0px; font-size: 80%;">Create your own tutor profile for<br> 
                                    free <br>
                                    Allow Students to find you by <br>
                                    postcode<br>
                                    List all your subjects and set your<br>
                                    own prices.
                                </p>
                            </div>
                            <div id="joinnow">
                                <a href="teacher_account.php" target="_blank"><p>Join Now </p></a>
                            </div>
                        </div>
                    </div> 
                    <div id="under_lfyellowdv">   

                        <div id="lfbrows">
                            <div id="lfbrowsheader">
                                <p style="text-align: center; margin-top: 1%; font-size: 20px; color: #D9E021;">Browser Our Tutors By Subject</p>
                            </div>
                            <div id="images">
                                <div id="fst_images">
                                    <a href="tutor_search_subject/academic.php"><img src="images/aca.png"></a> 
                                    <a href="tutor_search_subject/academic.php" style="text-decoration: none"><p>Academic</p></a>
                                </div>
                                <div id="fst_images">
                                    <a href="tutor_search_subject/languge.php" style="text-decoration: none"><img src="images/lan.png"></a> 
                                    <a href="tutor_search_subject/languge.php" style="text-decoration: none"><p>Languages</p></a>
                                </div>
                                <div id="fst_images">
                                    <a href="tutor_search_subject/music.php" style="text-decoration: none"><img src="images/music.png"></a> 
                                    <a href="tutor_search_subject/music.php" style="text-decoration: none"><p>Music</p></a>
                                </div>
                                <div id="fst_images">
                                    <a href="tutor_search_subject/art.php" style="text-decoration: none"><img src="images/art.png"></a> 
                                    <a href="tutor_search_subject/art.php" style="text-decoration: none"><p>Art</p></a>
                                </div>
                                <div id="fst_images">
                                    <a href="tutor_search_subject/health.php" style="text-decoration: none"><img src="images/health.png"></a> 
                                    <a href="tutor_search_subject/health.php" style="text-decoration: none"><p>Health</p></a>
                                </div>
                                <div id="fst_images">
                                    <a href="tutor_search_subject/it.php" style="text-decoration: none"><img src="images/IT.png"></a> 
                                    <a href="tutor_search_subject/it.php" style="text-decoration: none"><p>IT</p></a>
                                </div>
                            </div>
                        </div>
                        <div id="lfexplor">

                            <div id="lfexplorheader">
                                <p style="text-align: center; margin-top: 1%; font-size: 20px; color: #D9E021;">Explore Teacher by Division</p>
                            </div>
                            <div id="map">
                            </div>
                            <div id="division">
                                <p> <a href="dhaka.php">Dhaka</a><br>
                                    <a href="chittagong.php">Chittagong</a><br>
                                    <a href="rajshahi.php">Rajshahi</a><br>
                                    <a href="khulna.php">Khulna</a><br>
                                    <a href="barisal.php">Barisal</a><br>
                                    <a href="rangpur.php">Rangpur</a><br>
                                    <a href="sylhet.php">Sylhet</a><br>
                                </p>
                            </div>
                        </div>
                        <div id="kstdiv">
                            <div id="jodinfo">
                                <div id="jodinfoheader">
                                    <p>Kindergarten List</p>
                                </div>
                                <div id="tabil">
                                    <?
                                    include ("database.php");
                                    if (isset($_GET["page"])) {
                                        $page = $_GET["page"];
                                    } else {
                                        $page = 1;
                                    }
                                    $start_from = ($page - 1) * 3;
                                    $sql = "SELECT * FROM kg_list WHERE is_active=1 limit $start_from, 4";
                                    $result = mysql_query($sql);
//                                $row = mysql_fetch_array($result);

                                    while ($row = mysql_fetch_array($result)) {
                                        ?>
                                        <table>
                                            <tr>
                                                <td>
                                                    <a href="kg_list-search.php?id=<?= $row['id'] ?>">
                                                        <?
                                                        $total = $row['name'];
                                                        $len = strlen($total);
                                                        if ($len >30) {
                                                            $show = substr($total, 0, 30);
                                                            echo $show . '...';
                                                        } else {
                                                            echo $total;
                                                        }
                                                        ?>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                        <?
                                    }
                                    ?>
                                    <p style="text-align: center; margin-top: 0px;"><a href="more_kg_list.php" style="text-decoration:none;">More list...</a></p>
                                </div> 
                            </div>
                            <div id="stconsutancy">
                                <div id="stconsutancyheader">
                                    <p>University List</p>
                                </div>

                                <div id="tabil">
                                    <?
                                    include ("database.php");
                                    if (isset($_GET["page"])) {
                                        $page = $_GET["page"];
                                    } else {
                                        $page = 1;
                                    }
                                    $start_from = ($page - 1) * 4;
                                    $sql = "select * from varsity_list left join more_univ on varsity_list.id=more_univ.id where varsity_list.is_active=1 or more_univ.is_active=1 limit $start_from, 4";
                                    $re = mysql_query($sql);
                                    while ($rw = mysql_fetch_array($re)) {
                                        ?>

                                        <table>
                                            <tr>
                                                <td>
                                                    <a href="varsity_list_search.php?id=<?= $rw['id'] ?>">
                                                      <?
                                                        $total = $rw['name'];
                                                        $len = strlen($total);
                                                        if ($len >30) {
                                                            $show = substr($total, 0, 30);
                                                            echo $show . '...';
                                                        } else {
                                                            echo $total;
                                                        }
                                                        ?>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                        <?
                                    }
                                    ?>
                                    <p style="text-align: center; margin-top: 0px;"><a href="more_varsity_list.php" style="text-decoration:none;">View more list...</a></p>
                                </div> 
                            </div>    
                            <div id="trainfacility">
                                <div id="trainfacilityheader">
                                    <p>Training Facility</p>
                                </div>
                                <div id="train_facility">
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <div id="right_content">
<!--                    <div style=" float: left; margin-left: 12px; margin-top: 40px; width: 89%; height: 35%; border-radius:8px; background: #F2F2F2;">
                        <p style="text-align:center; margin-top: 0;color: #D9E021; font-size: 20px; background: #808080;border-top-left-radius: 8px; border-top-right-radius: 8px;">Find us on Facebook</p>
                        <p style="text-align: center;margin: -10px auto;"><a style="text-decoration:none;font-size: 15px; color: #000;margin-top: -10px; " href="">onlinebdmoon.com/studentfacility</a></p>
                        <iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.onlinebdmoon/studentfacility.com&amp;layout=standard&amp;show_faces=true&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:20px;" allowTransparency="true"></iframe><style>.fbook{position: absolute; font-color:#ddd; top:-1668px; font-size:10px;}</style></style><a href="http://www.howtoaddlikebutton.com" class="fbook">Manager</a><style>.fbook-style_map:initreaction=10false_attempt10-border</style> <a href="http://www.kosmetikstudio-hamburg.net/fusspflege-hamburg" class="fbook">FACEBOOK</a><style>closemap"init"if=fb_connect-start="25"check_bandwith</style>
                        <iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.onlinebdmoon/studentfacility.com&amp;layout=standard&amp;show_faces=true&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90%;margin-top: 10px; margin-left: 12px; height:60px;border-radius: 8px; background: #F2F2F2;" allowTransparency="true"></iframe><style>.fbook{position: relative; font-color:#ddd; font-size:10px;}</style></style><style>.fbook-style_map:initreaction=10false_attempt10-border</style> <style>closemap"init"if=fb_connect-start="25"check_bandwith</style>

                    </div>-->
                    <div id="tutrsrch">
                        <div id="tutrsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Tutor Search</p>
                        </div>
                        <form action="teachersearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
<!--                                    <td>Subject</td>
                                    <td><input  type="text" size="20px" name="teaching_subject"></td>-->
                                    <td>Subject</td>
                                    <td>
                                        <select name="teaching_subject">
                                            <option selected="selected">Select subject</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT teaching_subject FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ts = $row['teaching_subject'];
                                                echo '<option value="' . $ts . '">' . $ts . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="stdsrch">
                        <div id="stdsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Student Search</p>
                        </div>
                        <form action="stdsearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM student_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

<!--                    <div id="TV">
                        <img src="images/tv.png">
                        <p style="text-align: center; margin-left: 15px;margin-top: 40px; font-size: 17px; "><span style="color: #42453D;">Please Contact<br> for <br>Advertisement <br>on size: <span style="color: green">250X200 px</span></span><strong><br>Mobile :<span> 01755666619</span></strong></p>
                    </div>-->
                </div>
<!--                <div id="gallery">
                    <div id="sub_gallery">
                        <? //include("gallery.php"); ?>
                    </div>
                </div>-->
            </div>
        </div>


        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>