<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>PentaGems Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>

        <script type="text/javascript">
            function check() {
                var p = document.getElementById("pass").value;
                var rP = document.getElementById("newpass").value;
                if (p != rP) {
                    document.getElementById("error").innerHTML = "passwords don't match";
                    return false;
                }
                else {
                    return true;
                }
            }
        </script>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div>            
            <div id="content"> 
                <div id="left_content">
                    <form action="update/update_admin_db.php" method="POST">
                        <fieldset>
                            <legend align="center"><strong>Edit Admin Name & Password</strong></legend>
                            <?
                            include ("database.php");
                            $sql = "SELECT * FROM user_info WHERE type='admin' AND is_active=1";
                            $result = mysql_query($sql);
                            $row = mysql_fetch_array($result);
                            $id = $row['user_id'];
                            ?>
                            <table>
                                <tr>
                                    <td>Type New Admin Name</td>
                                    <td>:</td>
                                    <td>
                                        <input style="background: #ffffff;" type="text" name="adname" value="<? echo $row['username']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Type New Password</td>
                                    <td>:</td>
                                    <td>
                                        <input style="background: #ffffff;"  type="text" name="pw" id="pass" value="<? echo $row['password']; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Confirm New Password </td>
                                    <td>:</td>
                                    <td>
                                        <input type="text" style="background: #ffffff;"  type="text" name="adpw" id="newpass">
                                    </td>
                                    <td><input type="hidden" name="update_id" value="<?php echo $id; ?>" /><span id="error"></span>
                                    </td>
                                </tr>
                            </table>
                            <p><input type="submit" name="submit" value="Submit" onclick="return check()"></p>
                        </fieldset>
                    </form>
                </div>
                <div id="right_content">
                    <div id="tutrsrch">
                        <div id="tutrsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Tutor Search</p>
                        </div>
                        <form action="teachersearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Subject</td>
                                    <td><input  type="text" size="20px" name="teaching_subject"></td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="stdsrch">
                        <div id="stdsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Student Search</p>
                        </div>
                        <form action="stdsearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM student_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

<!--                    <div id="TV">
                        <img src="images/tv.png">
                    </div>-->
                </div>
<!--                <div id="gallery">
                    <div id="sub_gallery">
                        <? //include("gallery.php"); ?>
                    </div>
                </div>-->
            </div>
        </div>


        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>