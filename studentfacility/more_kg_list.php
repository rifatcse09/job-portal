<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>PentaGems Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link  rel="stylesheet" type="text/css" href="css/searchstyle.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>

        <style type="text/css">
            .hovertable
            {
                width: 95%;
                margin: 25px auto;
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 14px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;

            }
            table.hovertable td {
                border-width: 1px;
                font-size: 13px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
        </style>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div>
            <div id="navigationbar">
                <? include ("searchmenu.php"); ?>
            </div>
            <div id="content"> 
                <table class="hovertable">
                    <tr>
                        <th>KG Name</th>
                        <th>Medium</th>
                        <th>Established Year</th>
                        <th>Principal's Name</th>
                        <th>School Time</th>
                        <th>Starting at</th>
                        <th>Ending at</th>
                        <th>View</th>
                    </tr>
                    <?
                    include ("database.php");
                    if (isset($_GET["page"])) {
                        $page = $_GET["page"];
                    } else {
                        $page = 1;
                    }
                    $start_from = ($page - 1) * 20;
                    include ("database.php");
                    $sql = "SELECT * FROM kg_list WHERE is_active=1 limit $start_from, 20";
                    $result = mysql_query($sql);
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td> <? echo $row['name']; ?></td>
                            <td><? echo $row['medium'] ;?></td>
                            <td> <? echo $row['est_year'] ;?></td>
                            <td><? echo $row['princ_name'] ;?></td>
                            <td><? echo $row['school_time']; ?></td>
                            <td><? echo $row['start'] ;?></td>
                            <td><? echo $row['end'] ;?></td>
                            <td><a href="kg_list-search.php?id=<?=$row['id']?>">Details</a></td>
                        </tr>
                        <?
                    }
                    ?>
                </table>

                <p align="center" style="margin-top: -4px; clear: both">
                    <?
                    $sql = "SELECT count(kg_list.id) FROM kg_list WHERE is_active=1";
                    $p_result = mysql_query($sql);
                    $row = mysql_fetch_array($p_result);
                    $tot_rec = $row[0];
                    $tot_page = ceil($tot_rec / 20);
                    for ($i = 1; $i <= $tot_page; $i++) {
                        if ($page == $i) {
                            echo $i . ' ';
                        } else {
                            echo "<a style='text-decoration: none; color: green' href = 'more_kg_list.php?page=" . $i . "'>" . $i .' '. "</a>";
                        }
                    }
                    ?>
                </p>

<!--                <div id="gallery">
                    <div id="sub_gallery">
                        <? //include("gallery.php"); ?>
                    </div>
                </div>-->
            </div>
        </div>
        <div id="clear">
        </div>   
        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>
