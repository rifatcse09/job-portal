<?php
error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>PentaGems Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />

            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>
            
            <style type="text/css">
            .hovertable
            {
                /*float: left;*/
                margin:auto;
                margin-top: 25px;
                width: 80%;
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 14px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;

            }
            table.hovertable td {
                /*border-width: 1px;*/
                font-size: 13px;
                padding: 8px;
/*                border-style: solid;
                border-color: #a9c6c9;*/
            }
        </style>
            
        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="content_tprof"> 
                    <table class="hovertable">
                        <tr>
                            <th colspan="4">Teacher Profile</th>
                        </tr>
                        <?
                        include("database.php");
                        $show_id = $_REQUEST['t_id'];
                        $sql = "select * from teacher_info where t_id='$show_id'";
                        $data = mysql_query($sql);
                        $row = mysql_fetch_array($data); {
                            ?>
                            <tr>
                                <td colspan="4">
                                    <table class="hovertable">
                                        <tr>
                                            <td valign="top"><b><u><? echo $row['tea_name']; ?></u></b><br />
                                                <? echo $row['present_address']; ?></td>
                                            <td width="350">&nbsp;</td>
                                            <td valign="top"><img src="../teacher/teacher_image/<? echo $row['name']; ?>" width="80" height="50" /></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4"><b><u>Carrer Objective</u></b></td>
                            </tr>
                            <tr>
                                <td>Carrer Objective</td>
                                <td>:</td>
                                <td colspan="2"><? echo $row['carrer_obj']; ?>
                            </tr>
                            <tr>
                                <td colspan="4"><b><u>Academic Information</u></b></td>
                            </tr>
                            <tr>
                                <td>ast Graduation</td>
                                <td>Institute Name</td>
                                <td>Result</td>
                                <td>Year Of Passing</td>
                            </tr>
                            <tr>
                                <td><? echo $row['last_graduation']; ?></td>
                                <td><? echo $row['institute']; ?></td>
                                <td><? echo $row['tea_result']; ?></td>
                                <td><? echo $row['yearpass']; ?></td>
                            </tr>
                            <tr>
                                <td colspan="4"><b><u>Teaching Experience</u></b></td>
                            </tr>
                            <tr>
                                <td>Teaching Experience</td>
                                <td>:</td>
                                <td colspan="2"><? echo $row['teaching_exp']; ?>
                            </tr>
                            <tr>
                                <td colspan="4"><b><u>Teaching Subject</u></b></td>
                            </tr>
                            <tr>
                                <td>Teaching Experience</td>
                                <td>:</td>
                                <td colspan="2"><? echo $row['teaching_subject']; ?>
                            </tr>
                            <tr>
                                <td colspan="4"><b><u>Personal Information</u></b></td>
                            </tr>
                            <tr>
                                <td colspan="4"><table>
                                        <tr>
                                            <td>Teacher ID</td>
                                            <td>:</td>
                                            <td><? echo $row['tea_id']; ?></td>
                                            <td></td>
                                            <td>Gender</td>
                                            <td>:</td>
                                            <td><? echo $row['gender']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Blood Group</td>
                                            <td>:</td>
                                            <td><? echo $row['blood']; ?></td>
                                            <td></td>
                                            <td>Date Of Birth</td>
                                            <td>:</td>
                                            <td><? echo $row['bod']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile No</td>
                                            <td>:</td>
                                            <td><? echo $row['mobile_no']; ?></td>
                                            <td></td>
                                            <td>E-mail</td>
                                            <td>:</td>
                                            <td><? echo $row['email']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Permanent Address</td>
                                            <td>:</td>
                                            <td colspan="5"><? echo $row['per_address']; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?
                        }
                        ?>
                    </table>
<!--                <div id="gallery">
                    <div id="sub_gallery">
                        <? //include("gallery.php"); ?>
                    </div>
                </div>-->

                </div>

            </div>

            <div id="clear">
            </div>   


            <div id="footer">
                <div id="footer_div">
                    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>