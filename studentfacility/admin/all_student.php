<?php
//error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
//$user_id = $_SESSION['user_id'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>PentaGems Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>

            <style type="text/css">
            .hovertable
            {
                float: left;
                margin-top: 25px;
                width: 100%;
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 14px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;

            }
            table.hovertable td {
                border-width: 1px;
                font-size: 13px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
        </style>

        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="content_stdprof"> 

                    <?php
                    // error_reporting(0);
                    //declaration
                    include("database.php");
                    if (isset($_GET["page"])) {
                        $page = $_GET["page"];
                    } else {
                        $page = 1;
                    };
                    $start_from = ($page - 1) * 2;  //2 is the no. of row per page.
                    $sql = "SELECT * FROM student_info where is_active=1 LIMIT $start_from, 2";
                    $rs_result = mysql_query($sql);
                    ?>

                    <table class="hovertable">
                        <tr>
                            <th colspan="11">All Student Information</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>Present Address</th>
                            <th>Mobile</th>
                            <th>E-mail</th>
                            <th>Current Education</th>
                            <th>Institute</th>
                            <th>Result</th>
                            <th>Year Of Passing</th>
                            <th>Picture</th>
                            <th>Action</th>
                            <th>Action</th>
                        </tr>
                        <?
                        include("database.php");
                        while ($row = mysql_fetch_assoc($rs_result)) {
                            ?>
                            <tr>
                                <td><? echo $row['student_name']; ?></td>
                                <td><? echo $row['present_address']; ?></td>
                                <td><? echo $row['mobile_no']; ?></td>
                                <td><? echo $row['email']; ?></td>
                                <td><? echo $row['current_education']; ?></td>
                                <td><? echo $row['institute']; ?></td>
                                <td><? echo $row['student_result']; ?></td>
                                <td><? echo $row['yearpass']; ?></td>
                                <td><img src="../student/student_image/<? echo $row['name']; ?>" width="30" height="16" /></p></td>
                                <td><a href="student_profile.php?stu_id=<? echo $row['stu_id']; ?>" style="text-decoration:none; color:#006699" target="_blank">View</a></td>
                                <td><a href="delete/student_delet.php?stu_id=<? echo $row['stu_id']; ?>" style="text-decoration:none; color:#006699">Delete</a></td>
                            </tr>
                            <?
                        }
                        ?>
                    </table>

                    <p align="center">
                        <?php
                        include("database.php");
                        $sql = "SELECT COUNT(stu_id) FROM student_info";
                        $rs_result = mysql_query($sql);
                        $row = mysql_fetch_row($rs_result);
                        $total_records = $row[0];
                        // echo $row[0];
                        $total_pages = ceil($total_records / 2);
                        //paging.......
                        for ($i = 1; $i <= $total_pages; $i++) {
                            echo "<a href='all_student.php?page=" . $i . "'>" . $i . "</a> ";
                        };
                        ?>
                    </p>


<!--                    <div id="gallery">
                        <div id="sub_gallery">
                            <? //include("gallery.php"); ?>
                        </div>
                    </div>-->
                </div>
            </div>


            <div id="clear">
            </div>   
            <div id="footer">
                <div id="footer_div">
                    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>