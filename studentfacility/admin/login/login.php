
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Moon Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="../css/style.css" />
        <link href="../images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>
        
        <style type="text/css">
            .hovertable
            {
                text-align: center;
             margin-top: 25px;
             
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 14px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;
                
            }
            table.hovertable td {
                /*border-width: 1px;*/
                font-size: 13px;
                padding: 8px;
                /*border-style: solid;*/
                /*border-color: #a9c6c9;*/
            }
        </style>
        
    </head>
    <body>

        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div>            
            <div id="content"> 
                <div id="left_content">
                    <form action="login/user_check.php" method="post">
                        <fieldset style="width:94%;">
                            <legend align="center"><strong>Admin Login Panel</strong></legend>
                            <table class="hovertable">
                                <tr>
                                    <td>User Name</td>
                                    <td>:</td>
                                    <td><input style="background:#ffffff;" type="text" name="username" /></td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td>:</td>
                                    <td><input style="background:#ffffff;"  type="password" name="password" /></td>
                                </tr>
<!--                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><input type="image" src="images/login_button.jpg" /></td>
                                </tr>-->
                            </table>
                            <p style="clear:left"><input type="image" src="images/login-content.png" /></p>
                        </fieldset>
                    </form>
                </div>
                <div id="right_content">
                    <div id="tutrsrch">
                        <div id="tutrsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Tutor Search</p>
                        </div>
                        <form action="teachersearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
<!--                                    <td>Subject</td>
                                    <td><input  type="text" size="20px" name="teaching_subject"></td>-->
                                    <td>Subject</td>
                                    <td>
                                        <select name="teaching_subject">
                                            <option selected="selected">Select subject</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT teaching_subject FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ts = $row['teaching_subject'];
                                                echo '<option value="' . $ts . '">' . $ts . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="stdsrch">
                        <div id="stdsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Student Search</p>
                        </div>
                        <form action="stdsearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM student_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="TV">
                        <!--<img src="images/tv.png">-->
                        <blink><p style="text-align: center; margin-left: 15px;margin-top: 40px; font-size: 17px; "><span style="color: #42453D;">Please Contact for Advertisement on size: 250X200 px</span><strong><br>Mobile :01755666619</strong></p></blink>
                    </div>
                </div>
                <div id="gallery">
                    <div id="sub_gallery">
                        <? include("gallery.php"); ?>
                    </div>
                </div>
            </div>
        </div>


        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>