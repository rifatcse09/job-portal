<?php
//error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>Moon Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />
            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>
            
            <style type="text/css">
                .hovertable
                {
                    float: left;
                    width: 96%;
                    margin-left: 20px;
                    margin-top: 30px;
                    margin-bottom: 10px;
                }
                table.hovertable {
                    font-family: verdana,arial,sans-serif;
                    font-size:11px;
                    color:#333333;
                    border-width: 1px;
                    border-color: #999999;
                    border-collapse: collapse;
                }
                table.hovertable th {
                    background-color:#c3dde0;
                    border-width: 1px;
                    font-size: 14px;
                    padding: 8px;
                    border-style: solid;
                    border-color: #a9c6c9;
                }
                table.hovertable tr {
                    background-color:#d4e3e5;

                }
                table.hovertable td {
                    border-width: 1px;
                    font-size: 12px;
                    padding: 8px;
                    /*border-style: solid;*/
                    border-color: #a9c6c9;
                }
            </style>
            
        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="contentmsf"> 
                    <form method="post" enctype="multipart/form-data" action="update/update_teacher_send_db.php">
                        <?php
                        include("database.php");
                        $sql = "SELECT * FROM teacher_info JOIN user_info ON user_info.user_id = teacher_info.t_id
                              WHERE user_info.username = '$user_name'";
                        $data = mysql_query($sql);
                        $row = mysql_fetch_array($data);
                        $update_id = $row['t_id']; 
                          {
                            ?>

                            <table class="hovertable">
                                <tr>
                                    <th colspan="6">Personal Information</th>
                                </tr>
                                <tr>
                                    <td>Teacher ID</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_tea_id" value="<? echo $row['tea_id']; ?>" /></td>
                                    <!--<td>&nbsp;</td>-->
                                    <td>Teacher Name</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_tea_name" value="<? echo $row['tea_name']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_gender" value="<? echo $row['gender']; ?>"></td>
                                    <!--<td>&nbsp;</td>-->
                                    <td>Birth Of Date</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_bod" id="bod" value="<? echo $row['bod']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Present Address</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_present_address" value="<? echo $row['present_address']; ?>" /></td>
                                    <!--<td>&nbsp;</td>-->
                                    <td>Permanent Address</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_per_address" value="<? echo $row['per_address']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Blood Group</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_blood" value="<? echo $row['blood']; ?>"></td>
                                    <!--<td>&nbsp;</td>-->
                                    <td>Mobile No</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_mobile_no" value="<? echo $row['mobile_no']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>E-mail</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_email" value="<? echo $row['email']; ?>" /></td>
                                    <!--<td>&nbsp;</td>-->
                                    <td>Picture</td>
                                    <td>:</td>
                                <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
                                <td><input type="file" name="userfile" value="<? echo $row['name']; ?>" id="userfile" /></td>
                                </tr>
                                <tr>
                                    <th colspan="6">Carrer Objective</th>
                                </tr>
                                <tr>
                                    <td>Carrer Objective</td>
                                    <td>:</td>
                                    <td colspan="5"><input type="text" name="update_carrer_obj" value="<? echo $row['carrer_obj']; ?>" /></td>
                                </tr>
                                <tr>
                                    <th colspan="6">::Academic Information::</th>
                                </tr>
                                <tr>
                                    <td>Last Graduation</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_last_graduation" value="<? echo $row['last_graduation']; ?>" /></td>
                                    <!--<td></td>-->
                                    <td>Institute Name</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_institute" value="<? echo $row['institute']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Result</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_tea_result" value="<? echo $row['tea_result']; ?>" /></td>
                                    <!--<td></td>-->
                                    <td>Year Of Passing</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_yearpass" value="<? echo $row['yearpass']; ?>" /></td>
                                </tr>
                                <tr>
                                    <th colspan="6">:: Teaching Experience ::</th>
                                </tr>
                                <tr>
                                    <td>Teaching Experience </td>
                                    <td>:</td>
                                    <td colspan="5"><input type="text" name="update_teaching_exp" value="<? echo $row['teaching_exp']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Teaching Subject</td>
                                    <td>:</td>
                                    <td colspan="5"><input type="text" name="update_teaching_subject" value="<? echo $row['teaching_subject']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Teaching Subject </td>
                                    <td>:</td>
                                    <td colspan="4"><input type="text" name="update_teaching_subject" value="<? echo $row['teaching_subject']; ?>" /></td>
                                </tr>
                                <tr>
                                    <th colspan="6">:: Teaching Location ::</th>
                                </tr>
                                <tr>
                                    <td>Tution Location</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_location" value="<? echo $row['location']; ?>" /></td>
                                    <td>Area</td>
                                    <td>:</td>
                                    <td><input type="text" name="update_area" value="<? echo $row['area']; ?>" /></td>
                                    <input type="hidden" name="update_id" value="<?php echo $update_id; ?>" />
                                </tr>
<!--                                <tr>
                                    <td><input type="hidden" name="update_id" value="<?php //echo $update_id; ?>" /></td>
                                    <td><input type="submit" name="change" value="Update" /></td>
                                </tr>-->
                            </table>
                        <p style="text-align:center;"><input type="submit" name="change" value="Update" /></td></p>
                            <?php
                        }
                        ?>
                    </form>

                    <div id="gallery">
                        <div id="sub_gallery">
                            <? include("gallery.php"); ?>
                        </div>
                    </div>

                </div>

            </div>

            <div id="clear">
            </div>   


            <div id="footer">
                <div id="footer_div">
                    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
    echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
   // echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>