<?php
error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>Moon Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />
            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>
        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="content"> 
                    <form id="form"style="float: left; width: 100%; height: auto;">
                        <table>
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td> <input type="text" name="nam" /></td>
                            </tr>
                            <tr>
                                <td>Mobile No.</td>
                                <td>:</td>
                                <td><input type="text" name="mobile" /></td>
                            </tr>
                            <tr>
                                <td>Subject</td>
                                <td>:</td>
                                <td><input type="text" name="subject" /></td>
                            </tr>
                            <tr>
                                <td>Message</td>
                                <td>:</td>
                                <td>
                                    <textarea name="message" rows="1" cols="16" VIRTUAL="wrap">
                                    </textarea>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div id="gallery">
                    <div id="sub_gallery">
                        <? include("gallery.php"); ?>
                    </div>
                </div>
            </div>

            <div id="clear">
            </div>   


            <div id="footer">
                <div id="footer_div">
                    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>