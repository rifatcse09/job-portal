<?php
//error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>Moon Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />

            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>

            <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
            <script type="text/javascript" src="js/jquery-ui.js"></script>
            <link  rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui.css" />

            <script type="text/javascript">

                $(document).ready(function() {
                    $("#pw_dialog").dialog({
                        autoOpen: false,
                        height: 170,
                        width: 450,
                        modal: true,
                        title: "Password Editing Panel",
                        position: ["center", "middle"],
                        autoresize: false
                    });
                });

                function pw_change(stu_id) {
                    $.ajax({
                        url: "update/teacher_pw_update/update_password.php?tea_id=" + tea_id,
                        type: "GET",
                        contentType: 'application/json; charset=utf-8',
                        success: function(res) {
                            if (res.redirect) {
                                alert("sorry, something is wrong. May be you are not authenticated.Page will go to the home after clicking ok.")
                                window.location.href = res.redirect;
                                return;
                            }
                            $("#pw_dialog").html(res);

                        }
                    });
                    $("#pw_dialog").dialog("open");

                }

            </script>

            <style type="text/css">
                .hovertable
                {
                    float: left;
                    margin-left: 18%;
                    width: 60%;
                    margin-top: 25px;
                    margin-bottom: 10px;
                }
                table.hovertable {
                    font-family: verdana,arial,sans-serif;
                    font-size:11px;
                    color:#333333;
                    border-width: 1px;
                    border-color: #999999;
                    border-collapse: collapse;
                }
                table.hovertable th {
                    background-color:#ffffff;
                    border-width: 1px;
                    font-size: 14px;
                    padding: 8px;
                    border-style: solid;
                    border-color: #a9c6c9;
                }
                table.hovertable tr {
                    background-color:#d4e3e5;

                }
                table.hovertable td {
                    border-width: 1px;
                    font-size: 13px;
                    padding: 8px;
                    /*border-style: solid;*/
                    border-color: #a9c6c9;
                }
            </style>

        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="contentmsf"> 
                    <form id="form"style="float: left; width: 100%; height: 100%;">
                        <table align="center" class="hovertable">
                            <tr>
                                <th colspan="6">Teacher's Profile</th>
                            </tr>

                            <?
                            include("database.php");
                            $sql = "SELECT * FROM teacher_info JOIN user_info ON teacher_info.t_id=user_info.user_id where username='$user_name'";
                            $data = mysql_query($sql);
                            while ($row = mysql_fetch_array($data)) {
                                ?>

                                <tr>
                                    <td colspan="6">
                                </tr>
                                <tr>
                                    <th colspan="6">Teacher's Personal Information</th>
                                </tr>
                                <tr>
                                    <td colspan="6"><img src="../teacher/teacher_image/<? echo $row['name']; ?>" style=" float: left; width: 120px; height: 100px; border: 3px #ffffff solid;   " /></td>
                                </tr>
                                <tr>
                                    <td>Teacher ID</td>
                                    <td>:</td>
                                    <td> <? echo $row['tea_id']; ?></td>
                                    <td>Teacher Name</td>
                                    <td>:</td>
                                    <td> <? echo $row['tea_name']; ?></td>
                                </tr>

                                <tr>
                                    <td>Gender</td>
                                    <td>:</td>
                                    <td> <? echo $row['gender']; ?></td>
                                    <td>Present Address</td>
                                    <td>:</td>
                                    <td> <? echo $row['present_address']; ?></td>
                                </tr>
                                <tr>
                                    <td>Permanent Address</td>
                                    <td>:</td>
                                    <td><? echo $row['present_address']; ?></td>
                                    <td>Blood Group</td>
                                    <td>:</td>
                                    <td> <? echo $row['blood']; ?></td>
                                </tr>

                                <tr>
                                    <td>Career Objectives</td>
                                    <td>:</td>
                                    <td colspan="6"> <? echo $row['carrer_obj']; ?></td>
                                </tr>
                                <tr>
                                    <th colspan="6">Teacher's Academic Information</th>
                                </tr>
                                <tr>
                                    <td>Graduation</td>
                                    <td>:</td>
                                    <td><? echo $row['last_graduation']; ?></td>
                                    <td>Institute Name</td>
                                    <td>:</td>
                                    <td><? echo $row['institute']; ?></td>
                                </tr>

                                <tr>
                                    <td>Additional Training</td>
                                    <td>:</td>
                                    <td><? echo $row['ad_train']; ?></td>
                                    <td>Institute Name</td>
                                    <td>:</td>
                                    <td><? echo $row['ad_ins']; ?></td>
                                </tr>

                                <tr>
                                    <th colspan="6">Information as a Tutor</th>
                                </tr>
                                <tr>
                                    <td>Teacheing Subject</td>
                                    <td>:</td>
                                    <td> <? echo $row['teaching_subject']; ?></td>
                                    <td>Level</td>
                                    <td>:</td>
                                    <td> <? echo $row['level']; ?></td>
                                </tr>

                                <tr>
                                    <td>Medium</td>
                                    <td>:</td>
                                    <td><? echo $row['medium']; ?></td>
                                    <td>Designation</td>
                                    <td>:</td>
                                    <td><? echo $row['designation']; ?></td>

                                </tr>
                                <tr>
                                    <td colspan="6"><a href="javascript:pw_change(tea_id=<? echo $row['tea_id']; ?>)"  style="float: right;background: #D9E021;text-decoration: none;font-size: 18px; color:#000;">Change Password</a></td>
                                </tr>

                                <?
                            }
                            ?>
                        </table>
                    </form>

                    <div id="pw_dialog">
                    </div>

                    <div id="gallery">
                        <div id="sub_gallery">
                            <? include("gallery.php"); ?>
                        </div>
                    </div>

                </div>

            </div>

            <div id="clear">
            </div>   


            <div id="footer">
                <div id="footer_div">
                    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>

    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>