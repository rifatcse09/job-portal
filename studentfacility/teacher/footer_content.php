<style type="text/css">
    #ac 
    {
        border:#CCCCCC solid 1px; border-radius:10px;
        background:#cccccc;
        text-decoration:none;
        color:#000000;
        float:right;
        padding:2px 10px;
    }
    #mc table tr td
    {
        color:#FFFFFF;
        font-size:10px;
        margin:0px;
        width:200px;
    }
    .h4
    {
        float:left;
        width:150px;
    }

    .moon  table 
    {
        width:250px;
    }
    .moon   tr td
    {
        width:250px;
        line-height:10px;
    }
    .moon tr td a 
    {
        color:#FFFFFF;
        font-family:Geneva, Arial, Helvetica, sans-serif;
        vertical-align:text-top;
        text-decoration:none;
        font-size:10px;}
    .moon   tr td a:hover
    {
        color:#e66a94;
        text-decoration:underline;
    }
    .moonsocial  table 
    {
        width:100px;

    }
    .moonsocial   tr td
    {
        width:100px;
        line-height:10px;
    }
    .moonsocial tr td a 
    {
        float:left;
        padding:0px 5px;
        color:#FFFFFF;
        width:150px;
        font-family:Geneva, Arial, Helvetica, sans-serif;
        vertical-align:text-top;
        text-decoration:none;
        font-size:12px;}
    .moonsocial tr td a:hover
    {
        color:#f9d11d;
        text-decoration:underline;
    }
</style>  
<table width="1000">
    <tr>
        <td valign="top"><h4 class="h4" style="margin:0 auto; color:#FFFFFF">Newsletter Sign Up</h4></td>
        <td></td>
        <td colspan="3" valign="top"><h4 class="h4" style="margin:0; color:#FFFFFF">Moon Group</h4></td>
        <td></td>
        <td valign="top"><h4 class="h4" style="margin:0; color:#FFFFFF">We're Social Nuts!</h4></td>
        <td></td>
        <td valign="top"><h4 class="h4" style="margin:0; color:#FFFFFF">Contact Us</h4></td>
    </tr>
    <tr>
        <td valign="top">
            <p style="color:#FFFFFF; font-size:12px;">Sign up for the latest events and promos.
            </p>
            <br />
            <form action="#" method="post">
                <input type="text" onclick="this.value=''" value="Enter Email ID" />
            </form>
            <br />
            <a href="#"><img src="images/signup.png" border="0" /></a>
        </td>
        <td style="margin:0;padding:0 6px;"><hr size="1" style="border-style:solid; border-color:#f8ce0c; height:220px" /></td>
        <td width="250px" valign="top">
            <table class="moon">
                <tr>
                    <td><a href="#">Moon Trade Links (PVT) Ltd.</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Event Management</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Tourism Network</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Catering </a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Interior Decoration & Furniture </a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Press & Printing</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Media & Publication</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Home Delivery </a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Green Bangla Multipurpose Co-operative Society</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Agro industries</a></td>
                </tr>
            </table>
        </td>
        <td style="margin:0;padding:0 6px;"><hr size="1" style="border-style:solid; border-color:#f8ce0c; height:220px" /></td>
        <td width="250px" valign="top">
            <table class="moon">
                <tr>
                    <td><a href="#">Moon Drink & Beverage</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Shopping Center</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Consumer Product</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Real Estate & Land Info</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Poultry, Fisheries & Dairy</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Electric &amp; Electronics</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Tour &amp; Travels</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Construction</a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Hajj Kafela </a></td>
                </tr>
                <tr>
                    <td><a href="#">Moon Training Institute</a></td>
                </tr>
            </table>
        </td>
        <td style="margin:0;padding:0 6px;"><hr size="1" style="border-style:solid; border-color:#f8ce0c; height:220px" /></td>
        <td width="150px">
            <table class="moonsocial">
                <tr>
                    <td><img src="images/social/f.png" height="30" /></td>
                    <td><a href="#">Facebook</a></td>
                </tr>
                <tr>
                    <td><img src="images/social/t.png" height="30" /></td>
                    <td><a href="#">Twitter</a></td>
                </tr>
                <tr>
                    <td><img src="images/social/l.png" height="30" /></td>
                    <td><a href="#">Linked In</a></td>
                </tr>
                <tr>
                    <td><img src="images/social/r.png" height="30" /></td>
                    <td><a href="#">RSS</a></td>
                </tr>
                <tr>
                    <td><img src="images/social/g.png" height="30" /></td>
                    <td><a href="#">Google</a></td>
                </tr>
                <tr>
                    <td><img src="images/social/y.png" height="30" /></td>
                    <td><a href="#">Youtube</a></td>
                </tr>
            </table>
        </td>
        <td style="margin:0;padding:0 6px;"><hr size="1" style="border-style:solid; border-color:#f8ce0c; height:220px" /></td>
        <td valign="top">
            <table style="font-size:12px; font-family:Geneva, Arial, Helvetica, sans-serif; padding-top:20px;">
                <tr>
                    <td valign="top">Address</td>
                    <td valign="top">:</td>
                    <td>Moon Trade Links (PVT) Ltd, Plot#03, Block#G, Barapole, Halishahar, Chittagong.</td>
                </tr>
                <tr>
                    <td>Cell</td>
                    <td>:</td>
                    <td>01823-079015-17</td>
                </tr>
                <tr>
                    <td>Tel</td>
                    <td>:</td>
                    <td>031-711233-35</td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td>:</td>
                    <td>moongroup4@gmail.com</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table style="color:#f8ce0c; font-size:12px;">
    <tr>
        <td colspan="3"><hr size="2" style="border-style:solid; border-color:#f8ce0c; width:1000px;" /></td>
    </tr>
    <tr>
        <td>Copyright <a href="#"></a>Moon Trade Links (PVT) Ltd. || All Rights Reserved</td>
        <td>&nbsp;</td>
        <td align="right">Design By: <a href="http://www.macrosoftwaresbd.com" target="_blank" style="text-decoration:none; color:#f8ce0c;"> Macro Softwares Limited</a></td>

    </tr>
</table>
