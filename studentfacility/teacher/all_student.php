<?php
//error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
        <head>
            <title>Moon Student Facility</title>
            <link  rel="stylesheet" type="text/css" href="css/style.css" />
            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link  rel="stylesheet" type="text/css" href="css/stylemsf.css" />
            <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <script type="text/javascript" src="js/crawler.js"></script>

            <style type="text/css">
                .gradienttable
                {
                    width: 100%; 
                    margin-top: 24px;
                }
                table.gradienttable {
                    font-family: verdana,arial,sans-serif;
                    font-size:14px;
                    color:#333333;
                    border-width: 1px;
                    border-color: #999999;
                    border-collapse: collapse;
                }
                table.gradienttable th{
                    padding: 0px;
                    background: #d5e3e4;
                    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2Q1ZTNlNCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQwJSIgc3RvcC1jb2xvcj0iI2NjZGVlMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNiM2M4Y2MiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
                    background: -moz-linear-gradient(top,  #d5e3e4 0%, #ccdee0 40%, #b3c8cc 100%);
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d5e3e4), color-stop(40%,#ccdee0), color-stop(100%,#b3c8cc));
                    background: -webkit-linear-gradient(top,  #d5e3e4 0%,#ccdee0 40%,#b3c8cc 100%);
                    background: -o-linear-gradient(top,  #d5e3e4 0%,#ccdee0 40%,#b3c8cc 100%);
                    background: -ms-linear-gradient(top,  #d5e3e4 0%,#ccdee0 40%,#b3c8cc 100%);
                    background: linear-gradient(to bottom,  #d5e3e4 0%,#ccdee0 40%,#b3c8cc 100%);
                    border: 1px solid #999999;
                }
                table.gradienttable td {
                    padding: 0px;
                    background: #ebecda;
                    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ViZWNkYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQwJSIgc3RvcC1jb2xvcj0iI2UwZTBjNiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjZWNlYjciIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
                    background: -moz-linear-gradient(top,  #ebecda 0%, #e0e0c6 40%, #ceceb7 100%);
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ebecda), color-stop(40%,#e0e0c6), color-stop(100%,#ceceb7));
                    background: -webkit-linear-gradient(top,  #ebecda 0%,#e0e0c6 40%,#ceceb7 100%);
                    background: -o-linear-gradient(top,  #ebecda 0%,#e0e0c6 40%,#ceceb7 100%);
                    background: -ms-linear-gradient(top,  #ebecda 0%,#e0e0c6 40%,#ceceb7 100%);
                    background: linear-gradient(to bottom,  #ebecda 0%,#e0e0c6 40%,#ceceb7 100%);
                    border: 1px solid #999999;
                }
                table.gradienttable th{
                    margin:0px;
                    padding:3px;
                    border-top: 1px solid #eefafc;
                    border-bottom:0px;
                    border-left: 1px solid #eefafc;
                    border-right:0px;
                }
                table.gradienttable td{
                    margin:0px;
                    padding:8px;
                    border-top: 1px solid #fcfdec;
                    border-bottom:0px;
                    border-left: 1px solid #fcfdec;;
                    border-right:1px;
                }
            </style>

        </head>
        <body>
            <div id="containermsf">           
                <div id="headermsf">
                    <? include("header.php"); ?>   
                </div>            
                <div id="navigationbarmsf">
                    <? include("menumsf.php"); ?>
                </div> 

                <div id="contentmsf"> 


                    <?php
                    // error_reporting(0);
                    //declaration
                    include("database.php");
                    if (isset($_GET["page"])) {
                        $page = $_GET["page"];
                    } else {
                        $page = 1;
                    };
                    $start_from = ($page - 1) * 2;  //2 is the no. of row per page.
                    $sql = "SELECT * FROM student_info LIMIT $start_from, 2";
                    $rs_result = mysql_query($sql);
                    ?>

                    <table class="gradienttable">
                        <tr>
                            <th colspan="9"><p>All Student Information</p></th>
                        </tr>
                        <tr>
                            <th>Student ID</th>
                            <th>Name</th>
                            <th>Present Address</th>
                            <th>Current Education</th>
                            <th>Institute</th>
                            <th>Result</th>
                            <th>Year Of Passing</th>
                            <th>Picture</th>
                            <th>Action</th>
                        </tr>
                        <?
                        while ($row = mysql_fetch_assoc($rs_result)) {
                            ?>
                            <tr>
                                <td><? echo $row['student_id']; ?></td>
                                <td><? echo $row['student_name']; ?></td>
                                <td><? echo $row['present_address']; ?></td>
                                <td><? echo $row['current_education']; ?></td>
                                <td><? echo $row['institute']; ?></td>
                                <td><? echo $row['student_result']; ?></td>
                                <td><? echo $row['yearpass']; ?></td>
                                <td><img src="../student/student_image/<? echo $row['name']; ?>" width="35" height="16" /></td>
                                <td><a href="student_details.php?stu_id=<? echo $row['stu_id']; ?>" style="text-decoration:none;">View</a></td>
                            </tr>
                            <?
                        };
                        ?>
                    </table>


                    <p align="center">
                        <?php
                        include("database.php");
                        $sql = "SELECT COUNT(stu_id) FROM student_info";
                        $rs_result = mysql_query($sql);
                        $row = mysql_fetch_row($rs_result);
                        $total_records = $row[0];
                        // echo $row[0];
                        $total_pages = ceil($total_records / 2);
                        //paging.......
                        for ($i = 1; $i <= $total_pages; $i++) {
                            echo "<a href='all_student.php?page=" . $i . "'>" . $i . "</a> ";
                        };
                        ?>
                    </p>


                    <div id="gallery">
                        <div id="sub_gallery">
                            <? include("gallery.php"); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="clear">
            </div>   


            <div id="footer">
                <div id="footer_div">
                    <? include("footer_content.php"); ?>
                </div>   
            </div>
        </body>
    </html>
    <?php
} else {
    ?>
    <script type = text/javascript>
        alert("Please Type username and password.");
    </script>
    <?
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>