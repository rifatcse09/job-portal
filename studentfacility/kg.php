<style type="text/css">
    .hovertable
    {
        margin: 25px auto;
        width: 100%;
    }
    table.hovertable {
        font-family: verdana,arial,sans-serif;
        font-size:11px;
        color:#333333;
        border-width: 1px;
        border-color: #999999;
        border-collapse: collapse;
    }
    table.hovertable th {
        background-color:#c3dde0;
        border-width: 1px;
        font-size: 18px;
        padding: 2px;
        border-style: solid;
        border-color: #a9c6c9;
    }
    table.hovertable tr {
        background-color:#d4e3e5;

    }
    table.hovertable td {
        font-size: 14px;
        padding: 8px 6px 6px 18px;
    }
</style>

<script>
    function show() {
        alert("Are You Sure? Time Formate as dd:dd AM|PM");
    }
    function kgname_check() {
        var testresults;
        var str = document.getElementById('kg');
        var filter = /^[a-zA-Z. ]{3,100}$/;
        if (filter.test(str.value))
            testresults = true;
        else {
            alert("Input a valid KG name between 3-100 characters!");
            testresults = false;
        }
        return (testresults);
    }

    function year_check() {
        var testresults;
        var str = document.getElementById('syear');
        var filter = /^(19|20)\d{2}$/;
        if (filter.test(str.value))
            testresults = true;
        else {
            alert("Input a valid Year!");
            testresults = false;
        }
        return (testresults);
    }

    function schooltime_check() {
        var testresults;
        var str = document.getElementById('inp');
        var filter = /^((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))$|^([01]\d|2[0-3])(:[0-5]\d){0,2}$/;
        if (filter.test(str.value))
            testresults = true;
        else {
            alert("Input a valid Time!");
            testresults = false;
        }
        return (testresults);
    }

//    function mobile_check() {
//        var testresults;
//        var str = document.getElementById('mobile');
//        var filter = /^[0-9]{5}[0-9]{6}$/;
//        if (filter.test(str.value))
//            testresults = true;
//        else {
//            alert("Input a valid mobile no.!");
//            testresults = false;
//        }
//        return (testresults);
//    }

//    function phone_check() {
//        var testresults;
//        var str = document.getElementById('phone');
//        var filter = /^[0-9]{3}-[0-9]{7}$/;
//        if (filter.test(str.value))
//            testresults = true;
//        else {
//            alert("Input a valid Phone no.!");
//            testresults = false;
//        }
//        return (testresults);
//    }
    
      function checkemail() {
                var testresults;
                var str = document.getElementById('email');
                var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid email address!");
                    testresults = false;
                }
                return (testresults);
            }
    
    
    function validation()
    {
        if (document.valid.kg.value == "") {
            alert("Please enter name of KG!");
            document.valid.kg.focus();
            return false;
        }
        if (kgname_check() !== true) {
            return false;
        }

        if (document.valid.syear.value == "") {
            alert("Please enter established year");
            document.valid.syear.focus();
            return false;
        }
        if (year_check() !== true) {
            return false;
        }

        if (document.valid.tname.value == "") {
            alert("Please enter number of teacher");
            document.valid.tname.focus();
            return false;
        }

        if (document.valid.stime.value == "") {
            alert("Please enter School Time");
            document.valid.stime.focus();
            return false;
        }
        if (schooltime_check() !== true) {
            return false;
        }
        if (document.valid.session.value == "Select") {
            alert("Please enter Session");
            document.valid.session.focus();
            return false;
        }
        
        if (document.valid.st.value == "") {
            alert("Please enter Starting Time");
            document.valid.st.focus();
            return false;
        }
        if (document.valid.et.value == "") {
            alert("Please enter Ending Time");
            document.valid.et.focus();
            return false;
        }

        if (document.valid.medium.value == "Select") {
            alert("Please enter Medium");
            document.valid.medium.focus();
            return false;
        }

        if (document.valid.secs.value == "Select") {
            alert("Please select Security System");
            document.valid.secs.focus();
            return false;
        }

           if (document.valid.transp.value == "Select") {
            alert("Please select Transportation");
            document.valid.transp.focus();
            return false;
        }
//        if (document.valid.mobile.value == "") {
//            alert("Please enter Mobile No.");
//            document.valid.mobile.focus();
//            return false;
//        }
//
//        if (mobile_check() !== true) {
//            return false;
//        }
//        if (document.valid.phone.value == "") {
//            alert("Please enter Phone No.");
//            document.valid.phone.focus();
//            return false;
//        }
//        if (phone_check() !== true) {
//            return false;
//        }
        
           if (document.valid.eid.value == "") {
            alert("Please enter Email address");
            document.valid.eid.focus();
            return false;
        }
        if (checkemail() !== true) {
            return false;
        }
        
              
        return true;
    }
</script>

<form action="insert/insert_kg.php" name="valid" method="POST" onsubmit="return validation()">
    <table class="hovertable">
        <h5 style="margin:0"><i>N.B: <span>*</span>fields are mandetory!</i></h5>
        <tr>
            <td><span>*</span>KG Name</td>
            <td>:</td>
            <td>
                <input type="text" name="kg" id="kg">
            </td>
            <td><span>*</span>Established Year</td>
            <td>:</td>
            <td>
                <input type="text" name="syear" id="syear">
            </td>
        </tr>
        <tr>
            <td>Name of Principal</td>
            <td>:</td>
            <td>
                <input type="text" name="pname">
            </td>
            <td><span>*</span>Number of Teacher</td>
            <td>:</td>
            <td>
                <input type="text" name="tname">
            </td>
        </tr>


        <tr>
            <td><span>*</span>School Time</td>
            <td>:</td>
            <td>
                <input type="text" name="stime" id="inp" onblur="return show();">
            </td>
            <td><span>*</span>Session</td>
            <td>:</td>
            <td>
                <select name="session">
                    <option value="Select">Select</option>
                    <option value="Morning">Morning</option>
                    <option value="Day">Day</option>
                    <option value="Morning & Day">Morning & Day</option>
                </select>

            </td>
        </tr>

        <tr>
            <td><span>*</span>Starting Time</td>
            <td>:</td>
            <td>
                <input type="text" name="st">
            </td>
            <td><span>*</span>Ending Time</td>
            <td>:</td>
            <td>
                <input type="text" name="et">
            </td>
        </tr>

        <tr>
            <td><span>*</span>Medium</td>
            <td>:</td>
            <td>
                <select name="medium">
                    <option value="Select">Select</option>
                    <option value="Bangla">Bangla</option>
                    <option value="English National Curriculum">English National Curriculum</option>
                    <option value="English British Curriculum">English British Curriculum</option>
                </select>
            </td>
            <td>Extra Curriculum Activities</td>
            <td>:</td>
            <td>
                <textarea type="text" name="xcuract">

                </textarea>
            </td>
        </tr>

        <tr>
            <td>Play Ground</td>
            <td>:</td>
            <td>
                <select name="pg">
                    <option value="">Select</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td><span>*</span>Security Systems</td>
            <td>:</td>
            <td>
                <select name="secs">
                    <option value="Select">Select</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>

        <tr>
            <td><span>*</span>Transportation</td>
            <td>:</td>
            <td>
                <select name="transp">
                    <option value="Select">Select</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>Location Details</td>
            <td>:</td>
            <td>
                <textarea name="ld">
                </textarea>
            </td>
        </tr>

        <tr>
            <td>Mobile <span style="font-size:10px; color: #000">[xxxxxxxxxxx]</span></td>
            <td>:</td>
            <td>
                <input type="text" name="mobile" id="mobile">
            </td>
            <td>Phone<span style="font-size:10px; color: #000">[xxx-xxxxxxx]</span></td>
            <td>:</td>
            <td>
                <input type="text" name="phone" id="phone">
            </td>
        </tr>
        <tr>
            <td><span>*</span>Email ID</td>
            <td>:</td>
            <td>
                <input type="text" name="eid" id="email">
            </td>
            <td>Website URL</td>
            <td>:</td>
            <td>
                <input type="text" name="weburl" id="url">
            </td>
        </tr>
        <tr>
            <td>Other Information</td>
            <td>:</td>
            <td colspan="5">
                <textarea name="oinfo">
                </textarea>
            </td>
        </tr>
    </table>
    <p style="text-align: center"> <input type="submit" name="send" value="Save"></p>

</form>
