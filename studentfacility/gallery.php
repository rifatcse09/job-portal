<!--<div class="marquee" id="mycrawler2">
<a href="#"><img src="images/animation/1.png" /></a><a href="#"><img src="images/animation/2.png" /></a> <a href="#"><img src="images/animation/3.png" /></a>
<a href="#"><img src="images/animation/4.png" /></a><a href="#"><img src="images/animation/5.png" /></a> <a href="#"><img src="images/animation/6.png" /></a>
<a href="#"><img src="images/animation/7.png"/></a><a href="#"><img src="images/animation/3.png"/></a><a href="#"><img src="images/animation/1.png"/></a>
</div>-->

<script type="text/javascript">
marqueeInit({
	uniqueid: 'mycrawler2',
	style: {
		'padding': '2px',
		'width': '980px',
		'height': '100px'
	},
	inc: 5, //speed - pixel increment for each iteration of this marquee's movement
	mouse: 'cursor driven', //mouseover behavior ('pause' 'cursor driven' or false)
	moveatleast: 2,
	neutral: 150,
	savedirection: true,
	random: true
});
</script>