<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Moon Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js"></script>
        <!--username check-->
        <script type="text/javascript" src="js/jquery.js"></script>
        <!--start date-->
        <link  rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui.css" />
        <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript">
            //ON LOADING PAGE
            $(document).ready(function() {
                $("#dob").datepicker({dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, yearRange: "-100:+0"});
            });
        </script>
        <!--end date-->
        <script type="text/javascript">

            $(document).ready(function() {
                var min_chars = 3;
                var characters_error = 'Minimum amount of chars is 3';
                var checking_html = '<img src="images/loading.gif" /> Checking...';
                //when button is clicked
                $('#check_username_availability').click(function() {
                    //run the character number check
                    if ($('#username').val().length < min_chars) {
                        //if it's bellow the minimum show characters_error text
                        $('#username_availability_result').html(characters_error);
                    } else {
                        //else show the cheking_text and run the function to check
                        $('#username_availability_result').html(checking_html);
                        check_availability();
                    }
                });

            });

            //function to check username availability	
            function check_availability() {
                //get the username
                var username = $('#username').val();
                //use ajax to run the check
                $.post("user_check/check_user_tea.php", {username: username},
                function(result) {
                    //if the result is 1'
                    if (result == 1) {
                        //show that the username is available
                        $('#username_availability_result').html('<span class="is_available"><b>' + username + '</b> is Available</span>');
                    } else {
                        //show that the username is NOT available
                        $('#username_availability_result').html('<span class="is_not_available"><b>' + username + '</b> is not Available</span>');
                    }
                });

            }

            function id_check() {
                var testresults;
                var str = document.getElementById('teacher_id');
                var filter = /^[0-9]+$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid ID!");
                    testresults = false;
                }
                return (testresults);
            }

            function name_check() {
                var testresults;
                var str = document.getElementById('tname');
                var filter = /^[a-zA-Z. ]{3,30}$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid Name between 3-30 character!");
                    testresults = false;
                }
                return (testresults);
            }

            function mobile_check() {
                var testresults;
                var str = document.getElementById('mno');
                var filter = /^[0-9]{5}[0-9]{6}$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid mobile no.!");
                    testresults = false;
                }
                return (testresults);
            }

            function checkemail() {
                var testresults;
                var str = document.getElementById('email');
                var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid email address!");
                    testresults = false;
                }
                return (testresults);
            }

            function gpa_check() {
                var testresults;
                var str = document.getElementById('gpa');
                var filter = /^(?:(?:[0-3](?:\.\d\d?)?)|4(?:\.0)?)$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("input a valid GPA!");
                    testresults = false;
                }
                return (testresults);
            }

            function year_check() {
                var testresults;
                var str = document.getElementById('yearpass');
                var filter = /^(19|20)\d{2}$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid Year!");
                    testresults = false;
                }
                return (testresults);
            }

            function image_check()
            {
                var fup = document.getElementById('userfile');
                var fileName = fup.value;
                var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
                if (ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "PNG" || ext == "png")
                {
                    return true;
                }
                else
                {
                    alert("Upload gif/jpg/png images only");
                    fup.focus();
                    return false;
                }
            }

            function validform() {
                if (document.regis.username.value == "") {
                    alert("(Red * ) symbol field is mandatory. Enter UserName.");
                    document.regis.username.focus();
                    return  false;
                }

                if (document.regis.password.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Passwrord.");
                    document.regis.password.focus();
                    return  false;
                }
                if (document.regis.tea_id.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter ID.");
                    document.regis.tea_id.focus();
                    return false;
                }
                if (id_check() !== true) {
                    return false;
                }

                if (document.regis.tea_name.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Name.");
                    document.regis.tea_name.focus();
                    return false;
                }

                if (name_check() !== true) {
                    return false;
                }

                if (document.regis.gender.value == "Select Gender.....") {
                    alert("(Red * ) symbol field is mandatory.Enter gender.");
                    document.regis.gender.focus();
                    return false;
                }


                if (document.regis.present_address.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Present address.");
                    document.regis.present_address.focus();
                    return false;
                }


                if (document.regis.mobile_no.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Mobile no.");
                    document.regis.mobile_no.focus();
                    return false;
                }
                if (mobile_check() !== true) {
                    return false;
                }

                if (document.regis.email.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter email address.");
                    document.regis.email.focus();
                    return false;
                }
                if (checkemail() !== true) {
                    return false;
                }

                if (document.regis.userfile.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter an image.");
                    document.regis.userfile.focus();
                    return false;
                }
                if (image_check() !== true) {
                    return false;
                }


                if (document.regis.last_graduation.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter last graduation.");
                    document.regis.last_graduation.focus();
                    return false;
                }

                if (document.regis.institute.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Institute.");
                    document.regis.institute.focus();
                    return false;
                }

                if (document.regis.tea_result.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter GPA.");
                    document.regis.tea_result.focus();
                    return false;
                }

                if (gpa_check() !== true) {
                    return false;
                }

                if (document.regis.yearpass.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Passing year");
                    document.regis.yearpass.focus();
                    return false;
                }

                if (year_check() !== true) {
                    return false;
                }

                if (document.regis.teaching_exp.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Teaching experience.");
                    document.regis.teaching_exp.focus();
                    return false;
                }

                if (document.regis.teaching_subject.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Teaching subject.");
                    document.regis.teaching_subject.focus();
                    return false;
                }
                if (document.regis.location.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Tution location.");
                    document.regis.location.focus();
                    return false;
                }
                if (document.regis.area.value == "") {
                    alert("(Red * ) symbol field is mandatory.Enter Tution area.");
                    document.regis.area.focus();
                    return false;
                }
                return true;
            }


        </script>

        <script>
            $(document).ready(function() {
                $("#biog").dialog({
                    autoOpen: false,
                    height: 230,
                    width: 500,
                    modal: true,
                    title: "More Teacher info Panel",
                    position: ["center", "middle"],
                    autoresize: false
                });
            });

            function insert_more() {
                $.ajax({
                    url: "more_teacher.php",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    success: function(res) {
                        if (res.redirect) {
                            alert("sorry, something is wrong. May be you are not authenticated.Page will go to the home after clicking ok.")
                            window.location.href = res.redirect;
                            return;
                        }
                        $("#biog").html(res);

                    }
                });
                $("#biog").dialog("open");

            }
        </script>

        <style type="text/css">
            .hovertable
            {
                float: left;
                margin-top: 25px;
                /*width: 100%;*/
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                /*border-width: 1px;*/
                font-size: 14px;
                padding:2px;
                /*border-style: solid;*/
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;

            }
            table.hovertable td {
                /*border-width: 1px;*/
                font-size: 13px;
                padding: 8px;
                /*                border-style: solid;*/
                border-color: #a9c6c9;
            }
        </style>

        <script type="text/javascript">
            var add_ins = '';
            var add_xpyear = '';
            var p = 1;
            function add() {
                add_ins =add_ins +'<input id="'+p+'ins'+'" type="text" name="'+p+'ins'+'" />';
                add_xpyear =add_xpyear+'<input id="'+p+'xp'+'" type="text" name="'+p+'xp'+'" />';
                document.getElementById("ins").innerHTML = add_ins;
                document.getElementById("year").innerHTML = add_xpyear;
                document.getElementById("no_ins").value =p;
                ++p;
            }
        </script>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header_student.php"); ?>   
            </div> 
            <div id="navigationbar">
                <? include ("searchmenu.php"); ?>
            </div>
            <div id="content"> 
                <div id="left_content_st">
                    <form action="insert/teacher_insert.php" name="regis" enctype="multipart/form-data" method="post" onsubmit="return validform()" style="float: left; width: 70%;height: 40%;padding-left: 5px;margin-top: 20px;">
                        <!--<p>Welcome to Teacher Registration Panel</p>-->
                        <table class="hovertable">
                            <tr>
                                <th colspan="9" style="font-size:20px ;font-weight: bold; background:  #D9E021;">Welcome to Teacher Registration Panel</th>
                            </tr>
                            <tr>
                                <th colspan="9">Login Information</th>
                            </tr>
                            <tr>
                                <td><span>*</span>User Name</td>
                                <td>:</td>
                                <td  colspan="7">
                                    <input type="hidden" name="user_type" value="teacher" readonly="teacher" />
                                    <input type="text" name="username" id="username" /><div id='username_availability_result'></div>
                                    <input type='button' id='check_username_availability' value='Check Availability' />

                                </td>
                            </tr>
                            <tr>
                                <td><span>*</span>Password</td>
                                <td>:</td>
                                <td  colspan="7"><input type="password" name="password" /></td>
                            </tr>
                            <tr>
                                <th colspan="9">Personal Information</th>
                            </tr>
                            <tr>
                                <td><span>*</span>Teacher ID</td>
                                <td>:</td>
                                <td><input type="text" name="tea_id" id="teacher_id" /></td>
                                <td colspan="2"><span>*</span>Teacher Name</td>
                                <td>:</td>
                                <td colspan="3"><input type="text" name="tea_name" id="tname" /></td>
                            </tr>
                            <tr>
                                <td><span>*</span>Gender</td>
                                <td>:</td>
                                <td><select name="gender">
                                        <option>Select Gender.....</option>
                                        <option value="Male">Male</option> 
                                        <option value="Female">Female</option>
                                    </select>
                                </td>
                                <td colspan="2">Date Of Birth</td>
                                <td>:</td>
                                <td colspan="3"><input type="text" name="dob" id="dob"/></td>
                            </tr>
                            <tr>
                                <td><span>*</span>Present Address</td>
                                <td>:</td>
                                <td><textarea name="present_address"></textarea></td>
                                <td colspan="2">Permanent Address</td>
                                <td>:</td>
                                <td colspan="3"><textarea name="per_address"></textarea></td>
                            </tr>
                            <tr>
                                <td>Blood Group</td>
                                <td>:</td>
                                <td><select name="blood">
                                        <option>Select Blood Group....</option>
                                        <option>A+</option>
                                        <option>B+</option>
                                        <option>AB+</option>
                                        <option>O+</option>
                                        <option>A-</option>
                                        <option>B-</option>
                                        <option>AB-</option>
                                        <option>O-</option>
                                    </select>
                                </td>
                                <td colspan="2"><span>*</span>Mobile No</td>
                                <td>:</td>
                                <td  colspan="3"><input type="text" name="mobile_no" id="mno"/></td>
                            </tr>
                            <tr>
                                <td><span>*</span>E-mail</td>
                                <td>:</td>
                                <td><input type="text" name="email" id="email"/></td>
                                <td colspan="2"><span>*</span>Picture :</td>
                            <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
                            <td  colspan="3"><input type="file" name="userfile" id="userfile" style="margin-left: -70px;float: left;"></td>
                            </tr>
                            <tr>
                                <th colspan="9">Carrer Objective</th>
                            </tr>
                            <tr>
                                <td>Carrer Objective</td>
                                <td>:</td>
                                <td colspan="7"><textarea name="carrer_obj" cols="60"></textarea></td>
                            </tr>
                            <tr>
                                <th colspan="9">Academic Information</th>
                            </tr>
                            <tr>
                                <td><span>*</span>Last Graduation</td>
                                <td>:</td>
                                <td><input type="text" name="last_graduation" /></td>
                                <td colspan="2"><span>*</span>Institute Name</td>
                                <td>:</td>
                                <td  colspan="3"><input type="text" name="institute" /></td>
                            </tr>
                            <tr>
                                <td>Concentrations<td>
                                <td style="float:left; margin-left: -13px;">:</td>
                                <td colspan="5">
                                    <textarea name="concent" style="float:left; margin-left: -170px"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><span>*</span>Additional Training</td>
                                <td>:</td>
                                <td><input type="text" name="adtrain" /></td>
                                <td></td>
                                <td><span>*</span>Institute Name</td>
                                <td>:</td>
                                <td  colspan="3" ><input type="text" name="adstitute" /></td>
                            </tr>
<!--                            <tr>
                                <td><span>*</span>Current GPA</td>
                                <td>:</td>
                                <td><input type="text" name="tea_result" id="gpa" /></td>
                                <td></td>
                                <td><span>*</span>Year Of Passing</td>
                                <td>:</td>
                                <td><input type="text" name="yearpass" id="yearpass" /></td>
                            </tr>-->
                            <tr>
                                <th colspan="9">Teaching Experience</th>
                            </tr>
<!--                            <tr>
                                <td><span>*</span>Teaching Experience </td>
                                <td>:</td>
                                <td colspan="5"><textarea name="teaching_exp" cols="60"></textarea></td>
                            </tr>-->

                            <tr>
                                <td><span>*</span>Institution</td>
                                <td>:</td>
                                <!--<td><input type="text" name="teachins" /><div id="ins"></div></td>-->
                                <td>
                                    <input type="text" name="0ins" />
                                    <div id="ins"></div>
                                </td>
                                <td></td>
                                <td><span>*</span>Year</td>
                                <td>:</td>
                                <td  colspan="3">
                                    <input type="text" name="0xp" />
                                    <div id="year"></div>
                                </td>
                                <td><input style="width:110px; font-size: 11px; background: #ffffff" type="button" value="More Institute/Exp.year" onclick="add()" /></td>
                            <input type="hidden" value="0" id="no_ins" name="no_ins">
                            </tr>
                            <tr>
                                <td><span>*</span>Medium</td>
                                <td>:</td>
<!--                                <td><input type="text" name="last_graduation" /></td>-->
                                <td>
                                    <select name="medium">
                                        <option>Select...</option>
                                        <option value="Bengali">Bengali</option>
                                        <option value="English">English</option>
                                        <option value="Arabic">Arabic</option>
                                    </select>
                                </td>
                                <td></td>
                                <td><span>*</span>Designation</td>
                                <td>:</td>
                                <td><input type="text" name="desig" /></td>
                            </tr>
<!--                            <tr>
                                <td colspan="9">
                                    <p style="margin-left:165px;"><input type="button" name="admore" value="Add more"></p>
                                </td>
                            </tr>-->
                            <tr>
                                <th colspan="9">Teaching Subject & Level</th>
                            </tr>
<!--                            <tr>
                                <td><span>*</span>Teaching Subject </td>
                                <td>:</td>
                                <td colspan="5"><input type="text" name="teaching_subject" size="60">(eg: Bangla, English etc.)</td>
                            </tr>-->

                            <tr>
                                <td><span>*</span>Teaching Subject </td>
                                <td>:</td>
                                <td>
                                    <select name="teaching_subject" cols="60">
                                        <option>Select ...</option>
                                        <optgroup label="Academic Subjects">
                                            <option value="Bangla">Bangla</option>
                                            <option value="English">English</option>
                                            <option value="Mathematics">Mathematics</option>
                                            <option value="Physics">Physics</option>
                                            <option value="Chemistry">Chemistry</option>
                                            <option value="Biology">Biology</option>
                                            <option value="Accounting">Accounting</option>
                                            <option value="Management Studies">Management Studies</option>
                                            <option value="Economics">Economics</option>
                                            <option value="Computer Learning">Computer Learning</option>
                                        </optgroup>

                                        <optgroup label="IT Subjects">
                                            <option value="C/C++">C/C++</option>
                                            <option value="JAVA">JAVA</option>
                                            <option value="C#(Sharp)">C#(Sharp)</option>
                                            <option value="PHP & MYSQL">PHP & MYSQL</option>
                                            <option value="ASP.NET">ASP.NET</option>
                                            <option value="HTML, CSS & javascript">HTML, CSS & javascript</option>
                                            <option value="Managing Information technology">Managing Information technology</option>
                                            <option value="E-Commerce">E-Commerce</option>
                                            <option value="Web Applications">Web Applications</option>
                                            <option value="Computer Applications">Computer Applications</option>
                                            <option value="Computer Networking">Computer Networking</option>
                                            <option value="Internet Protocols">Internet Protocols</option>
                                            <option value="Algebra and Discrete Mathematics">Algebra and Discrete Mathematics</option>
                                            <option value="Fundamentals of Computer and Programming">Fundamentals of Computer and Programming</option>
                                            <option value="Computer Programming and Problem Solving">Computer Programming and Problem Solving</option>
                                            <option value="Computer Problem Solving for IT">Computer Problem Solving for IT</option>
                                            <option value="Business Solutions in IT"> Business Solutions in IT</option>
                                            <option value="Machine and Assembly Language programming">Machine and Assembly Language programming</option>
                                            <option value="UNIX Operating Systems">UNIX Operating Systems</option>
                                            <option value="Data Structures and Algorithm Design">Data Structures and Algorithm Design</option>
                                            <option value="Database Management and Administration">Database Management and Administration</option>
                                            <option value="Computer Organization">Computer Organization</option>
                                            <option value="Parallel Computer Organization">Parallel Computer Organization</option>
                                            <option value="Advance Computer Organization">Advance Computer Organization</option>
                                            <option value="Information Systems and Productivity Toolware">Information Systems and Productivity Toolware</option>
                                            <option value="File Structures and Management">File Structures and Management</option>
                                            <option value="Multimedia Information Systems">Multimedia Information Systems</option>
                                            <option value="Introduction to Artificial Intelligence">Introduction to Artificial Intelligence</option>
                                            <option value="Programming Language Concepts">Programming Language Concepts</option>
                                            <option value="Object-Oriented Software Systems Design">Object-Oriented Software Systems Design</option>
                                            <option value="System Design and Configuration">System Design and Configuration</option>
                                            <option value="IT Project Management">IT Project Management</option>
                                        </optgroup>

                                        <optgroup label="Art Subjects">
                                            <option value="Art & Design">Art & Design</option>
                                            <option value="Dance">Dance</option>
                                            <option value="Drama">Drama</option>
                                            <option value="Fine Art">Fine Art</option>
                                        </optgroup>

                                        <optgroup label="Language Subjects">
                                            <option value="Bengali">Bengali</option>
                                            <option value="English">English</option>
                                            <option value="Arabic">Arabic</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Chinese">Chinese</option>
                                            <option value="French">French</option>
                                            <option value="German">German</option>
                                            <option value="Spanish">Spanish</option>
                                        </optgroup>

                                        <optgroup label="Music Subjects">
                                            <option value="Music Production">Music Production</option>
                                            <option value="Song Writing">Song Writing</option>
                                            <option value="Guitar">Guitar</option>
                                            <option value="Drums">Drums</option>
                                            <option value="Bass">Bass</option>
                                            <option value="voice">voice</option>
                                            <option value="Music education">Music education</option>
                                            <option value="Music Business">Music Business</option>
                                            <option value="Music for film, Games, & TV">Music for film, Games, & TV</option>
                                        </optgroup>

                                        <optgroup label="Health Subjects">
                                            <option value="Allied Health">Allied Health</option>
                                            <option value="Alternative Medicine">Alternative Medicine</option>
                                            <option value="Anesthesia Technician">Anesthesia Technician</option>
                                            <option value="Bioinformatics">Bioinformatics</option>
                                            <option value="Dental Assistant">Dental Assistant</option>
                                            <option value="Healthcare Information Systems">Healthcare Information Systems</option>
                                            <option value="Healthcare Management">Healthcare Management</option>
                                            <option value="Nursing">Nursing</option>
                                            <option value="Nutrition">Nutrition</option>
                                            <option value="Physical Therapy">Physical Therapy</option>
                                        </optgroup>

                                    </select>
                                </td>
                                <td><span>*</span>Level</td>
                                <td>:</td>
                                <td  colspan="3">
                                    <select type="text" name="level">
                                        <option>Select</option>
                                        <option value="Primary">Primary</option>
                                        <option value="Secondary">Secondary</option>
                                        <option value="Higher Secondary">Higher Secondary</option>
                                        <option value="O Level">O Level</option>
                                        <option value="A Level">A Level</option>
                                        <option value="Pass & Hons">Pass & Hons</option>
                                        <option value="Post Graduation">Post Graduation</option>
                                        <option value="Addmission Test Preparation">Addmission Test Preparation</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <th colspan="7">Teaching Location</th>
                            </tr>
<!--                            <tr>
                                <td><span>*</span>Tution Location</td>
                                <td>:</td>
                                <td colspan="3"><input type="text" name="location" size="36">(eg: Dhaka,ctg etc.)</td>
                            </tr>-->
                            <tr>
                                <td><span>*</span>Tution Location</td>
                                <td>:</td>
                                <td>
                                    <select name="location">
                                        <option>Select...</option>
                                        <option value="Dhaka">Dhaka</option>
                                        <option value="Chittagong">Chittagong</option>
                                        <option value="Rajshahi">Rajshahi</option>
                                        <option value="Sylhet">Sylhet</option>
                                        <option value="Barisal">Barisal</option>
                                        <option value="Khulna">Khulna</option>
                                        <option value="Rangpur">Rangpur</option>
                                    </select>
                                </td>
                                <td><span>*</span>Area </td>
                                <td>:</td>
                                <td colspan="3"><input type="text" name="area" size="36"></td>
                            </tr>

                        </table>
                        <p><input name="upload" type="submit" value="Register" id="upload"></p>
                        <!--<p style="float:right;margin-top:-44px;font-size: 13px;padding: 2px;background: #c3dde0; margin-right: 15%; border: 1px #cccccc solid; border-radius: 4px;"><a href="javascript:insert_more()" style="text-decoration: none;">Add More?</a></p>-->
                    </form>
                    <div id="biog"></div>
<!--                    <div id="gallery">
                        <div id="sub_gallery">
                            <? //include("gallery.php"); ?>
                        </div>
                    </div>-->
                </div>

                <div id="right_content"  style="margin-top:10px">
                    <div id="tutrsrch">
                        <div id="tutrsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Tutor Search</p>
                        </div>
                        <form action="teachersearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Subject</td>
                                    <td>
                                        <select name="teaching_subject">
                                            <option selected="selected">Select subject</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT teaching_subject FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ts = $row['teaching_subject'];
                                                echo '<option value="' . $ts . '">' . $ts . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="stdsrch">
                        <div id="stdsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Student Search</p>
                        </div>
                        <form action="stdsearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM student_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

<!--                    <div id="TV">
                        <img src="images/tv.png">
                    </div>-->
                </div>

            </div>
        </div>

        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>