
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Moon Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js"></script>
        <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <link  rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui.css" />

        <script type="text/javascript">
            var tagadd1 = '';
            var tagadd2 = '';
            var v = 1;
            function add() {
                tagadd1 = tagadd1 + v + '<input id="' + v + 'f' + '" type="text" name="' + v + 'f' + '" />';
                tagadd2 = tagadd2 + v + '<input id="' + v + 'd' + '" type="text" name="' + v + 'd' + '" />';
                document.getElementById("fname").innerHTML = tagadd1;
                document.getElementById("din").innerHTML = tagadd2;
                document.getElementById("no_din").value = v;
                v = v + 1;
            }

        </script>

        <script>
            $(document).ready(function() {
                $("#biog").dialog({
                    autoOpen: false,
                    height: auto,
                    width: 600,
                    modal: true,
                    title: "More  university Details Panel",
                    position: ["center", "middle"],
                    autoresize: false
                });
            });


            function insert_more() {
                $.ajax({
                    url: "more.php",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    success: function(res) {
                        if (res.redirect) {
                            alert("sorry, something is wrong. May be you are not authenticated.Page will go to the home after clicking ok.")
                            window.location.href = res.redirect;
                            return;
                        }
                        $("#biog").html(res);

                    }
                });
                $("#biog").dialog("open");

            }
        </script>
        <style type="text/css">
            .hovertable
            {
                float: left;
                width: 105%;
                margin-top: 25px;
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 14px;
                padding: 8px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;

            }
            table.hovertable td {
                /*border-width: 1px;*/
                font-size: 12px;
                padding: 4px;
                /*border-style: solid;*/
                border-color: #a9c6c9;
            }
        </style>

        <script>

            function mob() {
                alert("Mobile No. Format: xxxxxxxxxxx");
            }
            function phon() {
                alert("Phone No. Format as xxx-xxxxxxx");
            }
            function show() {
                alert("Are You Sure? Time Format as dd:dd AM|PM");
            }
            function year_check() {
                var testresults;
                var str = document.getElementById('est');
                var filter = /^(19|20)\d{2}$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid Year!");
                    testresults = false;
                }
                return (testresults);
            }

            function time_check() {
                var testresults;
                var str = document.getElementById('time');
                var filter = /^((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))$|^([01]\d|2[0-3])(:[0-5]\d){0,2}$/;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid Time!");
                    testresults = false;
                }
                return (testresults);
            }

//            function mobile_check() {
//                var testresults;
//                var str = document.getElementById('mobile');
//                var filter = /^[0-9]{5}[0-9]{6}$/;
//                if (filter.test(str.value))
//                    testresults = true;
//                else {
//                    alert("Input a valid mobile no.!");
//                    testresults = false;
//                }
//                return (testresults);
//            }
//
//            function phone_check() {
//                var testresults;
//                var str = document.getElementById('phone');
//                var filter = /^[0-9]{3}-[0-9]{7}$/;
//                if (filter.test(str.value))
//                    testresults = true;
//                else {
//                    alert("Input a valid Phone no.!");
//                    testresults = false;
//                }
//                return (testresults);
//            }

//            function check_weburl() {
//                var testresults;
//                var str = document.getElementById('url');
//                var filter = /((ftp|http|https):\/\/)*(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
//                if (filter.test(str.value))
//                    testresults = true;
//                else {
//                    alert("Input a valid Web URL!");
//                    testresults = false;
//                }
//                return (testresults);
//            }


            function checkemail() {
                var testresults;
                var str = document.getElementById('email');
                var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                if (filter.test(str.value))
                    testresults = true;
                else {
                    alert("Input a valid email address!");
                    testresults = false;
                }
                return (testresults);
            }

            function validating()
            {
                if (document.valid.univ_type.value == "Select")
                {
                    alert("Please Select University Type!");
                    document.valid.univ_type.focus();
                    return  false;
                }

                if (document.valid.name.value == "")
                {
                    alert("Please enter University Name!");
                    document.valid.name.focus();
                    return  false;
                }

                if (document.valid.est_year.value == "")
                {
                    alert("Please enter Eslablished year!");
                    document.valid.est_year.focus();
                    return  false;
                }
                if (year_check() !== true)
                {
                    return false;
                }

                if (document.valid.session.value == "Select")
                {
                    alert("Please select Session!");
                    document.valid.session.focus();
                    return  false;
                }

                if (document.valid.srtt.value == "")
                {
                    alert("Please enter Starting time!");
                    document.valid.srtt.focus();
                    return  false;
                }

                if (time_check() !== true)
                {
                    return false;
                }
                if (document.valid.endt.value == "")
                {
                    alert("Please enter Ending time!");
                    document.valid.endt.focus();
                    return  false;
                }

                if (document.valid.adt.value == "")
                {
                    alert("Please enter Admission time!");
                    document.valid.adt.focus();
                    return  false;
                }

                if (document.valid.sect.value == "Select")
                {
                    alert("Please select Security type!");
                    document.valid.sect.focus();
                    return  false;
                }

                if (document.valid.transp.value == "Select")
                {
                    alert("Please Select Transportation type!");
                    document.valid.transp.focus();
                    return  false;
                }

                if (document.valid.hostel.value == "Select")
                {
                    alert("Please Select Hostel Facility type!");
                    document.valid.hostel.focus();
                    return  false;
                }

//                if (document.valid.mobile.value == "")
//                {
//                    alert("Please enter Mobile No.!");
//                    document.valid.mobile.focus();
//                    return  false;
//                }
//
//                if (mobile_check() !== true)
//                {
//                    return false;
//                }

//                if (document.valid.phone.value == "")
//                {
//                    alert("Please enter Phone!");
//                    document.valid.phone.focus();
//                    return  false;
//                }

//                if (phone_check() !== true)
//                {
//                    return false;
//                }

                if (document.valid.eid.value == "")
                {
                    alert("Please enter Email Address!");
                    document.valid.eid.focus();
                    return  false;
                }
                if (checkemail() !== true)
                {
                    return false;
                }

//                if (document.valid.weburl.value == "")
//                {
//                    alert("Please enter Web URL!");
//                    document.valid.weburl.focus();
//                    return  false;
//                }
//                if (check_weburl() !== true)
//                {
//                    return false;
//                }

                return true;
            }
        </script>

    </head>
    <body>

        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div>            
            <div id="content"> 
                <div id="left_content_univ" style="float: left;width: 70%; margin-top: -24px;">
                    <form action="insert/insert_univ.php" name="valid" method="POST" onsubmit="return validating()">
                        <table class="hovertable">
                            <h5 style="margin-bottom: -15px;"><i>N.B: <span>*</span> fields are mandetory!</i></h5>
                            <tr>
                                <th colspan="8">University Details</th>
                            </tr>
                            <tr>
                                <td><span>*</span>University Type</td>
                                <td>:</td>
                                <td>
                                    <select name="univ_type">
                                        <option value="Select">Select</option>
                                        <option value="Public">Public</option>
                                        <option value="Private">Private</option>
                                    </select>
                                </td>
                                <td><span>*</span>Name</td>
                                <td>:</td>
                                <td colspan="3">
                                    <input type="text" name="name">
                                </td>
                            </tr>

                            <tr>
                                <td><span>*</span>Established Year</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="est_year" id="est">
                                </td>
                                <td>Name of VC</td>
                                <td>:</td>
                                <td colspan="2">
                                    <input type="text" name="vcn">
                                </td>
                            </tr>

                            <tr>
                                <td>Name of Faculty</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="0f">
                                    <div id="fname"></div>
                                </td>
                                <td>DIN</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="0d">
                                    <div id="din"></div>
                                </td>
                                <td><input style="width:110px; font-size: 11px; background: #ffffff" type="button" value="More Faculty/DIN" onclick="add()" /></td>
                            <input type="hidden" value="0" id="no_din" name="no_din">
                            </tr>
                            <tr>
                                <td><span>*</span>Session</td>
                                <td>:</td>
                                <td colspan="6">
                                    <select name="session">
                                        <option value="Select">Select</option>
                                        <option value="Morning">Morning</option>
                                        <option value="Day">Day</option>
                                        <option value="Morning & Day">Morning & Day</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><span>*</span>Starting Time</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="srtt" id="time" onblur="show()">
                                </td>
                                <td><span>*</span>Ending Time</td>
                                <td>:</td>
                                <td colspan="2">
                                    <input type="text" name="endt">
                                </td>
                            </tr>

                            <tr>
                                <td>No. of Campus</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="no_campus">
                                </td>
                                <td style="width:100%"><span>*</span>Admission Time</td>
                                <td>:</td>
                                <td colspan="2">
                                    <input type="text" name="adt"> 
                                </td>
                            </tr>

                            <tr>
                                <td><span>*</span>Security system</td>
                                <td>:</td>
                                <td>
                                    <select name="sect">
                                        <option value="Select">Select</option>
                                        <option value="yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </td>
                                <td><span>*</span>Transportation</td>
                                <td>:</td>
                                <td colspan="3">
                                    <select name="transp">
                                        <option value="Select">Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td><span>*</span>Hostel Facility</td>
                                <td>:</td>
                                <td>
                                    <select name="hostel">
                                        <option value="Select">Select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </td>
                                <td>Classroom Facility</td>
                                <td>:</td>
                                <td colspan="2">
                                    <input type="text" name="clssroom">
                                </td>
                            </tr>

                            <tr>
                                <td>Location Details</td>
                                <td>:</td>
                                <td colspan="5">
                                    <textarea name="locd">
                                    </textarea>

                                </td>
                            </tr>

                            <tr>
                                <td>Mobile No.</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="mobile" id="mobile" onclick="mob()">
                                </td>
                                <td>Phone No.</td>
                                <td>:</td>
                                <td  colspan="2">
                                    <input type="text" name="phone" id="phone" onclick="phon()">
                                </td>
                            </tr>
                            <tr>
                                <td><span>*</span>Email ID</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="eid" id="email">
                                </td>
                                <td>Website URL</td>
                                <td>:</td>
                                <td  colspan="2">
                                    <input type="text" name="weburl" id="url">
                                </td>
                            </tr>

                            <tr>
                                <td style="width:58%">Other Information</td>
                                <td>:</td>
                                <td colspan="5">
                                    <textarea type="text" name="oinfo">
                                    </textarea>
                                </td>
                            </tr>
                        </table>
                        <p style="text-align: center"> <input type="submit" name="send" value="Save"></p>
                        <!--<p style="float:right;margin-top:-40px;font-size: 13px;padding: 2px;background: #c3dde0; margin-right: 30%; border: 1px #cccccc solid; border-radius: 4px;"><a href="javascript:insert_more()" style="text-decoration: none;">Add More?</a></p>-->
                    </form>
                </div>
                <div id="biog">
                </div>
                <div id="right_content" style="margin-top:25px;">
                    <div id="tutrsrch">
                        <div id="tutrsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Tutor Search</p>
                        </div>
                        <form action="teachersearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
<!--                                    <td>Subject</td>
                                    <td><input  type="text" size="20px" name="teaching_subject"></td>-->
                                    <td>Subject</td>
                                    <td>
                                        <select name="teaching_subject">
                                            <option selected="selected">Select subject</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT teaching_subject FROM teacher_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ts = $row['teaching_subject'];
                                                echo '<option value="' . $ts . '">' . $ts . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="stdsrch">
                        <div id="stdsrcheader">
                            <p style=" font-size: 20px; font-family: 'denmark regular'; text-align: center;margin-top: 8px;">Student Search</p>
                        </div>
                        <form action="stdsearch.php" method="GET">
                            <table>
                                <tr>
                                    <td>Location</td>
                                    <td>
                                        <select name="location">
                                            <option value="">Select Location</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT District_Name FROM district");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $dis = $row['District_Name'];
                                                echo '<option value="' . $dis . '">' . $dis . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Institute</td>
                                    <td><select name="institute">
                                            <option selected="selected">Select Institute</option>
                                            <?php
                                            include('database.php');
                                            $sql = mysql_query("SELECT DISTINCT institute FROM student_info WHERE is_active=1");
                                            while ($row = mysql_fetch_array($sql)) {
                                                $ins = $row['institute'];
                                                echo '<option value="' . $ins . '">' . $ins . '</option>';
                                            }
                                            ?>
                                        </select> 
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>

                            </table>
                            <p><input type="submit" name="submit" value="Search"></p>
                        </form>
                    </div>

                    <div id="TV">
                        <!--<img src="images/tv.png">-->
                        <blink><p style="text-align: center; margin-left: 15px;margin-top: 40px; font-size: 17px; "><span style="color: #42453D;">Please Contact for Advertisement on size: 250X200 px</span><strong><br>Mobile :01755666619</strong></p></blink>
                    </div>
                </div>

            </div>
        </div>


        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>