<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Moon Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>
    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div>            
            <div id="navigationbar">
                <? include("menu.php"); ?>
            </div> 

            <div id="content"> 
                <div id="left_content">
                    <div id="lftyellowdv">
                        <div id="stimage_text">
                            <div id="stimage">
                            </div>
                            <div id="stimage_hdr">
                                <p> FOR STUDENTS</p>
                                <p style=" float: left;margin:0px; color: #D9E021;">Search Thousands of Tutors</p>
                                <hr style="float:left; width:100%; height:2px; background-color: #CCCCCC;"></hr>
                            </div>
                            <div id="stimage_content">
                                <p style="float: left;margin-top: 0px;">Find a tutor in your area Contact and arrange Lessons with<br>
                                    tutors <br>
                                    Provide and read tutor feedback
                                </p>
                            </div>
                            <div id="joinnow">
                                <a href="#"><p>Join Now</p></a>
                            </div>
                        </div>
                        <div id="tutrimage_text">
                            <div id="tutrimage">
                            </div>
                            <div id="tutrimage_hdr">
                                <p style="float:left; margin: 10px 0px 0px 0px;">FOR TUTORS</p>
                                <p style=" float: left;margin:0px;color: #D9E021;">Let students Find you</p>
                                <hr style="float:left; width:100%; height:2px; background-color: #CCCCCC;"></hr>
                            </div>
                            <div id="tutrimage_content">
                                <p style="float: left;margin-top: 0px;">Create your own tutor profile for<br> 
                                    free <br>
                                    Allow Students to find you by <br>
                                    postcode<br>
                                    List all your subjects and set your<br>
                                    own prices.
                                </p>
                            </div>
                            <div id="joinnow">
                                <a href="#"><p>Join Now </p></a>
                            </div>
                        </div>
                    </div> 

                    <div id="image_table_text">
                        <div id="image_table">
                            <div id="yellowdiv">
                            </div>
                            <div id="reddiv">
                                <p>Moon Tutor Information</p>
                            </div>
                            <div id="tablee">
                                <table>
                                    <tr><td><a href="@">Mission</a></td></tr>
                                    <tr><td><a href="@">Vision</a></td></tr>
                                    <tr><td><a href="@">Objectives</a></td></tr>
                                    <tr><td><a href="@">Destinations</a></td></tr>
                                    <tr><td><a href="@">Motto</a></td></tr>
                                    <tr><td><a href="@">Tutor's Profile</a></td></tr>
                                    <tr><td><a href="@">Student's Profile</a></td></tr>
                                    <tr><td><a href="@">Our service</a></td></tr>
                                </table>
                            </div> 
                        </div>

                        <div id="text_header">
                            <h3>Moon Tutor Information:</h3>
                            <p>
                                <b style="font-size: 25px; font-family: century gothic;">I</b>n a globalized world people are more updated for every information. They are smart knowledgeable person. So to match with the smart people you must gather more knowledge. That's why education is more important for a person. When you became educated man/women you will be more knowledgeable, smart and updated.
                            </p>
                        </div>
                    </div>
                </div>

                <div id="right_content">
                    <div id="tutrsrch">
                        <div id="tutrsrcheader">
                            <p>Tutor Search</p>
                        </div>
                        <div id="form">
                            <table style="float: left;">
                                <tr>
                                    <td>Subject </td>
                                    <td><input type="text" name="khan" />
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input type="text" name="ali" />
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Postcode </td>
                                    <td><input type="text" size="20px" name="ershad"></td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div id="adinfo">
                        <div id="adinfoheader">
                            <p>Admission Information</p>
                        </div>
                        <div id="form">
                            <table style="float: left;">
                                <tr>
                                    <td style="color: red;">Type </td>
                                    <td><input type="text" name="khan" />
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input type="text" name="ali" />
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: red;">Postcode </td>
                                    <td><input type="text" size="20px" name="ershad"></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div id="resultinfo">
                        <div id="resultheader">
                            <p>Result Information</p>
                        </div>
                        <div id="form">
                            <table style="float: left;">
                                <tr>
                                    <td style="color: red;">Type </td>
                                    <td><input type="text" name="khan" />
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input type="text" name="ali" />
                                        <img src="images/arrow.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: red;">Postcode </td>
                                    <td><input type="text" size="20px" name="ershad"></td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="gallery">
                    <div id="mymoon_sub_gallery">
                        <? include("gallery.php"); ?>
                    </div>
                </div>
            </div>
        </div>


        <div id="clear">
        </div>   


        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>