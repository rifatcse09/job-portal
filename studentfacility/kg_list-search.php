<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>PentaGems Student Facility</title>
        <link  rel="stylesheet" type="text/css" href="css/style.css" />
        <link  rel="stylesheet" type="text/css" href="css/searchstyle.css" />
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquerycssmenu.js"></script>
        <script type="text/javascript" src="js/crawler.js">
        </script>

        <style type="text/css">
            .hovertable
            {
                margin: 25px auto;
            }
            table.hovertable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-width: 1px;
                border-color: #999999;
                border-collapse: collapse;
            }
            table.hovertable th {
                background-color:#c3dde0;
                border-width: 1px;
                font-size: 18px;
                padding: 2px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            table.hovertable tr {
                background-color:#d4e3e5;
            }
            table.hovertable td {
                font-size: 14px;
                padding: 8px 6px 6px 18px;
                background: #ceceb7;
            }
        </style>

    </head>
    <body>
        <div id="container">           
            <div id="header">
                <? include("header.php"); ?>   
            </div>
            <div id="navigationbar">
                <? include ("searchmenu.php"); ?>
            </div>
            <div id="content"> 

                <table class="hovertable">
                    <tr>
                        <th colspan="6">KG Information Details</th>
                    </tr>
                    <?
                    include ("database.php");
                    $id = $_GET['id'];
                    $sql = "SELECT * FROM kg_list WHERE is_active=1 AND id='$id'";
                    $result = mysql_query($sql);
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td colspan="6">
                        </tr>
                        <tr>
                            <td>KG Name</td>
                            <td>:</td>
                            <td> <? echo $row['name'].'. '; ?></td>
                            <td>Established Year</td>
                            <td>:</td>
                            <td> <? echo $row['est_year'].'. '; ?></td>
                        </tr>


                        <tr>
                            <td>Principal's Name</td>
                            <td>:</td>
                            <td><? echo $row['princ_name'].'. '; ?></td>
                            <td>Teacher's Number</td>
                            <td>:</td>
                            <td><? echo $row['teacher_name'].'. '; ?></td>
                        </tr>

                        <tr>
                            <td>School Time</td>
                            <td>:</td>
                            <td><? echo $row['school_time'].'. '; ?></td>
                            <td>Session</td>
                            <td>:</td>
                            <td><? echo $row['session'].'. '; ?></td>
                        </tr>

                        <tr>
                            <td>Starting at</td>
                            <td>:</td>
                            <td><? echo $row['start'].'. '; ?></td>
                            <td>Ending at</td>
                            <td>:</td>
                            <td><? echo $row['end'].'. '; ?></td>
                        </tr>

                        <tr>
                            <td>Medium</td>
                            <td>:</td>
                            <td><? echo $row['medium'].'. '; ?></td>
                            <td>Play Ground</td>
                            <td>:</td>
                            <td><? echo $row['play_ground'].'. '; ?></td>
                        </tr>

                        <tr>
                            <td>Security System</td>
                            <td>:</td>
                            <td><? echo $row['sec_system'].'. '; ?></td>
                            <td>Transportation System</td>
                            <td>:</td>
                            <td><? echo $row['transport'].'. '; ?></td>
                        </tr>

                        <tr>

                            <td>Location details</td>
                            <td>:</td>
                            <td><? echo $row['loc_details'].'. '; ?></td>
                            <td>Mobile & Phone No.</td>
                            <td>:</td>
                            <td><?= $row['mobile'] . ' <span>&</span> ' . $row['phone'].'. ' ?></td>
                        </tr> 
                        <tr>
                            <td>Email Address</td>
                            <td>:</td>
                            <td><? echo $row['email'].'. '; ?></td>
                            <td>Web URL</td>
                            <td>:</td>
                            <td><? echo $row['web'].'. '; ?></td>
                        </tr>

                        <tr>
                            <td>Extra Curriculum Activities</td>
                            <td>:</td>
                            <td colspan="4"><? echo $row['xtra_cur_activities'].'. '; ?></td>
                        </tr>
                        <tr>
                            <td>Other Information</td>
                            <td>:</td>
                            <td colspan="4"><? echo $row['other_info'].'. '; ?></td>
                        </tr>
                        <?
                    }
                    ?>
                </table>

<!--                <div id="gallery">
                    <div id="sub_gallery">
                        <? //include("gallery.php"); ?>
                    </div>
                </div>-->
            </div>
        </div>
        <div id="clear">
        </div>   
        <div id="footer">
            <div id="footer_div">
                <? include("footer_content.php"); ?>
            </div>   
        </div>
    </body>
</html>
