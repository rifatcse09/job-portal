<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1">
<title>Portfolio</title>
<link rel="icon" href="favicon.png" type="image/png">
<link href="css1/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css1/style.css" rel="stylesheet" type="text/css"> 
<link href="css1/font-awesome.css" rel="stylesheet" type="text/css"> 
<link href="css1/animate.css" rel="stylesheet" type="text/css">
 
<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->
 
</head>
<body>

<!--Header_section-->
<header id="header_wrapper">
  <div class="container">
    <div class="header_box">
      <div class="logo"><a href="#"><img src="img/logo.png" alt="logo"></a></div>
	  <nav class="navbar navbar-inverse" role="navigation">
      <div class="navbar-header">
        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
	    <div id="main-nav" class="collapse navbar-collapse navStyle">
			<ul class="nav navbar-nav" id="mainNav">
			  <li class="active"><a href="#hero_section" class="scroll-link">Home</a></li>
			  <li><a href="#aboutUs" class="scroll-link">About Us</a></li>
			  <li><a href="#service" class="scroll-link">Services</a></li>
			  <li><a href="#Portfolio" class="scroll-link">Portfolio</a></li>
			  <li><a href="#clients" class="scroll-link">Clients</a></li>
			  <li><a href="#team" class="scroll-link">Team</a></li>
			  <li><a href="#contact" class="scroll-link">Contact</a></li>
			</ul>
      </div>
	 </nav>
    </div>
  </div>
</header>
<!--Header_section--> 

<!--Hero_Section-->
<!--Hero_Section--> 

<section id="aboutUs"><!--Aboutus-->
<div class="inner_wrapper">
  <div class="container">
  <!--Test--> 
  
  
 <?
                                include ("database.php");
                                $id = $_GET['id'];
                                $sql = "SELECT p.id,p.product_name,p.description,c.client_name,p.file_name,c.web_add FROM product as p join client as c on   c.id=p.com_id where p.id='$id'";
                                $result = mysql_query($sql);
                                while ($row = mysql_fetch_array($result)) {
                                    ?>
  
  
  
  <!--Test--> 
  
  
  
    <h2><?= $row['product_name'] ?></h2>
    <div class="inner_section">
	<div class="row">
      <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right">
	  
	  <img src="admin/document_upload/<?= $row['file_name'] ?>" class="img-circle delay-03s animated wow zoomIn" alt=""/></div>
      	<div class=" col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-left">
        	<div class=" delay-01s animated fadeInDown wow animated">
			<h3><?= $row['client_name'] ?></h3><br/> 
            <p><?= $row['description'] ?></p>

			
</div>
<div class=" delay-01s animated fadeInDown wow animated">
			
		
			<p font-size=16px><b>Company Wevsite:</b> <a href="<?= $row['web_add'] ?>" target="_blank" >
			<?= $row['web_add'] ?></a></p>
			
</div>


<div class="work_bottom"> <span></span> <a href="../pentagems/" class="contact_btn">Home Link</a> </div>       
	   </div>
      	
      </div>
	  
      
    </div>
	 <? } ?>
  </div> 
  </div>
</section>
<!--Aboutus--> 


<!--Service-->

<!--Service-->




<!-- Portfolio -->

<!--/Portfolio --> 


<!--client_logos-->


<!--/Team-->
<!--Footer-->
<footer class="footer_wrapper" id="contact">
  <div class="container">
    
  </div>
  <div class="container">
    <div class="footer_bottom"><span>Copyright © 2015,    Developed by PentaGems. </span> </div>
  </div>
</footer>

<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
<script type="text/javascript" src="js/jquery.nav.js"></script> 
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.isotope.js"></script>
<script type="text/javascript" src="js/wow.js"></script> 
<script type="text/javascript" src="js/custom.js"></script>

</body>
</html>