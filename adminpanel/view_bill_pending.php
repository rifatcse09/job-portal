<?php
error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <title>Welcome To Azadijobs Jobsite</title>
            <meta http-equiv="X-UA-Compatible" content="chrome=1"> 
            <meta name="description" content="Welcome to BDMessenger!, the world's most visited home page. Quickly find what you're searching for, get in touch with friends and stay in-the-know with the latest tender and information."> 
            <meta name="keywords" content="bdmessenger, bdmessenger home page, bdmessenger homepage, bdmessenger search, bdmessenger tender, bdmessenger exam, bdmessenger tutor, home tutor, sport, online tender"> 
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <style type="text/css">
                body
                {
                    background:#e5e7f0;
                    margin: 0 auto;	  
                }
                #container
                {
                    margin: 0 auto;
                    width:1000px;
                }

                #header
                {
                    padding:0px;
                    height:60px;

                }

                #header h1 { margin: 0; }

                #navigation
                {
                    float: left;
                    width: 1000px;
                    background: #006699;
                    border:#006699 ridge 1px;
                    margin-top:50px
                }

                #navigation ul
                {
                    margin: 0;
                    padding: 0;
                }

                #navigation ul li
                {
                    list-style-type: none;
                    display: inline;
                }

                #navigation li a
                {
                    display: block;
                    float: left;
                    padding: 5px 5px;
                    color: #fff;
                    text-decoration: none;
                    border-right: 1px solid #fff;
                }

                #navigation li a:hover { background: #383; }

                #content-container
                {
                    float: left;
                    width:1000px;
                    background:#FFFFFF;
                    border:#999999 ridge 1px;
                }
                #clear
                {
                    clear:both;
                }
                #section-navigation
                {
                    float: left;
                    width:150px;
                    display: inline;
                    margin-top: 0px;
                }
                #section-navigation div
                {
                    float: left;
                    width:150px;
                    text-align:center;
                    background:#3b5998;
                    color:#FFFFFF;

                }

                #section-navigation ul
                {
                    margin: 0;
                    padding: 0;
                }

                #section-navigation ul li 
                {
                    padding-left:0px;
                    list-style-type:none;
                    list-style-position:inside;
                    border-bottom:#FFCC00 dotted 1px;
                }
                #section-navigation  li a
                {
                    color:#000066;
                    font-size:11px;
                    padding: 0px 10px;
                    font-family:Geneva, Arial, Helvetica, sans-serif;
                    text-decoration:none;
                    list-style-type:circle;
                }
                #section-navigation  h2
                {
                    color:#000066;
                    margin-top: 0px;
                    font-size:14px;
                    padding: 5px 10px 0px 10px;
                }
                #section-navigation  li a:hover
                {
                    color:#fb0095;
                }
                #content
                {
                    float: left;
                    width:800px;
                    padding: 0px 0;
                    margin: 0 0px  0 10px;
                    border:#CCCCCC dotted 1px;
                }

                #content h2 { color:#006699; border-bottom:#CCCCCC dotted 1px; }

                #aside
                {
                    float:right;
                    width:300px;
                    padding: 0px 0 0 0px;
                    margin: 0 0px 0 0;
                }
                #aside table th
                {
                    text-align:left;
                    color:#006699;
                }
                #aside table td a
                {
                    text-decoration:none;
                    color:#006699;
                    font-family:Verdana, Arial, Helvetica, sans-serif;
                    font-size:11px;
                }

                #aside h3 {color:#006699; }
                #footer_div
                {
                    width:1000px;
                    padding:5px;
                    background:#FFFFFF;
                    border:#CCCCCC ridge 1px; 
                    border-radius:10px;
                }
                #footer_div a
                {
                    text-decoration:none;
                    color:#006699;
                    font-family:Verdana, Arial, Helvetica, sans-serif;
                    font-size:10px;
                }
                #footer_div ul li
                {
                    color:#FF3399;
                }

                #footer
                {
                    clear: left;
                    padding:10px;
                    color:#006699;
                    font-size:12px;
                    width:1000px;

                }
                #footer a
                {
                    text-decoration:none;
                    color:#006699;
                }
                #footer a:hover
                {
                    text-decoration:none;
                    color:#fb0095;
                }
                #table
                {
                    width:950px;
                    padding:5px;
                }
                #table tr th
                {
                    border:#666666 solid 1px;
                }
                #table tr td
                {
                    padding:5px;
                    border:#666666 solid 1px;
                }
                #table tr td img
                {
                    border:0;
                }
            </style>
        </head>
        <body>
            <div id="container">
                <div id="header">
                    <table>
                        <tr>
                            <td width="300"><img src="images/logo.png"></td>
                            <td width="400"><? echo date("d-M-Y || H:i:s"); ?></td>
                            <td><a href="login/logout.php">Logout</a></td>
                        </tr>
                    </table>
                </div>
                <div id="navigation">
                    <? include("menu.php"); ?>
                </div>
                <div id="content-container">
                    <div id="section-navigation">
                        <? include 'sub_menu.php';?>
                    </div>
                    <div id="content">
                        <table width="800">
                            <caption style="background: #666666; padding: 5px; color: #FFFFFF; font-size: 14px; font-weight: bold;">Pending Jobs</caption>
                            <tr>
                                <th width="150">Company Name</th>
                                 <th>Job Title</th>
                                  <th>Voucher</th>
                                 <th>Posting date</th>
                                 
                                 <th>Billing Contact</th>
                                 <th>Amount</th>
                                 <th>Vat</th>
                                 <th>Action</th>
                               
                                
                            </tr>
                            <tr>
                                <td colspan="8"><hr style="margin: 0px;"></td>
                            </tr>
                            <?
                            include("database.php");
                            
                            if (isset($_GET["page"])) {
                                    $page = $_GET["page"];
                                } else {
                                    $page = 1;
                                }
                                $start_from1 = ($page - 1) * 10;
                            $sql = "SELECT v.id,e.job_title,e.email_id,e.posting_date,e.billing_contact,e.company_id,e.company_name,e.is_active,v.voucher_no,v.amount,v.vat,v.is_paid FROM emp_new_job as e join 

emp_voucher as v where e.id=v.job_id and v.is_paid=0 and e.is_active=1 group by e.company_name limit $start_from1, 10";
                            
                            $data = mysql_query($sql);
                            
                            while ($r = mysql_fetch_array($data)) {
                                $url=$r['website'];
                                $newurl=  str_replace('http://www.', '', $url);
                                ?>
                                <tr>
                                    <td style="text-align: center;"><? echo $r['company_name'] ?></td> 
                                    <td style="text-align: center"><? echo $r['job_title'] ?></td>
                                    <td style="text-align: center;"><? echo $r['voucher_no'] ?></td>
                                    <td style="text-align: center"><? echo $r['posting_date'] ?></td>
                                  
                                    <td style="text-align: center"><? echo $r['billing_contact'] ?></td>
                                    <td style="text-align: center"><? echo $r['amount'] ?></td>
                                    <td style="text-align: center"><? echo $r['vat'] ?></td>
                                    <td style="text-align: center"><a href="publish_job.php?id=<?=$r['id']?>">Publish job</a></td>
  
                                </tr>
                                <tr>
                                    <td colspan="8"><hr style="margin: 0px;"></td>
                                </tr>
                                <?
                            }
                            ?>
                        </table>
                        
                          <p align="center" style="margin-top:-2px; clear: both">
                                <?
                                $sql2 = "SELECT count(e.job_title) from emp_new_job as e join 

emp_voucher as v where e.id=v.job_id and v.is_paid=0 and e.is_active=1 group by e.company_name ";
                                $p_result = mysql_query($sql2);
                                $row2 = mysql_fetch_array($p_result);
//                                $tot_rec = $row2[0];
                                $tot_rec = mysql_num_rows($p_result);
                                $tot_page = ceil($tot_rec / 10);
                                for ($i = 1; $i <= $tot_page; $i++) {
                                    if ($page == $i) {
                                       
                                       // echo $i . ' ';
                                        
                                    } else {
//                                                    echo "<a href = 'pending_tour_requisition.php?page=" . $i . "&id=" . $u_id . "'>" . $i . "</a>";
                                        echo "<a style='text-decoration:none;color:green' href = 'view_job_pending.php?page=" . $i . "'>" . $i.' ' ."</a>";
                                    }
                                }
                                ?>
                            </p>
                    </div>
                </div>
                <div id="aside">

                </div>	
                <div id="footer">
                    <? include("footer.php"); ?>	
                </div>
            </div>
        </div>
    </body>
    </html>
    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>