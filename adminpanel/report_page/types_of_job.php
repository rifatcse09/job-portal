<?php
 
//Import the PhpJasperLibrary
include_once('../PhpJasperLibrary/tcpdf/tcpdf.php');
include_once("../PhpJasperLibrary/PHPJasperXML.inc.php");
include_once("setting.php");
 
 
//database connection details

$pchartfolder="../../class/pchart2";
 

$job_type=$_POST['job_type'];

if($job_type=='installation')
{
    $job="Installation";
}
if($job_type=='commissioning')
{
    $job="Commissioning";
}
if($job_type=='overhauling')
{
    $job="Overhauling";
}
if($job_type=='site_visit')
{
    $job="Site Visit";
}
if($job_type=='material_list')
{
    $job="Material List";
}

if($job_type=='trouble_shooting')
{
    $job="Trouble Shooting";
}

if($job_type=='periodic_mainten')
{
    $job="Periodic Maintenance";
}

if($job_type=='monthly_service')
{
    $job="Monthly Service";
}
if($job_type=='super_vision')
{
    $job="Super Vision";
}
if($job_type=='load_calculation')
{
    $job="Load Calculation";
}


$fdate=$_POST['fdate'];
$todate=$_POST['todate'];
//display errors should be off in the php.ini file
ini_set('display_errors', 0);

//setting the path to the created jrxml file
$xml =  simplexml_load_file("../report/types_job.jrxml");

$PHPJasperXML = new PHPJasperXML();
//$PHPJasperXML->debugsql=true;
$PHPJasperXML->arrayParameter=array("from_date"=>"$fdate","to_date"=>"$todate","job_type"=>"$job_type","job_head"=>"$job");
$PHPJasperXML->xml_dismantle($xml);
 
$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);
$PHPJasperXML->outpage("I");    //page output method I:standard output  D:Download file
 
 
?>