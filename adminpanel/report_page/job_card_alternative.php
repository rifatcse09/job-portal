<?php
 
//Import the PhpJasperLibrary
include_once('../PhpJasperLibrary/tcpdf/tcpdf.php');
include_once("../PhpJasperLibrary/PHPJasperXML.inc.php");
include_once("setting.php");
 
 
//database connection details

$pchartfolder="../../class/pchart2";
 
 
//display errors should be off in the php.ini file
ini_set('display_errors', 0);

$query_id=$_GET['query_id'];

 
//setting the path to the created jrxml file
$xml =  simplexml_load_file("../report/rpt_job_card_alternative.jrxml");
 
$PHPJasperXML = new PHPJasperXML();
//$PHPJasperXML->debugsql=true;
$PHPJasperXML->arrayParameter=array("parameter1"=>"$query_id");
$PHPJasperXML->xml_dismantle($xml);
 
$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);
$PHPJasperXML->outpage("I");    //page output method I:standard output  D:Download file
 
 
?>