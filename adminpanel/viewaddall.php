<?php
error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <title>Welcome To Azadijobs Jobsite</title>
            <meta http-equiv="X-UA-Compatible" content="chrome=1"> 
            <meta name="description" content="Welcome to BDMessenger!, the world's most visited home page. Quickly find what you're searching for, get in touch with friends and stay in-the-know with the latest tender and information."> 
            <meta name="keywords" content="bdmessenger, bdmessenger home page, bdmessenger homepage, bdmessenger search, bdmessenger tender, bdmessenger exam, bdmessenger tutor, home tutor, sport, online tender"> 
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <style type="text/css">
                body
                {
                    background:#e5e7f0;
                    margin: 0 auto;	  
                }
                #container
                {
                    margin: 0 auto;
                    width:1000px;
                }

                #header
                {
                    padding:0px;
                    height:60px;

                }

                #header h1 { margin: 0; }

                #navigation
                {
                    float: left;
                    width: 1000px;
                    background: #006699;
                    border:#006699 ridge 1px;
                }

                #navigation ul
                {
                    margin: 0;
                    padding: 0;
                }

                #navigation ul li
                {
                    list-style-type: none;
                    display: inline;
                }

                #navigation li a
                {
                    display: block;
                    float: left;
                    padding: 5px 5px;
                    color: #fff;
                    text-decoration: none;
                    border-right: 1px solid #fff;
                }

                #navigation li a:hover { background: #383; }

                #content-container
                {
                    float: left;
                    width:1000px;
                    background:#FFFFFF;
                    border:#999999 ridge 1px;
                }
                #clear
                {
                    clear:both;
                }
                #section-navigation
                {
                    float: left;
                    width:150px;
                    display: inline;
                    margin-top: 0px;
                }
                #section-navigation div
                {
                    float: left;
                    width:150px;
                    text-align:center;
                    background:#3b5998;
                    color:#FFFFFF;

                }

                #section-navigation ul
                {
                    margin: 0;
                    padding: 0;
                }

                #section-navigation ul li 
                {
                    padding-left:0px;
                    list-style-type:none;
                    list-style-position:inside;
                    border-bottom:#FFCC00 dotted 1px;
                }
                #section-navigation  li a
                {
                    color:#000066;
                    font-size:11px;
                    padding: 0px 10px;
                    font-family:Geneva, Arial, Helvetica, sans-serif;
                    text-decoration:none;
                    list-style-type:circle;
                }
                #section-navigation  h2
                {
                    color:#000066;
                    margin-top: 0px;
                    font-size:14px;
                    padding: 5px 10px 0px 10px;
                }
                #section-navigation  li a:hover
                {
                    color:#fb0095;
                }
                #content
                {
                    float: left;
                    width:800px;
                    padding: 0px 0;
                    margin: 0 0px  0 10px;
                    border:#CCCCCC dotted 1px;
                }

                #content h2 { color:#006699; border-bottom:#CCCCCC dotted 1px; }

                #aside
                {
                    float:right;
                    width:300px;
                    padding: 0px 0 0 0px;
                    margin: 0 0px 0 0;
                }
                #aside table th
                {
                    text-align:left;
                    color:#006699;
                }
                #aside table td a
                {
                    text-decoration:none;
                    color:#006699;
                    font-family:Verdana, Arial, Helvetica, sans-serif;
                    font-size:11px;
                }

                #aside h3 {color:#006699; }
                #footer_div
                {
                    width:1000px;
                    padding:5px;
                    background:#FFFFFF;
                    border:#CCCCCC ridge 1px; 
                    border-radius:10px;
                }
                #footer_div a
                {
                    text-decoration:none;
                    color:#006699;
                    font-family:Verdana, Arial, Helvetica, sans-serif;
                    font-size:10px;
                }
                #footer_div ul li
                {
                    color:#FF3399;
                }

                #footer
                {
                    clear: left;
                    padding:10px;
                    color:#006699;
                    font-size:12px;
                    width:1000px;

                }
                #footer a
                {
                    text-decoration:none;
                    color:#006699;
                }
                #footer a:hover
                {
                    text-decoration:none;
                    color:#fb0095;
                }
                #table
                {
                    width:950px;
                    padding:5px;
                }
                #table tr th
                {
                    border:#666666 solid 1px;
                }
                #table tr td
                {
                    padding:5px;
                    border:#666666 solid 1px;
                }
                #table tr td img
                {
                    border:0;
                }
            </style>
        </head>
        <body>
            <div id="container">
                <div id="header">
                    <table>
                        <tr>
                            <td width="300"><img src="images/logo.png"></td>
                            <td width="400"><? echo date("d-M-Y || H:i:s"); ?></td>
                            <td><a href="login/logout.php">Logout</a></td>
                        </tr>
                    </table>
                </div>
                <div id="navigation">
                    <? include("menu.php"); ?>
                </div>
                <div id="content-container">
                    <div id="section-navigation">
                        <? include 'sub_menu.php'; ?>
                    </div>
                    <div id="content">
                        <table width="800">
                            <caption style="background: #666666; padding: 5px; color: #FFFFFF; font-size: 14px; font-weight: bold;">Result View</caption> 
                            <?
                            include("database.php");
                            $id = $_GET['add_id'];
                            $sql = "select * from advertisement where add_id=" . $id;
                            $data = mysql_query($sql);
                            while ($r = mysql_fetch_array($data)) {
                                ?>
                                <tr>
                                    <td width="150px"><strong>Advertisement Name</strong></td>
                                    <td>:</td>
                                    <td><a href="<? echo $r['website']?>" target="_blank"><? echo $r['advertisement_name'] ?></a></td> 
                                </tr>
                                <tr>
                                    <td><strong>Advertisement Type</strong></td>
                                    <td>:</td>
                                    <td><? echo $r['advertisement_type'] ?></td>
                                </tr>
                                <tr>
                                    <td valign="top" colspan="3"><img src="advertise_images/<? echo $r['name']; ?>" width="800" height="600" /></td>
                                </tr>
                                <?
                            }
                            ?>

                        </table>
                    </div>
                </div>
                <div id="aside">

                </div>	
                <div id="footer">
                    <? include("footer.php"); ?>	
                </div>
            </div>
        </div>
    </body>
    </html>
    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>