<?php
error_reporting(0);
session_start();
$user_name = $_SESSION['username'];
if ($user_name) {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <title>Welcome To Azadijobs Jobsite</title>
            <meta http-equiv="X-UA-Compatible" content="chrome=1"> 
            <meta name="description" content="Welcome to BDMessenger!, the world's most visited home page. Quickly find what you're searching for, get in touch with friends and stay in-the-know with the latest tender and information."> 
            <meta name="keywords" content="bdmessenger, bdmessenger home page, bdmessenger homepage, bdmessenger search, bdmessenger tender, bdmessenger exam, bdmessenger tutor, home tutor, sport, online tender"> 
            <link rel="stylesheet" type="text/css" href="css/jquerycssmenu.css" />
            <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.css" rel="stylesheet" type="text/css"> 
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/custom.css" rel="stylesheet" type="text/css">
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/jquerycssmenu.js"></script>
            <style type="text/css">
                body
                {
                    background:#e5e7f0;
                    margin: 0 auto;	  
                }
                #container
                {
                    margin: 0 auto;
                    width:1000px;
                }

                #header
                {
                    padding:0px;
                    height:60px;

                }

                #header h1 { margin: 0; }

                #navigation
                {
                    float: left;
                    width: 1000px;
                    background: #006699;
                    border:#006699 ridge 1px;
                    margin-top: 35px;
                }

                #navigation ul
                {
                    margin: 0;
                    padding: 0;
                }

                #navigation ul li
                {
                    list-style-type: none;
                    display: inline;
                }

                #navigation li a
                {
                    display: block;
                    float: left;
                    padding: 5px 5px;
                    color: #fff;
                    text-decoration: none;
                    border-right: 1px solid #fff;
                }

                #navigation li a:hover { background: #383; }

                #content-container
                {
                    float: left;
                    width:1000px;
                    background:#FFFFFF;
                    border:#999999 ridge 1px;
                }
                #clear
                {
                    clear:both;
                }
                
                #section-navigation div
                {
                    float: left;
                    width:150px;
                    text-align:center;
                    background:#3b5998;
                    color:#FFFFFF;

                }

                #section-navigation
                {
                    float: left;
                    width:150px;
                    display: inline;
                    margin-top: 0px;
                    background: #001A33;
                    text-align: center;
                }

                #section-navigation ul
                {
                    margin: 0;
                    padding: 0;
                }

                #section-navigation ul li 
                {
                    padding-left:0px;
                    padding-bottom: 3px;
                    list-style-type:none;
                    list-style-position:inside;
                    border-bottom:#75A3A3 solid 1px;
                }
                #section-navigation  li a
                {
                    color:#75A3A3;
                    font-size:11px;
                    padding: 0px 10px;
                    font-family:Geneva, Arial, Helvetica, sans-serif;
                    text-decoration:none;
                    list-style-type:circle;
                }
                #section-navigation  h2
                {
                    color:#75A3A3;
                    margin-top: 0px;
                    font-size:14px;
                    padding: 5px 10px 0px 10px;
                }
                #section-navigation  li a:hover
                {
                    color:#fff;
                }
                #content
                {
                    float: left;
                    width:800px;
                    padding: 0px 0;
                    margin: 0 0px  0 10px;
                    border:#CCCCCC dotted 1px;
                }

                #content h2 { color:#006699; border-bottom:#CCCCCC dotted 1px; }

                #aside
                {
                    float:right;
                    width:300px;
                    padding: 0px 0 0 0px;
                    margin: 0 0px 0 0;
                }
                #aside table th
                {
                    text-align:left;
                    color:#006699;
                }
                #aside table td a
                {
                    text-decoration:none;
                    color:#006699;
                    font-family:Verdana, Arial, Helvetica, sans-serif;
                    font-size:11px;
                }

                #aside h3 {color:#006699; }
                #footer_div
                {
                    width:1000px;
                    padding:5px;
                    background:#FFFFFF;
                    border:#CCCCCC ridge 1px; 
                    border-radius:10px;
                }
                #footer_div a
                {
                    text-decoration:none;
                    color:#006699;
                    font-family:Verdana, Arial, Helvetica, sans-serif;
                    font-size:10px;
                }
                #footer_div ul li
                {
                    color:#FF3399;
                }

                #footer
                {
                    clear: left;
                    padding:10px;
                    color:#006699;
                    font-size:12px;
                    width:1000px;

                }
                #footer a
                {
                    text-decoration:none;
                    color:#006699;
                }
                #footer a:hover
                {
                    text-decoration:none;
                    color:#fb0095;
                }
                #table
                {
                    width:950px;
                    padding:5px;
                }
                #table tr th
                {
                    border:#666666 solid 1px;
                }
                #table tr td
                {
                    padding:5px;
                    border:#666666 solid 1px;
                }
                #table tr td img
                {
                    border:0;
                }
            </style>
            <script type="text/javascript" src="tiny_mce/tiny_mce.js"></script>
            <script type="text/javascript">
                tinyMCE.init({
                    // General options
                    mode: "textareas",
                    theme: "advanced",
                    plugins: "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",
                    // Theme options
                    theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                    theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                    theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                    theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
                    theme_advanced_toolbar_location: "top",
                    theme_advanced_toolbar_align: "left",
                    theme_advanced_statusbar_location: "bottom",
                    theme_advanced_resizing: true,
                    // Example content CSS (should be your site CSS)
                    content_css: "css/content.css",
                    // Drop lists for link/image/media/template dialogs
                    template_external_list_url: "lists/template_list.js",
                    external_link_list_url: "lists/link_list.js",
                    external_image_list_url: "lists/image_list.js",
                    media_external_list_url: "lists/media_list.js",
                    // Style formats
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ],
                    // Replace values for the template plugin
                    template_replace_values: {
                        username: "Some User",
                        staffid: "991234"
                    }
                });
            </script>
             <script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
            <script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
            <link  rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.16.custom.css" />
            <script type="text/javascript">
                $(document).ready(function(){
                    $("#last_date").datepicker({
                        dateFormat:"yy-mm-dd"
                    })
                })   
            </script>
        </head>
        <body>
            <div id="container">
                <div id="header">
                    <table>
                        <tr>
                            <td width="300"><img src="images/logo.png"></td>
                            <td width="400"><? echo date("d-M-Y || H:i:s"); ?></td>
                            <td><a href="login/logout.php">Logout</a></td>
                        </tr>
                    </table>
                </div>
                <div id="navigation">
                    <? include("menu.php"); ?>
                </div>
                <div id="content-container">
                    <div id="section-navigation">
                        <? include 'sub_menu.php'; ?>
                    </div>
                    <div id="content">
                        <form action="insert/hire_job_insert.php" enctype="multipart/form-data" method="post">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                
                               
                                <tr>
                                    <td>Name</td>
                                    <td>:</td>
                                    <td><input type="text" name="name" size="40" /></td>
                                </tr>
                                <tr>
                                    <td>Image</td>
                                    <td>:</td>
                                    <td>
                                        <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
                                        <input type="file" name="userfile" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td>Expert in</td>
                                    <td>:</td>
                                    <td><input type="text" name="expert" size="40" /></td>
                                    
                                </tr>
                                <tr>
                                    <td>Experience Description</td>
                                    <td>:</td>
                                    <td><textarea name="expert_area" ></textarea></td>
                                </tr>
                                <tr>
                                    <td>Hourly/Daily Rate</td>
                                    <td>:</td>
                                    <td><input type="text" name="rate" size="40" /></td>
                           
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td><input type="text" name="email" size="40" /></td>
                                </tr>
                                 <tr>
                                    <td>Mobile</td>
                                    <td>:</td>
                                     <td><input type="text" name="mobile" size="40" /></td>
                                </tr>
                                
                                 <tr>
                                    <td>National ID</td>
                                    <td>:</td>
                                    <td><input type="text" name="nid" size="40" /></td>
                                   
                                </tr>
                                
                                 
                                <tr>
                                    <td>Location</td>
                                    <td>:</td>
                                    <td><textarea name="location" ></textarea></td>
                                </tr>
                                <tr>
                                    <td>Free Time & Date</td>
                                    <td>:</td>
                                    <td><input type="text" name="free" size="40" /></td>
                               
                                </tr>
                                
                               
                             
                                <tr>
                                    <td colspan="3" align="center"><input type="submit" name="submit" size="30" style="border:#006699 solid 5px; width:80px; font-size:14px; border-radius:20px; background:#006699; padding:5px; color:#FFFFFF" value="Send" /></td>
                                </tr>
                            </table>
                            <?
                            ?>
                        </form>
                    </div>
                </div>
                <div id="aside">

                </div>	
                <div id="footer">
                    <? include("footer.php"); ?>	
                </div>
            </div>
        </div>
    </body>
    </html>
    <?php
} else {
//echo "index.php";
    echo "<h2 align=center><font color='#009900' size='+2'>Wrong Information</font></h2>";
    echo "<meta http-equiv='refresh' content='1 URL=../index.php'>";
}
?>