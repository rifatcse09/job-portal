<?php

/**
 * Description of m_mail
 *
 * @author: 
 */
class m_mail extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('Guid');
        $this->load->library('upload');
    }

    function insert_entry($data = array()) {
        $ins['to'] = isset($data['to']) ? $data['to'] : '';
        $ins['from'] = isset($data['from']) ? $data['from'] : '';
        $ins['cc'] = isset($data['cc']) ? $data['cc'] : '';
        $ins['bcc'] = isset($data['bcc']) ? $data['bcc'] : '';
        $ins['subject'] = isset($data['subject']) ? $data['subject'] : '';
        $ins['content'] = isset($data['content']) ? $data['content'] : '';
        $ins['attachment'] = isset($data['attachment']) ? $data['attachment'] : 'true';
        $ins['status'] = isset($data['status']) ? $data['status'] : '1';

        $this->db->insert('mail_list', $ins);
    }

    function upload_files($file_name) {
        $error = '';
        $success = '';
        $config['upload_path'] = './assets/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|doc|pdf|docx|xls|xlsx|zip|rar';
        $config['max_size'] = '5000';
        $config['overwrite'] = True;

        $cnt = count($_FILES[$file_name]['name']);

        $files = $_FILES;
        for ($i = 0; $i < count($_FILES[$file_name]['name']); $i++) {
            $_FILES['userfile']['name'] = $_FILES[$file_name]['name'][$i];
            $_FILES['userfile']['type'] = $_FILES[$file_name]['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES[$file_name]['tmp_name'][$i];
            $_FILES['userfile']['error'] = $_FILES[$file_name]['error'][$i];
            $_FILES['userfile']['size'] = $_FILES[$file_name]['size'][$i];

            $ext = pathinfo($_FILES[$filename]['name'], PATHINFO_EXTENSION);
            $file = $this->guid->create_guid() . '.' . $ext;
            $config['file_name'] = $file;
            
            $this->upload->initialize($config);
            if(!$this->upload->do_upload()){
                
            }
        }

        return $error . '<br/><br/>' . $success;
    }

}

?>