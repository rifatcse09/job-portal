<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class mainpage extends CI_Controller {

    public function index() {
        //$data['title'] = 'Welcome to Azadijobs :: Largest Job Portal In Chittagong';
        $this->load->model('Category');
        $data['title'] = 'Welcome to Azadijobs :: Largest Job Portal In Chittagong';
        $this->load->model('Job');
        $data['hot_jobs'] = $this->Job->hot_job();
        $data['hire_jobs'] = $this->Job->hire_job();

        $data['categories'] = $this->Category->select_category_for_plain_list();
        $this->load->view('view_mainpage', $data);
    }

    public function create_acc() {
        $data['title'] = 'Create New Account';
        $this->load->view('create_new_account', $data);
    }

    public function log_me_in() {
        
//        $this->load->model('employee');
//        if ($this->employee->log_me_in($_POST['user_name'], $_POST['password']) == true) {
//            redirect(base_url() . 'employee/index.php/page');//here employee is employee project
//        } else {
//            $this->session->set_flashdata('msg', 'User name and password combination is wrong');
            redirect(base_url() . 'employee/index.php/page/login_failed');
        //}
    }
  public function reset_pass()
    {
         $this->load->view('reset_pass');
    }
  public function pagination_ready_all()
    {
        $this->load->view('ready_all');
    }
    
  public function pagination_micro_job_all()
    {
        $this->load->view('microjob_all');
    }
    public function pagination_hire_job_all()
    {
        $this->load->view('hire_job_all');
    }
    
    public function about_azadijobs()
    {
        $this->load->view('about');
    }
    
    public function corporate_products()
    {
        $this->load->view('cor_product');
    }
      public function terms_condition()
    {
        $this->load->view('term');
    }
    
    
       
      
	 public function gen_service()
    {
        $this->load->view('gen_service');
    }
	public function feedback()
    {
        $this->load->view('feedback');
    }

      
	
    
      public function partner()
    {
        $this->load->view('partner');
    }
    
      public function contact()
    {
        $this->load->view('contact');
    }
    
       public function faq()
    {
        $this->load->view('faq');
    }
	

}