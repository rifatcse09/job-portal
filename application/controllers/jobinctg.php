<?php
 class JobInCtg extends CI_Controller{
     function showAllJobsCtg (){
         $data['title'] = empty($_GET['cat_name']) ? 'Suprovat Jobs' : $_GET['cat_name'];
	 $this->load->model('Category');
         $data['jobs'] = $this->Category->jobs_by_category($_GET['cat_id']);
         $ses_data = array(
             'cat_id' => $_GET['cat_id'],
             'cat_name' => $_GET['cat_name']
         );
         $this->session->set_userdata($ses_data);
         $data['cat_id'] = $this->session->userdata('cat_id');
         $data['cat_name'] = $this->session->userdata('cat_name');
         $this->load->view('jobsbybatagories', $data);
     }
     
     function JobDetailsCtg(){
         $this->load->model('Job');
         $this->load->model('emp_acc_info');
         $data['main_job'] = $this->Job->job_details($_GET['ID']);
         $data['business_types'] = $this->Job->job_business_type($_GET['ID']);
         $data['business_areas'] = $this->Job->job_business_area($_GET['ID']);
         $data['cat_name'] = $this->session->userdata('cat_name');
         $com_id = $data['main_job']['company_id'];
         $data['com_info'] = $this->Emp_acc_info->get_acc_info($com_id);
         $this->load->view('jobbetails', $data);
     }
 }
?>
