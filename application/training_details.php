<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Training</title>
        <style type="text/css">
            #content_container
            {
                width: 100%;
                background: #f0f0f0;
            }
            #content
            {
                float: left;
                width: 80%;
                margin: 0% 10%;
                border-top: 20px solid #d7d7d7;
                border-right: 2px solid #d7d7d7;
                border-left: 2px solid #d7d7d7;
                border-bottom: 2px solid #d7d7d7;
                border-radius: 5px;
                background: #fff;
            }
            #training_header
            {
                float:left;
                width:600px;
                margin-left: 130px;
                margin-bottom: 20px;
            }

            #headline_box
            {
                float: left;
                width: 60%;
                height: 35px;
                background: #a5090a;
                margin-top: 20px;
            }
            #headline 
            {
                float: left;
                width: 100%;
            }
            #headline_bgcol
            {
                float: left;
                width: 90%;
                height: 25px;
                background: #f0f0f0;
                margin-left: 10px;
                margin-top: 5px;
            }
            #headline_bgcol p
            {
                text-align: left;
                font-family: euphemia;
                font-weight: bold;
                font-size: 20px;
                color: #000;
                margin-top: -4px;
                margin-left: 20px;
            }
            #main_box
            {
                float: left;
                width: 99.1%;
                border: 5px solid #a5090a;
            }
            #headbox
            {
                float: left;
                width: 98%;
                margin-left: 1%;
                margin-top: 2%;
                border: 1px solid #000;
                border-radius: 10px;
                margin-right: 10%;
            }
            #headbox_left
            {
                float: left;
                width: 80%;
                margin-left: 10px;
            }
            #headbox_left td
            {
                font-size: 11pt;
                font-family: arial;
            }
            #headbox_right
            {
                float: left;
                width: auto;
                margin-left:15px;
                margin-top: 0px;
             
            }
            #headbox_right td
            {
                font-size: 11pt;
                font-family: arial;
            }
            #main_content
            {
                float: left;
                margin-top: 20px;
                width: 100%;
            }
            #main_content_left
            {
                float: left;
                width: 300px;
                margin-left: 15px;
            }
            #main_content_resource
            {
                width: 300px;
                height: auto;
                border: 1px solid #000;
                border-radius: 5px;
            }
            #main_content_resource_header
            {
                width: 200px;
                margin-left: 15px;

            }
            #main_content_resource_header p
            {
                font-size: 10pt;
                font-weight: bold;
                color: #6894F9;
                font-family: arial;

            }
/*            #main_content_resource_text
            {
                float: left;
                width: 275px;
                margin-left: 15px;
               
            }*/
            #main_content_resource_text p
            {
                font-family: arial;
                font-size: 8pt;
                text-align: justify;
                color: #21201e;
            }
            #main_content_attend
            {
                width: 300px;
                height: 250px;
                border: 1px solid #000;
                border-radius: 5px;
                margin-top: 10px;
            }
            #main_content_attend_text
            {
                float: left;
                width: 275px;
                margin-top: -20px;
            }
            #main_content_attend_text li
            {
                font-family: arial;
                font-size: 8pt;
                text-align: justify;
                color: #21201e;
            }
            #main_content_methodology
            {
                width: 300px;
                height: 100px;;
                border: 1px solid #000;
                border-radius: 5px;
                margin-top: 10px;
            }
            #main_content_methodology_text
            {
                float: left;
                width: 275px;
                margin-top: -20px;
                margin-left: 15px;
            }
            #main_content_methodology_text p
            {
                font-family: arial;
                font-size: 8pt;
                color: #21201e;
                font-weight: bold;
            }
            #main_content_trainee
            {
                width: 300px;
                height: 150px;;
                border: 1px solid #000;
                border-radius: 5px;
                margin-top: 10px;
            }
            #main_content_trainee_text
            {
                float: left;
                width: 275px;
                margin-top: -20px;
                margin-left: 15px;
            }
            #main_content_trainee_text p
            {
                font-family: arial;
                font-size: 8pt;
                color: #21201e;
                text-align: justify;
            }
            #main_content_workshop
            {
                width: 300px;
                height: 230px;;
                border: 1px solid #000;
                border-radius: 5px;
                margin-top: 10px;
            }
            #main_content_workshop td
            {
                font-family: arial;
                font-size: 8pt;
            }

            #main_content_right
            {
                float: left;
                width: 720px;
                margin-left: 300px;
                border: 1px solid #000;
                border-radius: 5px;
            }
            
         
            #main_content_right_top_header
            {
                float: left;
                width: 680px;
                margin-left: 15px;
            }
            #main_content_right_top_header p
            {
                font-family: arial;
                color: #808686;
                font-size: 12pt;
                font-weight: bold;
            }
            #main_content_right_text
            {
                float: left;
                width: 690px;
                margin-left: 15px;
              
            }
            #main_content_right_text p
            {
                font-family: arial;
                font-size: 8pt;
                color: #21201e;
                text-align: justify;
            }
            #main_content_right_subHeader
            {
                float: left;
                width: 680px;
                margin-left: 15px;
                margin-top: -15px;
            }
            #main_content_right_subHeader p
            {
                font-family: arial;
                font-size: 10pt;
                color: #21201e;
                font-weight: bold;
            }
            #main_content_right_subHeader_text
            {
                float: left;
                width: 680px;
                margin-left: 15px;
                margin-top: -15px;
            }
            #main_content_right_subHeader_text p
            {
                font-family: arial;
                font-size: 8pt;
                color: #21201e;
                text-align: justify;
            }
            #main_content_right_subHeader_text li
            {
                font-family: arial;
                font-size: 8pt;
                color: #21201e;
                text-align: justify;
            }
            #main_content_bottom
            {
                float: left;
                width: 680px;
                margin-left: 15px;
            }
            #bottom_div
            {
                float: right;
                width: 720px;
                height: 40px;
                border: 1px solid #000;
                border-radius: 5px;
                margin-right: 15px;
                margin-top: 20px;
                margin-bottom: 20px;
            }
            #bottom_div_text
            {
                width: 720px;
                float: left;
                text-align: center;
            }
            #bottom_div_text a
            {
                font-family: arial;
                font-size: 10pt;
                font-weight: bold;
                text-decoration: none;
                color: #013b9f;
            }
        </style>
    </head>
    <body>
        <div id="content_container">
            <div id="training_header">
                <img src="<?= base_url() ?>images/logo.png"/>
            </div>
            <div id="content">
                <?
                include("database.php");
                $id = $_GET['training_id'];
                $sql = "select * from training where training_id=" . $id;
                $data = mysql_query($sql);
                while ($r = mysql_fetch_array($data)) {
                    ?>
                    <div id="headline_box">
                        <div id="headline">
                            <div id="headline_bgcol">
                                <p><? echo $r['title']; ?></p>
                            </div>
                        </div>
                    </div>
                    <div id="main_box">
                        <div id="headbox">
                            <div id="headbox_left">
                                <table>
                                    <tr>
                                        <td style="color: #a5090a;font-size: 10pt; text-align: justify;width: 300px">Date : <? echo $r['training_date']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Shift : <? echo $r['shift']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Venue : <? echo $r['Venue']; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div id="headbox_right">
                                <table style='margin-top:0px'>
                                    <tr>
                                        <td>Last Date of Registration : <? echo $r['lastdate_registration']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Time : <? echo $r['time']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Registration Fees : <? echo $r['registration_fees']; ?> </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div id="main_content">
                            <div id="main_content_left">
                                <div id="main_content_resource">
                                    <div id="main_content_resource_header">
                                        <p>RESOURCE PERSON</p>
                                        
                                    </div>
                                    
                                    <div id="main_content_resource_text">
                                        <img src="<?= base_url() ?>adminpanel/training_images/<? echo $r['name']; ?>" width="125" height="125" style="margin-left: 20px;">
                                        <p style="margin-left: 10px; color: #013b9f;margin-right: 10px"><? echo $r['resource_person']; ?></p>
<!--                                        <p>Having obtained M.Sc from University of Dhaka in 1992 , Mr. Bhuiyan started his career with an international trading company and worked for LG Corporation, Korea, Sumitomo Corporation, Singapore, Mitsui Corporation, Mitsubishi Corp. Japan at later stage of his life.</p>
                                        <p>He attended many trainings and seminars on international business and got experience in import, export, indenting and international tender business. He participated in many international tender with PDB, REB, DESA, WASA, DGDP and Biman Bangladesh Airlines. Now he has been working in international business for about 20 years. As part of his job responsibility, he is dealing with Banks, Insurance, Inspection Co., Customs Authority, C&F agents, DCCI, BIAA, BOI, EPB and others business related organizations.</p>-->
                                    </div>
                                    
                                </div>
                                <div id="main_content_attend">
                                    <div id="main_content_resource_header">
                                        <p>Who can Attend</p>
                                    </div>
                                    <div id="main_content_attend_text">
                                        <? echo $r['attend_person']; ?>
<!--                                        <ol>
                                            <li>Official involved in C&F Agent, Export, Import, Indenting and International Tender Business.</li>

                                            <li>Importers, Exporters, and new Entrepreneurs.</li>

                                            <li>Official engaged in ready-made Garments.</li>

                                            <li>Persons are willing to job in the International Trading Organization and Multinational companies in home and abroad.</li>

                                            <li>Students are willing to learn Practical Oriented C& F Agent Business.</li>

                                            <li>Business Manager, Marketing Manager, Marketing Executive, and other interested suitable persons.</li>
                                        </ol>-->
                                    </div>
                                </div>
                                <div id="main_content_methodology">
                                    <div id="main_content_resource_header">
                                        <p>METHODOLOGY</p>
                                    </div>
                                    <div id="main_content_methodology_text">
                                        
                                        <p><?= $r['methodology'];?></p>
                                    </div>
                                </div>
                                <div id="main_content_trainee">
                                    <div id="main_content_resource_header">
                                        <p>TRAINEE TESTIMONIAL</p>
                                    </div>
                                    <div id="main_content_trainee_text">
                                      <p><?= $r['t_testimonial'];?></p>
                                    </div>
                                </div>
                                <div id="main_content_workshop">
                                    <div id="main_content_resource_header">
                                        <p>WORKSHOP DETAILS</p>
                                    </div>
                                    <div id="headbox_left">
                                        <table>
                                            <tr>
                                                <td>Venue :<?= $r['Venue'];?></td>
                                                
                                            </tr>
                                            <tr>
                                                <td style="color: #a5090a;font-size: 10pt;">Date : <?= date('d-m-Y',strtotime($r['training_date']));?> to <?=  date('d-m-Y',strtotime($r['training_date_to']))?></td>
                                            </tr>
                                            <tr>
                                                <td>Total Hours :<?= $r['hour']?> </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Time : <?= $r['time'];?></td>
                                            </tr>
                                            <tr>
                                                <td>Days: <?= $r['days']?></td>
                                            </tr>
                                            <tr>
                                                <td>Shift :<?= $r['shift']?></td>
                                            </tr>
                                            <tr>
                                                <td>Registration Fees : Tk. <?= $r['registration_fees'];?>/Participant </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="main_content_right">
                                <div id="main_content_right_top_header">
                                    <p><? echo $r['title']; ?></p>
                                </div>
                                <div id="main_content_right_text">
                                    <P><? echo $r['training_content']; ?></p>
                                    
                                </div>
                                
                                
                               
                                
                                <div id="main_content_right_subHeader_text">
                                    
                                </div>
                                
                                <a style="float: right;margin-right: 35px;text-decoration: none;color: #a5090a;"href="<?= site_url()?>/jobcategory/request">Request for Registration</a>
<!--                                <div id="main_content_bottom">
                                    <h4 style="text-align: center">Certificates will be awarded to participants at the end of workshop REGISTRATION DETAILS</h4>
                                    <div id="main_content_right_subHeader_text">
                                        <p style="text-align: center">For Registration or additional information please call us at 9140345,9117179, 01811 487982, 01926673098</p>
                                        <p style="text-align: center;">or email at</p>
                                        <a style="margin-left: 200px;"href="#">ridoy@bdjobs.com</a> or <a href="#">or hasantareq@bdjobs.com </a>
                                        <p style="text-align: center">To confirm your registration, Please pay the registration fee by <b>20 December, 2012 at Bdjobs.com Office by A/C Payee Cheque or cash.</b> </p>
                                    </div>

                                    <div id="main_content_right_subHeader_text">
                                        <table style="margin-top: 20px;">
                                            <tr>
                                                <td style="float: left;margin-left: 300px;"><b>Purbodesh</b></td>
                                            </tr>
                                            <tr>
                                                <td style="float: left;margin-left: 300px;">Address</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        <div id="bottom_div">
                            <div id="bottom_div_text">
                                <a href="#">Purbodesh Jobs Training Program in January 2014</a>
                            </div>
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>
