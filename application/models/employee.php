<?php

/**
 * Description of employee
 *
 * @author RIfat
 */
class employee extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function log_me_in($user, $pass) {
        $q = $this->db->get_where('job_employee_login', array('user_name' => $user, 'password' => $pass));
        if ($q->num_rows() > 0) {
            $res = $q->row_array(1);
            $query = $this->db->get_where('job_resume_personal', array('id' => $res['user_id']));
            $personal_info = $query->row_array(1);
            $ses_array = array(
                'user_id' => $res['user_id'],
                'user_name' => $res['user_name'],
                'name' => $personal_info['name']
            );

            $this->session->set_userdata($ses_array);
            return true;
        }
        else
            return false;
    }

    function logout() {
        $this->session->unset_userdata();
        session_destroy();
        redirect('/mainpage/view_mainpage');
    }

}

?>
