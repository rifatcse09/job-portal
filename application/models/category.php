<?php

class Category extends CI_Model {

    public $num_rows = 0;

    function __construct() {
        parent::__construct();
    }

    function select_all_category() {
        $query = $this->db->get_where('job_category', array('is_active' => 1));
        return $query->result_array();
    }

    function get_category($cat_id) {
        $query = $this->db->get_where('job_category', array('category_id' => $cat_id));
        return $query->row_array();
    }

    function select_category_for_plain_list() {
    
//        $q = $this->db->query('Select job_category.*, IFNULL(total.count,0) count FROM job_category left 
//            join (select category_id,Count(id) count 
//            from emp_new_job, emp_requirement_s2
//            where id = emp_job_id and deadline >= \'' . date('Y-m-d') . '\' 
//            and emp_new_job.is_active = 1 and (is_blue_worker_job != \'y\' OR is_blue_worker_job is null)
//            group by category_id) total on total.category_id = job_category.category_id');
              $q = $this->db->query('Select job_category.*, IFNULL(total.count,0) count FROM job_category left 
            join (select category_id,Count(emp_new_job.id) count 
            from emp_new_job, emp_requirement_s2,emp_voucher
            where emp_new_job.id = emp_requirement_s2.emp_job_id and emp_new_job.id=emp_voucher.job_id and
			deadline >= \'' . date('Y-m-d') . '\' 
            and emp_new_job.is_active = 1 and emp_voucher.is_publish=1
			and (is_blue_worker_job != \'y\' OR is_blue_worker_job is null)
            group by category_id) total on total.category_id = job_category.category_id
');
        return $q->result_array();
    }

    function get_total_jobs($cat_id) {
        $n = $this->db->get_where('emp_new_job', array('category_id' => $cat_id, 'deadline >=' => date('Y-m-d')));
        return $n->num_rows();
    }

    function jobs_by_category($cat_id, $limit, $start) {
        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
		$this->db->join('emp_voucher', 'emp_new_job.id = emp_voucher.job_id');
        $where = "(is_blue_worker_job != 'y' OR is_blue_worker_job is null)";
        $this->db->where(array('category_id' => $cat_id, 'deadline >=' => date('Y-m-d'), 'emp_new_job.is_active' => 1, 'emp_voucher.is_publish' => 1));
        $this->db->where($where);
        $this->db->order_by('posting_date desc');
        $this->db->limit($limit, $start);
        $q = $this->db->get();

        $n = $this->db->get_where('emp_new_job', array('category_id' => $cat_id, 'deadline >=' => date('Y-m-d')));
        $this->num_rows = $n->num_rows();

        return $q->result_array();
    }

    function job_in_ctg($limit, $start) {
        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
        $this->db->where(array('job_location LIKE' => '%10%', 'deadline >=' => date('Y-m-d'), 'emp_new_job.is_active' => 1));
        $this->db->limit($limit, $start); //for pagination
        $q = $this->db->get();

        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
        $this->db->where(array('job_location LIKE' => '%10%', 'deadline >=' => date('Y-m-d'), 'emp_new_job.is_active' => 1));

        $n = $this->db->get();
        $this->num_rows = $n->num_rows();

        return $q->result_array();
    }
    ///test for dhaka job
    function job_in_dhaka($limit, $start) {
        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
        $this->db->where(array('job_location LIKE' => '%14%', 'deadline >=' => date('Y-m-d'), 'emp_new_job.is_active' => 1));
        $this->db->limit($limit, $start); //for pagination
        $q = $this->db->get();

        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
        $this->db->where(array('job_location LIKE' => '%14%', 'deadline >=' => date('Y-m-d'), 'emp_new_job.is_active' => 1));

        $n = $this->db->get();
        $this->num_rows = $n->num_rows();

        return $q->result_array();
    }
    
    
    ///test for dhaka job
    //test part
    
    function part_time_job($limit, $start) {
        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
        $this->db->where(array('job_type LIKE' => '%Part time%', 'deadline >=' => date('Y-m-d'), 'emp_new_job.is_active' => 1));
        $this->db->limit($limit, $start); //for pagination
        $q = $this->db->get();

        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
        $this->db->where(array('job_type LIKE' => '%Part time%', 'deadline >=' => date('Y-m-d'), 'emp_new_job.is_active' => 1));

        $n = $this->db->get();
        $this->num_rows = $n->num_rows();

        return $q->result_array();
    }
    
    //test part
    
    
    
    

    function job_in_blue($limit, $start) {
        $offset = 0;
        if ($start > 0)
            $offset = $start . ', ';
        $q = $this->db->query('SELECT * FROM emp_new_job join  emp_requirement_s2 on emp_new_job.id= emp_requirement_s2.emp_job_id join
                                 job_category on job_category.category_id=emp_new_job.category_id 
                                 where emp_new_job.is_blue_worker_job="y" and emp_new_job.is_active=1');
        $this->num_rows = $q->num_rows();
        $query = $this->db->query('SELECT * FROM emp_new_job join  emp_requirement_s2 on emp_new_job.id= emp_requirement_s2.emp_job_id join
                                 job_category on job_category.category_id=emp_new_job.category_id 
                                 where emp_new_job.is_blue_worker_job="y" and emp_new_job.is_active=1 limit ' . $offset . $limit);


        return $query->result_array();
    }

    function job_deadline() {
        $to_date = date('Y-m-d');
        $tomorrow = strtotime('+1 day', strtotime($to_date));
        $tomorrow = date('Y-m-d', $tomorrow);
        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id = emp_requirement_s2.emp_job_id');
        $this->db->where(array('deadline' => $tomorrow, 'emp_new_job.is_active' => 1));
        $q = $this->db->get();
        return $q->result_array();
    }

    function keyword_search($key, $limit, $start) {
        $offset = 0;
        if ($start > 0)
            $offset = $start . ', ';
        $q = $this->db->query('SELECT * FROM emp_new_job join emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id where (emp_new_job.job_title like \'%' . $key . '%\' or emp_new_job.company_name like \'%' . $key . '%\' or emp_requirement_s2.job_type like \'%' . $key . '%\' or emp_requirement_s2.edu_qualification like \'%' . $key . '%\' or job_location like \'%' . $key . '%\') and emp_new_job.deadline >= \'' . date('Y-m-d') . '\' and emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 limit ' . $offset . $limit);
        //echo $this->db->last_query();

        $n = $this->db->query('SELECT * FROM emp_new_job join emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id where (emp_new_job.job_title like \'%' . $key . '%\' or emp_new_job.company_name like \'%' . $key . '%\' or emp_requirement_s2.job_type like \'%' . $key . '%\' or emp_requirement_s2.edu_qualification like \'%' . $key . '%\' or job_location like \'%' . $key . '%\') and emp_new_job.deadline >= \'' . date('Y-m-d') . '\' and emp_new_job.is_active=1 and emp_requirement_s2.is_active=1');
        $this->num_rows = $n->num_rows();

        return $q->result_array();
    }

    function category_search($cat_id, $level, $location, $nature, $posted_within,$deadline_within, $limit, $start) {
        $offset = 0;
        if ($start > 0)
            $offset = $start . ', ';
		
		
        $date = strtotime('-' . $posted_within . ' day', strtotime(date('Y-m-d')));
        $post_date = date('Y-m-d', $date);
     
        
        $date1 = strtotime('+' . $deadline_within . ' day', strtotime(date('Y-m-d')));
	
        $deadline = date('Y-m-d', $date1);
	if($deadline_within=='')$date_query="emp_new_job.deadline >='".date('Y-m-d')."'";
		else
		$date_query="emp_new_job.deadline between '".date('Y-m-d'). "' and " . "'$deadline'";
		
		if($posted_within!='')$date_query.="and posting_date >= '$post_date'";
		
        $query = 'SELECT * FROM emp_new_job join job_category on emp_new_job.category_id= job_category.category_id join 
            emp_requirement_s2 join emp_voucher on emp_new_job.id=emp_requirement_s2.emp_job_id and emp_new_job.id = emp_voucher.job_id where '.$date_query. ' and emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 and emp_voucher.is_publish = 1 and
            (is_blue_worker_job != \'y\' OR is_blue_worker_job is null)';
        
		
        if (!empty($cat_id)) {
            $query .= ' and job_category.category_id =  ' . $cat_id;
        }

        if (!empty($level)) {
            $query .= ' and emp_requirement_s2.job_level like \'%' . $level . '%\' ';
        }
		if (!empty($location)) {
            $query .= ' and emp_requirement_s2.job_location = '.$location;
        }
		
        if (!empty($nature)) {
            $query .= ' and emp_requirement_s2.job_type like \'%' . $nature . '%\' ';
        }

        $n = $query;

//        $q = $this->db->query('SELECT * FROM emp_new_job join job_category on emp_new_job.category_id= job_category.category_id join 
//            emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id where job_category.category_id =  ' . $cat_id . '  and emp_new_job.deadline >= \'' . date('Y-m-d') . '\' and 
//                emp_requirement_s2.job_level like \'%' . $level . '%\' and  emp_requirement_s2.job_type like \'%' . $nature . '%\' and  
//                    emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 and (is_blue_worker_job != \'y\' OR is_blue_worker_job is null) limit ' . $offset . $limit);
//
//        $n = $this->db->query('SELECT * FROM emp_new_job join job_category on emp_new_job.category_id= job_category.category_id join 
//            emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id where job_category.category_id =  ' . $cat_id . '  and emp_new_job.deadline >= \'' . date('Y-m-d') . '\' and
//                emp_requirement_s2.job_level like \'%' . $level . '%\' and  emp_requirement_s2.job_type like \'%' . $nature . '%\' and  
//                    emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 and (is_blue_worker_job != \'y\' OR is_blue_worker_job is null)');
       //echo $query;
        $q = $this->db->query($query);
        $this->num_rows = $q->num_rows();
        $query .= ' limit ' . $offset . $limit;

       // $q = $this->db->query($query);
        //echo $this->db->last_query();
        return $q->result_array();
    }

    function company_search($alp) {
        $q = $this->db->query('SELECT * FROM emp_acc_info WHERE  emp_acc_info.company_name like \'' . $alp . '%\' and emp_acc_info.is_active=1');

        return $q->result_array();
    }

    function company_job($comp_id, $limit, $start) {
        $offset = 0;
        if ($start > 0)
            $offset = $start . ', ';

        $q = $this->db->query('SELECT * FROM emp_acc_info JOIN emp_new_job ON emp_acc_info.id = emp_new_job.company_id 
                               JOIN emp_requirement_s2 ON emp_new_job.id = emp_requirement_s2.emp_job_id 
                               join job_category on job_category.category_id=emp_new_job.category_id WHERE  emp_acc_info.id=' . $comp_id . '  and emp_new_job.deadline >= \'' . date('Y-m-d') . '\' and  
                               emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 limit ' . $offset . $limit);

        $n = $this->db->query('SELECT * FROM emp_acc_info JOIN emp_new_job ON emp_acc_info.id = emp_new_job.company_id 
                               JOIN emp_requirement_s2 ON emp_new_job.id = emp_requirement_s2.emp_job_id 
                               join job_category on job_category.category_id=emp_new_job.category_id WHERE  emp_acc_info.id=' . $comp_id . '  and emp_new_job.deadline >= \'' . date('Y-m-d') . '\' and  
                               emp_new_job.is_active=1 and emp_requirement_s2.is_active=1');
        $this->num_rows = $n->num_rows();
        //echo $this->db->last_query();
        return $q->result_array();
    }

    function new_job() {
        $q = $this->db->query('select * from emp_new_job join emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id join 
                               
                               job_category on emp_new_job.category_id=job_category.category_id where emp_new_job.posting_date=\'' . date('Y-m-d') . '\' and  
                               emp_new_job.is_active=1 and emp_requirement_s2.is_active=1');
        return $q->result_array();
    }

}

?>