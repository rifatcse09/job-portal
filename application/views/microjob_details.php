<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Micro Job Topics</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/same.css" />

<!--        <style type="text/css">
            #content_container
            {
                width: 100%;
                background: #f0f0f0;
            }
            #training_header
            {
                float:left;
                width:600px;
                margin-left: 10%;
                margin-bottom: 20px;
            }
            #content
            {
                float: left;
                width: 80%;
                margin: 0% 10%;
                border-top: 0px solid #d7d7d7;
                border-right: 2px solid #d7d7d7;
                border-left: 2px solid #d7d7d7;
                border-bottom: 2px solid #d7d7d7;
                border-radius: 5px;
                background: #fff;
            }
            #main_box
            {
                float: left;
                width: 99.3%;
                border: 5px solid #0099ff;
                margin-top: -1%;
/*                margin-top: 40px;*/
            }
            #header_box
            {
                float: left;
                width: 100%;
                height: 52px;
                background: #0099ff;
            }
            #header_box_training
            {
                float: left;
                width: 100%;
                height: 32px;
/*                background: #000;*/
                margin-top: 10px;
            }
            #training_navigation
            {
                float:left;
                margin-left:10px;
            }
            #training_navigation ul
            {
                margin: 0;
                padding: 0;
            }

            #training_navigation ul li
            {
                display: inline;
            }
            #training_navigation li a
            {
                display: block;
                float: left;
                padding:6px 10px;
                color: #FFFFFF;
                text-decoration: none;
/*                border-right: 1px solid #fff;*/
/*                background:#000;*/
            }
            #training_navigation li a:hover {background:#a5090a; }
            #training_container_header
            {
                float: left;
                width: 60%;
                height: 30px;
                background: #f0f0f0;
                margin-top: 10px;
            }
            #training_container_header p
            {
                color: #000;
                font-size: 15pt;
                text-align: justify;
                font-family: arial;
                margin-top: 2px;
                margin-left: 20px;
            }
            #training_container
            {
                float: left;
                width: 100%; 
                background: #fff;
            }

            #training_content
            {
                float: left;
                width: 100%;
            }
            #training_content p
            {
                text-align: left;
            }
            #img
            {
                padding:0px 20px 6px 0px;
                margin-top: 0px;
                clear:both;}
            #footer_last_navigation
            {
                float:right;
                width:100%;
                margin-top: 5px;
                margin-right: 10%;
                margin-bottom:5%;
             
            }
            #footer_last_navigation ul
            {
                margin: 0;
                padding: 0;
            }
            #footer_last_navigation ul li
            {
                display: inline;
            }
            #footer_last_navigation li a
            {
                display: block;
                float: right;
                padding:0px 10px;
                color: #b12929;
                text-decoration: none;
                border-right: 1px solid #b12929;
                font-family: euphemia;
                font-size: 7pt;
            }
            #footer_last_navigation li a:hover {color:#d6a201; }
            #footer_last
            {
                float: right;
                width: 100%;
                height: 35px;
                background: #3d3729;
                margin-top: 20px;
                margin-bottom: 30px;
            }
        </style>-->
    </head>
    <body>
        <div id="content_container">
                <div id="training_header">
                    <img src="<?= base_url() ?>images/pglogo.png"/>
                </div>              
            <div id="content">

                <div id="main_box">
                    <div id="header_box">

                        <div id="header_box_training">
                            <div id="training_navigation">
                                <?= $this->load->view("jobsbycategory_menu"); ?>
                            </div>
                        </div>

                    </div>
                    <div id="training_container">

                        <div id="training_content" style="margin-bottom: 10px">
                           <? //include 'database.php';
                             $id=$_GET['id'];
                             $sql="select * from micro_job where id='$id' ";
                             $result=  mysql_query($sql);
                             $row=  mysql_fetch_array($result);
                           
                           
                           ?>
                            <div style='float:left;margin-left:20px'>
                                <table id="table" style="width:98%;margin-top: 0px" align='center'>
                                     
                                    <tr>
                                        <td valign="top" style='font-family:century Gothic;font-weight: bold;width:150px;padding-bottom: 20px'>Job Title</td>
                                       <td valign="top">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $row['job_title'];?></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       <td valign="top" style='font-family:century Gothic;font-weight: bold;width:100px;padding-bottom: 20px'>Subject</td>
                                       <td valign="top">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $row['subject'];?></td>
                                       
                                    </tr>            
                                     <tr>
                                       <td valign="top" style='font-family:century Gothic;font-weight: bold;width:100px'>Description</td>
                                       <td valign="top">:</td>
                                       <td style='font-family:century Gothic;text-align: justify'><?= $row['description'];?></td>
                                       
                                    </tr>
                                    
                                      <tr>
                                       <td valign="top" style='font-family:century Gothic;font-weight: bold;width:100px;padding-bottom: 20px;padding-top: 20px'>Location</td>
                                       <td valign="top" style="padding-bottom: 20px;padding-top: 20px">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px;padding-top: 20px'><?= $row['location'];?></td>
                                       
                                    </tr>
                                    <tr>
                                       <td  valign="top" style='font-family:century Gothic;font-weight: bold;width:100px;padding-bottom: 20px;'>Budget</td>
                                       <td valign="top">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px;'><?= $row['budget'];?></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       <td valign="top" style='font-family:century Gothic;font-weight: bold;width:100px;padding-bottom: 20px;'>Start Date</td>
                                       <td valign="top">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px;'><?= $row['start_date'];?></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       <td valign="top" style='font-family:century Gothic;font-weight: bold;width:100px;padding-bottom: 20px;'>Duration</td>
                                       <td valign="top">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px;'><?= $row['duration'];?></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       <td valign="top" style='font-family:century Gothic;font-weight: bold;width:100px;padding-bottom: 20px;'>Special Notes</td>
                                       <td valign="top">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px;'><?= $row['special_notes'];?></td>
                                       
                                    </tr>
                                    
                                     <tr>
                                       <td valign="top" style='font-family:century Gothic;font-weight: bold;padding-bottom: 20px;'>Submitted By</td>
                                       <td valign="top">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px;'><?= $row['submit_by'];?></td>
                                       
                                    </tr>
                                     
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div id="footer_bottom_navigation">
                <?= $this->load->view("footer_bottom_navigation"); ?>
            </div>
         <div id="wrapper_bottom">
             <div style="float:right;margin-right: 20px"><p title="Design & Developed by PentaGems" style="color:#fff">Design & Developed by <a title="PentaGems"  style="text-decoration: none;color:#E6E600;font-weight: bold"target="_blank" href="http://pentagemsbd.com">PentaGems</a></p></div>
                <? //$this->load->view("footer_bottom_navigation.php"); ?>
 </div>
    </body>
</html>
