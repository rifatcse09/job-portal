﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>Tenders and Consulting Opportunities in Bangladesh</title>
        <link rel="stylesheet" href="CSS/site.css" type="text/css" media="screen">
    </head>
    <body>
        <div  id="container">
            <div id="header">
                <? include("Header.php"); ?> 

            </div>
            <? include("menu.php"); ?>
            <? include("animation.php"); ?>
            <div id="content-container">
		<div id="registration">
		   <p>Tenders and Consulting Opportunities in Bangladesh</p>
	      <form action="insert/registration_insert.php" method="post">
		   <table>
				  <tr>
					<td width="500" valign="top" style="line-height:25px"> <table>
		        <tr>
				<td>Contact Person</td>
				<td>:</td>
				<td><input type="text" name="name" /></td>
			  </tr> 
			   <tr>
				<td>Designation</td>
				<td>:</td>
				<td><input type="text" name="designation" /></td>
			  </tr>
			   <tr>
				<td>Organization</td>
				<td>:</td>
				<td><input type="text" name="organization" /></td>
			  </tr>
			   <tr>
				<td>Mobile</td>
				<td>:</td>
				<td><input type="text" name="mobile" /></td>
			  </tr>
			   <tr>
				<td>Address</td>
				<td>:</td>
				<td><textarea name="address"></textarea></td>
			  </tr>
			  <tr>
				<td>Email ID</td>
				<td>:</td>
				<td><input type="text" name="email" /></td>
			  </tr>
			  <tr>
				<td>Password</td>
				<td>:</td>
				<td><input type="password" name="password" /></td>
			  </tr>
			  	<td>Service Plan</td>
				<td>:</td>
				<td><input type="radio" name="service_plan" />1 Month<input type="radio" name="service_plan" />3 Month<input type="radio" name="service_plan" />6 Month<input type="radio" name="service_plan" />1 Year</td>
			  </tr>
       </table></td>
	            <td valign="top"><table>
				  <tr>
					<td><p style="color:#006699 ; font-size:18; font-weight:bold">Important Notice</p>
				   <p>Category selection means from any organization or type or sector, this is very specific business area.</p>
				  <p>Selection of any Organization means tender notice of all categories published from the organization.</p>
				  <p>Selection of Type means all organizations, sectors and categories.</p>
				  <p>Selection of Sector means all organizations, types and categories.</p>
				  <p>Selection of District means all notices published from the district by any organizations, types, sectors or categories.</p>
				  <p>Choose your preferred mailing criteria. On selection each item you will get E-mail notification matching your area of selection automatically.</p></td>
				  </tr>
				</table>
				</td>
				 </tr>
				  <tr style="background:#191b1a; width:990px; color:#FFFFFF; font-weight:bold">
					<td colspan="2" align="center">For multiple selection press "Ctrl" and click on the Item to be selected</td>
				  </tr>
				    <tr>
				<td colspan="2" align="center"><input type="submit" value="Submit" style="border:#191b1a solid 1px; border-radius:10px; background:#191b1a; font-weight:bold padding:5px 20px; color:#FFFFFF" /></td>
			  </tr>
			</table>
		  </form>
		</div>
	</div>
        </div>
        <div id="foot-container">
<? include("footer.php") ?>
        </div>    
    </body>
</html>
