﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>Tenders and Consulting Opportunities in Bangladesh</title>
        <link rel="stylesheet" href="CSS/site.css" type="text/css" media="screen">
    </head>
    <body>
        <div  id="container">
            <div id="header">
                <? include("Header.php"); ?> 

            </div>
            <? include("menu.php"); ?>
            <? include("animation.php"); ?>
            <div id="content-container">
		<div id="registration">
		   <form  action="insert/tender_insert.php" enctype="multipart/form-data" method="post">
<table cellspacing="0" cellpadding="0" width="100%">
            
                <tr>
                    <td style="text-align: center; background:#191b1a; border:solid 1px; border-radius:10px 10px 0px 0px; font-weight:bold; color:#FFFFFF" colspan="2"> 
                     Submit  Limited Tender Notice For Publication on Website<br />
                                    </td>

                </tr>
                <tr>
                  <td colspan="2" style="text-align: right; width: 249px;"><div align="justify">
                    <blockquote>
                      <p><span class="style6">You may also online host  your tender notice in our website <a href="http://www.bdtender.com/">www.bdtender.com</a> as online&nbsp;publication for the  procurement of goods&nbsp;from suppliers. At present Pan Pacific Sonargoan and  some other organizations are publishing here their notices.</span></p>
                      <p><span class="style6">This is a  new&nbsp;idea&nbsp;in our country. This may help you tremendously for the  procurement of goods from suppliers. After online hosting your purchase notice,  thousands of suppliers will view your notice and submit quotation to you  immediately. You&nbsp;can do a good purchase just&nbsp;by choosing the right  supplier for the supply of&nbsp;right goods. By this way you can also  reduce&nbsp;the expenditure, which&nbsp;means save the money for your organization  or for nation.&nbsp;You can also see the number of visitors for your online  hosted tender notice.</span></p>

                      <p><strong>The objectives are:</strong></p>
                      <ol>
                        <li class="style10">Support to Build Digital Bangladesh </li>
                        <li class="style10">Wide circulation of the tender notice</li>
                        <li class="style10">Less expensive than newspaper advertisement</li>
                        <li class="style10">Quick advertisement</li>

                        <li class="style10">Advertise limited tender notice also</li>
                        <li class="style10">Save money during purchase, just choose a good supplier and       good price or competitive price </li>
                      </ol>
                    </blockquote>
                  </div>
                    <ol start="1" type="1">
                  </ol>                    
                    <blockquote>

                      <p align="left" class="style1"><strong>Service Charge: </strong>  Taka 5,000.00 per tender notice of maximum A4 size online   hosting up to the  tender closing date.                    </p>
                  </blockquote></td>
    </tr>
                <tr>
                <td style="text-align: right; width: 249px;"></td>
                <td style="color: #FF0000"><div align="left">Must put Data in * Field</div></td>
                </tr>

                <tr>
                    <td class="style4" style="text-align: right; width: 249px;"> Limited Tender Notice Title: </td><td class="style4">
<input type="text" name="title" style="width:300px" />
                                                    <span style="color: #FF3300">* </span>Ex. Computer Purchase Notice</td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 249px;"> Provider&#39;s Name:</td><td>

                    <input type="text" name="ten_name" style="width:300px" />
                                                    <span style="color: #FF3300">* </span>Ex. Karim 
                    Rahman</td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 249px;"> Designation:</td><td>
                    <input type="text" name="designation" style="width:300px" />
                                                    <span style="color: #FF3300">* </span>Ex. 
                    Purchase Manager</td>

                </tr>
                <tr>
                    <td style="text-align: right; width: 249px;"> Email Address:</td><td>
                    <input type="text" name="email" style="width:300px" />
                    <span style="color: #FF0000">*</span>
                    &nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: right; width: 249px;"> Contact Phone No.:</td><td>
                    <input type="text" name="phone" style="width:300px" />
                    <span style="color: #FF0000">* </span>Ex. 02-9123059 or Mobile Number</td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 249px;"> Address:</td><td>

                    <textarea name="address" style="width:300px"></textarea>
                    <span style="color: #FF0000">* </span>EX. House # 514, Road # 5, Dhanmondi.</td>
                </tr>
                <tr>
                    <td style="text-align: right; width: 249px;"> Attachment of the Tender Notice:</td><td valign="top">
                  
					<input type="hidden" name="MAX_FILE_SIZE" value="104857600">
	                <input type="file" name="userfile" id="userfile" />

                    (Attach here only MS Word or PDF file)<span style="color: #FF0000">*</span></td>
                </tr>
                <tr>
                <td style="text-align: right; width: 249px;">Message:</td>
                <td>
                <textarea name="message" style="width:405px; height:99px"></textarea>
                                                    <span style="color: #FF0000">* </span>Ex. Regarding Tender</td>

                </tr>
                <tr>
                    <td style="width: 249px; text-color:red">&nbsp; </td><td>
                                        </td>
                </tr>
                <tr>
				<td colspan="2" align="center"><input name="upload" type="submit" value="Submit" id="upload" style="border:#191b1a solid 1px; border-radius:10px; background:#191b1a; font-weight:bold padding:5px 20px; color:#FFFFFF" /></td>
			  </tr>
  </table>
</form>
		</div>
	</div>
        </div>
        <div id="foot-container">
<? include("footer.php") ?>
        </div>    
    </body>
</html>
