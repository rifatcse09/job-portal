<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<title>Job Category</title>
<head>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/job.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/style.css" />
</head>

<div id="content_container">
    <div id="job_category_header">
        <?= $this->load->view("jobsbycatagories_header.php"); ?>
    </div>
    <div id="content_all">
    <div id="job_category_navigation">
                <?= $this->load->view("jobsbycategory_menu.php"); ?> 
     </div>
        
    <div style="float: left;width: 100%">    
        <div id="content_content" style="width: 80%">
           
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr> 
                    <td height="15" class="BDJtabLinkSelected" style="text-align:center">&nbsp;</td>
                </tr>

                <tr> 
                    <td height="30" class="BDJtabLinkSelected" style="text-align:center;font-size: 15px">
                        <a style='color:#FD7700' href='<?= site_url()?>/jobcategory/companysearch?qjobnature=A'>&nbsp;A&nbsp;|&nbsp;</a>
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=B'>&nbsp;B</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=C'>&nbsp;C</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=D'>&nbsp;D</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=E'>&nbsp;E</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=F'>&nbsp;F</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=G'>&nbsp;G</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=H'>&nbsp;H</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=I'>&nbsp;I</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=J'>&nbsp;J</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=K'>&nbsp;K</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=L'>&nbsp;L</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=M'>&nbsp;M</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=N'>&nbsp;N</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=O'>&nbsp;O</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=P'>&nbsp;P</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=Q'>&nbsp;Q</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=R'>&nbsp;R</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=S'>&nbsp;S</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=T'>&nbsp;T</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=U'>&nbsp;U</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=V'>&nbsp;V</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=W'>&nbsp;W</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=X'>&nbsp;X</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=Y'>&nbsp;Y</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=Z'>&nbsp;Z</a>&nbsp;|&nbsp;

                    </td>
                </tr>
            </table>


            <div id="page_index">
            </div>

            <div id="job_type_result_curve">

            </div>

            <div id="job_type_result">
                <div id="job_type_pan">
                  
                </div>
            </div>
            <div id="job_type_container">
                <div id="job_type_content">
                    <table width="100%">

                        <?
                        if ($jobs) {
                            foreach ($jobs as $job) {
                                ?>
                                <tr>
                                    <td style="background-color:  #dfdddd;border:1px solid #c8d7dc"  height="29" class="body"><a style="font-family: Tahoma;font-size: 16px" href="<?= site_url()?>/jobcategory/company_jobs?id=<?= $job['id']?>"><?= $job['company_name'] ?></a></td>
                                  
                                </tr>
                                <?
                            }
                        } else {
                            echo '<tr>
                                    <td>No Data Available</td>
                                </tr>';
                        }
                        ?>
                    </table>
                </div>

            </div>
            <div id="page_index">

            </div>
        </div
        
        </div>   
        <div id="content_add" style="margin-left: 0px;text-align: center">
            <?
            include("database.php");
            $sql = "select * from  advertisement where advertisement_type='jobcategory2' ORDER BY add_id DESC LIMIT 0,3";
            $data = mysql_query($sql);
            while ($r = mysql_fetch_array($data)) {
                ?>
                <tr>
                    <td valign="top"><a href="<? echo $r['website']; ?>" target="_blank"><img src="<?= base_url() ?>adminpanel/advertise_images/<? echo $r['name']; ?>" width="130" height="120" style="margin:2px 0px 0px 0px;" /></a></td>
                </tr>
                <?
            }
            ?>
        </div>
    </div>
  </div>

</div>
    <div id="footer_bottom_navigation">
        <? $this->load->view("footer_bottom_navigation.php"); ?>
    </div>

 <div id="wrapper_bottom">
             <div style="float:right;margin-right: 20px"><p title="Design & Developed by PentaGems" style="color:#fff">Design & Developed by <a title="PentaGems"  style="text-decoration: none;color:#E6E600;font-weight: bold"target="_blank" href="http://pentagemsbd.com">PentaGems</a></p></div>
                <? //$this->load->view("footer_bottom_navigation.php"); ?>
 </div>
<!--    <div id="footer_last"></div>
    <div id="footer_last_bottom"></div>-->

</html>