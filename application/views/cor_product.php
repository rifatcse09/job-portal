<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Corporate Products Azadijobs</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/same.css" />
		<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
		<script>
		$(document).ready(function() {
    $('[id^=detail-]').hide();
    $('.toggle').click(function() {
        $input = $( this );
        $target = $('#'+$input.attr('data-toggle'));
        $target.slideToggle();
    });
});
		
		
		</script>
    </head>
    <body>
        <div id="content_container">
            <div id="training_header">
                <a href="<?= base_url() ?>"><img src="<?= base_url() ?>images/pglogo.png"/></a>
            </div>              
            <div id="content">

                <div id="main_box">
                    <div id="header_box">
                   
                        <div id="header_box_training">
                            <div id="training_navigation" style="margin-bottom:50px;">
                                <?= $this->load->view("mainpage_menu"); ?>
                            </div>
                        </div>
							<div style="margin:70px;border:3px solid #7AA3CC;border-radius:10px;padding:20px;background:#B2CCCC;">
								<h2 style="font-family:cambria">Corporate Product/Services</h2>
								<h3 style="font-family:cambria">Online Recruitment Services of Azadijobs.com</h3>
								<p style="font-family:calibri;text-align:justify;">Azadijobs.com the only job site in Chittagong established with the aim to provide the best online recruit service to the local Corporate House and Multinational Organization who has the operation in the port city. Our different type of service will help the empoyer finding the right candidates in the offered positions: Below the list of services are mentioned:</p>
								<br>
								<h4 style="font-family:cambria">General Service</h4>
								<h5 style="font-family:cambria">Online Job Vacancy Announcement at Classified/Category Section of www.azadijobs.com</h5>
								<p style="font-family:calibri;text-align:justify;">In this section, employers/recruiters will have the opportunity to post job in different category. As soon as employers post a job in www.azadijobs.com, the job becomes accessible by the job seeker. What the employer needs to do is- just log on www.azadijobs.com and simply create a employer account of the organization and post the job under the company account.
Features of the service
The posted job vacancies will stay for maximum 30 days long unless the posted job vacancies ending date below the maximum days.
There are three ways an employers receive job application through online. These are online account of the employer, email and traditional methds like Currier, mail, fax etc.
Employer will be able to view, short list, print application/resume if the employer want to get it through online.
Service Fee: BDT 500 (For each job position) </p>
										
										<br>
										<br>
										<h3 style="font-family:cambria">Online CV bank access</h3>
										<p style="font-family:calibri;text-align:justify;">
										Being a online CV bank accesser, you will be able to access all the CV in www.azadijobs.com. There are number of ways you can search and get the desire applicant for you. Searching can be done in the following ways.
										<ul style="font-family:calibri;text-align:justify;">
											<li style="font-family:calibri;text-align:justify;">
											Personal Information
												<ul>
													<li>Gender</li>
													<li>Age</li>
													<li>Level of Education</li>
												</ul>
											
											</li>
											<li>
											Academic information
												<ul>
													<li>Level</li>
													<li>Subject/Degree name</li>
													<li>Institute name</li>
												</ul>
											
											
											</li>
											<li>
											Experience Information
												<ul>
													<li>Year of experience</li>
													<li>Having experience in any type of business organization</li>
													<li>Having experience in any type work</li>
													<li>Having specialization any type of skill</li>
												</ul>
											
											</li>
											<li>
												Service Fee:
													<ul>
													<li>BDT: 7000 (For One Year)</li>
													<li>BDT: 5000 (Six Months)</li>
													
												</ul>
												
											</li>
											<li>The corporate member of azadijobs.com now enjoy the service with complementary.</li>
										</ul>
										
										
										</p>
										<h3 style="font-family:cambria">Corporate Membership</h3>
										<p style="font-family:calibri;text-align:justify;">
										Being a corporate member, you will be able to post unlimited number of jobs in different category. There will be no restriction whatsoever and posting job vacancies will be visible as soon as the corporate employer posts a job. There are number of facilities in www.azadijobs.com for corporate member.
										</p>
										<ul style="font-family:calibri;text-align:justify;">
											<li>Post unlimited job vacancy</li>
											<li>View applicant/resume</li>
											<li>Print applicant</li>
											<li>Email them if necessary</li>
											<li>make short list (By using different option, data & keyword)</li>
											<li>Modify your account information</li>
											<li>Modify Job vacancy</li>
											<li>Repost Job vacancy</li>
											<li>Payment status</li>
											<li>Service Fee
												<ul><li>BDT: 10000 (For One Year)</li></ul>
											</li>
										</ul>
										<br>
										<br>
										<h2 style="font-family:cambria">Premium Service</h2>
								<h3 style="font-family:cambria">Premium advertisement at the Priority Jobs section of www.azadijobs.com</h3>
								<h4 style="font-family:cambria">Priority Jobs</h4>
								<p style="font-family:calibri;text-align:justify;">
								Priority jobs announcement make job advertisement stand out from hundreds of job announcements published in azadijobs.com. It is a unique service that offer job publish in both priority and category section. This might be the best way to promote your respective organization. 
								</p>
								<h4 style="font-family:cambria">Features</h4>
											<ul style="font-family:calibri;text-align:justify;">
											<li>Job announcement will be published with company logo and name in front page.</li>
											<li>Priority jobs will be displayed for maximum 15 days unless job ending date below the maximum days.</li>
											<li>We offer a fully dynamic customize job detail page for priority job where logo and organization name at the home page will be linked.</li>
											<li>Priority jobs announcement will also be displayed in category for maximum 30 days unless post ending date below the maximum days.</li>
											<li>Service Fee:
												<ul>
													<li>BDT: 2000 (For Single job)</li>
													<li>BDT: 1500 (for each job if two job posted at a time)</li>
													<li>BDT: 1000 (for each job if three or more jobs posted at a time)</li>
												</ul>
											</li>
											</ul>
								<h3 style="font-family:cambria">On Demand Resume supply</h3>
								<p style="font-family:calibri;text-align:justify;">
								This service is designed for employer organization who would require Cvs of qualified candidates on very urgent basis. A pool of CV s will be delivered within 48 hours of receiving work order. Under this service package, azadijobs.com panel of recruitment experts will search from the CV bank for a pool of qualified candidates that match the required criteria provided by the employer organization. The CV s will then be delivered to the client in a soft copy or hard copy format within 48 hours. 
*** This service will resume soon.
								
								
								</p>
									<br>
									<h3 style="font-family:cambria">Banner Advertisement feature at different pages of www.azadijobs.com</h3>
									<p style="font-family:calibri;text-align:justify;">
									Online advertisement would be the great way to promote an organization nowadays. In light of that www.azadijobs.com are giving the opportunity to client organization to place a banner advertisement at any page of www.azadijobs.com for a certain period. The banner advertisement can be linked with the corporate web site of the client organization.
Banner Advertisement at the home page of www.azadijobs.com
In the home page of azadijobs.com offer threes different type of banner advertisement in three different position.
									</p>
									<ul style="font-family:calibri;text-align:justify;">
										<li>	Top Banner(2 fixed banner) 	
											<ul>
												<li>Banner size: 450x60px</li>
												<li>Banner file size: 25kb-40kb</li>
												<li>Price: BDT: 15000 (Per Month)</li>
											</ul>
										</li>
										
										<li>	Right Side Banner Full (Unlimited)
											<ul>
												<li>Banner size: 250x60px</li>
												<li>Banner file size: 15-20 kb</li>
												<li>Price: BDT: 10000 (Per Month)</li>
											</ul>
										</li>
										
										<li>	Right side Banner Half(Unlimited) 
											<ul>
												<li>Banner size: 120x60px</li>
												<li>Banner file size: 15kb</li>
												<li>Price: BDT: 8000 (Per Month)</li>
											</ul>
										</li>
									</ul>
									<p style="font-style:italic;font-family:calibri;text-align:justify;">Note: Animated banner advertisement (3 to 4 frames) and Banner will be designed by azadijobs.com without additional cost.

									<br>
									<br>
									** Modification Charge is applicable.
									
									</p>
								
								
							</div>
						
            
						
						
						
						</div>
						
                    </div>
                    
                </div>
            </div>

       
         
    </body>
</html>
