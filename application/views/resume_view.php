<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
    <head>
        <title>Purbodeshjobs</title>
        <link rel="stylesheet" type="text/css" href="<?  base_url()?>/mainpage_css/stylesheet_1.css" />
        <style type="text/css">
            body{
                background: #FFF;
            }
        </style>
    </head>
    <body style="font-size: 0.8em">
        
        <? 
          if(isset($id))
          {
              $user_id=$id;
           include 'database.php';
           $query="select * from job_resume_career as career join job_resume_personal as personal join district as d where career.user_id='$user_id' and personal.id='$user_id' and personal.current_loc=District_Id";
           $result=  mysql_query($query);
           $personal=  mysql_fetch_array($result);
           
        ?>
        <div style="width: 100%; border: 1px solid #FFF;">
             <? if($personal['objective']!=''){ ?>
                <table style="width: 100%; border: 0px;">
                    <thead>
                        <tr>
                            <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career Objective</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr><td style="text-align: left;"><?= $personal['objective'] ?></td></tr>
                    </tbody>
                </table>
             <?}?>
        
            <table style="width: 100%; border: 0px;">
                <thead>
                    <tr>
                        <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Personal Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align: left;">Name:</td>
                        <td style="text-align: left;"><?= $personal['name'] ?></td>
                        <td width="126" height="135" rowspan="5"><img src="http://purbodeshjobs.com/employee/photographs/<?=$personal['image']?>" alt="no photo" width="124" height="135" /></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Father's Name:</td>
                        <td style="text-align: left;"><?= $personal['father_name'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Mother's Name:</td>
                        <td style="text-align: left;"><?= $personal['mother_name'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Date of Birth:</td>
                        <td style="text-align: left;"><?= date('d F, Y', strtotime($personal['date_birth'])) ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Gender:</td>
                        <td style="text-align: left;"><? echo $personal['gender'] == 'm' ? 'Male' : 'Female'; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Marital status:</td>
                        <td style="text-align: left;" colspan="2"><? echo $personal['marital_status'] == 'm' ? 'Married' : 'Single'; ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Nationality:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['nationality'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Religion:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['religion'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Present Address:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['present_add'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Permanent Address:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['permanent_add'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Current Location:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['District_Name'] ?></td>
                    </tr>
<!--                    <tr>
                        <td style="text-align: left;">Contact(home):</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['home_phone'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Contact(Mobile):</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['mobile'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Contact(Office):</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['office_phone'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Email:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['email'] ?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Alternate Email:</td>
                        <td style="text-align: left;" colspan="2"><?= $personal['alternate_email'] ?></td>-->
                    </tr>
                </tbody>
            </table>
            <br/>
            
            
            <?
            $sql2="select * from job_employment_history where employee_id='$user_id'";
            $result2=  mysql_query($sql2);
            $row2=  mysql_num_rows($result2);

            if ($row2> 0) {
                ?>
                <table style="width: 100%; border: 0px;">
<!--                    <thead>
                        <tr>
                            <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career Objective</th>
                        </tr>
                    </thead>-->
                    <tbody>
                        <?
                        $i=1;
                      while($history=  mysql_fetch_array($result2)) {
                            ?>

                            <tr>
                                <td align="left">
                                    <span style="font-weight: bold; text-decoration: underline">
                                        <?= ($i++) . '.' . $history['position_held']; ?>
                                        <?= '(' . date('F d, Y', strtotime($history['from'])) . ' - '; ?>
                                        <?= ($history['continuing'] == 'y') ? 'continuing)' : date('F d, Y', strtotime($history['to'])) . ')'; ?>
                                    </span><br/><br/>
                                    <span style="font-weight: bold;">
                                        Company: <?= $history['company_name'] ?>
                                    </span><br/>
                                    <span>
                                        Company Location: <?= $history['company_location'] ?><br/>
                                        Department: <?= $history['department'] ?>
                                    </span><br/>
                                    <span>
                                        <i><b style="text-decoration: underline">Duties/Responsibilities</b></i><br/>
                                        <?
//                                        $duties = explode("\n", $history['responsibilities']);
//                                        foreach ($duties as $duty) {
//                                            echo $duty . '<br/>';
//                                        }
                                        echo $history['responsibilities'];
                                        ?>
                                        
                                    </span>
                                    <br/>
                                      <span style="margin-top:5px">
                                        <b>Area of Experience:</b> <?= $history['area_of_experience'] ?><br/>
                                      
                                    </span><br/>
                                </td>
                            </tr>

                            <?
                        }
                        ?>
                    </tbody>
                </table>
                <?
            }
            ?>
            <br />
            <?
               $sql1="select * from job_academic where employee_id='$user_id' order by level_id desc";
            $result1=  mysql_query($sql1);
            $rows=  mysql_num_rows($result1);
            
            if ($rows > 0) {
                ?>
                <table style="width: 100%; border: 0px;">
<!--                    <thead>
                        <tr>
                            <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career Objective</th>
                        </tr>
                    </thead>-->
                    <tbody>

                        <tr>
                            <td>
                                <table style="margin: 0 auto; border: 0px;">
                                    <thead>
                                        <tr>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Exam Title</th>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Concentration/Major</th>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Institute</th>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Result</th>
                                            <th style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Passing Year</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?
                                        while ($acc=  mysql_fetch_array($result1)) {
                                            ?>
                                            <tr>
                                                <td><?= $acc['degree_title'] ?></td>
                                                <td><?= $acc['major'] ?></td>
                                                <td><?= $acc['institute'] ?></td>
                                                <td><?= $acc['result'] ?></td>
                                                <td><?= $acc['passing_year'] ?></td>
                                            </tr>
                                            <?
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>       

                    </tbody>
                </table>
                <?
            }
            ?>
            <br />
            <?
            
            $sql3="select * from job_resume_career where user_id=' $user_id'";
            $result3=  mysql_query($sql3);
            $rows3=  mysql_num_rows($result3);
            $career=  mysql_fetch_array($result3);
            
            if ($rows3 > 0) {
                ?>

                <table style="width: 100%; border: 0px;">
                    <thead>
                        <tr>
                            <th colspan="2" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Career and Application</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: left;">Looking For:</td>
                            <td style="text-align: left;"><?= $career['looking_for'] ?> Level Job</td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">Available For:</td>
                            <td style="text-align: left;"><?= $career['available_for'] ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">Present Salary:</td>
                            <td style="text-align: left;"><?= $career['present_salary'] ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">Expected Salary:</td>
                            <td style="text-align: left;"><?= $career['expected_salary'] ?></td>
                        </tr>
                        <? 
                        $sql4="select * from job_emp_category join job_category where job_emp_category.category_id = job_category.category_id  and job_emp_category.employee_id='$user_id'";
                        $result4=  mysql_query($sql4);
                        $rows4=  mysql_num_rows($result4);
                      
                      
                            if ($rows4 > 0) { ?>
                                <tr>
                                    <td style="text-align: left;">Preferred Job Category:</td>
                                    <td style="text-align: left;">
                                        <?
                                        $i=1;
                                        while ($cat=  mysql_fetch_array($result4)) {
                                            
                                            if ($i == 1) {
                                                echo $cat['category_name'];
                                            } else {
                                                echo ', ' . $cat['category_name'];
                                            }
                                            $i++;
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <? }
                         ?>
                        <? 
                        
                          $sql5="select * from job_emp_preferred_org join emp_business_type where job_emp_preferred_org.org_id = emp_business_type.business_id  and job_emp_preferred_org.employee_id='$user_id'";
                        $result5=  mysql_query($sql5);
                        $pre_organization=  mysql_num_rows($result5);
                        if (count($pre_organization) > 0) { ?>
                            <tr>
                                <td style="text-align: left;">Preferred Organization:</td>
                                <td style="text-align: left;">
                                    <?
                                    $i=1;
                                    while($org=  mysql_fetch_array($result5)) {
                                       
                                        if ($i == 1) {
                                            echo $org['business_name'];
                                        } else {
                                            echo ', ' . $org['business_name'];
                                        }
                                        $i++;
                                    }
                                    ?>
                                </td>
                            </tr>
                        <? } ?>
                        <tr>
                            <td style="text-align: left;">Preferred Area:</td>
                            <td style="text-align: left;">
                                <? 
                                $sql6="select * from job_emp_location join district where job_emp_location.location_id = district.district_id  and job_emp_location.employee_id='$user_id'";
                                $result6=  mysql_query($sql6);
                                $pre_location=  mysql_num_rows($result6);
                                if ($pre_location > 0) { ?>

                                    <?
                                    $i=1;
                                   while ($loc=  mysql_fetch_array($result6)) {
                                        if ($i == 1) {
                                            echo $loc['District_Name'];
                                        } else {
                                            echo ', ' . $loc['District_Name'];
                                        }
                                        $i++;
                                    }
                                    ?>

                                    <?
                                } else {
                                    echo 'Anywhere in Bangladesh.';
                                }
                                ?>
                            </td>
                        </tr>
<!--                                <tr>
                        <td class="right">Looking For:</td>
                        <td>Name</td>
                    </tr>
                    <tr>
                        <td class="right">Available For:</td>
                        <td>Name</td>
                    </tr>-->

                    </tbody>
                </table>
            <? } ?>
            <br />


 
       
            <? 
            $sql7="select * from job_other_reference where employee_id='$user_id'";
            $result7=  mysql_query($sql7);
            $reference1=  mysql_num_rows($result7);
            
            while($row7=  mysql_fetch_array($result7))
            {
                $reference[]=$row7;
            }
            
            if ($reference1 > 0) { ?>
                <table style="width: 100%; border: 0px;">
                    <thead>
                        <tr>
                            <th colspan="3" style="padding-left: 10px; width: 40%; background: #ccc; color: #000; text-align: left;">Reference</th>
                        </tr>

                        <tr>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;"></th>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;"><strong>Reference: 1</strong></th>
                            <th style="padding-left: 10px; background: #ccc; color: #000; text-align: left;"><strong><? if (isset($reference[1])) { ?>Reference : 2<? } ?></strong></th>
                        </tr>
                    </thead>
                    <tbody>		
                        <tr> 
                            <td style="text-align: left;">Name :</td>
                            <td style="text-align: left;"><?= $reference[0]['ref_name'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['ref_name']) ? $reference[1]['ref_name'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">organization :</td>
                            <td style="text-align: left;"><?= $reference[0]['organization'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['organization']) ? $reference[1]['organization'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Designation :</td>
                            <td style="text-align: left;"><?= $reference[0]['designation'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['designation']) ? $reference[1]['designation'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Address :</td>
                            <td style="text-align: left;"><?= $reference[0]['address'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['address']) ? $reference[1]['address'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Phone(Off) :</td>
                            <td style="text-align: left;"><?= $reference[0]['phone_office'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['phone_office']) ? $reference[1]['phone_office'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Phone(Res) :</td>
                            <td style="text-align: left;"><?= $reference[0]['phone_resident'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['phone_resident']) ? $reference[1]['phone_resident'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">mobile :</td>
                            <td style="text-align: left;"><?= $reference[0]['mobile'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['mobile']) ? $reference[1]['mobile'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Email :</td>
                            <td style="text-align: left;"><?= $reference[0]['email'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['email']) ? $reference[1]['email'] : '' ?></td>
                        </tr>
                        <tr> 
                            <td style="text-align: left;">Relation :</td>
                            <td style="text-align: left;"><?= $reference[0]['relation'] ?></td>
                            <td style="text-align: left;"><?= isset($reference[1]['relation']) ? $reference[1]['relation'] : '' ?></td>
                        </tr>
                    </tbody>
                </table>
          <? } 
          
                            }?>
        </div>

    </body>
</html>
