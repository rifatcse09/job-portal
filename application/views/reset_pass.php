<!DOCTYPE html>
<html lang="en">
    <head>

        <script type="text/javascript" src="<?= base_url() ?>js/jquery-1.5.1.js"></script>
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/jquery-ui-1.8.18.custom.css" />
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-ui-1.8.11.min.js"></script>
        
        <meta charset="utf-8">
        <title>Employee Reset Password</title>
        
          <style type="text/css">
            body
            {
                margin:0 auto;
                width:1200px;
/*                background: #F0F0F0;*/
/*                background-image: -webkit-linear-gradient(bottom, #CCE0FF,#FFF); 
                background-image:    -moz-linear-gradient(bottom, #CCE0FF, #FFF);
                background-image:     -ms-linear-gradient(bottom, #CCE0FF, #FFF);
                background-image:      -o-linear-gradient(bottom, #CCE0FF, #FFF);
                background-image:         linear-gradient(bottom, #CCE0FF, #FFF);*/
            }
            #employer_page
            {
                float:left;
                margin:10px;
                width:1180px;
                border-radius:5px;
                padding-bottom:20px;


            }
            #employer_header
            {
                float:right;
                margin:0px 10px;
                width:1180px;
                height:100px;
                /*background:url(../employer_images/logo1.png) left top no-repeat;*/

                /*background: #A5090A;*/
            } 

            #employer_menu
            {
                float:left;
                width:1180px;
                background: #4D90F0;
                box-shadow:3px 3px 10px 10px #006699;
                border-radius:10px 10px 0px 0px;
                margin-top:30px;
                
            }
            #emp_in
            {
                float:left; 
                width:1170px;
                background:#fff ;
                margin-top: 35px;
                 margin-bottom: 35px;
                margin-left: 5px;
                border-radius:10px 10px 10px 10px;
            }
            #emp_format1
            {
                float:left; 
                width:1170px;
                background: #fff;
                height: 100px;
                border-radius:10px 10px 0px 0px;
            }
            #emp_format1_red
            {
                margin-top: 30px;
                float:left; 
                width:1170px;
                background: #025568;
                height: 30px;
                border-bottom: 5px #000 solid;
            }
            #red_all
            {
                float:left; 
                width:1170px;
                height: 1030px;
                background: #ffffff;

            }
            #table table 
            {
                width:920px;
            }
            #table table tr th
            {
                padding:2px;
                color:#FF6600;
                border:1px solid #3f3a30;
            }



/*            #red_all_1
            {
                float:left; 
                width:1160px;
                height: 30px;
                background: #0099ff;
                margin-top: 30px;
                margin-left: 5px;
                margin-right: 5px;
            }*/
            #red_all_2
            {
                float:left; 
                width:1160px;
                height: 300px;
                background: #fff;

                margin-left: 5px;
                margin-right: 5px;

            }
            #red_all_3
            {
                float:left; 
                width:255px;
                height: 290px;
/*                background: #240202;*/
                 border: 1px solid #4D90F0;
                margin-top:5px;
/*                background:#666680;*/
                margin-left: 30px;
                margin-bottom: 10px;
                border-bottom-left-radius: 20px;
                border-bottom-right-radius: 20px;
                border-top-left-radius: 2em;
                border-top-right-radius: 2em;

            }
            #red_all_3 h4
            {
                color: #fff;font-weight: 200;  margin-top: 2px;
            }
            #red_all_4
            {
                float:left; 
                width:255px;
                height: 40px;
                background: #0099ff;
                 border-top-left-radius: 1.9em;
                border-top-right-radius: 1.9em;
                
                margin-top: 0px;

            }
            #red_all_4 p
            {
                color: #fff; font-size: larger; margin-left: 65px; margin-top: 6px;
                
            }
            #red_all_4 p h4{
                
              
                
            }
            #red_all_5
            {
                float:left; 
                width:750px;
                height: 240px;
/*                background: #CDDBE4;*/
                margin: 22px 0px 0px 30px;
/*                border: 5px #0099ff solid;*/
               border-bottom-left-radius: 3em;
               border-bottom-right-radius: 3em;
               border-top-right-radius: 3em;
               border-top-left-radius: 3em;
               



            }
            #red_all_a
            {
                margin-left: 27px;
                margin-top: 25px;
                height: 180px;
                width: 220px;
/*                border-right: 1px #025568 solid;*/
                float: left;
                
                margin-left:240px;
            }

            #red_all_a h5
            {
                color: #025568;
                margin-top: 0px;
            }
            #red_all_aa
            {
                margin-left: 27px;
                margin-top: 25px;
                height: 150px;
                width: 220px;

                float: left;
            }
            #red_all_aa h5
            {
                color: #025568;
                margin-top: 0px;
            }
            #red_all_6
            {
                float:left; 
                width:100%;
                height: 10%;
                background: #0099ff;
                margin-top: 0px;
                margin-left: 0px;
                padding-bottom: 5px;
                border-radius:10px 10px 0px 0px;



            }
            #red_all_6 p
            {
                margin-top: 2px;
                margin-left: 200px;
                color: #fff;
                font-size: larger;
            }
            #red_all_7
            {
                float:left; 
                width:1160px;
                height: 30px;
                background: #8f8e8e;
                margin-top: 30px;
            }


            #red_all_8
            {
                float:left; 
                width:1160px;
                height: 570px;
                background: #fff;
                margin-top: 30px;
                margin-left: 5px;
                margin-right: 5px;

            }
            #red_all_9
            {
                float:left; width:351px;height: 250px;background: #CDDBE4;margin-top: 25px;margin-left: 26px;  
            }
            #red_all_10
            {
                float:left; width:351px;height: 30px;background: #0099ff;  
            }
            #red_all_10 p
            {
                color: #fff;margin-top: 3px;margin-left: 60px;
            }
            #red_all_11
            {
                float:left; width:351px;height: 190px;  
            }
            #red_all_11 ul
            {

            }
            #red_all_11 ul li
            {
                list-style-image:url(<?= base_url() ?>employer_images/list.png);
            }
            #red_all_12
            {
                float:left; width:351px;height: 25px;background: #0099ff;margin-top: 5px; 
            }
            #red_all_12 p
            {
                color: #fff;margin-top: 3px;margin-left: 60px;
            }


            #red_menu
            {
                width: 1100px;
                float: left;
                background: #8f8e8e;
                height: 40px;
                margin-left: 20px;
                margin-top: 20px;
            }



            #red_menu ul
            {
                margin-left: 50px;
                margin-top: 4px;
                padding: 0;
            }

            #red_menu ul li
            {
                display: inline;

            }

            #red_menu li a
            {
                display: block;
                float: left;
                padding:6px 20px;
                font-size:12px;
                color: #fff;
                font-weight:normal;
                text-decoration: none;
                border-right:1px solid #fff;

            }
            #red_menu li a:hover {color:#000; }
            
            
            
                                          input[type=text]:focus, input[type=password]:focus 
                                               {
                                               box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                                               padding: 3px 0px 3px 3px;
                                               margin: 5px 1px 3px 0px;
                                               border: 1px solid rgba(81, 203, 238, 1);
                                               }  
                                               
                                               input[type="submit"] 
                                               {
                                                       background-color: #F5F5F5;
                                                       border: 1px solid rgba(0, 0, 0, 0.1);
                                                       border-radius: 2px;
                                                       color: #222222;
                                                       font: bold 12px arial;
                                                       height: 30px;
                                                       margin: 1px 0 0 15px;
                                                       padding-bottom: 0;
                                                       text-align: center;
                                                       text-shadow: 0 1px rgba(0, 0, 0, 0.1);
                                                       vertical-align: top;
                                                       width: 80px;
                                              }
                                              #login {
    background-color: #4D90F0;
    border: 1px solid #3079ED;
    color: #FFFFFF;
    font-weight: bold;
    margin: 8px 0 0 18px;
}
                               #login:hover {
    background-color: #3571eb;
    border-color:#2f5bb7;
 
    font-weight: bold;
    margin: 8px 0 0 18px;
}                 
                          
          #creat_account tr td a
{
   text-decoration: none; 
   color:#4D90F0;
    
}  
        </style>
        </head>
        <body>
        <div id="employer_page">
            <div id="employer_header">
                <img src="<?= base_url() ?>/images/logo.png" style="margin-top: 10px;" />
            </div>
                    <div id="employer_menu">

                          <div id="emp_in">
            <a href="<?=site_url()?>"><img style="margin-top:10px;margin-left: 10px" src="<?= base_url() ?>images/home_m.png"  style=" margin-left: 25px;" title="Home"/></a>
                                         <form action="<?=site_url()?>/mainpage/reset_pass/" method="post">
                                                 <table align="center"><tr><td ><span style="font-size: 22px;color:red">Type your E-Mail address which you have used in your Resume :</span></td></tr></table>
                                             
                                             <table border="0" align="center" style="margin-top:50px;margin-bottom: 30px">

                                                              <tr>
                                                                       <td><span style="font-size:22px">Email</span></td>
                                                                           <td>:</td>
                                                                                <td><input type="text" name="email" style="width:220px;font-size: 16px"/></td>
 
                                                              </tr>
  
                                                              <tr>
                                                                  <td></td>
                                                                   <td></td>
                                                                           <td align="center"><input type="submit" value="Submit" id="login" /></td>
                                                              </tr>
                                                 </table>

</form>

<?
                if (isset($_POST['email'])) {
                    include "database.php";
                    $email = $_POST['email'];

                    $sql = "SELECT * FROM job_employee_login WHERE email='$email' ";
                    $result = mysql_query($sql);
                    $row = mysql_fetch_array($result);
                    $pw = $row['password'];
                    $user_name=$row['user_name'];
                    if (isset($pw)) {
                        include('SMTPconfig.php');
                        include('SMTPClass.php');
                        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                            $to = $email;
                            $from = 'info@purbodeshjobs.com';
                            $subject = "Request for Password.";
                            $password = 'Password: ' . $pw;
                            $user='Username: ' . $user_name;
                            $body=$user.' '.$password;
                            //$body= $_POST['message'];
                            $SMTPMail = new SMTPClient($SmtpServer, $SmtpPort, $SmtpUser, $SmtpPass, $from, $to, $subject, $body);
                            $SMTPChat = $SMTPMail->SendMail();
                            echo '<h2 align ="center">Password has been sent to your email account.If not found, Please check your spam folder.<br/>
                                click this link <a href='.site_url().'>Click Here</a></h2>';
                        }
                        echo "<meta http-equiv='refresh' content='3 url=".site_url()."/mainpage/reset_pass";
                    } else {
                        echo "<h2 style='margin-left: 20px; padding-bottom:20px; color=green'>Sorry, this Email has no user yet.</h2>";
                        echo "<meta http-equiv='refresh' content='3 url=".site_url()."/mainpage/reset_pass";
                    }
                } else {
                    
                    
                    
                }
?>
</div>
    </div>
            </div>
        </body>
    </html>