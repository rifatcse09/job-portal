<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<title>Scholarship</title>
<head>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/job.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/style.css" />
</head>

<div id="content_container">
    <div id="job_category_header">
        <?= $this->load->view("jobsbycatagories_header.php"); ?>
    </div>
    <div id="content_all">
 <div id="job_category_navigation">
                <?= $this->load->view("jobsbycategory_menu.php"); ?> 
            </div>
        <div id="content_content">
           
            <div id="page_index">
            </div>

            <div id="job_type_result_curve">

            </div>

            <div id="job_type_result">
                <div id="job_type_pan">
                    
                </div>
            </div>
            <div id="job_type_container">
                <div id="job_type_content">
                    <table width="100%">
                        <tr> 
                            <td width="45%" bgcolor="#000" height="29" align="center" class="BDJLebels" id="tdcornertl">
                                <strong>Institute Name</strong></td>
                            <td width="45%" bgcolor="#000" nowrap="" align="center" class="BDJLebels" id="tdcornertr">
                                <strong>Scholarship Name</strong></td>
                            <td width="45%" bgcolor="#000" nowrap="" align="center" class="BDJLebels" id="tdcornertr">
                                <strong>Last Date</strong></td>
                        </tr>
                     <?
                        include("database.php");
                        // $id = $_GET['exam_id'];
                        $sql = "select * from  scholarship";
                        $data = mysql_query($sql);
                        while ($r = mysql_fetch_array($data)) {
                            ?>
                            <tr>  
                               <td width="200" height="29" align="center" style="background-color: #dfdddd;padding-top: 10px;padding-bottom: 10px"><a style="text-decoration: none;color: #006699" target="_blank" href="<?= base_url() ?>adminpanel/scholarship_images/<? echo $r['name'] ?>"><? echo $r['institute_name'] ?></a></td>    
                               <td width="200" height="29" align="center" style="background-color: #dfdddd;padding-top: 10px;padding-bottom: 10px"><a style="text-decoration: none;color: #006699" target="_blank" href="<?= base_url() ?>adminpanel/scholarship_images/<? echo $r['name'] ?>"><? echo $r['scholarship_name'] ?></a></td>    

                                <td width="200" height="29" align="center" style="color: #006699;background-color: #dfdddd;padding-top: 10px;padding-bottom: 10px"><? echo date('d-F-Y', strtotime($r['last_date'])); ?></td>
                            </tr>

                            <?
                        }
                        ?>
                    </table>
                </div>

            </div>
            <div id="page_index">

            </div>
        </div>
        <div id="content_add">
            <?
            include("database.php");
            $sql = "select * from  advertisement where advertisement_type='jobcategory2' ORDER BY add_id DESC LIMIT 0,3";
            $data = mysql_query($sql);
            while ($r = mysql_fetch_array($data)) {
                ?>
                <tr>
                    <td valign="top"><a href="<? echo $r['website']; ?>" target="_blank"><img src="<?= base_url() ?>adminpanel/advertise_images/<? echo $r['name']; ?>" width="130" height="120" style="margin:2px 0px 0px 0px;" /></a></td>
                </tr>
                <?
            }
            ?>
        </div>
    </div>
    
<!--    <div id="footer_last"></div>
    <div id="footer_last_bottom"></div>-->
</div>
<div id="footer_bottom_navigation">
        <? $this->load->view("footer_bottom_navigation.php"); ?>
</div>
 <div id="wrapper_bottom">
             <div style="float:right;margin-right: 20px"><p title="Design & Developed by PentaGems" style="color:#fff">Design & Developed by <a title="PentaGems"  style="text-decoration: none;color:#E6E600;font-weight: bold"target="_blank" href="http://pentagemsbd.com">PentaGems</a></p></div>
                <? //$this->load->view("footer_bottom_navigation.php"); ?>
 </div>
</html>