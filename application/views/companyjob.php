<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<title>Job Category</title>
<head>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/job.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/style.css" />
</head>

<div id="content_container">
    <div id="job_category_header">
        <?= $this->load->view("jobsbycatagories_header.php"); ?>
    </div>
    <div id="content_all">

        <div id="content_content">
            <div id="job_category_navigation">
                <?= $this->load->view("jobsbycategory_menu.php"); ?> 
            </div>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr> 
                    <td height="15" class="BDJtabLinkSelected" style="text-align:center">&nbsp;</td>
                </tr>

                <tr> 
                     <td height="30" class="BDJtabLinkSelected" style="text-align:center">
                        <a style='color:#FD7700' href='<?= site_url()?>/jobcategory/companysearch?qjobnature=A'>&nbsp;A&nbsp;|&nbsp;</a>
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=B'>&nbsp;B</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=C'>&nbsp;C</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=D'>&nbsp;D</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=E'>&nbsp;E</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=F'>&nbsp;F</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=G'>&nbsp;G</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=H'>&nbsp;H</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=I'>&nbsp;I</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=J'>&nbsp;J</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=K'>&nbsp;K</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=L'>&nbsp;L</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=M'>&nbsp;M</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=N'>&nbsp;N</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=O'>&nbsp;O</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=P'>&nbsp;P</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=Q'>&nbsp;Q</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=R'>&nbsp;R</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=S'>&nbsp;S</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=T'>&nbsp;T</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=U'>&nbsp;U</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=V'>&nbsp;V</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=W'>&nbsp;W</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=X'>&nbsp;X</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=Y'>&nbsp;Y</a>&nbsp;|&nbsp;
                        <a href='<?= site_url()?>/jobcategory/companysearch?qjobnature=Z'>&nbsp;Z</a>&nbsp;|&nbsp;

                    </td>
                </tr>
            </table>


            <div id="page_index">
            </div>

            <div id="job_type_result_curve">

            </div>

            <div id="job_type_result">
                <div id="job_type_pan">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0"  height="30" align="center">
                        <tbody>
                            <tr>
                                <td class="BDJGrayArial11px" width="20%" style="background-color:#dfdddd;color:#1a4f91;font-size: 16px;padding-left: 15px;">
                                    <strong> <?= isset ($jobs[0]['company_name']) ?  $jobs[0]['company_name'] : ''?> </strong>

                                </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="job_type_container">
                <div id="job_type_content">
                    <table width="100%">

                        <?
                        if ($jobs) {
                            foreach ($jobs as $job) {
                                ?>
                                <tr>
                                    <td width="20%" style="background-color:#dfdddd;" height="29" class="body"><a href="<?= site_url() ?>/jobcategory/JobDetails?ID=<?= $job['id'] ?>"><?= $job['job_title'] ?></a></td>
                                   
                                </tr>
                                <?
                            }
                        } else {
                            echo '<tr>
                                    <td>No Data Available</td>
                                </tr>';
                        }
                        ?>
                    </table>
                </div>

            </div>
            <div id="page_index">

            </div>
        </div>
        <div id="content_add">
            <?
            include("database.php");
            $sql = "select * from  advertisement where advertisement_type='jobcategory2' ORDER BY add_id DESC LIMIT 0,3";
            $data = mysql_query($sql);
            while ($r = mysql_fetch_array($data)) {
                ?>
                <tr>
                    <td valign="top"><a href="<? echo $r['website']; ?>" target="_blank"><img src="<?= base_url() ?>adminpanel/advertise_images/<? echo $r['name']; ?>" width="130" height="120" style="margin:2px 0px 0px 0px;" /></a></td>
                </tr>
                <?
            }
            ?>
        </div>
    </div>

</div>
<div id="footer_bottom_navigation" style="float: right;">
        <? $this->load->view("footer_bottom_navigation.php"); ?>
    </div>
</html>