<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<head>
    <title><?= isset($title) ? $title : "Suprobhat Jobs"; ?></title>
    <script type="text/javascript">
        function verifyemail(){
            var fr_email = document.getElementById('friend_email');
            var your_email = document.getElementById('your_email');
            var fr_name = document.getElementById('friend_name');
            var your_name = document.getElementById('your_name');
        
        
            if(fr_email.value == "" || your_email.value == "" || fr_name.value == "" || your_name.value == ""){
                alert('You must fill up all data.');
                return false;
            }
        }
        
        function validate_email(email,alerttxt) // e-mail validation
        {
            var dv = document.getElementById(email);
            if((dv.indexOf(':', 0) >= 0) || (dv.indexOf(';', 0) >= 0) ||(dv.indexOf('<', 0) >= 0) ||(dv.indexOf('>', 0) >= 0) ||(dv.indexOf('\\', 0) >= 0) ||(dv.indexOf(',', 0) >= 0) ||(dv.indexOf(' ', 0) >= 0) ||(dv.indexOf('/', 0) >= 0) ||(dv.indexOf('@', 0) == -1) || (dv.indexOf('.', 0) == -1))
            {
                alert(alerttxt);
                return false;
            }
            apos=dv.indexOf("@")
            dotpos=dv.lastIndexOf(".")
            if (apos<1||dotpos-apos<2)
            {
                alert(alerttxt);
                //alert("Second If");
                return false;
            }
            apos2=dv.lastIndexOf("@")
            if (apos !=apos2)
            {
                alert(alerttxt);
                //alert("Third If");
                return false;
            }
            else 
            {
                return true
            }
        } 
    </script>
</head>
<body>
    <?= isset($msg) ? $msg : '' ?>
    <form action="<?= site_url() ?>/jobcategory/email_job_post" method="post" onsubmit="return verifyemail();">
        <input type="hidden" name="job_id" value="<?= isset($job_id) ? $job_id : '' ?>"/>
        <table>
            <tr>
                <td>Your Friend's name:</td>
                <td><input name="friend_name" id="friend_name"/></td>
            </tr>
            <tr>
                <td>Your Friend's email:</td>
                <td><input name="friend_email" id="friend_email"/></td>
            </tr>
            <tr>
                <td>Your name:</td>
                <td><input name="your_name" id="your_name"/></td>
            </tr>
            <tr>
                <td>Your email:</td>
                <td><input name="your_email" id="your_email"/></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" name="submit" value="Send"/></td>
            </tr>
        </table>

    </form>
</body>
</html>