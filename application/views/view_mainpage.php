<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title><?= $title; ?></title>
        <meta http-equiv="X-UA-Compatible" content="chrome=1"> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="Welcome to Azadijobs!, the world's most visited home page. Quickly find what you're searching for, get in touch with friends and stay in-the-know with the latest tender and information."> 
        <meta name="keywords" content="jobsite, bdjobsite, bdjobsite homepage, azadijobs search, azadijobs microjob, purbodeshjobs hire me, hire me, microjob, prothomalojobs, bdjobs, deshjobs"> 
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/mainpage_css/style.css" />
        <link rel="stylesheet" type="text/css" media="all" href="<?= base_url(); ?>/jsDatePick_ltr.min.css" />
        <link href="<?= base_url(); ?>/mainpage_css/adipoli.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="<?= base_url(); ?>/mainpage_css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>/mainpage_css/bootstrap.min.css">
        
        <link rel="stylesheet" href="<?= base_url(); ?>/mainpage_css/slide/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>/mainpage_css/slide/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>/mainpage_css/social.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

		<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/mainpage_css/engine1/style.css" />
<script type="text/javascript" src="<?= base_url(); ?>/mainpage_css/engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->

        <script src="<?= base_url() ?>js/jquery-1.7.1.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>js/jquery.adipoli.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jsDatePick.min.1.3.js"></script>
        <!---for facybox-->
        <script type="text/javascript" src="<?= base_url() ?>/source/jquery.fancybox.js?v=2.1.5"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />
        <link rel="stylesheet" href="<?= base_url(); ?>/mainpage_css/animate.css" type="text/css">
        <script>
            $('a.fancybox').fancybox({
                padding: 0,
                openEffect: 'elastic',
                closeEffect: 'elastic',
                prevEffect: 'fade',
                nextEffect: 'fade',
                scrolling: 'no',
                openEffect  : 'elastic',
                        type: "iframe"
            });
        </script>

        <script type="text/javascript">
            window.onload = function() {
                g_globalObject = new JsDatePick({
                    useMode: 1,
                    isStripped: true,
                    target: "div3_example"

                });

                g_globalObject.setOnSelectedDelegate(function() {
                    var obj = g_globalObject.getSelectedDay();
                    alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
                    document.getElementById("div3_example_result").innerHTML = obj.day + "/" + obj.month + "/" + obj.year;
                });
                g_globalObject2 = new JsDatePick({
                    useMode: 1,
                    isStripped: false,
                    target: "div4_example",
                    cellColorScheme: "beige"

                });
                g_globalObject2.setOnSelectedDelegate(function() {
                    var obj = g_globalObject2.getSelectedDay();
                    alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
                    document.getElementById("div3_example_result").innerHTML = obj.day + "/" + obj.month + "/" + obj.year;
                });

            };
            function login_check() {
                var user_name = document.getElementById('user_name');
                var password = document.getElementById('password');

                if (!user_name.value || !password.value) {
                    alert('user name and password both required.');
                    return false;
                }
            }

$("#expanding").mouseover(function(){
    $(this).animate({
        paddingTop: "15px"
    }, 100);
}).mouseout(function(){
    $(this).animate({
        paddingTop: "10px"
    }, 100);
});


        </script>
        <script type="text/javascript">

            $(function() {
                $('.row1').adipoli({
                    'startEffect': 'normal',
                    'hoverEffect': 'popout'
                });

            });

            function hello_msg(obj, val) {
                if (val) {

                } else {
                    alert('blank');
                    document.getElementById('user_name').focus();
                }
            }



        </script>

        <style type="text/css">
            #table tr td
            {
                padding: 5px 10px;
            }
            input[type=text]:focus, input[type=password]:focus 
            {
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 1px solid rgba(81, 203, 238, 1);
            }  

            input[type="submit"] 
            {
                background-color: #F5F5F5;
                border: 1px solid rgba(0, 0, 0, 0.1);
                border-radius: 2px;
                color: #222222;
                font: 15px Cambria;
                height: 27px;
                margin: 1px 0 0 15px;
                padding-bottom: 0;
                text-align: center;
                text-shadow: 0 1px rgba(0, 0, 0, 0.1);
                vertical-align: top;
                width: 75px;
            }
            #login {
                background-color: #4D90F0;
                border: 1px solid #3079ED;
                color: #FFFFFF;
                
/*                margin: 8px 0 0 18px;*/
            }
            #login:hover {
                background-color: #3571eb;
                border-color:#2f5bb7;

                font-weight: bold;
                margin: 8px 0 0 18px;
            }                 
            #table_reset tr td a{

                text-decoration: none;
                color:#4D90F0;
            }
            #table_reset tr td a:hover{

                text-decoration: none;
                color:#000000;
            } 
            
            /* entire container, keeps perspective */
.flip-container {
	perspective: 1000;
}
	/* flip the pane when hovered */
	.flip-container:hover .flipper, .flip-container.hover .flipper {
		transform: rotateY(180deg);
	}

.flip-container, .front, .back {
	width: 320px;
	padding-top: 2px;
            padding-bottom:2px
}

/* flip speed goes here */
.flipper {
	transition: 0.6s;
	transform-style: preserve-3d;

	position: relative;
}

/* hide back of pane during swap */
.front, .back {
	backface-visibility: hidden;

	position: absolute;
	top: 0;
	left: 0;
}

/* front pane, placed above back */
.front {
	z-index: 2;
	/* for firefox 31 */
	transform: rotateY(0deg);
}

/* back, initially hidden pane */
.back {
	transform: rotateY(180deg);
}
.btn.btn-info {
    color: #ffffff;
    background-color: #5BC0DE;
    background-image: linear-gradient(to bottom, #5BC0DE, #2AABD2);
    border-color: #28A4C9 #28A4C9 #28A4C9;
}
.btn.btn-info:hover {
    color: #ffffff;
    background-color: #2AABD2;
    background-image: linear-gradient(to bottom, #2AABD2, #2AABD2);
    border-color: #28A4C9 #28A4C9 #28A4C9;
}



        </style>


    </head>
    <body style="font-family:calibri;">
        <div id="page">
            <div id="top"></div>
            <div id="header" style="">
                <?= $this->load->view("mainpage_header"); ?>
            </div>
            <div id="navigation">
                <?= $this->load->view("mainpage_menu"); ?> 
            </div>
            <div id="contentpage" class="image_border">
			<?
                                
                                $sql = "select * from advertisement where advertisement_type='MainpageTop' order by add_id desc limit 0,1";
                                $result = mysql_query($sql);
                                $row = mysql_fetch_array($result)
                                ?>

                <div id="content">
                    <div style="width:930px" id="contentleft" class="col-md-4 wow fadeInUp" data-wow-duration="500ms">
                        <div style="float:left;width:100%;height: 450px;border-bottom-left-radius: 15px;border-bottom-right-radius: 15px;">
                            <div class="job" style="width: 100%;height:200px;margin-top: 8px;">
                              <!-- Start WOWSlider.com BODY section -->
                              <div style="border:2px solid #b4bfc1; border-radius: 3px;width:900px;margin-left: 1%; ">
<div id="wowslider-container1">
<div class="ws_images"><ul>
		<li><img style="max-height:120px;" src="<?= base_url() ?>data1/images/4.png" alt="4" title="4" id="wows1_0"/></li>
		<li><img style="max-height:120px;" src="<?= base_url() ?>data1/images/educationquotewallpaperhd5.jpg"  title="Education-Quote-Wallpaper-HD-5" id="wows1_1"/></li>
		<li><img style="max-height:120px;" src="<?= base_url() ?>data1/images/1.png" alt="1" title="1" id="wows1_2"/></li>
	</ul></div>
<div class="ws_script" style="position:absolute;left:-99%"></div>
<div class="ws_shadow"></div>
</div>	
                              </div>
<script type="text/javascript" src="<?= base_url() ?>/mainpage_css/engine1/wowslider.js"></script>
<script type="text/javascript" src="<?= base_url() ?>/mainpage_css/engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->
                            </div>
                            <div style="margin-top:-8%;text-align: left;" id="jobcontent" class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
                                <?php
                                $data['categories'] = $categories;
                                $this->load->view('common/category_list_plain', $data);
                                ?>
                            </div>

                            <div id="joblink" style="  " class="col-md-4 wow fadeInUp">
                                <div class="q" style="">
                                    <h4><img style="width: 20px;height: 20px;margin-top: 10px;" src="<?= base_url() ?>images/Link-Icon.png"><span style="padding-left: 5px;color:#CC0000;font-size: 14px;">Quick Links</span></h4>
                                    <div id="highlight_jobs_header_navigation">
                                <? $this->load->view("mainpage_highlight_menu"); ?>
                            </div>
                                    
                                    <!--                                    <ul>
                                                                            <li><a href="<?= site_url() ?>/jobcategory/showBlueJobs">Blue Collar Jobs</a></li>
                                                                            <li><a href="<?= site_url() ?>/jobcategory/examresultview" target="_blank">Exam Result</a></li>
                                                                            <li><a href="<?= site_url() ?>/jobcategory/scholarshipall" target="_blank">Scholarship</a></li>
                                                                            <li><a href="<?= site_url() ?>/jobcategory/admissionall">Admission Info</a></li>
                                                                            <li><a href="<?= site_url() ?>/jobcategory/admissionall">Government Job</a></li>
                                                                            <li ><a style="border:none" href="<?= site_url() ?>/training/ready_all">Ready for Job</a></li>
                                                                        </ul>-->
                                </div>

                            </div>
                        </div>
                        <?php
                        $count_jobs = count($hire_jobs);
                        ?>


                    </div>
                    <div id="contentright" class="sec-title text-center mb50 wow bounceInRight animated" data-wow-delay="500ms">
                        <div id="contentright_login" style="height: 80px;">
                            <div id="contentright_login_header" style="height:30px;">
                                <p>Job Seeker</p>
                            </div> 
                            <div id="rightr">             
                                <div id="user_login_content">

                                   <form action="<?= base_url() ?>employee/index.php/page/login_from_mainpage" method="post" >
                                       <!--<table class="table table-striped table-bordered table-hover dataTable no-footer" style="margin-left: 5px;height: 100px;margin-top:10px;margin-bottom: 0px;">
                                            <tr>
                                                <td style="font-size:12pt;text-align: right;color:#666666"><i class="fa fa-user"></i></td>
                                                <td><input  style="width: 100%;border:1px solid #e6e6e6;" class="design_form" type="text" name="user_name" id="user_name" size="20"></td></tr>
                                            <tr>
                                                <td style="font-size:12pt;text-align: right;color:#666666"><i class="fa fa-key"></i></td>
                                                <td><input style="width: 100%;border:1px solid #e6e6e6" type="password" name="password" id="password" size="20"></td>
                                            </tr>
                                        </table> -->
                                        <table  id="job_seeker" style="height:30px;margin-top: 15px;margin-left: 5px;">
                                            <tr>
                                                <td id="creat_account"> <a   style="font-family:cambria;text-decoration:none;border: 1px solid #04A531; padding: 5px;background: #04A531;color: #fff;border-radius: 2px;" href="<?= base_url() ?>employee/index.php/createaccount">Create New Account</a></td>
                                                <td><input type="submit" value="Login" style="font-family:cambria;" id="login" ></td>
                                            </tr>


<!--                                        <input type="image" src="<?= base_url() ?>images/lginbtn.png" name="" id=""/>-->
                                        </table>

                                       <!-- <table id="table_reset" style="margin-bottom:5px;margin-left: 6%;"><tr>
                                                <td>Forgot password?</td>
                                                <td><a style="text-decoration:none;" href="<?= site_url() ?>/mainpage/reset_pass/">Click here to reset</a>

                                            </tr></table>-->

                                    </form>
                                    <div id="user_login_content_logbtn">
                                        <!--                                        <a href="#" style="text-decoration: none; font-weight: bold; color: #096534;">Forget Password&nbsp;?</a>-->
                                    </div>
                                </div>
                            </div>

                            <!--                                <div id="righte">                                    
                                                                
                                                            </div>-->

                        </div>
                        <div id="advertisement_top">
                            <div id="advertisement_top_content" style="">
                                <table id="tabl" style="margin-left: 3%;">
                                    <tr>
                                        <?
                                        
                                        $sql = "select * from  advertisement where advertisement_type='MainpageAd1' ORDER BY add_id DESC LIMIT 0,2";
                                        $dataaa = mysql_query($sql);
                                        while ($r = mysql_fetch_array($dataaa)) {
                                            ?>

                                            <td><a href="<? echo $r['website']; ?>" target="_blank"><img id="img_hov" src="<?=base_url() ?>adminpanel/advertise_images/<? echo $r['name']; ?>"  style="height:90px;width:120px;border-radius: .2em;margin-left: 2px;margin-top: 5px;border: 1px solid #BDCDED;" /></a></td>

                                            <?
                                        }
                                        ?>
                                    </tr> 
                                    <tr>
                                        <?
                                        $sqll = "select * from  advertisement where advertisement_type='MainpageAd1' ORDER BY add_id DESC LIMIT 2,2";
                                        $dataa = mysql_query($sqll);
                                        while ($rr = mysql_fetch_array($dataa)) {
                                            ?>

                                            <td><a  href="<? echo $rr['website']; ?>" target="_blank"><img  src="<?= base_url() ?>adminpanel/advertise_images/<? echo $rr['name']; ?>"  style="height:90px;width:120px;border-radius:.2em;border: 1px solid #BDCDED;margin-left:2px;margin-top: 0px" /></a></td>

                                            <?
                                        }
                                        ?>
                                    </tr>

<!--                                    <tr>
                                    <?
                                    $sq = "select * from  advertisement where advertisement_type='MainpageAd1' ORDER BY add_id DESC LIMIT 4,2";
                                    $d = mysql_query($sq);
                                    while ($rt = mysql_fetch_array($d)) {
                                        ?>

                                                                <td><a  href="<? echo $rt['website']; ?>" target="_blank"><img  src="<?= base_url() ?>adminpanel/advertise_images/<? echo $rt['name']; ?>" style="border-radius:.2em;border: 1px solid #BDCDED;margin-left: 2px;margin-top: 0px" /></a></td>

                                        <?
                                    }
                                    ?>
                                    </tr>-->
                                </table> 

                               <div id="rightcontent_bottom_add">
                                    
                                   <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
	<div class="flipper">
		<div class="front" style="
    
    height: 70px;
      margin-left: -11%;
">
			<img src="images/concord1.png" style="width: 255px;border-radius:2px solid;height: 150px;">
		</div>
		<div class="back" style="
    
    height: 70px;
      margin-left: -11%;
">
			<img src="images/concord2.png" style="width: 255px;border-radius:2px solid;height: 150px;">
		
		</div>
	</div>
</div>
                                    
                                </div>
                            </div>
                            <script type="text/javascript">

            $(function() {
                $('.row1').adipoli({
                    'startEffect': 'normal',
                    'hoverEffect': 'popout'
                });

            });

                            </script>
                        </div>



                    </div>
                    <script src="<?= base_url() ?>js/site_toogle.js" type="text/javascript"></script>
                    <script>
                        $(function() {
                            $('.robi_advertise').tabSlideOut({
                                tabHandle: '.handle',
                                pathToTabImage: 'http://ckrbd.com/azadijobs/images/Robi_Axiata.png',
                                imageHeight: '50px',
                                imageWidth: '60px', tabLocation: 'right',
                                speed: 300,
                                action: 'click',
                                bottomPos: '0px',
                                fixedPosition: true
                            });
                        });


                    </script> 
                    <div class="robi_advertise" style="line-height: 1; position: fixed; height: 100px; right: -600px;">
                        <a class="handle" target="_blank" href="http://bit.ly/Winback_November" style="background: url('http://ckrbd.com/azadijobs/images/Robi_Axiata.png') no-repeat scroll 0% 0% transparent; width: 60px; height: 50px; display: block; text-indent: -99999px; outline: medium none; position: absolute; top: 0px; left: -60px;"></a>
                        <a style="border:none;" target="_blank" href="http://bit.ly/Winback_November">
                            <img width="600" height="100" src="http://localhost:81/purbodesh/images/jpeg600x100.jpg">
                        </a>
                    </div>
                    <div class="robi_advertise open" style="line-height: 1; position: fixed; height: 100px; right: -3px;">
                        <a target="_blank" href="http://bit.ly/Winback_November" class="handle" style="background: url(&quot;home/home/banner/client_banners/robi/Robi_Axiata.png&quot;) no-repeat scroll 0% 0% transparent; width: 60px; height: 50px; display: block; text-indent: -99999px; outline: medium none; position: absolute; top: 0px; left: -60px;"></a>
                        <a style="border:none;" target="_blank" href="http://bit.ly/Winback_November"><img width="600" height="100" src="http://ckrbd.com/azadijobs/images/jpeg600x100.jpg"></a>
                    </div>


                    <script src="<?= base_url() ?>js/ispahani.js" type="text/javascript"></script>

                    <script>
                        $(function() {
                            $('.Ispahani-Ad').CellSlideOut({
                                tabHandle: '.Ispahani',
                                pathToTabImage: 'http://ckrbd.com/azadijobs/images/Ispahani.gif',
                                imageHeight: '120px',
                                imageWidth: '24px',
                                tabLocation: 'left',
                                speed: 300,
                                action: 'click',
                                bottomPos: '5%',
                                fixedPosition: true
                            });
                        });</script>

                    <div class="Ispahani-Ad" style="margin-top: 155px;line-height: 1; position: fixed; height: 120px; left: -187px;">
                        <a class="Ispahani" target="_blank" href="" style="background: url('http://ckrbd.com/azadijobs/images/Ispahani.gif') no-repeat scroll 0% 0% transparent; width: 24px; height: 120px; display: block; text-indent: -99999px; outline: medium none; position: absolute; top: 0px; right: -24px;">ispahanibd.com</a>
                        <a style="border:none;" target="_blank" href="">
                            <img width="187" height="120" src="http://ckrbd.com/azadijobs/images/Ispahani_s.jpg">
                        </a>
                    </div>

                    <div class="Ispahani-Ad open" style="margin-top: 155px;line-height: 1; position: fixed; height: 120px; left: -3px;">
                        <a class="Ispahani" target="_blank" href="" style="background: url('http://ckrbd.com/azadijobs/images/Ispahani.gif') no-repeat scroll 0% 0% transparent; width: 24px; height: 120px; display: block; text-indent: -99999px; outline: medium none; position: absolute; top: 0px; right: -24px;">ispahanibd.com</a>
                        <a style="border:none;" target="_blank" href="">
                            <img width="187" height="120" src="http://ckrbd.com/azadijobs/images/Ispahani_s.jpg">
                        </a>
                    </div>
                   
                    <!--test-->
                    <div class="row" style="margin-left:0px;margin-right: 0px;">
                        <div class="col-lg-12 col-md-12" style="margin-top: 20px;border:2px solid #033B45;border-top-left-radius: 10px;border-top-right-radius: 10px;background: #033B45;color:#fff;">
                            <h3 style="margin-top: 0px;text-align:left;font-family: Cambria;font-style: italic;padding-left: 5px;"><i style="
    margin-right: 5px;
" class="fa fa-cog fa-spin"></i>Priority Job</h3>
                            </div>
                        <table style="border-radius:4px;" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <?php
                                $var = 1;
                                $count = 1;
                                $date = date('Y-m-d');


                                for ($i = 0; $i < $count_jobs;) {
                                    //echo '<tr style="background:'. $color[$i % 2] .';">';

                                    $count++;


                                    if (($count + 1) % 3 == 0) {
                                        echo '<tr style="background:#F1F2F3;border-bottom:1px solid #e6e6e6;border-radious:10px;">';
                                    } else {
                                        echo '<tr style="background:#E7ECF2;border-bottom:1px solid #e6e6e6">';
                                    }

                                    for ($j = 0; $j < 3; $j++) {

                                        error_reporting(0);
                                        if ($j == 2) {
                                            echo '<td  class="main_title" valign=top>';
                                        } else {
                                            echo '<td  class="main_title" valign=top  style="border-right:1px solid #cccccc">';
                                        }

                                        echo '<div style="float:left"><img style= "margin-top:5px;margin-left:5px" src="' . base_url() . 'employer/companylogos/' . $hot_jobs[$i]['company_logo'] . '" width=80 height=80 alt=""/></div>';

                                        echo '<div style="float:left"  ><div style=" font-weight:bold;margin-left:15px;float:left">' . $hot_jobs[$i]['company_name'] . '</div>' . '<br>';

                                        $company_id = $hot_jobs[$i]['company_id'];
                                        $sq = "select id,job_title,company_name,company_id from emp_new_job where deadline >= '" . date('Y-m-d') . "' and is_hot_job= 'y' AND emp_new_job.is_active = 1 AND emp_new_job.company_id='$company_id'";
                                        $result = mysql_query($sq);
                                        $job = 1;
                                        echo '<ul>';
                                        while ($row = mysql_fetch_array($result)) {
                                            //echo $row['job_title'];

                                            if ($job < 3) {
                                                echo '<li style=" list-style-type:square;font-family:calibri;font-weight:bold;text-decoration:none;">' . '<a href="' . site_url() . '/jobcategory/JobDetails?ID=' . $row['id'] . '" style="text-decoration:none;color:#006600;">' . $row['job_title'] . '</a>' . '</li>';
                                            }
                                            if ($job == 3) {
                                                echo '<a id="expanding" class="fancybox" href="' . site_url() . '/jobcategory/hot_job_more?company_id=' . $row['company_id'] . '" style="text-decoration:none;color:red;font-weight:bold">[more+] </a>';
                                            }
                                            $job++;
                                        }
                                        echo '</li></ul></div></td>';

                                        $i++;
                                    }

                                    $var++;

                                    echo '</tr>';
                                }
                                ?>
                            </table>
                        
                        </div>
                    
                    
                    <!--test-->

                    

                    

<!--                    <div id="onlinetender" style="float:left;width: 100%;margin-top: 10px;padding-bottom: 10px;">

                        <div style="height:120px;float:left;">
                            <div style="height:120px;;float:left">
                                <img style="float:left;margin-right: 7px;margin-left: 5px;" src="<?= base_url() ?>images/macro.jpg">   
                            </div>

                        </div>
                          <? $sq="select * from advertisement where advertisement_type='Mainpagebottom' limit 0,2";
                             $result=mysql_query($sq);
                              while($row=  mysql_fetch_array($result))
                             {
                          ?>
                        <div style="float:left;width:43%;background: #cccccc">
                            <div style="height:120px;;float:left;">
                                <a target="_blank" href="<?=$row['website']?>"><img style="height:120px;width:420px"src="adminpanel/advertise_images/<?=$row['name']?>"/></a>
                                <p style="font-size:22pt;padding-left: 50px">Space for Advertisement</p>
                            </div>

                        </div>

                        <div style="float:right;width:42%;background: #cccccc;margin-left: 5px">
                            <div style="height:120px;;float:left;">
                                <p style="font-size:22pt;padding-left: 50px">Space for Advertisement</p>
                            </div>

                        </div>
                             <?}?>
                        <div id="onlinetender" style="float:left;width: 100%;margin-top: 10px;padding-bottom: 10px;background: none repeat scroll 0 0 #f8f8f8;border: 1px solid #c8d7dc;border-radius: 10px;">

                            <div id="highlight_jobs_header"style="height: 40px;">
                                <div id="highlight_jobs_header_lefttop" style=" border-top-left-radius: 8px;border-top-right-radius: 8px;">
                                    <img style="float:left;margin-right: 5px;" src="<?= base_url() ?>images/microjob.png">
                                    <p style="color:#0047B2;font-weight: bold;"><i class="fa fa-wrench"></i> Micro Jobs</p>
                                </div>

                            </div>
                            <div id="highlght_job_container">
                                <table id="table" style="border-collapse: collapse;">

                                    <?
//include("database.php");

                                    $sql_last = "select * from micro_job order by id desc";
                                    $dataa = mysql_query($sql_last);
                                    while ($re = mysql_fetch_array($dataa)) {
                                        $resultt[] = $re;
                                    }

                                    $count_blue = 1;
                                    for ($k = 0; $k < 6;) {
                                        $count_blue++;
                                        if (($count_blue + 1) % 3 == 0) {
                                            echo '<tr style="background:#F1F2F3;border-bottom:1px solid #e6e6e6">';
                                        } else {
                                            echo '<tr style="background:#E7ECF2;border-bottom:2px solid #e6e6e6">';
                                        }
                                        for ($j = 0; $j < 3; $j++) {
                                            ?>
                                            <td style="border-right:1px solid #e6e6e6;width: 30%"><p style="width:98%; color:#ff0000; font-size: 8pt; margin-top: 10px;font-weight: bold;padding-left: 5px;"><? echo $resultt[$k][1]; ?></p>
                                                <p style="width:100%; ; font-size:8pt; margin-top: -6px;padding-left: 5px">শুরুর তারিখ: <? echo date('d-m-y', strtotime($resultt[$k][7])); ?></p>

                                                <div style="width:98%;margin-left: 0%;padding-left: 5px;font-size: 12px"><? echo substr($resultt[$k][5], 0, 240) ?></div>
                                                <a style="text-decoration:none; margin-top: 3px;padding-left: 5px;font-weight:bold; color:#1a4f91;font-size: 8pt" href="<?= site_url() ?>/training/microjob_details?id=<? echo $resultt[$k][0]; ?>" target="_blank">বিস্তারিত দেখুন</a>

                                            </td>


                                            <?
                                            $k++;
                                        }
                                        echo '</tr>';
                                    }
                                    ?>
                                </table>

                            </div>
                            <div  id='see_all'><a  href="<?= site_url() ?>/training/Microjob_all">All Micro Jobs</a></div>
                        </div>

                    </div>-->

                        <div class="row" style="margin-left:0px;margin-right: 0px;">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div style="border:1px solid green; background: green;color:#fff;border-radius: 3px;">
  
                                        <h3 style="text-align:center;"><i class="fa fa-pencil-square"></i>  Training <i class="fa fa-pencil-square"></i></h3>
                                        </div>
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <?
                                            
                                            $sql = "select * from training order by training_id desc";
                                            $data = mysql_query($sql);
                                            $num_rows = mysql_num_rows($data);
                                            while ($r = mysql_fetch_array($data)) {

                                                $resultSet[] = $r;
                                            }

                                            $count_train = 1;
                                            for ($i = 0; $i < 6;) {
                                                $count_train++;

                                                if (($count_train + 1) % 2 == 0) {
                                                    echo '<tr style="background:;border-bottom:1px solid #e5e6e6">';
                                                } else {
                                                    echo '<tr style="background:;border-bottom:1px solid #e6e6e6">';
                                                }

                                                for ($j = 0; $j < 2; $j++) {
                                                    ?>


                                                    <td style="border-right:1px solid #e6e6e6;width: 50%"><p style=" color: #4091f0; font-size: 10pt; font-family:calibri; margin-top: 10px;font-weight: bold;padding-left: 5px;padding-bottom: 5px;"><? echo $resultSet[$i][1]; ?></p>
                                                        <p style="color:#ff0000; font-size:10pt;font-family:calibri; margin-top: -6px;font-weight: bold;padding-left: 5px">Date: <? echo date('d-m-y', strtotime($resultSet[$i][2])); ?></p>

                                                        <div style="margin-left: 0%;padding-left: 5px;font-family:calibri;font-size: 14px"><? echo substr($resultSet[$i][3], 0, 50) ?></div>
                                                        <a style="text-decoration:none; margin-top: 3px;padding-left: 5px;font-weight:bold; color:#1a4f91;font-size: 8pt;" href="<?= site_url() ?>/training/training_details?training_id=<? echo $resultSet[$i][0]; ?>" target="_blank">Click for Details</a>
                                                    </td>


                                                    <?
                                                    $i++;
                                                }
                                                echo '</tr>';
                                            }
                                            ?>
                                        
                                        </table>
                                        <div >
                                            <a class="btn btn-success" style="margin-left: 35%;" href="<?= site_url() ?>/training/training_all">Next Training</a>
                                            </div>
                                    </div>
                            
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div style="border:1px solid #5C0000 ; background: #5C0000;color:#fff;border-radius: 3px;">
                                        <h3 style="text-align:center;"><i class="fa fa-book"></i>  Workshop  <i class="fa fa-book"></i></h3>
                                        </div>
                                <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                    <?
                                            

                                            $query = "select * from news order by id desc";
                                            $data = mysql_query($query);
                                            $num_rows = mysql_num_rows($data);
                                            while ($r = mysql_fetch_array($data)) {

                                                $resultSett[] = $r;
                                            }
                                            $count_workshp = 1;
                                            for ($i = 0; $i < 6;) {
                                                //echo '<tr style="background:'. $color[$i % 2] .';">';

                                                $count_workshp++;

                                                if (($count_workshp + 1) % 2 == 0) {
                                                    echo '<tr style="background:;border-bottom:1px solid #e6e6e6">';
                                                } else {
                                                    echo '<tr style="background:;border-bottom:1px solid #e6e6e6">';
                                                }
                                                for ($j = 0; $j < 2; $j++) {
                                                    ?>


                                                    <td style="border-right:1px solid #f8f8f8;"><p style=" color: #ab0165; font-size: 10pt; font-family:Calibri; margin-top: 10px;font-weight: bold;padding-left: 5px"><? echo $resultSett[$i][1]; ?></p>

                                                        <div style="padding-left: 5px;font-color:#424548;font-weight: bold;font-size: 12px"><? echo substr($resultSett[$i][2], 0, 140) ?></div><a style="text-decoration:none; margin-top: 3px;padding-left: 5px;font-weight:bold;font-size: 8pt; color:red;" href="<?= site_url() ?>/training/workshop?id=<? echo $resultSett[$i][0]; ?>" target="_blank">Click for Details</a>
                                                    </td>


                                                    <?
                                                    $i++;
                                                }
                                                echo '</tr>';
                                            }
                                            ?>
                                    
                                    
                                    
                                </table>
                                <div >
                                            <a class="btn btn-warning" style="margin-left: 35%;" href="<?= site_url() ?>/training/workshop_all">Next Workshop</a>
                                            </div>
                                    </div>

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div style="border:1px solid #330066 ; background: #330066;color:#fff;border-radius: 3px;">
                                        <h3 style="text-align:center;"><i class="fa fa-clock-o"></i>  Consultant Hire  <i class="fa fa-clock-o"></i></h3>
                                        </div>
                                
                                <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                    <?php
                                error_reporting(0);

                                for ($i = 0; $i < 6;) {


                                    $var++;
                                    if (($var + 1) % 3 == 0) {
                                        echo '<tr style="background:#F1F2F3;border-bottom:1px solid #e6e6e6">';
                                    } else {
                                        echo '<tr style="background:#E7ECF2;border-bottom:1px solid #e6e6e6">';
                                    }

                                    for ($j = 0; $j < 2; $j++) {

                                       // echo '<td  valign=top style="width:10%">';
                                        //echo '<img src="' . base_url() . 'adminpanel/hier_job_images/' . $hire_jobs[$i]['img_name'] . '" width=80 height=80 />';
                                        //echo '</td>';
                                        if ($j == 2) {
                                            echo '<td valign=top>';
                                        } else {
                                            echo '<td valign=top style="border-right:1px solid #e6e6e6">';
                                        }
                                        echo '<span style=" font-weight:bold;font-family:calibri">' . $hire_jobs[$i]['name'] . '</span>' . '<br>';
                                        echo '<span style=" font-family:calibri; text-decoration:none;float:left">' . $hire_jobs[$i]['expert'] . '</a>' . '</span>' . '<br>';
                                        echo '<span style="font-family:calibri;float:left">' . $hire_jobs[$i]['free_time'] . '</span>' . '<br>';
                                        echo '<span style=" font-weight:bold;"><a href="' . site_url() . '/jobcategory/hire_job?ID=' . $hire_jobs[$i]['id'] . '" style="text-decoration:none;color:#006600;">Click here for Details</span>' . '<br>';
                                        echo '</td>';
                                        $i++;
                                    }

                                    echo '</tr>';
                                }
                                ?>
                                    
                                    </table>
                                 <div >
                                            <a class="btn btn-primary" style="margin-left: 35%;" href="<?= site_url() ?>/training/hirejob_all">See All</a>
                                            </div>
                                
                                    </div>
                            </div>



                </div>
            </div>

        </div>
        <div id="footer_bottom_navigation" style="height:150px;">
            <? $this->load->view("footer_bottom_navigation.php"); ?>
        </div>
        <div id="wrapper_bottom">
            <div style="float:right;margin-right: 20px"><p title="Design & Developed by PentaGems" style="color:#fff">Design & Developed by <a title="PentaGems"  style="text-decoration: none;color:#E6E600;font-weight: bold"target="_blank" href="http://pentagemsbd.com">PentaGems</a></p></div>
            <? //$this->load->view("footer_bottom_navigation.php"); ?>
        </div>
    </body>
</html>


<!--                                <div class="jobsearch">
                                                                    <span><img src="<?= base_url() ?>images/findjob.png"></span>
                                                                    <form action="<?= site_url() ?>/jobcategory/showkeywordJobs" method="post" target="_blank">
                                                                        <table border="0" style="border-collapse: collapse;border-style:none">
                                                                            <tr>
                                
                                                                                <td><span style=" font-family:century gothic;color: #000; font-weight: bold;">Looking For a Job?</span>&nbsp;<input type="text" name="keyword"  size="18" /></td>
                                                                                <td><input type="image" src="<?= base_url() ?>images/searchjob.png" /></td>
                                                                            </tr>
                                                                        </table>
                                                                    </form>
                                                                </div>-->