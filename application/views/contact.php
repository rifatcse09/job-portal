<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contact Azadijobs</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/same.css" />
    </head>
    <body>
        <div id="content_container">
            <div id="training_header">
                <a href="<?= base_url() ?>"><img src="<?= base_url() ?>images/pglogo.png"/></a>
            </div>              
            <div id="content">

                <div id="main_box">
                    <div id="header_box">
                   
                        <div id="header_box_training">
                            <div id="training_navigation">
                                <?= $this->load->view("mainpage_menu"); ?>
                            </div>
                        </div>

                    </div>
                    <div id="training_container">

                        <div id="training_content">
                     

                            <div style='padding:30px;'>
							
								<h3 style="font-family:cambria;">E-mail</h3>
									<p style="font-family:calibri;"> info@azadijobs.com </p>
									<h3 style="font-family:cambria;">Web address: </h3>
									<p style="font-family:calibri;">http://www.azadijobs.com/ </p>
									<h3 style="font-family:cambria;">For Technical Support </h3>
									<p style="font-family:calibri;">Email: support@azadijobs.com </p>
									<h3 style="font-family:cambria;">For Sales Support</h3>
									<p style="font-family:calibri;">Email: sales@azadijobs.com </p>
									<h3 style="font-family:cambria;">Address: </h3>
									<p style="font-family:calibri;">Azadi Technologies 
9 CDA Commercial Area 3rd Floor, Momin Road, Chittagong-4000</p>
									
							
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
         <div id="footer_bottom_navigation">
                          <?= $this->load->view("footer_bottom_navigation"); ?>
         </div>
    </body>
</html>
