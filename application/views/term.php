<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Terms & Condition Azadijobs</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/same.css" />
    </head>
    <body>
        <div id="content_container">
            <div id="training_header">
                <img src="<?= base_url() ?>images/pglogo.png"/>
            </div>              
            <div id="content">

                <div id="main_box">
                    <div id="header_box">
                   
                        <div id="header_box_training">
                            <div id="training_navigation">
                                <?= $this->load->view("micro_job_menu"); ?>
                            </div>
                        </div>

                    </div>
                    <div id="training_container">

                        <div id="training_content">
                     

                            <div style='text-align: left'>
                                <p style="margin-top:-2px; clear: both;text-align: center">
                                 <h3 style='text-align: center'> Terms & Condition</h3>
                                  
                                  <span style="font-size: 16px">
								  <div>
					                <h4> Disclaimer - Terms and condition</h4>
								  </div>
								  
								  This is a public site with free access and Azadijobs.com assumes no liability for the quality and genuineness of responses. Azadijobs.com is not liable for any information provided my any individual. The individual/company would have to conduct its own background checks on the bonafide nature of all responses.

Azadijobs.com will not be liable on account of any inaccuracy of information on this web site. It is the responsibility of the visitor to further research the information on the site. Any breach of privacy or of the information provided by the consumer to Azadijobs.com to be placed on the website by technical or any other means is not the responsibility of Azadijobs.com. Azadijobs.com does not guarantee confidentiality of information provided to it by any person acquiring/using all/any information displayed on the Azadijobs.com website.

                                  
								  <h4></br>Resume Display</br></h4>
								   </br>1. Azadijobs.com allows you to Post/Submit your resume in the Azadijobs.com website free of cost.

</br></br>2. The resume displayed can be updated free of cost.

</br></br>3. Azadijobs.com offers no guarantee, warranties that there would be a satisfactory response or any response once the resume is put on display.

</br></br>4.       Azadijobs.com neither guarantees nor offers any warranty about the credentials of the prospective employer/organization which down loads the information and uses it to contact the prospective employee.

</br></br>5.       Azadijobs.com would not be held liable for loss of any data technical or otherwise, information, particulars supplied by customers due to reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are  beyond Azadijobs.com reasonable control including but not to strikes, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.

</br></br>6.       It shall be sole prerogative and responsibility of the individual to check  the authenticity of all or any response received pursuant to the resume being displayed by Azadijobs.com for going out of station or in station for any job, interview and Azadijobs.com assumes no responsibility in respect thereof

</br></br>7.       Azadijobs.com reserves its right to reject any insertion or information/data provided by the user without assigning any reason.

</br></br>8.       This subscription is not transferable i.e. it is for the same person through out whole period.

</br></br>9.       Azadijobs.com has the right to make all such modifications/editing of resume in order to fit resume in database. 

<h4></br>Resume Flash</br></h4>

 </br>1.       Azadijobs.com offers no guarantee nor warranties that there would be a satisfactory response or any response once the resume is  zapped.

</br></br>2.       Azadijobs.com neither guarantees nor offers any warranty about the credentials of the prospective employer/organization which receives the information and uses it to contact the prospective employee.

</br></br>3.       It shall be sole prerogative and responsibility of the individual to check  the authenticity of all or any response received pursuant to the resume being zapped by Azadijobs.com for going out of station or in station for any job interview and Azadijobs.com assumes no responsibility in respect thereof.

</br></br>4.       The user shall have no right to demand any information regarding the organizations to whom the resume has been sent and Azadijobs.com would be in no legal or other obligation to disclose/reveal the particulars of the organizations.

</br></br>5.       Azadijobs.com reserves its right to reject any insertion or information/data provided by the user without assigning any reason. But in such an eventuality, any amount so paid for, shall be refunded to the user on pro-rata basis. 

				

<h4></br>RESUME DEVELOPMENT</br></h4>

 </br>1.       The user shall certify that the information/data supplied by it toAzadijobs.comis accurate and correct.

</br></br>2.       The user shall give a reasonable time to Azadijobs.com for development of resume.

</br></br>3.       Azadijobs.com would not be held liable for loss of any data technical or otherwise, information, particulars supplied by customers due to reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are  beyond Azadijobs.com reasonable control including but not to strikes, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.

</br></br>4.       Azadijobs.com reserves its right to reject any insertion or information/data provided by the user without assigning any reason. 



<h4></br>CLASSIFIED JOB LISTING (SINGLE)</br></h4>


 </br>1.       Azadijobs.com will allow the Subscriber/Recruiter to place the information only in classified section of the site Azadijobs.com

</br></br>2.       The insertion so displayed in the classified section of Azadijobs.com will be for a period of maximum 30 days, which period is subject to change without notice.

</br></br>3.       Azadijobs.com reserves its right to reject any insertion or information/data provided by the user without assigning any reason. In such an eventuality, any amount so paid for, shall be refunded to the user on a pro-rata basis.

</br></br>4.      Azadijobs.com has the right to make all such modifications/editing of the vacancy details in order to fit in database.

</br></br>5.       The Subscriber/Recruiter may use up to a maximum of 2 email id's for vacancies posted on Azadijobs.com in the Classified section to collect response.

</br></br>6.       Azadijobs.com may at its sole discretion include the vacancy intimated by client for display on Azadijobs.com in the print media through various media alliances in other print vehicles with no extra costs to the client. However, if a client wishes not to have its vacancies/requirement from appearing in print media, then the client shall specifically inform Azadijobs.com in writing accordingly at the time of intimating of the vacancy.

</br></br>7.       Azadijobs.com offers no guarantee nor warranty that there would be a satisfactory response or any response once the job is put on display.

</br></br>8.       Azadijobs.com shall in no way be held liable for any information provided by the applicant to the subscriber and it shall be the sole responsibility of the user to check, authenticate and verify the information/response received at its own cost and expense.

</br></br>9.      Azadijobs.com would not be held liable for any loss of  data, technical or otherwise, information, particulars supplied by the customers, due  the reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are  beyond Azadijobs.com reasonable control including but not to strike, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.

</br></br>10.   The subscriber/Recruiter must give an undertaking to Azadijobs.com that the jobs sought to be advertised on the classified section of Azadijobs.com are in existence, are genuine and that the subscriber has the authority to list the jobs.

</br></br>11.   The subscriber/Recruiter must give an undertaking to Azadijobs.com that there will be no fee charged from an applicant who responds to jobs advertised on the classified section of Azadijobs.com for processing of the application.

</br></br>12.   Azadijobs.com reserves its right to change the look, feel, design, prominence, depiction, classification of the classified section of Azadijobs.com at any time without assigning any reason and without giving any notice.


<h4></br>CLASSIFIED SUBSCRIPTIONS FOR VARIOUS PERIOD</br></h4>


 </br>1. Azadijobs.com will allow the Subscriber/Recruiter to place the information only in classified section of the site Azadijobs.com

</br></br>2.       Each  insertion so displayed in the classified section of Azadijobs.com shall be for a period of 30 days, which period is subject to change without notice.

</br></br>3.       Azadijobs.com reserves its right to reject any insertion or information/data provided by the user without assigning any reason.  On such an eventuality, any amount so paid for, shall be refunded to the user on pro-data basis.

</br></br>4.       Azadijobs.com has the right to make all such modifications/editing of the vacancy details in order to fit in database.

</br></br>5.       The Subscriber/Recruiter may use up to a maximum of 2 email id's for vacancies posted on Azadijobs.com in the Classified section to collect response.

</br></br>6.       Azadijobs.com may at its sole discretion include the vacancy intimated by client for display on Azadijobs.com in the print media through various media alliances in other print vehicles with no extra costs to the client. However, if a client wishes not to have its vacancies/requirement from appearing in print media, then the client shall specifically inform Azadijobs.com in writing accordingly at the time of intimating of the vacancy.

</br></br>7.       Azadijobs.com offers no guarantee nor warranties that there would be a satisfactory response or any response once the job is put on display.

</br></br>8.       Azadijobs.com shall in no way be held liable for any information provided by the applicant to the subscriber and it shall be the sole responsibility of the subscriber to check, authenticate and verify the information/response received at its own cost and expense.

</br></br>9.       Azadijobs.com would not be held liable for loss of any data technical or otherwise, information, particulars supplied by the customers due to the reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are  beyond Azadijobs.com reasonable control including but not to strike, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.

</br></br>10.   The subscriber/Recruiter must give an undertaking to Azadijobs.com that the jobs sought to be advertised on the classified section of are in existence, are genuine and that the subscriber has the authority to list the jobs.

</br></br>11.   The subscriber/Recruiter must give an undertaking to Azadijobs.com that there will be no fee charged from an applicant who responds to jobs advertised on the classified section of Azadijobs.com for processing of the application.

</br></br>12.   Azadijobs.com reserves its right to change the look, feel, design, prominence, depiction, classification of the classified section of Azadijobs.com at any time without assigning any reason and without giving any notice.

</br></br>13.   The subscriber to this service shall be entitled to un listings during the period of subscription.

</br></br>14.   This service is neither resalable nor transferable by the subscriber to any other person, corporate body, firm or individual.

</br></br>15.   Only  one office of one corporate entity / firm will be entitled to use this service and in case other offices of the same company/ associated companies, want to use the said service, then, they shall be liable to make extra payment for the service.

</br></br>16.   The contact information given by the subscriber for all listing should be the same and the subscriber cannot give multiple contact information/data  for the purpose of listing.

</br></br>17.   Only insertions with contact information registered with Azadijobs.com will be displayed on the site




<h4></br>Priority Jobs' (SINGLE)</br></h4>

</br> 1.       Azadijobs.com shall place the information relating to vacancies in the Priority Jobs' & Classified sections of Azadijobs.com .

</br></br>2.       The insertion so displayed at Azadijobs.com shall be for a period of 30 days, which period is subject to change without notice.

</br></br>3.       Azadijobs.com has the right to make all such modifications/editing of the vacancy details in order to fit in database.

</br></br>4.       Azadijobs.com reserves its right to reject any insertion or information/data provided by the user without assigning any reason.  In such an eventuality, any amount so paid for, shall be refunded to the user on a pro-data basis.

</br></br>5.     Azadijobs.com may at its sole discretion include the vacancy intimated by client for display on Azadijobs.com in the print media through various media alliances in other print vehicles with no extra costs to the client. However, if a client wishes not to have its vacancies/requirement from appearing in print media, then the client shall specifically inform Azadijobs.com in writing accordingly at the time of intimating of the vacancy.

</br></br>6.      Azadijobs.com offers no guarantee nor warranties that there would be a satisfactory response or any response once the job vacancy is put on display.

</br></br>7.       Azadijobs.com shall in no way be held liable for any information provided by  applicant(s) to the subscriber and it shall be the sole responsibility of the subscriber to check, authenticate and verify the information/response received at its own cost and expense.

</br></br>8.      Azadijobs.com would not be held liable for any loss of data technical or otherwise, information, particulars supplied by the customers due to the reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are  beyond Azadijobs.com reasonable control including but not limited to strike, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.

</br></br>9.       The subscriber/Recruiter must give an undertaking to Azadijobs.com that the jobs sought to be advertised on the `Priority Jobs'' and `Classified' sections of Azadijobs.com are in existence, genuine and the subscriber has the authority to list the jobs.

</br></br>10.   The subscriber/Recruiter must give an undertaking to Azadijobs.com that the there will be no fee charged from an applicant who responds to jobs advertised on the ‘Priority Jobs'’ and ‘Classified’ sections of Azadijobs.com for processing of the application.

</br></br>11.   Azadijobs.com reserves its right to change the look, feel, design, prominence, depiction, classification of the  `Priority Jobs'' and `Classified' sections of Azadijobs.com at any time without assigning any reason and without giving any notice. 



<h4></br>PRIORITY JOBS SUBSCRIPTIONS</br></h4>

 </br>1.       Azadijobs.com shall place the information relating to vacancies only in  `Priority Jobs'' and `Classified' sections of Azadijobs.com

</br></br>2.       The insertion so displayed in the classified section of Azadijobs.comshall be for a period of 30 days, which is subject to change without notice.

</br></br>3.      Azadijobs.com has the right to make all such modifications/editing of the vacancy details in order to fit in database.

</br></br>4.       Azadijobs.com reserves its right to reject any insertion or information/data provided by the user without assigning any reason.  In such an eventuality, any amount so paid for, shall be refunded to the user on a pro-data basis.

</br></br>5.       Azadijobs. commit at its sole discretion include the vacancy intimated by client for display on Azadijobs.com in the print media through various media alliances in other print vehicles with no extra costs to the client. However, if a client wishes not to have its vacancies/requirement from appearing in print media, then the client shall specifically inform Azadijobs.com in writing accordingly at the time of intimating of the vacancy.

</br></br>6.       Azadijobs.com offers no guarantee nor warranties that there would be a satisfactory response or any response once the job vacancy is put on display.

</br></br>7.       Azadijobs.com shall in no way be held liable for any information provided by  applicant(s) to the subscriber and it shall be the sole responsibility of the subscriber to check, authenticate and verify the information/response received at its own cost and expense.

</br></br>8.      Azadijobs.com would not be held liable for any loss of data technical or otherwise, information, particulars supplied by the customers due to   reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are  beyond Azadijobs.com reasonable control including but not limited to strike, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.

</br></br>9.       The subscriber/Recruiter must give an undertaking to Azadijobs.com that the jobs sought to be advertised on the  `Priority Jobs'' and `Classified' sections of Azadijobs.com are in existence, genuine and the subscriber has the authority to list the jobs.

</br></br>10.   The subscriber/Recruiter must give an undertaking to Azadijobs.com that there will be no fee charged from an applicant who responds to jobs advertised on the 'Priority Jobs' and ‘Classified’ sections of Azadijobs.com for processing of the application.

</br></br>11.Azadijobs.com reserves its right to change the look, feel, design, prominence, depiction, classification of the classified and/or Priority Jobs' section of Azadijobs.com at any time without assigning any reason and without giving any notice.

</br></br>12.   The subscriber to this service shall be entitled for unlimited listing during the period of subscription.

</br></br>13.   This service is neither resalable nor transferable by the subscriber to any other person, corporate body, firm or individual.

 
 

<h4></br>MANUAL SHORTLISTING</br></h4>

</br> 1.       Azadijobs.com agrees to provide the service to the subscriber only for the duration and the number of vacancies contracted for, to the best of its ability.

</br></br>2.       Azadijobs.com will receive, open, read and sort all resumes received in response to the vacancies for which the service is offered.

</br></br>3.       Sorting shall be done using the parameters for selection decided by the subscriber.

</br></br>4.   Azadijobs.com would not be held liable for any loss of data technical or otherwise, information, particulars supplied by the customers due to the reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are  beyond Azadijobs.com reasonable control including but not limited to strike, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.

</br></br>5.       It is made clear that the subscriber being one corporate/business entity, cannot transfer/sell/sublet the micro site or any part thereof to any third party 




<h4></br>DISPLAY OF BANNERS</br></h4>


 </br>1.     Azadijobs.com agrees to provide the service to the subscriber only for the duration or the number of impressions contracted for, to the best of its ability.

</br></br>2.      Azadijobs.com will display banners on the home page on a rotation basis, wherein banners from different subscribers are displayed in a sequential manner, managed by a smart software.

</br></br>3.       In case of “Run of the Web site” deals, Azadijobs.com reserves the right to choose the sections where the banners of a subscriber is to be displayed, at any point in time, during the period of the contract.

</br></br>4.       Azadijobs.com reserves its right to reject any insertion or information/data provided by the user without assigning any reason.  In such an eventuality, any amount so paid for, shall be refunded to the user on a pro-data basis.

</br></br>5.       Azadijobs.com offers no guarantee nor warranties that there would be a satisfactory response or any response once the banners are put on display.

</br></br>6.    Azadijobs.com would not be held liable for any loss of data technical or otherwise, information, particulars supplied by the customers due to the reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are  beyond Azadijobs.com reasonable control including but not to strike, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.

</br></br>7.       It is made clear that the subscriber being one corporate/business entity, cannot transfer/sell/sublet the micro site or any part thereof to any third party.




				
                                  </span>
                                
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
         <div id="footer_bottom_navigation">
                          <?= $this->load->view("footer_bottom_navigation"); ?>
         </div>
    </body>
</html>
