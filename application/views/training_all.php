<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Training Topics</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/same.css" />
         <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
        <link href="<?= base_url(); ?>mainpage_css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?= base_url(); ?>mainpage_css/font-awesome.css" rel="stylesheet" type="text/css"> 
<link href="<?= base_url(); ?>mainpage_css/animate.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>mainpage_css/style.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>mainpage_css/custom.css" rel="stylesheet" type="text/css">
        <style type="text/css">                 
        .styled-button-12 {
          
	background: #5B74A8;
	background: -moz-linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	background: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#5B74A8), color-stop(100%,#5B74A8));
	background: -webkit-linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	background: -o-linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	background: -ms-linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	background: linear-gradient(top,#5B74A8 0%,#5B74A8 100%);
	filter:DXImageTransform.Microsoft.gradient( startColorstr='#5B74A8',endColorstr='#5B74A8',GradientType=0);
	padding:2px 6px;
	color:#fff;
	font-family:'Helvetica',sans-serif;
	font-size:14px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:1px solid #1A356E
        
        }
        </style>
    </head>
    <body>
        <div id="content_container">
            <div id="job_category_header">
        <?= $this->load->view("jobsbycatagories_header.php"); ?>
    </div>              
            <div id="content" style="width:1000px;">

                <div id="main_box">
                    <div id="header_box">
                   
                        <div id="header_box_training">
                            <div id="training_navigation">
                                <?= $this->load->view("mainpage_menu"); ?>
                            </div>
                        </div>

                    </div>
                    <div id="training_container">

                        <div id="training_content">
                            <?
                            if (isset($_GET["page"])) {
                                $page = $_GET["page"];
                            } else {
                                $page = 1;
                            }
                            $start_from1 = ($page - 1) * 5;

                            //include("database.php");
                            //$id = $_GET['training_id'];
                            $sql = "select * from training order by training_id desc limit $start_from1, 5";
                            $data = mysql_query($sql);
                            while ($r = mysql_fetch_array($data)) {
                                ?>
                                <table style="width: 900px;margin-left: 50px;" class="table table-striped table-bordered table-hover dataTable no-footer"> 
                                    
                                    <tr>
                                        <td valign="top" ><img src="<?= base_url() ?>adminpanel/training_images/<? echo $r['name']; ?>" width="100" height="100" id="img"></td>
                                 
                                        <td>
                                            <p align="left" style="color: #006699;">
                                                <strong>Date : </strong><? echo date('d-m-Y',strtotime($r['training_date'])) ?></p>
                                            <p style="width: 80%;color: #006699;font-family: cambria;font-size: 12pt;">
                                               <span style="font-weight: bold;font-family:cambria">Title : </span> <? echo $r['title']; ?></p>
                                            <p style="text-align: left; float: left; margin-top: -80px;font-family:calibri;"><? echo substr($r['training_content'], 0, 300); ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                           <a class="btn btn-success btn-sm" style="float: right;text-decoration: none;font-size:12pt;margin-right: 30px;" href="<?= site_url() ?>/training/training_details?training_id=<? echo $r['training_id']; ?>" target="_blank"> Click Here for Details <i class="fa fa-hand-o-up fa-lg"></i></a></td>
                                    </tr>
                                    
                                </table>
                                <?
                            }
                            ?>

                            <div style='text-align: center'>
                                <p style="margin-top:-2px; clear: both;text-align: center">
                                    <?
                                    $sql2 = "select count(*) from training order by training_id desc";
                                    $p_result = mysql_query($sql2);
                                    $row2 = mysql_fetch_array($p_result);
                                    $tot_rec = $row2[0];
                                    $tot_page = ceil($tot_rec / 5);
                                    for ($i = 1; $i <= $tot_page; $i++) {
                                        if ($page == $i) {
                                            echo $i . ' ';
                                        } else {
                                            ?>

                                            <a style='text-decoration:none;color:green;' href = '<?= site_url() ?>/training/training_all?page=<?= $i ?>'><?= $i . " " ?></a>
                                            <?
                                        }
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
         <div id="footer_bottom_navigation" style="height:150px;">
                          <?= $this->load->view("footer_bottom_navigation"); ?>
         </div>
        
         <div id="wrapper_bottom">
             <div style="float:right;margin-right: 20px"><p title="Design & Developed by PentaGems" style="color:#fff">Design & Developed by <a title="PentaGems"  style="text-decoration: none;color:#E6E600;font-weight: bold"target="_blank" href="http://pentagemsbd.com">PentaGems</a></p></div>
                <? //$this->load->view("footer_bottom_navigation.php"); ?>
         </div>
    </body>
</html>
