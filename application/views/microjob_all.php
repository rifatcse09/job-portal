<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Micro Job Topics</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/same.css" />    
    </head>
    <body>
        <div id="content_container">
                <div id="training_header">
                    <img src="<?= base_url() ?>images/pglogo.png"/>
                </div>              
            <div id="content">
       
                <div id="main_box">
                    <div id="header_box">

                        <div id="header_box_training">
                            <div id="training_navigation">
                                <?= $this->load->view("jobsbycategory_menu"); ?>
                            </div>
                        </div>

                    </div>
                    <div id="training_container">

                        <div id="training_content">
                            <?
                            
                                 if (isset($_GET["page"])) {
                                    $page = $_GET["page"];
                                } else {
                                    $page = 1;
                                }
                                $start_from1 = ($page - 1) * 5;

                            //include("database.php");
                            //$id = $_GET['training_id'];
                            $sql = "select * from micro_job order by id desc limit $start_from1, 5";
                            $data = mysql_query($sql);
                            $num_row=  mysql_num_rows($data);
                            $i=0;
                            while ($r = mysql_fetch_array($data)) {
                                $i++;
                                ?>
                                <table width="100%"> 
                                    <tr>
                                        <td><p style="margin-left: 20px;color: #0099ff;font-family: Cambria (Headings);font-size: 14pt;"><? echo $r['job_title']; ?></p></td>
                                       
                                    </tr>
                                    <tr>
                                        <td>
                                            <p align="left" style="margin-left:20px;color: #0099ff;"><strong>Date : </strong><? echo $r['start_date']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                            
                                        <td><p style="text-align: left; float: left;margin-left: 20px "><? echo substr($r['subject'],0,300); ?></p></td>
                                    
                                        
                                    </tr>
                                    <tr>
                                        <td><p style="text-align: left; float: left;margin-left:20px;"><? echo substr($r['location'],0,300); ?></p></td></tr>
                                    <tr>
         
                                        <td><p style="text-align: left; float: left;margin-left:20px;"><? echo substr($r['description'],0,300); ?></p></td>
                                        
                                    </tr>
                                    <tr>
                                        <td><p style="text-align: left; float: left;margin-left:20px;"><? echo substr($r['budget'],0,300); ?></p></td></tr>
                                    <tr>
                                        <td colspan="2" align="right"><a style="float: right;text-decoration: none;color: #013b9f;font-size:12pt;margin-right: 30px;" href="<?= site_url() ?>/training/microjob_details?id=<?echo $r['id'];  ?>" target="_blank">Click Here for Details</a></td>
                                    </tr>
                                    
                                    <?if($i!=$num_row)
                                    {?>
                                    <tr>
                                        <td colspan="3" ><hr style="height: 2px; color: #c8d7dc;"></td>
                                    </tr>
                                    <?}?>
                                </table>
                                <?
                            }
                            ?>
                            
                            <div style='text-align: center'>
                            <p style="margin-top:-2px; clear: both;text-align: center">
                                <?
                                $sql2 = "select count(*) from micro_job order by id desc";
                                $p_result = mysql_query($sql2);
                                $row2 = mysql_fetch_array($p_result);
                                $tot_rec = $row2[0];
                                $tot_page = ceil($tot_rec / 5);
                                for ($i = 1; $i <= $tot_page; $i++) {
                                    if ($page == $i) {
                                        echo $i . ' ';
                                    } else {
                                        ?>

                                        <a style='text-decoration:none;color:green;' href = '<?=site_url()?>/mainpage/pagination_micro_job_all?page=<?=$i?>'><?= $i." "?></a>
                                    <?
                                        }
                                    
                                }
                                ?>
                            </p>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div id="footer_bottom_navigation">
                <?= $this->load->view("footer_bottom_navigation"); ?>
        </div>
         <div id="wrapper_bottom">
             <div style="float:right;margin-right: 20px"><p title="Design & Developed by PentaGems" style="color:#fff">Design & Developed by <a title="PentaGems"  style="text-decoration: none;color:#E6E600;font-weight: bold"target="_blank" href="http://pentagemsbd.com">PentaGems</a></p></div>
                <? //$this->load->view("footer_bottom_navigation.php"); ?>
 </div>
    </body>
</html>
