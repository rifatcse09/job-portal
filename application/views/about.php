<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>About Azadijobs</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/same.css" />
    </head>
    <body>
        <div id="content_container">
            <div id="training_header">
                <img src="<?= base_url() ?>images/pglogo.png"/>
            </div>              
            <div id="content">

                <div id="main_box">
                    <div id="header_box">
                   
                        <div id="header_box_training">
                            <div id="training_navigation">
                                <?= $this->load->view("micro_job_menu"); ?>
                            </div>
                        </div>

                    </div>
                    <div id="training_container">

                        <div id="training_content">
                     

                            <div >
                                <p style="margin-top:-2px; clear: both;text-align: left">
                                  AzadiJobs.com likes to bridge the Job seekers and the employers of Chittagong by furnishing the best local job information. Dainik Azadi, one of the oldest newspapers of Bangladesh, has initiated this user friendly local job portal with a view to providing information regarding the desired jobs and thus helps the local employers search their required Human Resources (HR).   </br>                           
                                  <span style="font-size: 16px"></br>AzadiJobs.com serves as the primary source of both local and expatriate talent to the largest employers and recruitment agencies across this region. This portal will always explore to provide the maximum benefits to the employers as well as the job seekers in addition to meeting up their primary demands. We would like to impart professional training on different fields of interests and contemporary issues to make the incumbents easily acceptable to any particular employer. We aim at maximizing the number of organizations in the region to use Online Job Advertisement facility, Online CV Bank Access to build a better Port City.
                                  </span>
								  
								  <table style="text-align: center">
								  <tr style='text-align: left'>
								  <td>Email:</td>
								  <td>info@azadijobs.com</td>
								  
								  </tr>
								  
								  <tr style='text-align: left'>
								  <td>Web address:</td>
								  <td><a href="http://www.azadijobs.com/ ">http://www.azadijobs.com/ 
                                   </a></td>
								   </tr>
								   
								   <tr style='text-align: left'>
								  <td>Address:</td></a>
								  <td>Azadi Technologies 
9 CDA Commercial Area 3rd Floor,</br> Momin Road, Chittagong-4000</td>
								  
								  </tr>
								  
								  
								  <tr style='text-align: left'>
								  <td>Technical Support 
Email: </td>
								  <td style="margin:52px">support@azadijobs.com 
                                   </td>
								   </tr>
								   
								   
								   <tr style='text-align: left'>
								  <td> Sales Support
Email: </td>
								  <td>sales@azadijobs.com  
                                   </td>
								   </tr>
								  
								  
								  
								  </table>
								  
                                
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
         <div id="footer_bottom_navigation">
                          <?= $this->load->view("footer_bottom_navigation"); ?>
         </div>
    </body>
</html>
