<style type="text/css">
    .h4
    {
        float:left;
        width:150px;
    }

    .moon  table 
    {
        float: left;
    }
    .moon   tr td
    {
        line-height:10px;
    }
    .moon tr td a 
    {
        color:#FFFFFF;
        font-family:Geneva, Arial, Helvetica, sans-serif;
        vertical-align:text-top;
        text-decoration:none;
        font-size:10px;}
    .moon   tr td a:hover
    {
        color:#e5b5b5;
        text-decoration:underline;
    }
    #footer_right
    {
        float: right;
        width: 250px;
        height: 120px;
        background: #fff;
        border-radius: 20px 0px 0px 20px;
        margin-top: -20px;
    }
    #footer_right_text
    {
        width: 100px;
    }
    #footer_right_text p
    {
        font-family: euphemia;
        font-size: 10pt;
        color: #000;
        margin-left: 50px;
    }
</style>  
<table width="955">
    <tr>
        <td valign="top"><h4 class="h4" style="margin:0 auto; color:#FFFFFF">Job Seekers</h4></td>
        <td></td>
        <td valign="top"><h4 class="h4" style="margin:0; color:#FFFFFF">Top Category</h4></td>
        <td></td>
        <td valign="top"><h4 class="h4" style="margin:0; color:#FFFFFF">Employers</h4></td>
        <td></td>
        <td valign="top"><h4 class="h4" style="margin:0; color:#FFFFFF">Resume Services</h4></td>
        <td></td>
    </tr>
    <tr>
        <td valign="top">
            <table class="moon">
                <tr>
                    <td><a href="#">Browse Jobs</a></td>
                </tr>
                <tr>
                    <td><a href="#">Job Search</a></td>
                </tr>
                <tr>
                    <td><a href="#">Register Now</a></td>
                </tr>
                <tr>
                    <td><a href="#">Login</a></td>
                </tr>
                <tr>
                    <td><a href="#">Company Profiles</a></td>
                </tr>
                <tr>
                    <td><a href="#">Career Advice</a></td>
                </tr>
                <tr>
                    <td><a href="#">Industry Information</a></td>
                </tr>
                <tr>
                    <td><a href="#">Industries</a></td>
                </tr>
                <tr>
                    <td><a href="#">Companies</a></td>
                </tr>
                <tr>
                    <td><a href="#">Shine Weekly</a></td>
                </tr>
                <tr>
                    <td><a href="#">Report Problem</a></td>
                </tr>
            </table>
        </td>
        <td style="margin:0;padding:0 6px;"><hr size="1" style="border-style:solid; border-color:#fcee21; height:160px;margin-top: -40px;" /></td>
        <td width="250px" valign="top">
            <table class="moon">
                <tr>
                    <td><a href="#">Admin/Mgt./Operations</a></td>
                </tr>  
                <tr>
                    <td><a href="#">Advertising Media</a></td>
                </tr>
                <tr>
                    <td><a href="#">Agri./Fish/Livestock</a></td>
                </tr>
                <tr>
                    <td><a href="#">Bank Insurance</a></td>
                </tr>
                <tr>
                    <td><a href="#">Commercial/Supply Chain</a></td>
                </tr>
                <tr>
                    <td><a href="#">BPO/Data Entry/Operator</a></td>
                </tr>
                <tr>
                    <td><a href="#">Engineering/Diploma</a></td>
                </tr>
                <tr>
                    <td><a href="#">Development Jobs</a></td>
                </tr>
            </table>
        </td>
        <td style="margin:0;padding:0 6px;"><hr size="1" style="border-style:solid; border-color:#fcee21; height:160px;margin-top: -40px;" /></td>
        <td>
            <table class="moon">
                <tr>
                    <td><a href="#">Post Job</a></td>
                </tr>
                <tr>
                    <td><a href="#">Single Job</a></td>
                </tr>
                <tr>
                    <td><a href="#">Multiple jobs</a></td>
                </tr>
                <tr>
                    <td><a href="#">Search Resume</a></td>
                </tr>
                <tr>
                    <td><a href="#">Executive Services</a></td>
                </tr>
                <tr>
                    <td><a href="#">Virtual Job Fair</a></td>
                </tr>
            </table>
        </td>
        <td style="margin:0;padding:0 6px;"><hr size="1" style="border-style:solid; border-color:#fcee21; height:160px;margin-top: -40px;" /></td>
        <td>
            <table class="moon">
                <tr>
                    <td><a href="#">Resume</a></td>
                </tr>
                <tr>
                    <td><a href="#">Resume Writing</a></td>
                </tr>
                <tr>
                    <td><a href="#">Resume Booster</a></td>
                </tr>
                <tr>
                    <td><a href="#">Career Development</a></td>
                </tr>
                <tr>
                    <td><a href="#">Company Names</a></td>
                </tr>
            </table>
        </td>
        <td>
            <div id="footer_right">
                <div id="footer_right_text">
                    <p><a href="#" style="text-decoration: none;color: #000;">Contact</a></p>
                    <img style="float: right;margin-right: -30px;margin-top: -10px;"src="<?= base_url()?>images/call.png"/>
                    <p><a href="#" style="text-decoration: none;color: #000;">Help</a></p>
                </div>
            </div>
        </td>
    </tr>
</table>
