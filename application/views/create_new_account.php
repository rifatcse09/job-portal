<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<head>
    <title> Job Details</title>
    <style type="text/css">
        #content_container
        {
            width: 100%;
            background: #f0f0f0;
        }
        #content
        {
            float: left;
            width: 80%;
            margin: 0% 10%;
            border-top: 10px solid #d7d7d7;
            border-right: 2px solid #d7d7d7;
            border-left: 2px solid #d7d7d7;
            border-bottom: 2px solid #d7d7d7;
            border-radius: 5px;
            background: #fff;

        }
        #content_content
        {
            float: left;
            width: 85%;
            margin-left: 10px;
            margin-bottom: 10px;
        }
        #content_add
        {
            float: right;
            width: 14%;
            height: 200px;
        }
        #job_category_header
        {
            float:left;
            width:600px;
            margin-left: 130px;
            margin-bottom: 20px;
        }
        #con1
        {
            float:left;
            width:96%;
            height: 45px;
            background-color: #000000;

        }
        #con1 p
        {
            color:#E6E6E6;
            margin-left: 200px;
        }
        #con2
        {
            float:left;
            width:96%;
            height: 35px;
            background-color: #A5090A;

        }
        #con3
        {
            float:left;
            width:96%;
            height: 55px;
            background-color: #A5090A;

        }
        #job_category_navigation
        {
            float:right;
            width:80%;
            height: 55px;
            background: #E6E6E6;
            border-radius:15px 15px 0px 0px;
            margin-right: 5px;


        }
        #job_category_navigation ul
        {
            margin: 0;
            padding: 0;
        }

        #job_category_navigation ul li
        {
            display: inline;

        }
        #job_category_navigation li a
        {
            margin-left: 20px;
            display: block;
            float: left;
            padding-top: 15px;
            padding-left: 5px;
            padding-right: 5px;
            color: #000;
            height: 40px;
            text-decoration: none;
            font-size: larger;

            background:#E6E6E6;
        }
        #job_category_navigation li a:hover {background:#B3B3B3; }


        #header
        {
            float: left;
            width: 35%;
            height: 35px;
            background: #A5090A;
            border-radius: 20px 20px 0px 0px;
            margin-top: 35px;
        }
        #main_box
        {
            float: left;
            width: 94.9%;
            border: 5px solid #a5090a;
        }
        #header p
        {
            color: #fff;
            margin-left: 15px;
            margin-top: 5px;
            font-weight: bold;
            font-family: arial;
        }
        #header2
        {
            float: left;
            width: 100%;
            height: 55px;
            background: #000;
            margin-top: -5px;
        }
        #header2_left
        {
            float: left;
            background-color: #A5090A;
            width: 135px;
            height: 45px;
            margin-left: 30px;
            border-radius: 0px 0px 20px 20px;
        }
        #header2_left p
        {
            color:#fff;
            margin-left: 15px;
            margin-top: 5px;
            font-weight: bold;
            font-family: arial;
            padding-top: 10px;
            padding-left: 10px;
        }
        #header2_right
        {
            float: right;
            margin-right: 10px;
        }
        #header2_right p
        {
            font-size: 10pt;
            color: #333333;
        }
        #header3
        {
            float: left;
            width: 100%;
            height: 35px;
            background: #a5090a;

        }
        #header3 p
        {
            color: #fff;
            font-weight: bold;
            margin-top: 5px;
            margin-left: 10px;
            font-family: arial;
        }
        #job_details_container
        {
            float: left;
            width: 100%;
            background: #fff;
        }
        #job_details_content
        {

            background: #D8B5B6;
            width: 40%;
            float: left;

        }
        #job_details_content ul{

        }
        #job_details_content ul li {
            text-align:right;
            margin-top: 5px;
            padding-left: 0px;
            font-family: verdana;
            font-size: 12px;
            color: #0E0E0E;
            border-bottom: 1px #CC8D8E solid;

            list-style:none;
            padding-right: 15px;
            margin-left: -40px;
            height: 30px;
        }


        #job_details_content_box
        {

            background: #D8B5B6;
            width: 60%;
            float: left;


        }
        #job_details_content_box ul{

        }
        #job_details_content_box ul li {
            text-align:left;
            margin-top: 5px;
            padding-left: 0px;
            font-family: verdana;
            font-size: 12px;
            color: #0E0E0E;
            border-bottom: 1px #CC8D8E solid;

            list-style:none;
            padding-right: 15px;
            margin-left: -50px;
            height: 30px;
        }



        #job_details_bottom
        {
            float: left;
            width: 80%;
            margin: 0% 10%;
        }
        .BDJTopMenuNormal {
            color: rgb(98, 127, 242);
            font-family: verdana;
            font-size: 10px;
            font-weight: bold;
            text-decoration: none;
        }
        #tdcornertl {
            background-color: silver;
            border-radius: 5px 5px 0px 0px;
        }

        .BDJFormTitle {
            color: rgb(53, 82, 183);
            font-family: Verdana;
            font-size: 14px;
            font-weight: bold;
        }
        .BDJGrayArial11px {
            color: rgb(102, 102, 102);
            font-family: arial;
            font-size: 11px;
            text-decoration: none;
        }
        .BDJtabNormal {
            color: #a5090a;
            font-family: euphemia;
            font-size: 13px;
            font-weight: bold;
        }
        #footer_bottom_navigation
        {
            float:left;
            width:100%;
            margin-top: 20px;
            margin-bottom: 30px;
            margin-left: 170px;
        }
        #footer_bottom_navigation ul
        {
            margin: 0;
            padding: 0;
        }
        #footer_bottom_navigation ul li
        {
            display: inline;
        }
        #footer_bottom_navigation li a
        {
            display: block;
            float: left;
            padding:0px 10px;
            color: #b12929;
            text-decoration: none;
            border-right: 2px solid #b12929;
            font-family: euphemia;
            font-size: 10pt;
            font-weight: bold;
        }
        #footer_bottom_navigation li a:hover 
        {
            color:#49453a;
            text-decoration: underline;
        }
        #footer_end
        {
            float: left;
            width: 100%;
            height: 10px;
            background: #000;
        }
        #footer_last_navigation
        {
            float:right;
            width:800px;
            margin-top: 5px;
        }
        #footer_last_navigation ul
        {
            margin: 0;
            padding: 0;
        }
        #footer_last_navigation ul li
        {
            display: inline;
        }
        #footer_last_navigation li a
        {
            display: block;
            float: left;
            padding:0px 10px;
            color: #b12929;
            text-decoration: none;
            border-right: 1px solid #b12929;
            font-family: euphemia;
            font-size: 7pt;
        }
        #footer_last_navigation li a:hover {color:#d6a201; }
        #footer_last
        {
            float: left;
            width: 100%;
            height: 35px;
            background: #3d3729;
            margin-top: 20px;
            margin-bottom: 30px;
        }


    </style>
</head>
<body>
    <div id="content_container">
        <div id="job_category_header">
            <?= $this->load->view("jobsbycatagories_header"); ?>
        </div>
        <div id="content">
            <div id="content_content">
                <div id="con1">
                    <p>You must fill up at least first and second step to submit your resume</p>
                </div>
                <div id="con2">

                </div>
                <div id="con3">
                    <div id="job_category_navigation">
                        <ul>
                            <li><a   href="#">Personal</a></li>
                            <li><a href="#">Education</a></li>
                            <li><a href="#">Employment</a></li>
                            <li><a href="#">Others</a></li>
                            <li><a href="#">Photograph</a></li>

                        </ul>
                    </div>
                </div>


                <div id="main_box">
                    <div id="header2">
                        <div id="header2_left">
                            <p>Personal</p>
                        </div>
                    </div>
                    <!--                    <div id="header3">
                                            <p><?= $main_job['job_title'] ?></p>
                                        </div>-->
                    <div id="job_details_container">
<!--                        <table id="job_details_content">
                            <tr>
                                <td width="52%" valign="top" height="21">
                                    <span class="BDJtabNormal">No. of Vacancies :</span>
                                </td>
                                <td width="48%" valign="top">
                        <?= $main_job['no_vacancy'] ?>
                                </td>
                            </tr>
                        </table>-->
                        <br/>
                        <table id="job_details_content">
 <!--                            <tr>
                                 <td>
                                     <span class="BDJtabNormal"> Job Description / Responsibility : </span>
                                 </td>
                             </tr>-->
                            <tr>
                                <td>

                                    <ul> 
                                        <li>Name:</li>
                                        <li>Father's Name:</li>
                                        <li>Mother's Name:</li>
                                        <li>Date of Birth:</li>
                                        <li>Gender:</li>
                                        <li>Nationality:</li>
                                        <li>Present Address:</li>
                                        <li>Permanent Address:</li>
                                        <li>Religion:</li>
                                        <li>Current Location:</li>
                                        <li>Home Phone:</li>
                                        <li>Mobile:</li>
                                        <li>Office Phone:</li>
                                        <li>Email:</li>
                                        <li>Alternate Email:</li>

                                    </ul>


                                </td>
                            </tr>
                        </table>
                        <table id="job_details_content_box">
<!--                            <tr>
                                <td>
                                    <span class="BDJtabNormal"> Job Description / Responsibility : </span>
                                </td>
                            </tr>-->
                            <tr>
                                <td>

                                    <ul> 
                                        <li><input type="text" style="width:300px;"/></li>
                                        <li><input type="text" style="width:300px;"  /></li>
                                        <li><input type="text" style="width:300px;"/></li>
                                        <li><input type="text"/></li>
                                        <li><input type="text"/></li>
                                        <li><input type="text" style="width:300px;"/></li>
                                        <li><input type="text" style="width:300px;"/></li>
                                        <li><input type="text"/></li>
                                        <li><input type="text"/></li>
                                        <li><input type="text"/></li>
                                        <li><input type="text"/></li>
                                        <li><input type="text"/></li>
                                        <li><input type="text"/></li>
                                        <li><input type="text"/></li>
                                        <li><input type="text"/></li>

                                    </ul>


                                </td>
                            </tr>
                        </table>

<!--                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Job Nature :</span>
                                </td>
                                <td><?= $main_job['job_type'] ?></td>
                            </tr>


                        </table>-->
                        <br/>
<!--                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Educational Requirements :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                        <?
                        $edu_array = explode("\n", $main_job['edu_qualification']);
                        foreach ($edu_array as $edu) {
                            ?>
                                            <ul>
                                                <li><?= $edu ?></li>
                                            </ul>
                            <?
                        }
                        ?>
                                </td>
                            </tr>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Experience Requirements :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul><?
                        if ($main_job['experience_min'] >= 0 && $main_job['experience_max'] >= 0) {
                            echo '<li>' . $main_job['experience_min'] . ' to ' . $main_job['experience_max'] . ' year(s).</li>';
                        } else if ($main_job['experience_min'] >= 0 && $main_job['experience_max'] < 0) {
                            echo '<li>Minimum ' . $main_job['experience_min'] . ' year(s).</li>';
                        } else if ($main_job['experience_min'] < 0 && $main_job['experience_max'] >= 0) {
                            echo '<li>Maximum ' . $main_job['experience_min'] . ' year(s).</li>';
                        } else if ($main_job['experience_min'] < 0 && $main_job['experience_max'] < 0) {
                            echo '<li>N/A</li>';
                        }
                        if ($business_types) {
                            if (count($business_types)) {
                                echo '<li>The applicants should have experience in the following area(s):<br />';
                                foreach ($business_types as $business_type) {
                                    echo $business_type['business_name'];
                                }
                                echo '</li>';
                            }
                        }
                        if ($business_areas) {
                            if (count($business_areas)) {
                                echo '<li>The applicants should have experience in the following business area(s):<br />';
                                foreach ($business_areas as $business_area) {
                                    echo $business_area['area_name'];
                                }
                                echo '</li>';
                            }
                        }
                        ?>

                                    </ul>
                                </td>
                            </tr>
                        </table>-->

<!--                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Additional Job Requirements :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul>
                        <?
                        $add_req_array = explode("\n", $main_job['add_job_requirement']);
                        foreach ($add_req_array as $edu) {
                            ?>
                                                <li><?= $edu ?></li>
                            <?
                        }
                        ?>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Salary Range :</span>
                                </td>
                            </tr>
                        <?
                        if ($main_job['optBenefits'] != 2) {
                            ?>
                                    <tr>
                                        <td>
                                            <ul>
                            <?
                            if ($main_job['optBenefits'] == 1) {
                                echo '<li>Negotiable</li>';
                            } else if ($main_job['optBenefits'] == 3) {
                                if ($main_job['salary_min'] > 0 && $main_job['salary_max'] > 0) {
                                    echo '<li>' . $main_job['salary_min'] . ' to ' . $main_job['salary_max'] . ' BDT.</li>';
                                } else if ($main_job['salary_min'] > 0 && $main_job['salary_max'] <= 0) {
                                    echo '<li>' . $main_job['salary_min'] . ' BDT.</li>';
                                } else if ($main_job['salary_min'] <= 0 && $main_job['salary_max'] > 0) {
                                    echo '<li>' . $main_job['salary_max'] . ' BDT.</li>';
                                }
                            }
                            ?>
                                            </ul>
                                        </td>
                                    </tr>
                            <?
                        }
                        ?>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Other Benefits :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul>
                        <?
                        $opt_benefits = explode("\n", $main_job['other_benefits']);
                        foreach ($opt_benefits as $opt) {
                            ?>
                                                <li><?= $opt ?></li>
                            <?
                        }
                        ?>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Job Location :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul>
                                        <li>Anywhere in Bangladesh.</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <table id="job_details_content">
                            <tr>
                                <td>
                                    <span class="BDJtabNormal">Job Source :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul>
                                        <li>Suprovat Online Job Posting.</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        <div id="job_details_bottom">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                <form method="post" action="jobapply.asp" name="form1"></form>
                                <tbody>
                                    <tr class="body">
                                        <td align="center" style="padding-bottom:10px;">
                                            <input id="BDJButton5" type="submit" border="0" value="Apply Online" style="background-image:url(images/buttons/btn_icon_apply.gif)" name="Submit1">
                                            <br>
                                            or
                                            <br>

                                            Send your CV to
                                            <strong> <?= $main_job['email_id'] ?></strong>
                                            <br>

                                            <strong>Special Instruction :</strong>
                        <?= $main_job['apply_instruction'] ?>
                                            <br>

                                            <strong>
                                                <tr class="body">
                                                    <td align="center">
                                                        <strong class="BDJNoticeVerdana11Red">Application Deadline: </strong>
                                                        <span style="color:#FF0000"> <?= date("d F,Y", strtotime($main_job['deadline'])) ?></span>
                                                    </td>
                                                </tr>
                                                <tr class="body">
                                                    <td align="center" style="padding-top:10px;">
                                                        <span><?= $main_job['job_title'] ?></span>
                        <?
                        if ($main_job['hide_address'] == 'n') {
                            ?>    
                                                                <br>
                                                                <span>Company Address:</span>    
                                                                <span class="BDJGrayArial11px"><?= $com_info['company_address'] ?></span>
                                                                <br>

                                                                <span>Web : </span>
                                                                <a href="<?= $com_info['website_address'] ?>"><?= $com_info['website_address'] ?></a>
                                                                <br>
                                                                <span class="BDJGrayArial11px">
                                                                    <span style="color: #003399" <strong><?= $com_info['business_description'] ?></strong>
                                                                </span>
                            <?
                        }
                        ?>
                                                    </td>
                                                </tr>
                            </table>  -->
                    </div>
                    <div id="footer_bottom_navigation">
                        <?= $this->load->view("jobsbycategory_bottom_link.php"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="content_add"></div>
    </div>

    <div id="footer_last_navigation">
        <?= $this->load->view("footer_bottom_navigation"); ?>
    </div>

</div>
<div id="footer_last"></div>
</body>

</html>
