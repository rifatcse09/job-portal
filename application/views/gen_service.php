<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Privacy Policy Azadijobs</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/same.css" />
    </head>
    <body>
        <div id="content_container">
            <div id="training_header">
                <img src="<?= base_url() ?>images/pglogo.png"/>
            </div>              
            <div id="content">

                <div id="main_box">
                    <div id="header_box">
                   
                        <div id="header_box_training">
                            <div id="training_navigation">
                                <?= $this->load->view("micro_job_menu"); ?>
                            </div>
                        </div>

                    </div>
                    <div id="training_container">

                        <div id="training_content">
                     

                            <div >
                                <p style="margin-top:-2px; clear: both;>
                                  
								  
                                  
                                  <span style="font-size: 16px">
								  
                                 <h3 style=text-align:"center">General Services</h3>
								  <h4></br>Online Job Vacancy Announcement at Classified/Category Section of www.azadijobs.com</h4>
								   </br> In this section, employers/recruiters will have the opportunity to post job in different category. As soon as employers post a job in www.azadijobs.com, the job becomes accessible by the job seeker. 
								   What the employer needs to do is- just log on www.azadijobs.com and simply create a employer account of the organization and post the job under the company account.
                                   
								   <h4>Features of the service</h4> 
								   <ol type="1">
								   <li>The posted job vacancies will stay for maximum 30 days long unless the posted job vacancies ending date below the maximum days. </li>
                                   <li>There are three ways an employers receive job application through online. These are online account of the employer, email and traditional methds like Currier, mail, fax etc.</li>
                                   <li>Employer will be able to view, short list, print application/resume if the employer want to get it through online.</li> 
                                    </ol>
									<br><b>Service Fee: BDT 500 (For each job position)</b>

							
                              <h4></br>Corporate Membership</h4>
							  <br> Being a corporate member, you will be able to post unlimited number of jobs in different category. There will be no restriction whatsoever and posting job vacancies will be visible as soon as the corporate employer posts a job. There are number of facilities in www.azadijobs.com for corporate member.
                              <br><ol type="1">
							     
                             <li>Post unlimited job vacancy</li>
                             <li> View applicant/resume</li>
                              <li> Print applicant</li>
                             <li> Email them if necessary</li>
							 <li> make short list (By using different option, data & keyword)</li>
                             <li>Modify your account information</li>
                             <li>Modify Job vacancy</li>
                             <li>Repost Job vacancy</li>
                             <li>Payment status</li>
    
							  
							        </ol>
                           <b>Service Fee:(For one year) 	BDT: 10000</b>
						   <h4></br>Priority Jobs</h4>
						   <p>Priority jobs announcement make job advertisement stand out from hundreds of job announcements published in azadijobs.com. 
						   It is a unique service that offer job publish in both priority and category section. This might be the best way to promote your respective organization. </p>
						   <h5></br>Features</h5>
						   <ol type="1">
						   <li>Job announcement will be published with company logo and name in front page.</li>
						   <li>Priority jobs will be displayed for maximum 15 days unless job ending date below the maximum days.</li>
						   <li>We offer a fully dynamic customize job detail page for priority job where logo and organization name at the home page will be linked.</li>
						   <li>Priority jobs announcement will also be displayed in category for maximum 30 days unless post ending date below the maximum days.</li>
						 
						    </ol>
						   <b>Service Fee:</b>
						   <ol type="1">
						   <li>BDT: 2000 (For Single job)</li>
						   <li> BDT: 1500 (for each job if two job posted at a time)</li>
						   <li> BDT: 1000 (for each job if three or more jobs posted at a time)</li>
						   </ol>
						   
						   
						  <h4></br> Banner Advertisement at different pages of www.azadijobs.com </h4>
						<p>In the home page of azadijobs.com offer threes different type of banner advertisement in three different position.</p>
                         <h4>Top Banner(2 fixed banner)</h4> 						
						<ol type="1">
						<li>Banner size: 450x60px</li>
						<li>Banner file size: 25kb-40kb</li>
						<li>Price: BDT: 15000 (Per Month)</li>
						</ol>	  
						<h4>Right Side Banner Full (Unlimited)</h4>
						<ol type="1">
						<li>Banner size: 250x60px</li>
						<li>Banner file size: 15-20 kb</li>
						<li>Price: BDT: 10000 (Per Month)</li>
						</ol>
                         <h4>Right Side Banner Half (Unlimited)</h4>						
						<ol type="1">
						<li>Banner size: 120x60px</li>
						<li>Banner file size: 15kb</li>
						<li>Price: BDT: 8000 (Per Month)</li>
						</ol>	  
						<p>Note: Animated banner advertisement (3 to 4 frames) and Banner will be designed by azadijobs.com without additional cost. </p>		  
								  
                                  </span>
                              </p>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
         <div id="footer_bottom_navigation">
                          <?= $this->load->view("footer_bottom_navigation"); ?>
         </div>
    </body>
</html>
