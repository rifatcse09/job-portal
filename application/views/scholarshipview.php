<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<head>
    <title> Job Details</title>
    <style type="text/css">
         #content_container
        {
            width: 100%;
            background: #f0f0f0;
        }
        #content
        {
            float: left;
            width: 80%;
            margin: 0% 10%;
            border-top: 10px solid #006699;
            border-right: 2px solid #006699;
            border-left: 2px solid #006699;
            border-bottom: 2px solid #006699;
            border-radius: 5px;
            background: #fff;

        }
        #content_content
        {
            float: left;
            width: 80%;
            margin-left: 10px;
            margin-bottom: 10px;
        }
        #content_add
        {
            float: left;
             margin-top: 0;
            padding-top: 1%;
            width: 10%;
        }
        #job_category_header
        {
            float:left;
            width:600px;
            margin-left: 130px;
            margin-bottom: 20px;
        }
        #job_category_navigation
        {
            float:left;
            width:95%;
            height: 30px;
            background: #0099ff;
            border: 5px solid #0099ff;
            margin-right: 5%;
        }
        #job_category_navigation ul
        {
            margin: 0;
            padding: 0;
        }

        #job_category_navigation ul li
        {
            display: inline;
        }
        #job_category_navigation li a
        {
            display: block;
            float: left;
            padding:6px 10px;
            color: #FFFFFF;
            text-decoration: none;
            border-right: 2px solid #fff;
            background:#0099ff;
        }
        #job_category_navigation li a:hover {background:#21201e; }
        #header
        {
            float: left;
            width: 35%;
            height: 35px;
            background: #A5090A;
            border-radius: 10px 10px 0px 0px;
            margin-top: 35px;
        }
        #main_box
        {
            float: left;
            width: 94.9%;
            border: 5px solid #0099ff;
        }
        #header p
        {
            color: #fff;
            margin-left: 15px;
            margin-top: 5px;
            font-weight: bold;
            font-family: arial;
        }
        #header2
        {
            float: left;
            width: 100%;
            height: 35px;
        }
        #header2_left
        {
            float: left;
        }
        #header2_left p
        {
            color: #a5090a;
            margin-left: 15px;
            margin-top: 5px;
            font-weight: bold;
            font-family: arial;
        }
        #header2_right
        {
            float: right;
            margin-right: 10px;
        }
        #header2_right p
        {
            font-size: 10pt;
            color: #333333;
        }
        #header3
        {
            float: left;
            width: 100%;
            height: 35px;
            background: #a5090a;

        }
        #header3 p
        {
            color: #fff;
            font-weight: bold;
            margin-top: 5px;
            margin-left: 10px;
            font-family: arial;
        }
        #job_details_container
        {
            float: left;
            width: 100%;
            background: #fff;
        }
        #job_details_content
        {
            margin-left: 10px;
        }
        #job_details_content li {
            margin-top: 5px;
            padding-left: 0px;
            font-family: verdana;
            font-size: 12px;
            color: #0E0E0E;
        }
        #job_details_bottom
        {
            float: left;
            width: 80%;
            margin: 0% 10%;
        }
        .BDJTopMenuNormal {
            color: rgb(98, 127, 242);
            font-family: verdana;
            font-size: 10px;
            font-weight: bold;
            text-decoration: none;
        }
        #tdcornertl {
            background-color: silver;
            border-radius: 5px 5px 0px 0px;
        }

        .BDJFormTitle {
            color: rgb(53, 82, 183);
            font-family: Verdana;
            font-size: 14px;
            font-weight: bold;
        }
        .BDJGrayArial11px {
            color: rgb(102, 102, 102);
            font-family: arial;
            font-size: 11px;
            text-decoration: none;
        }
        .BDJtabNormal {
            color: #a5090a;
            font-family: euphemia;
            font-size: 13px;
            font-weight: bold;
        }
        #footer_bottom_navigation
        {
            float:left;
            width:100%;
            margin-top: 20px;
            margin-bottom: 30px;
            margin-left: 170px;
        }
        #footer_bottom_navigation ul
        {
            margin: 0;
            padding: 0;
        }
        #footer_bottom_navigation ul li
        {
            display: inline;
        }
        #footer_bottom_navigation li a
        {
            display: block;
            float: left;
            padding:0px 10px;
            color: #b12929;
            text-decoration: none;
            border-right: 2px solid #b12929;
            font-family: euphemia;
            font-size: 10pt;
            font-weight: bold;
        }
        #footer_bottom_navigation li a:hover 
        {
            color:#49453a;
            text-decoration: underline;
        }
        #footer_end
        {
            float: left;
            width: 100%;
            height: 10px;
            background: #000;
        }
        #footer_last_navigation
        {
               float:right;
                width:100%;
                margin-top: 5px;
                margin-right: 10%;
                margin-bottom:5%;
        }
        #footer_last_navigation ul
        {
            margin: 0;
            padding: 0;
        }
        #footer_last_navigation ul li
        {
            display: inline;
        }
        #footer_last_navigation li a
        {
            display: block;
            float: right;
            padding:0px 10px;
            color: #b12929;
            text-decoration: none;
            border-right: 1px solid #b12929;
            font-family: euphemia;
            font-size: 7pt;
        }
        #footer_last_navigation li a:hover {color:#d6a201; }
        #footer_last
        {
            float: left;
            width: 100%;
            height: 35px;
            background: #3d3729;
            margin-top: 20px;
            margin-bottom: 30px;
        }


    </style>
</head>
<body>
    <div id="content_container">
        <div id="job_category_header">
            <?= $this->load->view("jobsbycatagories_header"); ?>
        </div>
        <div id="content">
            <div id="content_content">
                <div id="job_category_navigation">
                    <?= $this->load->view("jobsbycategory_menu"); ?> 
                </div>
                <div id="main_box">
                    <?
                    include("database.php");
                    $id= $_GET['s_id'];
                    $sql = "select * from  scholarship where s_id=".$id;
                    $data = mysql_query($sql);
                    while ($r = mysql_fetch_array($data)) {
                        ?>
                        <tr>
                        <p style="color: #a5090a; width: 800px; margin-top: 0px; margin-left: 10px;"><? echo $r['scholarship_name']; ?></p><hr> <br>
                        <td valign="top"><a style="margin-top: 0px; margin-left: 10px; text-decoration: none;" target="_blank" href="<?= base_url() ?>adminpanel/scholarship_images/<? echo $r['name']; ?>">View</a></td>
                        </tr>
                        <?
                    }
                    ?>
                </div>
            </div>
            <div id="content_add">
                <?
                include("database.php");
                $sql = "select * from  advertisement where advertisement_type='jobdetails' ORDER BY add_id DESC LIMIT 0,3";
                $data = mysql_query($sql);
                while ($r = mysql_fetch_array($data)) {
                    ?>
                    <tr>
                        <td valign="top"><a href="<? echo $r['website']; ?>" target="_blank"><img src="<?= base_url() ?>adminpanel/advertise_images/<? echo $r['name']; ?>" width="130" height="120" style="margin:2px 0px 0px 0px;" /></a></td>
                    </tr>
                    <?
                }
                ?>
            </div>
        </div>

        <div id="footer_last_navigation">
            <?= $this->load->view("footer_bottom_navigation"); ?>
        </div>

    </div>
   
</body>

</html>
