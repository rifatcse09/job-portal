<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hire Job Topics</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>mainpage_css/same.css" />
    </head>
    <body>
        <div id="content_container">
                <div id="training_header">
                    <img src="<?= base_url() ?>images/pglogo.png"/>
                </div>              
            <div id="content">
<!--                    <img style="width:100%;" src="<?= base_url() ?>images/purbodeshjobs.png"/>             -->
                <div id="main_box">
                    <div id="header_box">
<!--                            <div id="training_container_header">
                                <img style="width:160%" src="<?= base_url() ?>images/purbodeshjobs.png"/>
                            </div>-->
                        <div id="header_box_training">
                            <div id="training_navigation">
                                <?= $this->load->view("micro_job_menu"); ?>
                            </div>
                        </div>
<!--                        <div id="training_container_header">
                                <img src="<?= base_url() ?>images/purbodeshjobs.png"/>
                        </div>-->
                    </div>
                    <div id="training_container">

                        <div id="training_content" style="margin-bottom: 20px">
                            <div style='float:left'><img width='150' height='150' style='margin-top:10px;margin-left: 10px' src='<?=base_url()?>adminpanel/hier_job_images/<?=$hire_job['img_name']?>'></div>
                            <div style='float:left;margin-left:20px'>
                                <table id="table" style="width:100%;margin-top: 0px" align='center'>
                                     
                                    <tr>
                                       <td style='font-family:century Gothic;font-weight: bold;padding-bottom: 20px'>Name</td>
                                       <td  style="vertical-align: top;padding-bottom: 20px">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $hire_job['name'];?></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       <td style='font-family:century Gothic;font-weight: bold;padding-bottom: 20px'>Expert In</td>
                                       <td  style="vertical-align: top;padding-bottom: 20px">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $hire_job['expert'];?></td>
                                       
                                    </tr>            
                                     <tr>
                                       <td style='font-family:century Gothic;font-weight: bold;padding-bottom: 20px'>Expert Area</td>
                                       <td  style="vertical-align: top;padding-bottom: 20px">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $hire_job['expert_area'];?></td>
                                       
                                    </tr>
                                    
                                      <tr>
                                       <td style='font-family:century Gothic;font-weight: bold;padding-bottom: 20px'>Rate</td>
                                       <td  style="vertical-align: top;padding-bottom: 20px">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $hire_job['rate'];?></td>
                                       
                                    </tr>
                                    <tr>
                                       <td style='font-family:century Gothic;font-weight: bold;padding-bottom: 20px'>Free</td>
                                       <td  style="vertical-align: top;padding-bottom: 20px">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $hire_job['free_time'];?></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       <td style='font-family:century Gothic;font-weight: bold;padding-bottom: 20px'>Mobile</td>
                                       <td  style="vertical-align: top;padding-bottom: 20px">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $hire_job['mobile'];?></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       <td style='font-family:century Gothic;font-weight: bold;padding-bottom: 20px'>Email</td>
                                       <td  style="vertical-align: top;padding-bottom: 20px">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $hire_job['email'];?></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       <td style='font-family:century Gothic;font-weight: bold'>Location</td>
                                       <td style="vertical-align: top;padding-bottom: 20px">:</td>
                                       <td style='font-family:century Gothic;padding-bottom: 20px'><?= $hire_job['location'];?></td>
                                       
                                    </tr>
                                     
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
           
        </div>
         <div id="footer_bottom_navigation">
                <?= $this->load->view("footer_bottom_navigation"); ?>
            </div>
         <div id="wrapper_bottom">
             <div style="float:right;margin-right: 20px"><p title="Design & Developed by PentaGems" style="color:#fff">Design & Developed by <a title="PentaGems"  style="text-decoration: none;color:#E6E600;font-weight: bold"target="_blank" href="http://pentagemsbd.com">PentaGems</a></p></div>
                <? //$this->load->view("footer_bottom_navigation.php"); ?>
 </div>
    </body>
</html>
