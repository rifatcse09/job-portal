<div class="marquee" id="mycrawler2">
<a href="#"><img src="images/animation/1.png" /></a><a href="#"><img src="images/animation/02.png" /></a> <a href="#"><img src="images/animation/03.png" /></a>
<a href="#"><img src="images/animation/4.png" /></a><a href="#"><img src="images/animation/5.png" /></a> <a href="#"><img src="images/animation/6.png" /></a>
<a href="#"><img src="images/animation/7.png" /></a><a href="#"><img src="images/animation/8.png" /></a> <a href="#"><img src="images/animation/9.png" /></a>
<a href="#"><img src="images/animation/10.png" /></a><a href="#"><img src="images/animation/11.png" /></a> <a href="#"><img src="images/animation/12.png" /></a>
<a href="#"><img src="images/animation/13.png" /></a><a href="#"><img src="images/animation/14.png" /></a> <a href="#"><img src="images/animation/15.png" /></a>
<a href="#"><img src="images/animation/16.png" /></a><a href="#"><img src="images/animation/17.png" /></a> <a href="#"><img src="images/animation/18.png" /></a>
<a href="#"><img src="images/animation/19.png" /></a><a href="#"><img src="images/animation/20.png" /></a><a href="#"><img src="images/animation/21.png" /></a><a href="#"><img src="images/animation/22.png" /></a>
</div>

<script type="text/javascript">
marqueeInit({
	uniqueid: 'mycrawler2',
	style: {
		'padding': '2px',
		'width': '980px',
		'height': '100px'
	},
	inc: 5, //speed - pixel increment for each iteration of this marquee's movement
	mouse: 'cursor driven', //mouseover behavior ('pause' 'cursor driven' or false)
	moveatleast: 2,
	neutral: 150,
	savedirection: true,
	random: true
});
</script>