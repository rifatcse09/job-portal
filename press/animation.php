<!---------------------------------------------->
<!-- START OF CODE FOR LAMELLA-SLIDESHOW -->
<!---------------------------------------------->

<!-- START OF MESSAGE-STYLE CONFIGURATION -->
<style>

/* Set the border and shadow around the slideshow */
.borderandshadowstyle {
	/* border for all browsers */

}

/* Set the attributes of the messagebox */
.messageboxstyle {
    background-color:black;
	opacity:0.5; /* Set the opacity for Firefox, Opera and Chrome (value may range from 0 - 1) */
	filter:alpha(opacity=50); /* Set the opacity for Internet Explorer (value may range from 1 - 100) */
	padding:3px;
}

/* Set the position of the messagebox */
.messagealignstyle {
	vertical-align:bottom; /* Set the vertical position (top or middle or bottom) */
	text-align:center; /* Set the gorizontal position (left or center or right) */
}

/* Set the font-attributes */
a.messagefontstyle {
	font-family:Arial;
	font-size:8pt;
	color:white;
	text-decoration:none;
}

</style>
<!-- END OF MESSAGE-STYLE CONFIGURATION -->

<script>
<!-- Beginning of JavaScript -

// CREDITS:
// Slideshow with with lamellar effect
// by Peter Gehrig 
// Copyright (c) 2010 Peter Gehrig. All rights reserved.
// Permission given to use the script provided that this notice remains as is.
// Additional scripts can be found at http://www.fabulant.com

// IMPORTANT: 
// If you add this script to a script-library or script-archive 
// you have to add a link to http://www.fabulant.com on the webpage 
// where this script will be running.

// CONFIGURATION:
// Go to http://www.fabulant.com and get the original code 
// with step-by-step instructions and copy-and-paste installation.

var picture= [

/////////////////////////////////////////////////////////////////////////
// START OF SCRIPT-CONFIGURATION
/////////////////////////////////////////////////////////////////////////

// Configure the URL (path) of the pictures, the corresponding messages and links below
// Add as many pictures a you like.
// You may embed simple HTML-Tags into the message
{
src : "images/slideshow/1.png", // The URL (path) of picture
mes : "", // The message (comment), in this sample with embedded HTML-tag
url : "#" // The link (where you go when you click on the message)
}, // Comma required

{
src : "images/slideshow/2.png",
mes : "",
url : "#"
}, // Comma required

{
src : "images/slideshow/3.png",
mes : "",
url : "#"
}, // Comma required

{
src : "images/slideshow/4.png",
mes : "",
url : "#"
}, // Comma required

{
src : "images/slideshow/5.png",
mes : "",
url : "#"
}, // Comma required

{
src : "images/slideshow/6.png",
mes : "",
url : "#"
}, // Comma required

{
src : "images/slideshow/7.png",
mes : "",
url : "#"
}, // Comma required

{
src : "images/slideshow/8.png",
mes : "",
url : "#"
}, // Comma required

{
src : "images/slideshow/9.png",
mes : "",
url : "#"
} // ATTENTION: the last bracket requires no comma!!!

]

// target of the picture-links ("_blank", "_top", "_self", "_parent" or "nameOfYourFrame")
var target_url="_blank"
	
// number of lamellas.
var x_slices=16

// width of slideshow (pixels)
var slideshow_width=690

// height of slideshow (pixels)
var slideshow_height=286

// pause beween the pictures (seconds)
var pause=2

/////////////////////////////////////////////////////////////////////////
// END OF SCRIPT-CONFIGURATION
/////////////////////////////////////////////////////////////////////////

// Do not edit below this line

var imgpreload=new Array()
for (iii=0;iii<=picture.length-1;iii++) {
	imgpreload[iii]=new Image()
	imgpreload[iii].src=picture[iii].src
}

var picturewidth
var pictureheight
var i_loop=0
var i_picture=0
var width_slice
var cliptop=0
var clipbottom
var i_clipright=1
var contentimg=""
var contentmes=""
pause=pause*1000


function initiate() {
	getcontent()
	for (i=0;i<=x_slices;i++) {
     	var thisinnersimg=eval("document.getElementById('s"+i+"')")
		var thisinnersmes=eval("document.getElementById('t"+i+"')")
		thisinnersimg.innerHTML=contentimg
		thisinnersmes.innerHTML=contentmes
		var thissimg=eval("document.getElementById('s"+i+"').style")
		thissimg.left=0+"px"
		thissimg.top=0+"px"
		var thissmes=eval("document.getElementById('t"+i+"').style")
		thissmes.left=0+"px"
		thissmes.top=0+"px"
  	}
	var thisspan=eval("document.getElementById('s0')")
	width_slice=Math.ceil(slideshow_width/x_slices)
	clipbottom=slideshow_height
	document.getElementById("whole").style.left=0+"px"
	document.getElementById("whole").style.top=0+"px"
	i_picture++
	openlamellar()
}

function openlamellar() {
	clipleft=-width_slice
	clipright=0
    if (i_clipright<=width_slice) {
        for (i=0;i<=x_slices;i++) {
            var thissimg=eval("document.getElementById('s"+i+"').style")
			var thissmes=eval("document.getElementById('t"+i+"').style")
            thissimg.clip ="rect("+cliptop+"px "+clipright+"px "+clipbottom+"px "+clipleft+"px)"
			thissmes.clip ="rect("+cliptop+"px "+clipright+"px "+clipbottom+"px "+clipleft+"px)"
            clipleft+=width_slice
            clipright=clipleft+i_clipright
		}
    i_clipright++
    var timer=setTimeout("openlamellar()",20)
   	}
   	else {
		clearTimeout(timer)
		document.getElementById("whole").innerHTML="<span style='position:absolute;top:0px;left:0px'>"+contentimg+"</span><span style='position:absolute;top:0px;left:0px'>"+contentmes+"</span>"
		var timer=setTimeout("changepicture()",pause)
	}
}

function getcontent() {
	contentimg="<table cellpadding=0 cellspacing=0 border=0 width="+slideshow_width+" height="+slideshow_height+"><tr><td width="+slideshow_width+" height="+slideshow_height+"'><img src="+picture[i_picture].src+" border=0 width="+slideshow_width+" height="+slideshow_height+"></td></tr></table>"
	contentmes="<table cellpadding=0 cellspacing=0 border=0 width="+slideshow_width+" height="+slideshow_height+"><tr valign=top><td class='messagealignstyle'><div class='messageboxstyle' style='width:"+slideshow_width+"px;'><a class='messagefontstyle' href='"+picture[i_picture].url+"' target='"+target_url+"'>"+picture[i_picture].mes+"</a></div></td></tr></table>"
}

function changepicture() {
	i_clipright=0
	clipleft=0
	clipright=0
	for (i=0;i<=x_slices;i++) {
       	var thiss=eval("document.getElementById('s"+i+"').style")
    	thiss.clip ="rect("+cliptop+"px "+clipright+"px "+clipbottom+"px "+clipleft+"px)"
	}
	if (i_picture>picture.length-1) {i_picture=0}
	getcontent()

	for (i=0;i<=x_slices;i++) {
        var thisinnersimg=eval("document.getElementById('s"+i+"')") 
		var thisinnersmes=eval("document.getElementById('t"+i+"')")
        thisinnersimg.innerHTML=contentimg
		thisinnersmes.innerHTML=contentmes
    }
	i_picture++
	openlamellar()
}

document.write("<div class='borderandshadowstyle' style='position:relative;width:"+slideshow_width+"px;height:"+slideshow_height+"px;overflow:hidden'>")
document.write("<span id='whole' style='position:absolute;top:0px;left:0px;width:"+slideshow_width+"px;height:"+slideshow_height+"px;'></span>")
for (i=0;i<=x_slices;i++) {
 	document.write("<span id='s"+i+"' style='position:absolute;top:0px;left:0px;width:"+slideshow_width+"px;height:"+slideshow_height+"px;'></span>")
document.write("<span id='t"+i+"' style='position:absolute;top:0px;left:0px;width:"+slideshow_width+"px;height:"+slideshow_height+"px;'></span>")
}
document.write("</div>")
document.close()
window.onload=initiate

// - End of JavaScript - -->
</script>

<!---------------------------------------------->
<!-- END OF CODE FOR LAMELLA-SLIDESHOW -->
<!---------------------------------------------->