<?

class m_members extends CI_Model {
    public $num_rows = 0;
    public function __construct() {
        parent::__construct();
    }

    function get_all_active_members($limit, $start) {
        $offset = 0;
        if ($start > 0)
            $offset = $start . ', ';
        
        $this->num_rows = $this->db->count_all_results('members');
        
        $this->db->where(array('status' => 1));
        $this->db->limit($limit, $offset);
        $q = $this->db->get('members');
        
        return $q->result_array();
    }
    
    function insert_mail(){
        
    }

}
?>