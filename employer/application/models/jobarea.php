<?php
    class JobArea extends CI_Model{
        function __construct() {
            parent::__construct();
        }
        function select_all_area(){
            $query = $this->db->get_where('emp_area',array('is_active' => 1));
            return $query->result_array();
        }
        
        function get_selected_btype($job_id){
            $this->db->from('emp_business_type');
            $this->db->join('job_to_business_type', 'emp_business_type.business_id = job_to_business_type.business_type_id');
            $this->db->where('job_to_business_type.emp_job_id', $job_id);
            
            $q = $this->db->get();
            return $q->result_array();
        }
    }
?>