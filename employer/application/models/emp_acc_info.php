<?php

class Emp_acc_info extends CI_Model {

    function insert_entry() {
        $data = array(
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'company_name' => trim($_POST['company_name']),
            'alt_company_name' => $_POST['alt_company_name'],
            'contact_person' => $_POST['contact_person'],
            'contact_designation' => $_POST['contact_designation'],
            'business_id' => $_POST['business_id'],
            'business_description' => $_POST['business_description'],
            'company_address' => $_POST['c_address'],
            'country_name' => $_POST['country_name'],
            'city' => $_POST['city'],
            'area' => $_POST['area'],
            'billing_address' => $_POST['billing_address'],
            'contact_phone' => $_POST['contact_phone'],
            'contact_email' => $_POST['contact_email'],
            'website_address' => $_POST['website_address'],
            'is_active' => 1
        );

        $this->db->insert('emp_acc_info', $data);
        $acc_id = $this->db->insert_id();
        if ($acc_id != "") {
            $this->save_hotjob_image($acc_id);
        }
    }

    function get_acc_info($acc_id) {
        $q = $this->db->get_where('id', $acc_id);
        return $q->row_array();
    }

    function save_hotjob_image($acc_id) {
        if (!empty($_FILES['company_logo']) && $_FILES['company_logo']['error'] == 0) {
            $config['upload_path'] = './companylogos/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '4000';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $config['overwrite'] = True;
            $ext = pathinfo($_FILES['company_logo']['name'], PATHINFO_EXTENSION);
            $file = $acc_id . '.' . $ext;
            $config['file_name'] = $file;

            $this->load->helper('form');
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('company_logo')) {
                $data['error'] = array('error' => $this->upload->display_errors());
                $data['title'] = 'Upload';
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            $update['company_logo'] = $file;
            $this->db->where('id', $acc_id);
            $this->db->update('emp_acc_info', $update);
        }
    }

    function get_account_status($com_id) {
        $q = "Select * FROM emp_acc_info, emp_voucher where emp_acc_info.id = emp_voucher.company_id 
            AND emp_voucher.company_id = " . $com_id . " Order By create_date desc";
        $res = $this->db->query($q);
        return $res->result_array();
    }
  

}

?>