<?php
class corporate_acc extends CI_Model{
    function __construct() {
        parent::__construct();
    }		
            
    function get_corporate_info($emp_id){
        
		
		$this->db->select('*');
        $this->db->from('corporate_client');
        $this->db->join('emp_acc_info', 'corporate_client.employer_id = emp_acc_info.id');
        
        $this->db->where('corporate_client.employer_id', $emp_id);
        $q = $this->db->get();
        return $q->row_array();
		
		
		
    }
    
//    function get_company_name($emp_id){
//        $query = $this->db->get_where('emp_acc_info', array('id' => $emp_id));
//        $data = $query->row();
//        return $data['company_name'];
//    }
}
?>