<?php
class EmployerAccount extends CI_Model{
    function __construct() {
        parent::__construct();
    }		
            
    function get_employer_info($emp_id){
        $query = $this->db->get_where('emp_acc_info', array('id' => $emp_id));
        return $query->row_array();
    }
    
//    function get_company_name($emp_id){
//        $query = $this->db->get_where('emp_acc_info', array('id' => $emp_id));
//        $data = $query->row();
//        return $data['company_name'];
//    }
}
?>