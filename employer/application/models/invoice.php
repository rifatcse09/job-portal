<?php

/**
 * Description of invoice
 *
 * @author Anick
 */
class invoice extends CI_Model {
    
    public function __construct(){
        parent::__construct();
    }

    public function get_invoice($id){
        $this->db->select('*');
        $this->db->from('emp_voucher');
        $this->db->join('emp_acc_info', 'emp_voucher.company_id = emp_acc_info.id');
        $this->db->join('emp_new_job', 'emp_voucher.job_id = emp_new_job.id');
        
        $this->db->where('emp_voucher.id', $id);
        $q = $this->db->get();
        return $q->row_array();
    }
}

?>
