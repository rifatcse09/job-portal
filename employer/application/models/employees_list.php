<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employees_list
 *
 * @author Anick
 */
class employees_list extends CI_Model {

    public $num_rows = 0;

    function all_employees() {
        $q = 'Select job_resume_personal.id, degree_title, employment.from, position_held, expected_salary, company_name, employment.to, name, present_add, home_phone, mobile, office_phone, email, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), date_birth)), "%Y")+0 AS age
                from 
                job_resume_personal 
                left join
                (Select degree_title, filt.employee_id 
                from
                (Select employee_id, max(level_id) level_id from job_academic group by employee_id) filt 
                join 
                job_academic on job_academic.employee_id = filt.employee_id AND
                filt.level_id = job_academic.level_id) highest_degree on
                highest_degree.employee_id = job_resume_personal.id
                left join
                (SELECT job_employment_history.employee_id, company_name, highest_job.from, position_held, IF( continuing = \'y\', \'present\', job_employment_history.to ) \'to\'
                FROM (
                SELECT employee_id, Max( job_employment_history.from ) \'from\'
                FROM job_employment_history
                GROUP BY employee_id
                )highest_job, job_employment_history
                WHERE job_employment_history.employee_id = highest_job.employee_id AND
                job_employment_history.from = highest_job.from) employment
                on employment.employee_id = job_resume_personal.id left join
                job_resume_career on job_resume_career.user_id = job_resume_personal.id 
                Where job_resume_personal.is_blue_worker != \'y\'';

        $query = $this->db->query($q);
        return $query->result_array();
    }

    function search_cvs($limit, $start) {
          $offset = 0;
          
        if ($start > 0)
        
            $offset = $start;
        $q = 'Select job_resume_personal.id, degree_title, employment.from, position_held, expected_salary, company_name, employment.to, name, present_add, home_phone, mobile, office_phone, email, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), date_birth)), "%Y")+0 AS age
                from 
                job_resume_personal 
                left join
                (Select job_academic.* 
                from
                (Select employee_id, max(level_id) level_id from job_academic group by employee_id) filt 
                join 
                job_academic on job_academic.employee_id = filt.employee_id AND
                filt.level_id = job_academic.level_id) highest_degree on
                highest_degree.employee_id = job_resume_personal.id
                left join
                (SELECT job_employment_history.employee_id, job_employment_history.area_of_experience, company_name, highest_job.from, position_held, IF( continuing = \'y\', \'present\', job_employment_history.to ) \'to\'
                FROM (
                SELECT employee_id, Max( job_employment_history.from ) \'from\'
                FROM job_employment_history
                GROUP BY employee_id
                )highest_job, job_employment_history
                WHERE job_employment_history.employee_id = highest_job.employee_id AND
                job_employment_history.from = highest_job.from) employment
                on employment.employee_id = job_resume_personal.id left join
                job_resume_career on job_resume_career.user_id = job_resume_personal.id 
                Where job_resume_personal.is_blue_worker != \'y\'';
        if (isset($_POST['edu_level']) && $_POST['edu_level'] != '')
            $q .= ' AND highest_degree.level_id = ' . $_POST['edu_level'];
        if (isset($_POST['result']) && $_POST['result'] != '')
            $q .= ' AND highest_degree.result like ' . $this->db->escape('%' . $_POST['result'] . '%');
        if (isset($_POST['degree_title']) && $_POST['degree_title'] != '')
            $q .= ' AND highest_degree.degree_title like ' . $this->db->escape('%' . $_POST['degree_title'] . '%');
        if (isset($_POST['major']) && $_POST['major'] != '')
            $q .= ' AND highest_degree.major like ' . $this->db->escape('%' . $_POST['major'] . '%');
        if (isset($_POST['institute']) && $_POST['institute'] != '')
            $q .= ' AND highest_degree.institute like ' . $this->db->escape('%' . $_POST['institute'] . '%');

        if (isset($_POST['name']) && $_POST['name'] != '')
            $q .= ' AND job_resume_personal.name like ' . $this->db->escape('%' . $_POST['name'] . '%');
        if (isset($_POST['lstJobArea']) && $_POST['lstJobArea'] != '')
            $q .= ' AND job_resume_personal.current_loc = ' . $_POST['lstJobArea'];
        if (isset($_POST['gender']) && $_POST['gender'] != '')
            $q .= ' AND job_resume_personal.gender = ' . $this->db->escape($_POST['gender']);

        if (isset($_POST['min_salary']) && $_POST['min_salary'] != '')
            $q .= ' AND expected_salary >= ' . $this->db->escape($_POST['min_salary']);

        if (isset($_POST['max_salary']) && $_POST['max_salary'] != '')
            $q .= ' AND expected_salary <= ' . $this->db->escape($_POST['max_salary']);

        if (isset($_POST['min_age']) && $_POST['min_age'] != '')
            $q .= ' AND DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), date_birth)), "%Y")+0 >= ' . $this->db->escape($_POST['min_age']);
        if (isset($_POST['max_age']) && $_POST['max_age'] != '')
            $q .= ' AND DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), date_birth)), "%Y")+0 <= ' . $this->db->escape($_POST['max_age']);


        if (isset($_POST['experience_area']) && $_POST['experience_area'] != '') {
            $pieces = preg_split('/[, ;]/', $_POST['experience_area']);
            $q .= ' AND (';
            foreach ($pieces as $k => $p) {
                if ($p != '') {
                    if ($k > 0)
                        $q .= ' OR ';

                    $q .= 'area_of_experience like ' . $this->db->escape('%' . $p . '%');
                }
            }
            $q .= ')';
        }


    
        //$this->num_rows = $query->num_rows();
       
      
        
        $q .= ' limit ' . $offset . ', ' . $limit;
       $query = $this->db->query($q);
       
       $query1 = $this->db->get('job_resume_personal');
         $this->num_rows = $query1->num_rows();
        return $query->result_array();
    }

    function get_applied_employees($job_id, $limit, $start) {
        $this->db->select('emp_id');
        $emp_s = $this->db->get_where('job_application', array('job_id' => $job_id))->result_array();
        $emp_list = '';
        foreach ($emp_s as $key => $emp) {
            $emp_list .= $emp['emp_id'];
            if ($key != count($emp_s) - 1)
                $emp_list .= ', ';
        }

        $q = 'Select job_resume_personal.id, degree_title, employment.from, position_held, expected_salary, company_name, employment.to, name, present_add, home_phone, mobile, office_phone, email, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), date_birth)), "%Y")+0 AS age
                from 
                job_resume_personal 
                left join
                (Select degree_title, filt.employee_id 
                from
                (Select employee_id, max(level_id) level_id from job_academic group by employee_id) filt 
                join 
                job_academic on job_academic.employee_id = filt.employee_id AND
                filt.level_id = job_academic.level_id) highest_degree on
                highest_degree.employee_id = job_resume_personal.id
                left join
                (SELECT job_employment_history.employee_id, company_name, highest_job.from, position_held, IF( continuing = \'y\', \'present\', job_employment_history.to ) \'to\'
                FROM (
                SELECT employee_id, Max( job_employment_history.from ) \'from\'
                FROM job_employment_history
                GROUP BY employee_id
                )highest_job, job_employment_history
                WHERE job_employment_history.employee_id = highest_job.employee_id AND
                job_employment_history.from = highest_job.from) employment
                on employment.employee_id = job_resume_personal.id left join
                job_resume_career on job_resume_career.user_id = job_resume_personal.id 
                Where job_resume_personal.id in (' . $emp_list . ')';

        $query = $this->db->query($q);
        $this->num_rows = $query->num_rows();
        $offset = 0;
        if ($start > 0)
            $offset = $start . ', ';
        $q .= ' limit ' . $offset . ', ' . $limit;

        //echo $this->db->last_query();
        return $query->result_array();
    }

    function all_blueworker() {
        $q = 'Select job_resume_personal.id, degree_title, employment.from, position_held, expected_salary, company_name, employment.to, name, present_add, home_phone, mobile, office_phone, email, DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), date_birth)), "%Y")+0 AS age
                from 
                job_resume_personal 
                left join
                (Select degree_title, filt.employee_id 
                from
                (Select employee_id, max(level_id) level_id from job_academic group by employee_id) filt 
                join 
                job_academic on job_academic.employee_id = filt.employee_id AND
                filt.level_id = job_academic.level_id) highest_degree on
                highest_degree.employee_id = job_resume_personal.id
                left join
                (SELECT job_employment_history.employee_id, company_name, highest_job.from, position_held, IF( continuing = \'y\', \'present\', job_employment_history.to ) \'to\'
                FROM (
                SELECT employee_id, Max( job_employment_history.from ) \'from\'
                FROM job_employment_history
                GROUP BY employee_id
                )highest_job, job_employment_history
                WHERE job_employment_history.employee_id = highest_job.employee_id AND
                job_employment_history.from = highest_job.from) employment
                on employment.employee_id = job_resume_personal.id left join
                job_resume_career on job_resume_career.user_id = job_resume_personal.id 
                Where job_resume_personal.is_blue_worker = \'y\'';

        $query = $this->db->query($q);
        return $query->result_array();
    }

}

?>