<?php

class NewJob extends CI_Model {

    //new job step 1 save
    function save_job_step01() {

        $session_data = $this->session->userdata('logged_in');
        $company_name = $session_data['company_name'];
        $company_id = $session_data['id'];

        //data formating
        $data['category_id'] = $_POST['category_id'];
        $data['job_title'] = $_POST['job_title'];
        $data['no_vacancy'] = $_POST['no_vacancy'];
        $data['online_cv'] = isset($_POST['online_cv']) ? 'y' : 'n';
        $data['email_attach'] = isset($_POST['email_attach']) ? 'y' : 'n';
        $data['email_id'] = ($_POST['corporate_email'] == "") ? 'n/a' : $_POST['corporate_email'];
        $data['hard_copy'] = isset($_POST['hard_copy']) ? 'y' : 'n';
        $data['photogrph_cv'] = ($_POST['photogrph_cv'] == 'yes') ? 'y' : 'n';
        $data['apply_instruction'] = ($_POST['apply_instruction'] == "") ? 'n/a' : $_POST['apply_instruction'];
        $data['deadline'] = $_POST['application_deadline'];
        $data['billing_contact'] = $_POST['billing_contact_person'];
        $data['designation'] = $_POST['designation'];
        $data['job_display'] = ($_POST['job_display'] == 'yes') ? 'y' : 'n';
        $data['company_name'] = ($_POST['company_name'] == "") ? $company_name : $_POST['company_name'];
        $data['company_id'] = $company_id;
        $data['hide_address'] = ($_POST['hide_address'] == 'yes') ? 'y' : 'n';
        $data['is_hot_job'] = $_POST['optHotJob'];
        $data['is_blue_worker_job'] = $_POST['optBlueJob'];
        $data['is_active'] = 1;
        $data['posting_date'] = date('Y-m-d');

        $this->db->insert('emp_new_job', $data);
        $new_job_id = $this->db->insert_id();
//        if (isset($_FILES['imgSrc'])) {
//            $dir = base_url() . "hotjobimages/";
//
//            $file_name = $new_job_id . "." . pathinfo($_FILES['imgSrc']['name'], PATHINFO_EXTENSION);
//            $ext = pathinfo($_FILES['imgSrc']['name'], PATHINFO_EXTENSION);
//            if ($ext == "jpg" || $ext == "gif" || $ext == "png") {
//                move_uploaded_file($_FILES["imgSrc"]["tmp_name"], $dir . $file_name);
//            }
//        }
        $new_req_id = $this->save_job_step02($new_job_id);
        $this->save_area($new_job_id, $new_req_id, $_POST['txtSelectedExperienceList']);
        $this->save_business_area($new_job_id, $new_req_id, $_POST['txtSelectedBusinessList']);
    }

    function save_job_step02($new_job_id) {
        $data['emp_job_id'] = $new_job_id;
        $data['age_from'] = $_POST['age_from'];
        $data['age_to'] = $_POST['age_to'];
        $data['gender'] = $_POST['gender'];
        $data['job_type'] = $_POST['job_type'];
        $joblevels = (isset($_POST['job_level']) && $_POST['job_level'] != "") ? $_POST['job_level'] : null;
        $count = count($joblevels);
        $joblevel = "";
        for ($i = 0; $i < $count; $i++) {
            if ($joblevel != "" && $i == ($count - 1)) {
                $joblevel .= ',' . $joblevels[$i];
            } else {
                if ($i != 0)
                    $joblevel .= ',';
                $joblevel .= $joblevels[$i];
            }
        }
        $data['job_level'] = $joblevel;
        $data['edu_qualification'] = $_POST['txtEducation'];
        $data['job_responsibility'] = $_POST['txtJobDescription'];
        $data['add_job_requirement'] = $_POST['txtJobRequirements'];
        $data['experience_min'] = (isset($_POST['cboExperienceFrom']) && $_POST['cboExperienceFrom'] == "")?  $_POST['cboExperienceFrom'] : 0;
        $data['experience_max'] = (isset($_POST['cboExperienceTo']) && $_POST['cboExperienceTo'] == "") ? $_POST['cboExperienceTo'] : 0;
        $data['optJobLocationType'] = $_POST['optJobLocationType'];
        $data['job_location'] = $_POST['txtSelectedLocationList'];
        $data['optBenefits'] = $_POST['optBenefits'];
        $data['salary_min'] = (isset($_POST['salary_min']) && $_POST['salary_min'] == "") ? $_POST['salary_min'] : 0;
        $data['salary_max'] = (isset($_POST['salary_max']) && $_POST['salary_max'] == "") ? $_POST['salary_max'] : 0;
        $data['other_benefits'] = ($_POST['other_benefits'] == "") ? 'n/a' : $_POST['other_benefits'];
        $data['is_active'] = 1;
        $this->db->insert('emp_requirement_s2', $data);
        return $this->db->insert_id();
    }

    function save_area($new_emp_id, $new_req_id, $business_area_array) {
        $areas = explode(',', $business_area_array);
        $count = count($areas);
        $data = array();
        for ($i = 0; $i < $count; $i++) {
            $row['emp_job_id'] = $new_emp_id;
            $row['job_requirement_id'] = $new_req_id;
            $row['experience_area_id'] = $areas[$i];
            array_push($data, $row);
        }
        if (!empty($data[0]['experience_area_id']))
            $this->db->insert_batch('job_to_experience_area', $data);
    }

    function save_business_area($new_emp_id, $new_req_id, $business_area_array) {
        $areas = explode(',', $business_area_array);
        $count = count($areas);
        $data = array();
        for ($i = 0; $i < $count; $i++) {
            $row['emp_job_id'] = $new_emp_id;
            $row['job_req_id'] = $new_req_id;
            $row['business_type_id'] = $areas[$i];
            array_push($data, $row);
        }
        if (!empty($data[0]['business_type_id']))
            $this->db->insert_batch('job_to_business_type', $data);
    }

}

?>