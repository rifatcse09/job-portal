<?php

//error_reporting(0);

Class User extends CI_Model {

    public $num_rows = 0;

    function login($username, $password) {
        $this->db->select('id, username, password, company_name');
        $this->db->from('emp_acc_info');
        $this->db->where('username = ' . "'" . $username . "'");
        $this->db->where('password = ' . "'" . $password . "'");
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function select_posted_job($acc_id, $limit, $start) {
        $offset = 0;
        if ($start > 0)
            $offset = $start . ', ';

        $query = $this->db->query('SELECT *, (sub.applied - sub.viewed) "not viewed" from emp_new_job join emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id join
                                emp_acc_info on emp_new_job.company_id=emp_acc_info.id 
                                left join
                                    (select main.job_id, main.count applied, IFNULL(viewed.count, 0) viewed
                                FROM
                                    (SELECT Count( job_id ) count, job_id
                                                                FROM job_application
                                                                GROUP BY job_id) as main
                                Left Join 
                                    (select Count(job_id) count, job_id
                                    From job_application
                                    Where is_viewed = 1
                                    Group by job_id) as viewed
                                    on main.job_id = viewed.job_id) sub
                                on job_id = emp_new_job.id where emp_acc_info.id=' . $acc_id . ' and
                                emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 limit ' . $offset . $limit);

        $n = $this->db->query('SELECT * from emp_new_job join emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id join
                                emp_acc_info on emp_new_job.company_id=emp_acc_info.id where emp_acc_info.id=' . $acc_id . ' and
                                emp_new_job.is_active=1 and emp_requirement_s2.is_active=1 ');
        $this->num_rows = $n->num_rows();

        return $query->result_array();
    }
     function select_blueworker_job($acc_id, $limit, $start) {
        $offset = 0;
        if ($start > 0)
            $offset = $start . ', ';

        $query = $this->db->query('SELECT *, (sub.applied - sub.viewed) "not viewed" from emp_new_job join emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id join
                                emp_acc_info on emp_new_job.company_id=emp_acc_info.id left join
                                    (select main.job_id, main.count applied, IFNULL(viewed.count, 0) viewed
                                FROM
                                    (SELECT Count( job_id ) count, job_id
                                                                    FROM job_application
                                                                    GROUP BY job_id) as main
                                    Left Join 
                                        (select Count(job_id) count, job_id
                                        From job_application
                                        Where is_viewed = 1
                                        Group by job_id) as viewed
                                    on main.job_id = viewed.job_id) sub
                                on job_id = emp_new_job.id where emp_acc_info.id=' . $acc_id . ' and
                                emp_new_job.is_active=1 and emp_new_job.is_blue_worker_job="y" and emp_requirement_s2.is_active=1 limit ' . $offset . $limit);

        $n = $this->db->query('SELECT * from emp_new_job join emp_requirement_s2 on emp_new_job.id=emp_requirement_s2.emp_job_id join
                                emp_acc_info on emp_new_job.company_id=emp_acc_info.id where emp_acc_info.id=' . $acc_id . ' and
                                emp_new_job.is_active=1 and emp_new_job.is_blue_worker_job="y"   and emp_requirement_s2.is_active=1 ');
        $this->num_rows = $n->num_rows();

        return $query->result_array();
    }
function showdelete($user_id)
{ 
    $n = $this->db->query('SELECT * from emp_new_job  where company_id=' . $user_id . ' and
                                is_active=0');
 
        
        return $n->result_array();
   
    
}
    function showempdata($user_id) {
        $query = $this->db->query('SELECT * FROM emp_acc_info where id=' . $user_id . ' and is_active=1');
        return $query->row_array();
    }

    function upempdata($user_id) {
        $data['username'] = $_POST['username'];
        $data['password'] = $_POST['password'];
        $data['company_name'] = $_POST['company_name'];
        $data['alt_company_name'] = $_POST['alt_company_name'];
        $data['contact_person'] = $_POST['contact_person'];
        $data['contact_designation'] = $_POST['contact_designation'];
        $data['business_description'] = $_POST['business_description'];
        $data['company_address'] = $_POST['company_address'];
        $data['country_name'] = $_POST['country_name'];
        $data['city'] = $_POST['city'];
        $data['area'] = $_POST['area'];
        $data['billing_address'] = $_POST['billing_address'];
        $data['contact_phone'] = $_POST['contact_phone'];
        $data['contact_email'] = $_POST['contact_email'];
        $data['website_address'] = $_POST['website_address'];
        
        $this->db->where(array('id' => $user_id));
        $this->db->update('emp_acc_info', $data);
        
        $this->load->model("Emp_acc_info");
        $this->Emp_acc_info->save_hotjob_image($user_id);
    }

    function showposteddata($job_id) {
        $query = $this->db->query('SELECT * FROM emp_new_job join emp_requirement_s2 on 
                                   emp_new_job.id=emp_requirement_s2.emp_job_id join job_category on
                                   emp_new_job.category_id=job_category.category_id 
                                   where emp_new_job.id=' . $job_id . ' and emp_new_job.is_active=1 and 
                                   emp_requirement_s2.is_active=1 and job_category.is_active=1');

        
        return $query->row_array();
    }

    function edit_posted_job($job_id) {
        $session_data = $this->session->userdata('logged_in');
        $company_name = $session_data['company_name'];
        $company_id = $session_data['id'];

        //data formating
        $data['category_id'] = $_POST['category_id'];
        $data['job_title'] = $_POST['job_title'];
        $data['no_vacancy'] = $_POST['no_vacancy'];
        $data['online_cv'] = isset($_POST['online_cv']) ? 'y' : 'n';
        $data['email_attach'] = isset($_POST['email_attach']) ? 'y' : 'n';
        $data['email_id'] = (isset($_POST['corporate_email']) && $_POST['corporate_email'] != "") ? $_POST['corporate_email'] : 'n/a';
        $data['hard_copy'] = isset($_POST['hard_copy']) ? 'y' : 'n';
        $data['photogrph_cv'] = ($_POST['photogrph_cv'] == 'yes') ? 'y' : 'n';
        $data['apply_instruction'] = (isset($_POST['apply_instruction']) && $_POST['apply_instruction'] != "") ? $_POST['apply_instruction'] : 'n/a';
        $data['deadline'] = $_POST['cboYear'] . '-' . $_POST['cboMonth'] . '-' . $_POST['cboDay'];
        $name = explode('_', $_POST['billing_contact']);
        $data['billing_contact'] = $name[1];
        $data['designation'] = $_POST['designation'];
        $data['job_display'] = ($_POST['job_display'] == 'yes') ? 'y' : 'n';
        $data['company_name'] = (isset($_POST['job_display']) && isset ($_POST['company_name'])) ? $_POST['company_name'] : $company_name;
        $data['company_id'] = $company_id;
        $data['hide_address'] = ($_POST['hide_address'] == 'yes') ? 'y' : 'n';
        $data['is_hot_job'] = $_POST['optHotJob'];
        $data['is_active'] = 1;
        $data['posting_date'] = date('Y-m-d');

        $this->db->where('id', $job_id);
        $this->db->update('emp_new_job', $data);
        $new_job_id = $job_id;
        $new_req_id = $this->edit_job_step02($new_job_id)->emp_job_id;
        $this->edit_area($new_job_id, $new_req_id, $_POST['txtSelectedExperienceList']);
        $this->edit_business_area($new_job_id, $new_req_id, $_POST['txtSelectedBusinessList']);
    }

    function edit_job_step02($new_job_id) {
        $data['emp_job_id'] = $new_job_id;
        $data['age_from'] = $_POST['age_from'];
        $data['age_to'] = $_POST['age_to'];
        $data['gender'] = $_POST['gender'];
        $data['job_type'] = $_POST['job_type'];
        $joblevels = $_POST['job_level'];
        $count = count($joblevels);
        $joblevel = "";
        for ($i = 0; $i < $count; $i++) {
            if ($joblevel != "" && $i == ($count - 1)) {
                $joblevel .= ',' . $joblevels[$i];
            } else {
                if ($i != 0)
                    $joblevel .= ',';
                $joblevel .= $joblevels[$i];
            }
        }
        $data['job_level'] = $joblevel;
        $data['edu_qualification'] = $_POST['txtEducation'];
        $data['job_responsibility'] = $_POST['txtJobDescription'];
        $data['add_job_requirement'] = $_POST['txtJobRequirements'];
        $data['experience_min'] = (isset($_POST['cboExperienceFrom']) && $_POST['cboExperienceFrom'] != "") ? $_POST['cboExperienceFrom'] : '-1';
        $data['experience_max'] = (isset($_POST['cboExperienceTo']) && $_POST['cboExperienceTo'] != "") ? $_POST['cboExperienceTo'] : '-1';
        $data['optJobLocationType'] = $_POST['optJobLocationType'];
        $data['job_location'] = $_POST['txtSelectedLocationList'];
        $data['optBenefits'] = $_POST['optBenefits'];
        $data['salary_min'] = (isset($_POST['salary_min']) && $_POST['salary_min'] != "") ? $_POST['salary_min'] : '-1';
        $data['salary_max'] = (isset($_POST['salary_max']) && $_POST['salary_max'] != "") ? $_POST['salary_max'] : '-1';
        $data['other_benefits'] = ($_POST['other_benefits'] == "") ? 'n/a' : $_POST['other_benefits'];

        $this->db->where('emp_job_id', $new_job_id);
        $this->db->update('emp_requirement_s2', $data);
        $this->db->select('emp_job_id');
        return $this->db->get_where('emp_requirement_s2', array('emp_job_id' => $new_job_id))->row();
    }

    function edit_area($new_emp_id, $new_req_id, $business_area_array) {
        $areas = explode(',', $business_area_array);
        $count = count($areas);
        $data = array();
        for ($i = 0; $i < $count; $i++) {
            $row['emp_job_id'] = $new_emp_id;
            $row['emp_job_id'] = $new_req_id;
            $row['experience_area_id'] = $areas[$i];
            array_push($data, $row);
        }
        if (!empty($data[0]['experience_area_id'])) {
            $this->db->where('emp_job_id', $new_emp_id);
            $this->db->delete('job_to_experience_area');
            $this->db->insert_batch('job_to_experience_area', $data);
        }
    }

    function edit_business_area($new_emp_id, $new_req_id, $business_area_array) {
        $areas = explode(',', $business_area_array);
        $count = count($areas);
        $data = array();
        for ($i = 0; $i < $count; $i++) {
            $row['emp_job_id'] = $new_emp_id;
            $row['job_req_id'] = $new_req_id;
            $row['business_type_id'] = $areas[$i];
            array_push($data, $row);
        }
        if (!empty($data[0]['business_type_id'])) {
            $this->db->where('emp_job_id', $new_emp_id);
            $this->db->delete('job_to_business_type');
            $this->db->insert_batch('job_to_business_type', $data);
        }
    }

//    function showdelete($job_id) {
//        $query = $this->db->query('SELECT * FROM emp_new_job join emp_requirement_s2 on 
//                                   emp_new_job.id=emp_requirement_s2.emp_job_id join job_category on
//                                   emp_new_job.category_id=job_category.category_id 
//                                   where emp_new_job.id=' . $job_id . ' and emp_new_job.is_active=1 and 
//                                   emp_requirement_s2.is_active=1 and job_category.is_active=1');
//
//
//        return $query->row_array();
//    }

    function delete_jobs($job_ids) {
        foreach ($job_ids as $key => $ids) {
            $this->db->where('id', $ids);
            $data['is_active'] = 0;

            $this->db->update('emp_new_job', $data);
        }
       
    }

    function get_all_employer() {
        $q = $this->db->get_where('emp_acc_info', 'is_active = 1');

        return $q->result_array();
    }

    function get_all_employer_json() {
        $this->db->select('id, company_name');
        $this->db->from('emp_acc_info');
        $this->db->where('is_active = 1');
        $q = $this->db->get()->result_array();

        return json_encode($q);
    }

    public function change_password($user_id, $user_name, $password) {
        $data = array('username' => $user_name, 'password' => $password);
        $this->db->update('emp_acc_info', $data, array('id' => $user_id));

        if ($this->db->affected_rows() > 0) {
            $sess_array = array(
                'id' => $user_id,
                'username' => $user_name,
                'company_name' => $this->session->userdata['logged_in']['company_name']
            );
            $this->session->set_userdata('logged_in', $sess_array);
            return true;
        } else {
            return false;
        }
    }
     function logout(){
        $array_items = array('username' => '', 'id' => '', 'name' => '');
        $this->session->unset_userdata($array_items);
    }

    public function get_emp_info($user_id) {
        $q = $this->db->get_where('emp_acc_info', array('id' => $user_id));
        return $q->result_array();
    }

}

?>