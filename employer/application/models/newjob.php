<?php

class NewJob extends CI_Model {

    public $voucher_id = 0;
    public $voucher='';
    public $com_email='';
    public $com_id = '';
    public $job_id = '';
    public function __construct() {
        parent::__construct();
    }
//new job step 1 save
    function save_job_step01() {

        $session_data = $this->session->userdata('logged_in');
        $company_name = $session_data['company_name'];
        $company_id = $session_data['id'];

//data formating
        $data['category_id'] = $_POST['category_id'];
        $data['job_title'] = $_POST['job_title'];
        $data['no_vacancy'] = $_POST['no_vacancy'];
        $data['online_cv'] = isset($_POST['online_cv']) ? $_POST['online_cv'] : 'n';
        $data['email_attach'] = isset($_POST['email_attach']) ? $_POST['email_attach'] : 'n';
        $data['email_id'] = ($_POST['corporate_email'] == "") ? 'n/a' : $_POST['corporate_email'];
        $data['hard_copy'] = isset($_POST['hard_copy']) ? $_POST['hard_copy'] : 'n';
        $data['photogrph_cv'] = ($_POST['photogrph_cv'] == 'yes') ? 'y' : 'n';
        $data['apply_instruction'] = ($_POST['apply_instruction'] == "") ? 'n/a' : $_POST['apply_instruction'];
        $data['deadline'] = $_POST['application_deadline'];
        $data['billing_contact'] = $_POST['billing_contact_person'];
        $data['designation'] = $_POST['designation'];
        $data['job_display'] = ($_POST['job_display'] == 'yes') ? 'y' : 'n';
        $data['company_name'] = ($_POST['company_name'] == "") ? $company_name : $_POST['company_name'];
        $data['company_id'] = $company_id;
        $data['hide_address'] = ($_POST['hide_address'] == 'yes') ? 'y' : 'n';
        $data['is_hot_job'] = $_POST['optHotJob'];
        $data['is_active'] = 1;
        $data['posting_date'] = date('Y-m-d');
        $data['is_blue_worker_job'] = (isset($_POST['optBlueJob']) && $_POST['optBlueJob'] != "") ? $_POST['optBlueJob'] : 'n';
        $data['template'] = $_POST['template'];
        
        $this->db->insert('emp_new_job', $data);
        $new_job_id = $this->db->insert_id();
        if (isset($_FILES['imgSrc'])) {
            $dir = base_url() . "hotjobimages/";

            $file_name = $new_job_id . "." . pathinfo($_FILES['imgSrc']['name'], PATHINFO_EXTENSION);
            $ext = pathinfo($_FILES['imgSrc']['name'], PATHINFO_EXTENSION);
            if ($ext == "jpg" || $ext == "gif" || $ext == "png") {
                move_uploaded_file($_FILES["imgSrc"]["tmp_name"], $dir . $file_name);
            }
        }
        $new_req_id = $this->save_job_step02($new_job_id);

        $this->com_id = $company_id;
        $this->job_id = $new_job_id;

        $this->load->model('employeraccount');
        $com_info = $this->employeraccount->get_employer_info($company_id);

        $this->com_email = $com_info['contact_email'];
       

        $this->save_area($new_job_id, $new_req_id, $_POST['txtSelectedExperienceList']);
        $this->save_business_area($new_job_id, $new_req_id, $_POST['txtSelectedBusinessList']);

        $job_type = 'regular';
        if ($data['is_hot_job'] == 'y')
            $job_type = 'hot-job';
        else if (isset($data['is_blue_worker_job']))
            $job_type = 'blue-job';
		
		$this->load->model('corporate_acc');
        $corporate_info = $this->corporate_acc->get_corporate_info($company_id);
		
		
		if($com_info['is_corporate']==1){
			$discount= $corporate_info['discount'];
			}
			else
			{
			$discount=0	;
			}

        $this->save_amount($new_job_id, $company_id, $data['deadline'], $job_type,$discount);
    }

    function save_amount($new_job_id, $company_id, $to_date, $job_type,$discount) {
        $this->load->library('guid');
        $amount = 1000;
        switch ($job_type) {
            case 'blue-job':
                $amount = 1500;
                break;
            case 'hot-job':
                $amount = 2500;
                break;
        }

        $data['company_id'] = $company_id;
        $data['job_id'] = $new_job_id;
        $data['amount'] = $amount;
		$data['discount'] = $discount;
        $data['voucher_no'] = $this->guid->create_guid();
        $data['vat'] = $amount*(15/100);
        $data['create_date'] = date('Y-m-d h:i:s');
        $data['from_date'] = date('Y-m-d h:i:s');
        $data['to_date'] = $to_date;
        $data['is_paid'] = 0;
        $data['status'] = 1;
      
        //echo $this->com_email;
        
        //$this->db->query("select contact_email from emp_acc_info where id='$company_id' ");
        //$this->com_email=$query['contact_email'];
        $this->db->insert('emp_voucher', $data);
        $this->voucher_id = $this->db->insert_id();
        
    }

    function save_job_step02($new_job_id) {
        $data['emp_job_id'] = $new_job_id;
        $data['age_from'] = $_POST['age_from'];
        $data['age_to'] = $_POST['age_to'];
        $data['gender'] = $_POST['gender'];
        $data['job_type'] = $_POST['job_type'];
        $joblevels = (isset($_POST['job_level']) && $_POST['job_level'] != "") ? $_POST['job_level'] : null;
        $count = count($joblevels);
        $joblevel = "";
        for ($i = 0; $i < $count; $i++) {
            if ($joblevel != "" && $i == ($count - 1)) {
                $joblevel .= ',' . $joblevels[$i];
            } else {
                if ($i != 0)
                    $joblevel .= ',';
                $joblevel .= $joblevels[$i];
            }
        }
        $data['job_level'] = $joblevel;
        $data['edu_qualification'] = $_POST['txtEducation'];
        $data['job_responsibility'] = $_POST['txtJobDescription'];
        $data['add_job_requirement'] = $_POST['txtJobRequirements'];
        $data['experience_min'] = (isset($_POST['cboExperienceFrom']) && $_POST['cboExperienceFrom'] != "") ? $_POST['cboExperienceFrom'] : 0;
        $data['experience_max'] = (isset($_POST['cboExperienceTo']) && $_POST['cboExperienceTo'] != "") ? $_POST['cboExperienceTo'] : 0;
        $data['optJobLocationType'] = $_POST['optJobLocationType'];
        $data['job_location'] = $_POST['txtSelectedLocationList'];
        $data['optBenefits'] = $_POST['optBenefits'];
        $data['salary_min'] = (isset($_POST['salary_min']) && $_POST['salary_min'] != "") ? $_POST['salary_min'] : 0;
        $data['salary_max'] = (isset($_POST['salary_max']) && $_POST['salary_max'] != "") ? $_POST['salary_max'] : 0;
        $data['other_benefits'] = ($_POST['other_benefits'] == "") ? 'n/a' : $_POST['other_benefits'];
        $data['is_active'] = 1;
        $this->db->insert('emp_requirement_s2', $data);
        return $this->db->insert_id();
    }

    function save_area($new_emp_id, $new_req_id, $business_area_array) {
        $areas = explode(',', $business_area_array);
        $count = count($areas);
        $data = array();
        for ($i = 0; $i < $count; $i++) {
            $row['emp_job_id'] = $new_emp_id;
            $row['job_requirement_id'] = $new_req_id;
            $row['experience_area_id'] = $areas[$i];
            array_push($data, $row);
        }
        if (!empty($data[0]['experience_area_id']))
            $this->db->insert_batch('job_to_experience_area', $data);
    }

    function save_business_area($new_emp_id, $new_req_id, $business_area_array) {
        $areas = explode(',', $business_area_array);
        $count = count($areas);
        $data = array();
        for ($i = 0; $i < $count; $i++) {
            $row['emp_job_id'] = $new_emp_id;
            $row['job_req_id'] = $new_req_id;
            $row['business_type_id'] = $areas[$i];
            array_push($data, $row);
        }
        if (!empty($data[0]['business_type_id']))
            $this->db->insert_batch('job_to_business_type', $data);
    }

    function send_mail($to, $from, $content, $subject, $cc = '', $bcc = '') {
        $this->load->library('email');

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ckrbd.com',
            'smtp_port' => '25',
            'smtp_user' => 'info@ckrbd.com',
            'smtp_pass' => 'pentagems14',
            'mailtype' => 'html',
			 'smtp_timeout' =>30,


        );
        $this->email->set_mailtype("html");
       // $this->email->initialize($config);
       // $this->load->library('email', $config);
        // $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);

        $this->email->subject($subject);
        $this->email->message($content);
        if ($this->email->send()) {
            return true;
        } else {
            //echo $this->email->display_errors();
            return FALSE;
        }
    }

    public function repost_job($job_id, $deadline) {
//     $this->db->select('job_emp_country.*, Country_Name');
        $this->db->select('*');
        $this->db->from('emp_new_job');
        $this->db->join('emp_requirement_s2', 'emp_new_job.id', 'emp_requirement_s2.emp_job_id');
        $this->db->where(array('id' => $job_id));

        $job = $this->db->get()->row_array();
        if (isset($job)) {
            $data = array();
            $data['category_id'] = $job['category_id'];
            $data['job_title'] = $job['job_title'];
            $data['no_vacancy'] = $job['no_vacancy'];
            $data['online_cv'] = $job['online_cv'];
            $data['email_attach'] = $job['email_attach'];
            $data['email_id'] = $job['email_id'];
            $data['hard_copy'] = $job['hard_copy'];
            $data['photogrph_cv'] = $job['photogrph_cv'];
            $data['apply_instruction'] = $job['apply_instruction'];
            $data['posting_date'] = date('Y-m-d');
            $data['deadline'] = date('Y-m-d', strtotime($deadline));
            $data['billing_contact'] = $job['billing_contact'];
            $data['designation'] = $job['designation'];
            $data['job_display'] = $job['job_display'];
            $data['company_name'] = $job['company_name'];
            $data['company_id'] = $job['company_id'];
            $data['hide_address'] = $job['hide_address'];
            $data['is_hot_job'] = $job['is_hot_job'];
            $data['is_blue_worker_job'] = $job['is_blue_worker_job'];
            $data['template'] = $job['template'];
            $data['is_active'] = 1;
            $this->db->insert('emp_new_job', $data);

            $data2 = array();
            $data2['emp_job_id'] = $this->db->insert_id();
            $data2['age_from'] = $job['age_from'];
            $data2['age_to'] = $job['age_to'];
            $data2['gender'] = $job['gender'];
            $data2['job_type'] = $job['job_type'];
            $data2['job_level'] = $job['job_level'];
            $data2['edu_qualification'] = $job['edu_qualification'];
            $data2['job_responsibility'] = $job['job_responsibility'];
            $data2['add_job_requirement'] = $job['add_job_requirement'];
            $data2['experience_min'] = $job['experience_min'];
            $data2['experience_max'] = $job['experience_max'];
            $data2['optJobLocationType'] = $job['optJobLocationType'];
            $data2['job_location'] = $job['job_location'];
            $data2['optBenefits'] = $job['optBenefits'];
            $data2['salary_min'] = $job['salary_min'];
            $data2['salary_max'] = $job['salary_max'];
            $data2['other_benefits'] = $job['other_benefits'];
            
            $data2['is_active'] = 1;
            $this->com_email=$job['email_id'];
//            $data2['template'] = $job['template'];

            $this->db->insert('emp_requirement_s2', $data2);
			
			$this->load->model('employeraccount');
            $com_info = $this->employeraccount->get_employer_info($job['company_id']);
			
			$this->load->model('corporate_acc');
            $corporate_info = $this->corporate_acc->get_corporate_info($job['company_id']);
		
		
		   if($com_info['is_corporate']==1){
			$discount= $corporate_info['discount'];
			}
			else
			{
			$discount=0	;
			}
            
            
            $this->save_amount($data2['emp_job_id'], $data['company_id'], $data['deadline'], $data2['job_type'],$discount);
        }
    }

}

?>
