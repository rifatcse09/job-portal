<?php
class BusinessType extends CI_Model{
        function __construct() {
            parent::__construct();
        }
        
        function select_all_business_type(){
            $query = $this->db->get_where('emp_business_type',array('is_active' => 1));
            return $query->result_array();
        }
        
        function get_selected_job_atype($job_id){
            $this->db->from('emp_area');
            $this->db->join('job_to_experience_area', 'emp_area.area_id = job_to_experience_area.experience_area_id');
            $this->db->where('job_to_experience_area.emp_job_id', $job_id);
            
            $q = $this->db->get();
            return $q->result_array();
        }
}
?>
