<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class CreateAccount extends CI_Controller{
    function index(){
        $this->load->library('template');
        $this->load->model('category');
        $data['categories'] = $this->Category->select_all_category();
        
        $this->template->write('title', 'Create a new Account');
        $this->template->add_region('style');
        $this->template->write('style', '#content {width: 100%;}');
        $this->template->write_view('header', 'common/header');
        $this->template->write_view('content','createaccount/step_01', $data, True);
        $this->template->render();
    }
    
    //create account of a user through this controller
    function Register(){
        $this->load->model('user');
        $this->load->model('useraccount');
        $this->load->model('career');
        $this->load->library('template');
        $this->template->write_view('header', 'common/header');
        
        //if user is not exist
        if($this->UserAccount->check_user($_POST['account_username']) == true){
            $res = $this->User->CreateAccount();
            $flag = false;
            //if user personal information saved, then create account info
            if($this->User->id > 1){            
                $this->UserAccount->CreateUserAccount($this->User->id);
                $this->Career->CreateCareer($this->User->id);
                $flag = true;
            }
            else{
                $flag = FALSE;
            }
        }
        else{
            $this->template->write('title', 'Error');
            $this->template->write('content','User name already taken. Please try with another one.');
            $this->template->render();
        }
        
        
    }
    
    function category_list(){
        $this->load->model('category');
        $data['categories'] = $this->Category->select_all_category();
        
        $this->load->view('common/category_list', $data);
    }
}
?>