<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
error_reporting(0);

class employer_account extends CI_Controller {

    public function index() {
        $this->load->model('BusinessType');
        $data['title'] = 'employer_account';
        $data['business_areas'] = $this->BusinessType->select_all_business_type();
        $data['business_type_size'] = 1;
        $data['business_name'] = 'business_id';
        $data['business_id'] = 'business_id';
        $this->load->view('employer_account', $data);
    }
    
    public function reset_pass()
    {
         $this->load->view('reset_pass');
    }

}
