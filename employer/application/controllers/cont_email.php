<?php

/**
 * Description of cont_email
 *
 * @author Anick
 */
class cont_email extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->library('template');
        $this->template->write('title', 'Email Resume');

        if (isset($_GET['job_id']) && $_GET['job_id'] != '') {
            $this->load->model('Job');
            $data['company'] = $this->Job->get_company_job_info($_GET['job_id']);
        }

        //User personal information
        $this->load->Model('User');
        $data['personal'] = $this->User->get_user_personal($this->session->userdata['user_id']);

        $this->template->write_view('header', 'common/header');
        $this->template->write_view('sidebar', 'common/sidebar');
        $this->template->write_view('content', 'resume_email/email_view', $data);
        $this->template->render();
    }

    public function post_email() {
        $this->load->library('email');

    
        
         $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ckrbd.com',
            'smtp_port' => '25',
            'smtp_user' => 'info@ckrbd.com',
            'smtp_pass' => 'pentagems14',
            'mailtype' => 'html',
			 'smtp_timeout' =>30

             
       );
         $this->email->set_mailtype("html");
       // $this->email->initialize($config);
        //$this->load->library('email',$config);
       // $this->email->set_newline("\r\n");   
        $this->email->from($_POST['email_id'], $_POST['name']);
        $this->email->to($_POST['company_email_address']);

        $this->email->subject($_POST['email_subject']);

        $msg = $_POST['email_message'];
        $salary= $_POST['salary']
        $this->load->model('User');
        $user_id = $this->session->userdata['user_id'];
       /* $data['personal'] = $this->User->get_user_personal($user_id);
        $data['emp_history'] = $this->User->get_user_emp_history($user_id);
        $data['acc_qualification'] = $this->User->get_acc_qualification($user_id);
        $data['career'] = $this->User->get_user_career($user_id);
        $data['pre_location'] = $this->User->get_user_preferred_location($user_id);
        $data['pre_organization'] = $this->User->get_user_preferred_organization($user_id);
        $data['language'] = $this->User->get_language($user_id);
        $data['reference'] = $this->User->get_reference($user_id);
        $data['categories'] = $this->User->get_user_preferred_category($user_id);
		*/
           $data['id']=$user_id;
        $view = $this->load->view('cv',$data,TRUE);
       // $this->email->attach($view);
        //$view = $this->load->view('only_resume_view', $data, TRUE);
        $this->email->message($msg ."Expected Salary:".$salary. $view);

        if (isset($_POST['job_id'])) {
            $this->load->model('Job');
            $this->Job->create_job_application($_POST['job_id'], $this->session->userdata['user_id']);
        }
 //echo $this->email->print_debugger();
        if (!$this->email->send()) {
            $this->session->set_flashdata('msg', 'sorry, your email couldn\'t be sent.');
        }
        redirect(site_url() . '/page');
    }

    public function resume_only() {
        if (isset($this->session->userdata['user_id'])) {
            $user_id = $this->session->userdata['user_id'];
            $this->load->model('User');
            $data['personal'] = $this->User->get_user_personal($user_id);
            $data['emp_history'] = $this->User->get_user_emp_history($user_id);
            $data['acc_qualification'] = $this->User->get_acc_qualification($user_id);
            $data['career'] = $this->User->get_user_career($user_id);
            $data['pre_location'] = $this->User->get_user_preferred_location($user_id);
            $data['pre_organization'] = $this->User->get_user_preferred_organization($user_id);
            $data['language'] = $this->User->get_language($user_id);
            $data['reference'] = $this->User->get_reference($user_id);
            $data['categories'] = $this->User->get_user_preferred_category($user_id);
            $this->load->view('only_resume_view', $data);
        }
    }

    public function resume_to_employer() {
        if (isset($_GET['emp_id'])) {
            $user_id = $_GET['emp_id'];
            $this->load->model('User');
            
            if (isset($_GET['job_id'])) {
                $this->User->update_job_application($user_id, $_GET['job_id']);
            }
            $data['personal'] = $this->User->get_user_personal($user_id);
            $data['emp_history'] = $this->User->get_user_emp_history($user_id);
            $data['acc_qualification'] = $this->User->get_acc_qualification($user_id);
            $data['career'] = $this->User->get_user_career($user_id);
            $data['pre_location'] = $this->User->get_user_preferred_location($user_id);
            $data['pre_organization'] = $this->User->get_user_preferred_organization($user_id);
            $data['language'] = $this->User->get_language($user_id);
            $data['reference'] = $this->User->get_reference($user_id);
            $data['categories'] = $this->User->get_user_preferred_category($user_id);
            $this->load->view('only_resume_view', $data);
        }
    }

}

?>