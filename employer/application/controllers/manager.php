<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class manager extends CI_Controller {

    public function index() {
        $data['title'] = 'Welcome To Purbodesh';
        $this->load->view('manager_view', $data);
    }

}
