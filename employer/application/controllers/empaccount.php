<?php

//session_start(); //we need to call PHP's session object to access it through CI
//error_reporting(0);

class Empaccount extends CI_Controller {

    function emp_acc_info() {
        $this->load->model('Emp_acc_info');
        $this->Emp_acc_info->insert_entry();
        redirect('login');
    }

    function view_voucher(){
        $voucher_id = $_GET['voucher_id'];
        $this->load->model('invoice');
        $data['invoice'] = $this->invoice->get_invoice($voucher_id);
        
        $this->load->view('invoice', $data);
    }
    
    function get_all_employers(){
        $this->load->model('user');
        
        $data['json'] = $this->user->get_all_employer_json();
        
        return $data['json'];
    }

}

?>