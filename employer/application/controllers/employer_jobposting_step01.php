<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
//error_reporting(0);

class employer_jobposting_step01 extends CI_Controller {

    public function index() {
        $this->load->model('Category');
        $this->load->model('EmployerAccount');
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');

            $emp_id = $session_data['id'];
            $result = $this->EmployerAccount->get_employer_info($emp_id);

            //company name
            $data['alt_company_name'] = $result['alt_company_name'];
            $data['corporate_email'] = $result['contact_email'];
            $data['designation_value'] = $result['id'] . '_' . $result['contact_person'] . '_' . $result['contact_designation'];
            $data['person_name'] = $result['contact_person'];
            //all categories
            $data['categories'] = $this->Category->select_all_category();
            $data['category_size'] = 1;
            $data['select_name'] = 'category_id';
            $data['select_id'] = 'cboJobCategory';
            $data['title'] = 'Post Jobs';

            $this->load->view('employer_jobposting_step01', $data);
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    public function jobposting_step_02() {
        if ($this->session->userdata('logged_in')) {
            $this->load->model('Category');
            $this->load->model('JobArea');
            $this->load->model('BusinessType');
            $session_data = $this->session->userdata('logged_in');
            $company_name = $session_data['company_name'];

            $data['category_id'] = $_POST['category_id'];
            $res = $this->Category->get_category($_POST['category_id']);
            $data['category_name'] = $res['category_name'];
            $data['job_title'] = $_POST['job_title'];
            $data['no_vacancy'] = (isset($_POST['no_vacancy'])) ? $_POST['no_vacancy'] : 0;
            $data['online_cv'] = isset($_POST['online_cv']) ? 'y' : 'n';
            $data['email_attach'] = isset($_POST['email_attach']) ? 'y' : 'n';
            $data['corporate_email'] = (isset($_POST['corporate_email']) && $_POST['corporate_email'] != "") ? $_POST['corporate_email'] : 'n/a';
            $data['hard_copy'] = isset($_POST['hard_copy']) ? 'y' : 'n';
            $data['photogrph_cv'] = $_POST['photogrph_cv'];
            $data['apply_instruction'] = (isset($_POST['apply_instruction']) && $_POST['apply_instruction'] != "") ? $_POST['apply_instruction'] : 'n/a';
            $data['application_deadline'] = $_POST['cboDay'] . '-' . $_POST['cboMonth'] . '-' . $_POST['cboYear'];
            $data['deadline'] = $_POST['cboYear'] . '-' . $_POST['cboMonth'] . '-' . $_POST['cboDay'];
            $data['billing_contact'] = $_POST['billing_contact'];
            $name = explode('_', $_POST['billing_contact']);
            $data['billing_contact_person'] = isset($name[1]) ? $name[1] : $company_name;
            $data['designation'] = $_POST['designation'];
            $data['job_display'] = $_POST['job_display'];
            $data['company_name'] = (isset($_POST['company_name']) && $_POST['company_name'] != "") ? $_POST['company_name'] : $company_name;
            $data['hide_address'] = $_POST['hide_address'];
            $data['title'] = 'Post Jobs';
            $data['optBlueJob'] = $_POST['optBlueJob'];

            $data['job_areas'] = $this->JobArea->select_all_area();
            $data['area_size'] = 5;
            $data['area_name'] = 'lstExperienceList';
            $data['area_id'] = 'lstExperienceList';

            $data['business_areas'] = $this->BusinessType->select_all_business_type();
            $data['business_type_size'] = 5;
            $data['business_name'] = 'lstBusinessList';
            $data['business_id'] = 'lstBusinessList';

            $this->load->view('employer_jobposting_step02', $data);
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    function job_posting_step02_save() {
        if ($this->session->userdata('logged_in')) {
            $this->load->model('NewJob');
            $this->load->model('invoice');
            $this->NewJob->save_job_step01();

            $voucher_id = $this->NewJob->voucher_id;
            $data['invoice'] = $this->invoice->get_invoice($voucher_id);

            $data['error'] = "sorry, something is wrong.";
            $content = $this->load->view('invoice', $data, TRUE);
            $to = $this->NewJob->com_email;
           // $to = 'rifat_callin@yahoo.com';
            $from = 'info@ckrbd.com';
            $subject = 'Payment Voucher';

            $this->NewJob->send_mail($to, $from, $content, $subject, '', '');
            redirect('home');
          
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    function job_repost() {
        if ($this->session->userdata("logged_in")) {
            $this->load->model('NewJob');
           //$this->NewJob->save_job_step01();
            $this->load->model('invoice');
            
            
            $deadline = $_POST['repost_deadline'];
            $job_id = $_POST['repost_job_id'];

            $this->NewJob->repost_job($job_id, $deadline);
            
           // $content = $this->load->view('invoice', $data, TRUE);
             $voucher_id = $this->NewJob->voucher_id;
            $data['invoice'] = $this->invoice->get_invoice($voucher_id);
            
           // $data['error'] = "sorry, something is wrong.";
            
            $content = $this->load->view('invoice', $data, TRUE);
            $to = $this->NewJob->com_email;
//           $to= "rifat_callin@yahoo.com";
           // $to = 'rifat_callin@yahoo.com';
            $from = 'info@ckrbd.com';
            $subject = 'Payment Voucher';

            $this->NewJob->send_mail($to, $from, $content, $subject, '', '');
           
            redirect('home');
        } else {
            redirect('login', 'refresh');
        }
        
     
    }
    
//    function job_repost1() {
//        if ($this->session->userdata("logged_in")) {
//            $this->load->model('NewJob');
//            $deadline = $_POST['repost_deadline'];
//            $job_id = $_POST['repost_job_id'];
//
//            $this->NewJob->repost_job($job_id, $deadline);
//            
//            redirect('home');
//        } else {
//            redirect('login', 'refresh');
//        }
        
     
 //   }

}