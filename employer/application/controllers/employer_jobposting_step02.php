<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
error_reporting(0);
class employer_jobposting_step02 extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Post Jobs';
		$this->load->view('employer_jobposting_step02', $data);
	}

}