<?php

/**
 * Description of cont_employee_list
 *
 * @author Anick 
 * date: 01/01/2013
 */
class cont_employee_list extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->library('pagination');
    }

    function all_employees() {
        if ($this->session->userdata('logged_in')) {
            $this->load->model('employees_list');
            $this->load->model('Location');
            $this->load->model('BusinessType');
            $data['employees'] = $this->employees_list->all_employees();
            $data['title'] = 'Employee List';

            $data['district']['districts'] = $this->Location->select_all_cities();
            $data['district']['district_size'] = 1;
            $data['district']['initial_value'] = 'Any';
            $data['district']['district_name'] = 'lstJobArea';
            $data['district']['district_id'] = 'lstJobArea';

            $data['business']['business_areas'] = $this->BusinessType->select_all_business_type();
            $data['business']['business_type_size'] = 1;
            $data['district']['initial_value'] = 'Any';
            $data['business']['business_name'] = 'lstOrganization';
            $data['business']['business_id'] = 'lstOrganization';

//            //pagination configuration
//            $config['total_rows'] = count($data['employees']);
//            $config['per_page'] = 1;
//            $config['page_query_string'] = true;
//            //$config['use_page_numbers'] = TRUE;
//            $config['base_url'] = site_url() . '/cont_employee_list/search_cv/';
//            
//            $this->pagination->initialize($config);
//            $page = isset ($_GET['page']) ? $_GET['page'] : 1;
//            $data["links"] = $this->pagination->create_links();

            $this->load->view('employees_list', $data);
        }
    }

    function search_cv() {
        if ($this->session->userdata('logged_in')) {
            $this->load->model('employees_list');
            $this->load->model('Location');
            $this->load->model('BusinessType');
            $data['title'] = 'Searched Employee List';

            $per_page = 20;
            $num_of_links=$this->uri->segment(3);
//            $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

            $data['district']['districts'] = $this->Location->select_all_cities();
            $data['district']['district_size'] = 1;
            $data['district']['initial_value'] = 'Any';
            $data['district']['district_name'] = 'lstJobArea';
            $data['district']['district_id'] = 'lstJobArea';

            //pagination configuration

            $data['employees'] = $this->employees_list->search_cvs($per_page, $num_of_links);

            $config['per_page'] = $per_page;
            $config['total_rows'] = $this->employees_list->num_rows;
            $config['base_url'] = site_url() . '/cont_employee_list/search_cv';

            $this->pagination->initialize($config);


            $data["links"] = $this->pagination->create_links();
            $this->load->view('employees_list', $data);
        }
    }

    function view_applied_employees() {
        $job_id = isset($_GET['job_id']) ? $_GET['job_id'] : 0;

        if ($job_id != 0) {
            $this->load->model('employees_list');
            $this->load->model('Location');
            $this->load->model('BusinessType');
            $data['title'] = 'Applied Employee List';

            $per_page = 20;
            $num_of_links = (isset($_GET['per_page']) && $_GET['per_page'] != '') ? $_GET['per_page'] : 0;

            $data['district']['districts'] = $this->Location->select_all_cities();
            $data['district']['district_size'] = 1;
            $data['district']['initial_value'] = 'Any';
            $data['district']['district_name'] = 'lstJobArea';
            $data['district']['district_id'] = 'lstJobArea';

            //pagination configuration

            $data['employees'] = $this->employees_list->get_applied_employees($job_id, $per_page, $num_of_links);

            $config['per_page'] = $per_page;
            $config['total_rows'] = $this->employees_list->num_rows;
            $config['base_url'] = site_url() . '/cont_employee_list/view_applied_employees?job_id=' . $job_id;

            $this->pagination->initialize($config);

            $data["links"] = $this->pagination->create_links();
            $this->load->view('applied_employees_list', $data);
        }
    }

    function all_blueworker() {
        if ($this->session->userdata('logged_in')) {
            $this->load->model('employees_list');
            $this->load->model('Location');
            $this->load->model('BusinessType');
            $data['employees'] = $this->employees_list->all_blueworker();
            $data['title'] = 'Blue Worker List';

            $data['district']['districts'] = $this->Location->select_all_cities();
            $data['district']['district_size'] = 1;
            $data['district']['initial_value'] = 'Any';
            $data['district']['district_name'] = 'lstJobArea';
            $data['district']['district_id'] = 'lstJobArea';

            $data['business']['business_areas'] = $this->BusinessType->select_all_business_type();
            $data['business']['business_type_size'] = 1;
            $data['district']['initial_value'] = 'Any';
            $data['business']['business_name'] = 'lstOrganization';
            $data['business']['business_id'] = 'lstOrganization';

//            //pagination configuration
//            $config['total_rows'] = count($data['employees']);
//            $config['per_page'] = 1;
//            $config['page_query_string'] = true;
//            //$config['use_page_numbers'] = TRUE;
//            $config['base_url'] = site_url() . '/cont_employee_list/search_cv/';
//            
//            $this->pagination->initialize($config);
//            $page = isset ($_GET['page']) ? $_GET['page'] : 1;
//            $data["links"] = $this->pagination->create_links();

            $this->load->view('blueworker_list', $data);
        }
    }

}

?>
