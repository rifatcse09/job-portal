<?php
class Category extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    function select_all_category(){
        $query = $this->db->get_where('job_category',array('is_active' => 1));
        return $query->result_array();
    }
}
?>
