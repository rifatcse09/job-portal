<?
$size = isset($business_type_size) ? $business_type_size : 1;
$name = isset($business_name) ? $business_name : 'business_area_id';
$id = isset($business_id) ? $business_id : 'business_area_id';
?>
<select class="form-control" id="<?= $id ?>" style="width:180px;" size="<?= $size; ?>" name="<?= $name ?>">
    <?php
    if ($business_areas) {
        foreach ($business_areas as $business_area) {
            echo '<option value="' . $business_area['business_id'] . '">' . $business_area['business_name'] . '</option>';
        }
    }
    ?>
</select>
