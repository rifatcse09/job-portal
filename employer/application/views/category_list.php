<? $size = isset($category_size) ? $category_size : 1; 
	$name = isset($select_name) ? $select_name : 'category_id';
	$id = isset($select_id) ? $select_id : 'category_id';
?>
<select id="<?=$id?>" style="width:180px;" size="<?=$size;?>" name="<?=$name?>" class="form-control">
    <option value="">Uncategorized</option>
<?php
    $selected = isset($selected_value) ? $selected_value : '';
        
    if($categories){
        foreach ($categories as $category){
            $attribute = ($selected == $category['category_id']) ? 'selected="selected"' : '';
            echo '<option value="' . $category['category_id'] . '" ' . $attribute . '>' . $category['category_name'] . '</option>';
        }
    }
?>
</select>
