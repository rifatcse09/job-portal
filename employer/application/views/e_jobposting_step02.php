<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?= $title; ?></title>
        <link rel="stylesheet" href="<?= base_url(); ?>css/style.css" type="text/css" />
        <script language="javascript" src="<?= base_url(); ?>js/JobPosting.js"></script>
        <script language="javascript" src="<?= base_url(); ?>js/charCount.js"></script>
        <SCRIPT language='javascript'>
            var i, j;
            arrDistrict = new Array(3);
            for(i = 0;i < 3; i++)
            {
                arrDistrict[i] = new Array(64);
                for(j = 0;j < 64; j++)arrDistrict[i][j]=""
            }
        </SCRIPT>
        <SCRIPT language='javascript'>
            arrDistrict[0][0] = "1"
            arrDistrict[0][1] = "2"
            arrDistrict[0][2] = "3"
            arrDistrict[0][3] = "4"
            arrDistrict[0][4] = "5"
            arrDistrict[0][5] = "6"
            arrDistrict[0][6] = "7"
            arrDistrict[0][7] = "8"
            arrDistrict[0][8] = "9"
            arrDistrict[0][9] = "10"
            arrDistrict[0][10] = "11"
            arrDistrict[0][11] = "12"
            arrDistrict[0][12] = "13"
            arrDistrict[0][13] = "14"
            arrDistrict[0][14] = "15"
            arrDistrict[0][15] = "16"
            arrDistrict[0][16] = "17"
            arrDistrict[0][17] = "18"
            arrDistrict[0][18] = "19"
            arrDistrict[0][19] = "20"
            arrDistrict[0][20] = "21"
            arrDistrict[0][21] = "22"
            arrDistrict[0][22] = "23"
            arrDistrict[0][23] = "24"
            arrDistrict[0][24] = "25"
            arrDistrict[0][25] = "26"
            arrDistrict[0][26] = "27"
            arrDistrict[0][27] = "28"
            arrDistrict[0][28] = "29"
            arrDistrict[0][29] = "30"
            arrDistrict[0][30] = "31"
            arrDistrict[0][31] = "32"
            arrDistrict[0][32] = "33"
            arrDistrict[0][33] = "34"
            arrDistrict[0][34] = "35"
            arrDistrict[0][35] = "36"
            arrDistrict[0][36] = "37"
            arrDistrict[0][37] = "38"
            arrDistrict[0][38] = "39"
            arrDistrict[0][39] = "40"
            arrDistrict[0][40] = "41"
            arrDistrict[0][41] = "42"
            arrDistrict[0][42] = "43"
            arrDistrict[0][43] = "44"
            arrDistrict[0][44] = "45"
            arrDistrict[0][45] = "46"
            arrDistrict[0][46] = "47"
            arrDistrict[0][47] = "48"
            arrDistrict[0][48] = "49"
            arrDistrict[0][49] = "50"
            arrDistrict[0][50] = "51"
            arrDistrict[0][51] = "52"
            arrDistrict[0][52] = "53"
            arrDistrict[0][53] = "54"
            arrDistrict[0][54] = "55"
            arrDistrict[0][55] = "56"
            arrDistrict[0][56] = "57"
            arrDistrict[0][57] = "58"
            arrDistrict[0][58] = "59"
            arrDistrict[0][59] = "60"
            arrDistrict[0][60] = "61"
            arrDistrict[0][61] = "62"
            arrDistrict[0][62] = "63"
            arrDistrict[0][63] = "64"
            arrDistrict[1][0] = "B. Baria"
            arrDistrict[1][1] = "Bagerhat"
            arrDistrict[1][2] = "Bandarban"
            arrDistrict[1][3] = "Barisal"
            arrDistrict[1][4] = "Bhola"
            arrDistrict[1][5] = "Bogra"
            arrDistrict[1][6] = "Borguna"
            arrDistrict[1][7] = "Chandpur"
            arrDistrict[1][8] = "Chapainawabganj"
            arrDistrict[1][9] = "Chittagong"
            arrDistrict[1][10] = "Chuadanga"
            arrDistrict[1][11] = "Comilla"
            arrDistrict[1][12] = "Cox's Bazar"
            arrDistrict[1][13] = "Dhaka"
            arrDistrict[1][14] = "Dinajpur"
            arrDistrict[1][15] = "Faridpur"
            arrDistrict[1][16] = "Feni"
            arrDistrict[1][17] = "Gaibandha"
            arrDistrict[1][18] = "Gazipur"
            arrDistrict[1][19] = "Gopalgonj"
            arrDistrict[1][20] = "Hobigonj"
            arrDistrict[1][21] = "Jamalpur"
            arrDistrict[1][22] = "Jessore"
            arrDistrict[1][23] = "Jhalokathi"
            arrDistrict[1][24] = "Jhenaidah"
            arrDistrict[1][25] = "Joypurhat"
            arrDistrict[1][26] = "Khagrachari"
            arrDistrict[1][27] = "Khulna"
            arrDistrict[1][28] = "Kishorgonj"
            arrDistrict[1][29] = "Kurigram"
            arrDistrict[1][30] = "Kushtia"
            arrDistrict[1][31] = "Lalmonirhat"
            arrDistrict[1][32] = "Laxmipur"
            arrDistrict[1][33] = "Madaripur"
            arrDistrict[1][34] = "Magura"
            arrDistrict[1][35] = "Manikgonj"
            arrDistrict[1][36] = "Meherpur"
            arrDistrict[1][37] = "MoulaviBazar"
            arrDistrict[1][38] = "Munshigonj"
            arrDistrict[1][39] = "Mymensingh"
            arrDistrict[1][40] = "Naogaon"
            arrDistrict[1][41] = "Narail"
            arrDistrict[1][42] = "Narayangonj"
            arrDistrict[1][43] = "Narshingdi"
            arrDistrict[1][44] = "Natore"
            arrDistrict[1][45] = "Netrokona"
            arrDistrict[1][46] = "Nilphamari"
            arrDistrict[1][47] = "Noakhali"
            arrDistrict[1][48] = "Pabna"
            arrDistrict[1][49] = "Panchagarh"
            arrDistrict[1][50] = "Patuakhali"
            arrDistrict[1][51] = "Pirojpur"
            arrDistrict[1][52] = "Rajbari"
            arrDistrict[1][53] = "Rajshahi"
            arrDistrict[1][54] = "Rangamati"
            arrDistrict[1][55] = "Rangpur"
            arrDistrict[1][56] = "Satkhira"
            arrDistrict[1][57] = "Shariatpur"
            arrDistrict[1][58] = "Sherpur"
            arrDistrict[1][59] = "Sirajgonj"
            arrDistrict[1][60] = "Sunamgonj"
            arrDistrict[1][61] = "Sylhet"
            arrDistrict[1][62] = "Tangail"
            arrDistrict[1][63] = "Thakurgaon"
            arrDistrict[2][0] = "Chittagong"
            arrDistrict[2][1] = "Khulna"
            arrDistrict[2][2] = "Chittagong"
            arrDistrict[2][3] = "Barisal"
            arrDistrict[2][4] = "Barisal"
            arrDistrict[2][5] = "Rajshahi"
            arrDistrict[2][6] = "Barisal"
            arrDistrict[2][7] = "Chittagong"
            arrDistrict[2][8] = "Rajshahi"
            arrDistrict[2][9] = "Chittagong"
            arrDistrict[2][10] = "Khulna"
            arrDistrict[2][11] = "Chittagong"
            arrDistrict[2][12] = "Chittagong"
            arrDistrict[2][13] = "Dhaka"
            arrDistrict[2][14] = "Rangpur"
            arrDistrict[2][15] = "Dhaka"
            arrDistrict[2][16] = "Chittagong"
            arrDistrict[2][17] = "Rangpur"
            arrDistrict[2][18] = "Dhaka"
            arrDistrict[2][19] = "Dhaka"
            arrDistrict[2][20] = "Sylhet"
            arrDistrict[2][21] = "Dhaka"
            arrDistrict[2][22] = "Khulna"
            arrDistrict[2][23] = "Barisal"
            arrDistrict[2][24] = "Khulna"
            arrDistrict[2][25] = "Rajshahi"
            arrDistrict[2][26] = "Chittagong"
            arrDistrict[2][27] = "Khulna"
            arrDistrict[2][28] = "Dhaka"
            arrDistrict[2][29] = "Rangpur"
            arrDistrict[2][30] = "Khulna"
            arrDistrict[2][31] = "Rangpur"
            arrDistrict[2][32] = "Chittagong"
            arrDistrict[2][33] = "Dhaka"
            arrDistrict[2][34] = "Khulna"
            arrDistrict[2][35] = "Dhaka"
            arrDistrict[2][36] = "Khulna"
            arrDistrict[2][37] = "Sylhet"
            arrDistrict[2][38] = "Dhaka"
            arrDistrict[2][39] = "Dhaka"
            arrDistrict[2][40] = "Rajshahi"
            arrDistrict[2][41] = "Khulna"
            arrDistrict[2][42] = "Dhaka"
            arrDistrict[2][43] = "Dhaka"
            arrDistrict[2][44] = "Rajshahi"
            arrDistrict[2][45] = "Dhaka"
            arrDistrict[2][46] = "Rangpur"
            arrDistrict[2][47] = "Chittagong"
            arrDistrict[2][48] = "Rajshahi"
            arrDistrict[2][49] = "Rangpur"
            arrDistrict[2][50] = "Barisal"
            arrDistrict[2][51] = "Barisal"
            arrDistrict[2][52] = "Dhaka"
            arrDistrict[2][53] = "Rajshahi"
            arrDistrict[2][54] = "Chittagong"
            arrDistrict[2][55] = "Rangpur"
            arrDistrict[2][56] = "Khulna"
            arrDistrict[2][57] = "Dhaka"
            arrDistrict[2][58] = "Dhaka"
            arrDistrict[2][59] = "Rajshahi"
            arrDistrict[2][60] = "Sylhet"
            arrDistrict[2][61] = "Sylhet"
            arrDistrict[2][62] = "Dhaka"
            arrDistrict[2][63] = "Rangpur"
        </SCRIPT>
        <SCRIPT language='javascript'>
            var i, j;
            arrCountry = new Array(3);
            for(i = 0;i < 3; i++)
            {
                arrCountry[i] = new Array(224);
                for(j = 0;j < 224; j++)arrCountry[i][j]=""
            }
        </SCRIPT>
        <SCRIPT language='javascript'>
            arrCountry[0][0] = "101"
            arrCountry[0][1] = "102"
            arrCountry[0][2] = "103"
            arrCountry[0][3] = "104"
            arrCountry[0][4] = "105"
            arrCountry[0][5] = "106"
            arrCountry[0][6] = "107"
            arrCountry[0][7] = "108"
            arrCountry[0][8] = "109"
            arrCountry[0][9] = "110"
            arrCountry[0][10] = "111"
            arrCountry[0][11] = "112"
            arrCountry[0][12] = "113"
            arrCountry[0][13] = "114"
            arrCountry[0][14] = "115"
            arrCountry[0][15] = "116"
            arrCountry[0][16] = "117"
            arrCountry[0][17] = "118"
            arrCountry[0][18] = "119"
            arrCountry[0][19] = "120"
            arrCountry[0][20] = "121"
            arrCountry[0][21] = "122"
            arrCountry[0][22] = "123"
            arrCountry[0][23] = "124"
            arrCountry[0][24] = "125"
            arrCountry[0][25] = "126"
            arrCountry[0][26] = "127"
            arrCountry[0][27] = "128"
            arrCountry[0][28] = "129"
            arrCountry[0][29] = "130"
            arrCountry[0][30] = "131"
            arrCountry[0][31] = "132"
            arrCountry[0][32] = "133"
            arrCountry[0][33] = "134"
            arrCountry[0][34] = "135"
            arrCountry[0][35] = "136"
            arrCountry[0][36] = "137"
            arrCountry[0][37] = "138"
            arrCountry[0][38] = "139"
            arrCountry[0][39] = "140"
            arrCountry[0][40] = "141"
            arrCountry[0][41] = "142"
            arrCountry[0][42] = "143"
            arrCountry[0][43] = "144"
            arrCountry[0][44] = "145"
            arrCountry[0][45] = "147"
            arrCountry[0][46] = "146"
            arrCountry[0][47] = "148"
            arrCountry[0][48] = "149"
            arrCountry[0][49] = "150"
            arrCountry[0][50] = "151"
            arrCountry[0][51] = "152"
            arrCountry[0][52] = "153"
            arrCountry[0][53] = "154"
            arrCountry[0][54] = "155"
            arrCountry[0][55] = "156"
            arrCountry[0][56] = "157"
            arrCountry[0][57] = "158"
            arrCountry[0][58] = "323"
            arrCountry[0][59] = "159"
            arrCountry[0][60] = "160"
            arrCountry[0][61] = "161"
            arrCountry[0][62] = "162"
            arrCountry[0][63] = "163"
            arrCountry[0][64] = "164"
            arrCountry[0][65] = "165"
            arrCountry[0][66] = "166"
            arrCountry[0][67] = "232"
            arrCountry[0][68] = "167"
            arrCountry[0][69] = "168"
            arrCountry[0][70] = "169"
            arrCountry[0][71] = "170"
            arrCountry[0][72] = "171"
            arrCountry[0][73] = "172"
            arrCountry[0][74] = "174"
            arrCountry[0][75] = "175"
            arrCountry[0][76] = "176"
            arrCountry[0][77] = "177"
            arrCountry[0][78] = "178"
            arrCountry[0][79] = "179"
            arrCountry[0][80] = "180"
            arrCountry[0][81] = "181"
            arrCountry[0][82] = "182"
            arrCountry[0][83] = "183"
            arrCountry[0][84] = "184"
            arrCountry[0][85] = "185"
            arrCountry[0][86] = "186"
            arrCountry[0][87] = "187"
            arrCountry[0][88] = "188"
            arrCountry[0][89] = "190"
            arrCountry[0][90] = "191"
            arrCountry[0][91] = "192"
            arrCountry[0][92] = "193"
            arrCountry[0][93] = "194"
            arrCountry[0][94] = "195"
            arrCountry[0][95] = "196"
            arrCountry[0][96] = "197"
            arrCountry[0][97] = "198"
            arrCountry[0][98] = "199"
            arrCountry[0][99] = "200"
            arrCountry[0][100] = "201"
            arrCountry[0][101] = "202"
            arrCountry[0][102] = "203"
            arrCountry[0][103] = "204"
            arrCountry[0][104] = "205"
            arrCountry[0][105] = "206"
            arrCountry[0][106] = "207"
            arrCountry[0][107] = "208"
            arrCountry[0][108] = "209"
            arrCountry[0][109] = "210"
            arrCountry[0][110] = "211"
            arrCountry[0][111] = "212"
            arrCountry[0][112] = "213"
            arrCountry[0][113] = "214"
            arrCountry[0][114] = "215"
            arrCountry[0][115] = "216"
            arrCountry[0][116] = "217"
            arrCountry[0][117] = "218"
            arrCountry[0][118] = "219"
            arrCountry[0][119] = "220"
            arrCountry[0][120] = "221"
            arrCountry[0][121] = "222"
            arrCountry[0][122] = "223"
            arrCountry[0][123] = "224"
            arrCountry[0][124] = "225"
            arrCountry[0][125] = "226"
            arrCountry[0][126] = "227"
            arrCountry[0][127] = "228"
            arrCountry[0][128] = "229"
            arrCountry[0][129] = "230"
            arrCountry[0][130] = "231"
            arrCountry[0][131] = "233"
            arrCountry[0][132] = "234"
            arrCountry[0][133] = "235"
            arrCountry[0][134] = "236"
            arrCountry[0][135] = "237"
            arrCountry[0][136] = "238"
            arrCountry[0][137] = "239"
            arrCountry[0][138] = "240"
            arrCountry[0][139] = "241"
            arrCountry[0][140] = "242"
            arrCountry[0][141] = "243"
            arrCountry[0][142] = "244"
            arrCountry[0][143] = "245"
            arrCountry[0][144] = "246"
            arrCountry[0][145] = "247"
            arrCountry[0][146] = "248"
            arrCountry[0][147] = "249"
            arrCountry[0][148] = "250"
            arrCountry[0][149] = "251"
            arrCountry[0][150] = "252"
            arrCountry[0][151] = "253"
            arrCountry[0][152] = "254"
            arrCountry[0][153] = "255"
            arrCountry[0][154] = "256"
            arrCountry[0][155] = "257"
            arrCountry[0][156] = "258"
            arrCountry[0][157] = "259"
            arrCountry[0][158] = "260"
            arrCountry[0][159] = "261"
            arrCountry[0][160] = "262"
            arrCountry[0][161] = "263"
            arrCountry[0][162] = "264"
            arrCountry[0][163] = "265"
            arrCountry[0][164] = "266"
            arrCountry[0][165] = "267"
            arrCountry[0][166] = "268"
            arrCountry[0][167] = "269"
            arrCountry[0][168] = "270"
            arrCountry[0][169] = "271"
            arrCountry[0][170] = "272"
            arrCountry[0][171] = "273"
            arrCountry[0][172] = "274"
            arrCountry[0][173] = "275"
            arrCountry[0][174] = "276"
            arrCountry[0][175] = "277"
            arrCountry[0][176] = "278"
            arrCountry[0][177] = "279"
            arrCountry[0][178] = "280"
            arrCountry[0][179] = "281"
            arrCountry[0][180] = "282"
            arrCountry[0][181] = "283"
            arrCountry[0][182] = "284"
            arrCountry[0][183] = "285"
            arrCountry[0][184] = "286"
            arrCountry[0][185] = "287"
            arrCountry[0][186] = "324"
            arrCountry[0][187] = "288"
            arrCountry[0][188] = "289"
            arrCountry[0][189] = "290"
            arrCountry[0][190] = "291"
            arrCountry[0][191] = "292"
            arrCountry[0][192] = "293"
            arrCountry[0][193] = "294"
            arrCountry[0][194] = "295"
            arrCountry[0][195] = "296"
            arrCountry[0][196] = "297"
            arrCountry[0][197] = "298"
            arrCountry[0][198] = "299"
            arrCountry[0][199] = "173"
            arrCountry[0][200] = "189"
            arrCountry[0][201] = "300"
            arrCountry[0][202] = "301"
            arrCountry[0][203] = "302"
            arrCountry[0][204] = "303"
            arrCountry[0][205] = "304"
            arrCountry[0][206] = "305"
            arrCountry[0][207] = "306"
            arrCountry[0][208] = "307"
            arrCountry[0][209] = "308"
            arrCountry[0][210] = "309"
            arrCountry[0][211] = "310"
            arrCountry[0][212] = "311"
            arrCountry[0][213] = "312"
            arrCountry[0][214] = "313"
            arrCountry[0][215] = "314"
            arrCountry[0][216] = "315"
            arrCountry[0][217] = "316"
            arrCountry[0][218] = "317"
            arrCountry[0][219] = "318"
            arrCountry[0][220] = "319"
            arrCountry[0][221] = "320"
            arrCountry[0][222] = "321"
            arrCountry[0][223] = "322"
            arrCountry[1][0] = "Afghanistan"
            arrCountry[1][1] = "Albania"
            arrCountry[1][2] = "Algeria"
            arrCountry[1][3] = "American Samoa"
            arrCountry[1][4] = "Andorra"
            arrCountry[1][5] = "Angola"
            arrCountry[1][6] = "Anguilla"
            arrCountry[1][7] = "Antarctica"
            arrCountry[1][8] = "Antigua"
            arrCountry[1][9] = "Argentina"
            arrCountry[1][10] = "Armenia"
            arrCountry[1][11] = "Aruba"
            arrCountry[1][12] = "Australia"
            arrCountry[1][13] = "Austria"
            arrCountry[1][14] = "Azerbaijan"
            arrCountry[1][15] = "Bahamas"
            arrCountry[1][16] = "Bahrain"
            arrCountry[1][17] = "Bangladesh"
            arrCountry[1][18] = "Barbados"
            arrCountry[1][19] = "Belarus"
            arrCountry[1][20] = "Belgium"
            arrCountry[1][21] = "Belize"
            arrCountry[1][22] = "Benin"
            arrCountry[1][23] = "Bermuda"
            arrCountry[1][24] = "Bhutan"
            arrCountry[1][25] = "Bolivia"
            arrCountry[1][26] = "Bosnia and Herzegovina"
            arrCountry[1][27] = "Botswana"
            arrCountry[1][28] = "Brazil"
            arrCountry[1][29] = "British Virgin Islands"
            arrCountry[1][30] = "Brunei"
            arrCountry[1][31] = "Bulgaria"
            arrCountry[1][32] = "Burkina Faso"
            arrCountry[1][33] = "Burma"
            arrCountry[1][34] = "Burundi"
            arrCountry[1][35] = "Cambodia"
            arrCountry[1][36] = "Cameroon"
            arrCountry[1][37] = "Canada"
            arrCountry[1][38] = "Cape Verde"
            arrCountry[1][39] = "Central African Republic"
            arrCountry[1][40] = "Chad"
            arrCountry[1][41] = "Chile"
            arrCountry[1][42] = "China"
            arrCountry[1][43] = "Colombia"
            arrCountry[1][44] = "Comoros"
            arrCountry[1][45] = "Congo"
            arrCountry[1][46] = "Congo (Zaire)"
            arrCountry[1][47] = "Cook Islands"
            arrCountry[1][48] = "Costa Rica"
            arrCountry[1][49] = "Cote d'Ivoire (Ivory Coast)"
            arrCountry[1][50] = "Croatia"
            arrCountry[1][51] = "Cuba"
            arrCountry[1][52] = "Cyprus"
            arrCountry[1][53] = "Czech Republic"
            arrCountry[1][54] = "Denmark"
            arrCountry[1][55] = "Djibouti"
            arrCountry[1][56] = "Dominica"
            arrCountry[1][57] = "Dominican Republic"
            arrCountry[1][58] = "East Timor"
            arrCountry[1][59] = "Ecuador"
            arrCountry[1][60] = "Egypt"
            arrCountry[1][61] = "El Salvador"
            arrCountry[1][62] = "Equatorial Guinea"
            arrCountry[1][63] = "Eritrea"
            arrCountry[1][64] = "Estonia"
            arrCountry[1][65] = "Ethiopia"
            arrCountry[1][66] = "Falkland Islands"
            arrCountry[1][67] = "Federated States of Micronesia"
            arrCountry[1][68] = "Fiji"
            arrCountry[1][69] = "Finland"
            arrCountry[1][70] = "France"
            arrCountry[1][71] = "French Guiana"
            arrCountry[1][72] = "French Polynesia"
            arrCountry[1][73] = "Gabon"
            arrCountry[1][74] = "Gaza Strip and West Bank"
            arrCountry[1][75] = "Georgia"
            arrCountry[1][76] = "Germany"
            arrCountry[1][77] = "Ghana"
            arrCountry[1][78] = "Gibraltar"
            arrCountry[1][79] = "Greece"
            arrCountry[1][80] = "Greenland"
            arrCountry[1][81] = "Grenada"
            arrCountry[1][82] = "Guadeloupe"
            arrCountry[1][83] = "Guam"
            arrCountry[1][84] = "Guatemala"
            arrCountry[1][85] = "Guinea"
            arrCountry[1][86] = "Guinea-Bissau"
            arrCountry[1][87] = "Guyana"
            arrCountry[1][88] = "Haiti"
            arrCountry[1][89] = "Honduras"
            arrCountry[1][90] = "Hong Kong"
            arrCountry[1][91] = "Hungary"
            arrCountry[1][92] = "Iceland"
            arrCountry[1][93] = "India"
            arrCountry[1][94] = "Indonesia"
            arrCountry[1][95] = "Iran"
            arrCountry[1][96] = "Iraq"
            arrCountry[1][97] = "Ireland"
            arrCountry[1][98] = "Israel"
            arrCountry[1][99] = "Italy"
            arrCountry[1][100] = "Jamaica"
            arrCountry[1][101] = "Japan"
            arrCountry[1][102] = "Jordan"
            arrCountry[1][103] = "Kazakhstan"
            arrCountry[1][104] = "Kenya"
            arrCountry[1][105] = "Kiribati"
            arrCountry[1][106] = "Kuwait"
            arrCountry[1][107] = "Kyrgyzstan"
            arrCountry[1][108] = "Laos"
            arrCountry[1][109] = "Latvia"
            arrCountry[1][110] = "Lebanon"
            arrCountry[1][111] = "Lesotho"
            arrCountry[1][112] = "Liberia"
            arrCountry[1][113] = "Libya"
            arrCountry[1][114] = "Liechtenstein"
            arrCountry[1][115] = "Lithuania"
            arrCountry[1][116] = "Luxembourg"
            arrCountry[1][117] = "Macau"
            arrCountry[1][118] = "Macedonia"
            arrCountry[1][119] = "Madagascar"
            arrCountry[1][120] = "Malawi"
            arrCountry[1][121] = "Malaysia"
            arrCountry[1][122] = "Maldives"
            arrCountry[1][123] = "Mali"
            arrCountry[1][124] = "Malta"
            arrCountry[1][125] = "Marshall Islands"
            arrCountry[1][126] = "Martinique"
            arrCountry[1][127] = "Mauritania"
            arrCountry[1][128] = "Mauritius"
            arrCountry[1][129] = "Mayotte"
            arrCountry[1][130] = "Mexico"
            arrCountry[1][131] = "Moldova"
            arrCountry[1][132] = "Monaco"
            arrCountry[1][133] = "Mongolia"
            arrCountry[1][134] = "Montserrat"
            arrCountry[1][135] = "Morocco"
            arrCountry[1][136] = "Mozambique"
            arrCountry[1][137] = "Namibia"
            arrCountry[1][138] = "Nauru"
            arrCountry[1][139] = "Nepal"
            arrCountry[1][140] = "Netherlands"
            arrCountry[1][141] = "Netherlands Antilles"
            arrCountry[1][142] = "New Caledonia"
            arrCountry[1][143] = "New Zealand"
            arrCountry[1][144] = "Nicaragua"
            arrCountry[1][145] = "Niger"
            arrCountry[1][146] = "Nigeria"
            arrCountry[1][147] = "North Korea"
            arrCountry[1][148] = "Northern Mariana Islands"
            arrCountry[1][149] = "Norway"
            arrCountry[1][150] = "Oman"
            arrCountry[1][151] = "Pakistan"
            arrCountry[1][152] = "Palau"
            arrCountry[1][153] = "Panama"
            arrCountry[1][154] = "Papua New Guinea"
            arrCountry[1][155] = "Paraguay"
            arrCountry[1][156] = "Peru"
            arrCountry[1][157] = "Philippines"
            arrCountry[1][158] = "Pitcairn Islands"
            arrCountry[1][159] = "Poland"
            arrCountry[1][160] = "Portugal"
            arrCountry[1][161] = "Puerto Rico"
            arrCountry[1][162] = "Qatar"
            arrCountry[1][163] = "Reunion"
            arrCountry[1][164] = "Romania"
            arrCountry[1][165] = "Russia"
            arrCountry[1][166] = "Rwanda"
            arrCountry[1][167] = "Saint Kitts and Nevis"
            arrCountry[1][168] = "Saint Lucia"
            arrCountry[1][169] = "Saint Pierre and Miquelon"
            arrCountry[1][170] = "Saint Vincent and the Grenadines"
            arrCountry[1][171] = "Samoa"
            arrCountry[1][172] = "San Marino"
            arrCountry[1][173] = "Sao Tome and Principe"
            arrCountry[1][174] = "Saudi Arabia"
            arrCountry[1][175] = "Senegal"
            arrCountry[1][176] = "Serbia and Montenegro (Yugoslavia)"
            arrCountry[1][177] = "Seychelles"
            arrCountry[1][178] = "Sierra Leone"
            arrCountry[1][179] = "Singapore"
            arrCountry[1][180] = "Slovakia"
            arrCountry[1][181] = "Slovenia"
            arrCountry[1][182] = "Solomon Islands"
            arrCountry[1][183] = "Somalia"
            arrCountry[1][184] = "South Africa"
            arrCountry[1][185] = "South Korea"
            arrCountry[1][186] = "South Sudan"
            arrCountry[1][187] = "Spain"
            arrCountry[1][188] = "Sri Lanka"
            arrCountry[1][189] = "Sudan"
            arrCountry[1][190] = "Suriname"
            arrCountry[1][191] = "Swaziland"
            arrCountry[1][192] = "Sweden"
            arrCountry[1][193] = "Switzerland"
            arrCountry[1][194] = "Syria"
            arrCountry[1][195] = "Taiwan"
            arrCountry[1][196] = "Tajikistan"
            arrCountry[1][197] = "Tanzania"
            arrCountry[1][198] = "Thailand"
            arrCountry[1][199] = "The Gambia"
            arrCountry[1][200] = "The Holy See"
            arrCountry[1][202] = "Tonga"
            arrCountry[1][203] = "Trinidad and Tobago"
            arrCountry[1][204] = "Tunisia"
            arrCountry[1][205] = "Turkey"
            arrCountry[1][206] = "Turkmenistan"
            arrCountry[1][207] = "Turks and Caicos Islands"
            arrCountry[1][208] = "Tuvalu"
            arrCountry[1][209] = "Uganda"
            arrCountry[1][210] = "Ukraine"
            arrCountry[1][211] = "United Arab Emirates"
            arrCountry[1][212] = "United Kingdom"
            arrCountry[1][213] = "United States"
            arrCountry[1][214] = "United States Virgin Islands"
            arrCountry[1][215] = "Uruguay"
            arrCountry[1][201] = "Togo"
            arrCountry[1][216] = "Uzbekistan"
            arrCountry[1][217] = "Vanuatu"
            arrCountry[1][218] = "Venezuela"
            arrCountry[1][219] = "Vietnam"
            arrCountry[1][220] = "West Bank and Gaza Strip"
            arrCountry[1][221] = "Western Sahara"
            arrCountry[1][222] = "Yemen"
            arrCountry[1][223] = "Zambia"
            arrCountry[2][0] = "Country"
            arrCountry[2][1] = "Country"
            arrCountry[2][2] = "Country"
            arrCountry[2][3] = "Country"
            arrCountry[2][4] = "Country"
            arrCountry[2][5] = "Country"
            arrCountry[2][6] = "Country"
            arrCountry[2][7] = "Country"
            arrCountry[2][8] = "Country"
            arrCountry[2][9] = "Country"
            arrCountry[2][10] = "Country"
            arrCountry[2][11] = "Country"
            arrCountry[2][12] = "Country"
            arrCountry[2][13] = "Country"
            arrCountry[2][14] = "Country"
            arrCountry[2][15] = "Country"
            arrCountry[2][16] = "Country"
            arrCountry[2][17] = "Country"
            arrCountry[2][18] = "Country"
            arrCountry[2][19] = "Country"
            arrCountry[2][20] = "Country"
            arrCountry[2][21] = "Country"
            arrCountry[2][22] = "Country"
            arrCountry[2][23] = "Country"
            arrCountry[2][24] = "Country"
            arrCountry[2][25] = "Country"
            arrCountry[2][26] = "Country"
            arrCountry[2][27] = "Country"
            arrCountry[2][28] = "Country"
            arrCountry[2][29] = "Country"
            arrCountry[2][30] = "Country"
            arrCountry[2][31] = "Country"
            arrCountry[2][32] = "Country"
            arrCountry[2][33] = "Country"
            arrCountry[2][34] = "Country"
            arrCountry[2][35] = "Country"
            arrCountry[2][36] = "Country"
            arrCountry[2][37] = "Country"
            arrCountry[2][38] = "Country"
            arrCountry[2][39] = "Country"
            arrCountry[2][40] = "Country"
            arrCountry[2][41] = "Country"
            arrCountry[2][42] = "Country"
            arrCountry[2][43] = "Country"
            arrCountry[2][44] = "Country"
            arrCountry[2][45] = "Country"
            arrCountry[2][46] = "Country"
            arrCountry[2][47] = "Country"
            arrCountry[2][48] = "Country"
            arrCountry[2][49] = "Country"
            arrCountry[2][50] = "Country"
            arrCountry[2][51] = "Country"
            arrCountry[2][52] = "Country"
            arrCountry[2][53] = "Country"
            arrCountry[2][54] = "Country"
            arrCountry[2][55] = "Country"
            arrCountry[2][56] = "Country"
            arrCountry[2][57] = "Country"
            arrCountry[2][58] = "Country"
            arrCountry[2][59] = "Country"
            arrCountry[2][60] = "Country"
            arrCountry[2][61] = "Country"
            arrCountry[2][62] = "Country"
            arrCountry[2][63] = "Country"
            arrCountry[2][64] = "Country"
            arrCountry[2][65] = "Country"
            arrCountry[2][66] = "Country"
            arrCountry[2][67] = "Country"
            arrCountry[2][68] = "Country"
            arrCountry[2][69] = "Country"
            arrCountry[2][70] = "Country"
            arrCountry[2][71] = "Country"
            arrCountry[2][72] = "Country"
            arrCountry[2][73] = "Country"
            arrCountry[2][74] = "Country"
            arrCountry[2][75] = "Country"
            arrCountry[2][76] = "Country"
            arrCountry[2][77] = "Country"
            arrCountry[2][78] = "Country"
            arrCountry[2][79] = "Country"
            arrCountry[2][80] = "Country"
            arrCountry[2][81] = "Country"
            arrCountry[2][82] = "Country"
            arrCountry[2][83] = "Country"
            arrCountry[2][84] = "Country"
            arrCountry[2][85] = "Country"
            arrCountry[2][86] = "Country"
            arrCountry[2][87] = "Country"
            arrCountry[2][88] = "Country"
            arrCountry[2][89] = "Country"
            arrCountry[2][90] = "Country"
            arrCountry[2][91] = "Country"
            arrCountry[2][92] = "Country"
            arrCountry[2][93] = "Country"
            arrCountry[2][94] = "Country"
            arrCountry[2][95] = "Country"
            arrCountry[2][96] = "Country"
            arrCountry[2][97] = "Country"
            arrCountry[2][98] = "Country"
            arrCountry[2][99] = "Country"
            arrCountry[2][100] = "Country"
            arrCountry[2][101] = "Country"
            arrCountry[2][102] = "Country"
            arrCountry[2][103] = "Country"
            arrCountry[2][104] = "Country"
            arrCountry[2][105] = "Country"
            arrCountry[2][106] = "Country"
            arrCountry[2][107] = "Country"
            arrCountry[2][108] = "Country"
            arrCountry[2][109] = "Country"
            arrCountry[2][110] = "Country"
            arrCountry[2][111] = "Country"
            arrCountry[2][112] = "Country"
            arrCountry[2][113] = "Country"
            arrCountry[2][114] = "Country"
            arrCountry[2][115] = "Country"
            arrCountry[2][116] = "Country"
            arrCountry[2][117] = "Country"
            arrCountry[2][118] = "Country"
            arrCountry[2][119] = "Country"
            arrCountry[2][120] = "Country"
            arrCountry[2][121] = "Country"
            arrCountry[2][122] = "Country"
            arrCountry[2][123] = "Country"
            arrCountry[2][124] = "Country"
            arrCountry[2][125] = "Country"
            arrCountry[2][126] = "Country"
            arrCountry[2][127] = "Country"
            arrCountry[2][128] = "Country"
            arrCountry[2][129] = "Country"
            arrCountry[2][130] = "Country"
            arrCountry[2][131] = "Country"
            arrCountry[2][132] = "Country"
            arrCountry[2][133] = "Country"
            arrCountry[2][134] = "Country"
            arrCountry[2][135] = "Country"
            arrCountry[2][136] = "Country"
            arrCountry[2][137] = "Country"
            arrCountry[2][138] = "Country"
            arrCountry[2][139] = "Country"
            arrCountry[2][140] = "Country"
            arrCountry[2][141] = "Country"
            arrCountry[2][142] = "Country"
            arrCountry[2][143] = "Country"
            arrCountry[2][144] = "Country"
            arrCountry[2][145] = "Country"
            arrCountry[2][146] = "Country"
            arrCountry[2][147] = "Country"
            arrCountry[2][148] = "Country"
            arrCountry[2][149] = "Country"
            arrCountry[2][150] = "Country"
            arrCountry[2][151] = "Country"
            arrCountry[2][152] = "Country"
            arrCountry[2][153] = "Country"
            arrCountry[2][154] = "Country"
            arrCountry[2][155] = "Country"
            arrCountry[2][156] = "Country"
            arrCountry[2][157] = "Country"
            arrCountry[2][158] = "Country"
            arrCountry[2][159] = "Country"
            arrCountry[2][160] = "Country"
            arrCountry[2][161] = "Country"
            arrCountry[2][162] = "Country"
            arrCountry[2][163] = "Country"
            arrCountry[2][164] = "Country"
            arrCountry[2][165] = "Country"
            arrCountry[2][166] = "Country"
            arrCountry[2][167] = "Country"
            arrCountry[2][168] = "Country"
            arrCountry[2][169] = "Country"
            arrCountry[2][170] = "Country"
            arrCountry[2][171] = "Country"
            arrCountry[2][172] = "Country"
            arrCountry[2][173] = "Country"
            arrCountry[2][174] = "Country"
            arrCountry[2][175] = "Country"
            arrCountry[2][176] = "Country"
            arrCountry[2][177] = "Country"
            arrCountry[2][178] = "Country"
            arrCountry[2][179] = "Country"
            arrCountry[2][180] = "Country"
            arrCountry[2][181] = "Country"
            arrCountry[2][182] = "Country"
            arrCountry[2][183] = "Country"
            arrCountry[2][184] = "Country"
            arrCountry[2][185] = "Country"
            arrCountry[2][186] = "Country"
            arrCountry[2][187] = "Country"
            arrCountry[2][188] = "Country"
            arrCountry[2][189] = "Country"
            arrCountry[2][190] = "Country"
            arrCountry[2][191] = "Country"
            arrCountry[2][192] = "Country"
            arrCountry[2][193] = "Country"
            arrCountry[2][194] = "Country"
            arrCountry[2][195] = "Country"
            arrCountry[2][196] = "Country"
            arrCountry[2][197] = "Country"
            arrCountry[2][198] = "Country"
            arrCountry[2][199] = "Country"
            arrCountry[2][200] = "Country"
            arrCountry[2][201] = "Country"
            arrCountry[2][202] = "Country"
            arrCountry[2][203] = "Country"
            arrCountry[2][204] = "Country"
            arrCountry[2][205] = "Country"
            arrCountry[2][206] = "Country"
            arrCountry[2][207] = "Country"
            arrCountry[2][208] = "Country"
            arrCountry[2][209] = "Country"
            arrCountry[2][210] = "Country"
            arrCountry[2][211] = "Country"
            arrCountry[2][212] = "Country"
            arrCountry[2][213] = "Country"
            arrCountry[2][214] = "Country"
            arrCountry[2][215] = "Country"
            arrCountry[2][216] = "Country"
            arrCountry[2][217] = "Country"
            arrCountry[2][218] = "Country"
            arrCountry[2][219] = "Country"
            arrCountry[2][220] = "Country"
            arrCountry[2][221] = "Country"
            arrCountry[2][222] = "Country"
            arrCountry[2][223] = "Country"
        </SCRIPT>
        <style type="text/css">
            #table table 
            {
                width:920px;
            }
            #table table tr th
            {
                padding:2px;
                color:#FF6600;
                border:1px solid #3f3a30;
            }   
        </style>
    </head>
    <body>

        <div id="container">
            <div id="employer_header">
            </div>

            <div id="employer_page">
                <div id="employer_menu">
                    <?= $this->load->view('employer_menu.php'); ?>	
                </div>

                <div id="main">
                    <div id="left_panel">
                        <h3 style="font-size:20px;">Corporate Panel</h3>
                        <div id="main_left"> 
                            <div id="main_left_header">
                                <h2>Main Menu</h2>
                            </div>

                            <div id="main_left_content">
                                <?= $this->load->view('employer_mainmenu'); ?>
                            </div>

                            <div id="acc_status">
                                <h2>Account Status</h2>  
                            </div>

                            <div id="acc_status_content">
                                <ul>
                                    <li><?php echo anchor('employer_jobposting_step01/', 'Job Posting', 'title="Post a New Job"') ?></li>
                                    <li><a href="#">CV Database</a></li>
                                </ul>
                            </div>

                            <div id="corporate">
                                <h3>Help Desk</h3>
                            </div>

                            <div id="corporate_content">
                                <ul>
                                    <li style="color: #a5090a;"><b>Help Desk :</b></li>
                                    <li>Phone</li>
                                    <li>Phone</li>
                                    <li>Please call us if you facing any difficulties during job posting or plan for subscribing other services.</li>
                                </ul>
                            </div>

                        </div>
                    </div>

                    <div id="right_panel">
                        <div id="right_panel_header">
                            <?= $this->load->view('employer_top'); ?>
                        </div>
                        <div id="main_box">
                            <div id="main_box_header">
                                <div id="main_box_logout">
                                   <button class="btn btn-danger" style="margin-right:50px">
                                       
                                       <i class="fa fa-sign-out"></i><a href="<?= site_url() ?>/home/logout">Log Out</a>
                                    </button>
                                </div>

                            </div>

                            <div id="rightside" class="table-responsive">
                                <form  name="frmJobPostingStep02" id="frmJobPostingStep02" enctype="multipart/form-data" action="<?= site_url() ?>/employer_jobposting_step01/job_posting_step02_save" method="post" onsubmit="return IsPostedJobValid_Step02();">
                                    <input type="hidden" name="category_id" value="<?= $category_id ?>" />
                                    <input type="hidden" name="category_name" value="<?= $category_name ?>">
                                    <input type="hidden" name="job_title" value="<?= $job_title ?>"/>
                                    <input type="hidden" name="no_vacancy" value="<?= $no_vacancy ?>">
                                    <input type="hidden" name="online_cv" value="<?= $online_cv ?>">
                                    <input type="hidden" name="email_attach" value="<?= $email_attach ?>">
                                    <input type="hidden" name="hard_copy" value="<?= $hard_copy ?>">
                                    <input type="hidden" name="corporate_email" value="<?= $corporate_email ?>">
                                    <input type="hidden" name="photogrph_cv" value="<?= $photogrph_cv ?>">
                                    <input type="hidden" name="apply_instruction" value="<?= $apply_instruction ?>">
                                    <input type="hidden" name="application_deadline" value="<?= $deadline ?>">
                                    <input type="hidden" name="billing_contact_person" value="<?= $billing_contact_person ?>">
                                    <input type="hidden" name="designation" value="<?= $designation ?>">
                                    <input type="hidden" name="job_display" value="<?= $job_display ?>">
                                    <input type="hidden" name="company_name" value="<?= $company_name ?>">
                                    <input type="hidden" name="hide_address" value="<?= $hide_address ?>">
                                    <table class="table table-striped table-bordered table-hover" style="margin-top:50px">
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Job Category</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $category_name ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Job Title</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $job_title ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">No. of Vacancies</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $no_vacancy ?></td>
                                        </tr>
                                        <tr>
                                            <td style="color:#575353;">How do you want to receive
                                                CV/Resume(s)? </td>

                                            <td style="font-family: century gothic;font-size: 10pt;">
                                                Online CV: <span style="color: #0000FF"><?= $online_cv ?></span>&nbsp;
                                                E-mail Attachment: <span style="color: #0000FF"><?= $email_attach ?></span>&nbsp;
                                                Hard copy: <span style="color: #0000FF"><?= $hard_copy ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Your Corporate E-mail</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $corporate_email ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Enclose Photograph with CV</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $photogrph_cv ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Apply Instruction(s) </td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $apply_instruction ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Application Deadline</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $application_deadline ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Billing Contact</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $billing_contact_person ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Designation</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $designation ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">In this job do you want to display Company Name</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $job_display ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Entered text to be displayed as Company Name</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $company_name ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Do you want to hide Company Address</td>

                                            <td style="font-family: century gothic;font-size: 10pt;"><?= $hide_address ?></td>
                                        </tr>
                                    </table>

                                    <table width="920" class="table table-striped table-bordered table-hover" style="margin-top:50px">
                                        <tr><td colspan="3" style="border-bottom:1px solid #000; margin:0; color:#a5090a; font-size:20px; font-weight:bold;">Job Requirements [ Step 2 of 2 ]</td></tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Age Range *</td> 
                                            <td></td> 
                                            <td>			
                                                From:<SELECT class="form-control" name="age_from" id="cboAgeFrom" style="vertical-align:middle">
                                                    <option value="-1">Any</option>

                                                    <?php
                                                    for ($i = 18; $i <= 90; $i++) {
                                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                                    }
                                                    ?>

                                                </SELECT>
                                                <span>To:</SPAN>
                                                <SELECT class="form-control" name="age_to" id="cboAgeTo" style="vertical-align:middle">
                                                    <option value="-1">Any</option>
                                                    <?php
                                                    for ($i = 18; $i <= 90; $i++) {
                                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                                    }
                                                    ?>

                                                </SELECT>
                                                <SPAN style="color:#a5090a">[Leave this field as it is if don’t want to mention age.]</SPAN>			
                                            </td> 
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #669900;"></td></tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Gender*</td>
                                            <td width="10"></td>
                                            <td><input type="radio" name="gender" value="m" /> Male Only	<input type="radio" name="gender" value="f" />Female Only<input type="radio" name="gender" value="a" checked="checked" />Any</td>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #669900;"></td></tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Job Type </td>
                                            <td></td>
                                            <td><input type="radio" name="job_type" value="Full time" checked="checked"/> Full Time	<input type="radio" name="job_type" value="Part time" />Part Time<input type="radio" name="job_type" value="Contract" />Contract</td>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #669900;"></td></tr>
                                        <tr>
                                            <td style="color:#575353; font-family: century gothic;font-size: 10pt;">Job Level *</td>
                                            <td></td>
                                            <td><input type="checkbox" name="job_level[]" id="chkJobLevel1" value="Entry" /> Entry Level Job	
                                                <input type="checkbox" name="job_level[]" id="chkJobLevel2" value="Mid" /> Mid/Managerial Level Job
                                                <input type="checkbox" name="job_level[]" id='chkJobLevel3' value="Top" />Top Level Job</td>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #669900;"></td></tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Educational Qualification *</td>
                                            <td></td>
                                            <td><textarea id="txtEducation" name="txtEducation" cols="70" rows="5"></textarea></td>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #669900;"></td></tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Job Description/Responsibility *</td>
                                            <td></td>
                                            <td><textarea id="txtJobDescription" name="txtJobDescription" cols="70" rows="5"></textarea></td>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #669900;"></td></tr>
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Additional Job Requirements *</td>
                                            <td></td>
                                            <td><textarea id="txtJobRequirements" name="txtJobRequirements" cols="70" rows="5"></textarea></td>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #669900;"></td></tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><input type="radio" name="optExperience" id="optExperienceRequired" value="1" style="vertical-align:middle;" checked="checked"  onClick="SetExperience();" />Experience Required
                                                <input type="radio" name="optExperience" id="optExperienceNotRequired" value="0" style="vertical-align:middle;"  onClick="SetExperience();" />No Experience Required</td>
                                        </tr>
                                        <tr>
                                            <td><span id="spnExperienceYear" style="font-family: century gothic;font-size: 10pt;">Total Years of Experience</span></td>
                                            <td></td>
                                            <td>
                                                <span id="spnExperienceFrom" style="color:#535A62">Min</span>: 
                                                <SELECT class="form-control" name="cboExperienceFrom" id="cboExperienceFrom" style="vertical-align:middle" >
                                                    <option value="-1">Any</option>

                                                    <?php
                                                    for ($i = 0; $i <= 50; $i++) {
                                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                                    }
                                                    ?>

                                                </SELECT>
                                                <span class="BDJGrayArial11px" id="spnExperienceTo" style="color:#535A62">
                                                    Max:
                                                    <SELECT class="form-control" name="cboExperienceTo" id="cboExperienceTo" style="vertical-align:middle" >
                                                        <option value="-1">Any</option>

                                                        <?php
                                                        for ($i = 1; $i <= 50; $i++) {
                                                            echo '<option value="' . $i . '">' . $i . '</option>';
                                                        }
                                                        ?>

                                                    </SELECT>
                                                    year(s)
                                                </span>

                                            </td>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #669900;"></td></tr>
                                        <tr>
                                            <td colspan="3" style="border-bottom:1px solid #CCCCCC;">
                                                <table class="table table-striped table-bordered table-hover" style="margin-top:50px">
                                                    <tr>
                                                        <td style="color:#006699;"><span id="spnExperienceList" style="color:#575353; ">
                                                                Do you need the candidate to have experience in the following areas?</span></td>
                                                        <td>
                                                            <?
                                                            $this->load->view('area_list', $data);
                                                            ?>
                                                        </td>
                                                        <td align="center" style="border:0px;">
                                                            <INPUT type="button" name="cmdAddExperience" id="cmdAddExperience" value="Add >" class="BDJButton2" style="margin-top:2px;" onClick="Add_Experience();UpdateExperienceIDList();" >
                                                            <br>
                                                            <INPUT type="button" name="cmdRemoveExperience" id="cmdRemoveExperience" value="< Remove" class="BDJButton2" style="margin-top:2px;"  onClick="Remove_Experience();UpdateExperienceIDList();" >
                                                            <br>
                                                            <INPUT name="txtSelectedExperienceList" type="hidden" id="txtSelectedExperienceList" size="8" value="" class="form-control" />
                                                        </td>
                                                        <td width="150" id="tdSelectedExperienceList" style="border:0px;">
                                                            <SELECT class="form-control" name="lstSelectedExperienceList" id="lstSelectedExperienceList" size="5"  style="width:180px;">

                                                            </SELECT>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table class="table table-striped table-bordered table-hover" style="margin-top:50px">
                                                    <tr>
                                                        <td style="color:#006699;"><span id="spnBusinessAreaList" style="color:#575353; ">
                                                                Do you need the candidate to have experience in the following Business Areas/Industries?
                                                            </span></td>
                                                        <td>
                                                            <?
                                                            $this->load->view('business_type_list', $data);
                                                            ?>
                                                        </td>
                                                        <td align="center" style="border:0px;">
                                                            <INPUT type="button" name="cmdAddBusiness" id="cmdAddBusiness" value="Add >" class="BDJButton2" style="margin-top:2px;" onClick="Add_BusinessAreas();UpdateBusinessIDList();" >
                                                            <br>
                                                            <INPUT type="button" name="cmdRemoveBusiness" id="cmdRemoveBusiness" value="< Remove" class="BDJButton2" style="margin-top:2px;"  onClick="Remove_BusinessAreas();UpdateBusinessIDList();" >
                                                            <br>
                                                            <INPUT name="txtSelectedBusinessList" type="hidden" id="txtSelectedBusinessList" size="8" value="" />
                                                        </td>
                                                        <td width="150" id="tdSelectedBusinessList" style="border:0px;padding-top:5px;">
                                                            <SELECT name="lstSelectedBusinessList" id="lstSelectedBusinessList" size="5" class="" style="width:180px;">

                                                            </SELECT>
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Job Location</td>
                                            <td></td>
                                            <td style="padding-top:2px; padding-bottom:2px; border:0px;">
                                                <SPAN class="BDJGrayArial11px">
                                                    <INPUT type="radio" name="optJobLocationType" id="optJobLocationType" value="-1" style="vertical-align:middle;" onClick="SetJobLocation(this.value);" checked="checked">
                                                    Anywhere in Bangladesh
                                                    <INPUT type="radio" name="optJobLocationType" id="optJobLocationType" value="0" style="vertical-align:middle;" onClick="SetJobLocation(this.value);" >
                                                    Within Bangladesh 
                                                    <INPUT type="radio" name="optJobLocationType" id="optJobLocationType" value="1" style="vertical-align:middle;" onClick="SetJobLocation(this.value);LoadCountry();" >
                                                    Outside Bangladesh
                                                </SPAN>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color:#575353; ">Do you want to display in Hot Jobs?</td>
                                            <td></td>
                                            <td style="padding-top:2px; padding-bottom:2px; border:0px;">
                                                <SPAN class="BDJGrayArial11px">
                                                    <INPUT type="radio" name="optHotJob" id="optHotJobYes" value="y" style="vertical-align:middle;" onClick="SetHotJob(this.value);">
                                                    yes
                                                    <INPUT type="radio" name="optHotJob" id="optHotJobNo" value="n" style="vertical-align:middle;" onClick="SetHotJob(this.value);" checked="checked" >
                                                    no 
                                                </SPAN>
                                                <input type="file" name="imgSrc" id="imgSrc" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table width="100" border="0" align="right" cellpadding="0" cellspacing="0">        
                                                    <tr>
                                                        <td align="center"  style="border:0px;height:20px;">
                                                            <SPAN id="spnDivision" class="BDJGrayArial11px" style="color:#999999">Division</SPAN>
                                                        </td>
                                                    </tr>
                                                    <!--DIVISION LIST:-->
                                                    <tr>
                                                        <td width="150" id="tdDivision" style="border:0px;">
                                                            <SELECT name="cboDivision" id="cboDivision" onChange="LoadDistrict(this.value);SetJobLocation_AnywhereInBangladesh(this.value);" disabled="disabled">
                                                                <option value="-1">Select</option>

                                                                <option value="Barisal" >Barisal</option>

                                                                <option value="Chittagong" >Chittagong</option>

                                                                <option value="Dhaka" >Dhaka</option>

                                                                <option value="Khulna" >Khulna</option>

                                                                <option value="Rajshahi" >Rajshahi</option>

                                                                <option value="Rangpur" >Rangpur</option>

                                                                <option value="Sylhet" >Sylhet</option>

                                                                <!--option value="-2">Anywhere</option-->						 
                                                            </SELECT>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td colspan="3">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">        
                                                    <tr>
                                                        <td align="center"  style="border:0px;height:20px;"><SPAN id="spnLocation" class="BDJGrayArial11px" style="color:#535A62">District</SPAN></td>
                                                        <td style="border:0px;height:20px;" align="center">&nbsp;</td>
                                                        <td align="center" style="border:0px;height:20px;"><SPAN id="spnSelectedLocation" class="BDJGrayArial11px" style="color:#535A62">Selected   District</SPAN></td>
                                                    </tr>

                                                    <tr>
                                                        <td width="150" id="tdLocation" style="border:0px;">
                                                            <SELECT name="lstLocation" id="lstLocation" size="5" class="form-control" style="width:180px;" scroling="no" >
                                                            </SELECT>
                                                        </td>

                                                        <td style="border:0px;" align="center">
                                                            <INPUT type="button" name="cmdAddLocation" id="cmdAddLocation" class="BDJButton2" style="margin-bottom:2px;" value="Add >" onClick="Add_Location();UpdateLocationIDList();" disabled="disabled">
                                                            <br>
                                                            <INPUT type="button" name="cmdRemoveLocation" id="cmdRemoveLocation" class="BDJButton2" style="margin-top:2px;" value="< Remove" onClick="Remove_Location();UpdateLocationIDList();" disabled="disabled">
                                                            <INPUT class="form-control" name="txtSelectedLocationList" type="hidden" id="txtSelectedLocationList" size="8" value="-1" />				
                                                        </td>
                                                        <td id="tdSelectedLocation" style="border:0px;" width="150">
                                                            <SELECT class="form-control" name="job_location" id="lstSelectedLocation" size="5" style="width:180px;">

                                                            </SELECT>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>  
                                        <tr>
                                            <td style="font-family: century gothic;font-size: 10pt;">Compensation and other benefits</td>
                                            <td></td>
                                            <td style="padding-top:5px; padding-bottom:2px; padding-left:5px; border:0px;font-family: century gothic; font-size: 10pt;">
                                                <SPAN class="BDJGrayArial11px">
                                                    <STRONG><U>Salary Range</U></STRONG>
                                                    <BR>
                                                    <INPUT type="radio" name="optBenefits" id="optNegotiable" value="1" style="vertical-align:middle;" onClick="SetSalaryRange();" />
                                                    Negotiable
                                                    <BR>
                                                    <INPUT type="radio" name="optBenefits" id="optNotMention" value="2" style="vertical-align:middle;" onClick="SetSalaryRange();" />
                                                    Don’t want to mention
                                                    <BR>
                                                    <INPUT type="radio" name="optBenefits" id="optSalaryRange" value="3" style="vertical-align:middle;" onClick="SetSalaryRange();" checked="checked"/>
                                                    Want to display the following range
                                                    <BR>
                                                </SPAN>	
                                                <SPAN id="spnMinimumSalary">Minimum&nbsp;</SPAN>
                                                <INPUT type="text" 
                                                       name="salary_min" 
                                                       id="txtSalaryFrom" 
                                                       class="form-control"
                                                       onKeyPress="return blockNonNumbers(this, event, false, false);"
                                                       onChange="return CheckNumber(this.id);"
                                                       maxlength="10" 				   
                                                       value="" 

                                                       style="width:20%;vertical-align:middle;"/>
                                                <SPAN id="spnMaximumSalary" >Maximum&nbsp;</SPAN>
                                                <INPUT type="text" 
                                                       name="salary_max" 
                                                       id="txtSalaryTo" 
                                                       onKeyPress="return blockNonNumbers(this, event, false, false);"
                                                       onChange="return CheckNumber(this.id);"
                                                       maxlength="10"
                                                       class="form-control" 				   
                                                       value="" 

                                                       style="width:20%;vertical-align:middle;" />
                                                <SPAN id="spnBDT">monthly in BDT</SPAN>
                                                <BR><BR>
                                                <SPAN class="BDJGrayArial11px">Other Benefits</SPAN>
                                                <TEXTAREA name="other_benefits" id="txtOtherBenefits" class="form-control" style="width:100%; height:80px;"></TEXTAREA>			
                                            </td>
                                        </tr>    
                                        <tr>
                                            <td colspan="2" align="center"><button class="btn btn-default btn-lg" type="submit"><i class="fa fa-wrench" ></i> Update </button></td>
                                        </tr>
                                    </table>

                                </form>		

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </body>
</html>