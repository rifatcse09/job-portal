<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?= $title; ?></title>
        <link rel="stylesheet" href="<?= base_url(); ?>css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui-1.8.18.custom.css" type="text/css" />
        <link href="<?= base_url(); ?>/css/bootstrap.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>/css/custom.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />


<link href="<?= base_url(); ?>/css/morris-0.4.3.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <style type="text/css">

        </style>
    </head>
    <body>
        <div id="container">
            <div id="employer_header">
            </div>
            <div id="employer_page">
                <div id="employer_menu">
                    <?= $this->load->view('employer_menu'); ?>	
                </div>
                <div id="main">
                    <div id="left_panel">
                        <h3 style="font-size:20px;">Corporate Panel</h3>
                        <div id="main_left"> 
                            <div id="main_left_header">
                                <h2>Main Menu</h2>
                            </div>

                            <div id="main_left_content">
                                <?= $this->load->view('employer_mainmenu'); ?>
                            </div>

                            <div id="acc_status">
                                <h2>Account Status</h2>  
                            </div>

                            <div id="acc_status_content">
                                <ul>
                                    <li><?php echo anchor('employer_jobposting_step01/', 'Job Posting', 'title="Post a New Job"') ?></li>
                                    <li><a href="#">CV Database</a></li>
                                </ul>
                            </div>

                            <div id="corporate">
                                <h2>Help Desk</h2>
                            </div>

                              <div id="corporate_content" >
                                <ul style="margin-right:5px">
                                  
                                    <li><i><b>PentaGems</b></i></li>
                                    <li><br><b><i>Address:</i></b>Singapore Market(5th Floor),<br>Haji Para, Agrabad, Ctg</li>
                                    <li>E-Mail:<br>info@purbodeshjobs.com</li>
                                    <li>Cell:01785878789</li>
                                </ul>
                            </div>
                        </div>

                    </div> 

                    <div id="right_panel">
                        <div id="right_panel_header">
                            <?= $this->load->view('employer_top'); ?>
                        </div>
                        <div id="main_box">
                            <div id="main_box_header">
                                <div id="main_box_totaljobs">
                                    <p><b>total vouchers: <?= isset($emp_acc_status) ? count($emp_acc_status) : 0;?> </b></p>
                                </div>
                                <div id="main_box_logout">
                                   
  <a class="btn btn-danger" style="margin-right:50px" href="../../">
                                     
                                <i class="fa fa-sign-out"> </i>Log Out</a>
                                </div>
                            </div>
                            <div id="rightside" class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                    <tr>
                                        <th scope="col">SL</th>
                                        <th scope="col">Invoice No.</th>
                                        <th scope="col">Invoice Date</th>
                                        <th scope="col">Invoice Purpose</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Pay Status</th>
                                    </tr>

                                    <?
                                    if (isset($emp_acc_status)) {
                                        foreach ($emp_acc_status as $k => $job) {
                                            ?>
                                            <tr>
                                                <td><?= $k + 1 ?></td>
                                                <td><a style="color:#000" href="<?= site_url()?>/empaccount/view_voucher?voucher_id=<?= $job['id']?>">
                                                    <?= $job['voucher_no'] ?></a></td>
                                                <td><?= date("d-M-y", strtotime($job['create_date'])) ?></td>
                                                <td>Online job posting</td>
                                                <td><?= $job['amount'] + $job['vat'] ?></td>
                                                <td>
                                                    <?
                                                        if($job['is_paid'] == 0){
                                                            echo 'not paid';
                                                        }else{
                                                            echo 'paid';
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?
                                        }
                                    }
                                    ?>
                                </table>
                                <div id="rightside_bottom">
                                   <!-- <div id="rightside_bottom_deletebox">
                                        <form>
                                            <input type="submit" value="Delete" />
                                        </form>
                                    </div>-->
                                </div> 
                            </div>
                            <div id="rightside_bottomenu">
                                <div id="rightside_bottomenu_post">
                                    
                                    
                                      <a class="btn btn-primary btn-sm" style="margin-right:50px;padding-right: 20px;color: #fff;" href="<?= site_url()?>/employer_jobposting_step01"><i class="fa fa-keyboard-o"></i> Post a New Job </a>
                                   
                                </div>
                                <div id="rightside_bottomenu_showDelete">
                                    
                                    <a class="btn btn-warning btn-sm" style="margin-right:50px" href="<?= site_url()?>/home/delete_show"><i class="fa fa-trash"></i> Show Deleted Jobs</a>
                                   
                                </div>
                                <div id="rightside_bottomenu_viewAll">
                                 
                                       
                                        <a class="btn btn-success btn-sm" style="margin-right:50px" href=""><i class="fa fa-eye"></i> View All Jobs</a>
                                    
                                    
                                </div>
                                <div id="rightside_bottomenu_next">
                                    
                                      <a class="btn btn-info btn-sm" style="margin-right:50px" href="" ><i class="fa fa-hand-o-right"></i>   Next </a>
                                  
                                </div>
                            </div>
                        </div> 
                    </div>


                </div> 

            </div>

        </div>
    </body>
</html>