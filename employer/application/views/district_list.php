<? 
    $size = isset($district_size) ? $district_size : 1; 
    $name = isset($district_name) ? $district_name : 'district_name';
    $id = isset($district_id) ? $district_id : 'district_id';
?>
<select class="form-control" id="<?=$id?>" style="width:180px;" size="<?=$size;?>" name="<?=$name?>" <?= isset($additional_att)? $additional_att : '' ?>>
<?php
    if($districts){
        if(isset($initial_value)) echo '<option value="" selected>' . $initial_value . '</option>';
        foreach ($districts as $district){
            echo '<option value="' . $district['District_Id'] . '">' . $district['District_Name'] . '</option>';
        }
    }
?>
</select>