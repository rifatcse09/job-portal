<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        
        <meta charset="utf-8">
        <title><?= $title; ?></title>
        <link rel="stylesheet" href="<?= base_url(); ?>css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui-1.8.18.custom.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui-1.8.18.custom.css" type="text/css" />
        <link href="<?= base_url(); ?>/css/bootstrap.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>/css/custom.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />


<link href="<?= base_url(); ?>/css/morris-0.4.3.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

        <style type="text/css">

        </style>
        <script type="text/javascript" src="<?= base_url(); ?>ajax/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>ajax/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#repost_dialog").dialog({
                    autoOpen: false,
                    modal: true,
                    width: "350",
                    height: "auto",
                    title: "Repost Job",
                    resizable: false,
                    position: ["center", "top"]
                });
                $("#repost_deadline").datepicker({ minDate: 0, dateFormat: "yy/mm/dd" })
            });
            function job_repost(job_id){
                $("#repost_dialog").dialog("open");
                $("#repost_job_id").val(job_id);
            }
        </script>
    </head>
    <body>
        <div id="container">
            <div id="employer_header">
            </div>
            <div id="employer_page">
                <div id="employer_menu">
                    <?= $this->load->view('employer_menu'); ?>	
                </div>
                <div id="main" style="background-color: #ECF2EF">
                    <div id="left_panel">
                        <h3 style="font-size:20px;">Corporate Panel</h3>
                        
                        <div id="main_left"> 
                            <div id="main_left_header">
                                <h2>Main Menu</h2>
                            </div>

                            <div id="main_left_content">
                                <?= $this->load->view('employer_mainmenu'); ?>
                            </div>

                            <div id="acc_status">
                                <h2>Account Status</h2>  
                            </div>

                            <div id="acc_status_content">
                                <ul>
                                    <li><a href="<?= site_url() ?>/cont_employee_list/search_cv">CV Database</a></li>
                                </ul>
                            </div>

                            <div id="corporate">
                                <h2>Help Desk</h2>
                            </div>

                            <div id="corporate_content" >
                                <ul style="margin-right:5px;">
                                    
                                    <li><i><b>PentaGems</b></i></li>
                                    
                                    <li><br><i>Address:</i>Singapore Market(5th Floor)<br>Haji Para, Agrabad, Ctg</li>
                                    
                                    <li><br>Email:<br>robindulal2004@yahoo.com</li>
                                    
                                    <li>Cell:01785878789</li>
                                </ul>
                            </div>

                        </div>

                    </div> 

                    <div id="right_panel">
                        <div id="right_panel_header">
                            <?= $this->load->view('employer_top'); ?>
                        </div>
                        <div id="main_box">
                            <div id="main_box_header">
                                <div id="main_box_totaljobs">
                                    <p><b>Total Posted Jobs : </b></p>
                                </div>
                                <div id="main_box_logout">
  <a class="btn btn-danger" style="margin-right:50px" href="../../">
                                     
                                <i class="fa fa-sign-out"> </i>Log Out</a>
                                    
                                </div>
                            </div>
                             <div id="rightside" class="table-responsive">
                                <form action="<?= site_url() ?>/home/delete_job" method="post">
                                    <table class="table table-striped table-bordered table-hover" style="margin-top:50px">
                                       <thead>
                                        <tr>
                                            <th scope="col">SL</th>
                                            <th scope="col">Job Title</th>
                                            <th scope="col">Post Date</th>
                                            <th scope="col">Deadline</th>
                                            <th scope="col">Repost</th>
                                            <th scope="col">Edit</th>
                                            <th scope="col">Applied</th>
                                            <th scope="col">Viewed</th>
                                            <th scope="col">Not Viewed</th>
<!--                                            <th scope="col">Shortlisted	</th>-->
                                            <th></th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <?
                                        if (isset($jobs)) {
                                            foreach ($jobs as $k => $job) {
                                                ?>
                                        <tbody>
                                                <tr>
                                                    <td style="text-align:center "><?= $k + 1 ?></td>
                                                    <td style="text-align:center "><?= $job['job_title'] ?></td>
                                                    <td style="text-align:center "><?= date("d-M-y", strtotime($job['posting_date'])) ?></td>
                                                    <td style="text-align:center "><?= date("d-M-y", strtotime($job['deadline'])) ?></td>
                                                    <td style="text-align:center "><a style="color:#000" href="javascript:job_repost(<?= $job['emp_job_id'] ?>)">Repost</a></td>
                                                    <td style="text-align:center "><a style="color:#000" href="<?= site_url() ?>/home/edit_jobposting?job_id=<?= $job['emp_job_id'] ?>">Edit</a></td>
                                                    <td style="text-align:center "><?= isset($job['applied']) ? '<a style="color:#000" href="' . site_url() . '/cont_employee_list/view_applied_employees?job_id=' . $job['emp_job_id'] . '">' . $job['applied'] . '</a>' : 0 ?></td>
                                                    <td style="text-align:center "><?= isset($job['viewed']) ? $job['viewed'] : 0 ?></td>
                                                    <td style="text-align:center "><?= isset($job['not viewed']) ? $job['not viewed'] : 0 ?></td>
        <!--                                                    <td>0</td>-->
                                                    <td></td>
                                                    <td style="text-align:center "><input name="job_ids[]" type = "checkbox" value="<?= $job['emp_job_id'] ?>" /></td>
                                                </tr>
                                                </tbody>
                                                <?
                                            }
                                        }
                                        ?>
                                    </table>
                                    <div id="rightside_bottom_deletebox" style="margin-right: 20px;margin-bottom: 5px;">
                                        <!--<input type="submit" class="btn btn-danger" <i class="fa fa-pencil"></i> value="Delete"/>       --> 
                                        <button class="btn btn-danger" type="submit"><i class="fa fa-pencil"></i> Delete</button>
                                    </div>
                                </form>
                                <div id="page">
                                    <? echo isset($links) ? $links : 'e'; ?>
                                </div>
                                <div id="rightside_bottom">





                                </div>

                            </div>
                            <div id="rightside_bottomenu">
                                <div id="rightside_bottomenu_post">
                                    
                                    
                                      <a class="btn btn-primary btn-sm" style="margin-right:50px;padding-right: 20px;color: #fff;" href="<?= site_url()?>/employer_jobposting_step01"><i class="fa fa-keyboard-o"></i> Post a New Job </a>
                                   
                                </div>
                                <div id="rightside_bottomenu_showDelete">
                                    
                                    <a class="btn btn-warning btn-sm" style="margin-right:50px" href="<?= site_url()?>/home/delete_show"><i class="fa fa-trash"></i> Show Deleted Jobs</a>
                                   
                                </div>
                                <div id="rightside_bottomenu_viewAll">
                                 
                                       
                                        <a class="btn btn-success btn-sm" style="margin-right:50px" href=""><i class="fa fa-eye"></i> View All Jobs</a>
                                    
                                    
                                </div>
                                <div id="rightside_bottomenu_next">
<!--                                <div id="rightside_bottomenu_next">
                                    
                                        <a class="btn btn-info btn-sm" style="margin-right:50px" href="" ><i class="fa fa-hand-o-right"></i> Next </a>
                                 
                                </div>-->
                                </div>
                            </div>
                        </div> 
                    </div>



                </div> 

            </div>

        </div>
        <div id="repost_dialog">
            <form id="repost_form" method="post" action="<?= site_url()?>/employer_jobposting_step01/job_repost">
                <input type="hidden" name="repost_job_id" id="repost_job_id" />
                Deadline: <input type="text" name="repost_deadline" id="repost_deadline" readonly /><br />
                <input type="submit" value="Repost" />
            </form>
        </div>
    </body>
</html>