<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?= $title; ?></title>
        <link rel="stylesheet" href="<?= base_url(); ?>css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui-1.8.18.custom.css" type="text/css" />
        <link href="<?= base_url(); ?>/css/bootstrap.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>/css/custom.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />


<link href="<?= base_url(); ?>/css/morris-0.4.3.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <script language="javascript" src="<?= base_url(); ?>js/JobPosting.js"></script>
        <script language="javascript" type="text/javascript">
            <!--
            function PopItUp(url) 
            {
                //document.getElementById('txtDesignation').value = "";
		
                newwindow = window.open(url, 'name', 'height=250,width=450');
                if (window.focus) 
                {
                    newwindow.focus()
                }
                return false;
            }
            
            function checkBlueWorker(){
                var blue_job = document.getElementById('optBlueJobYes');
                if(blue_job.checked == false){
                    return IsPostedJobValid_Step01('JobInsert')
                }
                else{
                    var job_title = document.getElementById('txtJobTitle');
                    if(job_title.value == ""){
                        alert('Please fill up job title');
                        job_title.focus();
                        return false;
                    }
                    var dateDay = document.getElementById('cboDay');
                    if(dateDay.value == "" || dateDay.value == '-1'){
                        alert('Please select a valid date for application deadline');
                        dateDay.focus();
                        return false;
                    }
                    var dateMon = document.getElementById('cboMonth');
                    if(dateMon.value == "" || dateMon.value == '-1'){
                        alert('Please select a valid date for application deadline');
                        dateMon.focus();
                        return false;
                    }
                    var cboYear = document.getElementById('cboYear');
                    if(cboYear.value == "" || cboYear.value == '-1'){
                        alert('Please select a valid date for application deadline');
                        cboYear.focus();
                        return false;
                    }
                    
                    var bill_contact = document.getElementById('txtBillingContact');
                    if(bill_contact.value == ""){
                        alert('Please select a valid contact.');
                        bill_contact.focus();
                        return false;
                    }
                     var email_contact = document.getElementById('txtAppliedEmail');
                    if(email_contact.value == ""){
                alert(email_contact.value);        
                alert('Please insert a email to recieve CV');
                        email_contact.focus();
                        return false;
                    }
                    return true;
                }
            }
	
            function SetDesignation(strData)
            {
                var arrData = strData.split("_");
		
                var intContactId = arrData[0];
                var strDesignation = arrData[2];
		
                //alert(arrData[0]);
                //document.getElementById('divMyData').innerHTML = "<img src='../images/indicator.gif' />";
		
                if(intContactId != "") 
                {
                    document.getElementById('txtDesignation').value = strDesignation;
                }
                else
                {
                    document.getElementById('txtDesignation').value = "";
                }
                /**/
            }
            //-->
        </script>

        <script language="JavaScript" src="<?= base_url(); ?>ajax/ajax.js"> </script>

    </head>
    <body>

        <div id="container">
            <div id="employer_header">
            </div>

            <div id="employer_page">
                <div id="employer_menu">
                    <?= $this->load->view('employer_menu'); ?>	
                </div>

                <div id="main">
                    <div id="left_panel">
                        <h3>Corporate Panel</h3>
                        <div id="main_left"> 
                            <div id="main_left_header">
                                <h2>Main Menu</h2>
                            </div>

                            <div id="main_left_content">
                                <?= $this->load->view('employer_mainmenu'); ?>
                            </div>

                            <div id="acc_status">
                                <h2>Account Status</h2>  
                            </div>

                            <div id="acc_status_content">
                                <ul>
                                    <li><a href="<?= site_url() ?>/cont_employee_list/search_cv">CV Database</a></li>
                                </ul>
                            </div>

                            <div id="corporate">
                                <h3>Help Desk</h3>
                            </div>

                             <div id="corporate_content" >
                                <ul style="margin-right:5px">
                                    <li><b>Help Desk :</b></li>
                                    <li><b><i>PentaGems</i></b></li>
                                    <li><br><b>Address:</b>Singapore Market(5th Floor),<br>Haji Para, Agrabad, Ctg</li>
                                    <li><br>E-Mail:<br>info@purbodeshjobs.com</li>
                                    <li>mobile: 01785878789</li>
                                </ul>
                            </div>

                        </div>
                    </div>

                    <div id="right_panel">
                        <div id="right_panel_header">
                            <?= $this->load->view('employer_top'); ?>
                        </div>

                        <div id="main_box">
                            <div id="main_box_header">
                                <div id="main_box_logout">
                                   
  <a class="btn btn-danger" style="margin-right:50px" href="<?= site_url() ?>/home/logout">
                                     
                                <i class="fa fa-sign-out"> </i>Log Out</a>
                                </div>

                            </div>

                            <div id="rightside" class="table-responsive">
                                <form action="<?= site_url() ?>/employer_jobposting_step01/jobposting_step_02" onSubmit="return checkBlueWorker();" method="post">
                                    <table width="1000px" class="table table-striped table-bordered table-hover" style="margin-top:50px">
                                        <tr><td colspan="3" style="border-bottom:1px solid #000; margin:0; color:#a5090a; font-size:20px; font-family: century gothic;font-weight:bold;">Job Requirements [ Step 1 of 2 ]</td></tr>
                                        <tr>
                                            <td style="color:#575353; ">Is this Blue Color Job?</td>
                                            <td></td>
                                            <td style="padding-top:2px; padding-bottom:2px; border:0px;">
                                                <SPAN class="BDJGrayArial11px">
                                                    <INPUT type="radio" name="optBlueJob" id="optBlueJobYes" value="y" style="vertical-align:middle;" />
                                                    yes
                                                    <INPUT type="radio" checked="checked"  name="optBlueJob" id="optBlueJobNo" value="n" style="vertical-align:middle;" />
                                                    no 
                                                </SPAN>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color:#575353; font-family: century gothic;font-size: 10pt;">Job Category *</td> 
                                            <td></td> 
                                            <td>
                                                <? $this->load->view('category_list', $categories); ?>

                                            </td> 
                                        </tr>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                        <tr>
                                            <td style="color:#575353;font-family: century gothic;font-size: 10pt;">Job Title *</td>
                                            <td width="10"></td>
                                            <td><input type="text" name="job_title" id="txtJobTitle" size="70" class="form-control" /></td>
                                        </tr>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                        <tr>
                                            <td style="color:#575353;font-family: century gothic;font-size: 10pt;">No. of Vacancies *</td>
                                            <td></td>
                                            <td><input type="text" name="no_vacancy" id="txtTotalVacancy" size="10" class="form-control"></td>
                                        </tr>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                        <tr><td><p style="color:#575353;">How do you want to receive CV/Resume(s) ?</p></td>
                                            <td></td> 
                                            <td><p style="color:#a5090a;">(User must select at least one from the given options)</p></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #575353"><input type="checkbox" name="online_cv" id="chkOnlineCV" value="true">Online CV/Resume</td>
                                            <td></td>
                                            <td><p style="color:#575353">[Azadijobs Jobs CV format; CV will be available in your corporate inbox.]</p></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #575353"><input checked="checked" type="hidden" name="email_attach" value="true" id="chkEmailAttachmentCV" onClick="SetEmailAttachmentCV(); SetApplyInstruction();" /> E-mail Attachment</td>
                                            <td></td>
                                            <td><p style="color:#575353">[Applicants can send their CV as an attachment in your given e-mail address.]</p></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td style="color: #575353">
                                                <input name="corporate_email" type="text" class="form-control" id="txtAppliedEmail" style="width:40%; background-color:#F7F7F7"    value="" maxlength="80"  />

                                                <input  type="checkbox" id="chkCorporateEmail" value="true" style=""  onClick="ShowCorporateEmail(); ">Use Corporate E-Mail
                                                <INPUT type="hidden" name="email_id"  id="txtCorporateEmail" size="10" value="<?= $corporate_email ?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color: #575353"><input type="checkbox" name="hard_copy" id="chkHardCopyCV" value="true" style="vertical-align:middle;"  onClick="SetApplyInstruction();SetHideCompanyAddress();">Hard Copy CV</td>
                                            <td></td>
                                            <td><p style="color:#575353">[Applicants can send hard copy of their CV in your corporate address.]</p></td>
                                        </tr>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                        <tr>
                                            <td><p style="color:#575353;margin:0;">Applicant should enclose Photograph with CV ?</p></td>
                                            <td></td>
                                            <td><input type="radio" name="photogrph_cv" id="optPhotograph" value="yes">Yes<input type="radio" name="photogrph_cv" id="optPhotograph" value="no" style="vertical-align:middle;" checked="checked">No</td>
                                        </tr>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                        <tr>
                                            <td style="color:#575353; font-family: century gothic;font-size: 10pt;"><span id="spnApplyInstruction" style="color:#999999">
                                                    Apply Instruction(s)
                                                </span></td>
                                            <td></td>
                                            <td><textarea name="apply_instruction" id="txtApplyInstruction" class="form-control" style="width:100%; height:80px; background-color:#F7F7F7;" disabled="disabled"></textarea>
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                        <tr>
                                            <td style="border-bottom:1px solid #CCCCCC; margin:0; color:#575353; font-family: century gothic;font-size: 10pt;">Application Deadline *</td>
                                            <td style="border-bottom:1px solid #CCCCCC; margin:0; color:#006699;"></td>
                                            <td style="border-bottom:1px solid #CCCCCC;"><SELECT name="cboDay" id="cboDay" style="vertical-align:middle;float: left" class="form-control"> 
                                                    <option value="-1" selected>dd</option>

                                                    <option value="1" >1</option>					      

                                                    <option value="2" >2</option>					      

                                                    <option value="3" >3</option>					      

                                                    <option value="4" >4</option>					      

                                                    <option value="5" >5</option>					      

                                                    <option value="6" >6</option>					      

                                                    <option value="7" >7</option>					      

                                                    <option value="8" >8</option>					      

                                                    <option value="9" >9</option>					      

                                                    <option value="10" >10</option>					      

                                                    <option value="11" >11</option>					      

                                                    <option value="12" >12</option>					      

                                                    <option value="13" >13</option>					      

                                                    <option value="14" >14</option>					      

                                                    <option value="15" >15</option>					      

                                                    <option value="16" >16</option>					      

                                                    <option value="17" >17</option>					      

                                                    <option value="18" >18</option>					      

                                                    <option value="19" >19</option>					      

                                                    <option value="20" >20</option>					      

                                                    <option value="21" >21</option>					      

                                                    <option value="22" >22</option>					      

                                                    <option value="23" >23</option>					      

                                                    <option value="24" >24</option>					      

                                                    <option value="25" >25</option>					      

                                                    <option value="26" >26</option>					      

                                                    <option value="27" >27</option>					      

                                                    <option value="28" >28</option>					      

                                                    <option value="29" >29</option>					      

                                                    <option value="30" >30</option>					      

                                                    <option value="31" >31</option>					      

                                                </SELECT>
                                                <SELECT name="cboMonth" id="cboMonth" style="vertical-align:middle; float: left" class="form-control">
                                                    <option value="-1" selected>mm</option>
                                                    <option value="01" >Jan</option>
                                                    <option value="02" >Feb</option>
                                                    <option value="03" >Mar</option>
                                                    <option value="04" >Apr</option>
                                                    <option value="05" >May</option>
                                                    <option value="06" >Jun</option>
                                                    <option value="07" >Jul</option>
                                                    <option value="08" >Aug</option>
                                                    <option value="09" >Sep</option>
                                                    <option value="10" >Oct</option>
                                                    <option value="11"  >Nov</option>
                                                    <option value="12" >Dec</option>
                                                </SELECT>

                                                <SELECT name="cboYear" id="cboYear" style="vertical-align:middle; float: left;" class="form-control">
                                                    <option value="-1">yyyy</option>
                                                    <option value="2012" >2012</option>
                                                    <option value="2013" >2013</option>
                                                    <option value="2014" >2014</option>
                                                    <option value="2015" >2015</option>
                                                    <option value="2016" >2016</option>
                                                    <option value="2017" >2017</option>
                                                    <option value="2018" >2018</option>
                                                    <option value="2019" >2019</option>
                                                    <option value="2020" >2020</option>
                                                </SELECT>
                                                &nbsp;
                                                <SPAN class="BDJRedArial11px">

                                                    <span style="color:#a5090a;"> [Date Format : day-month-year; i.e. 16-Dec-1971]</span>

                                                </SPAN></td>
                                        </tr>
                                        <tr>
                                            <td style="color:#575353; font-family: century gothic;font-size: 10pt;">Billing Contact *</td>
                                            <td></td>
                                            <td>
                                                <select name="billing_contact" id="txtBillingContact" onChange="SetDesignation(this.value)" class="form-control">
                                                    <option value="">Select </option>
                                                    <option value='<?= $designation_value ?>'><?= $person_name ?></option>
                                                </select>&nbsp;&nbsp;
                             <!--                   <a href="#" style="text-decoration:none" onClick="return Refresh('',1);">Refresh</a> | <a href="#" style="text-decoration:none" onClick="return PopItUp('<?= site_url() ?>/manager?BillingContact=1');">Manage</a>-->
                                            </td>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                            <td style="color:#575353; font-family: century gothic;font-size: 10pt;">Designation *</td>
                                            <td></td>
                                            <td>
                             <input type="text" name="designation" id="txtDesignation" value="" readonly="true" class="form-control" />&nbsp;&nbsp;&nbsp;<!--<a href="#" onClick="return PopItUp('<?= site_url() ?>/manager?BillingContact=1');" class="BDJGreenArial11px">Edit</a></span>-->
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                        <tr>
                                            <td style="color:#575353; font-family: century gothic;font-size: 10pt;">In this job do you want to display</td>
                                            <td></td>
                                            <td><input type="radio" name="job_display" id="optCompanyName" value="yes" style="vertical-align:middle; padding-left: 2px" checked="checked"  onClick="SetAliasName();"> Company Name <input type="radio" name="job_display" id="optOtherName" value="no" style="vertical-align:middle;"  onClick="SetAliasName();">Other</td>
                                        </tr>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                        <tr>
                                            <td><span id="spnAliasName" style="color:#575353; font-family: century gothic;font-size: 10pt;">
                                                    Enter text to be displayed as Company Name		  	  
                                                </span></td>
                                            <td></td>
                                            <td><input class="form-control" type="text" name="company_name" id="txtAliasName" style="width:40%; vertical-align:middle; background-color:#F7F7F7;" value="" maxlength="70" disabled="disabled" />
                                                <input type="hidden" name="txtAliasName2" id="txtAliasName2" size="10" value="<?= $alt_company_name ?>" />
                                                <script language="javascript">
                                                    if(document.getElementById("optCompanyName").checked == true)
                                                    {
                                                        document.getElementById("spnAliasName").style.color = "#999999";
                                                        document.getElementById("txtAliasName").value = "";
                                                        document.getElementById("txtAliasName").disabled = true;
                                                    }
					
                                                    if(document.getElementById("optOtherName").checked == true)
                                                    {
                                                        document.getElementById("spnAliasName").style.color = "#226EC7";
                                                        document.getElementById("txtAliasName").disabled = false;
                                                        //document.getElementById("txtAliasName").value = document.getElementById("txtAliasName2").value;
                                                        document.getElementById("txtAliasName").focus();
                                                    }					
                                                </script>
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr><td colspan="3" style="border-bottom:1px solid #CCCCCC;"></tr>
                                        <tr>
                                        <tr>
                                            <td style="color:#575353;">Do you want to hide Company Address ?</td>
                                            <td></td>
                                            <td><input type="radio" name="hide_address" value="yes" id="optionsRadios1" style="padding-right: 2px" /> Yes <input id="optionsRadios1" type="radio" name="hide_address"  value="no" checked />No</td>
                                        </tr>
                                        <tr>
                                            <th colspan="3"><p style="color:#a5090a;">N.B. Alternative Name like, A Leading Manufacturing Company or A Leading Multinational Company etc.</p></th>
                                        </tr>
                                        <tr>
                                            <th colspan="3">
                                                <button class="btn btn-default btn-lg" type="submit"><i class="fa fa-hand-o-right"></i> Next </button>
                                                
                                                    </th>
                                        </tr>
                                    </table>

                                </form>

                            </div>

                        </div>

                    </div>  


                </div>

            </div>
        </div>
    </body>
</html>