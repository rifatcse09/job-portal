<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>deleted jobs</title>
          <link rel="stylesheet" href="<?= base_url(); ?>css/style.css" type="text/css" />
          <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui-1.8.18.custom.css" type="text/css" />
        <link href="<?= base_url(); ?>/css/bootstrap.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>/css/custom.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />


<link href="<?= base_url(); ?>/css/morris-0.4.3.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

        
        <style type="text/css">

        </style>
        
    </head>
    <body>
        <div id="container">
            <div id="employer_header">
            </div>
               <div id="employer_page">
                <div id="employer_menu">
                    <?= $this->load->view('employer_menu'); ?>	
                </div>
                <div id="main">
                    <div id="left_panel">
                        <h3 style="font-size:20px;">Corporate Panel</h3>
                        <div id="main_left"> 
                            <div id="main_left_header">
                                <h2>Main Menu</h2>
                            </div>

                            <div id="main_left_content">
                                <?= $this->load->view('employer_mainmenu'); ?>
                            </div>

                            <div id="acc_status">
                                <h2>Account Status</h2>  
                            </div>

                            <div id="acc_status_content">
                                <ul>
                                    <li><a href="<?= site_url() ?>/cont_employee_list/search_cv">CV Database</a></li>
                                </ul>
                            </div>

                            <div id="corporate">
                                <h2>Help Desk</h2>
                            </div>

                            <div id="corporate_content">
                                <ul>
                                    
                                    <li>Phone</li>
                                    <li>Phone</li>
                                    <li>Please call us if you facing any difficulties during job posting or plan for subscribing other services.</li>
                                </ul>
                            </div>

                        </div>

                    </div> 

                    <div id="right_panel">
                        <div id="right_panel_header">
                            <?= $this->load->view('employer_top'); ?>
                        </div>
                        <div id="main_box">
                            <div id="main_box_header">
                                <div id="main_box_totaljobs">
                                    <p><b>Deleted Jobs : </b></p>
                                </div>
                                <div id="main_box_logout">
                                 
  <a class="btn btn-danger" style="margin-right:50px" href="<?= site_url() ?>/home/logout">
                                     
                                <i class="fa fa-sign-out"> </i>Log Out</a>
                                </div>
                            </div>
                            <div id="rightside" class="table-responsive">
<!--                                <form action="<?= site_url() ?>/home/delete_job" method="post">-->
                                    <table class="table table-striped table-bordered table-hover" style="margin-top:50px">
                                        <tr>
                                            <th scope="col">SL</th>
                                            <th scope="col" width="200">Job Title</th>
                                            <th scope="col">Post Date</th>
                                            <th scope="col">Deadline</th>
<!--                                            <th scope="col">Repost</th>
                                            <th scope="col">Edit</th>-->
<!--                                            <th scope="col">Applied</th>
                                            <th scope="col">Viewed</th>
                                            <th scope="col">Not Viewed</th>
                                            <th scope="col">Shortlisted	</th>-->
                                            <th></th>
<!--                                            <th scope="col">Action</th>-->
                                        </tr>

                                        <?
                                        if (isset($delete_job)) {
                                            foreach ($delete_job as $k => $job) {
                                                ?>
                                                <tr>
                                                    <td style="text-align:center "><?= $k + 1 ?></td>
                                                    <td style="text-align:center "><?= $job['job_title'] ?></td>
                                                    <td style="text-align:center "><?= date("d-M-y", strtotime($job['posting_date'])) ?></td>
                                                    <td style="text-align:center "><?= date("d-M-y", strtotime($job['deadline'])) ?></td>
<!--                                                    <td style="text-align:center "><a href="javascript:job_repost(<?//= $job['emp_job_id'] ?>)">Repost</a></td>
                                                    <td style="text-align:center "><a href="<?//= site_url() ?>/home/edit_jobposting?job_id=<?//= $job['emp_job_id'] ?>">Edit</a></td>-->
                                                   
                                                   
        <!--                                                    <td>0</td>-->
                                                    <td></td>
                                           
                                                </tr>
                                                <?
                                            }
                                        }
                                        ?>
                                    </table>
                                   
                                                  <div id="page"> <? echo isset($links) ? $links : ''; ?>
                                </div>
                                <div id="rightside_bottom">





                                </div>

                            </div>
                            <div id="rightside_bottomenu">
                                <div id="rightside_bottomenu_post">
                                    <button class="btn btn-primary btn-sm" style="margin-right:50px;padding-right: 20px;color: #fff;">
                                    <i class="fa fa-keyboard-o"></i>
                                      <a href="<?= site_url()?>/employer_jobposting_step01">Post a New Job </a>
                                    </button>
                                </div>
                                <div id="rightside_bottomenu_showDelete">
                                    <button class="btn btn-warning btn-sm" style="margin-right:50px">
                                    <i class="fa fa-trash"></i><a href="<?= site_url()?>/home/delete_show">Show Deleted Jobs</a>
                                    </button>
                                </div>
                                <div id="rightside_bottomenu_viewAll">
                                  <button class="btn btn-success btn-sm" style="margin-right:50px">
                                       
                                        <i class="fa fa-eye"></i><a href="">View All Jobs</a>
                                    </button>
                                    
                                </div>
                                <div id="rightside_bottomenu_next">
<!--                                <div id="rightside_bottomenu_next">
                                    <button class="btn btn-info btn-sm" style="margin-right:50px">
                                    <i class="fa fa-hand-o-right"></i>    <a href="" >Next </a>
                                    </button>
                                </div>-->
                                </div>
                            </div>
                        </div> 
                    </div>


                </div> 

            </div>
           
                </div>       
        
    </body>
    
    
</html>