<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Password Change</title>
        <link rel="stylesheet" href="<?= base_url(); ?>css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui-1.8.18.custom.css" type="text/css" />
        <link href="<?= base_url(); ?>/css/bootstrap.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>/css/custom.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />


<link href="<?= base_url(); ?>/css/morris-0.4.3.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

        <style type="text/css">

        </style>
        <script type="text/javascript">
            function verify_input(){
                var form1 = document.getElementById('form1');
                if(document.getElementById('account_username'))
                {
                    var USER=document.getElementById('account_username').value;
                    var PASS1=document.getElementById('account_password').value;
                    var PASS2=document.getElementById('account_retype_password').value;
                    //--------------------------------------------User Name Validation--------------------------------------------------------------------------------
                    if (USER=="")
                    {
                        alert('Please enter user name');
                        form1.account_username.focus();
                        return false;
                    }
                    if (USER.length < 5 )
                    {
                        alert('User name must be at least 5 characters!');
                        form1.account_username.focus();
                        return false;
                    }
                    //dv.indexOf(';', 0) >= 0
                    if (USER.indexOf('/', 0) >= 0 )
                    {
                        alert('User name does not allow /');
                        form1.account_username.focus();
                        return false;
                    }
                    // If does not work properly
                    if (USER.indexOf("\\", 0) >= 0 )
                    {
                        alert("User name does not allow '\\'");
                        form1.account_username.focus();
                        return false;
                    }
                    if (USER.indexOf(';', 0) >= 0 )
                    {
                        alert('User name does not allow ;');
                        form1.account_username.focus();
                        return false;
                    }
                    if (USER.indexOf(':', 0) >= 0 )
                    {
                        alert('User name does not allow :');
                        form1.account_username.focus();
                        return false;
                    }
                    if (USER.indexOf('&', 0) >= 0 )
                    {
                        alert('User name does not allow & ');
                        form1.account_username.focus();
                        return false;
                    }
                    if (USER.indexOf('"', 0) >= 0 )
                    {
                        alert('User name does not allow "');
                        form1.account_username.focus();
                        return false;
                    }
                    if (USER.indexOf("'", 0) >= 0 )
                    {
                        alert("User name does not allow '");
                        form1.account_username.focus();
                        return false;
                    }
                    if (USER.indexOf(' ', 0) >= 0 )
                    {
                        alert('User Name should be only one word! No space allowed.');
                        form1.account_username.focus();
                        return false;
                    }
                    //-------------------------------------------Password Validation-------------------------------------------------------------------------------------------
                    if(PASS1=="")
                    {
                        alert('Please enter your password');
                        form1.account_password.focus();
                        return false;
                    }
                    if (PASS1.indexOf(' ', 0) >= 0 )
                    {
                        alert('Password should be only one word!');
                        form1.account_password.focus();
                        return false;
                    }
                    if (PASS1.indexOf("'", 0) >= 0 )
                    {
                        alert("Password field does not allow ' ");
                        form1.account_password.focus();
                        return false;
                    }
                    if (PASS1.indexOf('"', 0) >= 0 )
                    {
                        alert('Password field does not allow "');
                        form1.account_password.focus();
                        return false;
                    }
                    if (PASS1.indexOf(';', 0) >= 0 )
                    {
                        alert('Password field does not allow ;');
                        form1.account_password.focus();
                        return false;
                    }
                    if (PASS1.indexOf('&', 0) >= 0 )
                    {
                        alert('Password field does not allow &');
                        form1.account_password.focus();
                        return false;
                    }
                    if (PASS1.length<8)
                    {
                        alert(' Password must be 8 to 12 characters long .');
                        form1.account_password.focus();
                        return false;
                    }
                    if(PASS2=="")
                    {
                        alert('Please re-enter your password.');
                        form1.account_retype_password.focus();
                        return false;
                    }
                    if(PASS1!=PASS2)
                    {
                        alert('Please be sure of your password \n Enter same password in the fields');
                        form1.account_retype_password.focus();
                        return false;
                    }
                    //-------------------------------End Password Validation----------------------------------------------------------------------------------
                }
            }
        </script>
        <style type="text/css">
            .form-table
            {
                width: 400px;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <div id="employer_header">
            </div>
            <div id="employer_page">
                <div id="employer_menu">
                    <?= $this->load->view('employer_menu'); ?>	
                </div>
                <div id="main">
                    <div id="left_panel">
                        <h3>Corporate Panel</h3>
                        <div id="main_left"> 
                            <div id="main_left_header">
                                <h2>Main Menu</h2>
                            </div>

                            <div id="main_left_content">
                                <?= $this->load->view('employer_mainmenu'); ?>
                            </div>

                            <div id="acc_status">
                                <h2>Account Status</h2>  
                            </div>

                            <div id="acc_status_content">
                                <ul>
                                    <li><a href="<?= site_url() ?>/cont_employee_list/search_cv">CV Database</a></li>
                                </ul>
                            </div>

                            <div id="corporate">
                                <h2>Help Desk</h2>
                            </div>

                         <div id="corporate_content" style="margin-right:5px;" >
                                <ul style="margin-right:5px">
                                    
                                    <li>PentaGems</li>
                                    <li style="text-align:justify">Address:Singapore Market(5th Floor),Haji Para, Agrabad, Ctg</li>
                                    <li>E-Mail: info@purbodeshjobs.com</li>
                                    <li>mobile: 01785878789</li>
                                </ul>
                            </div>

                        </div>

                    </div> 

                    <div id="right_panel">
                        <div id="right_panel_header">
                            <?= $this->load->view('employer_top'); ?>
                        </div>
                        <div id="main_box" style="height:400px">
                            <div id="main_box_header">
                                <div id="main_box_totaljobs">
                                    <p><b></b></p>
                                </div>
                                <div id="main_box_logout">
                                   
  <a class="btn btn-danger" style="margin-right:50px" href="../../">
                                     
                                <i class="fa fa-sign-out"> </i>Log Out</a>
                                </div>
                                <? $this->load->helper('form') ?>
                                <?= form_open('home/change_password_post', 'method="Post" name="form1" id="form1" onsubmit="return verify_input();"') ?>
                                
                                <br></br>
                                <table class="table table-striped table-bordered" style="margin-top:50px">
                                    <tr>
                                        <td class="right">User Name: </td>
                                        <td class="left"><input class="form-control" type="text" name="account_username" id="account_username" value="<?= $username ?>" />
                                            <span>*</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="right">Password: </td>
                                        <td class="left">
                                            <input class="form-control" type="password" name="account_password" id="account_password" />
                                            <span>*</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="right">Confirm Password: </td>
                                        <td class="left">
                                            <input class="form-control" type="password" name="account_retype_password" id="account_retype_password" />
                                            <span>*</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center"><button class="btn btn-default btn-lg" type="submit"><i class="fa fa-wrench" ></i> Update </button></td>
                                    </tr>
                                </table>
                                <?= form_close(); ?>
                            </div>
                        </div> 
                    </div>


                </div> 

            </div>

        </div>
    </body>
</html>