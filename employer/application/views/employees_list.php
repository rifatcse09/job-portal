
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?= $title; ?></title>
        <link rel="stylesheet" href="<?= base_url(); ?>css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui-1.8.18.custom.css" type="text/css" />
        <link href="<?= base_url(); ?>/css/bootstrap.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>/css/custom.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />


<link href="<?= base_url(); ?>/css/morris-0.4.3.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <style type="text/css">

        </style>
    </head>
    <body>
        <div id="container">
            <div id="employer_header">
            </div>
            <div id="employer_page">
                <div id="employer_menu">
                    <?= $this->load->view('employer_menu'); ?>	
                </div>
                <div id="main">

                    <div id="employer_content">
                        <div id="left_panel">
                            <h3 style="font-size:20px;">Corporate Panel</h3>
                            <div id="main_left"> 
                                <div id="main_left_header">
                                    <h2>Main Menu</h2>
                                </div>
                                <div id="main_left_content">
                                    <?= $this->load->view('employer_mainmenu'); ?>
                                </div>
                                <div id="acc_status">
                                    <h2>Account Status</h2>  
                                </div>
                                <div id="acc_status_content">
                                    <ul>
                                        <li><a href="<?= site_url()?>/cont_employee_list/search_cv">CV Database</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="right_panel">
                            <form method="Post" action="<?= site_url() ?>/cont_employee_list/search_cv" name="search_cv">
                                <div style="width: 100%; margin: 0 auto; border: 1px solid #0066FF; border-radius: 5px;">
                                    <h3 style="background-color: #001A33;padding-left: 10px;padding-top: 10px;padding-bottom: 10px; color: white;margin: 0px 0px auto 0px;">CV Search Panel</h3>

                                    <div style="margin: 10px;">
                                        <h4>Personal:</h4>
                                        <table border="0" style="padding:5px">
                                            <tr>
                                                <td>Name: </td>
                                                <td><input type="text" name="name" id="name"/></td>
                                                <td>Age range:</td>
                                                <td>
                                                    Min - 
                                                    <select name="min_age" id="min_age" style="margin-right: 19px;padding-right: 19px;">
                                                        <option value="" selected>Any</option>
                                                        <?
                                                        for ($i = 15; $i <= 65; $i++) {
                                                            echo '<option value="' . $i . '">' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                    Max - 
                                                    <select name="max_age" id="min_age" style="padding-right: 18px;">
                                                        <option value="" selected>Any</option>
                                                        <?
                                                        for ($i = 15; $i <= 70; $i++) {
                                                            echo '<option value="' . $i . '">' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Current Location: </td>
                                                <td><? $this->load->view('district_list', $district) ?></td>
                                                <td>Expected Salary Range: </td>
                                                <td>
                                                    Min - 
                                                    <select name="min_salary" style="margin-right: 19px;">
                                                        <option value="" selected>Any</option>
                                                        <?
                                                        for ($i = 3000; $i <= 250000;) {
                                                            if ($i < 25000) {
                                                                ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                                <?
                                                                $i = $i + 1000;
                                                            } else if ($i >= 25000 && $i < 50000) {
                                                                ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                                <?
                                                                $i = $i + 5000;
                                                            } else {
                                                                ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                                <?
                                                                $i = $i + 10000;
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    Max - 
                                                    <select name="max_salary">
                                                        <option value="">Any</option>
                                                        <?
                                                        for ($i = 4000; $i <= 250000;) {
                                                            if ($i < 25000) {
                                                                ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                                <?
                                                                $i = $i + 1000;
                                                            } else if ($i >= 25000 && $i < 50000) {
                                                                ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                                <?
                                                                $i = $i + 5000;
                                                            } else {
                                                                ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                                <?
                                                                $i = $i + 10000;
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Gender: </td>
                                                <td>
                                                    <input type="radio" name="gender" value="" checked/> Any
                                                    <input type="radio" name="gender" value="m"/> Male
                                                    <input type="radio" name="gender" value="f"/> Female
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div style="margin: 10px;">
                                        <h4>Academic:</h4>
                                        <table border="0">
                                            <tr>
                                                <td>Level of Degree: </td>
                                                <td>
                                                    <select name="edu_level">
                                                        <option value="" selected>Any</option>
                                                        <option value="1">Secondary</option>
                                                        <option value="2">Higher Secondary</option>
                                                        <option value="3">Diploma</option>
                                                        <option value="4">Bachelor/Honors</option>
                                                        <option value="5">Masters</option>
                                                        <option value="6">Doctoral</option>
                                                    </select>
                                                </td>
                                                <td>Result</td>
                                                <td><input type="text" name="result"/></td>
                                            </tr>
                                            <tr>
                                                <td>Course/Degree: </td>
                                                <td><input type="text" name="degree_title"/></td>
                                                <td>Institute: </td>
                                                <td><input type="text" name="institute"/></td>
                                            </tr>
                                            <tr>
                                                <td>Major: </td>
                                                <td><input type="text" name="major"/></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="margin: 10px;">
                                        <h4>Experience:</h4>
                                        <span>
                                            Experience in selected type of business area: 
                                            <input type="text" name="experience_area" />
                                        </span>
                                    </div>
                                    <div style="text-align: center">
                                        <input type="submit" name="search_submit" value="Search job seekers"/>
                                    </div>
                                </div>
                            </form>
                            <div id="right_panel_header">
                                <?= $this->load->view('employer_top'); ?>
                            </div>

                            <div id="main_box" class="table-responsive">
                                <?
                                if (isset($employees) && count($employees) > 0) {
                                    ?>
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead >
                                        <th>
                                            sl
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Highest Degree
                                        </th>
                                        <th>
                                            Age
                                        </th>
                                        <th>
                                            Experience
                                        </th>
                                        <th>
                                            Expected Salary
                                        </th>
                                        </thead>
                                        <tbody>
                                            <?
                                            $i = 1;
                                            foreach ($employees as $employee) {
                                                echo '<tr>';
                                                echo '<td>' . $i++ . '</td>';
                                                echo '<td>' . $employee['name'] . '</td>';
                                                echo '<td>' . $employee['degree_title'] . '</td>';
                                                echo '<td>' . $employee['age'] . '</td>';
                                                if (isset($employee['company_name'])) {
                                                    echo '<td>' . $employee['company_name'] . '<br/>';
                                                    echo $employee['position_held'] . '<br/>';
                                                    echo '(' . date('F, Y', strtotime($employee['from'])) . '-';
                                                    echo $employee['to'] . ')';
                                                    echo '</td>';
                                                } else {
                                                    echo '<td></td>';
                                                }
                                                echo '<td>' . $employee['expected_salary'] . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?
                                } else {
                                    echo 'sorry, no data found.';
                                }
                                ?>
                            </div>
                            <div>
                                <? echo isset($links) ? $links : '';?>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </body>
</html>