<div class="content-top">
    <ul>
        <li style="display: block;float: left;width: 18%;color: #FD724C">1. personal</li>
        <li style="display: block;float: left;width: 18%">3. Employment</li>
        <li style="display: block;float: left;width: 18%">4. Photograph</li>
        <li style="display: block;float: left;width: 18%">2. Education/Training</li>
        <li style="display: block;float: left;width: 18%">5. Others</li>
    </ul>
</div>
<form name="personal_form" action="<?=  site_url()?>/CreateAccount/Register" method="Post">
<div id="main-content">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>stylesheets/jquery-ui-1.8.18.custom.css" />
    <script type="text/javascript" src="<?=  base_url()?>JS/personal_entry_form.js"></script>
    <script type="text/javascript" src="<?=  base_url()?>JS/personal_validation.js"></script>
    <script type="text/javascript" src="<?=  base_url()?>JS/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?=  base_url()?>JS/jquery-ui-1.8.11.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#account_dob').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '-60:-15',
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
    <table class="form-table" style="width: 80%">
    <thead>
        <tr>
            <th colspan="2">Personal Detail</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="right">Name:</td>
            <td class="left"><input type="text" name="account_name" id="account_name" /></td>
        </tr>
        <tr>
            <td class="right">Father's Name:</td>
            <td class="left"><input type="text" name="account_father_name" id="account_father_name" /></td>
        </tr>
        <tr>
            <td class="right">Mother's Name:</td>
            <td class="left"><input type="text" name="account_mather_name" id="account_mather_name" /></td>
        </tr>
        <tr>
            <td class="right">Date of Birth:</td>
            <td class="left">
                <input type="text" readonly="readonly" id="account_dob" name="account_dob" />
            </td>
        </tr>
        <tr>
            <td class="right">Gender:</td>
            <td class="left">
                <select name="account_gender" id="account_gender">
                    <option value="m">Male</option>
                    <option value="f">Female</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="right">Marital status:</td>
            <td class="left">
                <select name="account_marital_status" id="account_marital_status">
                    <option value="s">Single</option>
                    <option value="m">Married</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="right">Nationality:</td>
            <td class="left"><input type="text" name="account_nationality" id="account_nationality" /></td>
        </tr>
        <tr>
            <td class="right">Religion:</td>
            <td class="left"><input type="text" name="account_religion" id="account_religion" /></td>
        </tr>
        <tr>
            <td class="right">Present Address:</td>
            <td class="left"><textarea name="account_present_address" id="account_present_address"></textarea></td>
        </tr>
        <tr>
            <td class="right">Permanent Address:</td>
            <td class="left"><textarea name="account_permanent_address" id="account_permanent_address"></textarea></td>
        </tr>
        <tr>
            <td class="right">Current Location:</td>
            <td class="left">
                <SCRIPT language="javascript">
			function LoadLocations(intOption)
			{
				var strHtml = "";				
				document.getElementById("spnLocation").innerHTML = "";
				if (intOption == "0")
				{					
					strHtml = "<SELECT name='account_loaction' size='1' id='account_loaction'>"
					strHtml = strHtml + "<option value='-1' Selected>Select</option><option value='1'>B. Baria</option><option value='2'>Bagerhat</option><option value='3'>Bandarban</option><option value='4'>Barisal</option><option value='5'>Bhola</option><option value='6'>Bogra</option><option value='7'>Borguna</option><option value='8'>Chandpur</option><option value='9'>Chapainawabganj</option><option value='10'>Chittagong</option><option value='11'>Chuadanga</option><option value='12'>Comilla</option><option value='13'>Cox's Bazar</option><option value='14'>Dhaka</option><option value='15'>Dinajpur</option><option value='16'>Faridpur</option><option value='17'>Feni</option><option value='18'>Gaibandha</option><option value='19'>Gazipur</option><option value='20'>Gopalgonj</option><option value='21'>Hobigonj</option><option value='22'>Jamalpur</option><option value='23'>Jessore</option><option value='24'>Jhalokathi</option><option value='25'>Jhenaidah</option><option value='26'>Joypurhat</option><option value='27'>Khagrachari</option><option value='28'>Khulna</option><option value='29'>Kishorgonj</option><option value='30'>Kurigram</option><option value='31'>Kushtia</option><option value='32'>Lalmonirhat</option><option value='33'>Laxmipur</option><option value='34'>Madaripur</option><option value='35'>Magura</option><option value='36'>Manikgonj</option><option value='37'>Meherpur</option><option value='38'>MoulaviBazar</option><option value='39'>Munshigonj</option><option value='40'>Mymensingh</option><option value='41'>Naogaon</option><option value='42'>Narail</option><option value='43'>Narayangonj</option><option value='44'>Narshingdi</option><option value='45'>Natore</option><option value='46'>Netrokona</option><option value='47'>Nilphamari</option><option value='48'>Noakhali</option><option value='49'>Pabna</option><option value='50'>Panchagahr</option><option value='51'>Patuakhali</option><option value='52'>Pirojpur</option><option value='53'>Rajbari</option><option value='54'>Rajshahi</option><option value='55'>Rangamati</option><option value='56'>Rangpur</option><option value='57'>Satkhira</option><option value='58'>Shariatpur</option><option value='59'>Sherpur</option><option value='60'>Sirajgonj</option><option value='61'>Sunamgonj</option><option value='62'>Sylhet</option><option value='63'>Tangail</option><option value='64'>Thakurgaon</option>";
					strHtml = strHtml + "</SELECT>";
				}
				if (intOption == "1")
				{
					document.getElementById("spnLocation").innerHTML = "";
					strHtml = "<SELECT name='account_loaction' size='1' id='account_loaction'>"
					strHtml = strHtml + "<option value='-1' Selected>Select</option><option value='101'>Afghanistan</option><option value='102'>Albania</option><option value='103'>Algeria</option><option value='104'>American Samoa</option><option value='105'>Andorra</option><option value='106'>Angola</option><option value='107'>Anguilla</option><option value='108'>Antarctica</option><option value='109'>Antigua</option><option value='110'>Argentina</option><option value='111'>Armenia</option><option value='112'>Aruba</option><option value='113'>Australia</option><option value='114'>Austria</option><option value='115'>Azerbaijan</option><option value='116'>Bahamas</option><option value='117'>Bahrain</option><option value='119'>Barbados</option><option value='120'>Belarus</option><option value='121'>Belgium</option><option value='122'>Belize</option><option value='123'>Benin</option><option value='124'>Bermuda</option><option value='125'>Bhutan</option><option value='126'>Bolivia</option><option value='127'>Bosnia and Herzegovina</option><option value='128'>Botswana</option><option value='129'>Brazil</option><option value='130'>British Virgin Islands</option><option value='131'>Brunei</option><option value='132'>Bulgaria</option><option value='133'>Burkina Faso</option><option value='134'>Burma</option><option value='135'>Burundi</option><option value='136'>Cambodia</option><option value='137'>Cameroon</option><option value='138'>Canada</option><option value='139'>Cape Verde</option><option value='140'>Central African Republic</option><option value='141'>Chad</option><option value='142'>Chile</option><option value='143'>China</option><option value='144'>Colombia</option><option value='145'>Comoros</option><option value='147'>Congo</option><option value='146'>Congo (Zaire)</option><option value='148'>Cook Islands</option><option value='149'>Costa Rica</option><option value='150'>Cote d'Ivoire (Ivory Coast)</option><option value='151'>Croatia</option><option value='152'>Cuba</option><option value='153'>Cyprus</option><option value='154'>Czech Republic</option><option value='155'>Denmark</option><option value='156'>Djibouti</option><option value='157'>Dominica</option><option value='158'>Dominican Republic</option><option value='323'>East Timor</option><option value='159'>Ecuador</option><option value='160'>Egypt</option><option value='161'>El Salvador</option><option value='162'>Equatorial Guinea</option><option value='163'>Eritrea</option><option value='164'>Estonia</option><option value='165'>Ethiopia</option><option value='166'>Falkland Islands</option><option value='232'>Federated States of Micronesia</option><option value='167'>Fiji</option><option value='168'>Finland</option><option value='169'>France</option><option value='170'>French Guiana</option><option value='171'>French Polynesia</option><option value='172'>Gabon</option><option value='174'>Gaza Strip and West Bank</option><option value='175'>Georgia</option><option value='176'>Germany</option><option value='177'>Ghana</option><option value='178'>Gibraltar</option><option value='179'>Greece</option><option value='180'>Greenland</option><option value='181'>Grenada</option><option value='182'>Guadeloupe</option><option value='183'>Guam</option><option value='184'>Guatemala</option><option value='185'>Guinea</option><option value='186'>Guinea-Bissau</option><option value='187'>Guyana</option><option value='188'>Haiti</option><option value='190'>Honduras</option><option value='191'>Hong Kong</option><option value='192'>Hungary</option><option value='193'>Iceland</option><option value='194'>India</option><option value='195'>Indonesia</option><option value='196'>Iran</option><option value='197'>Iraq</option><option value='198'>Ireland</option><option value='199'>Israel</option><option value='200'>Italy</option><option value='201'>Jamaica</option><option value='202'>Japan</option><option value='203'>Jordan</option><option value='204'>Kazakhstan</option><option value='205'>Kenya</option><option value='206'>Kiribati</option><option value='207'>Kuwait</option><option value='208'>Kyrgyzstan</option><option value='209'>Laos</option><option value='210'>Latvia</option><option value='211'>Lebanon</option><option value='212'>Lesotho</option><option value='213'>Liberia</option><option value='214'>Libya</option><option value='215'>Liechtenstein</option><option value='216'>Lithuania</option><option value='217'>Luxembourg</option><option value='218'>Macau</option><option value='219'>Macedonia</option><option value='220'>Madagascar</option><option value='221'>Malawi</option><option value='222'>Malaysia</option><option value='223'>Maldives</option><option value='224'>Mali</option><option value='225'>Malta</option><option value='226'>Marshall Islands</option><option value='227'>Martinique</option><option value='228'>Mauritania</option><option value='229'>Mauritius</option><option value='230'>Mayotte</option><option value='231'>Mexico</option><option value='233'>Moldova</option><option value='234'>Monaco</option><option value='235'>Mongolia</option><option value='236'>Montserrat</option><option value='237'>Morocco</option><option value='238'>Mozambique</option><option value='239'>Namibia</option><option value='240'>Nauru</option><option value='241'>Nepal</option><option value='242'>Netherlands</option><option value='243'>Netherlands Antilles</option><option value='244'>New Caledonia</option><option value='245'>New Zealand</option><option value='246'>Nicaragua</option><option value='247'>Niger</option><option value='248'>Nigeria</option><option value='249'>North Korea</option><option value='250'>Northern Mariana Islands</option><option value='251'>Norway</option><option value='252'>Oman</option><option value='253'>Pakistan</option><option value='254'>Palau</option><option value='255'>Panama</option><option value='256'>Papua New Guinea</option><option value='257'>Paraguay</option><option value='258'>Peru</option><option value='259'>Philippines</option><option value='260'>Pitcairn Islands</option><option value='261'>Poland</option><option value='262'>Portugal</option><option value='263'>Puerto Rico</option><option value='264'>Qatar</option><option value='265'>Reunion</option><option value='266'>Romania</option><option value='267'>Russia</option><option value='268'>Rwanda</option><option value='269'>Saint Kitts and Nevis</option><option value='270'>Saint Lucia</option><option value='271'>Saint Pierre and Miquelon</option><option value='272'>Saint Vincent and the Grenadines</option><option value='273'>Samoa</option><option value='274'>San Marino</option><option value='275'>Sao Tome and Principe</option><option value='276'>Saudi Arabia</option><option value='277'>Senegal</option><option value='278'>Serbia and Montenegro (Yugoslavia)</option><option value='279'>Seychelles</option><option value='280'>Sierra Leone</option><option value='281'>Singapore</option><option value='282'>Slovakia</option><option value='283'>Slovenia</option><option value='284'>Solomon Islands</option><option value='285'>Somalia</option><option value='286'>South Africa</option><option value='287'>South Korea</option><option value='288'>Spain</option><option value='289'>Sri Lanka</option><option value='290'>Sudan</option><option value='291'>Suriname</option><option value='292'>Swaziland</option><option value='293'>Sweden</option><option value='294'>Switzerland</option><option value='295'>Syria</option><option value='296'>Taiwan</option><option value='297'>Tajikistan</option><option value='298'>Tanzania</option><option value='299'>Thailand</option><option value='173'>The Gambia</option><option value='189'>The Holy See</option><option value='300'>Togo</option><option value='301'>Tonga</option><option value='302'>Trinidad and Tobago</option><option value='303'>Tunisia</option><option value='304'>Turkey</option><option value='305'>Turkmenistan</option><option value='306'>Turks and Caicos Islands</option><option value='307'>Tuvalu</option><option value='308'>Uganda</option><option value='309'>Ukraine</option><option value='310'>United Arab Emirates</option><option value='311'>United Kingdom</option><option value='312'>United States</option><option value='313'>United States Virgin Islands</option><option value='314'>Uruguay</option><option value='315'>Uzbekistan</option><option value='316'>Vanuatu</option><option value='317'>Venezuela</option><option value='318'>Vietnam</option><option value='319'>West Bank and Gaza Strip</option><option value='320'>Western Sahara</option><option value='321'>Yemen</option><option value='322'>Zambia</option>";
					strHtml = strHtml + "</SELECT>";
				}
				document.getElementById("spnLocation").innerHTML = strHtml;				
			}
			</SCRIPT>			
			<INPUT name="optLocation" type="radio" value="0" checked="checked" onClick="LoadLocations(this.value);">
			Inside Bangladesh
			<INPUT name="optLocation" type="radio" value="1" onClick="LoadLocations(this.value);">
			Outside Bangladesh
                        <br />
                        <span id="spnLocation">
                            <select name="account_location" id="account_location">
                                <option value='-1' Selected>Select</option><option value='1'>B. Baria</option><option value='2'>Bagerhat</option><option value='3'>Bandarban</option><option value='4'>Barisal</option><option value='5'>Bhola</option><option value='6'>Bogra</option><option value='7'>Borguna</option><option value='8'>Chandpur</option><option value='9'>Chapainawabganj</option><option value='10'>Chittagong</option><option value='11'>Chuadanga</option><option value='12'>Comilla</option><option value='13'>Cox's Bazar</option><option value='14'>Dhaka</option><option value='15'>Dinajpur</option><option value='16'>Faridpur</option><option value='17'>Feni</option><option value='18'>Gaibandha</option><option value='19'>Gazipur</option><option value='20'>Gopalgonj</option><option value='21'>Hobigonj</option><option value='22'>Jamalpur</option><option value='23'>Jessore</option><option value='24'>Jhalokathi</option><option value='25'>Jhenaidah</option><option value='26'>Joypurhat</option><option value='27'>Khagrachari</option><option value='28'>Khulna</option><option value='29'>Kishorgonj</option><option value='30'>Kurigram</option><option value='31'>Kushtia</option><option value='32'>Lalmonirhat</option><option value='33'>Laxmipur</option><option value='34'>Madaripur</option><option value='35'>Magura</option><option value='36'>Manikgonj</option><option value='37'>Meherpur</option><option value='38'>MoulaviBazar</option><option value='39'>Munshigonj</option><option value='40'>Mymensingh</option><option value='41'>Naogaon</option><option value='42'>Narail</option><option value='43'>Narayangonj</option><option value='44'>Narshingdi</option><option value='45'>Natore</option><option value='46'>Netrokona</option><option value='47'>Nilphamari</option><option value='48'>Noakhali</option><option value='49'>Pabna</option><option value='50'>Panchagahr</option><option value='51'>Patuakhali</option><option value='52'>Pirojpur</option><option value='53'>Rajbari</option><option value='54'>Rajshahi</option><option value='55'>Rangamati</option><option value='56'>Rangpur</option><option value='57'>Satkhira</option><option value='58'>Shariatpur</option><option value='59'>Sherpur</option><option value='60'>Sirajgonj</option><option value='61'>Sunamgonj</option><option value='62'>Sylhet</option><option value='63'>Tangail</option><option value='64'>Thakurgaon</option>
                            </select>
                        </span>
            </td>
        </tr>
        <tr>
            <td class="left" colspan="2">[*Provide at least one Phone Number] </td>
        </tr>
        <tr>
            <td class="right">Contact(home):</td>
            <td class="left"><input type="text" name="account_contact_home" id="account_contact_home" /></td>
        </tr>
        <tr>
            <td class="right">Contact(Mobile):</td>
            <td class="left"><input type="text" name="account_contact_mobile" id="account_contact_mobile" /></td>
        </tr>
        <tr>
            <td class="right">Contact(Office):</td>
            <td class="left"><input type="text" name="account_contact_office" id="account_contact_office" /></td>
        </tr>
        <tr>
            <td colspan="2">[*Do not enter/provide more than one e-mail address in either of the fields] </td>
        </tr>
        <tr>
            <td class="right">Email:</td>
            <td class="left"><input type="text" name="account_email" id="account_email" /></td>
        </tr>
        <tr>
            <td class="right">Alternate Email:</td>
            <td class="left"><input type="text" name="account_alt_email" id="account_alt_email" /></td>
        </tr>
    </tbody>
</table>
<br/>
<table class="form-table" style="width: 80%">
    <thead>
        <tr>
            <th colspan="2">Career and Application</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="right">Career Objective:</td>
            <td class="left"><textarea name="career_objective" id="career_objective"></textarea></td>
        </tr>
        <tr>
            <td class="right">Year of Experience:</td>
            <td class="left"><input type="text" name="career_year_experience" id="career_year_experience" /></td>
        </tr>
        <tr>
            <td class="right">Present Salary:</td>
            <td class="left"><input type="text" name="career_present_salary" id="career_present_salary" /></td>
        </tr>
        <tr>
            <td class="right">Expected Salary:</td>
            <td class="left"><input type="text" name="career_exp_salary" id="career_exp_salary" /></td>
        </tr>
        <tr>
            <td class="right">Looking For:</td>
            <td class="left">
                <input type="radio" checked="" name="career_level" value="Entry" />Entry Level Job 
                <input type="radio" name="career_level" value="Mid" />Mid/Managerial Level Job
		<input type="radio" name="career_level" value="Top" />Top Level Job
            </td>
        </tr>
        <tr>
            <td class="right">Available For:</td>
            <td class="left">
                <input type="radio" checked="" value="Full Time" name="career_available" />Full Time   
		<input type="radio" value="Part Time" name="career_available" />Part-Time
		<input type="radio" value="Contract" name="career_available" />Contract 
            </td>
        </tr>
                
    </tbody>
</table>
<br />
<table class="form-table" style="width: 80%">
    <thead>
        <tr>
            <th colspan="4">Preferred Job Category</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="left">

    <? $this->load->view('common/category_list', $categories) ?>

                <input id="selected_Cat" type="hidden" value="" name="selected_Cat" />
            </td>
            <td>
                <input id="cmdAdd_WorkArea" type="button" name="cmdAdd_WorkArea" value="Add >" onclick="append('preferred_category','lstSelectedWorkArea','selected_Cat','2','You cannot add more than 2 working sector!');" /><br /><input id="cmdRemove_WorkArea" type="button" value="< Remove" onclick="remove('lstSelectedWorkArea','selected_Cat');" name="cmdRemove_WorkArea" /> 
            </td>
            <td>
                <select id="lstSelectedWorkArea" style="width:180px;" size="5" name="lstSelectedWorkArea">
      
                </select>
            </td>
            <td>
                <span><strong></strong><br />
                    <span></span>
                    
                </span>
                <span><strong></strong><br />
                    <span></span>
                    
                </span>
            </td>
        </tr>       
                
    </tbody>
</table>
<br />
<table class="form-table" style="width: 80%">
    <thead>
        <tr>
            <th>Preferred Job Location</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Preferred Job Location defines the geographical place where you prefer to work.</td>
        </tr>        
        <tr>
            <td>
                <u>Inside Bangladesh</u>
                <input id="optJobArea" type="radio" checked="" value="Anywhere" onclick="ClearListBox('lstSelectedJobArea'); EnableDisable('lstJobArea',true); EnableDisable('lstSelectedJobArea',true); EnableDisable('cmdAdd_JobArea',true); EnableDisable('cmdRemove_JobArea',true);" name="optJobArea" />
      
				Any where in Bangladesh  
				
                <input id="optJobArea" type="radio" value="Selection" onclick="EnableDisable('lstJobArea',false); EnableDisable('lstSelectedJobArea',false); EnableDisable('cmdAdd_JobArea',false); EnableDisable('cmdRemove_JobArea',false); SetValueNull('selected_Dist');" name="optJobArea" />
      
				Select from below
            </td>
        </tr>
        <tr>
            <td>
                <SELECT name="lstJobArea" size="5" id="lstJobArea"  disabled style="width:180px;"> 
                            <option value='1'>B. Baria</option><option value='2'>Bagerhat</option><option value='3'>Bandarban</option><option value='4'>Barisal</option><option value='5'>Bhola</option><option value='6'>Bogra</option><option value='7'>Borguna</option><option value='8'>Chandpur</option><option value='9'>Chapainawabganj</option><option value='10'>Chittagong</option><option value='11'>Chuadanga</option><option value='12'>Comilla</option><option value='13'>Cox's Bazar</option><option value='14'>Dhaka</option><option value='15'>Dinajpur</option><option value='16'>Faridpur</option><option value='17'>Feni</option><option value='18'>Gaibandha</option><option value='19'>Gazipur</option><option value='20'>Gopalgonj</option><option value='21'>Hobigonj</option><option value='22'>Jamalpur</option><option value='23'>Jessore</option><option value='24'>Jhalokathi</option><option value='25'>Jhenaidah</option><option value='26'>Joypurhat</option><option value='27'>Khagrachari</option><option value='28'>Khulna</option><option value='29'>Kishorgonj</option><option value='30'>Kurigram</option><option value='31'>Kushtia</option><option value='32'>Lalmonirhat</option><option value='33'>Laxmipur</option><option value='34'>Madaripur</option><option value='35'>Magura</option><option value='36'>Manikgonj</option><option value='37'>Meherpur</option><option value='38'>MoulaviBazar</option><option value='39'>Munshigonj</option><option value='40'>Mymensingh</option><option value='41'>Naogaon</option><option value='42'>Narail</option><option value='43'>Narayangonj</option><option value='44'>Narshingdi</option><option value='45'>Natore</option><option value='46'>Netrokona</option><option value='47'>Nilphamari</option><option value='48'>Noakhali</option><option value='49'>Pabna</option><option value='50'>Panchagahr</option><option value='51'>Patuakhali</option><option value='52'>Pirojpur</option><option value='53'>Rajbari</option><option value='54'>Rajshahi</option><option value='55'>Rangamati</option><option value='56'>Rangpur</option><option value='57'>Satkhira</option><option value='58'>Shariatpur</option><option value='59'>Sherpur</option><option value='60'>Sirajgonj</option><option value='61'>Sunamgonj</option><option value='62'>Sylhet</option><option value='63'>Tangail</option><option value='64'>Thakurgaon</option>
                    </SELECT>
                    <INPUT id="cmdAdd_JobArea" onClick="append('lstJobArea','lstSelectedJobArea','selected_Dist','15','You cannot add more than 15 district!');" type="button" value="Add >" name="cmdAdd_JobArea" disabled />
                    
                    <INPUT name="cmdRemove_JobArea" type="button" id="cmdRemove_JobArea" disabled onClick="remove('lstSelectedJobArea','selected_Dist');" value="< Remove" />
                    <SELECT name="lstSelectedJobArea" size="5" class="BDJtextarea" id="lstSelectedJobArea" disabled style="width:180px;" >
                    </SELECT>
                    <INPUT id="selected_Dist" type="hidden" name="selected_Dist" value="" />
            </td>		
        </tr>
        <tr>
            <td>Outside Bangladesh:</td>
        </tr>
        <tr>
            <td>
                <SELECT name="lstJobCountry" size="5" class="BDJtextarea" id="lstJobCountry"  style="width:180px;">
                        <option value='101'>Afghanistan</option><option value='102'>Albania</option><option value='103'>Algeria</option><option value='104'>American Samoa</option><option value='105'>Andorra</option><option value='106'>Angola</option><option value='107'>Anguilla</option><option value='108'>Antarctica</option><option value='109'>Antigua</option><option value='110'>Argentina</option><option value='111'>Armenia</option><option value='112'>Aruba</option><option value='113'>Australia</option><option value='114'>Austria</option><option value='115'>Azerbaijan</option><option value='116'>Bahamas</option><option value='117'>Bahrain</option><option value='119'>Barbados</option><option value='120'>Belarus</option><option value='121'>Belgium</option><option value='122'>Belize</option><option value='123'>Benin</option><option value='124'>Bermuda</option><option value='125'>Bhutan</option><option value='126'>Bolivia</option><option value='127'>Bosnia and Herzegovina</option><option value='128'>Botswana</option><option value='129'>Brazil</option><option value='130'>British Virgin Islands</option><option value='131'>Brunei</option><option value='132'>Bulgaria</option><option value='133'>Burkina Faso</option><option value='134'>Burma</option><option value='135'>Burundi</option><option value='136'>Cambodia</option><option value='137'>Cameroon</option><option value='138'>Canada</option><option value='139'>Cape Verde</option><option value='140'>Central African Republic</option><option value='141'>Chad</option><option value='142'>Chile</option><option value='143'>China</option><option value='144'>Colombia</option><option value='145'>Comoros</option><option value='147'>Congo</option><option value='146'>Congo (Zaire)</option><option value='148'>Cook Islands</option><option value='149'>Costa Rica</option><option value='150'>Cote d'Ivoire (Ivory Coast)</option><option value='151'>Croatia</option><option value='152'>Cuba</option><option value='153'>Cyprus</option><option value='154'>Czech Republic</option><option value='155'>Denmark</option><option value='156'>Djibouti</option><option value='157'>Dominica</option><option value='158'>Dominican Republic</option><option value='323'>East Timor</option><option value='159'>Ecuador</option><option value='160'>Egypt</option><option value='161'>El Salvador</option><option value='162'>Equatorial Guinea</option><option value='163'>Eritrea</option><option value='164'>Estonia</option><option value='165'>Ethiopia</option><option value='166'>Falkland Islands</option><option value='232'>Federated States of Micronesia</option><option value='167'>Fiji</option><option value='168'>Finland</option><option value='169'>France</option><option value='170'>French Guiana</option><option value='171'>French Polynesia</option><option value='172'>Gabon</option><option value='174'>Gaza Strip and West Bank</option><option value='175'>Georgia</option><option value='176'>Germany</option><option value='177'>Ghana</option><option value='178'>Gibraltar</option><option value='179'>Greece</option><option value='180'>Greenland</option><option value='181'>Grenada</option><option value='182'>Guadeloupe</option><option value='183'>Guam</option><option value='184'>Guatemala</option><option value='185'>Guinea</option><option value='186'>Guinea-Bissau</option><option value='187'>Guyana</option><option value='188'>Haiti</option><option value='190'>Honduras</option><option value='191'>Hong Kong</option><option value='192'>Hungary</option><option value='193'>Iceland</option><option value='194'>India</option><option value='195'>Indonesia</option><option value='196'>Iran</option><option value='197'>Iraq</option><option value='198'>Ireland</option><option value='199'>Israel</option><option value='200'>Italy</option><option value='201'>Jamaica</option><option value='202'>Japan</option><option value='203'>Jordan</option><option value='204'>Kazakhstan</option><option value='205'>Kenya</option><option value='206'>Kiribati</option><option value='207'>Kuwait</option><option value='208'>Kyrgyzstan</option><option value='209'>Laos</option><option value='210'>Latvia</option><option value='211'>Lebanon</option><option value='212'>Lesotho</option><option value='213'>Liberia</option><option value='214'>Libya</option><option value='215'>Liechtenstein</option><option value='216'>Lithuania</option><option value='217'>Luxembourg</option><option value='218'>Macau</option><option value='219'>Macedonia</option><option value='220'>Madagascar</option><option value='221'>Malawi</option><option value='222'>Malaysia</option><option value='223'>Maldives</option><option value='224'>Mali</option><option value='225'>Malta</option><option value='226'>Marshall Islands</option><option value='227'>Martinique</option><option value='228'>Mauritania</option><option value='229'>Mauritius</option><option value='230'>Mayotte</option><option value='231'>Mexico</option><option value='233'>Moldova</option><option value='234'>Monaco</option><option value='235'>Mongolia</option><option value='236'>Montserrat</option><option value='237'>Morocco</option><option value='238'>Mozambique</option><option value='239'>Namibia</option><option value='240'>Nauru</option><option value='241'>Nepal</option><option value='242'>Netherlands</option><option value='243'>Netherlands Antilles</option><option value='244'>New Caledonia</option><option value='245'>New Zealand</option><option value='246'>Nicaragua</option><option value='247'>Niger</option><option value='248'>Nigeria</option><option value='249'>North Korea</option><option value='250'>Northern Mariana Islands</option><option value='251'>Norway</option><option value='252'>Oman</option><option value='253'>Pakistan</option><option value='254'>Palau</option><option value='255'>Panama</option><option value='256'>Papua New Guinea</option><option value='257'>Paraguay</option><option value='258'>Peru</option><option value='259'>Philippines</option><option value='260'>Pitcairn Islands</option><option value='261'>Poland</option><option value='262'>Portugal</option><option value='263'>Puerto Rico</option><option value='264'>Qatar</option><option value='265'>Reunion</option><option value='266'>Romania</option><option value='267'>Russia</option><option value='268'>Rwanda</option><option value='269'>Saint Kitts and Nevis</option><option value='270'>Saint Lucia</option><option value='271'>Saint Pierre and Miquelon</option><option value='272'>Saint Vincent and the Grenadines</option><option value='273'>Samoa</option><option value='274'>San Marino</option><option value='275'>Sao Tome and Principe</option><option value='276'>Saudi Arabia</option><option value='277'>Senegal</option><option value='278'>Serbia and Montenegro (Yugoslavia)</option><option value='279'>Seychelles</option><option value='280'>Sierra Leone</option><option value='281'>Singapore</option><option value='282'>Slovakia</option><option value='283'>Slovenia</option><option value='284'>Solomon Islands</option><option value='285'>Somalia</option><option value='286'>South Africa</option><option value='287'>South Korea</option><option value='288'>Spain</option><option value='289'>Sri Lanka</option><option value='290'>Sudan</option><option value='291'>Suriname</option><option value='292'>Swaziland</option><option value='293'>Sweden</option><option value='294'>Switzerland</option><option value='295'>Syria</option><option value='296'>Taiwan</option><option value='297'>Tajikistan</option><option value='298'>Tanzania</option><option value='299'>Thailand</option><option value='173'>The Gambia</option><option value='189'>The Holy See</option><option value='300'>Togo</option><option value='301'>Tonga</option><option value='302'>Trinidad and Tobago</option><option value='303'>Tunisia</option><option value='304'>Turkey</option><option value='305'>Turkmenistan</option><option value='306'>Turks and Caicos Islands</option><option value='307'>Tuvalu</option><option value='308'>Uganda</option><option value='309'>Ukraine</option><option value='310'>United Arab Emirates</option><option value='311'>United Kingdom</option><option value='312'>United States</option><option value='313'>United States Virgin Islands</option><option value='314'>Uruguay</option><option value='315'>Uzbekistan</option><option value='316'>Vanuatu</option><option value='317'>Venezuela</option><option value='318'>Vietnam</option><option value='319'>West Bank and Gaza Strip</option><option value='320'>Western Sahara</option><option value='321'>Yemen</option><option value='322'>Zambia</option>
                </SELECT>
                <INPUT name="cmdAdd_JobCountry" id="cmdAdd_JobCountry" onClick="append('lstJobCountry','lstSelectedJobCountry','selected_JobCountry','10','You cannot add more than 10 Country!');" type="button" value="Add >" />
				
				<INPUT name="cmdRemove_JobCountry" type="button" id="cmdRemove_JobCountry" onClick="remove('lstSelectedJobCountry','selected_JobCountry');" value="< Remove" />
		
            			<SELECT name="lstSelectedJobCountry" size="5" class="BDJtextarea" id="lstSelectedJobCountry" style="width:180px;">
					
				</SELECT>
				<INPUT id="selected_JobCountry" type="hidden" name="selected_JobCountry" value="" />
            </td>
        </tr>
    </tbody>
</table>
<br />
<table class="form-table" style="width: 80%">
    <thead>
        <tr>
            <th>Preferred Organization Type</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>(You can select maximum 12 Organization Types.) </td>
        </tr>       
        <tr>
            <td>
                <SELECT name="lstOrganization" size="5" class="BDJtextarea" id="lstOrganization" style="width:180px;"> 
                        <option value='13'>Advertising Agency</option><option value='48'>Agro based firms (incl. Agro Processing/Seed/GM)</option><option value='43'>Airline/GSA</option><option value='52'>Architecture/Interior Design Firm</option><option value='38'>Audit Firms /Tax Consultant</option><option value='70'>Automobile Companies</option><option value='1'>Banks</option><option value='18'>BPO</option><option value='37'>Buying House (Garments)</option><option value='68'>Call Center</option><option value='75'>Cement Industry</option><option value='67'>Chemical Industries</option><option value='27'>Clearing & Forwarding (C&F) companies</option><option value='20'>Computer Hardware/Network Companies</option><option value='34'>Consulting Firms</option><option value='59'>Cosmetics/Personal Care</option><option value='14'>Design/Printing/Publishing</option><option value='23'>Development Agencies</option><option value='54'>Direct Selling/Marketing Service Company</option><option value='8'>Distribution Companies/Wholesale</option><option value='11'>Education (School & Colleges)</option><option value='10'>Education (Universities)</option><option value='61'>Electronic Equipment/Home Appliances</option><option value='31'>Embassies/Foreign Consulate</option><option value='32'>Engineering Firms</option><option value='15'>Event Management</option><option value='81'>Fisheries</option><option value='47'>Food (Packaged)/Beverage</option><option value='64'>Freight forwarding</option><option value='76'>Furniture Manufacturer</option><option value='35'>Garments (Woven/Apparel/Knitting)</option><option value='78'>Garments Accessories</option><option value='21'>Governement Organizations</option><option value='66'>Group of Companies</option><option value='39'>Hospitals/Clinic/Diagonastic Centre</option><option value='41'>Hotels/Rosorts</option><option value='51'>Immigration & Education Consultancy Service</option><option value='55'>Indenting Firm</option><option value='2'>Insurance</option><option value='4'>Investment/Merchant Banking</option><option value='19'>ISP</option><option value='17'>IT Enabled Service</option><option value='77'>Jute Goods/ Jute Yarn</option><option value='57'>Law Firm</option><option value='3'>Leasing</option><option value='26'>Logistic/Courier/Air Express Companies</option><option value='53'>Manpower Recruitment</option><option value='6'>Manufacturing (FMCG)</option><option value='7'>Manufacturing (Light Engineering & Heavy Industry)</option><option value='33'>Market Research Firms</option><option value='30'>Media/Public Relation Companies</option><option value='62'>Medical Equipment</option><option value='80'>Mobile Accessories</option><option value='28'>Multinational Companies</option><option value='29'>Newspaper/Magazine</option><option value='22'>NGOs</option><option value='65'>Overseas Companies</option><option value='74'>Packaging Industry</option><option value='40'>Pharmaceutical/Medicine Companies</option><option value='73'>Plastic/ Polymer Industry</option><option value='60'>Poultry/Dairy/Veterinary</option><option value='49'>Power Equipment/Generator/CNG</option><option value='63'>Real Estate/ Developer</option><option value='72'>Research Organization</option><option value='42'>Resturants</option><option value='9'>Retail/Shops</option><option value='56'>Security Service Company</option><option value='71'>Share Brokerage/ Securities House</option><option value='25'>Shipping</option><option value='46'>Shrimp/Hachery</option><option value='16'>Software Companies</option><option value='79'>Sweater Industry</option><option value='45'>Tannery/Footwear</option><option value='50'>Tea Company</option><option value='5'>Telecommunication</option><option value='36'>Textile (Spinning, Weaving, Knitting, Dyeing/Finishing)</option><option value='69'>Tobacco</option><option value='24'>Trading or Export/Import</option><option value='12'>Training Institutues</option><option value='44'>Travel Agents/Tour Operators</option>
                </SELECT>
                <INPUT class="BDJButton2" id="cmdAdd_Org" onClick="append('lstOrganization','lstSelectedOrganization','selected_Job','12','You cannot add more than 12 organization!');" type="button" value="Add >" name="cmdAdd_Org" />

                <INPUT name="cmdRemove_Org" type="button" class="BDJButton2" id="cmdRemove_Org" onClick="remove('lstSelectedOrganization','selected_Job');" value="< Remove" />
                
                <SELECT name="lstSelectedOrganization" size="5" class="BDJtextarea" id="lstSelectedOrganization" style="width:180px;">
                </SELECT>
                <INPUT id="selected_Job" type="hidden" name="selected_Job" value="" />
            </td>
        </tr>       
    </tbody>
</table>
<br />
<table class="form-table" style="width: 80%">
    <thead>
        <tr>
            <th colspan="2">Other Relevant Information</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="right">Career Summary:</td>
            <td class="left">
                <textarea name="career_summary" id="career_summary"></textarea>
            </td>
        </tr> 
        <tr>
            <td class="right">Special Qualification:</td>
            <td class="left"><textarea name="career_summary" id="career_summary"></textarea></td>
        </tr>  
        <tr>
            <td class="right">Keyword:</td>
            <td class="left"><input type="text" name="account_keyword" id="account_keyword" /></td>
        </tr>         
    </tbody>
</table>
<table class="form-table" style="width: 80%">
    <thead>
        <tr>
            <th colspan="2">User name & password of your account</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="right">User Name:</td>
            <td class="left">
                <input type="text" name="account_username" id="account_username" />
            </td>
        </tr> 
        <tr>
            <td class="right">Password:</td>
            <td class="left"><input type="password" name="account_password" id="account_password" /></td>
        </tr>  
        <tr>
            <td class="right">Retype Password:</td>
            <td class="left"><input type="password" name="account_retype_password" id="account_retype_password" /></td>
        </tr>         
    </tbody>
</table>
<div style="text-align: center">
    <input id="Continue" type="submit" onclick="return personal_validate(this.form,'DOB');" value="Continue" />
</div>
</div>
</form>