<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?= $title; ?></title>
        <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui-1.8.18.custom.css" type="text/css" />
        <link href="<?= base_url(); ?>/css/bootstrap.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>/css/custom.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />


<link href="<?= base_url(); ?>/css/morris-0.4.3.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

        <script type="text/javascript">
            
        </script>
        <style type="text/css">
            body
            {
                margin:0 auto;
                width:1200px;
                
            }
            #employer_page
            {
                float:left;
                margin:10px;
                width:1180px;

                border-radius:5px;
                padding-bottom:20px;


            }
            #employer_header
            {
                float:right;
/*                margin:0px 10px;*/
                width:1180px;
                height:100px;
                margin-bottom: 50px;
                margin-top: -20px;
                /*background:url(../employer_images/logo1.png) left top no-repeat;*/

                /*background: #A5090A;*/
            } 

            #employer_menu
            {
                float:left;
                width:1180px;
                background: #4d90f0;
                box-shadow:3px 3px 10px 5px #B5B4B4;
                border-radius:10px 10px 0px 0px;
            }
            #emp_in
            {
                float:left; 
                width:1170px;
                background:#fff ;
                margin-top: 35px;
                margin-left: 5px;
                border-radius:10px 10px 0px 0px;
            }
            #emp_format1
            {
                float:left; 
                width:1170px;
                background: #fff;
                height: 100px;
                border-radius:10px 10px 0px 0px;
            }
            #emp_format1_red
            {
                margin-top: 30px;
                float:left; 
                width:1170px;
                background: #A5090A;
                height: 30px;
                border-bottom: 5px #000 solid;
            }
            #red_all
            {
                float:left; 
                width:1170px;
                height: 1000px;
                background: #A5090A;

            }

            table.employer_account
            {
                float:left;
                margin:0px 10px;
                width:800px;

            }
            table.employer_account tr th
            {
                text-align:left;
                padding:5px 10px;
                color:#FF6600;
                border-bottom:1px #3f3a30 solid;
            }  
            table.employer_account tr td
            {
                color:#000000;
                padding:5px 10px;
                border-bottom: 1px #0099ff solid;
            } 
            table.employer_account tr td input
            {

            }
            #new_menu
            {
                width: 1140px;
                height: auto;
                float: left;
                background: #fff;
                margin-left: 20px;
                margin-top: 20px;
                border-radius:10px 10px 0px 0px;
                margin-bottom: 20px;
            }
            #red_menu
            {
                width: 1100px;
                float: left;
                background: #fff;
                height: 40px;
                margin-left: 20px;
            }



            #red_menu ul
            {
                margin-left: 50px;
                margin-top: 4px;
                padding: 0;
            }

            #red_menu ul li
            {
                display: inline;

            }

            #red_menu li a
            {
                display: block;
                float: left;
                padding:6px 20px;
                font-size:12px;
                color: #fff;
                font-weight:normal;
                text-decoration: none;
                border-right:1px solid #fff;

            }
            #red_menu li a:hover {color:#000; }
            #free_space
            {
                width: 1140px;
                float: left;
                background: #fff;
/*                height: 0px;*/
                margin-top: 50px;
            }
            #free_space table
            {
                float: right;
                margin-top: -40px;
                margin-right: 10px;
            }
            #all_details
            {
                float: left;
                width: 1100px;
                height: auto;
                background: #fff;
                margin-left: 20px;
/*                margin-top: 35px;*/
                margin-bottom: 30px;

            }
            #details1
            {
                float: left;
                width: 1080px;
                height: 30px;
                background: #000;
                margin-left: 10px;
                margin-top: 20px;
            }
            #details2
            {
                float: left;
                width: 900px;
                height: 200px;
                background: #cddbe4;
                margin-left: 90px;
                margin-top: 50px;
            }
            #details2_2
            {
                float: left;
                width: 900px;
                height: 370px;
                background:#cddbe4;
                margin-left: 90px;
                margin-top: 50px;
            }
            #white_details
            {
                float: left;
                width: 1080px;
                height:auto;
                background: #fff;
                margin-left: 10px;
                margin-bottom: 10px;
            }
            #details3
            {
                float: left;
                width: 900px;
                height: 35px;
                
            }
            #details4
            {
                float: left;
                width: 210px;
                height: 30px;
                
                border-radius:10px 10px 0px 0px;
                
                
            }
            #details4 pre
            {
                font-size: larger;
                margin-top: 3px;
                color: #000;
            }
            #last
            {
                float: left;
                width: 100%;
                height: 30px;
                background: #3C3728;

            }
        </style>
    </head>
    <body>


        <div id="employer_page">
            <div id="employer_header">
                <img src="<?= base_url() ?>employer_images/logo1.png" style="margin-top: 30px;" />
            </div>
            <div id="employer_menu">
                <div id="new_menu">
                    <div id="free_space">
                        <table>
                            <rt>
                            <td><img src="<?= base_url() ?>/employer_images/fac.png"/></td>
                            <td><img src="<?= base_url() ?>/employer_images/twi.png"/></td>
                            <td><img src="<?= base_url() ?>/employer_images/rss.png"/></td>
                            </rt>
                        </table>
                    </div>
                    <div id="red_menu">
                        <?= $this->load->view('emploginmenu.php'); ?>
                    </div>

                    <div id="all_details">  	
<!--                        <div id="details1">
                        </div>-->
                        <div id="white_details">

                            <?
                            if (isset($employer)) {
                                ?>
                                <form action="<?= site_url() ?>/home/up_employer" enctype="multipart/form-data" method="post" > 
                                    <div id="details2"  style="height:120px">
                                        <div id="details3">
                                            <div id="details4">
                                                <pre> Account Information</pre>
                                            </div>
                                            
                                            <table class="table table-striped table-bordered" style="margin-top:50px">
                                                
                                                
                                                <tr>
                                                    <td>User Name</td>

                                                    <td><input class="form-control" type="text" name="username" value="<?= $employer['username'] ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Password</td>

                                                    <td><input class="form-control" type="password" name="password" value="<?= $employer['password'] ?>"/><span>[Maximum 6 Characters.]</span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>


                                    <div id="details2" style="height: 250px">
                                        <div id="details3">
                                            <div id="details4">
                                                <pre> Company Details</pre>
                                            </div>
                                            <table class="table table-striped table-bordered " style="margin-top:50px">

                                                <tr>
                                                    <td>Company Name</td>

                                                    <td><input class="form-control" type="text" name="company_name"  value="<?= $employer['company_name'] ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Alternative Company Name</td>

                                                    <td><input class="form-control" type="text" name="alt_company_name" value="<?= $employer['alt_company_name'] ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Contact Person</td>

                                                    <td><input class="form-control" type="text" name="contact_person" value="<?= $employer['contact_person'] ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Contact Person's Designation</td>

                                                    <td><input class="form-control" type="text" name="contact_designation"  value="<?= $employer['contact_designation'] ?>" /></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div id="details2" style="height: 250px">
                                        <div id="details3">
                                            <div id="details4">
                                                <pre> Business Type</pre>
                                            </div>
                                            <table class="table table-striped table-bordered" style="margin-top:50px">
                                                <tr>
            <!--                                        <th colspan="3">Business Type</th>-->
                                                </tr>
                                                <tr>
                                                    <td>Business Type</td>

                                                    <td><? $this->load->view('business_type_list', '$data') ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Business Description</td>

                                                    <td><textarea class="form-control" name="business_description" cols="50" rows="5"> <?= $employer['business_description'] ?></textarea></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div id="details2_2" style="height: 550px">
                                        <div id="details3">
                                            <div id="details4">
                                                <pre> Contact Details</pre>
                                            </div>
                                            <table class="table table-striped table-bordered " style="margin-top:50px">
                                                <tr>

                                                </tr>
                                                <tr>
                                                    <td>Company Address</td>

                                                    <td><input  class="form-control" type="text" name="company_address" size="50" value="<?= $employer['company_address'] ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Country</td>

                                                    <td>
                                                        <input class="form-control" type="text" name="country_name" value="<?= $employer['country_name'] ?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>City</td>

                                                    <td><input class="form-control" type="text" name="city" value="<?= $employer['city'] ?>" /></td>
                                                </tr> 
                                                <tr>
                                                    <td>Area</td>

                                                    <td><input class="form-control" type="text" name="area" value="<?= $employer['area'] ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Billing Address</td>

                                                    <td><input class="form-control" type="text" name="billing_address" value="<?= $employer['billing_address'] ?>" ></td>
                                                </tr>
                                                <tr>
                                                    <td>Contact Phone</td>

                                                    <td><input class="form-control" type="text" name="contact_phone" value="<?= $employer['contact_phone'] ?>" ></td>
                                                </tr>
                                                <tr>
                                                    <td>Contact E-Mail</td>

                                                    <td><input class="form-control" type="text" name="contact_email" value="<?= $employer['contact_email'] ?>" ></td>
                                                </tr>
                                                <tr>
                                                    <td>Website Address (URL)            http://</td>

                                                    <td> <input class="form-control" type="text" name="website_address" value="<?= $employer['website_address'] ?>" size="50" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Company Logo</td>

                                                    <td><input type="file" name="company_logo" /></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center"><button class="btn btn-default btn-lg" type="submit"><i class="fa fa-wrench"></i> Update </button></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </form>
                                <?
                            } else {
                                echo'No Data';
                            }
                            ?>

                        </div>

                    </div>

                </div>

            </div>
        </div>
<!--        <div id="last">

        </div>-->


    </body>
</html>