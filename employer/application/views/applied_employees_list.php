<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Purbodeshjobs</title>
        <link rel="stylesheet" href="<?= base_url(); ?>css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>css/jquery-ui-1.8.18.custom.css" type="text/css" />
        <link href="<?= base_url(); ?>/css/bootstrap.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>/css/custom.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.css" rel="stylesheet" />
<link href="<?= base_url(); ?>/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />


<link href="<?= base_url(); ?>/css/morris-0.4.3.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

        <style type="text/css">

        </style>
        <script type="text/javascript">
            function OpenResumeWindow(link){
                window.open(link, "mywin", "menubar=0, toolbar=1, scrollbars=1, status=1, width=800, resizable=1, top=50, left=100");
            }
    
    
        </script>
    </head>
    <body>
        <div id="container">
            <div id="employer_header">
            </div>
            <div id="employer_page">
                <div id="employer_menu">
                    <?= $this->load->view('employer_menu'); ?>	
                </div>
                <div id="main">

                    <div id="employer_content">
                        <div id="left_panel">
                            <h3>Corporate Panel</h3>
                            <div id="main_left"> 
                                <div id="main_left_header">
                                    <h2>Main Menu</h2>
                                </div>
                                <div id="main_left_content">
                                    <?= $this->load->view('employer_mainmenu'); ?>
                                </div>
                                <div id="acc_status">
                                    <h2>Account Status</h2>  
                                </div>
                                <div id="acc_status_content">
                                    <ul>
                                        <li><a href="#">Job Posting</a></li>
                                        <li><a href="#">CV Database</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="right_panel" >

                            <div id="right_panel_header">
                                <?= $this->load->view('employer_top'); ?>
                            </div>

                            <div id="main_box">
                                <?
                                if (isset($employees) && count($employees) > 0) {
                                    ?>
                                    <table class="table table-striped table-bordered table-hover" style="margin-top:50px">
                                        <thead>
                                        <th>
                                            Sl
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Highest Degree
                                        </th>
                                        <th>
                                            Age
                                        </th>
                                        <th>
                                            Experience
                                        </th>
                                        <th>
                                            Expected Salary
                                        </th>
                                        </thead>
                                        <tbody>
                                            <?
                                            $i = 1;
                                            foreach ($employees as $employee) {
                                                echo '<tr>';
                                                echo '<td>' . $i++ . '</td>';
                                                //echo '<td><a href="../../../employee/index.php/cont_email/resume_to_employer?emp_id=' . $employee['id'] . '&job_id=' . $_GET['job_id'] . '\'" onclick="OpenResumeWindow(\'http://' . $_SERVER['HTTP_HOST'] . '/purbodesh/employee/index.php/cont_email/resume_to_employer?emp_id=' . $employee['id'] . '&job_id=' . $_GET['job_id'] . '\')">' . $employee['name'] . '</a></td>';
                                                  echo '<td><a href="#" onclick="OpenResumeWindow(\'http://' . $_SERVER['HTTP_HOST'] . '/purbodesh/employee/index.php/cont_email/resume_to_employer?emp_id=' . $employee['id'] . '&job_id=' . $_GET['job_id'] . '\')">' . $employee['name'] . '</a></td>';
                                                echo '<td>' . $employee['degree_title'] . '</td>';
                                                echo '<td>' . $employee['age'] . '</td>';
                                                if (isset($employee['company_name'])) {
                                                    echo '<td>' . $employee['company_name'] . '<br/>';
                                                    echo $employee['position_held'] . '<br/>';
                                                    echo '(' . date('F, Y', strtotime($employee['from'])) . '-';
                                                    echo $employee['to'] . ')';
                                                    echo '</td>';
                                                } else {
                                                    echo '<td></td>';
                                                }
                                                echo '<td>' . $employee['expected_salary'] . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?
                                } else {
                                    echo 'sorry, no data found.';
                                }
                                ?>
                            </div>
                            <div>
                                <?= isset($links) ? $links : '' ?>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </body>
</html>