<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Invoice</title>
        <style type="text/css">
            .right{
                text-align: right;
            }
            .left{
                text-align: left
            }
        </style>
    </head>
    <body>
        <div style="text-align: center; margin: 0 auto;width: 70%">
            <h3 style="border-bottom: 1px solid #000; width: 90%; margin: 0 auto;">Azadi Jobs</h3>
            <? if (isset($invoice)) { ?>
                <div style="margin: 20px auto;">
                    <h3><u>Invoice</u></h3>
                    <table border="0" style="margin: 0 auto">
                        <tr>
                            <td class="right">Invoice No:</td>
                            <td class="left"><?= $invoice['voucher_no'] ?></td>
                        </tr>
                        <tr>
                            <td class="right">Issued To:</td>
                            <td class="left"><?= $invoice['company_name'] ?></td>
                        </tr>
                        <tr>
                            <td class="right">VAT reg. no:</td>
                            <td class="left">{ }</td>
                        </tr>
                        <tr>
                            <td class="right">Issued on:</td>
                            <td class="left"><?= date('d F, Y', strtotime($invoice['create_date'])) ?></td>
                        </tr>
                        <tr>
                            <td class="right">Attn:</td>
                            <td class="left">Mr. <?= $invoice['billing_contact'] ?></td>
                        </tr>
                    </table>
                </div>
                <div>
                    <p style="text-align: left">
                        Dear Sir/Madam, <br/>
                        Thank you for choosing our services. For your kind information, you are requested to view the following voucher:
                    </p>
                    <table border="1">
                        <tr>
                            <th>Description of the Products/Services</th>
                            <th>Amount in Taka</th>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <span>
                                    Payment for &quot;Online Job Posting&quot; at Classified section of azadijobs.com
                                    from <?= date('d F, Y', strtotime($invoice['posting_date'])) ?> to 
                                    <?= date('d F, Y', strtotime($invoice['deadline'])) ?> for the following job position(s):
                                </span><br/>
                                <?= $invoice['job_title'] ?>
                            </td>
                            <td style="vertical-align: text-top"><?= $invoice['amount'] ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                VAT
                            </td>
                            <td style="vertical-align: text-top"><?= $invoice['vat'] ?></td>
                        </tr>
						<tr>
                            <td style="text-align: left">
                                Discount
                            </td>
                            <td style="vertical-align: text-top"><?= $invoice['discount'] ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <b>Total</b>
                            </td>
                            <td style="vertical-align: text-top"><?= $invoice['amount'] + $invoice['vat'] - $invoice['discount'] ?></td>
                        </tr>
                    </table>
                </div>
                <div>
                    Note: All Payment should be in account payee cheque in favour of "azadijobs.com".<br/>
                    We request you to send Cheque/Demand draft/Pay order by courier to the address below within 2 days.
                </div>
                <div style="margin-top: 30px;border-top: 1px solid #000">
                    Singapore Market(5th floor), Agrabad Access Road, Hajipara, Chittagong, Bangladesh.
                </div>
            <? } else { ?>
                <div>Sorry, something is wrong.</div>
            <? } ?>
        </div>
    </body>
</html>