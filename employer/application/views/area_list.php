<? 
    $size = isset($area_size) ? $area_size : 1; 
    $name = isset($area_name) ? $area_name : 'job_area_name';
    $id = isset($area_id) ? $area_id : 'job_area_id';
?>
<select class="form-control" id="<?=$id?>" style="width:180px;" size="<?=$size;?>" name="<?=$name?>">
<?php
    if($job_areas){
        foreach ($job_areas as $job_area){
            echo '<option value="' . $job_area['area_id'] . '">' . $job_area['area_name'] . '</option>';
        }
    }
?>
</select>