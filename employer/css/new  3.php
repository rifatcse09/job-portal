 <div id="rightside" class="table-responsive">
                                <form action="<?= site_url() ?>/home/delete_job" method="post">
                                    <table class="table table-striped table-bordered table-hover" style="margin-top:50px">
                                       <thead>
                                        <tr>
                                            <th scope="col">SL</th>
                                            <th scope="col">Job Title</th>
                                            <th scope="col">Post Date</th>
                                            <th scope="col">Deadline</th>
                                            <th scope="col">Repost</th>
                                            <th scope="col">Edit</th>
                                            <th scope="col">Applied</th>
                                            <th scope="col">Viewed</th>
                                            <th scope="col">Not Viewed</th>
<!--                                            <th scope="col">Shortlisted	</th>-->
                                            <th></th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <?
                                        if (isset($jobs)) {
                                            foreach ($jobs as $k => $job) {
                                                ?>
                                        <tbody>
                                                <tr>
                                                    <td style="text-align:center "><?= $k + 1 ?></td>
                                                    <td style="text-align:center "><?= $job['job_title'] ?></td>
                                                    <td style="text-align:center "><?= date("d-M-y", strtotime($job['posting_date'])) ?></td>
                                                    <td style="text-align:center "><?= date("d-M-y", strtotime($job['deadline'])) ?></td>
                                                    <td style="text-align:center "><a style="color:#000" href="javascript:job_repost(<?= $job['emp_job_id'] ?>)">Repost</a></td>
                                                    <td style="text-align:center "><a style="color:#000" href="<?= site_url() ?>/home/edit_jobposting?job_id=<?= $job['emp_job_id'] ?>">Edit</a></td>
                                                    <td style="text-align:center "><?= isset($job['applied']) ? '<a style="color:#000" href="' . site_url() . '/cont_employee_list/view_applied_employees?job_id=' . $job['emp_job_id'] . '">' . $job['applied'] . '</a>' : 0 ?></td>
                                                    <td style="text-align:center "><?= isset($job['viewed']) ? $job['viewed'] : 0 ?></td>
                                                    <td style="text-align:center "><?= isset($job['not viewed']) ? $job['not viewed'] : 0 ?></td>
        <!--                                                    <td>0</td>-->
                                                    <td></td>
                                                    <td style="text-align:center "><input name="job_ids[]" type = "checkbox" value="<?= $job['emp_job_id'] ?>" /></td>
                                                </tr>
                                                </tbody>
                                                <?
                                            }
                                        }
                                        ?>
                                    </table>
                                    <div id="rightside_bottom_deletebox" style="margin-right: 20px;margin-bottom: 5px;">
                                        <!--<input type="submit" class="btn btn-danger" <i class="fa fa-pencil"></i> value="Delete"/>       --> 
                                        <button class="btn btn-danger" type="submit"><i class="fa fa-pencil"></i> Delete</button>
                                    </div>
                                </form>
                                <div id="page">
                                    <? echo isset($links) ? $links : 'e'; ?>
                                </div>
                                <div id="rightside_bottom">





                                </div>

                            </div>
                            <div id="rightside_bottomenu">
                                <div id="rightside_bottomenu_post">
                                    <button class="btn btn-primary btn-sm" style="margin-right:50px;padding-right: 20px;color: #fff;">
                                    <i class="fa fa-keyboard-o"></i>
                                      <a href="<?= site_url()?>/employer_jobposting_step01">Post a New Job </a>
                                    </button>
                                </div>
                                <div id="rightside_bottomenu_showDelete">
                                    <button class="btn btn-warning btn-sm" style="margin-right:50px">
                                    <i class="fa fa-trash"></i><a href="<?= site_url()?>/home/delete_show">Show Deleted Jobs</a>
                                    </button>
                                </div>
                                <div id="rightside_bottomenu_viewAll">
                                  <button class="btn btn-success btn-sm" style="margin-right:50px">
                                       
                                        <i class="fa fa-eye"></i><a href="">View All Jobs</a>
                                    </button>
                                    
                                </div>
                                <div id="rightside_bottomenu_next">
<!--                                <div id="rightside_bottomenu_next">
                                    <button class="btn btn-info btn-sm" style="margin-right:50px">
                                    <i class="fa fa-hand-o-right"></i>    <a href="" >Next </a>
                                    </button>
                                </div>-->
                                </div>
                            </div>
                        </div> 
                    </div>
