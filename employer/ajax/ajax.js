// ************************************************ Common  Start ************************************************
var xmlHttp;
function GetXmlHttpObject()
{
	xmlHttp=null;
	try
	{
		//alert('Firefox');
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e)
	{
		//alert('IE');
		// Internet Explorer
		try
		{
			//alert('IE');
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			//alert('IE');
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");			
		}
	}
	
	//xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
	//xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	//xmlHttp=new XMLHttpRequest();
	return xmlHttp;
}

var objRequest = GetXmlHttpObject();

// ************************************************ Common End ************************************************


function Refresh(strContactPerson, IsBillingContact)
{
	//document.getElementById('divMyData').innerHTML = "<img src='../images/indicator.gif' />";
	var strPage = 'http://localhost/jobsite/index.php/Corporate/GetContact?contactPerson=' + strContactPerson + '&BillingContact=' + IsBillingContact;
	objRequest.open('GET', strPage, true);
	objRequest.onreadystatechange = updateLogData;
	objRequest.send(null);
	return false;
		
	//}
}

function updateLogData()
{
   if(objRequest.readyState == 4)
   {
	  //alert(objRequest.responseText);
      document.getElementById('divMyData').innerHTML = objRequest.responseText;
   }
}